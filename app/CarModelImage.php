<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CarModelImage extends Model
{
    protected $table = 'au_brands_images';
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PropertyBlogImageModel extends Model
{
    protected $table = 're_property_blog_images';
}

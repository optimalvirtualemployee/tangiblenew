<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DialColorModel extends Model
{
    protected $table = 'wa_factory_features_dial_color';
    protected $fillable = ['dial_color', 'status'];
}

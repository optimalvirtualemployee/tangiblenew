<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PropertyBlogModel extends Model
{
    protected $table = 're_blog';
}

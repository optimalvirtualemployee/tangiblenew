<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PropertyFeaturesModel extends Model
{
    protected $table = 're_property_features';
    protected $fillable = ['listing_id','view','concierge_services','country_club','deck','dock','doorman','gymnasium','garden'
        ,'balcony','outdoor_area','pool','fireplace','garage','billiards_room','bowling_alley','built_in_vaccum','studio','office','kitchen'
        ,'butlers_pantry','fitness_center','gated_community','guest_house','security_guard','hardwood_flooring','horse_facilties','basketball_court'
        ,'intercom_system','library','runway','elevator','lake','recreation_area','putting_green','heated_floores','security_system','ski_in_out','solar_power'
        ,'spa_hot_tub','split_system_air_conditioning','squash_court','staff_quarters','steam_room_sauna','tennis_court','terrace','underground_parking','vineyard'
        ,'sprinkler_system','lawn_sprinklers','walk_in_closet','waterfront','bar','zoned_air_condition','camera_surviellence_system','wine_cellar','cinema'];
}

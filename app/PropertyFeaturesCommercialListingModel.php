<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PropertyFeaturesCommercialListingModel extends Model
{
    protected $table = 're_properties_features_listing_commercial';
}

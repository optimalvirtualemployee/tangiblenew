<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AutomobileBannerModel extends Model
{
    protected $table = 'au_banner_images';
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserDetailsModel extends Model
{
    protected $table = 'tbl_user_details';

    public function cityName()
    {
        return $this->belongsTo('App\CityModel', 'city_id', 'id');
    }

 	public function countryName()
    {
        return $this->belongsTo('App\CountryModel', 'country_id', 'id');
    }

       
}

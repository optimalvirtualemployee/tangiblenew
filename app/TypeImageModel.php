<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TypeImageModel extends Model
{
    protected $table = 'wa_type_images';
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WatchModelImage extends Model
{
    protected $table = 'wa_brands_images';
}

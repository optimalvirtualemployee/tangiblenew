<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Permission;
use App\CategoriesModel;
use App\CategoriesPermissionModel;

class CategoriesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $categories = CategoriesModel::get(); 
        
        return view('admin.admincommon.categories.index')->with(array(
            'categories' => $categories
            
        ));
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        
    }
    
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $category = CategoriesModel::find($id);
        $categoryPermissions = Permission::join("tbl_category_permission","tbl_category_permission.permissionId","=","permissions.id")
        ->where("tbl_category_permission.categoryId",$id)
        ->get();
        
        return view('admin.admincommon.categories.show',compact('category', 'categoryPermissions'));
    }
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $category = CategoriesModel::find($id); 
        
        $permission = Permission::get();
        
        $categoryPermissions = DB::table("tbl_category_permission")->where("tbl_category_permission.categoryId",$id)
        ->pluck('tbl_category_permission.permissionId')
        ->all();
        
        return view('admin.admincommon.categories.edit',compact('category', 'permission' ,'categoryPermissions'));
    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        CategoriesPermissionModel::where('categoryId', '=', $id)->delete();
        
        if($request->input('permission')){
            foreach ($request->input('permission') as $permission){
                
                $categoryPermission = new CategoriesPermissionModel();
                
                $categoryPermission->categoryId = $id;
                $categoryPermission->permissionId = $permission;
                
                $categoryPermission->save();
            }
        }
        
        return redirect('/admin/categories')
        ->with('success','Category Permission updated successfully');
    }
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        
    }
}

<?php

namespace App\Http\Controllers\admin;

use App\WatchContractModel;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class WatchContractController extends Controller
{
   
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
     
    public function createContract()
    { 
        $data = array();
        
        $data['watchContract'] = WatchContractModel::find(1);
        
        return view('admin.watch.contract.createContract')->with($data);
    }
    
    
    
    public function updateContract(Request $request)
    { 
       
       $data = $request->validate([
            
            'contractText' => ['required'],
        ]);
        
        $id = 1;
        
        $watchContract = WatchContractModel::find($id);
        
        $watchContract->contractText = $request->input('contractText');
        $watchContract->popUpText = $request->input('popUpText');
        
        $watchContract->save();
        
     
        return redirect('/admin/watch/contract')->with('message', 'Information Updated successfully!');
        
    }
    
   
}

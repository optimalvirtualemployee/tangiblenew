<?php

namespace App\Http\Controllers;

use DB;
use PDF;
use App\User;
use App\CityModel;
use App\StateModel;
use App\AgentModel;
use App\CountryModel;
use App\CurrencyModel;
use App\AgenciesModel;
use App\UserDetailsModel;
use App\ShippingPriceModel;
use App\WatchContractModel;
use Illuminate\Http\Request;
use App\WatchDataListingModel;

class PdfController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index($productID,$agentId,$userID)
    {
        $data = $replacements =array();
        
        $data['watchContract'] = WatchContractModel::find(1);

        $productDetails= WatchDataListingModel::find($productID);
        $watchBrand    = DB::table('wa_brands')->where('id',$productDetails->brand_id)->get()->first();
        $watchModel    = DB::table('wa_model')->where('id',$productDetails->model_id)->where('watch_brand_id',$productDetails->brand_id)->get()->first();
        $sellerDetails = AgentModel::find($agentId);
        $buyerDetails  = User::where('id',$userID)->where('usertype',3)->get()->first();
        $buyerAddress  = DB::table('tbl_user_details')->where('user_id',$userID)->get()->first();
        $ShippingPrice = (!empty(ShippingPriceModel::where('product_id',$productID)->get()->first()) ? ShippingPriceModel::where('product_id',$productID)->get()->first() : '0');
        $selectedCurrency = CurrencyModel::select('currency_code as code')->where('status', '=', '1')->first();
        currency()->setUserCurrency($selectedCurrency->code);
        
        if(!empty($sellerDetails->agency_id)){
            if(!is_numeric($sellerDetails->agency_id)){//if this returns as true then, it is a private seller

               //SELLER------------------------------------------------------------------------------------------//
               $sellerCityName   =(!empty($sellerDetails->city_id)   ? CityModel::find($sellerDetails->city_id)->city_name:""); 
               $sellerStateName  =(!empty($sellerDetails->state_id)  ? " / ".StateModel::find($sellerDetails->state_id)->state_name:"");
               $sellerCountryName=(!empty($sellerDetails->country_id)? CountryModel::find($sellerDetails->country_id)->country_name:"");
               
               //BUYER-----------------------------------------------------------------------------------------//
               $buyerCityName   =(!empty($buyerAddress->city_id)   ? CityModel::find($buyerAddress->city_id)->city_name:""); 
               $buyerStateName  =(!empty($buyerAddress->state_id)  ? " / ".StateModel::find($buyerAddress->state_id)->state_name:"");
               $buyerCountryName=(!empty($buyerAddress->country_id)? CountryModel::find($buyerAddress->country_id)->country_name:"");
               //------------------------------------------------------------------------------------------------//
                $replacements = [
                                    "{{contract_heading}}" => "<b style='color:#084376' class='txtUnderline'>Buyer & Sellers Details</b><br><br>",
                                    "{{seller_details}}"   => "<br><br><br><b>Seller's Detail<b><br><br>
                                                                <div class='row'>
                                                                    <div class='col-md-6'>".$sellerDetails->first_name." ".$sellerDetails->last_name."<br>". $sellerCountryName." , ".$sellerCityName.$sellerStateName."<br><br>
                                                                        Watch Reference Number: ".$productDetails->reference_no."<br>
                                                                        Watch Brand & Model: [ ".$watchBrand->watch_brand_name." ] and [ ".$watchModel->watch_model_name." ]<br>
                                                                        Sell Price: ".currency()->convert(floatval($productDetails->watch_price), 'USD', currency()->getUserCurrency())."<br>
                                                                        Shipping Price: ".currency()->convert($ShippingPrice, 'USD', currency()->getUserCurrency())."
                                                                    </div><br><br><br>",
                                    "{{buyer_details}}"    => "<br><br><b>Buyer's Detail<b><br><br>
                                                                <div class='col-md-6'>
                                                                    ".$buyerDetails->first_name." ".$buyerDetails->last_name."<br>
                                                                    ". $buyerCountryName." , ".$buyerCityName.$buyerStateName."<br><br>
                                                                </div>
                                                            </div><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>",
               ];
            }else{//if this returns as true then, it is a dealer
                $dealerDetails = AgenciesModel::find($sellerDetails->agency_id);
                
                //SELLER------------------------------------------------------------------------------------------//
               $dealerCityName   =(!empty($dealerDetails->city_id)   ? CityModel::find($dealerDetails->city_id)->city_name:""); 
               $dealerStateName  =(!empty($dealerDetails->state_id)  ? StateModel::find($dealerDetails->state_id)->state_name:"");
               $dealerCountryName=(!empty($dealerDetails->country_id)? " / ".CountryModel::find($dealerDetails->country_id)->country_name:"");
               
               //BUYER-----------------------------------------------------------------------------------------//
               $buyerCityName   =(!empty($buyerAddress->city_id)   ? CityModel::find($buyerAddress->city_id)->city_name:""); 
               $buyerStateName  =(!empty($buyerAddress->state_id)  ? " / ".StateModel::find($buyerAddress->state_id)->state_name:"");
               $buyerCountryName=(!empty($buyerAddress->country_id)? CountryModel::find($buyerAddress->country_id)->country_name:"");
               //------------------------------------------------------------------------------------------------//
                $replacements = [
                                    "{{contract_heading}}" => "<b style='color:#084376' class='txtUnderline'>Buyer & Sellers Details</b><br><br>",
                                    "{{seller_details}}"   => "<br><br><br><b>Dealer's Detail<b><br><br>
                                                                <div class='row'>
                                                                    <div class='col-md-6'>
                                                                        ".$dealerDetails->first_name." ".$dealerDetails->last_name."<br>
                                                                            ".$dealerDetails->street_1." , "
                                                                             .$dealerDetails->street_2." , "
                                                                             .$dealerDetails->postal_code." , "
                                                                             .$dealerCityName." , "
                                                                             .$dealerStateName
                                                                             .$dealerCountryName."<br>
                                                                        ".$dealerDetails->phone_no."<br>
                                                                        ".$dealerDetails->email."<br><br>
                                                                        Watch Reference Number: ".$productDetails->reference_no."<br>
                                                                        Watch Brand & Model: [ ".$watchBrand->watch_brand_name." ] and [ ".$watchModel->watch_model_name." ]<br>
                                                                        Sell Price: ".currency()->convert(floatval($productDetails->watch_price), 'USD', currency()->getUserCurrency())."<br>
                                                                        Shipping Price: ".currency()->convert($ShippingPrice, 'USD', currency()->getUserCurrency())."
                                                                    </div><br><br><br>",
                                    "{{buyer_details}}"    => "<br><br><b>Buyer's Detail<b><br><br>
                                                                <div class='col-md-6'>
                                                                    ".$buyerDetails->first_name." ".$buyerDetails->last_name."<br>
                                                                    ". $buyerCountryName." , ".$buyerCityName.$buyerStateName."<br><br>
                                                                </div>
                                                            </div><br><br><br><br><br><br><br><br><br><br><br><br>
                                                            <br><br><br><br><br><br><br><br><br><br><br><br>",
                                ];                            
        }
    }
        $data['HTML_content'] = strtr($data['watchContract']->contractText,$replacements);

        // $pdf = PDF::loadView($data['HTML_content']);
        $pdf = PDF::loadView('pdf.contract',$data);
        return $pdf->download('customers.pdf');
    }

    public function customDuties(){
        $data = array();
        
        $data['watchContract'] = WatchContractModel::find(1);
        $data['popupContent'] = $data['watchContract']->popUpText;

        echo json_encode($data['popupContent']);
        
    }
}

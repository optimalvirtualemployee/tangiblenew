<?php

namespace App\Http\Controllers;

use App\AgentModel;
use App\AutomobileImageModel;
use App\CityModel;
use App\ContactusModel;
use App\CurrencyModel;
use App\EnquiryModel;
use App\MyMessageModel;
use App\OrderDeliveryDetailModel;
use App\ProductOrderModel;
use App\StateModel;
use App\WatchImageModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\CountryModel;
use App\FinalOfferModel;
use App\OrderTrackingDetailModel;
use App\OrderStatusHierarchyModel;

class SellerOfferController extends Controller
{
    public function acceptOffer(Request $request){
        
        //dd($request->all());
        
        if (Auth::user() == null){
            return redirect('/');
        }
        
        $id = Auth::user()->id;
        
        $currentStep = ProductOrderModel::select('order_status')->where('id', '=', $request->enquiryId)->where('category', '=', $request->category)->where('agent_userid', '=', $id)->first();
        
        $contactUs = ContactusModel::get()->first();
        $selectedCurrency = CurrencyModel::select('currency_code as code')->where('status', '=', '1')->first();
        
        currency()->setUserCurrency($selectedCurrency->code);

        $enquiry ="";
        DB::enableQueryLog();
        if($request->category == 'Automobile'){
        $enquiry = EnquiryModel::select('tbl_enquiry.agent_id','tbl_enquiry.id as id','productId','tbl_enquiry.created_at', 'ad_title', 'value', 'tbl_country.filename as countryflag',
            'tbl_agencies.company_name', DB::raw('CONCAT(tbl_agent.first_name, " ", tbl_agent.last_name) AS agent_name'), 'tbl_agencies.created_at as agencycreate')
            ->where('tbl_enquiry.id','=',$request->enquiryId)
            ->where('category', '=', 'Automobile')
            ->join('au_automobile_detail','au_automobile_detail.id', 'tbl_enquiry.productId')
            ->join('tbl_country','tbl_country.id', 'au_automobile_detail.country_id')
            ->join('tbl_agent','tbl_agent.id', 'tbl_enquiry.agent_id')
            ->leftJoin('tbl_agencies','tbl_agencies.id', '=', 'tbl_agent.agency_id')
            ->first();
        
            $images = AutomobileImageModel::where('listing_id',$enquiry->productId)->first();
        }else if($request->category == 'Watch' ){
            $enquiry = MyMessageModel::select('submitpricerequest.submit_price as submitprice','submitpricerequest.id as submitpriceId','tbl_enquiry.agent_id','tbl_enquiry.id as id','productId','tbl_enquiry.created_at', 'ad_title', 'watch_price as value', 'tbl_country.filename as countryflag',
                'tbl_agencies.company_name', DB::raw('CONCAT(tbl_agent.first_name, " ", tbl_agent.last_name) AS agent_name'), 'tbl_agencies.created_at as agencycreate', 'tbl_enquiry.productId as productId', 'tbl_user_details.user_mobile as userMobile', 'tbl_user_details.country_id as userCountryId','users.email as userEmail',DB::raw('CONCAT(users.first_name, " ", users.last_name) AS user_name'), 'tbl_enquiry.userID as userID',
                'tbl_final_offer.finalOfferValue as finalOfferValue', 'tbl_final_offer.deliveryTime as deliveryTime' ,'tbl_final_offer.shippingCost as shippingCost', 'tbl_final_offer.messageFrom as messageFinalFrom', 'tbl_user_details.city_id as userCityId','tbl_user_details.country_id as userCountryId','tbl_message_details.id as messageId','tbl_message_details.category as category')
                ->where('tbl_message_details.id', '=', $request->input('messageId'))
                /* ->where('tbl_message_details.queryType', '=', 'counterOffer') */
                ->where('tbl_message_details.category', '=', 'watch')
                ->join('submitpricerequest','submitpricerequest.id', 'tbl_message_details.message_id')
                ->join('tbl_enquiry','tbl_enquiry.id','tbl_message_details.message_id')
                ->leftJoin( 'tbl_final_offer', 'tbl_final_offer.messageId','tbl_message_details.id')
                ->join('tbl_user_details', 'tbl_user_details.user_id','tbl_enquiry.userID')
                ->join('users', 'users.id','tbl_enquiry.userID')
                ->join('wa_watch_detail','wa_watch_detail.id', 'tbl_enquiry.productId')
                ->join('tbl_country','tbl_country.id', 'wa_watch_detail.country_id')
                ->join('tbl_agent','tbl_agent.id', 'tbl_enquiry.agent_id')
                ->leftJoin('tbl_agencies','tbl_agencies.id', '=', 'tbl_agent.agency_id')
                ->first();
                //dd($enquiry);
                $images = WatchImageModel::where('listing_id',$enquiry->productId)->first();
        }else if($request->category == 'Watch' && $request->queryType == 'submitprice'){
            $enquiry = MyMessageModel::select('submitpricerequest.submit_price as submitprice','submitpricerequest.id as submitpriceId','tbl_enquiry.agent_id','tbl_enquiry.id as id','productId','tbl_enquiry.created_at', 'ad_title', 'watch_price as value', 'tbl_country.filename as countryflag',
                'tbl_agencies.company_name', DB::raw('CONCAT(tbl_agent.first_name, " ", tbl_agent.last_name) AS agent_name'), 'tbl_agencies.created_at as agencycreate', 'tbl_enquiry.productId as productId', 'tbl_user_details.user_mobile as userMobile', 'tbl_user_details.country_id as userCountryId','users.email as userEmail',DB::raw('CONCAT(users.first_name, " ", users.last_name) AS user_name'), 'tbl_enquiry.userID as userID',
                 'tbl_final_offer.shippingCost as shippingCost', 'tbl_final_offer.messageFrom as messageFinalFrom')
                ->where('tbl_message_details.message_id', '=', $request->input('enquiryId'))
                ->where('tbl_message_details.queryType', '=', 'submitprice')
                ->where('tbl_message_details.category', '=', 'watch')
                ->join('submitpricerequest','submitpricerequest.id', 'tbl_message_details.message_id')
                ->join('tbl_enquiry','tbl_enquiry.id','tbl_message_details.message_id')
                ->leftJoin( 'tbl_final_offer', 'tbl_final_offer.messageId','tbl_message_details.id')
                ->join('tbl_user_details', 'tbl_user_details.user_id','tbl_enquiry.userID')
                ->join('users', 'users.id','tbl_enquiry.userID')
                ->join('wa_watch_detail','wa_watch_detail.id', 'tbl_enquiry.productId')
                ->join('tbl_country','tbl_country.id', 'wa_watch_detail.country_id')
                ->join('tbl_agent','tbl_agent.id', 'tbl_enquiry.agent_id')
                ->leftJoin('tbl_agencies','tbl_agencies.id', '=', 'tbl_agent.agency_id')
                ->first();
                //dd($enquiry);
                $images = WatchImageModel::where('listing_id',$enquiry->productId)->first();
        }
        //dd($enquiry);
        
        if($enquiry->userCityId != null)
            $user_city = CityModel::find($enquiry->userCityId);
            
        if($enquiry->userCountryId != null)
            $user_country = CountryModel::find($enquiry->userCountryId);
                
         if($user_city != null)
             $user_state = StateModel::find($user_city->state_id);
                    
          $selectedCurrency = CurrencyModel::select('currency_code as code')->where('status', '=', '1')->first();
                    
          $converted_shipping = currency()->convert(floatval($enquiry->shippingCost), "USD",$selectedCurrency->code, false);
          //dd($currentStep->order_status);
          
          $country = CountryModel::get();

        if($currentStep->order_status == 'waiting for approval'){
            return view('tangiblehtml.acceptoffer', compact('currentStep','user_city','user_country', 'user_state','contactUs', 'enquiry', 'images', 'selectedCurrency','converted_shipping', 'country'));
        }else if($currentStep->order_status == 'seller_approved' || $currentStep->order_status == 'waiting_for_payment'){
            return view('tangiblehtml.initialOrderStatusSeller', compact('currentStep','user_city','user_country', 'user_state','contactUs', 'enquiry', 'images', 'selectedCurrency','converted_shipping'));
        }else if($currentStep->order_status == 'payment done'){
            return view('tangiblehtml.shipping-info-seller', compact('currentStep','user_city','user_country', 'user_state','contactUs', 'enquiry', 'images', 'selectedCurrency','converted_shipping'));
        }else if($currentStep->order_status == 'shipped'){
            return view('tangiblehtml.initialOrderStatusSeller', compact('currentStep','user_city','user_country', 'user_state','contactUs', 'enquiry', 'images', 'selectedCurrency','converted_shipping'));
        }else if($currentStep->order_status == 'delivered'){
            
            $deliveryStatus = OrderDeliveryDetailModel::where('enquiry_id', '=',$request->enquiryId)->first();
            
            return view('tangiblehtml.final-delivery-status-seller', compact('deliveryStatus','currentStep', 'contactUs', 'enquiry', 'images', 'user_state', 'user_country', 'user_city', 'converted_shipping'));
        }
    }
    
    public function submitFinalOffer(Request $request){
        
        $agent = AgentModel::find($request->agentId);
        
        $request->validate([

            'enquiryId' => ['required'],
            'finalOfferValue' => ['required'],
            'deliveryTime' => ['required'],
            'offerValid' => ['required'],
            'message' => ['required']
            
        ]);
        
        $converted_amount = NULL;
        
        $converted_shipping = NULL;
        
        if($request->finalOfferValue != ''){
            $selectedCurrency = CurrencyModel::select('currency_code as code')->where('status', '=', '1')->first();

            $converted_amount = currency()->convert(floatval($request->finalOfferValue), $selectedCurrency->code, "USD",false);
        }
        
        if($request->shippingCost != ''){
            $selectedCurrency = CurrencyModel::select('currency_code as code')->where('status', '=', '1')->first();
            
            $converted_shipping = currency()->convert(floatval($request->shippingCost), $selectedCurrency->code, "USD",false);
        }
        
        
        $seller_category = 'private_seller';
        if($agent->agency_id != null)
            $seller_category = 'agent';
            
            $myMessage = new MyMessageModel();
            $myMessage->message = $request->message;
            $myMessage->message_id = $request->enquiryId;
            $myMessage->queryType = 'counterOffer';
            $myMessage->category = 'Watch';
            $myMessage->agent_id = $request->agentId;
            $myMessage->product_id = $request->productid;
            $myMessage->messageFrom = $agent->userId;
            $myMessage->messageTo = $request->userID;
            
            $myMessage->save();
        
        $finalOffer = new FinalOfferModel();
        
        $finalOffer->messageId = $myMessage->id;
        $finalOffer->finalOfferValue = $converted_amount;
        $finalOffer->shippingCost = $converted_shipping;
        $finalOffer->deliveryTime = $request->input('deliveryTime');
        $finalOffer->offerValid = $request->input('offerValid');
        $finalOffer->message = $request->input('message');
        $finalOffer->messageFrom = 'seller';
        
        $finalOffer->save();
        
            
        $order =  ProductOrderModel::find($request->input('enquiryId'));
            
        $order->order_status = 'seller_approved';
            
        $order->update();
            
        
        $orderStatus = new OrderStatusHierarchyModel();
        
        $orderStatus->enquiryId = $request->enquiryId;
        $orderStatus->status = 'seller_approved';
        
        $orderStatus->save();
        
        $response = "success";
        
        $myResponse = json_encode($response);
        
        return $myResponse;
    }
    
    public function submitShippingDetail(Request $request){
        
        $request->validate([
            
            'enquiryId' => ['required'],
            'courierName' => ['required'],
            'trackingNumber' => ['required'],
            'trackingUrl' => ['required']
            
        ]);
        
        
        if (Auth::user() == null){
            return redirect('/');
        }
        
        $id = Auth::user()->id;
        
        
        $shippmentDetails = new OrderTrackingDetailModel();
        
        $shippmentDetails->enquiryId = $request->enquiryId;
        $shippmentDetails->courier_service = $request->courierName;
        $shippmentDetails->tracking_id = $request->trackingNumber;
        $shippmentDetails->tracking_url = $request->trackingUrl;

        $shippmentDetails->save();
        
        $enquiry = MyMessageModel::select('submitpricerequest.submit_price as submitprice','submitpricerequest.id as submitpriceId','tbl_enquiry.agent_id','tbl_enquiry.id as id','productId','tbl_enquiry.created_at', 'ad_title', 'watch_price as value', 'tbl_country.filename as countryflag',
            'tbl_agencies.company_name', DB::raw('CONCAT(tbl_agent.first_name, " ", tbl_agent.last_name) AS agent_name'), 'tbl_agencies.created_at as agencycreate', 'tbl_enquiry.productId as productId', 'tbl_user_details.user_mobile as userMobile', 'tbl_user_details.country_id as userCountryId','users.email as userEmail',DB::raw('CONCAT(users.first_name, " ", users.last_name) AS user_name'), 'tbl_enquiry.userID as userID',
            'tbl_final_offer.finalOfferValue as submitprice', 'tbl_final_offer.shippingCost as shippingCost', 'tbl_final_offer.messageFrom as messageFinalFrom', 'tbl_user_details.city_id as userCityId','tbl_user_details.country_id as userCountryId','tbl_message_details.id as messageId')
            ->where('tbl_message_details.id', '=', $request->input('messageId'))
            /* ->where('tbl_message_details.queryType', '=', 'counterOffer') */
        ->where('tbl_message_details.category', '=', 'watch')
        ->join('submitpricerequest','submitpricerequest.id', 'tbl_message_details.message_id')
        ->join('tbl_enquiry','tbl_enquiry.id','tbl_message_details.message_id')
        ->leftJoin( 'tbl_final_offer', 'tbl_final_offer.messageId','tbl_message_details.id')
        ->join('tbl_user_details', 'tbl_user_details.user_id','tbl_enquiry.userID')
        ->join('users', 'users.id','tbl_enquiry.userID')
        ->join('wa_watch_detail','wa_watch_detail.id', 'tbl_enquiry.productId')
        ->join('tbl_country','tbl_country.id', 'wa_watch_detail.country_id')
        ->join('tbl_agent','tbl_agent.id', 'tbl_enquiry.agent_id')
        ->leftJoin('tbl_agencies','tbl_agencies.id', '=', 'tbl_agent.agency_id')
        ->first();
        
        $images = WatchImageModel::where('listing_id',$enquiry->productId)->first();
        
        $order =  ProductOrderModel::find($request->input('enquiryId'));
        
        $order->order_status = 'shipped';
        
        $order->update();
        
        $orderStatus = new OrderStatusHierarchyModel();
        
        $orderStatus->enquiryId = $request->enquiryId;
        $orderStatus->status = 'shipped';
        
        $orderStatus->save();
        
        $contactUs = ContactusModel::get()->first();
        $selectedCurrency = CurrencyModel::select('currency_code as code')->where('status', '=', '1')->first();
        
        currency()->setUserCurrency($selectedCurrency->code);
        
        if($enquiry->userCityId != null)
            $user_city = CityModel::find($enquiry->userCityId);
            
            if($enquiry->userCountryId != null)
                $user_country = CountryModel::find($enquiry->userCountryId);
                
                if($user_city != null)
                    $user_state = StateModel::find($user_city->state_id);
                    
                    $selectedCurrency = CurrencyModel::select('currency_code as code')->where('status', '=', '1')->first();
                    
                    $converted_shipping = currency()->convert(floatval($enquiry->shippingCost), "USD",$selectedCurrency->code, false);
        
        $currentStep = ProductOrderModel::select('order_status')->where('id', '=', $request->enquiryId)->where('category', '=', $request->category)->where('agent_userid', '=', $id)->first();
        
        return view('tangiblehtml.initialOrderStatusSeller', compact('currentStep','user_city','user_country', 'user_state','contactUs', 'enquiry', 'images', 'selectedCurrency','converted_shipping'));
    }
}

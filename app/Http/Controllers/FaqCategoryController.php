<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\FaqCategoryModel;

class FaqCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $FAQCategoryListing = FaqCategoryModel::get();
        return view('admin.admincommon.faq.category.index', compact('FAQCategoryListing'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.admincommon.faq.category.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validate = $this->validate($request, [
            'category'   => 'required',
            'status'     => 'required',
        ]);

        $faqCat            = new FaqCategoryModel;
        $faqCat->name      = $request->category;
        $faqCat->slug      = $request->slug;
        $faqCat->status    = $request->status;
        
        $faqCat->save();
        return redirect('/admin/faq-cat')->with('success_msg','FAQ category added successfully!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $editData = FaqCategoryModel::find($id);
        return view('admin.admincommon.faq.category.edit',compact('editData'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $faqCat = FaqCategoryModel::find($id);

        $validate = $this->validate($request, [
            'category'   => 'required',
            'status'     => 'required',
        ]);

        $faqCat->name      = $request->category;
        $faqCat->slug      = $request->slug;
        $faqCat->status    = $request->status;
        
        $faqCat->save();
        return redirect('/admin/faq-cat')->with('success_msg','FAQ category updated successfully!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post = FaqCategoryModel::find($id);    
        $post->delete();
        return redirect()->back()->with('success_msg','FAQ Category deleted successfully!');
    }
}

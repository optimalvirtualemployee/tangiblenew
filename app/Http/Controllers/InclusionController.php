<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\InclusionModel;

class InclusionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $inclusion = InclusionModel::get();
        return view('admin.watch.inclusion.index',compact('inclusion'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.watch.inclusion.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validate = $this->validate($request, [
            'inclusion' => 'required|regex:/^[\pL\s_]+$/u|unique:wa_inclusion',
            'status' => 'required'
        ]);

        $inclusion = InclusionModel::create($validate);
        $inclusion->save();
        return redirect('/admin/inclusion')->with('success_msg','Inclusion Created successfully!');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $inclusion_data = InclusionModel::find($id);
        return view('admin.watch.inclusion.edit', compact('inclusion_data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validate = $this->validate($request, [
            'inclusion' => 'required|regex:/^[\pL\s_]+$/u'
        ]);

        $inclusionUpdate = InclusionModel::find($id);

        if ($inclusionUpdate->inclusion == $validate['inclusion']) {
            $inclusionUpdate->status = $request->input('status');
        } else {
            $validate = $this->validate($request, ['inclusion' => 'unique:wa_inclusion']);
            $inclusionUpdate->inclusion = $validate['inclusion'];
            $inclusionUpdate->status = $request->input('status');
        }

        $inclusionUpdate->save();

        return redirect('/admin/inclusion')->with('success_msg','Inclusion Updated successfully!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deleteRecords = InclusionModel::find($id)->delete();
        return redirect('/admin/inclusion')->with('success_msg','Inclusion deleted successfully!');
    }
}

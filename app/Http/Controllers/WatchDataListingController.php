<?php

namespace App\Http\Controllers;

use App\CountryModel;
use App\StateModel;
use App\WatchDataListingModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use App\AgentModel;
use App\CaseDiameterModel;
use App\PriceModel;
use App\Timezone;
use App\Watchbrandmodel;
use App\WatchModel;
use App\WatchColourModel;
use App\MovementModel;
use App\TypeModel;
use App\YearOfManufactureModel;
use App\GenderModel;
use App\CityModel;
use App\CurrencyModel;
use App\DialPowerReserveModel;
use App\DialColorModel;
use App\BraceletMaterialModel;
use App\BraceletColorModel;
use App\BraceletClaspTypeModel;
use App\BraceletClaspMaterialModel;
use App\CaseMaterialModel;
use App\CaseMMModel;
use App\CaseWaterRDModel;
use App\CaseGlassTypeModel;
use App\FunctionChronographModel;
use App\FunctionTourbillionModel;
use App\FunctionGMTModel;
use App\FunctionAnnualCalenderModel;
use App\FunctionMinuteRepeaterModel;
use App\FunctionDoubleChronographModel;
use App\FunctionPanormaDateModel;
use App\FunctionJumpingHourModel;
use App\FunctionAlarmModel;
use App\FunctionYearModel;
use App\FunctionDayModel;
use App\WatchBezelModel;
use App\WatchFeaturesModel;
use App\WatchCommentModel;
use App\WatchImageModel;
use App\WatchFeaturesListingModel;
use App\WatchDetailsFeaturesModel;
use App\WatchAdditionalFeaturesModel;
use App\WatchSecurityImageModel;
use App\WatchDamageImageModel;
use App\InclusionModel;
use App\WatchDocumentImageModel;
use Auth;

class WatchDataListingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $details = WatchDataListingModel::get();
        
        return view('admin.watch.listing_datamsf.index',compact('details'));
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $country_data = CountryModel::orderBy('country_name','ASC')->get();
        $models = Watchbrandmodel::orderBy('watch_model_name','ASC')->get(); 
        $brands = WatchModel::orderBy('watch_brand_name','ASC')->get();
        $prices = PriceModel::get();
        $case_diameters = CaseDiameterModel::orderBy('case_diameter','ASC')->get();
        //newly created model
        $colors = WatchColourModel::get();
        $movements = MovementModel::get();
        $types = TypeModel::get();
        $year_of_manufactures = YearOfManufactureModel::orderBy('year_of_manufacture','ASC')->get();
        $genders = GenderModel::get();
        $currency_data = CurrencyModel::get();
        $inclusions_data = InclusionModel::get();
        
        $power_reserve = DialPowerReserveModel::get();
        $bezel_material = WatchBezelModel::get();
        $dial_color = DialColorModel::get();
        $bracelet_material = BraceletMaterialModel::get();
        $bracelet_color = BraceletColorModel::get();
        $type_of_clasp = BraceletClaspTypeModel::get();
        $clasp_material = BraceletClaspMaterialModel::get();
        $case_material = CaseMaterialModel::get();
        $case_mm = CaseMMModel::get();
        $wrd = CaseWaterRDModel::get();
        $glass_type = CaseGlassTypeModel::get();
        $chronograph = FunctionChronographModel::get();
        $tourbillion = FunctionTourbillionModel::get();
        $gmt = FunctionGMTModel::get();
        $annual_calender = FunctionAnnualCalenderModel::get();
        $minute_repeater = FunctionMinuteRepeaterModel::get();
        $double_chronograph = FunctionDoubleChronographModel::get();
        $panorma_date = FunctionPanormaDateModel::get();
        $jumping_hour = FunctionJumpingHourModel::get();
        $alarm = FunctionAlarmModel::get();
        $year = FunctionYearModel::get();
        $day = FunctionDayModel::get();
        $agents = AgentModel::get();
        $timezone = Timezone::Orderby('offset')->get();
        
        $feature_listing = WatchFeaturesListingModel::get();
        
        $html ='';
        foreach ($feature_listing as $values){
            $id = $values->id;
            $propertyName = $values->property_name;
            
            $html .= '<div class="form-group">
                    <tr>
                   <td><label style= margin-right:10px!important; ><Strong>'.$propertyName.':</Strong></label></td>
                    <td><input  style= margin-right:5px!important; type="radio" id="'.$propertyName.'" name="'.$id.'" value="1"><label style= margin-right:5px!important; for="yes"> Yes </label></td>
                    <td><input  style= margin-right:5px!important; type="radio" id="'.$propertyName.'" name="'.$id.'" value="0" checked><label style= margin-right:5px!important; for="no"> No</label></td>
                    </tr>
                      </div>';
            
        }
        
        return view('admin.watch.listing_datamsf.create',
            compact('timezone','html','agents','country_data','models','brands', 'prices', 'case_diameters', 'colors','movements', 'types',
                'year_of_manufactures', 'genders', 'currency_data','power_reserve', 'bezel_material' ,'dial_color', 'bracelet_material', 'bracelet_color','type_of_clasp', 'clasp_material', 'case_material',
                'case_mm', 'wrd', 'glass_type','chronograph','tourbillion', 'gmt', 'annual_calender', 'minute_repeater', 'double_chronograph', 'panorma_date',
                'jumping_hour', 'alarm', 'year', 'day', 'inclusions_data'));
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $userId = Auth::id();
        $data = $request->validate([
            
            
            'currency' => ['required'],
            'countryId' => ['required'],
            'cityId' => ['required'],
            'brand' => ['required'],
            'model' => ['required'],
            'gender' => ['required'],
            'inclusions' => ['required'],
            'referenceNo' => ['required'],
            'watchCondition' => ['required'],
            'comment' => ['required'],
            'photos.*' => 'image|mimes:jpeg,png,jpg,gif,svg'
        ]);
        
        $converted_amount = NULL;
        
        if($request->input('watch_price') != ''){
            $currency_code = CurrencyModel::find($request->input('currency'));
            
            $converted_amount = currency()->convert(floatval($request->input('watch_price')), $currency_code->currency_code, "USD",false);
        }
        
        $agent = AgentModel::find($request->input('agent'));
        $dealer_id = $agent->agency_id;
        
        $createWatch = new WatchDataListingModel();
        if($userId){
        $createWatch->createdID = $userId;
        }
        $createWatch->ad_title = $request->input('adTitle');
        $createWatch->currency_id = $request->input('currency');
        if($request->input('stateId') != '')
        $createWatch->state_id = $request->input('stateId');
        $createWatch->country_id = $request->input('countryId');
        $createWatch->city_id = $request->input('cityId');
        $createWatch->brand_id = $request->input('brand');
        $createWatch->price_on_request = $request->input('price_on_request');
        $createWatch->model_id = $request->input('model');
        $createWatch->watch_price = $converted_amount;
        if($request->input('price') != '')
        $createWatch->price_id = $request->input('price');
        if($request->input('case_diameter') != '')
        $createWatch->case_diameter_id = $request->input('case_diameter');
        $createWatch->movement_id = $request->input('movement');
        $createWatch->type_id = $request->input('type');
        $createWatch->year_of_manufacture_id = $request->input('yom');
        $createWatch->gender_id = $request->input('gender');
        $createWatch->inclusions  = $request->input('inclusions');
        $createWatch->reference_no = $request->input('referenceNo');
        $createWatch->watch_condition = $request->input('watchCondition');
        $createWatch->status = $request->input('status');
        $createWatch->brand_name = WatchModel::find($request->input('brand'))->watch_brand_name;
        $createWatch->model_name = Watchbrandmodel::find($request->input('model'))->watch_model_name;
        $createWatch->agent_id = $request->input('agent');
        $createWatch->agencies_id = $dealer_id;
        $createWatch->timezone_id = $request->input('timezone');
        
        $createWatch->save();
        
        $listing_id = $createWatch->id;
        
        $createFeature = new WatchFeaturesModel();
        
        $createFeature->listing_id = $listing_id;
        $createFeature->bezel_material_id = $request->input('bezelMaterial');
        $createFeature->power_reserve_id = $request->input('powerReserve');
        $createFeature->dial_color_id = $request->input('dialColor');
        $createFeature->bracelet_material_id = $request->input('braceletMaterial');
        $createFeature->bracelet_color_id = $request->input('braceletColor');
        $createFeature->type_of_clasp_id = $request->input('typeOfClasp');
        $createFeature->clasp_material_id = $request->input('claspMaterial');
        $createFeature->case_material_id = $request->input('caseMaterial');
        $createFeature->case_mm_id = $request->input('caseMM');
        $createFeature->water_resisitance_depth_id = $request->input('wrd');
        $createFeature->glass_type_id = $request->input('glassType');
        
        /* if($request->input('chronograph') !='')
        $createFeature->chronograph_id = $request->input('chronograph');
        if($request->input('tourbillion') !='')
        $createFeature->tourbillion_id = $request->input('tourbillion');
        if($request->input('gmt') !='')
        $createFeature->gmt_id = $request->input('gmt');
        if($request->input('annualCalender') !='')
        $createFeature->annual_calender_id = $request->input('annualCalender');
        if($request->input('minuteRepeater') !='')
        $createFeature->minute_repeater_id = $request->input('minuteRepeater');
        if($request->input('doubleChronograph') !='')
        $createFeature->double_chronograph_id = $request->input('doubleChronograph');
        if($request->input('panormaDate') !='')
        $createFeature->panorma_date_id = $request->input('panormaDate');
        if($request->input('jumpingHour') !='')
        $createFeature->jumping_hour_id = $request->input('jumpingHour');
        if($request->input('alarm') !='')
        $createFeature->alarm_id = $request->input('alarm');
        if($request->input('year') !='')
        $createFeature->year_id = $request->input('year');
        if($request->input('day') !='')
        $createFeature->day_id = $request->input('day'); */

        $createFeature->dial_color = DialColorModel::find($request->input('dialColor'))->dial_color;
        $createFeature->bracelet_material = BraceletMaterialModel::find($request->input('braceletMaterial'))->bracelet_material;
        
        $createFeature->save();
        
        if($request->input('additional_feature') ?? ''){
            
            foreach ($request->input('additional_feature') as $features){
                
                if($features != null){
                    $uploadFeature = new WatchAdditionalFeaturesModel();
                    
                    $uploadFeature->feature = $features;
                    $uploadFeature->listing_id = $listing_id;
                    $uploadFeature->save();
                }
            }
        }
        
        foreach ($request->request as $key => $value){
            if($key != '_token' && $key != 'listing_id' && gettype($key) != 'string'){
                $createDetailFeature = new WatchDetailsFeaturesModel();
                $createDetailFeature->listing_id = $listing_id;
                $createDetailFeature->feature_id = $key;
                $createDetailFeature->feature_available = $value;
                $createDetailFeature->save();
            }
            
        }
        
        if($request->comment ?? ''){
            $createComment = new WatchCommentModel();
            $createComment->comment = $request->input('comment');
            $createComment->listing_id = $listing_id;
            $createComment->save();
        }
        if($request->file('photos') ?? ''){
            
            foreach ($request->file('photos') as $image){
                $uploadImage = new WatchImageModel();
                
                $extension = $image->getClientOriginalExtension();
                Storage::disk('public')->put($image->getFilename().'.'.$extension,  File::get($image));
                
                $uploadImage->images = $image->getClientMimeType();
                $uploadImage->filename = $image->getFilename().'.'.$extension;
                $uploadImage->listing_id = $listing_id;
                $uploadImage->save();
            }
        }
        
        return redirect('/admin/createwatchdata')->with('success_msg', 'Watch Created successfully!');
    }
    
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $watch = WatchDataListingModel::find($id);

        $currency_data = CurrencyModel::get();
        $country_data = CountryModel::get();
        $state_data = StateModel::get();
        $city_data = CityModel::get();
        $models = Watchbrandmodel::get();
        $brands = WatchModel::get();
        $prices = PriceModel::get();
        $case_diameters = CaseDiameterModel::get();
        //newly created model
        $colors = WatchColourModel::get();
        $movements = MovementModel::get();
        $types = TypeModel::get();
        $year_of_manufactures = YearOfManufactureModel::get();
        $genders = GenderModel::get();
        $agents = AgentModel::get();
        $timezone = Timezone::Orderby('offset')->get();
        $inclusions_data = InclusionModel::get();
        
        $currencySelected = CurrencyModel::find($watch->currency_id);
        $stateSelected = StateModel::find($watch->state_id);
        $countrySelected = CountryModel::find($watch->country_id);
        $citySelected = CityModel::find($watch->city_id);
        $modelSelected = Watchbrandmodel::find($watch->model_id);
        $brandSelected = Watchmodel::find($watch->brand_id);
        $priceSelected = Pricemodel::find($watch->price_id);
        $caseDiameterSelected = CaseDiametermodel::find($watch->case_diameter_id);
        $colorSelected = WatchColourmodel::find($watch->color_id);
        $movementSelected = Movementmodel::find($watch->movement_id);
        $typeSelected = Typemodel::find($watch->type_id);
        $year_of_manufactureSelected = YearOfManufactureModel::find($watch->year_of_manufacture_id);
        $genderSelected = Gendermodel::find($watch->gender_id);
        $agentSelected = AgentModel::find($watch->agent_id);
        $timezoneSelected = Timezone::find($watch->timezone_id);
        $inclusionSelected = InclusionModel::find($watch->inclusions);
        
        
        $feature_data =WatchFeaturesModel::where('listing_id',$id)->first();
        
        $power_reserve = DialPowerReserveModel::get();
        $bezel_material = WatchBezelModel::get();
        $dial_color = DialColorModel::get();
        $bracelet_material = BraceletMaterialModel::get();
        $bracelet_color = BraceletColorModel::get();
        $type_of_clasp = BraceletClaspTypeModel::get();
        $clasp_material = BraceletClaspMaterialModel::get();
        $case_material = CaseMaterialModel::get();
        $case_mm = CaseMMModel::get();
        $wrd = CaseWaterRDModel::get();
        $glass_type = CaseGlassTypeModel::get();
        $chronograph = FunctionChronographModel::get();
        $tourbillion = FunctionTourbillionModel::get();
        $gmt = FunctionGMTModel::get();
        $annual_calender = FunctionAnnualCalenderModel::get();
        $minute_repeater = FunctionMinuteRepeaterModel::get();
        $double_chronograph = FunctionDoubleChronographModel::get();
        $panorma_date = FunctionPanormaDateModel::get();
        $jumping_hour = FunctionJumpingHourModel::get();
        $alarm = FunctionAlarmModel::get();
        $year = FunctionYearModel::get();
        $day = FunctionDayModel::get();
        
        
        $powerReserveSelected = DialPowerReserveModel::find($feature_data->power_reserve_id);
        $bezelMaterialSelected = WatchBezelModel::find($feature_data->bezel_material_id);
        $dialColorSelected = DialColorModel::find($feature_data->dial_color_id);
        $braceletMaterialSelected = BraceletMaterialModel::find($feature_data->bracelet_material_id);
        $braceletColorSelected = BraceletColorModel::find($feature_data->bracelet_color_id);
        $typeOfClaspSelected = BraceletClaspTypeModel::find($feature_data->type_of_clasp_id);
        $claspMaterialSelected = BraceletClaspMaterialModel::find($feature_data->clasp_material_id);
        $caseMaterialSelected = CaseMaterialModel::find($feature_data->case_material_id);
        $caseMMSelected = CaseMMModel::find($feature_data->case_mm_id);
        $wrdSelected = CaseWaterRDModel::find($feature_data->water_resisitance_depth_id);
        $glassTypeSelected = CaseGlassTypeModel::find($feature_data->glass_type_id);
       
        $chronographSelected = FunctionChronographModel::find($feature_data->chronograph_id);
        $tourbillionSelected = FunctionTourbillionModel::find($feature_data->tourbillion_id);
        $gmtSelected = FunctionGMTModel::find($feature_data->gmt_id);
        $annualCalenderSelected = FunctionAnnualCalenderModel::find($feature_data->annual_calender_id);
        $minuteRepeaterSelected = FunctionMinuteRepeaterModel::find($feature_data->minute_repeater_id);
        $doubleChronographSelected = FunctionDoubleChronographModel::find($feature_data->double_chronograph_id);
        $panormaDateSelected = FunctionPanormaDateModel::find($feature_data->panorma_date_id);
        $jumpingHourSelected = FunctionJumpingHourModel::find($feature_data->jumping_hour_id);
        $alarmSelected = FunctionAlarmModel::find($feature_data->alarm_id);
        $yearSelected = FunctionYearModel::find($feature_data->year_id);
        $daySelected = FunctionDayModel::find($feature_data->day_id);
        
        $currency_code = $currencySelected->currency_code;
        
        $converted_amount = currency()->convert(floatval($watch->watch_price), "USD", $currency_code ,false);
        
        $watch->watch_price = $converted_amount;
        
        $additionlFeatures = WatchAdditionalFeaturesModel::select('wa_watch_additional_features.id as id', 'listing_id' , 'feature')
        ->where('listing_id', '=', $id)
        ->get();
        
        $additionalHtml = '';
        foreach($additionlFeatures as $features){
            
            $additionalHtml .= '<tr class = "control-group-feature">
                    <td><label  >Additional Feature</label>
                    <input  class="form-control form-control-user " type="text" maxLength="100"  name="additional_feature[]" value="'.$features->feature.'"></td>
                    <td><button class="btn btn-primary  btn-danger btn-danger-feature" type="button">
				<i class="glyphicon glyphicon-remove"></i> Remove
			</button></td>
                    </tr>';
        }
        
        $feature_listing = WatchFeaturesListingModel::where('status', '=', '1')->get();
        
        $html = '';
        foreach ($feature_listing as $values){
            
            $feature_data = WatchDetailsFeaturesModel::where('feature_id', '=', $values->id)->where('listing_id', '=', $id)
            ->where('feature_available', '=', '1')->first();
            
            if($feature_data != null){
                
                $html .= '<div class="form-group">
                    <tr>
                    <td><label style= margin-right:10px!important; ><Strong>'.$values->property_name.':</Strong></label></td>
                    <td><input  style= margin-right:5px!important; type="radio" id="'.$values->propertyName.'" name="'.$values->feature_id.'" value="1" checked><label style= margin-right:5px!important; for="yes"> Yes </label></td>
                    <td><input  style= margin-right:5px!important; type="radio" id="'.$values->propertyName.'" name="'.$values->feature_id.'" value="0" ><label style= margin-right:5px!important; for="no"> No</label></td>
                    </tr>
                    </div>';
            }else{
                
                $html .= '<div class="form-group">
                    <tr>
                    <td><label style= margin-right:10px!important; ><Strong>'.$values->property_name.':</Strong></label></td>
                    <td><input  style= margin-right:5px!important; type="radio" id="'.$values->propertyName.'" name="'.$values->feature_id.'" value="1" ><label style= margin-right:5px!important; for="yes"> Yes </label></td>
                    <td><input  style= margin-right:5px!important; type="radio" id="'.$values->propertyName.'" name="'.$values->feature_id.'" value="0" checked><label style= margin-right:5px!important; for="no"> No</label></td>
                    </tr>
                    </div>';
            }
            
        }
        
        $comment_data = WatchCommentModel::where('listing_id',$id)->first();
        
        $images_data = WatchImageModel::where('listing_id', '=', $id)->get();
        
        $images_damages = WatchDamageImageModel::where('listing_id', '=', $id)->get();
        
        $security_images = WatchSecurityImageModel::where('listing_id', '=', $id)->get();

        $document = WatchDocumentImageModel::where('listing_id', '=', $id)->get();
        // echo "<pre>";
        // print_r($document);die;
        return view('admin.watch.listing_datamsf.edit',
            compact('timezoneSelected', 'timezone','additionalHtml','html','agents','agentSelected','watch','currency_data','country_data', 'state_data','city_data','models','brands', 'prices', 'case_diameters', 'colors','movements', 'types',
                     'year_of_manufactures', 'genders', 'stateSelected', 'countrySelected', 'citySelected', 'modelSelected', 
                     'brandSelected', 'priceSelected', 'caseDiameterSelected', 'colorSelected', 'movementSelected', 'typeSelected',
                'year_of_manufactureSelected', 'genderSelected', 'currencySelected', 'comment_data', 'images_data','bezel_material','feature_data','power_reserve', 'dial_color', 'bracelet_material', 'bracelet_color','type_of_clasp', 'clasp_material', 'case_material',
                'case_mm', 'wrd', 'glass_type','chronograph','tourbillion', 'gmt', 'annual_calender', 'minute_repeater', 'double_chronograph', 'panorma_date',
                'jumping_hour', 'alarm', 'year', 'day', 'powerReserveSelected', 'dialColorSelected', 'braceletMaterialSelected','braceletColorSelected','typeOfClaspSelected',
                'claspMaterialSelected','caseMaterialSelected', 'caseMMSelected', 'wrdSelected', 'glassTypeSelected','chronographSelected','tourbillionSelected','gmtSelected'
                ,'annualCalenderSelected','minuteRepeaterSelected', 'doubleChronographSelected', 'panormaDateSelected','jumpingHourSelected', 'alarmSelected','yearSelected',
                'daySelected','bezelMaterialSelected', 'security_images', 'images_damages', 'inclusions_data', 'inclusionSelected','document'));
    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->validate([
            
            
            'currency' => ['required'],
            'countryId' => ['required'],
            'cityId' => ['required'],
            'brand' => ['required'],
            'model' => ['required'],
            'gender' => ['required'],
            'inclusions' => ['required'],
            'referenceNo' => ['required'],
            'watchCondition' => ['required'],
            'comment' => ['required'],
            'listingApproval' => ['required'],
            'photos.*' => 'image|mimes:jpeg,png,jpg,gif,svg'
        ]);
        
        $agent = AgentModel::find($request->input('agent'));
        $dealer_id = $agent->agency_id;
        
        $watch_price = NULL;
        if($request->input('watch_price') != ''){
            
            $currency_code = CurrencyModel::find($request->input('currency'));
            
            $watch_price = currency()->convert(floatval($request->input('watch_price')), $currency_code->currency_code, "USD",false);
            //$priceSqft = $request->input('priceSqft');
        }
        
        $updateWatch = WatchDataListingModel::find($id);
        
        $updateWatch->ad_title = $request->input('adTitle');
        $updateWatch->currency_id = $request->input('currency');
        if($request->input('stateId') != '')
        $updateWatch->state_id = $request->input('stateId');
        $updateWatch->country_id = $request->input('countryId');
        $updateWatch->city_id = $request->input('cityId');
        $updateWatch->brand_id = $request->input('brand');
        if($request->input('price_on_request') != '')
        $updateWatch->price_on_request = $request->input('price_on_request');
        $updateWatch->watch_price = $watch_price;
        $updateWatch->model_id = $request->input('model');
        if($request->input('price') != '')
        $updateWatch->price_id = $request->input('price');
        $updateWatch->case_diameter_id = $request->input('case_diameter');
        if($request->input('movement') != '')
        $updateWatch->movement_id = $request->input('movement');
        if($request->input('type') != '')
        $updateWatch->type_id = $request->input('type');
        if($request->input('yom') != '')
        $updateWatch->year_of_manufacture_id = $request->input('yom');
        $updateWatch->gender_id = $request->input('gender');
        $updateWatch->inclusions  = $request->input('inclusions');
        $updateWatch->reference_no = $request->input('referenceNo');
        $updateWatch->watch_condition = $request->input('watchCondition');
        $updateWatch->status = $request->input('status');
        $updateWatch->agent_id = $request->input('agent');
        $updateWatch->agencies_id = $dealer_id;
        $updateWatch->timezone_id = $request->input('timezone');
        $updateWatch->brand_name = WatchModel::find($request->input('brand'))->watch_brand_name;
        $updateWatch->model_name = Watchbrandmodel::find($request->input('model'))->watch_model_name;
        $updateWatch->approval_status = $request->input('listingApproval');
        
        
        $updateWatch->save();
        
        $createFeature = WatchFeaturesModel::where('listing_id',$id)->first();
        
        $createFeature->listing_id = $id;
        $createFeature->bezel_material_id = $request->input('bezelMaterial');
        $createFeature->power_reserve_id = $request->input('powerReserve');
        $createFeature->dial_color_id = $request->input('dialColor');
        $createFeature->bracelet_material_id = $request->input('braceletMaterial');
        $createFeature->bracelet_color_id = $request->input('braceletColor');
        $createFeature->type_of_clasp_id = $request->input('typeOfClasp');
        $createFeature->clasp_material_id = $request->input('claspMaterial');
        $createFeature->case_material_id = $request->input('caseMaterial');
        $createFeature->case_mm_id = $request->input('caseMM');
        $createFeature->water_resisitance_depth_id = $request->input('wrd');
        $createFeature->glass_type_id = $request->input('glassType');
        $createFeature->chronograph_id = $request->input('chronograph');
        $createFeature->tourbillion_id = $request->input('tourbillion');
        $createFeature->gmt_id = $request->input('gmt');
        $createFeature->annual_calender_id = $request->input('annualCalender');
        $createFeature->minute_repeater_id = $request->input('minuteRepeater');
        $createFeature->double_chronograph_id = $request->input('doubleChronograph');
        $createFeature->panorma_date_id = $request->input('panormaDate');
        $createFeature->jumping_hour_id = $request->input('jumpingHour');
        $createFeature->alarm_id = $request->input('alarm');
        $createFeature->year_id = $request->input('year');
        $createFeature->day_id = $request->input('day');
        $createFeature->dial_color = DialColorModel::find($request->input('dialColor'))->dial_color;
        $createFeature->bracelet_material = BraceletMaterialModel::find($request->input('braceletMaterial'))->bracelet_material;
        
        $createFeature->save();
        
        $deleteRecords = WatchAdditionalFeaturesModel::where('listing_id', '=', $id)->delete();
        
        if($request->input('additional_feature') ?? ''){
            
            foreach ($request->input('additional_feature') as $features){
                
                if($features != null){
                    $uploadFeature = new WatchAdditionalFeaturesModel();
                    
                    $uploadFeature->feature = $features;
                    $uploadFeature->listing_id = $id;
                    $uploadFeature->save();
                }
            }
        }
        
        $deleteRecords = WatchDetailsFeaturesModel::where('listing_id', '=', $id)->delete();
        
        foreach ($request->request as $key => $value){
            
            if($key != '_token' && $key != 'listing_id' && gettype($key) != 'string'){
                $createFeature = new WatchDetailsFeaturesModel();
                $createFeature->listing_id = $id;
                $createFeature->feature_id = $key;
                $createFeature->feature_available = $value;
                $createFeature->save();
            }
            
        }
        
        if($request->comment ?? ''){
            $updateComment = WatchCommentModel::where('listing_id',$id)->first();
            $updateComment->comment = $request->comment;
            $updateComment->listing_id = $id;
            $updateComment->save();
        }
        if($request->file('photos') ?? ''){
            foreach ($request->file('photos') as $image){
                $uploadImage = new WatchImageModel();
                
                $extension = $image->getClientOriginalExtension();
                Storage::disk('public')->put($image->getFilename().'.'.$extension,  File::get($image));
                
                $uploadImage->images = $image->getClientMimeType();
                $uploadImage->filename = $image->getFilename().'.'.$extension;
                $uploadImage->listing_id = $id;
                $uploadImage->save();
            }
        }
        
        return redirect('/admin/createwatchdata')->with('success_msg', 'Watch Updated successfully!');
    }
    
    public function getModelByBrand(Request $request){
        
        $brandId = $request->input('brandId');
        $model = WatchBrandModel::select('id', 'watch_model_name')->where('watch_brand_id', $brandId)->get();
        return $model;
    }
    
    public function updateLatestOffer($id){
        
        $watch = WatchDataListingModel::find($id);
        
        if($watch->latestOffer == '0'){
            
            $watch->latestOffer = '1';
            
        }else{
            $watch->latestOffer = '0';
        }
        
        $watch->save();
        
        return redirect('/admin/createwatchdata')->with('success_msg', 'Latest Offer Updated successfully!');
    }
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deleteRecords = WatchDataListingModel::find($id)->delete();
        return redirect('/admin/createwatchdata')->with('success_msg', 'Watch Deleted successfully!');
    }
}

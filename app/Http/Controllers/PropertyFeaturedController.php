<?php

namespace App\Http\Controllers;

use App\PropertyDataModel;
use Illuminate\Http\Request;

class PropertyFeaturedController extends Controller
{
    public function showFeaturedProperties(Request $request){
        
        $properties = PropertyDataModel::select('re_property_details.id', 'ad_title', 'land_size', 're_property_details.state_id', 're_property_details.city_id', 'city_name', 'state_name')->join('tbl_city', 'tbl_city.id', '=', 're_property_details.city_id')
        ->join('tbl_state', 'tbl_state.id', '=', 're_property_details.state_id')
        ->where('featured', '=','1')
        ->get();
        
        return view('admin.realestate.featured.featured')->with(array(
            'properties' => $properties
        ));
        
    }
    
    public function editFeatured($id){
        
        $featured = PropertyDataModel::find($id);
        
        return view('admin.realestate.featured.editfeatured', compact('featured'));
    }
    
    public function updateFeatured(Request $request, $id){
        
        $data = $request->validate([
            'featured' => ['required']
        ]);
        
        $updateFeatured = PropertyDataModel::find($id);
        
        $updateFeatured->featured = $request->input('featured');
        
        $updateFeatured->save();
        
        return redirect('/admin/featuredproperty')->with('success_msg', 'Property Feature Updated successfully!');
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use App\WatchModel;
use App\Watchbrandmodel;
use Session;
use App\WatchModelImage;


class WatchadminController extends Controller
{
    
    // this function redirects to dashboard page from where all watch data can be entered
    public function home() {
        return view('admin.watch.dashboard');
    }
    // this function displayes the list of watch brands which are created in system
    public function displaybrands(){
       $brands = WatchModel::select('id','watch_brand_name','status','show_under_brand')
        ->get();
        $images = WatchModelImage::get();
        return view('admin.watch.displaybrands')->with(array('brands'=>$brands, 'images'=>$images));
    }
    // this function takes us to the page from where brands can be created
    public function createbrands(){

        return view('admin.watch.create_watch_brand_msf');
    }
    // this function updates the watch brands in the system
    public function updatebrands($id){
      $brands = WatchModel::select('id','watch_brand_name','status', 'show_under_brand')->where('id','=',$id)
        ->get();
        $images_data = WatchModelImage::where('listing_id', '=', $id)->get();
        return view('admin.watch.updatebrandmsf')->with(array('brands'=>$brands, 'images_data'=>$images_data));
    }

    // this function saves the brands in the system
    public function savebrands(Request $request){
        $brandname = $request->input('brandname');
        $status = $request->input('status');
        $showunderbrand = $request->input('show_under_brand');

        $data = $request->validate([
                'brandname'   => ['required', 'unique:wa_brands,watch_brand_name'],
                'status' => ['required']
                
        ]);
        if($brandname == ''){
            return back()->withErrors(['error' => ['Any of the field cannot be left blanck']]);
        } else {  

            $data = new WatchModel;
            $data->watch_brand_name =  $brandname;
            $data->show_under_brand = $showunderbrand;
            $data->status =  $status;
            $data->save();
            
            $listing_id = $data->id;
            
            if($request->file('photos') ?? ''){
                foreach ($request->file('photos') as $image){
                    $uploadImage = new WatchModelImage();
                    
                    $extension = $image->getClientOriginalExtension();
                    Storage::disk('public')->put($image->getFilename().'.'.$extension,  File::get($image));
                    
                    $uploadImage->images = $image->getClientMimeType();
                    $uploadImage->filename = $image->getFilename().'.'.$extension;
                    $uploadImage->listing_id = $listing_id;
                    $uploadImage->save();
                }
            }
            
            $brands = WatchModel::select('id','watch_brand_name','status')
            ->get();
          return redirect('admin/watch/displaybrands')->with('success_msg','Watch brand created Successfully..!!');
        }
    }
    // this function edists the watch brands in the system
    public function editbrands(Request $request){
        $brandname = $request->input('brandname');
        $id=$request->input('watchid');
        $brandstatus=$request->input('brandstatus');
        $showunderbrand = $request->input('show_under_brand');
        $data = $request->validate([
                'brandname'   => ['required','unique:wa_brands,watch_brand_name,' . $id],
        ]);
        $data = WatchModel::find($id);
        $data->watch_brand_name = $brandname;
        $data->status = $brandstatus;
        $data->show_under_brand = $showunderbrand;
        $data->save();
        $brands = WatchModel::select('id','watch_brand_name','status')
        ->get();
        
        if($request->file('photos') ?? ''){
            foreach ($request->file('photos') as $image){
                $uploadImage = new WatchModelImage();
                
                $extension = $image->getClientOriginalExtension();
                Storage::disk('public')->put($image->getFilename().'.'.$extension,  File::get($image));
                
                $uploadImage->images = $image->getClientMimeType();
                $uploadImage->filename = $image->getFilename().'.'.$extension;
                $uploadImage->listing_id = $id;
                $uploadImage->save();
            }
        }
         
       return redirect('admin/watch/displaybrands')->with('success_msg','Watch brand updated Successfully..!!');

    }
    // this function deletes the watch brands from the system

    public function deletebrands(Request $request){
        $id=$request->input('id');
        $deletewatch = WatchModel::find($id);
        $deletewatch->delete(); 
        return response()->json([
                    'success_msg'=>'Watch brand deleted Successfully..!!']); 
    }

    // this function displayes the list of models created for the watch brands
    public function displaybrandsmodel(){
        
        $brands = Watchbrandmodel::select('wa_model.id','watch_model_name','wa_model.status','watch_brand_name')
        ->join('wa_brands','wa_brands.id','=','wa_model.watch_brand_id')
        ->get();
       
       return view('admin.watch.displaybrandsmodel')->with(array('brands'=>$brands));
    }

    // this function takes us to the page from where models of the brands can be created
    public function createbrandsmodel(){
        $brands = WatchModel::select('id','watch_brand_name','status')->where('status','=','1')
        ->get();
        return view('admin.watch.createbrandsmodel')->with(array('brands'=>$brands));

    }

    // this function saves the watch brands model in the system
    public function savebrandsmodel(Request $request){
        $brandid = $request->brandid;
        $brandmodelname = $request->brandmodelname;
        $status = $request->status;

         $data = $request->validate([
                'brandid'   => ['required'],
                'brandmodelname' => ['required','unique:wa_model,watch_model_name'],
                'status' => ['required']
                
        ]);

        if($brandid == '' || $brandmodelname == ''){
            return back()->withErrors(['error' => ['Any of the field cannot be left blanck']]);
       }else{
            $data = new Watchbrandmodel;
            $data->watch_brand_id =  $brandid;
            $data->watch_model_name = $brandmodelname;
            $data->status = $status;
            $data->save();
            return redirect('admin/watch/displaybrandsmodel')->with('success_msg','Watch brand Model created Successfully..!!');
       }
    }

    // this function is created for displaying the update page of brandsmodel

    public function updatebrandsmodel($id){
        $brandsmodel = Watchbrandmodel::select('wa_model.id','watch_model_name','wa_model.status','watch_brand_name')
        ->join('wa_brands','wa_brands.id','=','wa_model.watch_brand_id')
        ->where('wa_model.id','=',$id)
        ->get();
        

        return view('admin.watch.updatebrandsmodel')->with(array('brandsmodel'=>$brandsmodel));
    }

    // this function is to edit the model of the brands

    public function editbrandsmodel(Request $request){
        $brandmodelname = $request->brandmodelname;
        $modelid=$request->modelid;
        $brandstatus=$request->input('brandstatus');
        $data = $request->validate([
                'brandmodelname' => ['required','unique:wa_model,watch_model_name, '. $modelid],
                
        ]);
        if($brandmodelname == '' || $modelid == ''){
            return back()->withErrors(['error' => ['Any of the field cannot be left blanck']]);
        }else{
            $data = Watchbrandmodel::find($modelid);
            $data->watch_model_name = $brandmodelname;
            $data->status = $brandstatus;

            $data->save();
          
            return redirect('admin/watch/displaybrandsmodel')->with('success_msg','Watch brand Model created Successfully..!!');
       }
    }

    // this function is to delete brands models
    public function deletebrandsmodel(Request $request){
        $id=$request->input('id');
        $deletewatch = Watchbrandmodel::find($id);
        $deletewatch->delete(); 
        return response()->json([
                    'success_msg'=>'Watch brand deleted Successfully..!!']); 
    }
    
}

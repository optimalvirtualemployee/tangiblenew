<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Bracelet;

class BraceletController extends Controller
{
     function __construct()
    {
         $this->middleware('permission:watch-list|watch-create|watch-edit|watch-delete', ['only' => ['index','store']]);
         $this->middleware('permission:watch-create', ['only' => ['create','store']]);
         $this->middleware('permission:watch-edit', ['only' => ['edit','update']]);
         $this->middleware('permission:watch-delete', ['only' => ['destroy']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $bracelet = Bracelet::get();
        return view('admin.watch.bracelet.index',compact('bracelet'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.watch.bracelet.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
        $validator = $this->validate($request, [
            "bracelet_material" => 'required|unique:wa_factory_features_bracelet',
            "bracelet_color"  => "required",
            'type_of_clasp' => "required",
            'clasp_material' => "required"
        ]);

          $bracelet = Bracelet::create($validator);
          $bracelet->save();
       
      return redirect('/admin/bracelet')->with('success_msg','Bracelet Created successfully!');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $bracelet_data = Bracelet::find($id);
        return view('admin.watch.bracelet.edit', compact('bracelet_data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $deleteRecords = Bracelet::find($id)->delete();
        if($deleteRecords){
            $validator = $this->validate($request, [
            "bracelet_material" => 'required|unique:wa_factory_features_bracelet',
            "bracelet_color"  => "required",
            'type_of_clasp' => "required",
            'clasp_material' => "required"
        ]);

          $bracelet = Bracelet::create($validator);
          $bracelet->save();
        }  
       
      return redirect('/admin/bracelet')->with('success_msg','Bracelet Updated successfully!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deleteRecords = Bracelet::find($id)->delete();
        return redirect('/admin/bracelet')->with('success_msg','Bracelet deleted successfully!');
    }
}

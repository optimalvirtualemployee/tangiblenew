<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\NoOfBedroomsModel;

class NoOfBedroomsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $no_of_bedrooms = NoOfBedroomsModel::get();
        return view('admin.realestate.no_of_bedrooms.index',compact('no_of_bedrooms'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
         return view('admin.realestate.no_of_bedrooms.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validate = $this->validate($request, [
            'number_of_bedrooms' => 'required|numeric|unique:re_beds',
            'status' => 'required'
        ]);

        $no_of_bedrooms = NoOfBedroomsModel::create($validate);
        $no_of_bedrooms->save();
        return redirect('/admin/no_of_bedrooms')->with('success_msg','No. of Bedrooms Created successfully!');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $no_of_bedrooms_data = NoOfBedroomsModel::find($id);
        return view('admin.realestate.no_of_bedrooms.edit', compact('no_of_bedrooms_data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validate = $this->validate($request, [
            'number_of_bedrooms' => 'required|numeric'
        ]);

        $no_of_bedroomsUpdate = NoOfBedroomsModel::find($id);

        if ($no_of_bedroomsUpdate->number_of_bedrooms == $validate['number_of_bedrooms']) {
            $no_of_bedroomsUpdate->status = $request->input('status');
        } else {
            $validate = $this->validate($request, ['number_of_bedrooms' => 'unique:re_beds']);
            $no_of_bedroomsUpdate->number_of_bedrooms = $validate['number_of_bedrooms'];
            $no_of_bedroomsUpdate->status = $request->input('status');
        }

        $no_of_bedroomsUpdate->save();

        return redirect('/admin/no_of_bedrooms')->with('success_msg','No. of Bedrooms Updated successfully!'); 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deleteRecords = NoOfBedroomsModel::find($id)->delete();
        return redirect('/admin/no_of_bedrooms')->with('success_msg','No. of Bedrooms deleted successfully!');
    }
}

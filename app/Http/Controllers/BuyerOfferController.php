<?php
namespace App\Http\Controllers;

use App\AgentModel;
use App\ContactusModel;
use App\CurrencyModel;
use App\MyMessageModel;
use App\ProductOrderModel;
use App\WatchImageModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\CityModel;
use App\CountryModel;
use App\StateModel;
use App\FinalOfferModel;
use App\OrderTrackingDetailModel;
use App\OrderDeliveryDetailModel;
use App\OrderStatusHierarchyModel;

class BuyerOfferController extends Controller
{
    
    public function checkout(Request $request)
    {
        if (Auth::user() == null) {
            return redirect('/');
        }
        
        
        
        $id = Auth::user()->id;
        
        $currentStep = ProductOrderModel::select('order_status')->where('id', '=', $request->enquiryId)
        ->where('category', '=', $request->category)
        ->where('userID', '=', $id)
        ->first();
        
        $contactUs = ContactusModel::get()->first();
        $selectedCurrency = CurrencyModel::select('currency_code as code')->where('status', '=', '1')->first();
        
        currency()->setUserCurrency($selectedCurrency->code);
        
        if ($request->category == 'Watch') {
            $enquiry = MyMessageModel::select('submitpricerequest.submit_price as submitprice', 'submitpricerequest.id as submitpriceId', 'tbl_enquiry.agent_id', 'tbl_enquiry.id as id', 'productId', 'tbl_enquiry.created_at', 'ad_title', 'watch_price as value', 'tbl_country.filename as countryflag', 'tbl_agencies.company_name', DB::raw('CONCAT(tbl_agent.first_name, " ", tbl_agent.last_name) AS agent_name'), 'tbl_agencies.created_at as agencycreate', 'tbl_enquiry.productId as productId', 'tbl_user_details.user_mobile as userMobile', 'tbl_user_details.user_address as userAddress', 'tbl_user_details.user_postalCode as userPostalCode', 'tbl_user_details.country_id as userCountryId', 'users.email as userEmail', DB::raw('CONCAT(users.first_name, " ", users.last_name) AS user_name'), 'users.first_name as userFirstName', 'users.last_name as userLastName', 'tbl_user_details.city_id as userCityId', 'tbl_user_details.country_id as userCountryId', 'tbl_enquiry.userID as userID', 'tbl_final_offer.finalOfferValue as finalOfferValue', 'tbl_final_offer.shippingCost as shippingCost', 'tbl_final_offer.deliveryTime as deliveryTime', 'tbl_final_offer.offerValid as offerValid', 'tbl_final_offer.message as finalMessage', 'tbl_final_offer.created_at as requestSent', 'tbl_country.countrycode as countrycode', 'wa_watch_detail.agencies_id as agencyId', 'tbl_message_details.id as messageId', 'tbl_message_details.category as category', 'tbl_message_details.queryType as queryType')
            ->where('tbl_message_details.id', '=', $request->input('messageId'))
            /* ->where('tbl_message_details.queryType', '=', 'counterOffer') */
            ->where('tbl_message_details.category', '=', 'watch')
            ->join('submitpricerequest', 'submitpricerequest.id', 'tbl_message_details.message_id')
            ->leftJoin('tbl_final_offer', 'tbl_final_offer.messageId', 'tbl_message_details.id')
            ->join('tbl_enquiry', 'tbl_enquiry.id', 'tbl_message_details.message_id')
            ->join('tbl_user_details', 'tbl_user_details.user_id', 'tbl_enquiry.userID')
            ->join('users', 'users.id', 'tbl_enquiry.userID')
            ->join('wa_watch_detail', 'wa_watch_detail.id', 'tbl_enquiry.productId')
            ->join('tbl_country', 'tbl_country.id', 'wa_watch_detail.country_id')
            ->join('tbl_agent', 'tbl_agent.id', 'tbl_enquiry.agent_id')
            ->leftJoin('tbl_agencies', 'tbl_agencies.id', '=', 'tbl_agent.agency_id')
            ->first();
            
            $images = WatchImageModel::where('listing_id', $enquiry->productId)->first();
        }
        
        if ($enquiry->userCityId != null)
            $user_city = CityModel::find($enquiry->userCityId);
            
            if ($enquiry->userCountryId != null)
                $user_country = CountryModel::find($enquiry->userCountryId);
                
                if ($user_city != null)
                    $user_state = StateModel::find($user_city->state_id);
                    
                    $selectedCurrency = CurrencyModel::select('currency_code as code')->where('status', '=', '1')->first();
                    
                    $converted_shipping = currency()->convert(floatval($enquiry->shippingCost), "USD", $selectedCurrency->code, false);
                    
                    if ($currentStep->order_status == 'waiting for approval') {
                        
                        return view('tangiblehtml.initialOrderStatusBuyer', compact('currentStep', 'user_city', 'user_country', 'user_state', 'contactUs', 'enquiry', 'images', 'selectedCurrency', 'converted_shipping'));
                    }
                    
                    if ($currentStep->order_status == 'seller_approved') {
                        
                        return view('tangiblehtml.checkout-details-received-offer', compact('contactUs', 'enquiry', 'images', 'user_state', 'user_country', 'user_city', 'converted_shipping'));
                    }
                    if ($currentStep->order_status == 'waiting_for_payment') {
                        return view('tangiblehtml.checkout-payment', compact('user_city', 'user_country', 'user_state', 'contactUs', 'enquiry', 'images', 'selectedCurrency', 'converted_shipping'));
                    }
                    
                    if ($currentStep->order_status == 'payment done') {
                        return view('tangiblehtml.initialOrderStatusBuyer', compact('currentStep', 'contactUs', 'enquiry', 'images', 'user_state', 'user_country', 'user_city', 'converted_shipping'));
                    }
                    if ($currentStep->order_status == 'shipped') {
                        return view('tangiblehtml.order-delivery-status', compact('currentStep', 'contactUs', 'enquiry', 'images', 'user_state', 'user_country', 'user_city', 'converted_shipping'));
                    }
                    if ($currentStep->order_status == 'delivered') {
                        
                        $deliveryStatus = OrderDeliveryDetailModel::where('enquiry_id', '=', $request->enquiryId)->first();
                        
                        return view('tangiblehtml.final-delivery-status-buyer', compact('deliveryStatus', 'currentStep', 'contactUs', 'enquiry', 'images', 'user_state', 'user_country', 'user_city', 'converted_shipping'));
                    }
    }
    
    public function counteroffer(Request $request)
    {
        if (Auth::user() == null) {
            return redirect('/');
        }
        
        $id = Auth::user()->id;
        
        $contactUs = ContactusModel::get()->first();
        $selectedCurrency = CurrencyModel::select('currency_code as code')->where('status', '=', '1')->first();
        
        currency()->setUserCurrency($selectedCurrency->code);
        
        if ($request->queryType == 'counterOffer') {
            
            $enquiry = MyMessageModel::select('submitpricerequest.submit_price as submitprice', 'submitpricerequest.id as submitpriceId', 'tbl_enquiry.agent_id', 'tbl_enquiry.id as id', 'productId', 'tbl_enquiry.created_at', 'ad_title', 'watch_price as value', 'tbl_country.filename as countryflag', 'tbl_agencies.company_name', DB::raw('CONCAT(tbl_agent.first_name, " ", tbl_agent.last_name) AS agent_name'), 'tbl_agencies.created_at as agencycreate', 'tbl_enquiry.productId as productId', 'tbl_user_details.user_mobile as userMobile', 'tbl_user_details.user_address as userAddress', 'tbl_user_details.user_postalCode as userPostalCode', 'tbl_user_details.country_id as userCountryId', 'users.email as userEmail', DB::raw('CONCAT(users.first_name, " ", users.last_name) AS user_name'), 'users.first_name as userFirstName', 'users.last_name as userLastName', 'tbl_enquiry.userID as userID', 'tbl_final_offer.finalOfferValue as finalOfferValue', 'tbl_final_offer.shippingCost as shippingCost', 'tbl_final_offer.deliveryTime as deliveryTime', 'tbl_final_offer.offerValid as offerValid', 'tbl_final_offer.message as finalMessage', 'tbl_final_offer.created_at as requestSent', 'tbl_country.countrycode as countrycode', 'wa_watch_detail.agencies_id as agencyId')->where('tbl_message_details.id', '=', $request->input('messageId'))
            ->where('tbl_message_details.queryType', '=', 'counterOffer')
            ->where('tbl_message_details.category', '=', 'watch')
            ->join('submitpricerequest', 'submitpricerequest.id', 'tbl_message_details.message_id')
            ->leftJoin('tbl_final_offer', 'tbl_final_offer.messageId', 'tbl_message_details.id')
            ->join('tbl_enquiry', 'tbl_enquiry.id', 'tbl_message_details.message_id')
            ->join('tbl_user_details', 'tbl_user_details.user_id', 'tbl_enquiry.userID')
            ->join('users', 'users.id', 'tbl_enquiry.userID')
            ->join('wa_watch_detail', 'wa_watch_detail.id', 'tbl_enquiry.productId')
            ->join('tbl_country', 'tbl_country.id', 'wa_watch_detail.country_id')
            ->join('tbl_agent', 'tbl_agent.id', 'tbl_enquiry.agent_id')
            ->leftJoin('tbl_agencies', 'tbl_agencies.id', '=', 'tbl_agent.agency_id')
            ->first();
        } else if ($request->queryType == 'submitprice') {
            
            $enquiry = MyMessageModel::select('submitpricerequest.submit_price as finalOfferValue', 'submitpricerequest.id as submitpriceId', 'tbl_enquiry.agent_id', 'tbl_enquiry.id as id', 'productId', 'tbl_enquiry.created_at', 'ad_title', 'watch_price as value', 'tbl_country.filename as countryflag', 'tbl_agencies.company_name', DB::raw('CONCAT(tbl_agent.first_name, " ", tbl_agent.last_name) AS agent_name'), 'tbl_agencies.created_at as agencycreate', 'tbl_enquiry.productId as productId', 'tbl_user_details.user_mobile as userMobile', 'tbl_user_details.user_address as userAddress', 'tbl_user_details.user_postalCode as userPostalCode', 'tbl_user_details.country_id as userCountryId', 'users.email as userEmail', DB::raw('CONCAT(users.first_name, " ", users.last_name) AS user_name'), 'users.first_name as userFirstName', 'users.last_name as userLastName', 'tbl_enquiry.userID as userID', 'tbl_final_offer.shippingCost as shippingCost', 'tbl_final_offer.deliveryTime as deliveryTime', 'tbl_final_offer.offerValid as offerValid', 'tbl_final_offer.message as finalMessage', 'tbl_final_offer.created_at as requestSent', 'tbl_country.countrycode as countrycode', 'wa_watch_detail.agencies_id as agencyId')->where('tbl_message_details.message_id', '=', $request->input('enquiryId'))
            ->where('tbl_message_details.queryType', '=', 'submitprice')
            ->where('tbl_message_details.category', '=', 'watch')
            ->join('submitpricerequest', 'submitpricerequest.id', 'tbl_message_details.message_id')
            ->leftJoin('tbl_final_offer', 'tbl_final_offer.messageId', 'tbl_message_details.id')
            ->join('tbl_enquiry', 'tbl_enquiry.id', 'tbl_message_details.message_id')
            ->join('tbl_user_details', 'tbl_user_details.user_id', 'tbl_enquiry.userID')
            ->join('users', 'users.id', 'tbl_enquiry.userID')
            ->join('wa_watch_detail', 'wa_watch_detail.id', 'tbl_enquiry.productId')
            ->join('tbl_country', 'tbl_country.id', 'wa_watch_detail.country_id')
            ->join('tbl_agent', 'tbl_agent.id', 'tbl_enquiry.agent_id')
            ->leftJoin('tbl_agencies', 'tbl_agencies.id', '=', 'tbl_agent.agency_id')
            ->first();
        }
        
        $images = WatchImageModel::where('listing_id', $enquiry->productId)->first();
        
        $selectedCurrency = CurrencyModel::select('currency_code as code')->where('status', '=', '1')->first();
        
        $converted_shipping = currency()->convert(floatval($enquiry->shippingCost), "USD", $selectedCurrency->code, false);
        
        return view('tangiblehtml.buyer-counter-offer', compact('contactUs', 'enquiry', 'images', 'selectedCurrency', 'converted_shipping'));
    }
    
    public function buyerSubumitCounterOffer(Request $request)
    {
        $agent = AgentModel::find($request->agentId);
        
        $request->validate([
            
            'enquiryId' => [
                'required'
            ],
            'finalOfferValue' => [
                'required'
            ],
            'deliveryTime' => [
                'required'
            ],
            'offerValid' => [
                'required'
            ],
            'message' => [
                'required'
            ]
        ]);
        
        $converted_amount = NULL;
        
        $converted_shipping = NULL;
        
        if ($request->finalOfferValue != '') {
            $selectedCurrency = CurrencyModel::select('currency_code as code')->where('status', '=', '1')->first();
            
            $converted_amount = currency()->convert(floatval($request->finalOfferValue), $selectedCurrency->code, "USD", false);
        }
        
        if ($request->shippingCost != '') {
            $selectedCurrency = CurrencyModel::select('currency_code as code')->where('status', '=', '1')->first();
            
            $converted_shipping = currency()->convert(floatval($request->shippingCost), $selectedCurrency->code, "USD", false);
        }
        
        $seller_category = 'private_seller';
        if ($agent->agency_id != null)
            $seller_category = 'agent';
            
            $myMessage = new MyMessageModel();
            $myMessage->message = $request->message;
            $myMessage->message_id = $request->enquiryId;
            $myMessage->queryType = 'counterOffer';
            $myMessage->category = 'Watch';
            $myMessage->agent_id = $request->agentId;
            $myMessage->product_id = $request->productid;
            $myMessage->messageFrom = $request->userID;
            $myMessage->messageTo = $agent->userId;
            
            $myMessage->save();
            
            $finalOffer = new FinalOfferModel();
            
            $finalOffer->messageId = $myMessage->id;
            $finalOffer->finalOfferValue = $converted_amount;
            $finalOffer->shippingCost = $converted_shipping;
            $finalOffer->deliveryTime = $request->input('deliveryTime');
            $finalOffer->offerValid = $request->input('offerValid');
            $finalOffer->message = $request->input('message');
            $finalOffer->messageFrom = 'buyer';
            
            $finalOffer->save();
            
            $order = ProductOrderModel::find($request->input('enquiryId'));
            
            $order->order_status = 'waiting for approval';
            
            $order->update();
            
            OrderStatusHierarchyModel::where('enquiryId', '=', $request->enquiryId)->delete();
            
            $orderStatus = new OrderStatusHierarchyModel();
            
            $orderStatus->enquiryId = $request->enquiryId;
            $orderStatus->status = 'waiting for approval';
            
            $orderStatus->save();
            
            return redirect('/mymessages')->with('success_msg', 'Offer Sent successfully!');
    }
    
    public function buyerDeliveryStatus(Request $request)
    {
        $request->validate([
            
            'enquiryId' => [
                'required'
            ],
            'deliveryStatus' => [
                'required'
            ],
            'productStatus' => [
                'required'
            ],
            'comment' => [
                'required'
            ]
        ]);
        
        $deliveryStatus = new OrderDeliveryDetailModel();
        
        $deliveryStatus->enquiry_id = $request->enquiryId;
        $deliveryStatus->delivery_status = $request->deliveryStatus;
        $deliveryStatus->product_status = $request->productStatus;
        $deliveryStatus->comment = $request->comment;
        
        $deliveryStatus->save();
        
        if (Auth::user() == null) {
            return redirect('/');
        }
        
        $id = Auth::user()->id;
        
        $currentStep = ProductOrderModel::select('order_status')->where('id', '=', $request->enquiryId)
        ->where('category', '=', $request->category)
        ->where('userID', '=', $id)
        ->first();
        
        $contactUs = ContactusModel::get()->first();
        $selectedCurrency = CurrencyModel::select('currency_code as code')->where('status', '=', '1')->first();
        
        currency()->setUserCurrency($selectedCurrency->code);
        
        if ($request->category == 'Watch') {
            $enquiry = MyMessageModel::select('submitpricerequest.submit_price as submitprice', 'submitpricerequest.id as submitpriceId', 'tbl_enquiry.agent_id', 'tbl_enquiry.id as id', 'productId', 'tbl_enquiry.created_at', 'ad_title', 'watch_price as value', 'tbl_country.filename as countryflag', 'tbl_agencies.company_name', DB::raw('CONCAT(tbl_agent.first_name, " ", tbl_agent.last_name) AS agent_name'), 'tbl_agencies.created_at as agencycreate', 'tbl_enquiry.productId as productId', 'tbl_user_details.user_mobile as userMobile', 'tbl_user_details.user_address as userAddress', 'tbl_user_details.user_postalCode as userPostalCode', 'tbl_user_details.country_id as userCountryId', 'users.email as userEmail', DB::raw('CONCAT(users.first_name, " ", users.last_name) AS user_name'), 'users.first_name as userFirstName', 'users.last_name as userLastName', 'tbl_user_details.city_id as userCityId', 'tbl_user_details.country_id as userCountryId', 'tbl_enquiry.userID as userID', 'tbl_final_offer.finalOfferValue as finalOfferValue', 'tbl_final_offer.shippingCost as shippingCost', 'tbl_final_offer.deliveryTime as deliveryTime', 'tbl_final_offer.offerValid as offerValid', 'tbl_final_offer.message as finalMessage', 'tbl_final_offer.created_at as requestSent', 'tbl_country.countrycode as countrycode', 'wa_watch_detail.agencies_id as agencyId', 'tbl_message_details.id as messageId', 'tbl_message_details.category as category', 'tbl_message_details.queryType as queryType')->where('tbl_message_details.id', '=', $request->input('messageId'))
            /* ->where('tbl_message_details.queryType', '=', 'counterOffer') */
            ->where('tbl_message_details.category', '=', 'watch')
            ->join('submitpricerequest', 'submitpricerequest.id', 'tbl_message_details.message_id')
            ->leftJoin('tbl_final_offer', 'tbl_final_offer.messageId', 'tbl_message_details.id')
            ->join('tbl_enquiry', 'tbl_enquiry.id', 'tbl_message_details.message_id')
            ->join('tbl_user_details', 'tbl_user_details.user_id', 'tbl_enquiry.userID')
            ->join('users', 'users.id', 'tbl_enquiry.userID')
            ->join('wa_watch_detail', 'wa_watch_detail.id', 'tbl_enquiry.productId')
            ->join('tbl_country', 'tbl_country.id', 'wa_watch_detail.country_id')
            ->join('tbl_agent', 'tbl_agent.id', 'tbl_enquiry.agent_id')
            ->leftJoin('tbl_agencies', 'tbl_agencies.id', '=', 'tbl_agent.agency_id')
            ->first();
            
            $images = WatchImageModel::where('listing_id', $enquiry->productId)->first();
        }
        
        if ($enquiry->userCityId != null)
            $user_city = CityModel::find($enquiry->userCityId);
            
            if ($enquiry->userCountryId != null)
                $user_country = CountryModel::find($enquiry->userCountryId);
                
                if ($user_city != null)
                    $user_state = StateModel::find($user_city->state_id);
                    
                    $selectedCurrency = CurrencyModel::select('currency_code as code')->where('status', '=', '1')->first();
                    
                    $converted_shipping = currency()->convert(floatval($enquiry->shippingCost), "USD", $selectedCurrency->code, false);
                    
                    $order = ProductOrderModel::find($request->input('enquiryId'));
                    
                    $order->order_status = 'delivered';
                    
                    $order->update();
                    
                    $orderStatus = new OrderStatusHierarchyModel();
                    
                    $orderStatus->enquiryId = $request->enquiryId;
                    $orderStatus->status = 'delivered';
                    
                    $orderStatus->save();
                    
                    return view('tangiblehtml.final-delivery-status-buyer', compact('deliveryStatus', 'currentStep', 'contactUs', 'enquiry', 'images', 'user_state', 'user_country', 'user_city', 'converted_shipping'));
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\FunctionChronographModel;

class FunctionChronographController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $functions = FunctionChronographModel::get();
        return view('admin.watch.functionChronograph.index',compact('functions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
         return view('admin.watch.functionChronograph.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
         $validator = $this->validate($request, [
            "chronograph" => 'required|unique:wa_factory_features_function_chronograph',
             "status" => 'required'
        ]);

          $case = FunctionChronographModel::create($validator);
          $case->save();
       
      return redirect('/admin/functionChronograph')->with('success_msg','Chronograph Created successfully!');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $functions_data = FunctionChronographModel::find($id);
        return view('admin.watch.functionChronograph.edit', compact('functions_data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = $this->validate($request, [
            "chronograph" => 'required|unique:wa_factory_features_function_chronograph',
            "status" => 'required'
        ]);
        
          $case = FunctionChronographModel::find($id);
          $case->chronograph = $request->input('chronograph');
          $case->status = $request->input('status');
          $case->save();
        
        return redirect('/admin/functionChronograph')->with('success_msg','Chronograph Updated successfully!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deleteRecords = FunctionChronographModel::find($id)->delete();
        return redirect('/admin/functionChronograph')->with('success_msg','Chronograph Deleted successfully!');
    }
}

<?php
namespace App\Http\Controllers;

use App\AgentModel;
use App\AutomobileAdditionalFeaturesModel;
use App\AutomobileBodyTypeModel;
use App\AutomobileCommentModel;
use App\AutomobileDamageImageModel;
use App\AutomobileDataListingModel;
use App\AutomobileFeatureListingModel;
use App\AutomobileFeaturesModel;
use App\AutomobileImageModel;
use App\AutomobileSecurityImageModel;
use App\BraceletClaspMaterialModel;
use App\BraceletClaspTypeModel;
use App\BraceletColorModel;
use App\BraceletMaterialModel;
use App\BuildYearModel;
use App\CarBrandModel;
use App\CarModel;
use App\CarPriceModel;
use App\CaseDiameterModel;
use App\CaseGlassTypeModel;
use App\CaseMMModel;
use App\CaseMaterialModel;
use App\CaseWaterRDModel;
use App\CityModel;
use App\ColourModel;
use App\ContactusModel;
use App\CountryModel;
use App\CurrencyModel;
use App\DialColorModel;
use App\DialPowerReserveModel;
use App\FuelTypeModel;
use App\FunctionAlarmModel;
use App\FunctionAnnualCalenderModel;
use App\FunctionChronographModel;
use App\FunctionDayModel;
use App\FunctionDoubleChronographModel;
use App\FunctionGMTModel;
use App\FunctionJumpingHourModel;
use App\FunctionMinuteRepeaterModel;
use App\FunctionPanormaDateModel;
use App\FunctionTourbillionModel;
use App\FunctionYearModel;
use App\GenderModel;
use App\InteriorColourModel;
use App\MovementModel;
use App\NoOfBalconiesModel;
use App\NoOfBathroomsModel;
use App\NoOfBedroomsModel;
use App\NoOfDoorsModel;
use App\PriceModel;
use App\PropertyAdditionalFeaturesModel;
use App\PropertyCommentModel;
use App\PropertyDataModel;
use App\PropertyFeaturesListingModel;
use App\PropertyFeaturesModel;
use App\PropertyImageModel;
use App\PropertyPriceModel;
use App\PropertyTypeModel;
use App\StateModel;
use App\Timezone;
use App\TopPropertyTypeModel;
use App\TransmissionModel;
use App\TypeModel;
use App\UserDetailsModel;
use App\WatchAdditionalFeaturesModel;
use App\WatchBezelModel;
use App\WatchColourModel;
use App\WatchCommentModel;
use App\WatchDamageImageModel;
use App\WatchDataListingModel;
use App\WatchDetailsFeaturesModel;
use App\WatchFeaturesListingModel;
use App\WatchFeaturesModel;
use App\WatchImageModel;
use App\WatchModel;
use App\WatchSecurityImageModel;
use App\Watchbrandmodel;
use App\YearOfManufactureModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use App\CommercialPropertyTypeModel;
use App\InclusionModel;

class ManageAgentInventoryController extends Controller
{

    public function editAuto($id)
    {
        $automobile = AutomobileDataListingModel::find($id);

        $contactUs = ContactusModel::get()->first();
        $propertyType = PropertyTypeModel::get();

        $makes = CarModel::get();
        $models = CarBrandModel::get();
        $colours = ColourModel::get();
        $interior_colours = InteriorColourModel::get();
        $country_data = CountryModel::get();
        $state_data = StateModel::get();
        $city_data = CityModel::get();
        $currency_data = CurrencyModel::get();
        $fuel_types = FuelTypeModel::get();
        $no_of_doors = NoOfDoorsModel::get();
        $body_types = AutomobileBodyTypeModel::get();
        $year = BuildYearModel::orderBy('build_year', 'DESC')->get();
        $transmissions = TransmissionModel::get();
        $prices = CarPriceModel::get();
        $timezone = Timezone::Orderby('offset')->get();
        $userDetails = UserDetailsModel::get();

        $feature_listing = AutomobileFeatureListingModel::get();

        /*
         * $i =0;
         * $html1 = '';
         * $html2 = '';
         * foreach ($feature_listing as $values){
         * $id = $values->id;
         * $propertyName = $values->property_name;
         *
         * if($i % 2 == 0){
         *
         *
         *
         * $html1 .= '<div class="form-check-inline w-100 border p-1 mt-1 mb-1">
         * <label class="form-check-label" for="'.$propertyName.'"></label>
         * <input type="checkbox" class="form-check-input" id="'.$propertyName.'" name="'.$id.'" value="1">'.$propertyName.'
         * </div>';
         * }else {
         *
         * $html2 .= '<div class="form-check-inline w-100 border p-1 mt-1 mb-1">
         * <label class="form-check-label" for="'.$propertyName.'"></label>
         * <input type="checkbox" class="form-check-input" id="'.$propertyName.'" name="'.$id.'" value="1">'.$propertyName.'
         * </div>';
         *
         *
         * }
         *
         * $i++;
         *
         * }
         */

        $currencySelected = CurrencyModel::find($automobile->currency_id);
        $citySelected = CityModel::find($automobile->city_id);
        $stateSelected = StateModel::find($automobile->state_id);
        $countrySelected = CountryModel::find($automobile->country_id);
        $makeSelected = CarModel::find($automobile->make_id);
        $modelSelected = CarBrandModel::find($automobile->model_id);
        $priceSelected = CarPriceModel::find($automobile->price_id);
        $fuelTypeSelected = FuelTypeModel::find($automobile->fuel_type_id);
        $colourSelected = ColourModel::find($automobile->colour_id);
        $noOfDoorsSelected = NoOfDoorsModel::find($automobile->no_of_doors_id);
        $bodyTypeSelected = AutomobileBodyTypeModel::find($automobile->body_type_id);
        $interiorColourSelected = InteriorColourModel::find($automobile->interior_colour_id);
        $transmissionSelected = TransmissionModel::find($automobile->transmission_id);
        $agentSelected = AgentModel::find($automobile->agent_id);
        $yearSelected = BuildYearModel::find($automobile->year_id);
        $timezoneSelected = Timezone::find($automobile->timezone_id);

        $currency_code = $currencySelected->currency_code;

        $converted_amount = currency()->convert(floatval($automobile->value), "USD", $currency_code, false);

        $automobile->value = $converted_amount;

        $models = CarBrandModel::select('id', 'automobile_model_name')->where('automobile_brand_id', '=', $makeSelected->id)->get();

        $additionlFeatures = AutomobileAdditionalFeaturesModel::select('au_automobile_additional_features.id as id', 'listing_id', 'feature')->where('listing_id', '=', $id)->get();

        $additionalHtml = '';
        foreach ($additionlFeatures as $features) {

            $additionalHtml .= '<tr class = "control-group-feature">
                    <td><label  >Additional Feature</label>
                    <input  class="form-control form-control-user " type="text" maxLength="100"  name="additional_feature[]" value="' . $features->feature . '"></td>
                    <td><button class="btn btn-primary  btn-danger btn-danger-feature" type="button">
				<i class="glyphicon glyphicon-remove"></i> Remove
			</button></td>
                    </tr>';
        }

        $feature_list = AutomobileFeatureListingModel::where('status', '=', '1')->get();

        $i = 0;
        $html1 = '';
        $html2 = '';
        foreach ($feature_list as $values) {

            $feature_id = $values->id;
            $propertyName = $values->property_name;

            $feature_data = AutomobileFeaturesModel::where('feature_id', '=', $feature_id)->where('listing_id', '=', $id)
                ->where('feature_available', '=', '1')
                ->first();

            if ($feature_data != null) {
                if ($i % 2 == 0) {
                    $html1 .= '<div class="form-check-inline w-100 border p-1 mt-1 mb-1">
                   <label class="form-check-label" for="' . $propertyName . '"></label>
                    <input   type="checkbox" class="form-check-input" id="' . $propertyName . '" name="' . $feature_id . '" value="1" checked>' . $propertyName . '
                      </div>';
                } else {
                    $html2 .= '<div class="form-check-inline w-100 border p-1 mt-1 mb-1">
                   <label class="form-check-label" for="' . $propertyName . '"></label>
                    <input   type="checkbox" class="form-check-input" id="' . $propertyName . '" name="' . $feature_id . '" value="1" checked>' . $propertyName . '
                      </div>';
                }
            } else {

                if ($i % 2 == 0) {
                    $html1 .= '<div class="form-check-inline w-100 border p-1 mt-1 mb-1">
                   <label class="form-check-label" for="' . $propertyName . '"></label>
                    <input   type="checkbox" class="form-check-input" id="' . $propertyName . '" name="' . $feature_id . '" value="1">' . $propertyName . '
                      </div>';
                } else {
                    $html2 .= '<div class="form-check-inline w-100 border p-1 mt-1 mb-1">
                   <label class="form-check-label" for="' . $propertyName . '"></label>
                    <input   type="checkbox" class="form-check-input" id="' . $propertyName . '" name="' . $feature_id . '" value="1">' . $propertyName . '
                      </div>';
                }
            }
            $i ++;
        }

        // dd($htmlSelected);
        $comment_data = AutomobileCommentModel::where('listing_id', $id)->first();

        $images_data = AutomobileImageModel::where('listing_id', '=', $id)->get();

        $security_data = AutomobileSecurityImageModel::where('listing_id', '=', $id)->get();

        $damageImages = AutomobileDamageImageModel::where('listing_id', '=', $id)->get();

        $preloadImages = array();
        $i = 0;
        foreach ($images_data as $data) {
            $array = array();
            $array['id'] = $data->id;
            $array['src'] = 'uploads/' . $data->filename;
            $preloadImages[$i ++] = (object) $array;
        }

        $loadedDamageImages = array();
        $i = 0;
        foreach ($damageImages as $data) {
            $array = array();
            $array['id'] = $data->id;
            $array['src'] = 'uploads/' . $data->filename;
            $loadedDamageImages[$i ++] = (object) $array;
        }

        return view('agent.edit-automobile-ads', compact('preloadImages', 'loadedDamageImages', 'automobile', 'makes', 'models', 'year', 'colours', 'interior_colours', 'country_data', 'state_data', 'city_data', 'currency_data', 'fuel_types', 'no_of_doors', 'body_types', 'transmissions', 'prices', 'timezone', 'html1', 'html2', 'contactUs', 'propertyType', 'userDetails', 'yearSelected', 'stateSelected', 'countrySelected', 'citySelected', 'makeSelected', 'modelSelected', 'priceSelected', 'fuelTypeSelected', 'colourSelected', 'noOfDoorsSelected', 'bodyTypeSelected', 'interiorColourSelected', 'currencySelected', 'comment_data', 'images_data', 'transmissions', 'transmissionSelected', 'security_data', 'additionalHtml'));
    }

    public function storeAuto(Request $request)
    {
        $listing_id = $request->productId;

        $converted_amount = NULL;
        if ($request->input('price') != '') {
            $currency_code = CurrencyModel::find($request->input('currency'));

            $converted_amount = currency()->convert(floatval($request->input('price')), $currency_code->currency_code, "USD", false);
        }

        $createAutomobile = AutomobileDataListingModel::find($listing_id);
        ;

        $createAutomobile->ad_title = $request->get('adtitle');
        $createAutomobile->currency_id = $request->get('currency');
        if ($request->get('stateId') != '')
            $createAutomobile->state_id = $request->get('stateId');
        $createAutomobile->country_id = $request->get('countryId');
        $createAutomobile->city_id = $request->get('cityId');
        $createAutomobile->make_id = $request->get('make1');
        $createAutomobile->model_id = $request->get('model');
        if ($request->get('price_on_request') != '')
            $createAutomobile->price_on_request = $request->get('price_on_request');
        $createAutomobile->value = $converted_amount;
        $createAutomobile->price_id = $request->get('pricetType');
        $createAutomobile->fuel_type_id = $request->get('fuelType');
        $createAutomobile->colour_id = $request->get('color');
        if ($request->get('no_of_doors') != '')
            $createAutomobile->no_of_doors_id = $request->get('no_of_doors');
        $createAutomobile->body_type_id = $request->get('body_type');
        $createAutomobile->transmission = $request->get('transmission');
        if ($request->get('registrationPlace') != '')
            $createAutomobile->registration_place = $request->get('registrationPlace');
        $createAutomobile->rhdorlhd = $request->get('rhdorlhd');
        $createAutomobile->year_id = $request->get('year');
        $createAutomobile->neworused = $request->get('condition');
        $createAutomobile->odometer = $request->get('odometer');
        $createAutomobile->metric = $request->get('odometermetric');
        if ($request->get('engine') != '')
            $createAutomobile->engine = $request->get('engine');
        if ($request->get('interior_color') != '')
            $createAutomobile->interior_colour_id = $request->get('interior_color');
        $createAutomobile->transmission_id = $request->get('transmission');
        $createAutomobile->timezone_id = $request->get('timezone');
        $createAutomobile->agent_id = $request->get('agentId');
        $createAutomobile->vin_number = $request->get('VIN');
        $createAutomobile->registration_number = $request->get('registrationPlate');

        $createAutomobile->save();

        $deleteRecords = AutomobileAdditionalFeaturesModel::where('listing_id', '=', $listing_id)->delete();

        if ($request->input('additional_feature') ?? '') {

            foreach ($request->input('additional_feature') as $features) {

                if ($features != null) {
                    $uploadFeature = new AutomobileAdditionalFeaturesModel();

                    $uploadFeature->feature = $features;
                    $uploadFeature->listing_id = $listing_id;
                    $uploadFeature->save();
                }
            }
        }

        $deleteRecords = AutomobileFeaturesModel::where('listing_id', '=', $listing_id)->delete();

        foreach ($request->request as $key => $value) {
            if ($key != '_token' && $key != 'listing_id' && gettype($key) != 'string') {
                $createFeature = new AutomobileFeaturesModel();
                $createFeature->listing_id = $listing_id;
                $createFeature->feature_id = $key;
                $createFeature->feature_available = $value;
                $createFeature->save();
            }
        }

        $deleteRecords = AutomobileCommentModel::where('listing_id', '=', $listing_id)->delete();

        if ($request->comment ?? '') {
            $createComment = new AutomobileCommentModel();
            $createComment->comment = $request->input('comment');
            $createComment->listing_id = $listing_id;
            $createComment->save();
        }

        if ($request->file('normalImages') ?? '') {
            foreach ($request->file('normalImages') as $image) {
                echo "<br>";
                $uploadImage = new AutomobileImageModel();

                $extension = $image->getClientOriginalExtension();
                Storage::disk('public')->put($image->getFilename() . '.' . $extension, File::get($image));

                $uploadImage->images = $image->getClientMimeType();
                $uploadImage->filename = $image->getFilename() . '.' . $extension;
                $uploadImage->listing_id = $listing_id;

                $uploadImage->save();
            }
        }

        if ($request->file('damageImages2') ?? '') {
            foreach ($request->file('damageImages2') as $image) {
                $uploadImage = new AutomobileDamageImageModel();

                $extension = $image->getClientOriginalExtension();
                Storage::disk('public')->put($image->getFilename() . '.' . $extension, File::get($image));

                $uploadImage->images = $image->getClientMimeType();
                $uploadImage->filename = $image->getFilename() . '.' . $extension;
                $uploadImage->listing_id = $listing_id;

                $uploadImage->save();
            }
        }

        if ($request->file('security1') ?? '') {
            foreach ($request->file('security1') as $image) {
                $uploadImage = new AutomobileSecurityImageModel();

                $extension = $image->getClientOriginalExtension();
                Storage::disk('public')->put($image->getFilename() . '.' . $extension, File::get($image));

                $uploadImage->images = $image->getClientMimeType();
                $uploadImage->filename = $image->getFilename() . '.' . $extension;
                $uploadImage->listing_id = $listing_id;

                $uploadImage->save();
            }
        }

        if ($request->file('security2') ?? '') {
            foreach ($request->file('security2') as $image) {
                $uploadImage = new AutomobileSecurityImageModel();

                $extension = $image->getClientOriginalExtension();
                Storage::disk('public')->put($image->getFilename() . '.' . $extension, File::get($image));

                $uploadImage->images = $image->getClientMimeType();
                $uploadImage->filename = $image->getFilename() . '.' . $extension;
                $uploadImage->listing_id = $listing_id;

                $uploadImage->save();
            }
        }

        return redirect('agent/inventoryList')->with('success_msg', 'Automobile Updated successfully!');
    }

    public function deleteauto($id, $imageType)
    {
        if ($imageType == 'normalPhoto') {

            $deleteRecords = AutomobileImageModel::where('id', '=', $id)->delete();
        } else if ($imageType == 'damagePhotos') {

            $deleteRecords = AutomobileDamageImageModel::where('id', '=', $id)->delete();
        } else if ($imageType == 'securityPhotos') {

            $deleteRecords = AutomobileSecurityImageModel::where('id', '=', $id)->delete();
        }

        return response()->json([
            'success_msg' => 'Image deleted Successfully..!!'
        ]);
    }

    public function editWatch($id)
    {
        $watch = WatchDataListingModel::find($id);

        $selectedCurrency = CurrencyModel::select('currency_code as code')->where('status', '=', '1')->first();
        currency()->setUserCurrency($selectedCurrency->code);

        $contactUs = ContactusModel::get()->first();
        $propertyType = PropertyTypeModel::get();

        $models = Watchbrandmodel::orderBy('watch_model_name', 'ASC')->get();
        $brands = WatchModel::orderBy('watch_brand_name', 'ASC')->get();
        $country_data = CountryModel::get();
        $case_diameters = CaseDiameterModel::orderBy('case_diameter', 'ASC')->get();
        $movements = MovementModel::get();
        $types = TypeModel::get();
        $year_of_manufactures = YearOfManufactureModel::orderBy('year_of_manufacture', 'ASC')->get();
        $genders = GenderModel::get();
        $currency_data = CurrencyModel::get();
        $prices = PriceModel::get();
        $inclusions_data = InclusionModel::get();

        $power_reserve = DialPowerReserveModel::get();
        $bezel_material = WatchBezelModel::get();
        $dial_color = DialColorModel::get();
        $bracelet_material = BraceletMaterialModel::get();
        $bracelet_color = BraceletColorModel::get();
        $type_of_clasp = BraceletClaspTypeModel::get();
        $clasp_material = BraceletClaspMaterialModel::get();
        $case_material = CaseMaterialModel::get();
        $case_mm = CaseMMModel::get();
        $wrd = CaseWaterRDModel::get();
        $glass_type = CaseGlassTypeModel::get();
        $chronograph = FunctionChronographModel::get();
        $tourbillion = FunctionTourbillionModel::get();
        $gmt = FunctionGMTModel::get();
        $annual_calender = FunctionAnnualCalenderModel::get();
        $minute_repeater = FunctionMinuteRepeaterModel::get();
        $double_chronograph = FunctionDoubleChronographModel::get();
        $panorma_date = FunctionPanormaDateModel::get();
        $jumping_hour = FunctionJumpingHourModel::get();
        $alarm = FunctionAlarmModel::get();
        $year = FunctionYearModel::get();
        $day = FunctionDayModel::get();
        $agents = AgentModel::get();
        $timezone = Timezone::Orderby('offset')->get();
        $userDetails = UserDetailsModel::get();
        $country_data = CountryModel::get();
        $state_data = StateModel::get();
        $city_data = CityModel::get();

        $currencySelected = CurrencyModel::find($watch->currency_id);
        $stateSelected = StateModel::find($watch->state_id);
        $countrySelected = CountryModel::find($watch->country_id);
        $citySelected = CityModel::find($watch->city_id);
        $modelSelected = Watchbrandmodel::find($watch->model_id);
        $brandSelected = Watchmodel::find($watch->brand_id);
        $priceSelected = Pricemodel::find($watch->price_id);
        $caseDiameterSelected = CaseDiametermodel::find($watch->case_diameter_id);
        $colorSelected = WatchColourModel::find($watch->color_id);
        $movementSelected = Movementmodel::find($watch->movement_id);
        $typeSelected = Typemodel::find($watch->type_id);
        $year_of_manufactureSelected = YearOfManufactureModel::find($watch->year_of_manufacture_id);
        $genderSelected = Gendermodel::find($watch->gender_id);
        $agentSelected = AgentModel::find($watch->agent_id);
        $timezoneSelected = Timezone::find($watch->timezone_id);
        $inclusionSelected = InclusionModel::find($watch->inclusions);
        
        $feature_data = WatchFeaturesModel::where('listing_id', $id)->first();

        $powerReserveSelected = DialPowerReserveModel::find($feature_data->power_reserve_id);
        $bezelMaterialSelected = WatchBezelModel::find($feature_data->bezel_material_id);
        $dialColorSelected = DialColorModel::find($feature_data->dial_color_id);
        $braceletMaterialSelected = BraceletMaterialModel::find($feature_data->bracelet_material_id);
        $braceletColorSelected = BraceletColorModel::find($feature_data->bracelet_color_id);
        $typeOfClaspSelected = BraceletClaspTypeModel::find($feature_data->type_of_clasp_id);
        $claspMaterialSelected = BraceletClaspMaterialModel::find($feature_data->clasp_material_id);
        $caseMaterialSelected = CaseMaterialModel::find($feature_data->case_material_id);
        $caseMMSelected = CaseMMModel::find($feature_data->case_mm_id);
        $wrdSelected = CaseWaterRDModel::find($feature_data->water_resisitance_depth_id);
        $glassTypeSelected = CaseGlassTypeModel::find($feature_data->glass_type_id);

        $chronographSelected = FunctionChronographModel::find($feature_data->chronograph_id);
        $tourbillionSelected = FunctionTourbillionModel::find($feature_data->tourbillion_id);
        $gmtSelected = FunctionGMTModel::find($feature_data->gmt_id);
        $annualCalenderSelected = FunctionAnnualCalenderModel::find($feature_data->annual_calender_id);
        $minuteRepeaterSelected = FunctionMinuteRepeaterModel::find($feature_data->minute_repeater_id);
        $doubleChronographSelected = FunctionDoubleChronographModel::find($feature_data->double_chronograph_id);
        $panormaDateSelected = FunctionPanormaDateModel::find($feature_data->panorma_date_id);
        $jumpingHourSelected = FunctionJumpingHourModel::find($feature_data->jumping_hour_id);
        $alarmSelected = FunctionAlarmModel::find($feature_data->alarm_id);
        $yearSelected = FunctionYearModel::find($feature_data->year_id);
        $daySelected = FunctionDayModel::find($feature_data->day_id);

        $currency_code = $currencySelected->currency_code;

        $converted_amount = currency()->convert(floatval($watch->watch_price), "USD", $currency_code, false);

        $watch->watch_price = $converted_amount;

        $feature_listing = WatchFeaturesListingModel::where('status', '=', '1')->get();

        $i = 0;
        $html1 = '';
        $html2 = '';
        foreach ($feature_listing as $values) {
            $feature_id = $values->id;
            $propertyName = $values->property_name;

            $feature_data = WatchDetailsFeaturesModel::where('feature_id', '=', $feature_id)->where('listing_id', '=', $id)
                ->where('feature_available', '=', '1')
                ->first();

            if ($feature_data != null) {
                if ($i % 2 == 0) {
                    $html1 .= '<div class="form-check-inline w-100 border p-1 mt-1 mb-1">
                   <label class="form-check-label" for="' . $propertyName . '"></label>
                    <input   type="checkbox" class="form-check-input" id="' . $propertyName . '" name="' . $feature_id . '" value="1" checked>' . $propertyName . '
                      </div>';
                } else {
                    $html2 .= '<div class="form-check-inline w-100 border p-1 mt-1 mb-1">
                   <label class="form-check-label" for="' . $propertyName . '"></label>
                    <input   type="checkbox" class="form-check-input" id="' . $propertyName . '" name="' . $feature_id . '" value="1" checked>' . $propertyName . '
                      </div>';
                }
            } else {

                if ($i % 2 == 0) {
                    $html1 .= '<div class="form-check-inline w-100 border p-1 mt-1 mb-1">
                   <label class="form-check-label" for="' . $propertyName . '"></label>
                    <input   type="checkbox" class="form-check-input" id="' . $propertyName . '" name="' . $feature_id . '" value="1">' . $propertyName . '
                      </div>';
                } else {
                    $html2 .= '<div class="form-check-inline w-100 border p-1 mt-1 mb-1">
                   <label class="form-check-label" for="' . $propertyName . '"></label>
                    <input   type="checkbox" class="form-check-input" id="' . $propertyName . '" name="' . $feature_id . '" value="1">' . $propertyName . '
                      </div>';
                }
            }

            $i ++;
        }

        $additionlFeatures = WatchAdditionalFeaturesModel::select('wa_watch_additional_features.id as id', 'listing_id', 'feature')->where('listing_id', '=', $id)->get();

        $additionalHtml = '';
        foreach ($additionlFeatures as $features) {

            $additionalHtml .= '<tr class = "control-group-feature">
                    <td><label  >Additional Feature</label>
                    <input  class="form-control form-control-user " type="text" maxLength="100"  name="additional_feature[]" value="' . $features->feature . '"></td>
                    <td><button class="btn btn-primary  btn-danger btn-danger-feature" type="button">
				<i class="glyphicon glyphicon-remove"></i> Remove
			</button></td>
                    </tr>';
        }

        $comment_data = WatchCommentModel::where('listing_id', $id)->first();

        $images_data = WatchImageModel::where('listing_id', '=', $id)->get();

        $security_images = WatchSecurityImageModel::where('listing_id', '=', $id)->get();

        $damageImages = WatchDamageImageModel::where('listing_id', '=', $id)->get();

        $preloadImages = array();
        $i = 0;
        foreach ($images_data as $data) {
            $array = array();
            $array['id'] = $data->id;
            $array['src'] = 'uploads/' . $data->filename;
            $preloadImages[$i ++] = (object) $array;
        }

        $loadedDamageImages = array();
        $i = 0;
        foreach ($damageImages as $data) {
            $array = array();
            $array['id'] = $data->id;
            $array['src'] = 'uploads/' . $data->filename;
            $loadedDamageImages[$i ++] = (object) $array;
        }

        return view('agent.edit-watch-ads', compact('inclusions_data','inclusionSelected','watch', 'contactUs', 'propertyType', 'models', 'brands', 'country_data', 'case_diameters', 'movements', 'types', 'year_of_manufactures', 'genders', 'power_reserve', 'bezel_material', 'dial_color', 'bracelet_material', 'bracelet_color', 'type_of_clasp', 'clasp_material', 'case_material', 'case_mm', 'wrd', 'glass_type', 'chronograph', 'tourbillion', 'gmt', 'annual_calender', 'minute_repeater', 'double_chronograph', 'panorma_date', 'jumping_hour', 'alarm', 'year', 'day', 'agents', 'timezone', 'html1', 'html2', 'currency_data', 'prices', 'userDetails', 'country_data', 'state_data', 'city_data', 'powerReserveSelected', 'bezelMaterialSelected', 'dialColorSelected', 'braceletMaterialSelected', 'braceletColorSelected', 'typeOfClaspSelected', 'claspMaterialSelected', 'caseMaterialSelected', 'caseMMSelected', 'wrdSelected', 'glassTypeSelected', 'chronographSelected', 'tourbillionSelected', 'gmtSelected', 'annualCalenderSelected', 'minuteRepeaterSelected', 'doubleChronographSelected', 'panormaDateSelected', 'jumpingHourSelected', 'alarmSelected', 'yearSelected', 'daySelected', 'currencySelected', 'stateSelected', 'countrySelected', 'citySelected', 'modelSelected', 'brandSelected', 'priceSelected', 'caseDiameterSelected', 'colorSelected', 'movementSelected', 'typeSelected', 'year_of_manufactureSelected', 'genderSelected', 'agentSelected', 'timezoneSelected', 'additionalHtml', 'comment_data', 'images_data', 'security_images', 'preloadImages', 'loadedDamageImages'));
    }

    public function storewatch(Request $request)
    {
        // dd($request);
        $dealer_id = "";
        $agent = AgentModel::find($request->input('agent'));
        if ($agent != null)
            $dealer_id = $agent->agency_id;

        $watch_price = NULL;
        if ($request->input('watch_price') != '') {

            $currency_code = CurrencyModel::find($request->input('currency'));

            $watch_price = currency()->convert(floatval($request->input('watch_price')), $currency_code->currency_code, "USD", false);
            // $priceSqft = $request->input('priceSqft');
        }

        $createWatch = WatchDataListingModel::find($request->productId);

        $createWatch->ad_title = $request->input('adtitle');
        $createWatch->currency_id = $request->input('currency');
        if ($request->input('stateId') != '')
            $createWatch->state_id = $request->input('stateId');
        $createWatch->country_id = $request->input('countryId');
        $createWatch->city_id = $request->input('city-id');
        $createWatch->brand_id = $request->input('brand');
        $createWatch->price_on_request = $request->input('price_on_request');
        $createWatch->model_id = $request->input('model');
        $createWatch->watch_price = $watch_price;
        if ($request->input('pricetType') != '')
            $createWatch->price_id = $request->input('pricetType');
        if ($request->input('case_diameter') != '')
            $createWatch->case_diameter_id = $request->input('case_diameter');
        $createWatch->movement_id = $request->input('movement');
        $createWatch->type_id = $request->input('type');
        $createWatch->year_of_manufacture_id = $request->input('yom');
        $createWatch->gender_id = $request->input('gender');
        $createWatch->inclusions = $request->input('inclusions');
        $createWatch->reference_no = $request->input('referenceNo');
        $createWatch->watch_condition = $request->input('watchCondition');
        $createWatch->status = $request->input('status');
        $createWatch->brand_name = WatchModel::find($request->input('brand'))->watch_brand_name;
        $createWatch->model_name = Watchbrandmodel::find($request->input('model'))->watch_model_name;
        $createWatch->agent_id = $request->input('agentId');
        if ($dealer_id != "")
            $createWatch->agencies_id = $dealer_id;
        $createWatch->timezone_id = $request->input('timezone');

        $createWatch->save();

        $createFeature = WatchFeaturesModel::where('listing_id', $request->productId)->first();

        $createFeature->listing_id = $request->productId;
        $createFeature->bezel_material_id = $request->input('bezelMaterial');
        $createFeature->power_reserve_id = $request->input('powerReserve');
        $createFeature->dial_color_id = $request->input('dialColor');
        $createFeature->bracelet_material_id = $request->input('braceletMaterial');
        $createFeature->bracelet_color_id = $request->input('braceletColor');
        $createFeature->type_of_clasp_id = $request->input('typeOfClasp');
        $createFeature->clasp_material_id = $request->input('claspMaterial');
        $createFeature->case_material_id = $request->input('caseMaterial');
        $createFeature->case_mm_id = $request->input('caseMM');
        $createFeature->water_resisitance_depth_id = $request->input('wrd');
        $createFeature->glass_type_id = $request->input('glassType');

        $createFeature->dial_color = DialColorModel::find($request->input('dialColor'))->dial_color;
        $createFeature->bracelet_material = BraceletMaterialModel::find($request->input('braceletMaterial'))->bracelet_material;

        $createFeature->save();

        $deleteRecords = WatchAdditionalFeaturesModel::where('listing_id', '=', $request->productId)->delete();

        if ($request->input('additional_feature') ?? '') {

            foreach ($request->input('additional_feature') as $features) {

                if ($features != null) {
                    $uploadFeature = new WatchAdditionalFeaturesModel();

                    $uploadFeature->feature = $features;
                    $uploadFeature->listing_id = $request->productId;
                    $uploadFeature->save();
                }
            }
        }

        $deleteRecords = WatchDetailsFeaturesModel::where('listing_id', '=', $request->productId)->delete();

        foreach ($request->request as $key => $value) {
            if ($key != '_token' && $key != 'listing_id' && gettype($key) != 'string') {
                $createDetailFeature = new WatchDetailsFeaturesModel();
                $createDetailFeature->listing_id = $request->productId;
                $createDetailFeature->feature_id = $key;
                $createDetailFeature->feature_available = $value;
                $createDetailFeature->save();
            }
        }

        $deleteRecords = WatchCommentModel::where('listing_id', '=', $request->productId)->delete();

        if ($request->comment ?? '') {
            $createComment = new WatchCommentModel();
            $createComment->comment = $request->input('comment');
            $createComment->listing_id = $request->productId;
            $createComment->save();
        }

        if ($request->file('normalImages') ?? '') {
            foreach ($request->file('normalImages') as $image) {
                $uploadImage = new WatchImageModel();

                $extension = $image->getClientOriginalExtension();
                Storage::disk('public')->put($image->getFilename() . '.' . $extension, File::get($image));

                $uploadImage->images = $image->getClientMimeType();
                $uploadImage->filename = $image->getFilename() . '.' . $extension;
                $uploadImage->listing_id = $request->productId;

                $uploadImage->save();
            }
        }

        if ($request->file('damageImages2') ?? '') {
            foreach ($request->file('damageImages2') as $image) {
                $uploadImage = new WatchDamageImageModel();

                $extension = $image->getClientOriginalExtension();
                Storage::disk('public')->put($image->getFilename() . '.' . $extension, File::get($image));

                $uploadImage->images = $image->getClientMimeType();
                $uploadImage->filename = $image->getFilename() . '.' . $extension;
                $uploadImage->listing_id = $request->productId;

                $uploadImage->save();
            }
        }

        if ($request->file('security1') ?? '') {
            foreach ($request->file('security1') as $image) {
                $uploadImage = new WatchSecurityImageModel();

                $extension = $image->getClientOriginalExtension();
                Storage::disk('public')->put($image->getFilename() . '.' . $extension, File::get($image));

                $uploadImage->images = $image->getClientMimeType();
                $uploadImage->filename = $image->getFilename() . '.' . $extension;
                $uploadImage->security_time = $request->get('security-time1');
                $uploadImage->listing_id = $request->productId;

                $uploadImage->save();
            }
        }

        if ($request->file('security2') ?? '') {
            foreach ($request->file('security2') as $image) {
                $uploadImage = new WatchSecurityImageModel();

                $extension = $image->getClientOriginalExtension();
                Storage::disk('public')->put($image->getFilename() . '.' . $extension, File::get($image));

                $uploadImage->images = $image->getClientMimeType();
                $uploadImage->filename = $image->getFilename() . '.' . $extension;
                $uploadImage->security_time = $request->get('security-time2');
                $uploadImage->listing_id = $request->productId;

                $uploadImage->save();
            }
        }
        $productId = $request->productId;

        return redirect('agent/inventoryList')->with('success_msg', 'Watch Updated successfully!');
    }

    public function deletewatch($id, $imageType)
    {
        if ($imageType == 'normalPhoto') {

            $deleteRecords = WatchImageModel::where('id', '=', $id)->delete();
        } else if ($imageType == 'damagePhotos') {

            $deleteRecords = WatchDamageImageModel::where('id', '=', $id)->delete();
        } else if ($imageType == 'securityPhotos') {

            $deleteRecords = WatchSecurityImageModel::where('id', '=', $id)->delete();
        }

        return response()->json([
            'success_msg' => 'Image deleted Successfully..!!'
        ]);
    }

    public function editProperty($id)
    {
        $property = PropertyDataModel::find($id);

        $currency_data = CurrencyModel::get();
        $city_data = CityModel::get();
        $state_data = StateModel::get();
        $country_data = CountryModel::get();
        $bedroom_data = NoOfBedroomsModel::get();
        $bathroom_data = NoOfBathroomsModel::get();
        $parking_data = NoOfBalconiesModel::get();
        $buildYear_data = BuildYearModel::get();
        $prices = PropertyPriceModel::get();
        $agents = AgentModel::get();
        $toppropertyType_data = TopPropertyTypeModel::get();
        $timezone = Timezone::Orderby('offset')->get();

        $priceSelected = PropertyPriceModel::find($property->price_id);
        $currencySelected = CurrencyModel::find($property->currency_id);
        $citySelected = CityModel::find($property->city_id);
        $stateId = 0;
        if ($property->state_id != '')
            $stateId = $property->state_id;

        $stateSelected = StateModel::find($stateId);
        $countrySelected = CountryModel::find($property->country_id);
        $bathroomSelected = NoOfBathroomsModel::find($property->bathroom_id);
        $parkingSelected = NoOfBalconiesModel::find($property->parking_id);
        $bedroomSelected = NoOfBedroomsModel::find($property->bed_id);
        $typeSelected = PropertyTypeModel::find($property->property_type_id);
        $yearSelected = BuildYearModel::find($property->year_built_id);
        $agentSelected = AgentModel::find($property->agent_id);

        $toppropertySelected = TopPropertyTypeModel::find($property->top_property_type_id);

        if ($toppropertySelected->property_type == 'Residential')
            $propertyType_data = PropertyTypeModel::where('id', '=', $property->property_type_id)->get();
        else
            $propertyType_data = CommercialPropertyTypeModel::where('id', '=', $property->property_type_id)->get();

        $timezoneSelected = Timezone::find($property->timezone_id);

        $currency_code = $currencySelected->currency_code;

        $converted_amount = currency()->convert(floatval($property->property_price), "USD", $currency_code, false);

        $property->property_price = $converted_amount;

        $additionlFeatures = PropertyAdditionalFeaturesModel::select('re_property_additional_features.id as id', 'listing_id', 'feature')->where('listing_id', '=', $id)->get();

        $additionalHtml = '';
        foreach ($additionlFeatures as $features) {

            $additionalHtml .= '<tr class = "control-group-feature">
                    <td><label  >Additional Feature</label>
                    <input  class="form-control form-control-user " type="text" maxLength="100"  name="additional_feature[]" value="' . $features->feature . '"></td>
                    <td><button class="btn btn-primary  btn-danger btn-danger-feature" type="button">
				<i class="glyphicon glyphicon-remove"></i> Remove
			</button></td>
                    </tr>';
        }

        $feature_listing = PropertyFeaturesListingModel::get();

        $i = 0;
        $html1 = '';
        $html2 = '';
        foreach ($feature_listing as $values) {

            $feature_id = $values->id;
            $propertyName = $values->property_name;

            $feature_data = PropertyFeaturesModel::where('feature_id', '=', $feature_id)->where('listing_id', '=', $id)
                ->where('feature_available', '=', '1')
                ->first();

            if ($feature_data != null) {
                if ($i % 2 == 0) {
                    $html1 .= '<div class="form-check-inline w-100 border p-1 mt-1 mb-1">
                   <label class="form-check-label" for="' . $propertyName . '"></label>
                    <input   type="checkbox" class="form-check-input" id="' . $propertyName . '" name="' . $feature_id . '" value="1" checked>' . $propertyName . '
                      </div>';
                } else {
                    $html2 .= '<div class="form-check-inline w-100 border p-1 mt-1 mb-1">
                   <label class="form-check-label" for="' . $propertyName . '"></label>
                    <input   type="checkbox" class="form-check-input" id="' . $propertyName . '" name="' . $feature_id . '" value="1" checked>' . $propertyName . '
                      </div>';
                }
            } else {

                if ($i % 2 == 0) {
                    $html1 .= '<div class="form-check-inline w-100 border p-1 mt-1 mb-1">
                   <label class="form-check-label" for="' . $propertyName . '"></label>
                    <input   type="checkbox" class="form-check-input" id="' . $propertyName . '" name="' . $feature_id . '" value="1">' . $propertyName . '
                      </div>';
                } else {
                    $html2 .= '<div class="form-check-inline w-100 border p-1 mt-1 mb-1">
                   <label class="form-check-label" for="' . $propertyName . '"></label>
                    <input   type="checkbox" class="form-check-input" id="' . $propertyName . '" name="' . $feature_id . '" value="1">' . $propertyName . '
                      </div>';
                }
            }
            $i ++;
        }

        $comment_data = PropertyCommentModel::where('listing_id', $id)->first();

        $images_data = PropertyImageModel::where('listing_id', '=', $id)->get();

        $editStatus = "edit";

        return view('agent.create-property', compact('editStatus', 'timezone', 'timezoneSelected', 'parkingSelected', 'parking_data', 'additionalHtml', 'images_data', 'comment_data', 'html1', 'html2', 'agents', 'property', 'prices', 'currency_data', 'city_data', 'state_data', 'country_data', 'bedroom_data', 'bathroom_data', 'propertyType_data', 'buildYear_data', 'citySelected', 'stateSelected', 'countrySelected', 'bathroomSelected', 'bedroomSelected', 'yearSelected', 'typeSelected', 'currencySelected', 'priceSelected', 'agentSelected', 'toppropertyType_data', 'toppropertySelected'));
    }

    public function storeProperty(Request $request)
    {

        // dd($request->productId);
        /*
         * $this->validate($request, [
         * 'adTitle' => 'required',
         * 'currency' => 'required',
         * 'cityId' => 'required',
         * 'countryId' => 'required',
         * 'establishmentType' => 'required',
         * 'bedRoom' => 'required',
         * 'bathRoom' => 'required',
         * 'toppropertyType' => 'required',
         * 'propertyType' => 'required',
         * 'metric' => 'required',
         * 'sol' => 'required',
         * 'buildingSize' => 'required',
         * 'propertySL' => 'required',
         * 'postalCode' => 'required',
         * 'builtId' => 'required',
         * 'street_1' => 'required',
         * 'street_2' => 'required',
         * 'agent' => 'required',
         * 'status' => 'required',
         * 'comment' => 'required'
         *
         * ]);
         */
        $unitNumber = NULL;
        if ($request->input('unitNumber') != '') {
            $unitNumber = $request->input('unitNumber');
        }

        $priceSqft = NULL;
        if ($request->input('priceSqft') != '') {

            $currency_code = CurrencyModel::find($request->input('currency'));

            $priceSqft = currency()->convert(floatval($request->input('priceSqft')), $currency_code->currency_code, "USD", false);
            // $priceSqft = $request->input('priceSqft');
        }

        $price = NULL;
        if ($request->input('price') != '') {
            $price = $request->input('price');
        }

        $agent = AgentModel::where('userID', '=', $request->input('agentId'))->first();
        // dd($agent);
        $dealer_id = $agent->agency_id;

        $updateProperty = PropertyDataModel::find($request->productId);

        $updateProperty->ad_title = $request->input('adTitle');
        $updateProperty->currency_id = $request->input('currency');
        $updateProperty->city_id = $request->input('cityId');
        if ($request->input('stateId') != '')
            $updateProperty->state_id = $request->input('stateId');
        $updateProperty->country_id = $request->input('countryId');
        $updateProperty->establishment_type = $request->input('establishmentType');
        $updateProperty->bed_id = $request->input('bedRoom');
        $updateProperty->bathroom_id = $request->input('bathRoom');
        $updateProperty->top_property_type_id = $request->input('toppropertyType');
        $updateProperty->property_type_id = $request->input('propertyType');
        $updateProperty->metric = $request->input('metric');
        $updateProperty->land_size = $request->input('sol');
        $updateProperty->building_size = $request->input('buildingSize');
        $updateProperty->property_sale_lease = $request->input('propertySL');
        $updateProperty->unit_number = $unitNumber;
        $updateProperty->postal_code = $request->input('postalCode');
        $updateProperty->price_id = $price;
        $updateProperty->price_on_request = $request->input('price_on_request');
        $updateProperty->property_price = $priceSqft;
        $updateProperty->year_built_id = $request->input('builtId');
        $updateProperty->street_1 = $request->input('street_1');
        $updateProperty->street_2 = $request->input('street_2');
        $updateProperty->agent_id = $agent->id;
        $updateProperty->agencies_id = $dealer_id;
        $updateProperty->status = $request->input('status');
        $updateProperty->parking_id = $request->input('parking');
        $updateProperty->timezone_id = $request->input('timezone');

        $updateProperty->save();

        $deleteRecords = PropertyAdditionalFeaturesModel::where('listing_id', '=', $request->productId)->delete();

        if ($request->input('additional_feature') ?? '') {

            foreach ($request->input('additional_feature') as $features) {

                if ($features != null) {
                    $uploadFeature = new PropertyAdditionalFeaturesModel();

                    $uploadFeature->feature = $features;
                    $uploadFeature->listing_id = $id;
                    $uploadFeature->save();
                }
            }
        }

        $deleteRecords = PropertyFeaturesModel::where('listing_id', '=', $request->productId)->delete();

        foreach ($request->request as $key => $value) {

            if ($key != '_token' && $key != 'listing_id' && gettype($key) != 'string') {
                $createFeature = new PropertyFeaturesModel();
                $createFeature->listing_id = $request->productId;
                $createFeature->feature_id = $key;
                $createFeature->feature_available = $value;
                $createFeature->save();
            }
        }
        if ($request->comment ?? '') {
            $updateComment = PropertyCommentModel::where('listing_id', $request->productId)->first();
            $updateComment->comment = $request->comment;
            $updateComment->listing_id = $request->productId;
            $updateComment->save();
        }
        if ($request->file('photos') ?? '') {
            foreach ($request->file('photos') as $image) {
                $uploadImage = new PropertyImageModel();

                $extension = $image->getClientOriginalExtension();
                Storage::disk('public')->put($image->getFilename() . '.' . $extension, File::get($image));

                $uploadImage->images = $image->getClientMimeType();
                $uploadImage->filename = $image->getFilename() . '.' . $extension;
                $uploadImage->listing_id = $request->productId;
                $uploadImage->save();
            }
        }

        return redirect('/agent/inventoryList')->with('success_msg', 'Property Updated successfully!');
    }
}

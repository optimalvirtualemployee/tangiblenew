<?php
namespace App\Http\Controllers;

use App\ContactusModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;
use App\AutomobileBodyTypeModel;
use App\AutomobileImageModel;
use App\AutomobileDataListingModel;
use App\AutomobileFeatureListingModel;
use App\AutomobileFeaturesModel;
use App\BuildYearModel;
use App\CarBrandModel;
use App\CarModel;
use App\CategoriesModel;
use App\CityModel;
use App\ColourModel;
use App\CountryModel;
use App\FuelTypeModel;
use App\PropertyTypeModel;
use App\SavedListingModel;
use App\TransmissionModel;
use App\InteriorColourModel;
use App\AgentImageModel;
use App\CurrencyModel;
use App\UserDetailsModel;
use App\AutomobileSubmitSuggestPriceModel;
use App\EnquiryModel;
use App\MyMessageModel;
use App\ProductOrderModel;
use App\SubmitSuggestPriceModel;
use App\AgentModel;
use App\StateModel;
use Illuminate\Support\Facades\Mail;
use App\User;
use App\Mail\ListingEnquiry;
use Illuminate\Support\Facades\Auth;
use App\CarModelImage;

class CarListingFrontEndController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $contactUs = ContactusModel::get()->first();
        $images = AutomobileImageModel::get();
        $selectedCurrency = CurrencyModel::select('currency_code as code')->where('status', '=', '1')->first();
        
        currency()->setUserCurrency($selectedCurrency->code);
        $propertyType = PropertyTypeModel::get();
        //dd($selectedCurrency->currency_code);
        
        $automobileListing = AutomobileDataListingModel::select('currency_code', 'au_automobile_detail.id as id', 'ad_title', 'build_year', 'neworused', 'value', 'odometer', 'au_transmission.transmission as transmission', 'rhdorlhd', 'au_automobile_detail.created_at as automobilecreationtime', 'tbl_country.filename as filename', 'tbl_country.countrycode as countrycode', 'tbl_agent.id as agent_id', 'tbl_agent.agency_id as agency_id')->join('tbl_currency', 'tbl_currency.id', '=', 'au_automobile_detail.currency_id')
            ->join('tbl_state', 'tbl_state.id', '=', 'au_automobile_detail.state_id')
            ->join('tbl_city', 'tbl_city.id', '=', 'au_automobile_detail.city_id')
            ->join('tbl_country', 'tbl_country.id', '=', 'au_automobile_detail.country_id')
            ->join('tbl_agent', 'tbl_agent.id', '=', 'au_automobile_detail.agent_id')
            ->join('re_build_year', 're_build_year.id', '=', 'au_automobile_detail.year_id')
            ->join('au_transmission', 'au_transmission.id', '=', 'au_automobile_detail.transmission_id')
            ->whereIN('au_automobile_detail.id', function ($query) {
            $query->select('listing_id')
                ->from('au_automobile_images');
        })
            ->whereIN('au_automobile_detail.id', function ($query) {
            $query->select('listing_id')
                ->from('au_automobile_comments');
        })
            ->where('au_automobile_detail.status', '=', '1')
            ->paginate(2);
        //dd(DB::getQueryLog());
        $makes = CarModel::get();
        $models = CarBrandModel::whereIN('au_model.automobile_brand_id', function ($query) {
            $query->select('id')->from('au_brands');
        })->get();
        $transmissions = TransmissionModel::get();
        $fuel_types = FuelTypeModel::get();
        $body_types = AutomobileBodyTypeModel::get();
        $years = BuildYearModel::orderBy('build_year', 'ASC')->get();
        $colours = ColourModel::get();
        $country_data = CountryModel::orderBy('country_name', 'ASC')->get();
        $price_max = currency()->convert(floatval(AutomobileDataListingModel::max('value') + 1000), "USD", currency()->getUserCurrency() ,false);
        
        //dd(currency()->getUserCurrency());
         //dd($automobileListing[0]);
        //dd(currency()->convert($automobileListing[0]->value, $automobileListing[0]->currency_code, 'ZAR'));

        return view('tangiblehtml.car-listing', compact('propertyType','contactUs', 'automobileListing', 'images', 'makes', 'models', 'transmissions', 'fuel_types', 'body_types', 'years', 'colours', 'country_data', 'price_max', 'selectedCurrency'));
    }

    public function searchFilter(Request $request)
    {
        
        //dd($request->all());
        $contactUs = ContactusModel::get()->first();
        $images = AutomobileImageModel::get();
        $selectedCurrency = CurrencyModel::select('currency_code as code')->where('status', '=', '1')->first();
        
        currency()->setUserCurrency($selectedCurrency->code);
        
        $price_max = currency()->convert(floatval(AutomobileDataListingModel::max('value') + 1000), "USD", currency()->getUserCurrency() ,false);
        
        $propertyType = PropertyTypeModel::get();
        
        $params = $request->all();
        $makeSelected = [];
        $modelSelected = [];
        $transmissionSelected = [];
        $condition ="";
        $driveSelected = "";
        $colorSelected = "";
        $fuelSelected = [];
        $bodySelected = [];
        $countrySelected = [];
        $sortByParam = "ASC";
        $sortByField = "id";
        
        foreach($params as $param=>$value){
            
            if(strpos($param, 'make') !== false){
                
                array_push($makeSelected,$value);
            } if(strpos($param, 'model') !== false){
                
                array_push($modelSelected,$value);
            } if(strpos($param, 'transmission') !== false){
                
                array_push($transmissionSelected,$value);
            } if(strpos($param, 'fuel_type') !== false){
                
                array_push($fuelSelected,$value);
            } if(strpos($param, 'body_type') !== false){
                
                array_push($bodySelected,$value);
            } if(strpos($param, 'country') !== false){
                
                array_push($countrySelected,$value);
            }
            
        }
        
        if($request->customRadioInline1 !=null)
            $condition = $request->customRadioInline1;
        
        if($request->color !=null)
            $colorSelected = $request->color;
            
        if($request->gender1 !=null)
            $driveSelected = $request->gender1;
        
            $sortBy = "";
            
            if($request->sortBy !=null){
                if($request->sortBy == 1){
                    $sortByField = 'value';
                    $sortByParam = 'DESC';
                    $sortBy = $request->sortBy;
                }
                if($request->sortBy == 2){
                    $sortByField = 'value';
                    $sortByParam = 'ASC';
                    $sortBy = $request->sortBy;
                }
                if($request->sortBy == 3){
                    $sortByField = 'au_automobile_detail.created_at';
                    $sortByParam = 'DESC';
                    $sortBy = $request->sortBy;
                }
            }
        
        $odoFrom = "";
        $odoTo = "";
          
        if($request->odoFrom != null){
            $odoFrom = $request->odoFrom;
        }if($request->odoTo != null){
            $odoTo = $request->odoTo;
        }
        
        $yearMin ="";
        $yearMax ="";
            
        if($request->fromYear != null){
            $yearMin = $request->fromYear;
         }if($request->toYear != null){
            $yearMax = $request->toYear;
         }
            
        $priceMin = "";
        $priceMax = "";
            
        if($request->minPrice != null){
            $priceMin = currency()->convert(floatval($request->minPrice), currency()->getUserCurrency(), "USD" ,false);
         }if($request->maxPrice != null){
             $priceMax = currency()->convert(floatval($request->maxPrice), currency()->getUserCurrency(), "USD" ,false);
         }
        
         //print_r($request->all());
         
         /* echo $yearMin;
         echo "<br>";
         echo $yearMax;
         dd('---------------'); */
         
         DB::enableQueryLog();
        $automobileListing = AutomobileDataListingModel::select('currency_code', 'au_automobile_detail.id as id', 'ad_title', 'build_year', 'neworused', 'value', 'odometer', 
                                'au_transmission.transmission as transmission', 'rhdorlhd', 'au_automobile_detail.created_at as automobilecreationtime',
                                'tbl_country.filename as filename', 'tbl_country.countrycode as countrycode', 'tbl_agent.id as agent_id', 'tbl_agent.agency_id as agency_id',
                                're_build_year.build_year as build_year')
        ->join('tbl_currency', 'tbl_currency.id', '=', 'au_automobile_detail.currency_id')
        ->join('tbl_state', 'tbl_state.id', '=', 'au_automobile_detail.state_id')
        ->join('tbl_city', 'tbl_city.id', '=', 'au_automobile_detail.city_id')
        ->join('tbl_country', 'tbl_country.id', '=', 'au_automobile_detail.country_id')
        ->join('tbl_agent', 'tbl_agent.id', '=', 'au_automobile_detail.agent_id')
        ->join('re_build_year', 're_build_year.id', '=', 'au_automobile_detail.year_id')
        ->join('au_transmission', 'au_transmission.id', '=', 'au_automobile_detail.transmission_id')
        ->whereIN('au_automobile_detail.id', function ($query) {
            $query->select('listing_id')
            ->from('au_automobile_images');
        })
        /* ->whereIN('au_automobile_detail.id', function($query){
         $query->select('listing_id')
         ->from('au_automobile_features');
         }) */
            ->whereIN('au_automobile_detail.id', function ($query) {
                $query->select('listing_id')
                ->from('au_automobile_comments');
            })->where(function($query) use($makeSelected) {
                foreach($makeSelected as $term) {
                    $query->orWhere('make_id', '=', $term);
                };
            })->where(function($query) use($modelSelected) {
                foreach($modelSelected as $term) {
                    $query->orWhere('model_id', '=', $term);
                };
            })->where(function($query) use($condition) {
                if($condition != null)
                    $query->orWhere('neworused', '=', $condition);
            })->where(function($query) use($transmissionSelected) {
                foreach($transmissionSelected as $term) {
                    $query->orWhere('transmission_id', '=', $term);
                };
            })->where(function($query) use($fuelSelected) {
                foreach($fuelSelected as $term) {
                    $query->orWhere('fuel_type_id', '=', $term);
                };
            })->where(function($query) use($bodySelected) {
                foreach($bodySelected as $term) {
                    $query->orWhere('body_type_id', '=', $term);
                };
            })->where(function($query) use($yearMin,$yearMax) {
                if($yearMax == null && $yearMin != null)
                    $yearMax = BuildYearModel::Select('build_year')->orderBy('build_year','DESC')->first()->build_year;
                    if($yearMin == null && $yearMax !=null)
                        $yearMin = BuildYearModel::Select('build_year')->orderBy('build_year','Asc')->first()->build_year;
                        
                        if($yearMin != null || $yearMax != null)
                            $query->whereBetween('build_year', [
                                $yearMin,
                                $yearMax
                            ]);
                            
            })->where(function($query) use($colorSelected) {
                if($colorSelected != null)
                    $query->orWhere('colour_id', '=', $colorSelected);
            })->where(function($query) use($countrySelected) {
                foreach($countrySelected as $term) {
                    $query->orWhere('au_automobile_detail.country_id', '=', $term);
                };
            })->where(function($query) use($driveSelected) {
                if($driveSelected != null)
                    $query->orWhere('rhdorlhd', '=', $driveSelected);
            })->where(function($query) use($priceMin,$priceMax,$price_max) {
                if(($priceMin != null && $priceMin > 0) || ($priceMax != null && $priceMax < $price_max))
                    $query->whereBetween('value', [
                        $priceMin,
                        $priceMax+1
                    ])->orWhere('value', NULL);
                    
            })->where(function($query) use($odoFrom,$odoTo) {
                if(($odoFrom != null && $odoFrom > 0) || ($odoTo != null))
                    $query->whereBetween('odometer', [
                        $odoFrom,
                        $odoTo+1
                    ]);
                    
            })
            ->where('au_automobile_detail.status', '=', '1')
            ->orderBy($sortByField, $sortByParam)
            ->paginate(2);
            //dd(DB::getQueryLog());
            
            $makes = CarModel::get();
            $models = CarBrandModel::whereIN('au_model.automobile_brand_id', function ($query) {
                $query->select('id')->from('au_brands');
            })->get();
            $transmissions = TransmissionModel::get();
            $fuel_types = FuelTypeModel::get();
            $body_types = AutomobileBodyTypeModel::get();
            $years = BuildYearModel::orderBy('build_year', 'ASC')->get();
            $colours = ColourModel::get();
            $country_data = CountryModel::orderBy('country_name', 'ASC')->get();
            
            $priceMin = $request->minPrice;
            $priceMax = $request->maxPrice;
            
            //echo "<br>";
            
            //dd($automobileListing);
            
            return view('tangiblehtml.car-listing', compact('sortBy','propertyType','price_max','contactUs', 'automobileListing', 'images', 'makes', 'models', 'transmissions', 'fuel_types',
                        'body_types', 'years', 'colours', 'country_data', 'makeSelected', 'modelSelected', 'transmissionSelected', 'condition', 'driveSelected','fuelSelected',
                         'bodySelected', 'countrySelected', 'odoFrom', 'odoTo', 'yearMin', 'yearMax', 'priceMin', 'priceMax', 'colorSelected'));
        
    }
    
    public function search(Request $request)
    {
        $type = $request->type;
        $finalResponse = [];

        if ($type = 'brandFilter') {

            $brandfilter = $request->brandFilter;
            
            if($brandfilter != null){
            $model = CarBrandModel::where(function($query) use($brandfilter) {
                foreach($brandfilter as $term) {
                    $query->orWhere('automobile_brand_id', '=', $term);
                };
            })->get();
            
            $finalResponse['model']= $model;
            }else {
                $model = CarBrandModel::get();
                
                $finalResponse['model']= $model;
                
            }
        }
        
        return response()->json($finalResponse);
    }
    
    public function findYear(Request $request)
    {
        $year = $request->year;
        $finalResponse = [];
        
        $to_year = BuildYearModel::where('build_year', '>', $year)->orderBy('build_year', 'ASC')->get();
        
        $finalResponse['to_year'] = $to_year;
        
        return response()->json($finalResponse);
    }
    
    /**
     * Display the single Car .
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //$property = PropertyDataModel::find($id);
        $productId=Crypt::decrypt($id);
        $id = Crypt::decrypt($id);
        $selectedCurrency = CurrencyModel::select('currency_code as code')->where('status', '=', '1')->first();
        $message = MyMessageModel::get();
        
        currency()->setUserCurrency($selectedCurrency->code);
        $propertyType = PropertyTypeModel::get();

        $category_id = CategoriesModel::select('id')->firstWhere("website_category", "=", 'Automobiles');
        
        $savedListing = SavedListingModel::select('categoryId', 'listingId', 'userId')->where('listingId', '=', $id)
        ->where('categoryId', '=', $category_id->id)->first();
        
        $automobile = AutomobileDataListingModel::select('au_automobile_detail.id as id','currency_code', 'ad_title', 'value', 'automobile_brand_name' , 'automobile_model_name',
                                                        'engine', 'au_transmission.transmission as transmission', 'colour', 'odometer', 'fuel_type', 'engine' , 'build_year as year', 'registration_place',
                                                        'tbl_agencies.first_name as dealer_name', 'dealerCity.city_name as dealer_city', 'dealerCountry.country_name as dealer_country', 'tbl_agencies.created_at as dealer_time', 'automobileCity.city_name as automobile_city',
                                                        'automobileCountry.country_name as automobile_country','interior_colour_id', 'au_price.price as priceType', 'tbl_agent.id as agent_id', DB::raw('CONCAT(tbl_agent.first_name, " ", tbl_agent.last_name) AS agentname'), 'tbl_agent.mobile as agentMobile','tbl_agent.country_id as agentCountry','tbl_agent.city_id as agentCity', 'tbl_agent.state_id as agentState')
            ->join('tbl_currency', 'tbl_currency.id', '=', 'au_automobile_detail.currency_id')
            ->join('au_brands', 'au_brands.id', '=', 'au_automobile_detail.make_id')
            ->join('au_model', 'au_model.id', '=', 'au_automobile_detail.model_id')
            ->join('au_colour', 'au_colour.id', '=', 'au_automobile_detail.colour_id')
            ->join('re_build_year', 're_build_year.id', '=', 'au_automobile_detail.year_id')
            ->join('au_transmission', 'au_transmission.id', '=', 'au_automobile_detail.transmission_id')
            ->join('au_fuel_type', 'au_fuel_type.id', '=', 'au_automobile_detail.fuel_type_id')
            ->join('tbl_agent','tbl_agent.id', '=', 'au_automobile_detail.agent_id')
            ->leftJoin('tbl_agencies','tbl_agencies.id', '=', 'tbl_agent.agency_id')
            ->leftJoin('tbl_city as dealerCity','dealerCity.id', '=', 'tbl_agencies.city_id')
            ->leftJoin('tbl_country as dealerCountry','dealerCountry.id', '=', 'tbl_agencies.country_id')
            ->join('tbl_city as automobileCity','automobileCity.id', '=', 'au_automobile_detail.city_id')
            ->join('tbl_country as automobileCountry','automobileCountry.id', '=', 'au_automobile_detail.country_id')
            ->leftJoin('au_price','au_price.id', '=', 'au_automobile_detail.price_id')
            ->find($id);
            //dd(DB::getQueryLog());
            // dd($automobile);

            $features = AutomobileFeaturesModel::where('listing_id', '=', $automobile->id)->where('feature_available', '=', '1')->get();
            $interior_colour = NULL;
            if($automobile->interior_colour_id != ''){
            $interior_colour = InteriorColourModel::where('id', '=', $automobile->interior_colour_id)->first();
            }
            
            $html = '';
            $i = 1;
            foreach($features as $values){
                
                
                $feature_name = AutomobileFeatureListingModel::select('property_name')->where('id','=',$values->feature_id)->first();
                if($i == 1){
                    $html .= '<ul class="list-style-2">';
                           }
                           
                    $html .= '<li>'.$feature_name->property_name.'</li>';
                            
                    
                    $i++;
                    if($i == 5){
                        $html .= '</ul>';
                            $i = 1;
                    }
            }
            
            //dd($html);
            $images = AutomobileImageModel::where('listing_id', '=', $automobile->id)->get();
            $agentImage = AgentImageModel::where('listing_id', '=', $automobile->agent_id)->first();

            $modelId = AutomobileDataListingModel::where('id',$id)->first();
            $brandId = CarBrandModel::where('id',$modelId->model_id)->first();
            $brandImg = CarModelImage::where('id', $brandId->automobile_brand_id)->first();

            $contactUs = ContactusModel::get()->first();
            $state = StateModel::where('id', '=', $automobile->agentState)->first();
            $country = CountryModel::where('id', '=', $automobile->agentCountry)->first();
            $city = CityModel::where('id', '=', $automobile->agentCity)->first();
            
            
            
            return view('tangiblehtml.car-detail', compact('state','country', 'city', 'agentImage','message','productId','propertyType','automobile', 'images', 'agentImage', 'contactUs','interior_colour', 'html', 'savedListing','brandImg'));
    }
    
    public function suggestPrice($id){
        
    $contactUs = ContactusModel::get()->first();
    $selectedCurrency = CurrencyModel::select('currency_code as code')->where('status', '=', '1')->first();
    currency()->setUserCurrency($selectedCurrency->code);
    $country_data = CountryModel::get();
    $category = "automobile";
    $city_data = CityModel::get();
    $userDetails = UserDetailsModel::get();
    
    $listing = AutomobileDataListingModel::select('au_automobile_detail.id as id','currency_code', 'ad_title', 'value', 'automobile_brand_name' , 'automobile_model_name',
        'engine', 'au_transmission.transmission as transmission', 'colour', 'odometer', 'fuel_type', 'engine' , 'build_year as year', 'registration_place',
        'tbl_agencies.first_name as dealer_name', 'dealerCity.city_name as dealer_city', 'dealerCountry.country_name as dealer_country', 'tbl_agencies.created_at as dealer_time', 'automobileCity.city_name as automobile_city',
        'automobileCountry.country_name as automobile_country','interior_colour_id', 'au_price.price as priceType', 'agent_id')
        ->join('tbl_currency', 'tbl_currency.id', '=', 'au_automobile_detail.currency_id')
        ->join('au_brands', 'au_brands.id', '=', 'au_automobile_detail.make_id')
        ->join('au_model', 'au_model.id', '=', 'au_automobile_detail.model_id')
        ->join('au_colour', 'au_colour.id', '=', 'au_automobile_detail.colour_id')
        ->join('re_build_year', 're_build_year.id', '=', 'au_automobile_detail.year_id')
        ->join('au_transmission', 'au_transmission.id', '=', 'au_automobile_detail.transmission_id')
        ->join('au_fuel_type', 'au_fuel_type.id', '=', 'au_automobile_detail.fuel_type_id')
        ->join('tbl_agent','tbl_agent.id', '=', 'au_automobile_detail.agent_id')
        ->leftJoin('tbl_agencies','tbl_agencies.id', '=', 'tbl_agent.agency_id')
        ->leftJoin('tbl_city as dealerCity','dealerCity.id', '=', 'tbl_agencies.city_id')
        ->leftJoin('tbl_country as dealerCountry','dealerCountry.id', '=', 'tbl_agencies.country_id')
        ->join('tbl_city as automobileCity','automobileCity.id', '=', 'au_automobile_detail.city_id')
        ->join('tbl_country as automobileCountry','automobileCountry.id', '=', 'au_automobile_detail.country_id')
        ->leftJoin('au_price','au_price.id', '=', 'au_automobile_detail.price_id')
        ->find($id);
        $images = AutomobileImageModel::where('listing_id', '=', $listing->id)->get();
        
    return view('tangiblehtml.suggest-price', compact('contactUs', 'listing', 'images', 'country_data', 'category', 'city_data', 'userDetails'));
    }
    
    public function submitSuggestPrice(Request $request){
        
        
        $this->validate($request, [
            'phoneNumber' => "required",
            'address' => "required",
            'country' => "required",
            'city' => "required",
            'postalCode' => "required",
            'suggestedPrice' => "required"
        ]);
        
        $updateUser = UserDetailsModel::where('user_id', '=', $request->userId)->first();
        
        if($updateUser != null){
            $updateUser->user_id = $request->userId;
            $updateUser->user_postalCode = $request->postalCode;
            $updateUser->user_mobile = $request->phoneNumber;
            $updateUser->city_id = $request->city;
            $updateUser->country_id = $request->country;
            $updateUser->user_address = $request->address;
            
            $updateUser->save();
        }else{
            $updateUser = new UserDetailsModel();
            
            $updateUser->user_id = $request->userId;
            $updateUser->user_postalCode = $request->postalCode;
            $updateUser->user_mobile = $request->phoneNumber;
            $updateUser->city_id = $request->city;
            $updateUser->country_id = $request->country;
            $updateUser->user_address = $request->address;
            
            $updateUser->save();
        }
        
        $suggestPrice = SubmitSuggestPriceModel::where('user_id', '=', $request->userId)->where('product_id', '=', $request->productId)->where('Category', '=', 'Automobile')->first();
        
        $agent = AgentModel::find($request->agentId);
        
        if($suggestPrice == null){
            $suggestPrice = new SubmitSuggestPriceModel();
        $suggestPrice->product_id = $request->productId;
        $suggestPrice->user_Id = $request->userId;
        $suggestPrice->agent_Id = $request->agentId;
        $suggestPrice->category = 'Automobile';
        $suggestPrice->submit_price = $request->suggestedPrice;
        $suggestPrice->shipping_cost = $request->shippingPrice;
        $suggestPrice->agent_userid = $agent->userId;
        
        $suggestPrice->save();
        
        }else if($suggestPrice != null && $suggestPrice->submit_price != null){
            
            return back()->with('success_msg','You already make an offer for this listing');
        }else if($suggestPrice != null && $suggestPrice->submit_price == null){
            
            $suggestPrice = SubmitSuggestPriceModel::find($suggestPrice->id);
            
            $suggestPrice->product_id = $request->productId;
            $suggestPrice->user_Id = $request->userId;
            $suggestPrice->agent_Id = $request->agentId;
            $suggestPrice->category = 'Automobile';
            $suggestPrice->submit_price = $request->suggestedPrice;
            $suggestPrice->shipping_cost = $request->shippingPrice;
            $suggestPrice->agent_userid = $agent->userId;
            
            $suggestPrice->save();
        }
        
        $enqury = EnquiryModel::where('userID', '=', $request->userId)->where('productId', '=', $request->productId)->where('Category', '=', 'Automobile')->first();
        
        if($enqury == null){
            $enqury = new EnquiryModel;
            $enqury->id = $suggestPrice->id;
            $enqury->name = $updateUser->first_name .' '. $updateUser->last_name;
            $enqury->phone = $updateUser->user_mobile;
            $enqury->queryType = $request->querytype;
            $enqury->message = $request->message;
            $enqury->Category = 'Automobile';
            $enqury->productId = $request->productId;
            $enqury->agent_id = $request->agentId;
            $enqury->agencies_id = $agent->agency_id;
            $enqury->userID = $request->userId;
            $enqury->agent_userid = $agent->userId;
            $enqury->save();
            
        }
        
        
        $productorder = ProductOrderModel::where('userID', '=', $request->userId)->where('productId', '=', $request->productId)->where('Category', '=', 'Automobile')->first();
        
        if($productorder == null){
            
            $order = new ProductOrderModel();
            $order->id = $suggestPrice->id;
            $order->name = $updateUser->first_name .' '. $updateUser->last_name;
            $order->user_emailid = $updateUser->email;
            $order->phone = $updateUser->user_mobile;
            $order->country = $request->country;
            $order->city = $request->city;
            $order->category = 'Automobile';
            $order->productId = $request->productId;
            $order->agencies_id = $agent->agencies_id;
            $order->agent_id = $request->agentId;
            $order->userId = $request->userId;
            $order->agent_userid = $agent->userId;
            $order->order_status = 'waiting for approval';
            
            $order->save();
        }
        
        
        $seller_category = 'private_seller';
        if($agent->agency_id != null)
            $seller_category = 'agent';
        
        $myMessage = new MyMessageModel();
        
        $myMessage->message = $request->message;
        $myMessage->message_id = $suggestPrice->id;
        $myMessage->queryType = 'submitprice';
        $myMessage->category = 'Automobile';
        $myMessage->agent_id = $request->agentId;
        $myMessage->product_id = $request->productId;
        $myMessage->messageFrom = $request->userId;
        $myMessage->messageTo = $agent->userId;
        
        
        $myMessage->save();
        
        return back()->with('success_msg','Make an Offer request send successfully');
    }
    
    public function sendenquery(Request $request){
        
        
        $this->validate($request, [
            'name'  => 'required|min:3',
            'mobile'  => 'required|min:10',
            'message'  => 'required'
        ]);
        
        $agent = AgentModel::find($request->agentId);

       
        
        $enqury = EnquiryModel::where('userID', '=', $request->userId)->where('productId', '=', $request->productid)->where('Category', '=', 'Automobile')->first();
        
       

        if($enqury == null){
        $enqury = new EnquiryModel;
        $enqury->name = $request->name;
        $enqury->phone = $request->mobile;
        $enqury->country = $request->country;
        $enqury->queryType = $request->querytype;
        $enqury->message = $request->message;
        $enqury->Category = 'Automobile';
        $enqury->productId = $request->productid;
        $enqury->agent_id = $request->agentId;
        $enqury->agencies_id = $agent->agency_id;
        $enqury->userID = $request->userId;
        $enqury->agent_userid = $agent->userId;
        $enqury->save();
        
        }

        
        
        $suggestPrice = SubmitSuggestPriceModel::where('user_id', '=', $request->userId)->where('product_id', '=', $request->productid)->where('Category', '=', 'Automobile')->first();
        
        
        if($suggestPrice == null){
            $suggestPrice = new SubmitSuggestPriceModel();
            $suggestPrice->id = $enqury->id;
            $suggestPrice->product_id = $request->productid;
            $suggestPrice->user_Id = $request->userId;
            $suggestPrice->agent_Id = $request->agentId;
            $suggestPrice->category = 'Automobile';
            $suggestPrice->submit_price = $request->suggestedPrice;
            $suggestPrice->shipping_cost = $request->shippingPrice;
            $suggestPrice->agent_userid = $agent->userId;
            
            $suggestPrice->save();
            
        }


        //dd($enqury);
        
        $productorder = ProductOrderModel::where('userID', '=', $request->userId)->where('productId', '=', $request->productid)->where('Category', '=', 'Automobile')->first();
        
        if($productorder == null){
            
            $order = new ProductOrderModel();
            $order->id = $enqury->id;
            $order->name = $request->name;
            $order->user_emailid = $request->email;
            $order->phone = $request->mobile;
            $order->country = $request->country;
            $order->city = $request->city;
            $order->category = 'Automobile';
            $order->productId = $request->productid;
            $order->agencies_id = $agent->agencies_id;
            $order->agent_id = $request->agentId;
            $order->userId = $request->userId;
            $order->agent_userid = $agent->userId;
            $order->order_status = 'waiting for approval';
            
            $order->save();
        }
        
        $seller_category = 'private_seller';
        if($agent->agency_id != null)
            $seller_category = 'agent';
            
            $myMessage = new MyMessageModel();
            $myMessage->message = $request->message;
            $myMessage->message_id = $enqury->id;
            $myMessage->queryType = 'enquiry';
            $myMessage->category = 'Automobile';
            $myMessage->agent_id = $request->agentId;
            $myMessage->product_id = $request->productid;
            $myMessage->messageFrom = $request->userId;
            $myMessage->messageTo = $agent->userId;
            
            $myMessage->save();
            
            $user = User::find($request->userId);
            
          //  Mail::to('priyank@optimalvirtualemployee.com')->send(new ListingEnquiry($user, $agent,$enqury ));
            
            /*Mail::send('email.contact-seller', [ 'user' =>$user ,'agent' =>  $agent, 'enqury' =>$enqury], function($message) {
                $message->to('priyank@optimalvirtualemployee.com', 'Priyank Sharma')->subject
                ('Enquiry');
                $message->from('pashu.sharma@gmail.com','Tangible Listing');
            });*/
            
        
        return back()->with('success_msg','Enquiry send Successfully');
        
    }
}

<?php
    
namespace App\Http\Controllers;
    
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use App\UserPermissionModel;
    
class UserController extends Controller
{
    
    
    function __construct()
    {
        $this->middleware('permission:userManagement-list|userManagement-create|userManagement-edit|userManagement-delete', ['only' => ['index','store']]);
        $this->middleware('permission:userManagement-create', ['only' => ['create','store']]);
        $this->middleware('permission:userManagement-edit', ['only' => ['edit','update']]);
        $this->middleware('permission:userManagement-delete', ['only' => ['destroy']]);
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data = User::orderBy('id','DESC')->paginate(5);
        
        return view('users.index',compact('data'))
            ->with('i', ($request->input('page', 1) - 1) * 5);
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = Role::get();
        $permission = Permission::get();
        
        return view('users.createmsf',compact('roles', 'permission'));
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
       $request->validate([
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required|email|unique:users,email',
           'password' => 'required|same:confirmpassword',
           'roles' => 'required'
           
       ]);
    
        
        $input = $request->all();
        $input['password'] = Hash::make($input['password']);
    
        $user = User::create($input);
        $user->assignRole($request->input('roles'));
        
        $userId = $user->id;
        
        if($request->input('permission')){
        foreach ($request->input('permission') as $permission){
            
            $userPermission = new UserPermissionModel();
            
            $userPermission->userId = $userId;
            $userPermission->permissionId = $permission;
            
            $userPermission->save();
        }
        }
        
        return redirect()->route('users.index')
                        ->with('success','User created successfully');
    }
    
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::find($id);
        return view('users.show',compact('user'));
    }
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id);
        $roles = Role::get();
        $userRole = $user->roles->pluck('name')->all();
        $permission = Permission::get();
        $rolePermissions = DB::table("tbl_user_permissions")->where("tbl_user_permissions.userId",$id)
        ->pluck('tbl_user_permissions.permissionId','tbl_user_permissions.permissionId')
        ->all();
        
        /* print_r($userRole[0]);
        echo "<br>";
        foreach ($roles as $role){
            echo $role->name;
            echo "<br>";
        }
        
        dd(); */
    
        return view('users.editmsf',compact('user','roles','userRole', 'permission', 'rolePermissions'));
    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required|email|unique:users,email,'.$id,
            'roles' => 'required'
        ]);
    
        $input['first_name'] = $request->input('first_name');
        $input['last_name'] = $request->input('last_name');
        $input['email'] = $request->input('email');
        
        if(!empty($request->input('password'))){ 
            $input['password'] = Hash::make($request->input('password'));
        }
    
        $user = User::find($id);
        $user->update($input);
        
        DB::table('model_has_roles')->where('model_id',$id)->delete();
    
        $user->assignRole($request->input('roles'));
    
        UserPermissionModel::where('userId', '=', $id)->delete();

        if($request->input('permission')){
        foreach ($request->input('permission') as $permission){
            
            $userPermission = new UserPermissionModel();
            
            $userPermission->userId = $id;
            $userPermission->permissionId = $permission;
            
            $userPermission->save();
        }
        }
        return redirect()->route('users.index')
                        ->with('success','User updated successfully');
    }
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        User::find($id)->delete();
        return redirect()->route('users.index')
                        ->with('success','User deleted successfully');
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\AgenciesModel;
use App\CountryModel;
use App\StateModel;
use App\CityModel;

class DealerManagementController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index(){
        
        $dealerId = Auth::user()->id;
        
        $agency = AgenciesModel::where('userId', '=', $dealerId)->first();
        
        $dealer = AgenciesModel::select('tbl_category.website_category as category')
        ->where('userId', '=', Auth::user()->id)
        ->join('tbl_category', 'tbl_category.id', 'tbl_agencies.category_id')
        ->first();
        
        
        $countrySelected = CountryModel::find($agency->country_id);
        $stateSelected = StateModel::find($agency->state_id);
        $citySelected = CityModel::find($agency->city_id);
        
        $countryData = CountryModel::get();
        $stateData = StateModel::get();
        $cityData = CityModel::get();
        //dd($agency);
        
        return view('dealer.dealer-management', compact('agency', 'countrySelected', 'stateSelected', 'countryData', 'stateData', 'dealer'));
    }
    
    public function update(Request $request){

        $updateAgencies = AgenciesModel::find($request->agencyId);
        $this->validate($request, [
            'company_name' => 'required',
            'email' => 'required|email|unique:users,email,'. $updateAgencies->userId,
            'countryId' => 'required',
            'stateId' => 'required',
            'street_1' => 'required',
            'street_2' => 'required',
            'postal_code' => 'required',
            'mobile' => 'required',
        ]);
        
        
        $updateAgencies->country_id = $request->input('countryId');
        $updateAgencies->company_name = $request->input('company_name');
        if($request->input('stateId') != '')
            $updateAgencies->state_id = $request->input('stateId');
        $updateAgencies->street_1 = $request->input('street_1');
        $updateAgencies->street_2 = $request->input('street_2');
        $updateAgencies->postal_code = $request->input('postal_code');
        $updateAgencies->phone_no = $request->input('mobile');
        $updateAgencies->email = $request->input('email');

        $updateAgencies->save();
        
        return redirect('/dealer/dealerManagement')->with('success_msg', 'Dealer Detail Updated successfully!');
    }
}

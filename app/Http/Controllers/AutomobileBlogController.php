<?php

namespace App\Http\Controllers;

use App\AutomobileBlogModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use App\AutomobileBlogImageModel;

class AutomobileBlogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $blogs = AutomobileBlogModel::get();
        
        return view('admin.automobile.blog_msf.index')->with(array(
            'blogs' => $blogs
        ));
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
        return view('admin.automobile.blog_msf.create');
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
       $data = $request->validate([
            
            
            'title' => ['required'],
            'blogPost' => ['required'],
            'blogType' => ['required'],
            'status' => ['required'],
            'photos.*' => 'image|mimes:jpeg,png,jpg,gif,svg'
        ]);
        
        
        $createAutomobileBlog = new AutomobileBlogModel();
        
        $createAutomobileBlog->title = $request->input('title');
        $createAutomobileBlog->blogPost = $request->input('blogPost');
        $createAutomobileBlog->blogType = $request->input('blogType');
        if($request->input('url') != null)
        $createAutomobileBlog->url = $request->input('url');
        $createAutomobileBlog->status = $request->input('status');
        
        $createAutomobileBlog->save();
        
        $listing_id = $createAutomobileBlog->id;
        
        if($request->file('photos') ?? ''){
            foreach ($request->file('photos') as $image){
                $uploadImage = new AutomobileBlogImageModel();
                
                $extension = $image->getClientOriginalExtension();
                Storage::disk('public')->put($image->getFilename().'.'.$extension,  File::get($image));
                
                $uploadImage->images = $image->getClientMimeType();
                $uploadImage->filename = $image->getFilename().'.'.$extension;
                $uploadImage->listing_id = $listing_id;
                $uploadImage->save();
            }
        }
        
        return redirect('/admin/automobileblog')->with('success_msg', 'Automobile Blog Created successfully!');
        
    }
    
    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
        $blog = AutomobileBlogModel::find($id);
        
        $images_data = AutomobileBlogImageModel::where('listing_id', '=', $id)->get();
        
        return view('admin.automobile.blog_msf.edit', compact('blog', 'images_data'));
    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->validate([
            
            
            'title' => ['required','unique:au_blog,title,' . $id],
            'blogPost' => ['required'],
            'blogType' => ['required'],
            'status' => ['required'],
            'photos.*' => 'image|mimes:jpeg,png,jpg,gif,svg'
        ]);
        
        $updateAutomobileBlog = AutomobileBlogModel::find($id);
        
        $updateAutomobileBlog->title = $request->input('title');
        $updateAutomobileBlog->blogPost = $request->input('blogPost');
        $updateAutomobileBlog->blogType = $request->input('blogType');
        $updateAutomobileBlog->url = $request->input('url');
        $updateAutomobileBlog->status = $request->input('status');
        
        $updateAutomobileBlog->save();
        
        if($request->file('photos') ?? ''){
            foreach ($request->file('photos') as $image){
                $uploadImage = new AutomobileBlogImageModel();
                
                $extension = $image->getClientOriginalExtension();
                Storage::disk('public')->put($image->getFilename().'.'.$extension,  File::get($image));
                
                $uploadImage->images = $image->getClientMimeType();
                $uploadImage->filename = $image->getFilename().'.'.$extension;
                $uploadImage->listing_id = $id;
                $uploadImage->save();
            }
        }
        
        return redirect('/admin/automobileblog')->with('success_msg', 'Automobile Blog Updated successfully!');
        
    }
    
    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deleteRecords = AutomobileBlogModel::find($id)->delete();
        return redirect('/admin/automobileblog')->with('success_msg', 'Automobile Blog Deleted successfully!');
        
    }
}

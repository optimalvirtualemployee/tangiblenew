<?php

namespace App\Http\Controllers;

use App\FunctionGMTModel;
use Illuminate\Http\Request;

class FunctionGMTController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $functions = FunctionGMTModel::get();
        return view('admin.watch.functionGMT.index',compact('functions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
         return view('admin.watch.functionGMT.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
         $validator = $this->validate($request, [
            "GMT" => 'required|unique:wa_factory_features_function_gmt',
             "status" => 'required'
        ]);

          $case = FunctionGMTModel::create($validator);
          $case->save();
       
      return redirect('/admin/functionGMT')->with('success_msg','GMT Created successfully!');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $functions_data = FunctionGMTModel::find($id);
        return view('admin.watch.functionGMT.edit', compact('functions_data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = $this->validate($request, [
            "GMT" => 'required|unique:wa_factory_features_function_gmt',
            "status" => 'required'
        ]);

          $case = FunctionGMTModel::find($id);
          $case->GMT = $request->input('GMT');
          $case->status = $request->input('status');
          $case->save();

          return redirect('/admin/functionGMT')->with('success_msg','GMT Updated successfully!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deleteRecords = FunctionGMTModel::find($id)->delete();
        return redirect('/admin/functionGMT')->with('success_msg','GMT Deleted successfully!');
    }
}

<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Session;
use App\User;
class DealerLoginController extends Controller
{
    public function login() {
        return view('dealer.login');
    }
    public function register(){
        return view('dealer.register');
    }
    function checklogin(Request $request) {
			$this->validate($request, [
				'email'   => 'required|email',
				'password'  => 'required|min:3'
			]);
			$user_data = array(
				'email'  => $request->post('email'),
				'password' => $request->post('password'),
                'usertype' =>'2'
			);
			
		$remember_me = 'false';	
		
		if($request->post('rememberMe') == 'true')
		    $remember_me = 'true';
		    
		if(Auth::attempt($user_data, $remember_me)){

            if(Auth::user()->status=='1'){
                return redirect('/dealer/dashboard');
            }
            else{
                return back()->withErrors(['error' => ['User is not active.']]);
            }

		} else {
            return back()->withErrors(['error' => ['Invalid user name or password.']]);
		}
    }
    public function newDealerRegister(Request $request){
        $this->validate($request, [
            'first_name' => ['required', 'string', 'max:255'],
            'last_name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'phone' => ['required', 'min:10', 'max:12'],
            'password' => ['required', 'string', 'min:8'],
            'i_agree'   =>['required'],
        ]);

        $regUser = new User;
        $regUser->usertype  =  2;
        $regUser->first_name  =  $request->first_name;
        $regUser->last_name  =   $request->last_name;
        $regUser->phone =   $request->phone;
        $regUser->email  =   $request->email;
        $regUser->password  =   Hash::make($request->password);
        $regUser->save();
            $user_data = array(
                'email'  => $request->post('email'),
                'password' => $request->post('password'),
                'usertype' =>'2'
            );
        Auth::attempt($user_data);
        return redirect('/dealer/dashboard');
    }

    function successlogin()  {
     	return view('dashbord');
    }

    function logout()  {
	    Auth::logout();
	    return redirect('main');
    }
}

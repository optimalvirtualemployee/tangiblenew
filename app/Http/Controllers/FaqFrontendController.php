<?php

namespace App\Http\Controllers;

use App\FaqModel;
use App\FaqCategoryModel;
use App\FaqSubCategoryModel;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FaqFrontendController extends Controller
{
    public function index(){
    	$title          = 'FAQ';
    	$Categories     = FaqCategoryModel::where('status','1')->get();
    	return view('tangiblehtml.faqfrontend.index',compact('Categories','title'));
    }

    public function getSubCat($slug){
    	$title     = 'FAQ Sub Category';
    	$getCatID  = FaqCategoryModel::where('slug',$slug)->where('status','1')->get()->first();
    	$CatName   = $getCatID->name;
    	$slug      = $getCatID->slug;
    	$getSubCat = FaqSubCategoryModel::Where('categoryId',$getCatID->id)->where('status','1')->get();
    	return view('tangiblehtml.faqfrontend.subCat',compact('CatName','getSubCat','slug','title'));	
    }

    public function getFaq($slug,$cat_id,$subcat_id){

    	$title = 'FAQ Page';
    	$FAQListing = FaqModel::where('categoryId',$cat_id)->where('subcategoryId',$subcat_id)->where('status','1')->get();
    	$getCatName = FaqCategoryModel::where('id',$cat_id)->where('status','1')->get()->first();
    	$getSubCatName = FaqSubCategoryModel::where('id',$subcat_id)->where('status','1')->get()->first();
    	return view('tangiblehtml.faqfrontend.faq',compact('FAQListing','getCatName','getSubCatName','title'));	

    }
}

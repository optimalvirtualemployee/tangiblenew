<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\AutomobileFeatureListingModel;

class AutomobileFeatureListingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $details = AutomobileFeatureListingModel::get();
        
        return view('admin.automobile.featureslisting.index', compact('details'));
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.automobile.featureslisting.create');
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->validate([
            
            
            'propertyName' => ['required','unique:au_automobiles_features_listing,property_name'],
            'status' => ['required']
        ]);
        
        $createFeature = new AutomobileFeatureListingModel();
        
        $createFeature->property_name = $request->input('propertyName');
        $createFeature->status = $request->input('status');
        
        $createFeature->save();
        
        return redirect('/admin/automobilefeaturelisting')->with('success_msg', 'Automobile Feature Added successfully!');
    }
    
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $feature_data = AutomobileFeatureListingModel::find($id);
        
        return view('admin.automobile.featureslisting.edit',compact('feature_data'));
    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->validate([
            
            
            'propertyName' => ['required','unique:au_automobiles_features_listing,property_name,'.$id],
            'status' => ['required']
        ]);
        
        $updateFeature = AutomobileFeatureListingModel::find($id);;
        
        $updateFeature->property_name = $request->input('propertyName');
        $updateFeature->status = $request->input('status');
        
        $updateFeature->save();
        
        return redirect('/admin/automobilefeaturelisting')->with('success_msg', 'Automobile Feature Updated successfully!');
    }
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deleteRecords = AutomobileFeatureListingModel::find($id)->delete();
        return redirect('/admin/automobilefeaturelisting')->with('success_msg','Automobile Feature deleted successfully!');
    }
}

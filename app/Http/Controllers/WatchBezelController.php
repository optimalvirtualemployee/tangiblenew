<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\WatchBezelModel;

class WatchBezelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $bezel = WatchBezelModel::get();
        return view('admin.watch.bezel.index',compact('bezel'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.watch.bezel.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
        $validate = $this->validate($request, [
            'bezel_material' => 'required|unique:wa_bezelmaterial',
            'status' => 'required'
        ]);

        $bezel = WatchBezelModel::create($validate);
        $bezel->save();
        return redirect('/admin/bezel')->with('success_msg','bezel Created successfully!');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $bezel_data = WatchBezelModel::find($id);
        return view('admin.watch.bezel.edit', compact('bezel_data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // dd($request->all());
        $validate = $this->validate($request, [
            'bezel_material' => 'required|unique:wa_bezelmaterial'
        ]);

        $bezelUpdate = WatchBezelModel::find($id);

        $bezelUpdate->bezel_material = $request->input('bezel_material');

        $bezelUpdate->save();

        return redirect('/admin/bezel')->with('success_msg','bezel Updated successfully!');          
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deleteRecords = WatchBezelModel::find($id)->delete();
        return redirect('/admin/bezel')->with('success_msg','bezel deleted successfully!');
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ConditionsModel;

class ConditionsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $conditions = ConditionsModel::get();
        return view('admin.watch.conditions.index',compact('conditions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.watch.conditions.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validate = $this->validate($request, [
            'conditions' => 'required|unique:wa_conditions',
            'status' => 'required'
        ]);

        $conditions = ConditionsModel::create($validate);
        $conditions->save();
        return redirect('/admin/conditions')->with('success_msg','Condition Created successfully!');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $conditions_data = ConditionsModel::find($id);
        return view('admin.watch.conditions.edit', compact('conditions_data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validate = $this->validate($request, [
            'conditions' => 'required|unique:wa_conditions'
        ]);

        $conditionsUpdate = ConditionsModel::find($id);

        if ($conditionsUpdate->conditions == $validate['conditions']) {
            $conditionsUpdate->status = $request->input('status');
        } else {
            $validate = $this->validate($request, ['conditions' => 'unique:wa_conditions']);
            $conditionsUpdate->conditions = $validate['conditions'];
            $conditionsUpdate->status = $request->input('status');
        }

        $conditionsUpdate->save();

        return redirect('/admin/conditions')->with('success_msg','Condition Updated successfully!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deleteRecords = ConditionsModel::find($id)->delete();
        return redirect('/admin/conditions')->with('success_msg','Condition deleted successfully!');
    }
}

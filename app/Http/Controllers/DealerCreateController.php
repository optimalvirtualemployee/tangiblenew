<?php

namespace App\Http\Controllers;

use App\ContactusModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class DealerCreateController extends Controller
{
    public function index(){
        
        $contactUs = ContactusModel::get()->first();
        
        return view('tangiblehtml.become-a-seller', compact('contactUs'));
    }
    
    public function sendDealerInfo(Request $request){
        
        $this->validate($request, [
            'name' => 'required',
            'phone_number' => 'required',
            'email' => 'required',
            'company_name' => 'required',
            'category' => 'required'
        ]);
        
        $data = array('name'=>$request->input('name'),
            'phone_number'=>$request->input('phone_number'),
            'email'=>$request->input('email'),
            'company_name'=>$request->input('company_name'),
            'category'=>$request->input('category')
        );
        
        try{
           
        Mail::send(['text'=>'dealerinfo'], $data, function($message) {
            $message->to('rajat@optimalvirtualeployee.com', 'Tanible Listing')->subject
            ('Dealer Info For Registraion');
            $message->from('rajat@optimalvirtualemployee.com','Dealer Info');
        });
        
        }
        catch(\Exception $e){
          // dd("Error in sending email");
        }
       
        
        session(['url.intended' => url()->previous()]);
        return redirect(session()->get('url.intended'))->with('success_msg','Your Contact Info received, we will get back to you shortly!!');
    }
}

<?php

namespace App\Http\Controllers;

use App\FunctionYearModel;
use Illuminate\Http\Request;

class FunctionYearController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $functions = FunctionYearModel::get();
        return view('admin.watch.functionYear.index',compact('functions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
         return view('admin.watch.functionYear.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
         $validator = $this->validate($request, [
            "year" => 'required|unique:wa_factory_features_function_year,year',
             "status" => 'required'
        ]);

          $case = FunctionYearModel::create($validator);
          $case->save();
       
      return redirect('/admin/functionYear')->with('success_msg','Year Created successfully!');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $functions_data = FunctionYearModel::find($id);
        return view('admin.watch.functionYear.edit', compact('functions_data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = $this->validate($request, [
            "year" => 'required|unique:wa_factory_features_function_year,year,'. $id,
            "status" => 'required'
        ]);

          $case = FunctionYearModel::find($id);
          $case->year = $request->input('year');
          $case->status = $request->input('status');
          $case->save();
        
        return redirect('/admin/functionYear')->with('success_msg','Year Updated successfully!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deleteRecords = FunctionYearModel::find($id)->delete();
        return redirect('/admin/functionYear')->with('success_msg','Year Deleted successfully!');
    }
}

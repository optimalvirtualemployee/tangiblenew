<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\PriceModel;

class PriceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $price = PriceModel::get();
        return view('admin.watch.price.index',compact('price'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.watch.price.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validate = $this->validate($request, [
            'price' => 'required|unique:wa_price',
            'status' => 'required'
        ]);

        $price = PriceModel::create($validate);
        $price->save();
        return redirect('/admin/price')->with('success_msg','Price Created successfully!');
    }

  
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
         $price_data = PriceModel::find($id);
        return view('admin.watch.price.edit', compact('price_data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validate = $this->validate($request, [
            'price' => 'required'
        ]);

        $priceUpdate = PriceModel::find($id);

        if ($priceUpdate->price == $validate['price']) {
            $priceUpdate->status = $request->input('status');
        } else {
            $validate = $this->validate($request, ['price' => 'unique:wa_price']);
            $priceUpdate->price = $validate['price'];
            $priceUpdate->status = $request->input('status');
        }

        $priceUpdate->save();

        return redirect('/admin/price')->with('success_msg','Price Updated successfully!'); 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deleteRecords = PriceModel::find($id)->delete();
        return redirect('/admin/price')->with('success_msg','Price deleted successfully!');
    }
}

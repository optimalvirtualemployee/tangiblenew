<?php

namespace App\Http\Controllers;

use App\FunctionDoubleChronographModel;
use Illuminate\Http\Request;

class FunctionDoubleChronographController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $functions = FunctionDoubleChronographModel::get();
        return view('admin.watch.functionDoubleChronograph.index',compact('functions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
         return view('admin.watch.functionDoubleChronograph.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
         $validator = $this->validate($request, [
            "double_chronograph" => 'required|unique:wa_factory_features_function_doubleChronograph,double_chronograph',
             "status" => 'required'
        ]);

          $case = FunctionDoubleChronographModel::create($validator);
          $case->save();
       
      return redirect('/admin/functionDoubleChronograph')->with('success_msg','Double Chronograph Created successfully!');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $functions_data = FunctionDoubleChronographModel::find($id);
        return view('admin.watch.functionDoubleChronograph.edit', compact('functions_data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = $this->validate($request, [
            "double_chronograph" => 'required|unique:wa_factory_features_function_doubleChronograph,double_chronograph,'.$id,
            "status" => 'required'
        ]);

          $case = FunctionDoubleChronographModel::find($id);
          $case->double_chronograph = $request->input('double_chronograph');
          $case->status = $request->input('status');
          $case->save();

          return redirect('/admin/functionDoubleChronograph')->with('success_msg','Double Chronograph Updated successfully!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deleteRecords = FunctionDoubleChronographModel::find($id)->delete();
        return redirect('/admin/functionDoubleChronograph')->with('success_msg','Double Chronograph Deleted successfully!');
    }
}

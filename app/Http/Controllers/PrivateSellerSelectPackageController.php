<?php

namespace App\Http\Controllers;

use App\PackageForPrivateSellersModel;
use App\PackageListingRangeManagementModel;
use Illuminate\Http\Request;
use App\ContactusModel;
use App\CurrencyModel;
use App\PropertyTypeModel;

class PrivateSellerSelectPackageController extends Controller
{
    public function index(){
        
        $contactUs = ContactusModel::get()->first();
        $propertyType = PropertyTypeModel::get();
        
        $selectedCurrency = CurrencyModel::select('currency_code as code')->where('status', '=', '1')->first();
        currency()->setUserCurrency($selectedCurrency->code);
        
        $pricing_data = PackageListingRangeManagementModel::Orderby('rangeFrom')->distinct()->get(['rangeFrom', 'rangeTo']);
        
        $value_data = PackageListingRangeManagementModel::select('tbl_package_pricing_range_private_seller.id as id','package_id','package_for_private_sellers as package', 'rangeFrom', 'rangeTo', 'pricing')
        ->join('tbl_package_for_private_sellers', 'tbl_package_for_private_sellers.id', '=', 'tbl_package_pricing_range_private_seller.package_id')
        ->get();
        
        dd($value_data);
        
        return view('privateSeller.select-package' ,compact('pricing_data', 'value_data', 'contactUs', 'propertyType'));
    }
}

<?php

namespace App\Http\Controllers;

use App\CaseMMModel;
use Illuminate\Http\Request;

class CaseMMController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $case = CaseMMModel::get();
        return view('admin.watch.caseMM.index',compact('case'));
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.watch.caseMM.create');
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = $this->validate($request, [
             "case_mm"  => "required|unique:wa_factory_features_case_mm|regex:/^\d+(\.\d{1,2})?$/",
            "status" => 'required'
             
        ]);
        
        $case = CaseMMModel::create($validator);
        $case->save();
        
        return redirect('/admin/caseMM')->with('success_msg','Case MM Created successfully!');
    }
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $case_data = CaseMMModel::find($id);
        return view('admin.watch.caseMM.edit', compact('case_data'));
    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = $this->validate($request, [
            "case_mm"  => "required|regex:/^\d+(\.\d{1,2})?$/",
            "status" => 'required'
        ]);
        
        $case = CaseMMModel::find($id);
        
        $case->case_mm = $validator['case_mm'];
        $case->status = $validator['status'];
        
        $case->save();
        return redirect('/admin/caseMM')->with('success_msg','Case MM Updated successfully!');
    }
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deleteRecords = CaseMMModel::find($id)->delete();
        return redirect('/admin/caseMM')->with('success_msg','Case MM Deleted successfully!');
    }
}

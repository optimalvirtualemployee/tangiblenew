<?php

namespace App\Http\Controllers;

use App\ContactusModel;
use App\PropertyTypeModel;
use Illuminate\Http\Request;

class PrivateSellerCategorySelectController extends Controller
{
    public function index(){
        
        $contactUs = ContactusModel::get()->first();
        $propertyType = PropertyTypeModel::get();
        
        return view('privateSeller.create-private-listing', compact('contactUs', 'propertyType'));
    }
}

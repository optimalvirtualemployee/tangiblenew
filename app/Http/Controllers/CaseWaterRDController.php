<?php

namespace App\Http\Controllers;

use App\CaseWaterRDModel;
use Illuminate\Http\Request;

class CaseWaterRDController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $case = CaseWaterRDModel::get();
        return view('admin.watch.caseWRD.index',compact('case'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.watch.caseWRD.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = $this->validate($request, [
            'water_resistant_depth' => "required|:wa_factory_features_case_waterRD",
            'status' => 'required'
        ]);

          $case = CaseWaterRDModel::create($validator);
          $case->save();
       
      return redirect('/admin/caseWRD')->with('success_msg','Case Water Resistance Depth Created successfully!');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $case_data = CaseWaterRDModel::find($id);
        return view('admin.watch.caseWRD.edit', compact('case_data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = $this->validate($request, [
            'water_resistant_depth' => "required",
            'status' => 'required'
        ]);

        $case = CaseWaterRDModel::find($id);

        $case->water_resistant_depth = $validator['water_resistant_depth'];
        $case->status = $validator['status'];
            
        $case->save();
        return redirect('/admin/caseWRD')->with('success_msg','Case Water Resistance Depth Updated successfully!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deleteRecords = CaseWaterRDModel::find($id)->delete();
        return redirect('/admin/caseWRD')->with('success_msg','Case Water Resistance Depth Deleted successfully!');
    }

}

<?php

namespace App\Http\Controllers;

use App\FaqModel;
use App\FaqCategoryModel;
use App\FaqSubCategoryModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class FaqController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $Categories = FaqCategoryModel::where('status','1')->get();
        $SubCategories = FaqSubCategoryModel::where('status','1')->get();
        $FAQListing = FaqModel::get();
        return view('admin.admincommon.faq.faq.index', compact('FAQListing','Categories','SubCategories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $Categories = FaqCategoryModel::where('status','1')->get();
        $SubCategories = FaqSubCategoryModel::where('status','1')->get();
        return view('admin.admincommon.faq.faq.create',compact('Categories','SubCategories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validate = $this->validate($request, [
            'question'   => 'required',
            'answer'     => 'required',
            'category'   => 'required',
            'subcategory'=> 'required',
            'status'     => 'required',
        ]);

        $faq               = new FaqModel;
        $faq->question     = $request->question;
        $faq->answer       = $request->answer;
        $faq->categoryId   = $request->category;
        $faq->subcategoryId= $request->subcategory;
        $faq->status       = $request->status;
        
        $faq->save();
        return redirect('/admin/faq')->with('success_msg','FAQ added successfully!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {   
        $Categories    = FaqCategoryModel::where('status','1')->get();
        $SubCategories = FaqSubCategoryModel::where('status','1')->get();
        $editData      = FaqModel::find($id);
        return view('admin.admincommon.faq.faq.edit',compact('editData','Categories','SubCategories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $updateErr = FaqModel::find($id);

        $validate = $this->validate($request, [
            'question'   => 'required',
            'answer'     => 'required',
            'category'   => 'required',
            'subcategory'=> 'required',
            'status'     => 'required',
        ]);

        $updateErr->question      = $request->question;
        $updateErr->answer        = $request->answer;
        $updateErr->categoryId    = $request->category;
        $updateErr->subcategoryId = $request->subcategory;
        $updateErr->status        = $request->status;
        
        $updateErr->save();
        return redirect('/admin/faq')->with('success_msg','FAQ updated successfully!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post = FaqModel::find($id);    
        $post->delete();
        return redirect()->back()->with('success_msg','FAQ deleted successfully!');
    }
}

<?php

namespace App\Http\Controllers;

use App\AgenciesModel;
use App\AgentModel;
use App\AutomobileBodyTypeModel;
use App\AutomobileFeatureListingModel;
use App\BraceletClaspMaterialModel;
use App\BraceletClaspTypeModel;
use App\BraceletColorModel;
use App\BraceletMaterialModel;
use App\BuildYearModel;
use App\CarBrandModel;
use App\CarModel;
use App\CarPriceModel;
use App\CaseDiameterModel;
use App\CaseGlassTypeModel;
use App\CaseMMModel;
use App\CaseMaterialModel;
use App\CaseWaterRDModel;
use App\CityModel;
use App\ColourModel;
use App\ContactusModel;
use App\CountryModel;
use App\CurrencyModel;
use App\DialColorModel;
use App\DialPowerReserveModel;
use App\FuelTypeModel;
use App\FunctionAlarmModel;
use App\FunctionAnnualCalenderModel;
use App\FunctionChronographModel;
use App\FunctionDayModel;
use App\FunctionDoubleChronographModel;
use App\FunctionGMTModel;
use App\FunctionJumpingHourModel;
use App\FunctionMinuteRepeaterModel;
use App\FunctionPanormaDateModel;
use App\FunctionTourbillionModel;
use App\FunctionYearModel;
use App\GenderModel;
use App\InteriorColourModel;
use App\MovementModel;
use App\NoOfBalconiesModel;
use App\NoOfBathroomsModel;
use App\NoOfBedroomsModel;
use App\NoOfDoorsModel;
use App\PriceModel;
use App\PropertyFeaturesListingModel;
use App\PropertyPriceModel;
use App\PropertyTypeModel;
use App\StateModel;
use App\Timezone;
use App\TopPropertyTypeModel;
use App\TransmissionModel;
use App\TypeModel;
use App\UserDetailsModel;
use App\WatchBezelModel;
use App\WatchFeaturesListingModel;
use App\WatchModel;
use App\Watchbrandmodel;
use App\YearOfManufactureModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use App\AutomobileDataListingModel;
use App\AutomobileAdditionalFeaturesModel;
use App\AutomobileFeaturesModel;
use App\AutomobileCommentModel;
use App\AutomobileImageModel;
use App\AutomobileDamageImageModel;
use App\AutomobileSecurityImageModel;
use App\WatchDataListingModel;
use App\WatchFeaturesModel;
use App\WatchAdditionalFeaturesModel;
use App\WatchDetailsFeaturesModel;
use App\WatchCommentModel;
use App\WatchImageModel;
use App\WatchDamageImageModel;
use App\WatchSecurityImageModel;
use App\CommercialPropertyTypeModel;
use App\PropertyDataModel;
use App\PropertyAdditionalFeaturesModel;
use App\PropertyFeaturesModel;
use App\PropertyCommentModel;
use App\PropertyImageModel;
use App\InclusionModel;
use App\PackageForPrivateSellersWatchModel;
use App\PackageListingRangeWatchManagementModel;
use App\WatchDocumentImageModel;
use App\WatchPayment;
use Auth;

class DealerAddListingsController extends Controller
{
    public function createAuto()
    {
        $selectedCurrency = CurrencyModel::select('currency_code as code')->where('status', '=', '1')->first();
        currency()->setUserCurrency($selectedCurrency->code);

        $contactUs = ContactusModel::get()->first();
        $propertyType = PropertyTypeModel::get();

        $makes = CarModel::get();
        $models = CarBrandModel::get();
        $colours = ColourModel::get();
        $interior_colours = InteriorColourModel::get();
        $country_data = CountryModel::get();
        $state_data = StateModel::get();
        $city_data = CityModel::get();
        $currency_data = CurrencyModel::get();
        $fuel_types = FuelTypeModel::get();
        $no_of_doors = NoOfDoorsModel::where('status','1')->get();
        $body_types = AutomobileBodyTypeModel::get();
        $buildYear = BuildYearModel::orderBy('build_year', 'DESC')->get();
        $transmissions = TransmissionModel::get();
        $prices = CarPriceModel::get();
        $timezone = Timezone::Orderby('offset')->get();

        $feature_listing = AutomobileFeatureListingModel::get();

        $i = 0;
        $html1 = '';
        $html2 = '';
        foreach ($feature_listing as $values) {
            $id = $values->id;
            $propertyName = $values->property_name;

            if ($i % 2 == 0) {

                $html1 .= '<div class="form-check-inline w-100 border p-1 mt-1 mb-1">
                   <label class="form-check-label" for="' . $propertyName . '"></label>
                    <input   type="checkbox" class="form-check-input" id="' . $propertyName . '" name="' . $id . '" value="1">' . $propertyName . '
                      </div>';
            } else {

                $html2 .= '<div class="form-check-inline w-100 border p-1 mt-1 mb-1">
                   <label class="form-check-label" for="' . $propertyName . '"></label>
                    <input   type="checkbox" class="form-check-input" id="' . $propertyName . '" name="' . $id . '" value="1">' . $propertyName . '
                      </div>';
            }

            $i ++;
        }

        // dd($html);

        return view('dealer.create-car', compact('makes', 'models', 'buildYear', 'colours', 'interior_colours', 'country_data', 'state_data', 'city_data', 'currency_data', 'fuel_types', 'no_of_doors', 'body_types', 'transmissions', 'prices', 'timezone', 'html1', 'html2', 'contactUs', 'propertyType'));
    }

    public function storeAutoData(Request $request)
    {
        $converted_amount = NULL;
        if ($request->input('price') != '') {
            $currency_code = CurrencyModel::find($request->input('currency'));

            $converted_amount = currency()->convert(floatval($request->input('price')), $currency_code->currency_code, "USD", false);
        }

        // $agent = AgentModel::where('userId', '=', $request->agentId)->first();

        // dd($agent);

        $dealer = AgenciesModel::where('userId', '=', $request->dealerId)->first();

        $createAutomobile = new AutomobileDataListingModel();

        $createAutomobile->ad_title = $request->get('adtitle');
        $createAutomobile->currency_id = $request->get('currency');
        if ($request->get('stateId') != '')
            $createAutomobile->state_id = $request->get('stateId');
        $createAutomobile->country_id = $request->get('countryId');
        $createAutomobile->city_id = $request->get('cityId');
        $createAutomobile->make_id = $request->get('make1');
        $createAutomobile->model_id = $request->get('model');
        if ($request->get('price_on_request') != '')
            $createAutomobile->price_on_request = $request->get('price_on_request');
        $createAutomobile->value = $converted_amount;
        $createAutomobile->price_id = $request->get('pricetType');
        $createAutomobile->fuel_type_id = $request->get('fuelType');
        $createAutomobile->colour_id = $request->get('color');
        if ($request->get('no_of_doors') != '')
            $createAutomobile->no_of_doors_id = $request->get('no_of_doors');
        $createAutomobile->body_type_id = $request->get('body_type');
        $createAutomobile->transmission = $request->get('transmission');
        if ($request->get('registrationPlace') != '')
            $createAutomobile->registration_place = $request->get('registrationPlace');
        $createAutomobile->rhdorlhd = $request->get('rhdorlhd');
        $createAutomobile->year_id = $request->get('year');
        $createAutomobile->neworused = $request->get('condition');
        $createAutomobile->odometer = $request->get('odometer');
        $createAutomobile->metric = $request->get('odometermetric');
        if ($request->get('engine') != '')
            $createAutomobile->engine = $request->get('engine');
        if ($request->get('interior_color') != '')
            $createAutomobile->interior_colour_id = $request->get('interior_color');
        $createAutomobile->transmission_id = $request->get('transmission');
        $createAutomobile->timezone_id = $request->get('timezone');
        // $createAutomobile->agent_id = $agent->id;
        $createAutomobile->agencies_id = $dealer->id;
        $createAutomobile->user_id = $request->get('dealerId');
        $createAutomobile->vin_number = $request->get('VIN');
        $createAutomobile->registration_number = $request->get('registrationPlate');

        $createAutomobile->save();

        $listing_id = $createAutomobile->id;

        if ($request->input('additional_feature') ?? '') {

            foreach ($request->input('additional_feature') as $features) {

                if ($features != null) {
                    $uploadFeature = new AutomobileAdditionalFeaturesModel();

                    $uploadFeature->feature = $features;
                    $uploadFeature->listing_id = $listing_id;
                    $uploadFeature->save();
                }
            }
        }

        foreach ($request->request as $key => $value) {
            if ($key != '_token' && $key != 'listing_id' && gettype($key) != 'string') {
                $createFeature = new AutomobileFeaturesModel();
                $createFeature->listing_id = $listing_id;
                $createFeature->feature_id = $key;
                $createFeature->feature_available = $value;
                $createFeature->save();
            }
        }

        if ($request->comment ?? '') {
            $createComment = new AutomobileCommentModel();
            $createComment->comment = $request->input('comment');
            $createComment->listing_id = $listing_id;
            $createComment->save();
        }
        if ($request->file('normalImages') ?? '') {
            foreach ($request->file('normalImages') as $image) {
                echo "<br>";
                $uploadImage = new AutomobileImageModel();

                $extension = $image->getClientOriginalExtension();
                Storage::disk('public')->put($image->getFilename() . '.' . $extension, File::get($image));

                $uploadImage->images = $image->getClientMimeType();
                $uploadImage->filename = $image->getFilename() . '.' . $extension;
                $uploadImage->listing_id = $listing_id;

                $uploadImage->save();
            }
        }

        if ($request->file('damageImages2') ?? '') {
            foreach ($request->file('damageImages2') as $image) {
                $uploadImage = new AutomobileDamageImageModel();

                $extension = $image->getClientOriginalExtension();
                Storage::disk('public')->put($image->getFilename() . '.' . $extension, File::get($image));

                $uploadImage->images = $image->getClientMimeType();
                $uploadImage->filename = $image->getFilename() . '.' . $extension;
                $uploadImage->listing_id = $listing_id;

                $uploadImage->save();
            }
        }

        if ($request->file('security1') ?? '') {
            foreach ($request->file('security1') as $image) {
                $uploadImage = new AutomobileSecurityImageModel();

                $extension = $image->getClientOriginalExtension();
                Storage::disk('public')->put($image->getFilename() . '.' . $extension, File::get($image));

                $uploadImage->images = $image->getClientMimeType();
                $uploadImage->filename = $image->getFilename() . '.' . $extension;
                $uploadImage->listing_id = $listing_id;

                $uploadImage->save();
            }
        }

        if ($request->file('security2') ?? '') {
            foreach ($request->file('security2') as $image) {
                $uploadImage = new AutomobileSecurityImageModel();

                $extension = $image->getClientOriginalExtension();
                Storage::disk('public')->put($image->getFilename() . '.' . $extension, File::get($image));

                $uploadImage->images = $image->getClientMimeType();
                $uploadImage->filename = $image->getFilename() . '.' . $extension;
                $uploadImage->listing_id = $listing_id;

                $uploadImage->save();
            }
        }

        return redirect('/dealer/inventoryList')->with('success_msg', 'Listing Created successfully!');
    }

    public function createWatch()
    {
        $selectedCurrency = CurrencyModel::select('currency_code as code')->where('status', '=', '1')->first();
        currency()->setUserCurrency($selectedCurrency->code);
        
        // $priceRangeArray = explode("-",$pricerange);
        
        // $packageId = PackageForPrivateSellersWatchModel::select('id')->where('package_for_private_sellers','=' , $package)->get()->first();
        
        // $packageList = PackageForPrivateSellersWatchModel::get();
        
        // $upgradepackage = PackageListingRangeWatchManagementModel::where('rangeFrom', '=', $priceRangeArray[0])->
        // where('rangeTo', '=', $priceRangeArray[1])->where('package_id', '!=', $packageId->id)->get();
        
        $contactUs = ContactusModel::get()->first();
        $propertyType = PropertyTypeModel::get();
        
        $models = Watchbrandmodel::orderBy('watch_model_name','ASC')->get();
        $brands = WatchModel::orderBy('watch_brand_name','ASC')->get();
        $country_data = CountryModel::get();
        $case_diameters = CaseDiameterModel::orderBy('case_diameter','ASC')->get();
        $movements = MovementModel::get();
        $types = TypeModel::get();
        $year_of_manufactures = YearOfManufactureModel::orderBy('year_of_manufacture','ASC')->get();
        $genders = GenderModel::get();
        $currency_data = CurrencyModel::get();
        $prices = PriceModel::get();
        $inclusions_data = InclusionModel::get();
        
        $power_reserve = DialPowerReserveModel::get();
        $bezel_material = WatchBezelModel::get();
        $dial_color = DialColorModel::get();
        $bracelet_material = BraceletMaterialModel::get();
        $bracelet_color = BraceletColorModel::get();
        $type_of_clasp = BraceletClaspTypeModel::get();
        $clasp_material = BraceletClaspMaterialModel::get();
        $case_material = CaseMaterialModel::get();
        $case_mm = CaseMMModel::get();
        $wrd = CaseWaterRDModel::get();
        $glass_type = CaseGlassTypeModel::get();
        $chronograph = FunctionChronographModel::get();
        $tourbillion = FunctionTourbillionModel::get();
        $gmt = FunctionGMTModel::get();
        $annual_calender = FunctionAnnualCalenderModel::get();
        $minute_repeater = FunctionMinuteRepeaterModel::get();
        $double_chronograph = FunctionDoubleChronographModel::get();
        $panorma_date = FunctionPanormaDateModel::get();
        $jumping_hour = FunctionJumpingHourModel::get();
        $alarm = FunctionAlarmModel::get();
        $year = FunctionYearModel::get();
        $day = FunctionDayModel::get();
        $agents = AgentModel::get();
        $timezone = Timezone::Orderby('offset')->get();
        $userDetails = UserDetailsModel::get();
        $country_data = CountryModel::get();
        $state_data = StateModel::get();
        $city_data = CityModel::get();
        
        $feature_listing = WatchFeaturesListingModel::get();
        
        $i =0;
        $html1 = '';
        $html2 = '';
        foreach ($feature_listing as $values){
            $id = $values->id;
            $propertyName = $values->property_name;

            if($i % 2 == 0){
            
            
            
            $html1 .= '<div class="form-check-inline w-100 border p-1 mt-1 mb-1">
                   <label class="form-check-label" for="'.$propertyName.'"></label>
                    <input   type="checkbox" class="form-check-input" id="'.$propertyName.'" name="'.$id.'" value="1">'.$propertyName.'
                      </div>';
            }else {
                
                $html2 .= '<div class="form-check-inline w-100 border p-1 mt-1 mb-1">
                   <label class="form-check-label" for="'.$propertyName.'"></label>
                    <input   type="checkbox" class="form-check-input" id="'.$propertyName.'" name="'.$id.'" value="1">'.$propertyName.'
                      </div>';
                
                
            }
            
            $i++;
        }

        // dd($html1);

        return view('dealer.create-watch', compact('inclusions_data','contactUs', 'propertyType','models','brands','country_data','case_diameters','movements','types', 'year_of_manufactures'
            ,'genders', 'power_reserve', 'bezel_material', 'dial_color', 'bracelet_material', 'bracelet_color', 'type_of_clasp', 'clasp_material',
            'case_material','case_mm', 'wrd', 'glass_type', 'chronograph', 'tourbillion', 'gmt', 'annual_calender', 'minute_repeater', 'double_chronograph',
            'panorma_date','jumping_hour', 'alarm', 'year', 'day', 'agents', 'timezone', 'html1', 'html2', 'currency_data', 'prices'
            , 'userDetails', 'country_data', 'state_data', 'city_data'));
    }

    public function storeWatchData(Request $request)
    {
  

        $userId = Auth::id();
        // dd($request);
        $watch_price=$request->input('watchprice');
        if ($request->input('watchprice') != '') {

            $currency_code = CurrencyModel::find($request->input('currency'));

            $watch_price = currency()->convert(floatval($request->input('watchprice')), $currency_code->currency_code, "USD", false);
            // $priceSqft = $request->input('priceSqft');
        }


        // $agent = AgentModel::where('userId', '=', $request->agentId)->first();

        $dealer = AgenciesModel::where('userId', '=', $userId)->first();

        $createWatch = new WatchDataListingModel();
        if($userId){
        $createWatch->createdID = $userId;
        }
        $createWatch->ad_title = $request->input('adtitle');
        $createWatch->currency_id = $request->input('currency');
        if ($request->input('stateId') != '')
            $createWatch->state_id = $request->input('stateId');
        $createWatch->country_id = $request->input('countryId');
        $createWatch->city_id = $request->input('cityId');
        $createWatch->brand_id = $request->input('brand');
        $createWatch->price_on_request = $request->input('price_on_request');
        $createWatch->model_id = $request->input('model');
        $createWatch->watch_price = $watch_price;
        if ($request->input('pricetType') != '')
            $createWatch->price_id = $request->input('pricetType');
        if ($request->input('case_diameter') != '')
            $createWatch->case_diameter_id = $request->input('case_diameter');
        $createWatch->movement_id = $request->input('movement');
        $createWatch->type_id = $request->input('type');
        $createWatch->year_of_manufacture_id = $request->input('yom');
        $createWatch->gender_id = $request->input('gender');
        $createWatch->inclusions = $request->input('inclusions');
        $createWatch->reference_no = $request->input('referenceNo');
        $createWatch->watch_condition = $request->input('watchCondition');
        $createWatch->status = 1;
        $createWatch->brand_name = WatchModel::find($request->input('brand'))->watch_brand_name;
        $createWatch->model_name = Watchbrandmodel::find($request->input('model'))->watch_model_name;
        // $createWatch->agent_id = $agent->id;
        $createWatch->agencies_id = $dealer->id;
        $createWatch->timezone_id = $request->input('timezone');

        $createWatch->save();

        $listing_id = $createWatch->id;

        $createFeature = new WatchFeaturesModel();

        $createFeature->listing_id = $listing_id;
        $createFeature->bezel_material_id = $request->input('bezelMaterial');
        $createFeature->power_reserve_id = $request->input('powerReserve');
        $createFeature->dial_color_id = $request->input('dialColor');
        $createFeature->bracelet_material_id = $request->input('braceletMaterial');
        $createFeature->bracelet_color_id = $request->input('braceletColor');
        $createFeature->type_of_clasp_id = $request->input('typeOfClasp');
        $createFeature->clasp_material_id = $request->input('claspMaterial');
        $createFeature->case_material_id = $request->input('caseMaterial');
        $createFeature->case_mm_id = $request->input('caseMM');
        $createFeature->water_resisitance_depth_id = $request->input('wrd');
        $createFeature->glass_type_id = $request->input('glassType');

        $createFeature->dial_color = DialColorModel::find($request->input('dialColor'));
        $createFeature->bracelet_material = BraceletMaterialModel::find($request->input('braceletMaterial'));

        $createFeature->save();

        if ($request->input('additional_feature') ?? '') {

            foreach ($request->input('additional_feature') as $features) {

                if ($features != null) {
                    $uploadFeature = new WatchAdditionalFeaturesModel();

                    $uploadFeature->feature = $features;
                    $uploadFeature->listing_id = $listing_id;
                    $uploadFeature->save();
                }
            }
        }

        foreach ($request->request as $key => $value) {
            if ($key != '_token' && $key != 'listing_id' && gettype($key) != 'string') {
                $createDetailFeature = new WatchDetailsFeaturesModel();
                $createDetailFeature->listing_id = $listing_id;
                $createDetailFeature->feature_id = $key;
                $createDetailFeature->feature_available = $value;
                $createDetailFeature->save();
            }
        }

        if ($request->comment ?? '') {
            $createComment = new WatchCommentModel();
            $createComment->comment = $request->input('comment');
            $createComment->listing_id = $listing_id;
            $createComment->save();
        }

        if ($request->file('normalImages') ?? '') {
            foreach ($request->file('normalImages') as $image) {
                echo "<br>";
                $uploadImage = new WatchImageModel();

                $extension = $image->getClientOriginalExtension();
                Storage::disk('public')->put($image->getFilename() . '.' . $extension, File::get($image));

                $uploadImage->images = $image->getClientMimeType();
                $uploadImage->filename = $image->getFilename() . '.' . $extension;
                $uploadImage->listing_id = $listing_id;

                $uploadImage->save();
            }
        }

        if ($request->file('damageImages2') ?? '') {
            foreach ($request->file('damageImages2') as $image) {
                $uploadImage = new WatchDamageImageModel();

                $extension = $image->getClientOriginalExtension();
                Storage::disk('public')->put($image->getFilename() . '.' . $extension, File::get($image));

                $uploadImage->images = $image->getClientMimeType();
                $uploadImage->filename = $image->getFilename() . '.' . $extension;
                $uploadImage->listing_id = $listing_id;

                $uploadImage->save();
            }
        }

        if ($request->file('security1') ?? '') {
            foreach ($request->file('security1') as $image) {
                $uploadImage = new WatchSecurityImageModel();

                $extension = $image->getClientOriginalExtension();
                Storage::disk('public')->put($image->getFilename() . '.' . $extension, File::get($image));

                $uploadImage->images = $image->getClientMimeType();
                $uploadImage->filename = $image->getFilename() . '.' . $extension;
                $uploadImage->security_time = $request->get('security-time1');
                $uploadImage->listing_id = $listing_id;

                $uploadImage->save();
            }
        }

        if ($request->file('security2') ?? '') {
            foreach ($request->file('security2') as $image) {
                $uploadImage = new WatchSecurityImageModel();

                $extension = $image->getClientOriginalExtension();
                Storage::disk('public')->put($image->getFilename() . '.' . $extension, File::get($image));

                $uploadImage->images = $image->getClientMimeType();
                $uploadImage->filename = $image->getFilename() . '.' . $extension;
                $uploadImage->security_time = $request->get('security-time2');
                $uploadImage->listing_id = $listing_id;

                $uploadImage->save();
            }
        }

        if ($request->hasFile('license')) {  // Check if file input is set

                        $image = $request->license;
                        $extension = $image->getClientOriginalExtension();
                        $uploadImage = new WatchDocumentImageModel();
                        
                        Storage::disk('public')->put($image->getFilename() . '.' . $extension, File::get($image));
                        
                       // $uploadImage->images = $image->getClientMimeType();
                       $uploadImage->filename = $image->getFilename() . '.' . $extension;
                       $uploadImage->type = 'license';
                       $uploadImage->listing_id = $listing_id;       
                       $uploadImage->save();
                  
                }

                if ($request->hasFile('drivingLicense')) {  // Check if file input is set
                   
                         $image = $request->drivingLicense;
                         $extension = $image->getClientOriginalExtension();
                         $uploadImage = new WatchDocumentImageModel();
                        
                        
                        Storage::disk('public')->put($image->getFilename() . '.' . $extension, File::get($image));
                        
                       // $uploadImage->images = $image->getClientMimeType();
                       $uploadImage->filename = $image->getFilename() . '.' . $extension;
                       $uploadImage->type = 'drivingLicense';
                       $uploadImage->listing_id = $listing_id;       
                    $uploadImage->save();
                    
                }

                if ($request->hasFile('utilityBill')) {  // Check if file input is set
                
                        $image = $request->utilityBill;
                        $extension = $image->getClientOriginalExtension();
                        $uploadImage = new WatchDocumentImageModel();
                        
                       
                        Storage::disk('public')->put($image->getFilename() . '.' . $extension, File::get($image));
                        
                       // $uploadImage->images = $image->getClientMimeType();
                       $uploadImage->filename = $image->getFilename() . '.' . $extension;
                       $uploadImage->type = 'utilityBill';
                       $uploadImage->listing_id = $listing_id;       
                    $uploadImage->save();
                    
                }

                if ($request->hasFile('passport')) {  // Check if file input is set
                
                        $image = $request->passport;
                        $extension = $image->getClientOriginalExtension();
                        $uploadImage = new WatchDocumentImageModel();
                        
                        
                        Storage::disk('public')->put($image->getFilename() . '.' . $extension, File::get($image));
                        
                       // $uploadImage->images = $image->getClientMimeType();
                       $uploadImage->filename = $image->getFilename() . '.' . $extension;
                       $uploadImage->type = 'passport';
                       $uploadImage->listing_id = $listing_id;       
                       $uploadImage->save();
                    
                }

                // Uploading Documents Logic


                    if ($createWatch) {
                    // Retrieve charge details
                    $detal = new WatchPayment;
                    $detal->listingId = $listing_id;   
                    $detal->name = $request->firstName." ".$request->lastName;
                    $detal->email = $request->email;
                    $detal->phone = $request->phoneNumber;
                    $detal->amount = $watch_price;
                    $detal->currency = $request->currency;
                    //'transaction_id' => $paymentDetails['balance_transaction']
                    // $detal->bankName = $request->bankName;
                    // $detal->beneficiaryName = $request->beneficiaryName;
                    // $detal->bankAccount = $request->bankAccount;
                    // $detal->ifsc = $request->ifsc;
                    $detal->payment_status = "pending";//$paymentDetails['status'],
                    $detal->address1 = $request->address;
                    $detal->address2 = $request->street_2;
                    $detal->zip = $request->zipcode;
                    $detal->country = $request->country_pi;
                    $detal->state = $request->state_pi;
                    $detal->city = $request->city_pi;
                    $detal->save();
                    //'receipt_url' => $paymentDetails['receipt_url']
                    // 'transaction_complete_details' => json_encode($paymentDetails)
                    
                }

        return redirect('/dealer/inventoryList')->with('success_msg', 'Listing Created successfully!');
    }

    public function createProperty()
    {
        $city_data = CityModel::get();
        $state_data = StateModel::get();
        $country_data = CountryModel::get();
        $bedroom_data = NoOfBedroomsModel::get();
        $bathroom_data = NoOfBathroomsModel::get();
        $parking_data = NoOfBalconiesModel::get();
        //$propertyType_data = PropertyTypeModel::get();
        $buildYear_data = BuildYearModel::get();
        $currency_data = CurrencyModel::get();
        $prices = PropertyPriceModel::get();
        $agents = AgentModel::get();
        $toppropertyType_data = TopPropertyTypeModel::get();
        $timezone = Timezone::Orderby('offset')->get();

        $feature_listing = PropertyFeaturesListingModel::get();

        $i = 0;
        $html1 = '';
        $html2 = '';
        foreach ($feature_listing as $values) {
            $id = $values->id;
            $propertyName = $values->property_name;

            if ($i % 2 == 0) {
                $html1 .= '<div class="form-check-inline w-100 border p-1 mt-1 mb-1">
                   <label class="form-check-label" for="' . $propertyName . '"></label>
                    <input   type="checkbox" class="form-check-input" id="' . $propertyName . '" name="' . $id . '" value="1">' . $propertyName . '
                      </div>';
            } else {
                $html2 .= '<div class="form-check-inline w-100 border p-1 mt-1 mb-1">
                   <label class="form-check-label" for="' . $propertyName . '"></label>
                    <input   type="checkbox" class="form-check-input" id="' . $propertyName . '" name="' . $id . '" value="1">' . $propertyName . '
                      </div>';
            }
            $i ++;
        }

        return view('dealer.create-property', compact('timezone', 'parking_data', 'html1', 'html2', 'city_data', 'state_data', 'country_data', 'bedroom_data', 'bathroom_data', 'buildYear_data', 'currency_data', 'prices', 'agents', 'toppropertyType_data'));
    }

    public function addPropertyData(Request $request)
    {
        //dd($request->all());
        $this->validate($request, [
            'currency' => 'required',
            'cityId' => 'required',
            'countryId' => 'required',
            'establishmentType' => 'required',
            'bedRoom' => 'required',
            'bathRoom' => 'required',
            'toppropertyType' => 'required',
            'propertyType' => 'required',
            'metric' => 'required',
            'sol' => 'required',
            'buildingSize' => 'required',
            'propertySL' => 'required',
            'postalCode' => 'required',
            'builtId' => 'required',
            'street_1' => 'required',
            'street_2' => 'required',
            'status' => 'required',
            'comment' => 'required',
            'normalImages' => 'required'
        ]);

        
        $converted_amount = NULL;

        if ($request->input('priceSqft') != '') {
            $currency_code = CurrencyModel::find($request->input('currency'));

            $converted_amount = currency()->convert(floatval($request->input('priceSqft')), $currency_code->currency_code, "USD", false);
        }

        $dealer = AgenciesModel::where('userId', '=', $request->dealerId)->first();

        $createProperty = new PropertyDataModel();
        $createProperty->ad_title = $request->input('adTitle');
        $createProperty->currency_id = $request->input('currency');
        $createProperty->city_id = $request->input('cityId');
        if ($request->input('stateId') != '')
            $createProperty->state_id = $request->input('stateId');
        $createProperty->country_id = $request->input('countryId');
        /*
         * $createProperty->featured = $request->input('featured');
         * $createProperty->topHomes = $request->input('topHomes');
         * $createProperty->dream_location = $request->input('dreamLocation');
         */
        $createProperty->establishment_type = $request->input('establishmentType');
        $createProperty->bed_id = $request->input('bedRoom');
        $createProperty->bathroom_id = $request->input('bathRoom');
        $createProperty->top_property_type_id = $request->input('toppropertyType');
        $createProperty->property_type_id = $request->input('propertyType');
        $createProperty->metric = $request->input('metric');
        $createProperty->land_size = $request->input('sol');
        $createProperty->building_size = $request->input('buildingSize');
        $createProperty->property_sale_lease = $request->input('propertySL');
        if ($request->input('unitNumber') != '')
            $createProperty->unit_number = $request->input('unitNumber');
        $createProperty->postal_code = $request->input('postalCode');
        $createProperty->price_id = $request->input('price');
        $createProperty->price_on_request = $request->input('price_on_request');
        $createProperty->property_price = $converted_amount;
        $createProperty->year_built_id = $request->input('builtId');
        $createProperty->street_1 = $request->input('street_1');
        $createProperty->street_2 = $request->input('street_2');
        //$createProperty->agent_id = $request->agentId;
        $createProperty->agencies_id = $dealer->id;
        $createProperty->status = $request->input('status');
        $createProperty->parking_id = $request->input('parking');
        $createProperty->timezone_id = $request->input('timezone');

        $createProperty->save();

        $listing_id = $createProperty->id;

        if ($request->input('additional_feature') ?? '') {

            foreach ($request->input('additional_feature') as $features) {

                if ($features != null) {
                    $uploadFeature = new PropertyAdditionalFeaturesModel();

                    $uploadFeature->feature = $features;
                    $uploadFeature->listing_id = $listing_id;
                    $uploadFeature->save();
                }
            }
        }

        foreach ($request->request as $key => $value) {
            if ($key != '_token' && $key != 'listing_id' && gettype($key) != 'string') {
                $createFeature = new PropertyFeaturesModel();
                $createFeature->listing_id = $listing_id;
                $createFeature->feature_id = $key;
                $createFeature->feature_available = $value;
                $createFeature->save();
            }
        }
        if ($request->comment ?? '') {
            $createComment = new PropertyCommentModel();
            $createComment->comment = $request->input('comment');
            $createComment->listing_id = $listing_id;
            $createComment->save();
        }
        if ($request->file('normalImages') ?? '') {
            foreach ($request->file('normalImages') as $image) {
                $uploadImage = new PropertyImageModel();

                $extension = $image->getClientOriginalExtension();
                Storage::disk('public')->put($image->getFilename() . '.' . $extension, File::get($image));

                $uploadImage->images = $image->getClientMimeType();
                $uploadImage->filename = $image->getFilename() . '.' . $extension;
                $uploadImage->listing_id = $listing_id;
                $uploadImage->save();
            }
        }
        return redirect('/dealer/inventoryList')->with('success_msg', 'Listing Created successfully!');
    }

    public function getSubPropertyType(Request $request)
    {
        $propertyId = $request->input('propertyId');
        $propertyType = '';

        if ($propertyId == '1')
            $propertyType = CommercialPropertyTypeModel::get();
        else
            $propertyType = PropertyTypeModel::get();

        return $propertyType;
    }

    // this function displays the states based on the selected country
    public function getStateByCountry(Request $request)
    {
        $countryId = $request->input('countryId');
        $state = StateModel::select('id', 'state_name')->where('country_id', $countryId)->get();
        return $state;
    }

    // this function is used to get the cities based on selected state
    public function getCityByState(Request $request)
    {
        $stateId = $request->input('stateId');
        $countryId = $request->input('countryId');

        if ($stateId == '')
            $city = CityModel::select('id', 'city_name')->where('country_id', $countryId)->get();
        else
            $city = CityModel::select('id', 'city_name')->where('state_id', $stateId)->get();

        return $city;
    }
}

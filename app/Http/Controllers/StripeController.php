<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Stripe;
use Session;
use App\ContactusModel;
use URL;
use App\PackageListingRangeWatchManagementModel;
use Illuminate\Support\Facades\Auth;

class StripeController extends Controller
{
    /**
     * payment view
     */

    public function handleGet()
    {   
        // Getting Package information

        $packageId = request("packageId");
       
        $data['packageData'] = PackageListingRangeWatchManagementModel::where('id', $packageId)->with('package')->first(); 

        // Getting Customer Information
       
        $data['user'] = Auth::user();

        $data['planInfo']['amount'] = $data['packageData']['pricing'] * 100;
       // $data['planInfo']['name'] = $data['packageData']->package->package_for_private_sellers;
        $data['planInfo']['id'] = $data['packageData']->id;
        //$data['planInfo']['description'] = "Membership Information";
        //$data['planInfo']['email'] = $data['user']->email;

      //  dd($data['user']);
    
        return view('home')->with($data);
       
    }
  
    /**
     * handling payment with POST
     */
    public function handlePost(Request $request)
    {
      $cardName = $request->cardName;
      $cardNumber = $request->cardNumber;
      $cardCVC = $request->cardCVC;
      $cardExpiryMonth = $request->cardExpiryMonth;
      $cardExpiryYear = $request->cardExpiryYear;
      $userId = $request->userId;
      $packageId = $request->packageId;

      $data['packageData'] = PackageListingRangeWatchManagementModel::where('id', $packageId)->with('package')->first(); 


      $data['user'] = Auth::user();
      $name =  $data['user']->first_name . " " .  $data['user']->last_name;

   
        Stripe\Stripe::setApiKey(env('STRIPE_SECRET_KEY'));

        $customer = \Stripe\Customer::create([
            'name' => $name,
            'email' => $data['user']->email,
            'address' => [
              'line1' => '510 Townsend St',
              'postal_code' => '121008',
              'city' => 'Faridabad',
              'state' => 'HR',
              'country' => 'IN',
            ],
          ]);

          $stripe = new \Stripe\StripeClient(
            'sk_test_51IbIAtSEBtRtnlVS8c1S888PJelnbBwGoJmiWYPxdfqb0NAqGyNERnUqn3FeufMpo7FwfsOVZ9ee7ik1FcXP6J9900A8JcvYcL'
          );

         $paymentMethod = $stripe->paymentMethods->create([
            'type' => 'card',
            'card' => [
              'number' => $cardNumber,
              'exp_month' => $cardExpiryMonth,
              'exp_year' => $cardExpiryYear,
              'cvc' => $cardCVC,
            ],
          ]);

          $stripe->paymentMethods->attach(
            $paymentMethod->id,
            ['customer' =>  $customer->id]
          );

        $payment_intent = \Stripe\PaymentIntent::create([
            'description' => $data['packageData']->package->package_for_private_sellers,
            'shipping' => [
              'name' => $name,
              'address' => [
                'line1' => '510 Townsend St',
                'postal_code' => '121008',
                'city' => 'Faridabad',
                'state' => 'HR',
                'country' => 'IN',
              ],
            ],
            'amount' => $data['packageData']['pricing'] * 100,
            'currency' => 'inr',
            "payment_method" => $paymentMethod->id,
            'payment_method_types' => ['card'],
            "customer" => $customer->id
          ]);

          $stripe->paymentIntents->confirm(
            $payment_intent->id,
            ['payment_method' => $paymentMethod->id]
          );
          
    
          

        //   $charge = Stripe\Charge::create ([
        //  //   "amount" => 100 * $paymentData['amount'],
        //         "amount" => 100 * 5,
        //         "currency" => "usd",
        //         "source" => $request->stripeToken,
        //         "description" => "payment store",
        //         //"customer" => $customer->id
        // ]);

       

        // $stripe = new \Stripe\StripeClient(
        // 'sk_test_51IbIAtSEBtRtnlVS8c1S888PJelnbBwGoJmiWYPxdfqb0NAqGyNERnUqn3FeufMpo7FwfsOVZ9ee7ik1FcXP6J9900A8JcvYcL'
        // );
        // $customer =  $stripe->customers->create([
        // 'description' => 'My First Test Customer (created for API docs)',
        // 'name' => "Rajat",
        // 'email' => "rajat@optimalvirtualemployee.com",
        // ]);


    //    $customer = Stripe\Customer::create([
    //         'name' => "Rajat",
    //         'email' => "rajat@optimalvirtualemployee.com",
    //         'source' => $request->stripeToken
    //     ]);

      // $paymentData = session('paymentData');

        // $charge = Stripe\Charge::create ([
        //  //   "amount" => 100 * $paymentData['amount'],
        //         "amount" => 100 * 5,
        //         "currency" => "usd",
        //         "source" => $request->stripeToken,
        //         "description" => "payment store",
        //         "customer" => $customer->id
        // ]);

        $paymentDetails = $payment_intent->jsonSerialize();

        Session::flash('success', 'Payment has been successfully processed.');
          
        return back();
    }

     /**
     * handling payment with POST
     */
    public function reauth($acId)
    {
         // Generating Onboarding Process Account Link
         Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));

         $baseUrl = URL::to('/');

         $account_links = \Stripe\AccountLink::create([
         'account' => $acId,
         'refresh_url' => $baseUrl.'/reauth',
         'return_url' => $baseUrl,
         'type' => 'account_onboarding',
         ]);

         $accountLinkDetails = $account_links->jsonSerialize();
         $onboardUrl = $accountLinkDetails['url'];

         return redirect($onboardUrl);
    }
}
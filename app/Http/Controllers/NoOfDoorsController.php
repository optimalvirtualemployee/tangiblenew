<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\NoOfDoorsModel;

class NoOfDoorsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $no_of_doors = NoOfDoorsModel::get();
        return view('admin.automobile.no_of_doors.index',compact('no_of_doors'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.automobile.no_of_doors.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validate = $this->validate($request, [
            'no_of_doors' => 'required|numeric|unique:au_no_of_doors',
            'status' => 'required'
        ]);

        $no_of_doors = NoOfDoorsModel::create($validate);
        $no_of_doors->save();
        return redirect('/admin/no_of_doors')->with('success_msg','No. of Doors Created successfully!');
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $no_of_doors_data = NoOfDoorsModel::find($id);
        return view('admin.automobile.no_of_doors.edit', compact('no_of_doors_data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validate = $this->validate($request, [
            'no_of_doors' => 'required|numeric'
        ]);

        $no_of_doorsUpdate = NoOfDoorsModel::find($id);

        if ($no_of_doorsUpdate->no_of_doors == $validate['no_of_doors']) {
            $no_of_doorsUpdate->status = $request->input('status');
        } else {
            $validate = $this->validate($request, ['no_of_doors' => 'unique:au_no_of_doors']);
            $no_of_doorsUpdate->no_of_doors = $validate['no_of_doors'];
            $no_of_doorsUpdate->status = $request->input('status');
        }

        $no_of_doorsUpdate->save();

        return redirect('/admin/no_of_doors')->with('success_msg','No. of Doors Updated successfully!'); 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deleteRecords = NoOfDoorsModel::find($id)->delete();
        return redirect('/admin/no_of_doors')->with('success_msg','No. of Doors deleted successfully!');
    }
}

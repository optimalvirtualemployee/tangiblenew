<?php

namespace App\Http\Controllers;

use App\WatchContactusModel;
use Illuminate\Http\Request;

class WatchContactusController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $contactuss = WatchContactusModel::get();
        
        return view('admin.watch.contactus.index')->with(array(
            'contactus' => $contactuss
        ));
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
        return view('admin.watch.contactus.create');
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $data = $request->validate([
            
            
            'address' => ['required'],
            'mobile' => ['required'],
            'whatsapp' => ['required'],
            'email' => ['required']
        ]);
        
        $createwatchcontactus = new WatchContactusModel();
        
        $createwatchcontactus->address = $request->input('address');
        $createwatchcontactus->mobile = $request->input('mobile');
        $createwatchcontactus->whatsapp = $request->input('whatsapp');
        $createwatchcontactus->email = $request->input('email');
        
        $createwatchcontactus->save();
        
        return redirect('/admin/watchcontactus')->with('success_msg', 'Watch contactus Created successfully!');
        
    }
    
    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
        $contactus = WatchContactusModel::find($id);
        
        
        return view('admin.watch.contactus.edit', compact('contactus'));
    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->validate([
            
            
            'address' => ['required'],
            'mobile' => ['required'],
            'whatsapp' => ['required'],
            'email' => ['required']
        ]);
        
        $updatewatchcontactus = WatchContactusModel::find($id);
        
        $updatewatchcontactus->address = $request->input('address');
        $updatewatchcontactus->mobile = $request->input('mobile');
        $updatewatchcontactus->whatsapp = $request->input('whatsapp');
        $updatewatchcontactus->email = $request->input('email');
        
        $updatewatchcontactus->save();
        
        return redirect('/admin/watchcontactus')->with('success_msg', 'Watch contactus Updated successfully!');
        
    }
    
    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deleteRecords = WatchContactusModel::find($id)->delete();
        return redirect('/admin/watchcontactus')->with('success_msg', 'Watch contactus Deleted successfully!');
        
    }
}

<?php

namespace App\Http\Controllers;

use App\WatchCommentModel;
use Illuminate\Http\Request;

class WatchCommentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function createComment(Request $request, $listing_id)
    {
        return view('admin.watch.comments.create',
            compact('listing_id'));
    }
    
    public function create()
    {
        
    }
    
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    
    public function store(Request $request)
    {
        $data = $request->validate([
            'comment' => ['required'],
            'listing_id' => ['required']
        ]);
        
        $createComment = new WatchCommentModel;
        
        $createComment->comment = $request->input('comment');
        $createComment->listing_id = $request->input('listing_id');
        
        $createComment->save();
        
        return redirect('/admin/watchcomment/'.$request->input('listing_id'))->with('success_msg', 'Watch Comment Created successfully!');
    }
    
    /**
     * Display the specified resource.
     *comment
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $listing_id = $id;
        $details = WatchCommentModel::where('listing_id', '=', $listing_id)
        ->get();
        
        return view('admin.watch.comments.index', compact('details','listing_id'));
        
    }
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $comment_data = WatchCommentModel::find($id);
        
        return view('admin.watch.comments.edit', compact('comment_data'));
    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->validate([
            'comment' => ['required'],
            'listing_id' => ['required']
        ]);
        
        $updateComment = WatchCommentModel::find($id);
        
        $updateComment->comment = $request->input('comment');
        $updateComment->listing_id = $request->input('listing_id');
        
        $updateComment->save();
        
        return redirect('/admin/watchcomment/'.$request->input('listing_id'))->with('success_msg', 'Watch Comment Updated successfully!');
    }
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $comment = WatchCommentModel::find($id);
        $listing_id = $comment->listing_id;
        
        $deleteRecords = WatchCommentModel::find($id)->delete();
        return redirect('/admin/watchcomment/'.$listing_id)->with('success_msg', 'Watch Comment Deleted successfully!');
    }
}

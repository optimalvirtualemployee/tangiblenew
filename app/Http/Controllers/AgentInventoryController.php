<?php

namespace App\Http\Controllers;

use App\AgentModel;
use App\AutomobileDataListingModel;
use App\AutomobileImageModel;
use App\CategoriesModel;
use App\PropertyDataModel;
use App\PropertyImageModel;
use App\WatchDataListingModel;
use App\WatchImageModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\AgenciesModel;

class AgentInventoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    
    public function inventoryList(Request $request){
        
        
        $userId = Auth::user()->id;
        
        $agent = AgentModel::select('tbl_agent.id','agency_id','tbl_category.website_category as category')
        ->where('tbl_agent.userId', '=', Auth::User()->id)
        ->join('tbl_agencies', 'tbl_agencies.id','tbl_agent.agency_id')
        ->join('tbl_category', 'tbl_category.id', 'tbl_agencies.category_id')
        ->first();

        if($agent):
        $agencyID = $agent->agency_id;
        else:
        $agencyID = '';
        endif;

        $agency = AgenciesModel::where('id', '=', $agencyID)->first();
        $category = CategoriesModel::find($agency->category_id);
        
        $listings_data = "";
        $images = "";
        
        DB::enableQueryLog();
        if($category->website_category == 'Watch'){
            
            $listings_data = WatchDataListingModel::select('model_id', 'brand_id', 'comment', 'ad_title', 'wa_watch_detail.id as id' , 'brand_name', 'model_name','watch_price as price'
                , 'wa_watch_detail.updated_at as updated_at', 'wa_watch_detail.status as status', 'timezones.name as timezone')
                ->join('tbl_currency', 'tbl_currency.id', '=', 'wa_watch_detail.currency_id')
                ->leftJoin('wa_year_of_manufacture', 'wa_year_of_manufacture.id', '=', 'wa_watch_detail.year_of_manufacture_id')
                ->leftJoin('wa_case_diameter','wa_case_diameter.id', '=', 'wa_watch_detail.case_diameter_id' )
                ->join('tbl_state', 'tbl_state.id', '=', 'wa_watch_detail.state_id')
                ->join('tbl_city', 'tbl_city.id', '=', 'wa_watch_detail.city_id')
                ->join('tbl_country', 'tbl_country.id', '=', 'wa_watch_detail.country_id')
                ->join('tbl_agent', 'tbl_agent.id', '=', 'wa_watch_detail.agent_id')
                ->join('wa_watch_comment', 'wa_watch_comment.listing_id', '=', 'wa_watch_detail.id')
                ->join('timezones', 'timezones.id', '=', 'wa_watch_detail.timezone_id')
                ->whereIN('wa_watch_detail.id', function($query){
                    $query->select('listing_id')
                    ->from('wa_watch_images');
                })
                ->whereIN('wa_watch_detail.id', function($query){
                    $query->select('listing_id')
                    ->from('wa_watch_features');
                })
                ->whereIN('wa_watch_detail.id', function($query){
                    $query->select('listing_id')
                    ->from('wa_watch_comment');
                })->where('agent_id', '=', $agencyID)
                ->where('wa_watch_detail.status','=', '1')
                ->orWhere('wa_watch_detail.case_diameter_id','=', 'NULL')
                ->get();
                
                $images = WatchImageModel::get();
        }
        //dd($category);
        if($category->website_category == 'Automobiles'){
            
            $listings_data = AutomobileDataListingModel::select('website_category','automobile_model_name as model_name','make_id as brand_id','model_id','automobile_brand_name as brand_name',
                'au_automobile_detail.id as id', 'ad_title', 'value as price',  'au_automobile_detail.updated_at as updated_at', 'au_automobile_detail.status as status', 'timezones.name as timezone')
                ->join('au_brands', 'au_brands.id', '=', 'au_automobile_detail.make_id')
                ->join('au_model','au_model.id', '=', 'au_automobile_detail.model_id')
                ->join('tbl_currency', 'tbl_currency.id', '=', 'au_automobile_detail.currency_id')
                ->join('tbl_state', 'tbl_state.id', '=', 'au_automobile_detail.state_id')
                ->join('tbl_city', 'tbl_city.id', '=', 'au_automobile_detail.city_id')
                ->join('tbl_country', 'tbl_country.id', '=', 'au_automobile_detail.country_id')
                ->join('tbl_agent', 'tbl_agent.id', '=', 'au_automobile_detail.agent_id')
                ->leftJoin('tbl_agencies', 'tbl_agencies.id', '=', 'tbl_agent.agency_id')
                ->leftJoin('tbl_category', 'tbl_category.id', '=', 'tbl_agencies.category_id')
                ->join('re_build_year', 're_build_year.id', '=', 'au_automobile_detail.year_id')
                ->join('au_transmission', 'au_transmission.id', '=', 'au_automobile_detail.transmission_id')
                ->join('timezones', 'timezones.id', '=', 'au_automobile_detail.timezone_id')
                ->whereIN('au_automobile_detail.id', function ($query) {
                    $query->select('listing_id')
                    ->from('au_automobile_images');
                })
                /* ->whereIN('au_automobile_detail.id', function($query){
                    $query->select('listing_id')
                    ->from('au_automobile_features');
                }) */
                ->whereIN('au_automobile_detail.id', function ($query) {
                    $query->select('listing_id')
                    ->from('au_automobile_comments');
                })->where('agent_id', '=', $agent->id)
                ->where('au_automobile_detail.status', '=', '1')
                ->get();
                
                $images = AutomobileImageModel::get();
        }
        //dd(DB::getQueryLog());
        //dd($listings_data);
        
        if($category->website_category == 'Real Estate'){
            $listings_data = PropertyDataModel::select('website_category','re_property_details.status as status','re_property_details.postal_code as brand_id','city_name as brand_name','comment','re_property_details.id as id' ,'ad_title','property_price as price', 're_property_details.updated_at as updated_at', 'timezones.name as timezone')
            ->join('tbl_currency', 'tbl_currency.id', '=', 're_property_details.currency_id')
            ->join('tbl_city', 'tbl_city.id', '=', 're_property_details.city_id')
            ->join('tbl_country', 'tbl_country.id', '=', 're_property_details.country_id')
            ->join('tbl_agent', 'tbl_agent.id', '=', 're_property_details.agent_id')
            ->leftJoin('tbl_agencies', 'tbl_agencies.id', '=', 'tbl_agent.agency_id')
            ->leftJoin('tbl_category', 'tbl_category.id', '=', 'tbl_agencies.category_id')
            ->join('re_build_year', 're_build_year.id', '=', 're_property_details.year_built_id')
            ->join('re_beds', 're_beds.id', '=', 're_property_details.bed_id')
            ->join('re_property_comment', 're_property_comment.listing_id', '=', 're_property_details.id')
            ->join('timezones', 'timezones.id', '=', 're_property_details.timezone_id')
            ->whereIN('re_property_details.id', function($query){
                $query->select('listing_id')
                ->from('re_property_images');
            })
            ->whereIN('re_property_details.id', function($query){
                $query->select('listing_id')
                ->from('re_property_features');
            })
            ->whereIN('re_property_details.id', function($query){
                $query->select('listing_id')
                ->from('re_property_comment');
            })->where('agent_id', '=', $agent->id)
            ->where('re_property_details.status','=', '1')
            ->get();
            
            $images = PropertyImageModel::get();
        }
        
        return view('agent.InventoryList', compact('listings_data', 'images', 'category', 'agent'));
        
    }
}

<?php

namespace App\Http\Controllers;

use App\PropertyNewsImageModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class PropertyNewsImageController extends Controller
{
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function createImage(Request $request, $listing_id)
    {
        return view('admin.realestate.newsimages.create',
            compact('listing_id'));
    }
    
    public function store(Request $request)
    {
        request()->validate([
            'photos'  => 'required',
            'photos.*' => 'mimes:jpg,jpeg,png,gif'
        ]);
        
        
        foreach ($request->file('photos') as $image){
            $uploadImage = new PropertyNewsImageModel();
            
            $extension = $image->getClientOriginalExtension();
            Storage::disk('public')->put($image->getFilename().'.'.$extension,  File::get($image));
            
            $uploadImage->images = $image->getClientMimeType();
            $uploadImage->filename = $image->getFilename().'.'.$extension;
            $uploadImage->listing_id = $request->input('listing_id');
            $uploadImage->save();
        }
        
        return redirect('/admin/uploadpropertynewsimage/'.$request->input('listing_id'))->with('success_msg', 'Property image Created successfully!');
    }
    
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $listing_id = $id;
        $details = PropertyNewsImageModel::where('listing_id', '=', $listing_id)
        ->get();
        
        return view('admin.realestate.newsimages.index', compact('details','listing_id'));
    }
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $image = PropertyNewsImageModel::find($id);
        $listing_id = $image->listing_id;
        
        $deleteRecords = PropertyNewsImageModel::find($id)->delete();
        return redirect('/admin/uploadpropertynewsimage/'.$listing_id)->with('success_msg', 'Property Image Deleted successfully!');
    }
}

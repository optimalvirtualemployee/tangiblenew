<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\MovementModel;

class MovementController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $movement = MovementModel::get();
        return view('admin.watch.movement.index',compact('movement'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.watch.movement.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validate = $this->validate($request, [
            'movement' => 'required|regex:/^([a-zA-Z_]+)(\s[a-zA-Z_]+)*$/|unique:wa_movement',
            'status' => 'required'
        ]);

        $movement = MovementModel::create($validate);
        $movement->save();
        return redirect('/admin/movement')->with('success_msg','Movement Created successfully!');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $movement_data = MovementModel::find($id);
        return view('admin.watch.movement.edit', compact('movement_data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validate = $this->validate($request, [
            'movement' => 'required|regex:/^([a-zA-Z_]+)(\s[a-zA-Z_]+)*$/'
        ]);

        $movementUpdate = MovementModel::find($id);

        if ($movementUpdate->movement == $validate['movement']) {
            $movementUpdate->status = $request->input('status');
        } else {
            $validate = $this->validate($request, ['movement' => 'unique:wa_movement']);
            $movementUpdate->movement = $validate['movement'];
            $movementUpdate->status = $request->input('status');
        }

        $movementUpdate->save();

        return redirect('/admin/movement')->with('success_msg','Movement Updated successfully!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deleteRecords = MovementModel::find($id)->delete();
        return redirect('/admin/movement')->with('success_msg','Movement deleted successfully!');
    }
}

<?php

namespace App\Http\Controllers;

use App\FunctionTourbillionModel;
use Illuminate\Http\Request;

class FunctionTourbillionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $functions = FunctionTourbillionModel::get();
        return view('admin.watch.functionTourbillion.index',compact('functions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
         return view('admin.watch.functionTourbillion.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
         $validator = $this->validate($request, [
            "tourbillion" => 'required|unique:wa_factory_features_function_tourbillion',
             "status" => 'required'
        ]);

          $case = FunctionTourbillionModel::create($validator);
          $case->save();
       
      return redirect('/admin/functionTourbillion')->with('success_msg','Tourbillion Created successfully!');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $functions_data = FunctionTourbillionModel::find($id);
        return view('admin.watch.functionTourbillion.edit', compact('functions_data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = $this->validate($request, [
            "tourbillion" => 'required|unique:wa_factory_features_function_tourbillion',
            "status" => 'required'
        ]);
        
          $case = FunctionTourbillionModel::find($id);
          $case->tourbillion = $request->input('tourbillion');
          $case->status = $request->input('status');
          $case->save();
        
        return redirect('/admin/functionTourbillion')->with('success_msg','Tourbillion Updated successfully!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deleteRecords = FunctionTourbillionModel::find($id)->delete();
        return redirect('/admin/functionTourbillion')->with('success_msg','Tourbillion Deleted successfully!');
    }
}

<?php
namespace App\Http\Controllers;

use App\EnquiryModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\ProductOrderModel;
use App\OrderStatusHierarchyModel;

class AdminOrderDetailsController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        DB::enableQueryLog();
        $orders = ProductOrderModel::select('tbl_order.id as id', 'productId', 'category', DB::raw('CONCAT(users.first_name, " ", users.last_name) AS user_name'), DB::raw('CONCAT(tbl_agent.first_name, " ", tbl_agent.last_name) AS agent_name'), 'tbl_agencies.company_name as company_name')
            ->join('users', 'users.id', 'tbl_order.userID')
            ->join('tbl_agent', 'tbl_agent.id', 'tbl_order.agent_id')
            ->leftJoin('tbl_agencies', 'tbl_agencies.id', '=', 'tbl_agent.agency_id')
            ->get();

        //dd(DB::getQueryLog());
        return view('admin.admincommon.orderstatus.index')->with(array(
            'orders' => $orders
        ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $orderStatus = OrderStatusHierarchyModel::select('id','status', 'created_at')->where('enquiryId', '=', $id)->orderBy('created_at', 'ASC')->get();
        
        //dd($orderStatus);
        return view('admin.admincommon.orderstatus.orderdetails', compact('orderStatus'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

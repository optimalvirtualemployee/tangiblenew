<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\BuildYearModel;

class BuildYearController extends Controller
{
     function __construct()
    {
         $this->middleware('permission:automobile-list|automobile-create|automobile-edit|automobile-delete', ['only' => ['index','store']]);
         $this->middleware('permission:automobile-create', ['only' => ['create','store']]);
         $this->middleware('permission:automobile-edit', ['only' => ['edit','update']]);
         $this->middleware('permission:automobile-delete', ['only' => ['destroy']]);
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $build_year = BuildYearModel::get();
        return view('admin.realestate.build_year.index',compact('build_year'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.realestate.build_year.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validate = $this->validate($request, [
            'build_year' => 'required|digits:4|integer|min:1900|max:'.(date('Y')+1).'|unique:re_build_year',
            'status' => 'required'
        ]);

        $build_year = BuildYearModel::create($validate);
        $build_year->save();
        return redirect('/admin/build_year')->with('success_msg','Build Year added successfully!');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $build_year_data = BuildYearModel::find($id);
        return view('admin.realestate.build_year.edit', compact('build_year_data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validate = $this->validate($request, [
            'build_year' => 'required|digits:4|integer|min:1900|max:'.(date('Y')+1)
        ]);

        $build_yearUpdate = BuildYearModel::find($id);

        if ($build_yearUpdate->build_year == $validate['build_year']) {
            $build_yearUpdate->status = $request->input('status');
        } else {
            $validate = $this->validate($request, ['build_year' => 'unique:re_build_year']);
            $build_yearUpdate->build_year = $validate['build_year'];
            $build_yearUpdate->status = $request->input('status');
        }

        $build_yearUpdate->save();

        return redirect('/admin/build_year')->with('success_msg','Build Year Updated successfully!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deleteRecords = BuildYearModel::find($id)->delete();
        return redirect('/admin/build_year')->with('success_msg','Build Year deleted successfully!');
    }
}

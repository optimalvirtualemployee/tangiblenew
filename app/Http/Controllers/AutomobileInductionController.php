<?php

namespace App\Http\Controllers;

use App\AutomobileInductionModel;
use Illuminate\Http\Request;

class AutomobileInductionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $induction = AutomobileInductionModel::get();
        return view('admin.automobile.induction.index',compact('induction'));
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.automobile.induction.create');
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validate = $this->validate($request, [
            'induction' => 'required|unique:au_induction',
            'status' => 'required'
        ]);
        
        $induction = AutomobileInductionModel::create($validate);
        $induction->save();
        return redirect('/admin/induction')->with('success_msg','Induction Created successfully!');
    }
    
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $induction_data = AutomobileInductionModel::find($id);
        return view('admin.automobile.induction.edit', compact('induction_data'));
    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validate = $this->validate($request, [
            'induction' => 'required'
        ]);
        
        $inductionUpdate = AutomobileInductionModel::find($id);
        
        
        $inductionUpdate->induction = $validate['induction'];
        $inductionUpdate->status = $request->input('status');
        
        $inductionUpdate->save();
        
        return redirect('/admin/induction')->with('success_msg','Induction Updated successfully!');
    }
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deleteRecords = AutomobileInductionModel::find($id)->delete();
        return redirect('/admin/induction')->with('success_msg','Induction deleted successfully!');
    }
}

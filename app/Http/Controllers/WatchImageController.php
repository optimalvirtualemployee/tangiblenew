<?php

namespace App\Http\Controllers;

use App\WatchImageModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class WatchImageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function createImage(Request $request, $listing_id)
    {
        return view('admin.watch.images.create',
            compact('listing_id'));
    }
    
    public function create()
    {
        
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate([
            'photos'  => 'required',
            'photos.*' => 'mimes:jpg,jpeg,png,gif'
        ]);
        
        
        foreach ($request->file('photos') as $image){
            $uploadImage = new WatchImageModel();
            
            $extension = $image->getClientOriginalExtension();
            Storage::disk('public')->put($image->getFilename().'.'.$extension,  File::get($image));
            
            $uploadImage->images = $image->getClientMimeType();
            $uploadImage->filename = $image->getFilename().'.'.$extension;
            $uploadImage->listing_id = $request->input('listing_id');
            $uploadImage->save();
        }
        
        return redirect('/admin/uploadwatchimage/'.$request->input('listing_id'))->with('success_msg', 'Watch image Created successfully!');
    }
    
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $listing_id = $id;
        $details = WatchImageModel::where('listing_id', '=', $listing_id)
        ->get();
        
        return view('admin.watch.images.index', compact('details','listing_id'));
    }
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $image = WatchImageModel::find($id);
        $listing_id = $image->listing_id;
        
        $deleteRecords = WatchImageModel::find($id)->delete();
        return redirect('/admin/uploadwatchimage/'.$listing_id)->with('success_msg', 'Watch Image Deleted successfully!');
    }
}

<?php
namespace App\Http\Controllers;

use App\AutomobileDataListingModel;
use App\ContactusModel;
use App\CurrencyModel;
use App\PropertyDataModel;
use App\PropertyTypeModel;
use App\WatchDataListingModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\AgentModel;
use App\AutomobileImageModel;
use App\WatchImageModel;
use App\PropertyImageModel;

class MyListingController extends Controller
{

    public function index()
    {
        if (Auth::user() == null){
        return redirect('/');
        }
        
        $id = Auth::user()->id;

        $contactUs = ContactusModel::get()->first();
        $selectedCurrency = CurrencyModel::select('currency_code as code')->where('status', '=', '1')->first();

        //dd($selectedCurrency);

        currency()->setUserCurrency($selectedCurrency->code);
        $propertyType = PropertyTypeModel::get();

        $agent = AgentModel::where('userId', '=',$id)->first();

        $my_listing = array();
                
        $automobile_listing = array();
        $watch_listing = array();
        $property_listing = array();
        
        if ($agent != null) {
        $id = $agent->id;
        $automobile_listing = AutomobileDataListingModel::select('currency_code', 'au_automobile_detail.id as id', 'ad_title', 'build_year', 'neworused', 'value', 'odometer', 'au_transmission.transmission as transmission', 'rhdorlhd', 'au_automobile_detail.created_at as automobilecreationtime', 'tbl_country.filename as filename', 'tbl_country.countrycode as countrycode', 'tbl_agent.id as agent_id', 'tbl_agent.agency_id as agency_id')->join('tbl_currency', 'tbl_currency.id', '=', 'au_automobile_detail.currency_id')
            ->join('tbl_state', 'tbl_state.id', '=', 'au_automobile_detail.state_id')
            ->join('tbl_city', 'tbl_city.id', '=', 'au_automobile_detail.city_id')
            ->join('tbl_country', 'tbl_country.id', '=', 'au_automobile_detail.country_id')
            ->join('tbl_agent', 'tbl_agent.id', '=', 'au_automobile_detail.agent_id')
            ->join('re_build_year', 're_build_year.id', '=', 'au_automobile_detail.year_id')
            ->join('au_transmission', 'au_transmission.id', '=', 'au_automobile_detail.transmission_id')
            ->join('au_body_type', 'au_body_type.id', '=', 'au_automobile_detail.body_type_id')
            ->where('agent_id', '=', $id)
            ->whereIN('au_automobile_detail.id', function ($query) {
            $query->select('listing_id')
                ->from('au_automobile_images');
        })
            ->whereIN('au_automobile_detail.id', function ($query) {
            $query->select('listing_id')
                ->from('au_automobile_features');
        })
            ->whereIN('au_automobile_detail.id', function ($query) {
            $query->select('listing_id')
                ->from('au_automobile_comments');
        })
            ->where('au_automobile_detail.status', '=', '1')
            ->orderBy('au_automobile_detail.created_at', 'DESC')
            ->get();
        }
        
        if ($agent != null) {
            $id = $agent->id;
            $watch_listing = WatchDataListingModel::select('ad_title', 'wa_watch_detail.id as id' ,'currency_code', 'brand_name', 'model_name','watch_price as value', 'year_of_manufacture', 'case_diameter', 'city_name',
                'state_name', 'watch_price', 'tbl_country.filename as filename', 'tbl_country.countrycode as countrycode', 'tbl_agent.id as agent_id', 'tbl_agent.agency_id as agency_id')
                ->join('tbl_currency', 'tbl_currency.id', '=', 'wa_watch_detail.currency_id')
                ->leftJoin('wa_year_of_manufacture', 'wa_year_of_manufacture.id', '=', 'wa_watch_detail.year_of_manufacture_id')
                ->leftJoin('wa_case_diameter','wa_case_diameter.id', '=', 'wa_watch_detail.case_diameter_id' )
                ->join('tbl_state', 'tbl_state.id', '=', 'wa_watch_detail.state_id')
                ->join('tbl_city', 'tbl_city.id', '=', 'wa_watch_detail.city_id')
                ->join('tbl_country', 'tbl_country.id', '=', 'wa_watch_detail.country_id')
                ->join('tbl_agent', 'tbl_agent.id', '=', 'wa_watch_detail.agent_id')
                ->whereIN('wa_watch_detail.id', function($query){
                    $query->select('listing_id')
                    ->from('wa_watch_images');
                })
                ->whereIN('wa_watch_detail.id', function($query){
                    $query->select('listing_id')
                    ->from('wa_watch_features');
                })
                ->whereIN('wa_watch_detail.id', function($query){
                    $query->select('listing_id')
                    ->from('wa_watch_comment');
                })->where('agent_id', '=', $id)
               // ->where('wa_watch_detail.status','=', '1')
                ->orWhere('wa_watch_detail.case_diameter_id','=', 'NULL')
                ->orderBy('wa_watch_detail.created_at', 'DESC')
                ->get();
        }

        if ($agent != null) {

            $id = $agent->id;

           // dd( $id);

            $property_listing = PropertyDataModel::select('re_property_details.id as id', 'currency_code' ,'ad_title', 'land_size as size','property_price as value', 'number_of_bedrooms as bedrooms',
                'metric', 'tbl_country.filename as filename', 'tbl_country.countrycode as countrycode','tbl_agent.id as agent_id', 'tbl_agent.agency_id as agency_id', 're_property_details.created_at as creationtime')
                ->join('tbl_currency', 'tbl_currency.id', '=', 're_property_details.currency_id')
                ->join('tbl_city', 'tbl_city.id', '=', 're_property_details.city_id')
                ->join('tbl_country', 'tbl_country.id', '=', 're_property_details.country_id')
                ->join('tbl_agent', 'tbl_agent.id', '=', 're_property_details.agent_id')
                ->join('re_build_year', 're_build_year.id', '=', 're_property_details.year_built_id')
                ->join('re_beds', 're_beds.id', '=', 're_property_details.bed_id')
                ->whereIN('re_property_details.id', function($query){
                    $query->select('listing_id')
                    ->from('re_property_images');
                })
                ->whereIN('re_property_details.id', function($query){
                    $query->select('listing_id')
                    ->from('re_property_features');
                })
                ->whereIN('re_property_details.id', function($query){
                    $query->select('listing_id')
                    ->from('re_property_comment');
                })->where('agent_id', '=', $id)
                ->where('re_property_details.status','=', '1')
                ->orderBy('re_property_details.created_at', 'DESC')
                ->get();
        }
        
        $automobile_images = AutomobileImageModel::get();
        $watch_images = WatchImageModel::get();
        $property_images = PropertyImageModel::get();

        //dd($watch_listing);
        
        
        return view('tangiblehtml.my-listings', compact('contactUs', 'propertyType', 'automobile_listing', 'automobile_images', 'watch_listing'
            , 'watch_images', 'property_listing', 'property_images'));
    }
}

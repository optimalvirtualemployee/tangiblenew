<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\NoOfBathroomsModel;

class NoOfBathroomsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $no_of_bathrooms = NoOfBathroomsModel::get();
        return view('admin.realestate.no_of_bathrooms.index',compact('no_of_bathrooms'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.realestate.no_of_bathrooms.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validate = $this->validate($request, [
            'number_of_bathrooms' => 'required|numeric|unique:re_bathrooms',
            'status' => 'required'
        ]);

        $no_of_bathrooms = NoOfBathroomsModel::create($validate);
        $no_of_bathrooms->save();
        return redirect('/admin/no_of_bathrooms')->with('success_msg','No. of Bathrooms Created successfully!');
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $no_of_bathrooms_data = NoOfBathroomsModel::find($id);
        return view('admin.realestate.no_of_bathrooms.edit', compact('no_of_bathrooms_data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validate = $this->validate($request, [
            'number_of_bathrooms' => 'required|numeric'
        ]);

        $no_of_bathroomsUpdate = NoOfBathroomsModel::find($id);

        if ($no_of_bathroomsUpdate->number_of_bathrooms == $validate['number_of_bathrooms']) {
            $no_of_bathroomsUpdate->status = $request->input('status');
        } else {
            $validate = $this->validate($request, ['number_of_bathrooms' => 'unique:re_bathrooms']);
            $no_of_bathroomsUpdate->number_of_bathrooms = $validate['number_of_bathrooms'];
            $no_of_bathroomsUpdate->status = $request->input('status');
        }

        $no_of_bathroomsUpdate->save();

        return redirect('/admin/no_of_bathrooms')->with('success_msg','No. of Bathrooms Updated successfully!'); 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deleteRecords = NoOfBathroomsModel::find($id)->delete();
        return redirect('/admin/no_of_bathrooms')->with('success_msg','No. of Bathrooms deleted successfully!');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CommercialPropertyTypeModel extends Model
{
    protected $table = 're_property_type_commercial';
    protected $fillable = ['property_type', 'status'];
}

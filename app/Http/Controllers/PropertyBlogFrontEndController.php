<?php

namespace App\Http\Controllers;

use App\ContactusModel;
use App\CurrencyModel;
use App\PropertyTypeModel;
use Illuminate\Http\Request;
use App\PropertyBlogModel;
use App\PropertyBlogImageModel;

class PropertyBlogFrontEndController extends Controller
{
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $contactUs = ContactusModel::get()->first();
        $selectedCurrency = CurrencyModel::select('currency_code as code')->where('status', '=', '1')->first();
        
        currency()->setUserCurrency($selectedCurrency->code);
        $propertyType = PropertyTypeModel::get();
        
        $blog = PropertyBlogModel::find($id);
        
        $blog_images = PropertyBlogImageModel::get();
        
        
        $type_blog = PropertyBlogModel::select('re_blog.id as id','filename','title', 'blogPost', 're_blog.created_at as created_at', 'blogType', 'url')
        ->join('re_property_blog_images', 're_property_blog_images.listing_id', '=', 're_blog.id')
        ->where('status', '=', '1')->where('re_blog.id', '!=', $id)->orderBy('re_blog.created_at','desc')->limit('4')->get();
        
        //dd($type_blog);
        
        return view('tangiblehtml.property-blog', compact('contactUs', 'propertyType', 'blog', 'blog_images', 'type_blog'));
    }
}

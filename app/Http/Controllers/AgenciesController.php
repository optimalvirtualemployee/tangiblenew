<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\AgenciesModel;
use App\AgenciesImageModel;
use App\CityModel;
use App\CountryModel;
use App\StateModel;
use App\Timezone;
use App\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use App\UserPermissionModel;
use App\CategoriesModel;
use Stripe;
use URL;

class AgenciesController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:dealer-list|dealer-create|dealer-edit|dealer-delete', ['only' => ['index','store']]);
        $this->middleware('permission:dealer-create', ['only' => ['create','store']]);
        $this->middleware('permission:dealer-edit', ['only' => ['edit','update']]);
        $this->middleware('permission:dealer-delete', ['only' => ['destroy']]);
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $agencies = AgenciesModel::select('tbl_agencies.id as id', 'status' ,'first_name', 'last_name', 'street_1', 'street_2' ,'phone_no', 'filename')
        ->join('tbl_agencies_images', 'tbl_agencies_images.listing_id', '=', 'tbl_agencies.id')
        ->get();
        
        return view('admin.admincommon.agencies_msf.display', compact('agencies'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {   
        
        $country_data = CountryModel::get();
        $timezone = Timezone::Orderby('offset')->get();
        $category_data = CategoriesModel::get();
        
        return view('admin.admincommon.agencies_msf.create', compact('country_data', 'timezone', 'category_data'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'first_name' => 'required',
            'last_name' => 'required',
            'company_name' => 'required',
            'email' => 'required|email|unique:users,email',
            'password' => 'required|same:confirmpassword',
            'countryId' => 'required',
            'cityId' => 'required',
            'street_1' => 'required',
            'street_2' => 'required',
            'postal_code' => 'required',
            'mobile' => 'required',
            'whatsapp' => 'required',
            'categoryId' => 'required',
            'status' => 'required',
            'photos' => 'required',
            'photos.*' => 'image|mimes:jpeg,png,jpg,gif,svg'
        ]);
        
        $createAgencies = new AgenciesModel();
        
        $createAgencies->first_name = $request->input('first_name');
        $createAgencies->last_name = $request->input('last_name');
        $createAgencies->country_id = $request->input('countryId');
        $createAgencies->company_name = $request->input('company_name');
        if($request->input('stateId') != '')
        $createAgencies->state_id = $request->input('stateId');
        $createAgencies->city_id = $request->input('cityId');
        $createAgencies->street_1 = $request->input('street_1');
        $createAgencies->street_2 = $request->input('street_2');
        $createAgencies->postal_code = $request->input('postal_code');
        $createAgencies->phone_no = $request->input('mobile');
        $createAgencies->whatsapp = $request->input('whatsapp');
        $createAgencies->email = $request->input('email');
        $createAgencies->timezone_id = $request->input('timezone');
        $createAgencies->category_id = $request->input('categoryId');
        $createAgencies->status = $request->input('status');
        
        
        //DB::enableQueryLog();
        $input['first_name'] = $request->input('first_name');
        $input['last_name'] = $request->input('last_name');
        $input['email'] = $request->input('email');
        $input['password'] = Hash::make($request->input('password'));
        $input['usertype'] = 2;
        $input['role_id'] = 2;
        
        
        $user = User::create($input);
        $user->assignRole('Dealer');
        
       // dd(DB::getQueryLog());
        
        $role = Role::findByName('Dealer');
        $permission = Permission::get();
        $rolePermissions = Permission::join("role_has_permissions","role_has_permissions.permission_id","=","permissions.id")
        ->where("role_has_permissions.role_id",$role->id)
        ->get();
        
        if($rolePermissions !=null){
            foreach ($rolePermissions as $permission){
                
                $userPermission = new UserPermissionModel();
                
                $userPermission->userId = $user->id;
                $userPermission->permissionId = $permission->id;
                
                $userPermission->save();
            }
        }
        
        
        $createAgencies->userId = $user->id;


       $category = $request->input('categoryId');
       if($category ==  1){

            // Creating Connect Account Process
            // Set your secret key. Remember to switch to your live secret key in production.
            // See your keys here: https://dashboard.stripe.com/apikeys
            // Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));
            Stripe\Stripe::setApiKey(config('services.stripe.stripe_secret'));

            $account = \Stripe\Account::create([
            'country' => 'US',
            'type' => 'express',
            ]);

            $accountDetails = $account->jsonSerialize();
            $accountId = $accountDetails['id'];

            // Generating Onboarding Process Account Link

            $baseUrl = URL::to('/');

            $account_links = \Stripe\AccountLink::create([
            'account' => $accountId,
            'refresh_url' => $baseUrl."/reauth/".$accountId,
            'return_url' => $baseUrl,
            'type' => 'account_onboarding',
            ]);

            $accountLinkDetails = $account_links->jsonSerialize();
            $onboardUrl = $accountLinkDetails['url'];

            $createAgencies->stripeAccountId = $accountId;

            $createAgencies->accountLink = $onboardUrl;

           // dd($accountDetails);

       }


           

        $createAgencies->save();
        
        $listing_id = $createAgencies->id;
        
        
        if($request->file('photos') ?? ''){
        foreach ($request->file('photos') as $image){
            $uploadImage = new AgenciesImageModel();
            
            $extension = $image->getClientOriginalExtension();
            Storage::disk('public')->put($image->getFilename().'.'.$extension,  File::get($image));
            
            $uploadImage->images = $image->getClientMimeType();
            $uploadImage->filename = $image->getFilename().'.'.$extension;
            $uploadImage->listing_id = $listing_id;
            $uploadImage->save();
        }
        }

        
        return redirect('/admin/agencies')->with('success_msg', 'Dealers Created successfully!');
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $agencies = AgenciesModel::find($id);
        $country_data = CountryModel::get();
        $state_data = StateModel::get();
        $city_data = CityModel::get();
        $timezone_data = Timezone::Orderby('offset')->get();
        $category_data = CategoriesModel::get();
        
        $citySelected = CityModel::find($agencies->city_id);
        $stateSelected = StateModel::find($agencies->state_id);
        $countrySelected = CountryModel::find($agencies->country_id);
        $timezoneSelected = Timezone::find($agencies->timezone_id);
        $categorySelected = CategoriesModel::find($agencies->category_id);
        
        
        $images_data = AgenciesImageModel::where('listing_id', '=', $id)->get();
        
        return view('admin.admincommon.agencies_msf.edit', compact('agencies', 'country_data', 'citySelected', 'stateSelected',
            'countrySelected', 'state_data', 'city_data', 'images_data', 'timezone_data', 'timezoneSelected', 'category_data',
            'categorySelected'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $updateAgencies = AgenciesModel::find($id);
        $this->validate($request, [
            'first_name' => 'required',
            'last_name' => 'required',
            'company_name' => 'required',
            'email' => 'required|email|unique:users,email,'. $updateAgencies->userId,
            'password' => 'same:confirmpassword',
            'countryId' => 'required',
            'cityId' => 'required',
            'street_1' => 'required',
            'street_2' => 'required',
            'postal_code' => 'required',
            'mobile' => 'required',
            'whatsapp' => 'required',
            'categoryId' => 'required',
            'status' => 'required',
            'photos.*' => 'image|mimes:jpeg,png,jpg,gif,svg'
        ]);
        
        
        $updateAgencies->first_name = $request->input('first_name');
        $updateAgencies->last_name = $request->input('last_name');
        $updateAgencies->country_id = $request->input('countryId');
        $updateAgencies->company_name = $request->input('company_name');
        if($request->input('stateId') != '')
        $updateAgencies->state_id = $request->input('stateId');
        $updateAgencies->city_id = $request->input('cityId');
        $updateAgencies->street_1 = $request->input('street_1');
        $updateAgencies->street_2 = $request->input('street_2');
        $updateAgencies->postal_code = $request->input('postal_code');
        $updateAgencies->phone_no = $request->input('mobile');
        $updateAgencies->whatsapp = $request->input('whatsapp');
        $updateAgencies->email = $request->input('email');
        $updateAgencies->timezone_id = $request->input('timezone');
        $updateAgencies->status = $request->input('status');
        $updateAgencies->category_id = $request->input('categoryId');
        
        
        $input['first_name'] = $request->input('first_name');
        $input['last_name'] = $request->input('last_name');
        $input['email'] = $request->input('email');
        
        if(!empty($request->input('password'))){
            $input['password'] = Hash::make($request->input('password'));
        }
        
        
        $user = User::find($updateAgencies->userId);
        $user->update($input);
        
        //dd($user);
        
        $updateAgencies->save();
        
        
        if($request->file('photos') ?? ''){
            foreach ($request->file('photos') as $image){
                $uploadImage = new AgenciesImageModel();
                
                $extension = $image->getClientOriginalExtension();
                Storage::disk('public')->put($image->getFilename().'.'.$extension,  File::get($image));
                
                $uploadImage->images = $image->getClientMimeType();
                $uploadImage->filename = $image->getFilename().'.'.$extension;
                $uploadImage->listing_id = $id;
                $uploadImage->save();
            }
        }
        return redirect('/admin/agencies')->with('success_msg', 'Dealers Updated successfully!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
         $dealer= AgenciesModel::find($id);
        
         if(User::find($dealer->userId) != null)
         User::find($dealer->userId)->delete();
        
        $deleteRecords = AgenciesModel::find($id)->delete();
        
        return redirect('/admin/agencies')->with('success_msg', 'Agencies Deleted successfully!');
    }
}

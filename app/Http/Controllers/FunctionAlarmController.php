<?php

namespace App\Http\Controllers;

use App\FunctionAlarmModel;
use Illuminate\Http\Request;

class FunctionAlarmController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $functions = FunctionAlarmModel::get();
        return view('admin.watch.functionAlarm.index',compact('functions'));
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.watch.functionAlarm.create');
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
        $validator = $this->validate($request, [
            "alarm" => 'required|unique:wa_factory_features_function_alarm,alarm',
            "status" => 'required'
        ]);
        
        $case = new FunctionAlarmModel;
        $case->alarm = $request->input('alarm');
        $case->status = $request->input('status');
        $case->save();
        
        return redirect('/admin/functionAlarm')->with('success_msg','Alarm Created successfully!');
    }
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $functions_data = FunctionAlarmModel::find($id);
        return view('admin.watch.functionAlarm.edit', compact('functions_data'));
    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = $this->validate($request, [
            "alarm" => 'required|unique:wa_factory_features_function_alarm,alarm,' . $id,
            "status" => 'required'
        ]);
        $case = FunctionAlarmModel::find($id);
        $case->alarm = $request->input('alarm');
        $case->status = $request->input('status');
        $case->save();
        
        return redirect('/admin/functionAlarm')->with('success_msg','Alarm Updated successfully!');
    }
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deleteRecords = FunctionAlarmModel::find($id)->delete();
        return redirect('/admin/functionAlarm')->with('success_msg','Alarm Deleted successfully!');
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\NoOfBalconiesModel;

class NoOfParkingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $no_of_balconies = NoOfBalconiesModel::get();
        return view('admin.realestate.no_of_balconies.index',compact('no_of_balconies'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.realestate.no_of_balconies.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validate = $this->validate($request, [
            'number_of_balconies' => 'required|numeric|unique:re_balcony',
            'status' => 'required'
        ]);

        $no_of_balconies = NoOfBalconiesModel::create($validate);
        $no_of_balconies->save();
        return redirect('/admin/no_of_parking')->with('success_msg','No. of Balconies Created successfully!');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $no_of_balconies_data = NoOfBalconiesModel::find($id);
        return view('admin.realestate.no_of_balconies.edit', compact('no_of_balconies_data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validate = $this->validate($request, [
            'number_of_balconies' => 'required|numeric'
        ]);

        $no_of_balconiesUpdate = NoOfBalconiesModel::find($id);

        if ($no_of_balconiesUpdate->number_of_balconies == $validate['number_of_balconies']) {
            $no_of_balconiesUpdate->status = $request->input('status');
        } else {
            $validate = $this->validate($request, ['number_of_balconies' => 'unique:re_balcony']);
            $no_of_balconiesUpdate->number_of_balconies = $validate['number_of_balconies'];
            $no_of_balconiesUpdate->status = $request->input('status');
        }

        $no_of_balconiesUpdate->save();

        return redirect('/admin/no_of_parking')->with('success_msg','No. of Balconies Updated successfully!'); 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deleteRecords = NoOfBalconiesModel::find($id)->delete();
        return redirect('/admin/no_of_parking')->with('success_msg','No. of Balconies deleted successfully!');
    }
}

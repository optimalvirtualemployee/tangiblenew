<?php

namespace App\Http\Controllers;

use App\AutomobileBodyTypeModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use App\AutomobileBodyTypeImageModel;

class AutomobileBodyTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $body_type = AutomobileBodyTypeModel::get();
        
        $images = AutomobileBodyTypeImageModel::get();
        return view('admin.automobile.body_typemsf.index',compact('body_type', 'images'));
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.automobile.body_typemsf.create');
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validate = $this->validate($request, [
            'body_type' => 'required|unique:au_body_type',
            'status' => 'required'
        ]);
        
        $body_type = AutomobileBodyTypeModel::create($validate);
        $body_type->save();
        
        $listing_id = $body_type->id;
        
        if($request->file('photos') ?? ''){
            foreach ($request->file('photos') as $image){
                $uploadImage = new AutomobileBodyTypeImageModel();
                
                $extension = $image->getClientOriginalExtension();
                Storage::disk('public')->put($image->getFilename().'.'.$extension,  File::get($image));
                
                $uploadImage->images = $image->getClientMimeType();
                $uploadImage->filename = $image->getFilename().'.'.$extension;
                $uploadImage->listing_id = $listing_id;
                $uploadImage->save();
            }
        }
        return redirect('/admin/body_type')->with('success_msg','Body Type Created successfully!');
    }
    
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $body_type_data = AutomobileBodyTypeModel::find($id);
        
        $images_data = AutomobileBodyTypeImageModel::where('listing_id', '=', $id)->get();
        return view('admin.automobile.body_typemsf.edit', compact('body_type_data','images_data'));
    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validate = $this->validate($request, [
            'body_type' => 'required'
        ]);
        
        $body_typeUpdate = AutomobileBodyTypeModel::find($id);
        
        
        $body_typeUpdate->body_type = $validate['body_type'];
        $body_typeUpdate->status = $request->input('status');
        
        $body_typeUpdate->save();
        
        if($request->file('photos') ?? ''){
            foreach ($request->file('photos') as $image){
                $uploadImage = new AutomobileBodyTypeImageModel();
                
                $extension = $image->getClientOriginalExtension();
                Storage::disk('public')->put($image->getFilename().'.'.$extension,  File::get($image));
                
                $uploadImage->images = $image->getClientMimeType();
                $uploadImage->filename = $image->getFilename().'.'.$extension;
                $uploadImage->listing_id = $id;
                $uploadImage->save();
            }
        }
        
        return redirect('/admin/body_type')->with('success_msg','Body Type Updated successfully!');
    }
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deleteRecords = AutomobileBodyTypeModel::find($id)->delete();
        return redirect('/admin/body_type')->with('success_msg','Body Type deleted successfully!');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function imageDestroy($id)
    {
        $deleteRecords = AutomobileBodyTypeImageModel::find($id)->delete();
        return redirect('/admin/body_type')->with('success_msg','Body Type Image deleted successfully!');
    }
}

<?php

namespace App\Http\Controllers;

use App\PropertyFeaturesCommercialListingModel;
use Illuminate\Http\Request;

class PropertyFeaturesCommercialListingController extends Controller
{
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $details = PropertyFeaturesCommercialListingModel::get();
        
        return view('admin.realestate.featureslisting_commercial.index', compact('details'));
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.realestate.featureslisting_commercial.create');
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->validate([
            
            
            'propertyName' => ['required','unique:re_properties_features_listing_commercial,property_name'],
            'status' => ['required']
        ]);
        
        $createFeature = new PropertyFeaturesCommercialListingModel();
        
        $createFeature->property_name = $request->input('propertyName');
        $createFeature->status = $request->input('status');
        
        $createFeature->save();
        
        return redirect('/admin/commercial_propertyfeature')->with('success_msg', 'Property Feature Added successfully!');
    }
    
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $feature_data = PropertyFeaturesCommercialListingModel::find($id);
        
        return view('admin.realestate.featureslisting_commercial.edit',compact('feature_data'));
    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->validate([
            
            
            'propertyName' => ['required','unique:re_properties_features_listing_commercial,property_name,'.$id],
            'status' => ['required']
        ]);
        
        $updateFeature = PropertyFeaturesCommercialListingModel::find($id);;
        
        $updateFeature->property_name = $request->input('propertyName');
        $updateFeature->status = $request->input('status');
        
        $updateFeature->save();
        
        return redirect('/admin/commercial_propertyfeature')->with('success_msg', 'Property Feature Updated successfully!');
    }
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deleteRecords = PropertyFeaturesCommercialListingModel::find($id)->delete();
        return redirect('/admin/commercial_propertyfeature')->with('success_msg','Property Feature deleted successfully!');
    }
}

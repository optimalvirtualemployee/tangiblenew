<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\PackageListingRangeManagementModel;
use App\PackageForPrivateSellersModel;

class PackageListingRangeManagementController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pricing = PackageListingRangeManagementModel::select('tbl_package_pricing_range.id as id','package_for_private_sellers as package_name', 'rangeFrom', 'rangeTo','pricing')
                    ->join('tbl_package_for_private_sellers', 'tbl_package_for_private_sellers.id', '=', 'tbl_package_pricing_range.package_id')
                    ->get();
        
         return view('admin.admincommon.package_pricing_range.display', compact('pricing'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       $packages = PackageForPrivateSellersModel::get();
       
       return view('admin.admincommon.package_pricing_range.create', compact('packages'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        //dd($request);
        $request->validate([
            'package' => ['required'],
            'from' => ['required'],
            'to' => ['required'],
            'pricing' => ['required'] 
            
        ]);
        
        $fromRange = $request->input('from');
        $toRange = $request->input('to');
        $pricing = $request->input('pricing');
        
        
        for ($i = 0; $i < sizeof($fromRange); $i++) {
            $createRange = new PackageListingRangeManagementModel();
            if($fromRange[$i] != null){
            $createRange->package_id = $request->input('package');
            $createRange->rangeFrom = $fromRange[$i];
            $createRange->rangeTo = $toRange[$i];
            $createRange->pricing = $pricing[$i];
            
            $createRange->save();
            }
        }

        return redirect('/admin/packagepricingrange')->with('success_msg', 'Pricing Range Created successfully!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $packagesPricing = PackageListingRangeManagementModel::find($id);
        
        $packages = PackageForPrivateSellersModel::get();
        $package_data = PackageForPrivateSellersModel::find($packagesPricing->package_id);
        
        return view('admin.admincommon.package_pricing_range.edit', compact('packagesPricing', 'packages', 'package_data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'package' => ['required'],
            'from' => ['required'],
            'to' => ['required'],
            'pricing' => ['required']
            
        ]);
        
        $updateRange = PackageListingRangeManagementModel::find($id);
        
        $updateRange->package_id = $request->input('package');
        $updateRange->rangeFrom = $request->input('from');
        $updateRange->rangeTo = $request->input('to');
        $updateRange->pricing = $request->input('pricing');
        
        $updateRange->save();
        
        return redirect('/admin/packagepricingrange')->with('success_msg', 'Pricing Range Edit successfully!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deleteRecords = PackageListingRangeManagementModel::find($id)->delete();
        return redirect('/admin/packagepricingrange')->with('success_msg', 'Pricing Range Deleted successfully!');
    }
}

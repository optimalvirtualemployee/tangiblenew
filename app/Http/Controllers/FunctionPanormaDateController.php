<?php

namespace App\Http\Controllers;

use App\FunctionPanormaDateModel;
use Illuminate\Http\Request;

class FunctionPanormaDateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $functions = FunctionPanormaDateModel::get();
        return view('admin.watch.functionPanormaDate.index',compact('functions'));
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.watch.functionPanormaDate.create');
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
        $validator = $this->validate($request, [
            "panorma_date" => 'required|unique:wa_factory_features_function_panormaDate',
            "status" => 'required'
        ]);
        
        $case = FunctionPanormaDateModel::create($validator);
        $case->save();
        
        return redirect('/admin/functionPanormaDate')->with('success_msg','Panorma Date Created successfully!');
    }
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $functions_data = FunctionPanormaDateModel::find($id);
        return view('admin.watch.functionPanormaDate.edit', compact('functions_data'));
    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = $this->validate($request, [
            "panorma_date" => 'required|unique:wa_factory_features_function_panormaDate',
            "status" => 'required'
        ]);
            
            $case = FunctionPanormaDateModel::find($id);
            $case->panorma_date = $request->input('panorma_date');
            $case->status = $request->input('status');
            $case->save();
        
        return redirect('/admin/functionPanormaDate')->with('success_msg','Panorma Date Updated successfully!');
    }
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deleteRecords = FunctionPanormaDateModel::find($id)->delete();
        return redirect('/admin/functionPanormaDate')->with('success_msg','Panorma Date Deleted successfully!');
    }
}

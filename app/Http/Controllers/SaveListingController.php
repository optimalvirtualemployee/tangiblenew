<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\CategoriesModel;
use App\SavedListingModel;

class SaveListingController extends Controller
{
    public function saveListing(Request $request){
        
        $user_id = Auth::user()->id;
        $listing_id = $request->listingId;
        $category = $request->category;

        $category_id = CategoriesModel::select('id')->firstWhere("website_category", "=", $category);
        
        $createSave = new SavedListingModel();
        
        $createSave->userId = $user_id;
        $createSave->listingId = $listing_id;
        $createSave->categoryId = $category_id->id;
        
        $createSave->save();
        
        return "success";
        
    }
}

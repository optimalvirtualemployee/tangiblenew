<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DealerPasswordResetController extends Controller
{
    public function index()
    {
        return view('dealer.resetpassword');
    }
}

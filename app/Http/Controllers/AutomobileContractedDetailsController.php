<?php

namespace App\Http\Controllers;

use App\AutomobileContractedDetailsModel;
use Illuminate\Http\Request;

class AutomobileContractedDetailsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $contractedAutomobile = AutomobileContractedDetailsModel::select('ad_title', 'tbl_agent.first_name as agent_first_name',
            'tbl_agent.last_name as agent_last_name' , 'tbl_customer.first_name as customer_first_name', 'tbl_customer.last_name as customer_last_name')
            ->join('tbl_agent', 'tbl_agent.id', '=', 'au_customer_product-details.dealer_id')
            ->join('tbl_customer', 'tbl_customer.id', '=', 'au_customer_product-details.customer_id')
            ->join('au_automobile_detail', 'au_automobile_detail.id', '=', 'au_customer_product-details.product_id')
            ->get();
            
            
            // dd($contractedWatch);
            
            return view('admin.admincommon.contractedautomobile.index', compact('contractedAutomobile'));
    }

    
}

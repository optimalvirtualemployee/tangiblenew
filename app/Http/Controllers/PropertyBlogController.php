<?php

namespace App\Http\Controllers;

use App\PropertyBlogModel;
use App\PropertyBlogImageModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class PropertyBlogController extends Controller
{
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $blogs = PropertyBlogModel::get();

        return view('admin.realestate.blog_msf.index')->with(array(
            'blogs' => $blogs
        ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return view('admin.realestate.blog_msf.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
         $data = $request->validate([
            
            
            'title' => ['required','unique:re_blog,title'],
            'blogPost' => ['required'],
            'blogType' => ['required'],
            'status' => ['required'],
            'photos.*' => 'image|mimes:jpeg,png,jpg,gif,svg'
            
        ]);
        
        
        $createPropertyBlog = new PropertyBlogModel();

        $createPropertyBlog->title = $request->input('title');
        $createPropertyBlog->blogPost = $request->input('blogPost');
        $createPropertyBlog->blogType = $request->input('blogType');
        if($request->input('url') != null)
        $createPropertyBlog->url = $request->input('url');
        $createPropertyBlog->status = $request->input('status');

        $createPropertyBlog->save();
        
        $listing_id = $createPropertyBlog->id;
        
        if($request->file('photos') ?? ''){
        foreach ($request->file('photos') as $image){
            $uploadImage = new PropertyBlogImageModel();
            
            $extension = $image->getClientOriginalExtension();
            Storage::disk('public')->put($image->getFilename().'.'.$extension,  File::get($image));
            
            $uploadImage->images = $image->getClientMimeType();
            $uploadImage->filename = $image->getFilename().'.'.$extension;
            $uploadImage->listing_id = $listing_id;
            $uploadImage->save();
        }
        }
        return redirect('/admin/propertyblog')->with('success_msg', 'Property Blog Created successfully!');
        
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
        $blog = PropertyBlogModel::find($id);
        
        $images_data = PropertyBlogImageModel::where('listing_id', '=', $id)->get();
        
        return view('admin.realestate.blog_msf.edit', compact('blog','images_data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       $data = $request->validate([
            
            
           'title' => ['required','unique:re_blog,title,' . $id],
            'blogPost' => ['required'],
            'blogType' => ['required'],
            'status' => ['required'],
            'photos.*' => 'image|mimes:jpeg,png,jpg,gif,svg'
            
        ]);
        
        $updatePropertyBlog = PropertyBlogModel::find($id);

        $updatePropertyBlog->title = $request->input('title');
        $updatePropertyBlog->blogPost = $request->input('blogPost');
        $updatePropertyBlog->blogType = $request->input('blogType');
        $updatePropertyBlog->url = $request->input('url');
        $updatePropertyBlog->status = $request->input('status');

        $updatePropertyBlog->save();
        
        if($request->file('photos') ?? ''){
            foreach ($request->file('photos') as $image){
                $uploadImage = new PropertyBlogImageModel();
                
                $extension = $image->getClientOriginalExtension();
                Storage::disk('public')->put($image->getFilename().'.'.$extension,  File::get($image));
                
                $uploadImage->images = $image->getClientMimeType();
                $uploadImage->filename = $image->getFilename().'.'.$extension;
                $uploadImage->listing_id = $id;
                $uploadImage->save();
            }
        }

        return redirect('/admin/propertyblog')->with('success_msg', 'Property Blog Updated successfully!');
        
    }
    
    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deleteRecords = PropertyBlogModel::find($id)->delete();
        return redirect('/admin/propertyblog')->with('success_msg', 'Property Blog Deleted successfully!');
        
    }
}

<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\AgenciesImageModel;
use App\AgenciesModel;
use App\CategoriesModel;
use App\CityModel;
use App\PropertyBlogImageModel;
use App\PropertyDataModel;
use App\NoOfBedroomsModel;
use App\NoOfBathroomsModel;
use App\PropertyTypeModel;
use App\PropertyImageModel;
use App\PropertyBlogModel;
use App\StateModel;
use App\CurrencyModel;
use App\AgentModel;
use App\PropertyCommentModel;
use App\PropertyFeaturesModel;
use App\AgentImageModel;
use App\ContactusModel;
use App\PropertyNewsModel;
use App\PropertyNewsImageModel;
use App\PropertyFeaturesListingModel;
use Crypt;
use App\SavedListingModel;
use App\EnquiryModel;
use App\MyMessageModel;
use App\SubmitSuggestPriceModel;
use App\ProductOrderModel;
use App\MessageModel;
use App\CountryModel;
use Illuminate\Support\Facades\Mail;
use App\User;
use App\Mail\ListingEnquiry;


class TangibleListingRealEstate extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        //DB::enableQueryLog();
        $images = PropertyImageModel::get();
        $contactUs = ContactusModel::get()->first();
        $selectedCurrency = CurrencyModel::select('currency_code as code')->where('status', '=', '1')->first();
        
        currency()->setUserCurrency($selectedCurrency->code);
        $propertyType = PropertyTypeModel::get();

        $propertyListing = PropertyDataModel::select('currency_code','metric','re_property_details.id as id', 'ad_title', 
            'property_price', 'land_size', 'number_of_bedrooms', 'number_of_bathrooms', 'property_type','tbl_country.filename as filename','tbl_country.countrycode as countrycode',
            'tbl_agent.id as agent_id', 'tbl_agent.agency_id as agency_id', 're_property_details.status as status', 'tbl_agencies.company_name as agency_name')
            ->join('re_beds', 're_beds.id', '=', 're_property_details.bed_id')
            ->join('tbl_city', 'tbl_city.id', '=', 're_property_details.city_id')
            ->join('tbl_country', 'tbl_country.id', '=', 're_property_details.country_id')
            ->join('re_bathrooms', 're_bathrooms.id', '=', 're_property_details.bathroom_id')
            ->join('tbl_currency', 'tbl_currency.id', '=', 're_property_details.currency_id')
            ->join('re_property_type', 're_property_type.id', '=', 're_property_details.property_type_id')
            ->join('tbl_agent', 'tbl_agent.id', '=', 're_property_details.agent_id')
            ->leftJoin('tbl_agencies', 'tbl_agencies.id', '=', 'tbl_agent.agency_id')
            ->whereIN('re_property_details.id', function($query){
                $query->select('listing_id')
                ->from('re_property_images');
                    })
             ->whereIN('re_property_details.id', function($query){
                 $query->select('listing_id')
                 ->from('re_property_features');
                    })
             ->whereIN('re_property_details.id', function($query){
                   $query->select('listing_id')
                   ->from('re_property_comment');
             })->where('re_property_details.status','=', '1')
            ->get();
                    
            
            $latest_properties = PropertyDataModel::select('currency_code','re_property_details.id as id', 'ad_title', 'property_price', 'land_size', 'number_of_bedrooms', 'number_of_bathrooms', 'property_type')->join('re_beds', 're_beds.id', '=', 're_property_details.bed_id')
            ->join('re_bathrooms', 're_bathrooms.id', '=', 're_property_details.bathroom_id')
            ->join('tbl_currency', 'tbl_currency.id', '=', 're_property_details.currency_id')
            ->join('re_property_type', 're_property_type.id', '=', 're_property_details.property_type_id')
            ->orderBy('re_property_details.created_at', 'desc')
            ->whereIN('re_property_details.id', function($query){
                $query->select('listing_id')
                ->from('re_property_images');
            })
            ->whereIN('re_property_details.id', function($query){
                $query->select('listing_id')
                ->from('re_property_features');
            })
            ->whereIN('re_property_details.id', function($query){
                $query->select('listing_id')
                ->from('re_property_comment');
            })->where('re_property_details.status','=', '1')
            ->get();
        
        $blog_images = PropertyBlogImageModel::get();
        $blogs = PropertyBlogModel::select('re_blog.id as id', 'title', 'blogPost','created_at', 'blogType', 'url')
        ->whereIN('re_blog.id', function($query){
            $query->select('listing_id')
            ->from('re_property_blog_images');
        })->where('re_blog.status','=', '1')
        ->get();
        
        $home_type = PropertyTypeModel::where('status','=', '1')->get();
        $agenciesImages = AgenciesImageModel::get();
        
        $dealer_listing = AgenciesModel::select('tbl_agencies.id as id', 'first_name', 'filename')->join('tbl_agencies_images', 'tbl_agencies_images.listing_id', '=', 'tbl_agencies.id')
        ->orderBy('tbl_agencies.created_at', 'desc')
        ->limit('4')
        ->get();
        
        return view('tangiblehtml.home', compact('dealer_listing','propertyType','propertyListing', 'images', 'latest_properties',  'blogs', 'blog_images' ,'contactUs', 'home_type', 'agenciesImages'));
    }

    public function autocompleteSearch(Request $request)
    {
        $searchTerm = $request->input('search');
        $states = StateModel::select('tbl_state.id as id', 'state_name', 'country_name')
        ->join('tbl_country', 'tbl_country.id', '=', 'tbl_state.country_id')
        ->where('state_name', 'like', '%' . $searchTerm . '%')->get();

        $cities = CityModel::select('tbl_state.id as id', 'city_name', 'state_name', 'country_name')
        ->leftjoin('tbl_state', function($join){
            $join->on('tbl_state.id','=','tbl_city.state_id');
            $join->orOn('tbl_state.id','=','tbl_city.state_id', '=', 0);
        })->join('tbl_country', 'tbl_country.id', '=', 'tbl_city.country_id')
        ->where('city_name', 'like', '%' . $searchTerm . '%')->get();

        $postalCodes = PropertyDataModel::select('id', 'postal_code')->where('postal_code', 'like', '%' . $searchTerm . '%')->get();

        $response = array();
        foreach ($states as $state) {
            $response[] = array(
                "value" => $state->id,
                "label" => $state->state_name,
                "finalLabel" => $state->state_name .'/'. $state->country_name
            );
        }
        foreach ($cities as $city) {
            
            $finalLabel = '';
            if($city->state_name !='')
            $finalLabel = $city->city_name .'/'.$city->state_name .'/'. $city->country_name;
            else
            $finalLabel = $city->city_name .'/'. $city->country_name;
                
            
            $response[] = array(
                "value" => $city->id,
                "label" => $city->city_name,
                "finalLabel" => $finalLabel 
            );
        }
        foreach ($postalCodes as $postalCode) {
            $response[] = array(
                "value" => $postalCode->id,
                "finalLabel" => $postalCode->postal_code
            );
        }

        return response()->json($response);
    }

    public function search(Request $request)
    {
        $selectedCurrency = CurrencyModel::select('currency_code as code')->where('status', '=', '1')->first();
        
        currency()->setUserCurrency($selectedCurrency->code);
        //dd($request);
        
        if($request->input('search') == null){
             
           
            return $this->index();
            
        }
        
        $searchTermTOShow = $request->input('search');
        $searchTerm = explode("/",$request->input('search'))[0];

        $state = StateModel::select('id')->where('state_name', '=', $searchTerm)->first();
        $state_id = "";
        if (! empty($state))
            $state_id = $state->id;

        $city = CityModel::select('id')->where('city_name', '=', $searchTerm)->first();
        $city_id = "";
        if (! empty($city))
            $city_id = $city->id;

        $pincode = '';
        if (empty($state) && empty($city))
            $pincode = $searchTerm;

        $propertyDrop = $request->input('propertyType');
        
        $propertyType = PropertyTypeModel::select('id')->whereIn('property_type', explode(',', $propertyDrop))->get();

        $propertyid = '';
        if (! empty($propertyType)) {
            foreach ($propertyType as $value) {
                $propertyid .= $value->id . ',';
            }
        }

        $priceMin = $request->input('minprice');
        $priceMax = $request->input('maxprice');

        
        if (empty($priceMin))
            $priceMin = 0;

        if (empty($priceMax))
            $priceMax = PHP_FLOAT_MAX;

            
            $propertyListing = '';
            
           /*  if (!empty($propertyid) && empty($state_id) && empty($city_id) && empty($postal_code)) {
                $propertyListing = PropertyDataModel::whereIn('property_type_id', explode(',', $propertyid))->where('re_property_details.status', '=', '1')->whereBetween('property_price', [
                $priceMin,
                $priceMax
            ])->get();
            } else  */if(!empty($propertyid) && !empty($state_id) && empty($city_id) && empty($postal_code)) {
                $propertyListing = PropertyDataModel::whereIn('property_type_id', explode(',', $propertyid))->where('state_id', '=', $state_id)->where('re_property_details.status', '=', '1')->whereBetween('property_price', [
                $priceMin,
                $priceMax
            ])->get();
            } else if(!empty($propertyid) && empty($state_id) && !empty($city_id) && empty($postal_code)) {
                $propertyListing = PropertyDataModel::whereIn('property_type_id', explode(',', $propertyid))->where('city_id', '=', $city_id)->where('re_property_details.status', '=', '1')->whereBetween('property_price', [
                    $priceMin,
                    $priceMax
                ])->get();
            } else if(!empty($propertyid) && empty($state_id) && empty($city_id) && !empty($postal_code)) {
                $propertyListing = PropertyDataModel::whereIn('property_type_id', explode(',', $propertyid))->where('postal_code', '=', $pincode)->where('re_property_details.status', '=', '1')->whereBetween('property_price', [
                    $priceMin,
                    $priceMax
                ])->get();
            } else if(empty($propertyid) && !empty($state_id) && empty($city_id) && empty($postal_code)) {
                $propertyListing = PropertyDataModel::where('state_id', '=', $state_id)->where('re_property_details.status', '=', '1')->whereBetween('property_price', [
                    $priceMin,
                    $priceMax
                ])->get();
            } else if(empty($propertyid) && empty($state_id) && !empty($city_id) && empty($postal_code)) {
                $propertyListing = PropertyDataModel::where('city_id', '=', $city_id)->where('re_property_details.status', '=', '1')->whereBetween('property_price', [
                    $priceMin,
                    $priceMax
                ])->get();
            } else if(empty($propertyid) && empty($state_id) && empty($city_id) && !empty($postal_code)) {
                $propertyListing = PropertyDataModel::where('postal_code', '=', $pincode)->where('re_property_details.status', '=', '1')->whereBetween('property_price', [
                    $priceMin,
                    $priceMax
                ])->get();
            }


        //dd(DB::getQueryLog());
        
        $images = PropertyImageModel::get();
        $propertyContactUs = ContactusModel::get()->first();
        
        
        $latest_properties = PropertyDataModel::select('currency_code','re_property_details.id as id', 'ad_title', 'property_price', 'land_size', 'number_of_bedrooms', 'number_of_bathrooms', 'property_type')->join('re_beds', 're_beds.id', '=', 're_property_details.bed_id')
        ->join('re_bathrooms', 're_bathrooms.id', '=', 're_property_details.bathroom_id')
        ->join('tbl_currency', 'tbl_currency.id', '=', 're_property_details.currency_id')
        ->join('re_property_type', 're_property_type.id', '=', 're_property_details.property_type_id')
        ->orderBy('re_property_details.created_at', 'desc')
        ->whereIN('re_property_details.id', function($query){
            $query->select('listing_id')
            ->from('re_property_images');
        })
        ->whereIN('re_property_details.id', function($query){
            $query->select('listing_id')
            ->from('re_property_features');
        })
        ->whereIN('re_property_details.id', function($query){
            $query->select('listing_id')
            ->from('re_property_comment');
        })->where('re_property_details.status', '=', '1')
        ->get();
        
        $blog_images = PropertyBlogImageModel::get();
        $blogs = PropertyBlogModel::select('re_blog.id as id', 'title', 'blogPost','created_at', 'blogType', 'url')
        ->whereIN('re_blog.id', function($query){
            $query->select('listing_id')
            ->from('re_property_blog_images');
        })->where('re_blog.status', '=', '1')
        ->get();
        
        //  dd(DB::getQueryLog());
        $propertyType = PropertyTypeModel::where('status','=', '1')->get();
        
        return view('tangiblehtml.home', compact('propertyType','propertyDrop','priceMax','priceMin','searchTermTOShow','propertyListing', 'images', 'latest_properties', 'blogs', 'blog_images' ,'propertyContactUs'));
        
    }

    /**
     * Display the single property .
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
        $id = Crypt::decrypt($id);
        $productId=$id;
        //$property = PropertyDataModel::find($id);"http://localhost:8000/uploads/php22vBYz.jpg"
        DB::enableQueryLog();
        $selectedCurrency = CurrencyModel::select('currency_code as code')->where('status', '=', '1')->first();
        currency()->setUserCurrency($selectedCurrency->code);
        $propertyType = PropertyTypeModel::get();
        $message = MessageModel::get();
        
        $category_id = CategoriesModel::select('id')->firstWhere("website_category", "=", 'Real Estate');
        
        $savedListing = SavedListingModel::select('categoryId', 'listingId', 'userId')->where('listingId', '=', $id)
        ->where('categoryId', '=', $category_id->id)->first();
        
        //dd($savedListing);
        
        $property = PropertyDataModel::select('property_type','currency_code','metric','number_of_bathrooms','agent_id','number_of_bedrooms','re_property_details.id as id','ad_title','land_size' ,DB::raw("CONCAT(re_property_details.street_1, ' ', re_property_details.street_2) AS address"),'re_property_details.postal_code as postal_code','property_price','tbl_agent.first_name as agentFirstName','tbl_agent.last_name as agentLastName', 'tbl_agent.mobile as agentMobile',
            'tbl_agent.whatsapp as agentWhatsapp','tbl_agent.office as agentOffice','tbl_agent.fax as agentFax', 'tbl_agent.email as agentEmail', 'comment', 're_price.price as priceType', DB::raw('CONCAT(tbl_agent.first_name, " ", tbl_agent.last_name) AS agentname'), 'tbl_agent.mobile as agentMobile','tbl_agent.country_id as agentCountry','tbl_agent.city_id as agentCity', 'tbl_agent.state_id as agentState')
            ->join('tbl_currency', 'tbl_currency.id', '=', 're_property_details.currency_id')
            ->join('re_bathrooms', 're_bathrooms.id', '=', 're_property_details.bathroom_id')
            ->join('re_beds', 're_beds.id', '=', 're_property_details.bed_id')
            ->join('tbl_agent','tbl_agent.id', '=', 're_property_details.agent_id')
            ->join('re_property_comment','re_property_comment.listing_id', '=', 're_property_details.id')
            ->join('re_property_type', 're_property_type.id', '=', 're_property_details.property_type_id')
            ->leftJoin('re_price','re_price.id', '=', 're_property_details.price_id')
            ->find($id); 
            
            //dd($property);
            
            //dd(DB::getQueryLog());
            if(!empty($property->id)){
                $images = PropertyImageModel::where('listing_id', '=', $property->id)->get();
                $features = PropertyFeaturesModel::where('listing_id', '=', $property->id)->where('feature_available', '=', '1')->get();
                $agentImage = AgentImageModel::where('listing_id', '=', $property->agent_id)->first();
                $contactUs = ContactusModel::get()->first();
            }
            
            $html = '';
            $i = 1;
            if(!empty($features)){
                foreach($features as $values){
                    $feature_name = PropertyFeaturesListingModel::select('property_name')->where('id','=',$values->feature_id)->first();
                    if($i == 1)
                    $html .= '<tr>';
                    
                    $html .= '<td><i class="fas fa-check"></i>'.$feature_name->property_name.'</td>';
                    
                    if($i == 4)
                    $html .= '</tr>';
                    
                    $i++;
                    if($i == 4)
                        $i = 1;
                }
            }
            //dd(strip_tags($property->property_type));
            
            $agentImage = AgentImageModel::where('listing_id', '=', $property->agent_id)->first();
            $state = StateModel::where('id', '=', $property->agentState)->first();
            $country = CountryModel::where('id', '=', $property->agentCountry)->first();
            $city = CityModel::where('id', '=', $property->agentCity)->first();
            
            return view('tangiblehtml.singledetail', compact('state','country', 'city', 'agentImage','propertyType','property', 'images', 'agentImage' ,'features', 'contactUs', 'html', 'savedListing', 'message', 'productId', 'id'));
    }
    
    public function sendenquery(Request $request){
        $this->validate($request, [
            'name'  => 'required|min:3',
            'mobile'  => 'required|min:10',
            'message'  => 'required'
        ]);
        
        $agent = AgentModel::find($request->agentId);
        
        $enqury = EnquiryModel::where('userID', '=', $request->userId)->where('productId', '=', $request->productid)->where('Category', '=', 'RealEstate')->first();
        
        
        if($enqury == null){
            $enqury = new EnquiryModel();
            $enqury->name = $request->name;
            $enqury->phone = $request->mobile;
            $enqury->queryType = $request->querytype;
            $enqury->message = $request->message;
            $enqury->Category = 'RealEstate';
            $enqury->productId = $request->productid;
            $enqury->agent_id = $request->agentId;
            $enqury->agencies_id = $agent->agency_id;
            $enqury->userID = $request->userId;
            $enqury->agent_userid = $agent->userId;
            
            $enqury->save();
        }
        
        $suggestPrice = SubmitSuggestPriceModel::where('user_id', '=', $request->userId)->where('product_id', '=', $request->productid)->where('Category', '=', 'RealEstate')->first();
        
        if($suggestPrice == null){
            $suggestPrice = new SubmitSuggestPriceModel();
            $suggestPrice->id = $enqury->id;
            $suggestPrice->product_id = $request->productid;
            $suggestPrice->user_Id = $request->userId;
            $suggestPrice->agent_Id = $request->agentId;
            $suggestPrice->category = 'RealEstate';
            $suggestPrice->submit_price = $request->suggestedPrice;
            $suggestPrice->shipping_cost = $request->shippingPrice;
            $suggestPrice->agent_userid = $agent->userId;
            
            $suggestPrice->save();
            
        }
        
        $productorder = ProductOrderModel::where('userID', '=', $request->userId)->where('productId', '=', $request->productid)->where('Category', '=', 'RealEstate')->first();
        
        if($productorder == null){
            
            $order = new ProductOrderModel();
            $order->id = $enqury->id;
            $order->name = $request->name;
            $order->user_emailid = $request->email;
            $order->phone = $request->mobile;
            $order->country = $request->country;
            $order->city = $request->city;
            $order->category = 'RealEstate';
            $order->productId = $request->productid;
            $order->agencies_id = $agent->agencies_id;
            $order->agent_id = $request->agentId;
            $order->userId = $request->userId;
            $order->agent_userid = $agent->userId;
            $order->order_status = 'waiting for approval';
            
            $order->save();
        }
        
        $seller_category = 'private_seller';
        if($agent->agency_id != null)
            $seller_category = 'agent';
            
            $myMessage = new MyMessageModel();
            $myMessage->message = $request->message;
            $myMessage->message_id = $enqury->id;
            $myMessage->queryType = 'enquiry';
            $myMessage->category = 'RealEstate';
            $myMessage->agent_id = $request->agentId;
            $myMessage->product_id = $request->productid;
            $myMessage->messageFrom = $request->userId;
            $myMessage->messageTo = $agent->userId;
            
            $myMessage->save();
            
            $user = User::find($request->userId);
            
            Mail::to('priyank@optimalvirtualemployee.com')->send(new ListingEnquiry($user, $agent,$enqury ));
            
            return back()->with('success_msg','Enquery send Successfully');
        
    }
}

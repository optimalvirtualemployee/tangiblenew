<?php

namespace App\Http\Controllers;

use App\CommercialPropertyTypeModel;
use Illuminate\Http\Request;

class PropertyCommercialTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $property_type = CommercialPropertyTypeModel::Select('id', 'property_type', 'status')
        ->get();
        
        return view('admin.realestate.property_type_commercial.index',compact('property_type'));
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $top_property_type = CommercialPropertyTypeModel::get();
        return view('admin.realestate.property_type_commercial.create', compact('top_property_type'));
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validate = $this->validate($request, [
            'property_type' => 'required|unique:re_property_type_commercial',
            'status' => 'required'
        ]);
        
        $property_type = CommercialPropertyTypeModel::create($validate);
        $property_type->save();
        return redirect('/admin/commercial_property_type')->with('success_msg','Property Type Created successfully!');
    }
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $property_type_data = CommercialPropertyTypeModel::find($id);
        
        return view('admin.realestate.property_type_commercial.edit', compact('property_type_data'));
    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validate = $this->validate($request, [
            'property_type' => 'required|unique:re_property_type_commercial,property_type,'.$id
        ]);
        
        $property_typeUpdate = CommercialPropertyTypeModel::find($id);
        
        if ($property_typeUpdate->property_type == $validate['property_type']) {
            $property_typeUpdate->status = $request->input('status');
        } else {
            $validate = $this->validate($request, ['property_type' => 'unique:re_property_type']);
            $property_typeUpdate->property_type = $validate['property_type'];
            $property_typeUpdate->status = $request->input('status');
        }
        
        $property_typeUpdate->save();
        
        return redirect('/admin/commercial_property_type')->with('success_msg','Property Type Updated successfully!');
    }
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deleteRecords = CommercialPropertyTypeModel::find($id)->delete();
        return redirect('/admin/commercial_property_type')->with('success_msg','Property Type deleted successfully!');
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Dial;

class DialController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    //get dial list
    public function index()
    {
        $dial = Dial::get();
        return view('admin.watch.dial.index',compact('dial'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.watch.dial.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
        $validator = $this->validate($request, [
            "power_reserve" => 'required|unique:wa_factory_features_dial',
            "dial_color"  => "required",
            
        ]);

          $dial = Dial::create($validator);
          $dial->save();
       
      return redirect('/admin/dial')->with('success_msg','Dial Created successfully!');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $dial_data = Dial::find($id);
        return view('admin.watch.dial.edit', compact('dial_data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // dd($request->all());
        $deleteRecords = Dial::find($id)->delete();
        if($deleteRecords){
            $validator = $this->validate($request, [
                "power_reserve" => 'required|unique:wa_factory_features_dial',
                "dial_color"  => "required",
                
            ]);

              $dial = Dial::create($validator);
              $dial->save();
        } 
      return redirect('/admin/dial')->with('success_msg','Dial Updated successfully!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deleteRecords = Dial::find($id)->delete();
        return redirect('/admin/dial')->with('success_msg','Dial deleted successfully!');
    }
}

<?php

namespace App\Http\Controllers;

use App\AutomobileBannerModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class AutomobileBannerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $details = AutomobileBannerModel::orderBy('priority', 'asc')->get();
        
        return view('admin.automobile.bannerimages.index', compact('details'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.automobile.bannerimages.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate([
            'photos'  => 'required',
            'photos.*' => 'mimes:jpg,jpeg,png,gif',
            'status' => 'required'
        ]);
        
        
        foreach ($request->file('photos') as $image){
            $uploadImage = new AutomobileBannerModel();
            
            $extension = $image->getClientOriginalExtension();
            Storage::disk('public')->put($image->getFilename().'.'.$extension,  File::get($image));
            
            $uploadImage->images = $image->getClientMimeType();
            $uploadImage->filename = $image->getFilename().'.'.$extension;
            
            $uploadImage->priority = $request->input('imagePriority');
            $uploadImage->status = $request->input('status');
            
            $uploadImage->save();
        }
        
        return redirect('/admin/automobilebanner/')->with('success_msg', 'Automobile Banner Created successfully!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = AutomobileBannerModel::find($id);
        
        return view('admin.automobile.bannerimages.edit', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validate = $this->validate($request, [
            'imagePriority' => 'required'
        ]);
        
        $updatePriority = AutomobileBannerModel::find($id);
        
        $updatePriority->priority = $request->input('imagePriority');
        $updatePriority->status = $request->input('status');
        
        $updatePriority->save();
        
        return redirect('/admin/automobilebanner/')->with('success_msg', 'Automobile Banner Updated successfully!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deleteRecords = AutomobileBannerModel::find($id)->delete();
        return redirect('/admin/automobilebanner/')->with('success_msg', 'Automobile Banner Deleted successfully!');
    }
}

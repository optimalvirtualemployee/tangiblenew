<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\CountryBasedCurrencyModel;
use App\CountryModel;
use App\CurrencyModel;

class CountryBasedCurrencyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     */
    public function index()
    {
        $country_currency = CountryBasedCurrencyModel::select('tbl_country_currency.id as id', 'tbl_country_currency.status as status','country_name', 'currency', 'currency_code')
        ->join('tbl_country', 'tbl_country.id', '=', 'tbl_country_currency.country_id')
        ->join('tbl_currency', 'tbl_currency.id', '=', 'tbl_country_currency.currency_id')
        ->get();
        
        return view('admin.admincommon.country_currency.display')->with(array('country_currency' => $country_currency));
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $countryList = CountryModel::get();
        $currencyList = CurrencyModel::get();
        
        return view('admin.admincommon.country_currency.create', compact('countryList', 'currencyList'));
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->validate([
            'country' => ['required'],
            'currency' => ['required'],
            'status' => ['required']
        ]);
        
        $createCountryCurrency = new CountryBasedCurrencyModel();
        
        $createCountryCurrency->country_id = $request->input('country');
        $createCountryCurrency->currency_id = $request->input('currency');
        $createCountryCurrency->status = $request->input('status');
        
        $createCountryCurrency->save();
        return redirect('/admin/countrycurrency')->with('success_msg', 'Country based currency Created successfully!');
    }
    
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $currency = CountryBasedCurrencyModel::find($id);
        
        $countryList = CountryModel::get();
        $currencyList = CurrencyModel::get();
        
        $countrySelected = CountryModel::find($currency->country_id);
        $currencySelected = CurrencyModel::find($currency->currency_id);
        
        return view('admin.admincommon.country_currency.edit',compact('currency', 'countryList','currencyList', 'countrySelected', 'currencySelected'));
    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->validate([
            'country' => ['required'],
            'currency' => ['required'],
            'status' => ['required']
        ]);
        
        $updateCurrency = CountryBasedCurrencyModel::find($id);
        
        $updateCurrency->currency_id = $request->input('currency');
        $updateCurrency->country_id = $request->input('country');
        $updateCurrency->status = $request->input('status');
        
        $updateCurrency->save();
        
        return redirect('/admin/countrycurrency')->with('success_msg', 'Country based currency Updated successfully!');
    }
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deleteRecords = CountryBasedCurrencyModel::find($id)->delete();
        return redirect('/admin/countrycurrency')->with('success_msg', 'Country based currency Deleted successfully!');
    }
}

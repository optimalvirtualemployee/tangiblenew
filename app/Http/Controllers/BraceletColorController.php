<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\BraceletColorModel;

class BraceletColorController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:watch-list|watch-create|watch-edit|watch-delete', ['only' => ['index','store']]);
        $this->middleware('permission:watch-create', ['only' => ['create','store']]);
        $this->middleware('permission:watch-edit', ['only' => ['edit','update']]);
        $this->middleware('permission:watch-delete', ['only' => ['destroy']]);
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $bracelet = BraceletColorModel::get();
        return view('admin.watch.braceletColor.index',compact('bracelet'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.watch.braceletColor.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
        $validator = $this->validate($request, [
            "bracelet_color" => 'required|unique:wa_factory_features_bracelet_color',
            "status" => 'required'
        ]);
        
        $bracelet = BraceletColorModel::create($validator);
        $bracelet->save();
        
        return redirect('/admin/braceletColor')->with('success_msg','Bracelet Color Created successfully!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $bracelet_data = BraceletColorModel::find($id);
        return view('admin.watch.braceletColor.edit', compact('bracelet_data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = $this->validate($request, [
            "bracelet_color" => 'required',
            "status" => 'required'
        ]);
            
            $bracelet = BraceletColorModel::find($id);
            $bracelet->bracelet_color =$request->input('bracelet_color');
            $bracelet->status =$request->input('status');
            $bracelet->save();
        
        return redirect('/admin/braceletColor')->with('success_msg','Bracelet Color Updated successfully!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deleteRecords = BraceletColorModel::find($id)->delete();
        return redirect('/admin/braceletColor')->with('success_msg','Bracelet Color deleted successfully!');
    }
}

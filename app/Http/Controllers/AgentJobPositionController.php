<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\AgentJobPositionModel;

class AgentJobPositionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return
     */
    public function index()
    {
        $jobPosition = AgentJobPositionModel::get();
            
        return view('admin.admincommon.agent_jobposition.display', compact('jobPosition'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.admincommon.agent_jobposition.create');
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'job_position' => 'required',
            'status' => 'required'
        ]);
        
        $jobPosition = new AgentJobPositionModel();
        $jobPosition->position_name = $request->input('job_position');
        $jobPosition->status = $request->input('status');
        
        $jobPosition->save();
        
        return redirect('/admin/agentjobposition')->with('success_msg', 'Agent Job Created successfully!');
    }
    
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $jobPosition = AgentJobPositionModel::find($id);
        
        return view('admin.admincommon.agent_jobposition.edit', compact('jobPosition'));
    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $updatePosition = AgentJobPositionModel::find($id);
        
        $this->validate($request, [
            'job_position' => 'required',
            'status' => 'required'
        ]);
        
        $updatePosition->position_name = $request->input('job_position');
        $updatePosition->status = $request->input('status');
        
        $updatePosition->save();
        
        return redirect('/admin/agentjobposition')->with('success_msg', 'Agent Job Updated successfully!');
    }
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deleteRecords = AgentJobPositionModel::find($id)->delete();
        return redirect('/admin/agentjobposition')->with('success_msg', 'Agent Job Deleted successfully!');
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\PackageForDealersModel;
use App\PackageSettingsModelForDealers;
use App\PackageListingRangeDealerModel;

class PackageForDealersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $package_for_dealers = PackageForDealersModel::get();
        return view('admin.admincommon.package_for_dealers.index',compact('package_for_dealers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.admincommon.package_for_dealers.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validate = $this->validate($request, [
            'package_for_dealers' => 'required|regex:/^[\w-]*$/|unique:tbl_package_for_dealers',
            'status' => ['required'] 
        ]);

        $package_for_dealers = PackageForDealersModel::create($validate);
        $package_for_dealers->save();
        
        
        return redirect('/admin/package_for_dealers')->with('success_msg','Package for Dealers Created successfully!');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $package_for_dealers_data = PackageForDealersModel::find($id);
        
        
        return view('admin.admincommon.package_for_dealers.edit', compact('package_for_dealers_data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         $validate = $this->validate($request, [
            'package_for_dealers' => 'required|regex:/^[\w-]*$/',
             'status' => ['required']
        ]);

        $package_for_dealersUpdate = PackageForDealersModel::find($id);
        
        

        if ($package_for_dealersUpdate->package_for_dealers == $validate['package_for_dealers']) {
            $package_for_dealersUpdate->status = $request->input('status');
            
        } else {
            $validate = $this->validate($request, ['package_for_dealers' => 'unique:tbl_package_for_dealers']);
            $package_for_dealersUpdate->package_for_dealers = $validate['package_for_dealers'];
            $package_for_dealersUpdate->status = $request->input('status');
            
        }

        $package_for_dealersUpdate->save();

        return redirect('/admin/package_for_dealers')->with('success_msg','Package for Dealers Updated successfully!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deleteRecords = PackageForDealersModel::find($id)->delete();
        return redirect('/admin/package_for_dealers')->with('success_msg','Package for Dealers deleted successfully!');
    }


//------------------- PACKAGE FOR DELAERS SETTING OPTIONS STARTS HERE ------------------------------//

    /** 
    *  This is a custom method for redirecting on setting page from where Admin can enable package options.
    *  @param  int  $id
    *  @return \Illuminate\Http\Response  
    */
    public function package_setting($id)
    {
        $package_settings = PackageSettingsModelForDealers::where('package_id',$id)->get()->first();
        return view('admin.admincommon.package_for_dealers.package_settings.edit', compact('package_settings'));
    }

    /** 
    *  This is a custom method for setup custom package option, 
    *  In which Admin / Moderator can select or update options for various packages for its dealers.
    *  @param  int  $id
    *  @return \Illuminate\Http\Response  
    */
    public function package_setting_update(Request $request, $id)
    {
        // dd($request->all());
        $validate = $this->validate($request, [
            'no_of_active_listing' => 'required|numeric',
            'no_of_images_per_listing' => 'required|numeric',
        ]);

          $if_package = PackageSettingsModelForDealers::where('package_id',$id)->first();  

        if($if_package){
            $package_setting = $if_package;
        }else{
            $package_setting = new PackageSettingsModelForDealers;
        }

        $package_setting->package_id                       = $request->input('package_id');
        $package_setting->no_of_active_listing             = $validate['no_of_active_listing'];
        $package_setting->no_of_images_per_listing         = $validate['no_of_images_per_listing'];
        $package_setting->normal_search_result             = $request->input('normal_search_result') ? '1' : '0' ;
        $package_setting->weekly_statistics                = $request->input('weekly_statistics'   ) ? '1' : '0';
        $package_setting->basic_analytics                  = $request->input('basic_analytics') ? '1' : '0';
        $package_setting->prioritized_search_result        = $request->input('prioritized_search_result') ? '1' : '0';
        $package_setting->escrow_platform                  = $request->input('escrow_platform') ? '1' : '0';
        $package_setting->personalized_logo                = $request->input('personalized_logo') ? '1' : '0';
        $package_setting->advanced_analytics               = $request->input('advanced_analytics') ? '1' : '0';
        $package_setting->top_search_result                = $request->input('top_search_result') ? '1' : '0';
        $package_setting->choice_listing_on_home_page      = $request->input('choice_listing_on_home_page') ? '1' : '0';
        $package_setting->tangible_listing_on_social_media = $request->input('tangible_listing_on_social_media') ? '1' : '0';
        $package_setting->professional_analytics           = $request->input('professional_analytics') ? '1' : '0';
        
        $package_setting->save();

        return redirect('/admin/package_for_dealers')->with('success_msg','Package Setting Updated successfully!');
    }

//------------------- PACKAGE FOR DELAERS SETTING OPTIONS ENDS HERE ------------------------------//

}

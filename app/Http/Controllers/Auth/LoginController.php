<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
//use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Auth;


class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function __construct(){
    	$this->middleware('guest', ['except' => ['logout', 'logout']]);
    }
    
    public function showLoginForm(){
        return view('auth.login');
    }
    
    // protected function redirectTo() {
    //     if(Auth::user()->usertype == '1'){
    //         return 'admin/home';
    //     }
        
    // }

    protected function authenticated(Request $request, $user)
    {
        if(Auth::user()->usertype == '1'){
            return redirect()->route('home');
        }
            
    }
    
    public function logout(Request $request)
    {

        Auth::logout();
        $request->session()->flush();

        $request->session()->regenerate();

        return redirect(route( 'admin.login' ));
    }
}

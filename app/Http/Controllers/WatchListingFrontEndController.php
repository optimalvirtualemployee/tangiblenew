<?php

namespace App\Http\Controllers;

use App\ContactusModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Validation\Rule;
use App\AgentImageModel;
use App\AgentModel;
use App\BraceletMaterialModel;
use App\CaseMaterialModel;
use App\CategoriesModel;
use App\CityModel;
use App\CountryModel;
use App\CurrencyModel;
use App\MessageModel;
use App\ProductOrderModel;
use App\PropertyTypeModel;
use App\SavedListingModel;
use App\StateModel;
use App\SubmitSuggestPriceModel;
use App\TypeModel;
use App\UserDetailsModel;
use App\WatchColourModel;
use App\WatchDataListingModel;
use App\WatchImageModel;
use App\Watchbrandmodel;
use App\WatchModel;
use App\MovementModel;
use App\YearOfManufactureModel;
use App\GenderModel;
use App\DialColorModel;
use App\WatchFeaturesModel;
use App\WatchCommentModel;
use App\WatchSubmitSuggestPriceModel;
use App\EnquiryModel;
use App\MyMessageModel;
use App\User;
use App\OrderStatusHierarchyModel;
use App\Mail\SuggestPriceRequest;
use App\Mail\ListingEnquiry;
use App\WatchModelImage;

class WatchListingFrontEndController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {  
        $contactUs        = ContactusModel::get()->first();
        $images           = WatchImageModel::get();
        $selectedCurrency = CurrencyModel::select('currency_code as code')->where('status', '=', '1')->first();
        currency()->setUserCurrency($selectedCurrency->code);
        $propertyType     = PropertyTypeModel::get();
        $watchListing     = WatchDataListingModel::select('ad_title', 'wa_watch_detail.id as id' ,'currency_code', 'brand_name', 'model_name','watch_price', 'year_of_manufacture', 'case_diameter', 'city_name',
            'state_name', 'watch_price', 'tbl_country.filename as filename', 'tbl_country.countrycode as countrycode', 'tbl_agent.id as agent_id', 'tbl_agent.agency_id as agency_id')
        ->join('tbl_currency', 'tbl_currency.id', '=', 'wa_watch_detail.currency_id')
        ->leftJoin('wa_year_of_manufacture', 'wa_year_of_manufacture.id', '=', 'wa_watch_detail.year_of_manufacture_id')
        ->leftJoin('wa_case_diameter','wa_case_diameter.id', '=', 'wa_watch_detail.case_diameter_id' )
        ->join('tbl_state', 'tbl_state.id', '=', 'wa_watch_detail.state_id')
        ->join('tbl_city', 'tbl_city.id', '=', 'wa_watch_detail.city_id')
        ->join('tbl_country', 'tbl_country.id', '=', 'wa_watch_detail.country_id')
        ->join('tbl_agent', 'tbl_agent.id', '=', 'wa_watch_detail.agent_id')
        ->whereIN('wa_watch_detail.id', function($query){
            $query->select('listing_id')
            ->from('wa_watch_images');
        })
        ->whereIN('wa_watch_detail.id', function($query){
            $query->select('listing_id')
            ->from('wa_watch_features');
        })
        ->whereIN('wa_watch_detail.id', function($query){
            $query->select('listing_id')
            ->from('wa_watch_comment');
        })->where('wa_watch_detail.status','=', '1')
        ->orWhere('wa_watch_detail.case_diameter_id','=', 'NULL')
        ->paginate(2);
        $brands = WatchModel::get();
        $models = Watchbrandmodel::whereIN('wa_model.watch_brand_id', function($query){
            $query->select('id')
            ->from('wa_brands');
        })->get();
        
        $movements          = MovementModel::get();
        $watchType          = TypeModel::get();
        $case_materials     = CaseMaterialModel::get();
        $bracelet_materials = BraceletMaterialModel::get();
        $price_max          = currency()->convert(floatval(WatchDataListingModel::max('watch_price') + 1000), "USD", currency()->getUserCurrency() ,false);
        $years              = YearOfManufactureModel::orderBy('year_of_manufacture','ASC')->get();
        $colors             = DialColorModel::get();
        $country_data       = CountryModel::orderBy('country_name','ASC')->get();
        $gender             = GenderModel::get();
        
        return view('tangiblehtml.watch-listing', compact('propertyType','contactUs','watchListing','watchType' ,'images', 'brands', 'models', 'movements','case_materials' ,'bracelet_materials','price_max', 'years', 'colors', 'country_data', 'gender'));
    }
    
    public function searchFilter(Request $request)
    {
        //echo $request->sortBy;
        $contactUs        = ContactusModel::get()->first();
        $images           = WatchImageModel::get();
        $selectedCurrency = CurrencyModel::select('currency_code as code')->where('status', '=', '1')->first();
        
        currency()->setUserCurrency($selectedCurrency->code);
       
        $price_max        = currency()->convert(floatval(WatchDataListingModel::max('watch_price') + 1000), "USD", currency()->getUserCurrency() ,false);
        $propertyType     = PropertyTypeModel::get();
        
        $params           = $request->all();
        $brandSelected    = [];
        $typeSelected     = [];
        $modelSelected    = [];
        $movementSelected = [];
        $caseSelected     = [];
        $braceletSelected = [];
        $countrySelected  = [];
        $condition        ="";
        $genderSelected   = "";
        $colorSelected    = "";
        $sortByParam      = "ASC";
        $sortByField      = "id";
        
        foreach($params as $param=>$value){
            
            if(strpos($param, 'brand') !== false){
                
                array_push($brandSelected,$value);
            }if(strpos($param, 'type') !== false){
                
                array_push($typeSelected,$value);
            }if(strpos($param, 'model') !== false){
                
                array_push($modelSelected,$value);
            } if(strpos($param, 'movement') !== false){
                
                array_push($movementSelected,$value);
            } if(strpos($param, 'case') !== false){
                
                array_push($caseSelected,$value);
            } if(strpos($param, 'bracelet') !== false){
                
                array_push($braceletSelected,$value);
            } if(strpos($param, 'country') !== false){
                
                array_push($countrySelected,$value);
            }
            
        }
        
        
        if($request->customRadioInline1 != null){
            $condition = $request->customRadioInline1;
            
        }if($request->gender1 != null){
            $genderSelected = $request->gender1;
        }if($request->color !=null)
            $colorSelected = $request->color;
        
        $sortBy = "";    
            
         if($request->sortBy !=null){
             if($request->sortBy == 1){
                 $sortByField = 'watch_price';
                 $sortByParam = 'DESC';
                 $sortBy = $request->sortBy;
             }
             if($request->sortBy == 2){
                 $sortByField = 'watch_price';
                 $sortByParam = 'ASC';
                 $sortBy = $request->sortBy;
             }
             if($request->sortBy == 3){
                 $sortByField = 'wa_watch_detail.created_at';
                 $sortByParam = 'DESC';
                 $sortBy = $request->sortBy;
             }
           }
                
        
        $yearMin = "";
        $yearMax =  "";
        
        if($request->fromYear != null){
            $yearMin = $request->fromYear;
        }if($request->toYear != null){
            $yearMax = $request->toYear;
        }
        
        $priceMin = "";
        $priceMax = "";
        
        if($request->minPrice != null){
            $priceMin = currency()->convert(floatval($request->minPrice), currency()->getUserCurrency(), "USD" ,false);
        }if($request->maxPrice != null){
            $priceMax = currency()->convert(floatval($request->maxPrice), currency()->getUserCurrency(), "USD" ,false);
        }
        
        //dd($gender);
        //print_r($request->all());
        //DB::enableQueryLog();
        $watchListing = WatchDataListingModel::select('ad_title','wa_watch_detail.id as id'  ,'currency_code', 'brand_name', 'model_name','watch_price', 'year_of_manufacture', 'case_diameter', 'city_name',
            'state_name', 'watch_price', 'tbl_country.filename as filename', 'tbl_country.countrycode as countrycode', 'tbl_agent.id as agent_id', 'tbl_agent.agency_id as agency_id',
            'wa_watch_features.id as case_id', 'wa_watch_features.case_material_id as case_material_id', 'wa_watch_features.bracelet_material_id as bracelet_material_id',
            'wa_year_of_manufacture.year_of_manufacture as year_manufacture', 'wa_watch_features.dial_color_id as dial_color_id')
            ->join('tbl_currency', 'tbl_currency.id', '=', 'wa_watch_detail.currency_id')
            ->leftJoin('wa_year_of_manufacture', 'wa_year_of_manufacture.id', '=', 'wa_watch_detail.year_of_manufacture_id')
            ->leftJoin('wa_case_diameter', 'wa_case_diameter.id', '=', 'wa_watch_detail.case_diameter_id')
            ->join('tbl_state', 'tbl_state.id', '=', 'wa_watch_detail.state_id')
            ->join('tbl_city', 'tbl_city.id', '=', 'wa_watch_detail.city_id')
            ->join('tbl_country', 'tbl_country.id', '=', 'wa_watch_detail.country_id')
            ->leftJoin('tbl_agent', 'tbl_agent.id', '=', 'wa_watch_detail.agent_id')
            ->join('wa_watch_features', 'wa_watch_features.listing_id', '=', 'wa_watch_detail.id')
            ->whereIN('wa_watch_detail.id', function($query){
                $query->select('listing_id')
                ->from('wa_watch_features');
            })
            ->whereIN('wa_watch_detail.id', function($query){
                $query->select('listing_id')
                ->from('wa_watch_comment');
            })->where(function($query) use($brandSelected) {
                foreach($brandSelected as $term) {
                    $query->orWhere('brand_id', '=', $term);
                };
            })->where(function($query) use($modelSelected) {
                foreach($modelSelected as $term) {
                    $query->orWhere('model_id', '=', $term);
                };
            })->where(function($query) use($movementSelected) {
                foreach($movementSelected as $term) {
                    $query->orWhere('movement_id', '=', $term);
                };
            })->where(function($query) use($condition) {
                if($condition != null)
                    $query->orWhere('watch_condition', '=', $condition);
            })->where(function($query) use($caseSelected) {
                foreach($caseSelected as $term) {
                    $query->orWhere('case_material_id', '=', $term);
                };
            })->where(function($query) use($braceletSelected) {
                foreach($braceletSelected as $term) {
                    $query->orWhere('bracelet_material_id', '=', $term);
                };
            })->where(function($query) use($typeSelected) {
                foreach($typeSelected as $term) {
                    $query->orWhere('type_id', '=', $term);
                };
            })->where(function($query) use($genderSelected) {
                if($genderSelected != null)
                    $query->orWhere('gender_id', '=', $genderSelected);
            })->where(function($query) use($yearMin,$yearMax) {
                if($yearMax == null && $yearMin != null)
                    $yearMax = YearOfManufactureModel::Select('year_of_manufacture')->orderBy('year_of_manufacture','DESC')->first()->year_of_manufacture;
                    if($yearMin == null && $yearMax !=null)
                        $yearMin = YearOfManufactureModel::Select('year_of_manufacture')->orderBy('year_of_manufacture','Asc')->first()->year_of_manufacture;
                
                if($yearMin != null || $yearMax != null)
                $query->whereBetween('year_of_manufacture', [
                    $yearMin,
                    $yearMax+1
                ]);
                
            })->where(function($query) use($priceMin,$priceMax,$price_max) {
                        if(($priceMin != null && $priceMin > 0) || ($priceMax != null && $priceMax < $price_max))
                            $query->whereBetween('watch_price', [
                                $priceMin,
                                $priceMax+1
                            ])->orWhere('watch_price', NULL);
                            
            })->where(function($query) use($colorSelected) {
                if($colorSelected != null)
                    $query->orWhere('dial_color_id', '=', $colorSelected);
            })->where(function($query) use($countrySelected) {
                foreach($countrySelected as $term) {
                    $query->orWhere('wa_watch_detail.country_id', '=', $term);
                };
            })->where('wa_watch_detail.status','=', '1')
            ->orderBy($sortByField, $sortByParam)
            ->paginate(2);
            
            //dd(DB::getQueryLog());
            //dd($watchListing);
            
            $brands = WatchModel::get();
            
            $models = Watchbrandmodel::whereIN('wa_model.watch_brand_id', function($query){
                $query->select('id')
                ->from('wa_brands');
            })->get();
            
            $movements = MovementModel::get();
            
            $watchType = TypeModel::get();
            
            $case_materials = CaseMaterialModel::get();
            
            $bracelet_materials = BraceletMaterialModel::get();
            
            
            $years = YearOfManufactureModel::orderBy('year_of_manufacture','ASC')->get();
            
            $colors = DialColorModel::get();
            
            $country_data = CountryModel::orderBy('country_name','ASC')->get();
            
            $gender = GenderModel::get();
            
            $priceMin = $request->minPrice;
            $priceMax = $request->maxPrice;
            
            return view('tangiblehtml.watch-listing', compact('priceMin','priceMax','sortBy','propertyType','contactUs','watchListing', 'watchType','typeSelected','images', 'brands', 'models', 'movements','case_materials' ,'bracelet_materials',
                'price_max', 'years', 'colors', 'country_data','gender','brandSelected','modelSelected','movementSelected','caseSelected','braceletSelected','countrySelected'
                    ,'condition','genderSelected', 'yearMin', 'yearMax', 'colorSelected'));
        
    }
    
    public function search(Request $request){
        
        $type = $request->type;
        $finalResponse = [];
        $watchListing = "";
        $model = "";
        if($type == 'brand'){
            
            $brandfilter = $request->brandfilter;
            
            if($brandfilter != null){
                $model = Watchbrandmodel::where(function($query) use($brandfilter) {
                    foreach($brandfilter as $term) {
                        $query->orWhere('watch_brand_id', '=', $term);
                    };
                })->get();
                
                $finalResponse['model']= $model;
            }else{
                
                $model = Watchbrandmodel::get();
                
                $finalResponse['model']= $model;
            }
        }
        
        return response()->json($finalResponse);
    }
    
    public function findYear(Request $request)
    {
        $year = $request->year;
        $finalResponse = [];
        
        $to_year = YearOfManufactureModel::where('year_of_manufacture', '>', $year)->orderBy('year_of_manufacture', 'ASC')->get();
        
        $finalResponse['to_year'] = $to_year;
        
        return response()->json($finalResponse);
    }
    
    /**
     * Display the single Watch .
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
            $productId=Crypt::decrypt($id);
            $id = Crypt::decrypt($id);
            $contactUs = ContactusModel::get()->first();
            $selectedCurrency = CurrencyModel::select('currency_code as code')->where('status', '=', '1')->first();
            
            currency()->setUserCurrency($selectedCurrency->code);
            $propertyType = PropertyTypeModel::get();
            $message = MessageModel::get();
            
            $category_id = CategoriesModel::select('id')->firstWhere("website_category", "=", 'Watch');
            
            $savedListing = SavedListingModel::select('categoryId', 'listingId', 'userId')->where('listingId', '=', $id)
            ->where('categoryId', '=', $category_id->id)->first();
            
            //dd($id);
            
            $watchListing = WatchDataListingModel::select('ad_title', 'wa_watch_detail.id as id' ,'currency_code', 'brand_name', 'model_name','watch_price', 'year_of_manufacture',
                'case_diameter', 'city_name', 'state_name', 'watch_price', 'watch_condition','tbl_country.filename as filename', 'tbl_country.countrycode as countrycode',
                'tbl_agent.agency_id as agency_id', 'movement', 'gender', 'tbl_agent.id as agent_id', 'dial_color', 'tbl_country.country_name as country_name',
                'wa_factory_features_bracelet_material.bracelet_material as bracelet_material', 'wa_factory_features_case_material.case_material as case_material', 'wa_price.price as priceType', DB::raw('CONCAT(tbl_agent.first_name, " ", tbl_agent.last_name) AS agentname'), 'tbl_agent.mobile as agentMobile','tbl_agent.country_id as agentCountry','tbl_agent.city_id as agentCity', 'tbl_agent.state_id as agentState')
                ->leftJoin('tbl_currency', 'tbl_currency.id', '=', 'wa_watch_detail.currency_id')
                ->leftJoin('wa_year_of_manufacture', 'wa_year_of_manufacture.id', '=', 'wa_watch_detail.year_of_manufacture_id')
                ->leftJoin('wa_case_diameter','wa_case_diameter.id', '=', 'wa_watch_detail.case_diameter_id' )
                ->leftJoin('tbl_state', 'tbl_state.id', '=', 'wa_watch_detail.state_id')
                ->leftJoin('tbl_city', 'tbl_city.id', '=', 'wa_watch_detail.city_id')
                ->leftJoin('tbl_country', 'tbl_country.id', '=', 'wa_watch_detail.country_id')
                ->leftJoin('tbl_agent', 'tbl_agent.id', '=', 'wa_watch_detail.agent_id')
                ->leftJoin('wa_movement', 'wa_movement.id', '=', 'wa_watch_detail.movement_id')
                ->leftJoin('wa_gender', 'wa_gender.id', '=', 'wa_watch_detail.gender_id')
                ->leftJoin('wa_watch_features', 'wa_watch_features.listing_id', '=', 'wa_watch_detail.id')
                ->leftJoin('wa_factory_features_bracelet_material', 'wa_factory_features_bracelet_material.id', '=', 'wa_watch_features.bracelet_material_id')
                ->leftJoin('wa_factory_features_case_material', 'wa_factory_features_case_material.id', '=', 'wa_watch_features.case_material_id')
                ->leftJoin('wa_price','wa_price.id', '=', 'wa_watch_detail.price_id')
                ->find($id);
                
                 //dd($watchListing);
                $features = WatchFeaturesModel::where('listing_id', '=', $watchListing->id)->get();
                $images = WatchImageModel::where('listing_id', '=', $watchListing->id)->get();
                $description = WatchCommentModel::where('listing_id', '=', $watchListing->id)->first();
                
                $agentImage = AgentImageModel::where('listing_id', '=', $watchListing->agent_id)->first();

                $brancdId = WatchDataListingModel::where('id',$id)->first();
                $brandImg = WatchModelImage::where('listing_id',$brancdId->brand_id)->first();

                $state = StateModel::where('id', '=', $watchListing->agentState)->first();
                $country = CountryModel::where('id', '=', $watchListing->agentCountry)->first();
                $city = CityModel::where('id', '=', $watchListing->agentCity)->first();
                //dd($features);
            return view('tangiblehtml.watch-detail', compact('id','state','country', 'city', 'agentImage','productId','propertyType','contactUs', 'images','description','watchListing','savedListing', 'message','brandImg'));
    }
    
    public function suggestPrice($id){
        
        $contactUs = ContactusModel::get()->first();
        
        $selectedCurrency = CurrencyModel::select('currency_code as code')->where('status', '=', '1')->first();
        currency()->setUserCurrency($selectedCurrency->code);
        $country_data = CountryModel::get();
        $category = "watch";
        $city_data = CityModel::get();
        $userDetails = UserDetailsModel::get();
        
        $listing = WatchDataListingModel::select('ad_title', 'wa_watch_detail.id as id' ,'currency_code', 'brand_name', 'model_name','watch_price', 'year_of_manufacture',
            'case_diameter', 'city_name', 'state_name', 'watch_price', 'watch_condition','tbl_country.filename as filename', 'tbl_country.countrycode as countrycode',
            'tbl_agent.agency_id as agency_id', 'movement', 'gender', 'tbl_agent.id as agent_id', 'dial_color', 'tbl_country.country_name as country_name',
            'wa_factory_features_bracelet_material.bracelet_material as bracelet_material', 'wa_factory_features_case_material.case_material as case_material', 'watch_price as value')
            ->leftJoin('tbl_currency', 'tbl_currency.id', '=', 'wa_watch_detail.currency_id')
            ->leftJoin('wa_year_of_manufacture', 'wa_year_of_manufacture.id', '=', 'wa_watch_detail.year_of_manufacture_id')
            ->leftJoin('wa_case_diameter','wa_case_diameter.id', '=', 'wa_watch_detail.case_diameter_id' )
            ->leftJoin('tbl_state', 'tbl_state.id', '=', 'wa_watch_detail.state_id')
            ->leftJoin('tbl_city', 'tbl_city.id', '=', 'wa_watch_detail.city_id')
            ->leftJoin('tbl_country', 'tbl_country.id', '=', 'wa_watch_detail.country_id')
            ->leftJoin('tbl_agent', 'tbl_agent.id', '=', 'wa_watch_detail.agent_id')
            ->leftJoin('wa_movement', 'wa_movement.id', '=', 'wa_watch_detail.movement_id')
            ->leftJoin('wa_gender', 'wa_gender.id', '=', 'wa_watch_detail.gender_id')
            ->leftJoin('wa_watch_features', 'wa_watch_features.listing_id', '=', 'wa_watch_detail.id')
            ->leftJoin('wa_factory_features_bracelet_material', 'wa_factory_features_bracelet_material.id', '=', 'wa_watch_features.bracelet_material_id')
            ->leftJoin('wa_factory_features_case_material', 'wa_factory_features_case_material.id', '=', 'wa_watch_features.case_material_id')
            ->find($id);
        
            $images = WatchImageModel::where('listing_id', '=', $listing->id)->get();
            
            return view('tangiblehtml.suggest-price', compact('contactUs', 'listing', 'images',  'country_data', 'category', 'city_data', 'userDetails'));
    }
    
    public function submitSuggestPrice(Request $request){
        
        $userId = $request->userId;
        $productId = $request->productId;
        $this->validate($request, [
            'phoneNumber' => "required",
            'address' => "required",
            'country' => "required",
            'city' => "required",
            'postalCode' => "required",
            'suggestedPrice' => "required"/* ,
            'userId' => ['required',
                Rule::unique('watch_submitpricerequest','user_id')->where(function ($query) use($userId,$productId) {
                    return $query->where('product_id', $productId)
                })] */
        ]);
        
        $updateUser = UserDetailsModel::where('user_id', '=', $request->userId)->first();
        
        if($updateUser != null){
            $updateUser->user_id = $request->userId;
            $updateUser->user_postalCode = $request->postalCode;
            $updateUser->user_mobile = $request->phoneNumber;
            $updateUser->city_id = $request->city;
            $updateUser->country_id = $request->country;
            $updateUser->user_address = $request->address;
            
            $updateUser->save();
        }else{
            $createUser = new UserDetailsModel();
            
            $createUser->user_id = $request->userId;
            $createUser->user_postalCode = $request->postalCode;
            $createUser->user_mobile = $request->phoneNumber;
            $createUser->city_id = $request->city;
            $createUser->country_id = $request->country;
            $createUser->user_address = $request->address;
            
            $createUser->save();
        }
        
        $agent = AgentModel::find($request->agentId);
        
        $suggestPrice = SubmitSuggestPriceModel::where('user_id', '=', $request->userId)->where('product_Id', '=', $request->productId)->where('Category', '=', 'Watch')->first();
        
        $converted_amount = NULL;
        
        if($request->suggestedPrice != ''){
           $selectedCurrency = CurrencyModel::select('currency_code as code')->where('status', '=', '1')->first();
           
            currency()->setUserCurrency($selectedCurrency->code);
            
            $converted_amount = currency()->convert(floatval($request->suggestedPrice), $selectedCurrency->currency_code, "USD",false);
        }
        
        if($suggestPrice == null){

        $suggestPrice = new SubmitSuggestPriceModel();
        $suggestPrice->product_id = $request->productId;
        $suggestPrice->user_Id = $request->userId;
        $suggestPrice->agent_Id = $request->agentId;
        $suggestPrice->category = 'Watch';
        $suggestPrice->submit_price = $converted_amount;
        $suggestPrice->shipping_cost = $request->shippingPrice;
        $suggestPrice->agent_userid = $agent->userId;
        
        $suggestPrice->save();

        }else if($suggestPrice != null && $suggestPrice->submit_price != null){
            
            return back()->with('success_msg','You already make an offer for this listing');
        
        }else if($suggestPrice != null && $suggestPrice->submit_price == null){
            
            $suggestPrice = SubmitSuggestPriceModel::find($suggestPrice->id);
            
            $suggestPrice->product_id = $request->productId;
            $suggestPrice->user_Id = $request->userId;
            $suggestPrice->agent_Id = $request->agentId;
            $suggestPrice->category = 'Watch';
            $suggestPrice->submit_price = $converted_amount;
            $suggestPrice->shipping_cost = $request->shippingPrice;
            $suggestPrice->agent_userid = $agent->userId;
            
            $suggestPrice->save();
        }
        
        
        
        $enqury = EnquiryModel::where('userID', '=', $request->userId)->where('productId', '=', $request->productId)->where('Category', '=', 'Watch')->first();
        
        if($enqury == null){
            $enqury = new EnquiryModel;
            $enqury->id = $suggestPrice->id;
            $enqury->name = $updateUser->first_name .' '. $updateUser->last_name;
            $enqury->phone = $updateUser->user_mobile;
            $enqury->queryType = $request->querytype;
            $enqury->message = $request->message;
            $enqury->Category = 'Watch';
            $enqury->productId = $request->productId;
            $enqury->agent_id = $request->agentId;
            $enqury->agencies_id = $agent->agency_id;
            $enqury->userID = $request->userId;
            $enqury->agent_userid = $agent->userId;
            $enqury->save();
            
        }
        
        $productorder = ProductOrderModel::where('userID', '=', $request->userId)->where('productId', '=', $request->productId)->where('Category', '=', 'Watch')->first();
        
        if($productorder == null){
            
            $order = new ProductOrderModel();
            $order->id = $suggestPrice->id;
            $order->name = $updateUser->first_name .' '. $updateUser->last_name;
            $order->user_emailid = $updateUser->email;
            $order->phone = $updateUser->user_mobile;
            $order->country = $request->country;
            $order->city = $request->city;
            $order->category = 'Watch';
            $order->productId = $request->productId;
            $order->agencies_id = $agent->agencies_id;
            $order->agent_id = $request->agentId;
            $order->userId = $request->userId;
            $order->agent_userid = $agent->userId;
            $order->order_status = 'waiting for approval';
            
            $order->save();
        }
        
        $seller_category = 'private_seller';
        if($agent->agency_id != null)
            $seller_category = 'agent';
            
            
            $myMessage = new MyMessageModel();
            
            $myMessage->message = $request->message;
            $myMessage->message_id = $suggestPrice->id;
            $myMessage->queryType = 'submitprice';
            $myMessage->category = 'Watch';
            $myMessage->agent_id = $request->agentId;
            $myMessage->product_id = $request->productId;
            $myMessage->messageFrom = $request->userId;
            $myMessage->messageTo = $agent->userId;
                
            $myMessage->save();
            
            
            $product = WatchDataListingModel::select('brand_name', 'model_name', 'watch_price as value')->where('id', '=', $request->productId)->first();
            
            $productImage = WatchImageModel::select('filename')->where('listing_id', '=', $request->productId)->first();
            
            $user = User::find($request->userId);
            
            
            $orderStatus = new OrderStatusHierarchyModel();
            
            $orderStatus->enquiryId = $suggestPrice->id;
            $orderStatus->status = 'waiting for approval';
            
            $orderStatus->save();
            
           // Mail::to('priyank@optimalvirtualemployee.com')->send(new SuggestPriceRequest($converted_amount, $user, $agent, $product, $productImage));
            
            
            /*Mail::send('email.sending-suggesting-price', ['suggestPrice' =>$converted_amount, 'user' =>$user ,'agent' =>  $agent, 'product' => $product, 'productImage' => $productImage], function($message) {
            $message->to('priyank@optimalvirtualemployee.com', 'Priyank Sharma')->subject
            ('Submit Price Request');
            $message->from('pashu.sharma@gmail.com','Tangible Listing');
        });*/
             
        
        return back()->with('success_msg','Make an Offer request send successfully');
    }



    public function checkout($id){
        
        $contactUs = ContactusModel::get()->first();
        
        $selectedCurrency = CurrencyModel::select('currency_code as code')->where('status', '=', '1')->first();
        currency()->setUserCurrency($selectedCurrency->code);
        $country_data = CountryModel::get();
        $category = "watch";
        $city_data = CityModel::get();
        $userDetails = UserDetailsModel::get();
        
        $listing = WatchDataListingModel::select('ad_title', 'wa_watch_detail.id as id' ,'currency_code', 'brand_name', 'model_name','watch_price', 'year_of_manufacture',
            'case_diameter', 'city_name', 'state_name', 'watch_price', 'watch_condition','tbl_country.filename as filename', 'tbl_country.countrycode as countrycode',
            'tbl_agent.agency_id as agency_id', 'movement', 'gender', 'tbl_agent.id as agent_id', 'dial_color', 'tbl_country.country_name as country_name',
            'wa_factory_features_bracelet_material.bracelet_material as bracelet_material', 'wa_factory_features_case_material.case_material as case_material', 'watch_price as value')
            ->leftJoin('tbl_currency', 'tbl_currency.id', '=', 'wa_watch_detail.currency_id')
            ->leftJoin('wa_year_of_manufacture', 'wa_year_of_manufacture.id', '=', 'wa_watch_detail.year_of_manufacture_id')
            ->leftJoin('wa_case_diameter','wa_case_diameter.id', '=', 'wa_watch_detail.case_diameter_id' )
            ->leftJoin('tbl_state', 'tbl_state.id', '=', 'wa_watch_detail.state_id')
            ->leftJoin('tbl_city', 'tbl_city.id', '=', 'wa_watch_detail.city_id')
            ->leftJoin('tbl_country', 'tbl_country.id', '=', 'wa_watch_detail.country_id')
            ->leftJoin('tbl_agent', 'tbl_agent.id', '=', 'wa_watch_detail.agent_id')
            ->leftJoin('wa_movement', 'wa_movement.id', '=', 'wa_watch_detail.movement_id')
            ->leftJoin('wa_gender', 'wa_gender.id', '=', 'wa_watch_detail.gender_id')
            ->leftJoin('wa_watch_features', 'wa_watch_features.listing_id', '=', 'wa_watch_detail.id')
            ->leftJoin('wa_factory_features_bracelet_material', 'wa_factory_features_bracelet_material.id', '=', 'wa_watch_features.bracelet_material_id')
            ->leftJoin('wa_factory_features_case_material', 'wa_factory_features_case_material.id', '=', 'wa_watch_features.case_material_id')
            ->find($id);
        
            $images = WatchImageModel::where('listing_id', '=', $listing->id)->get();
            
            return view('tangiblehtml.checkout', compact('contactUs', 'listing', 'images',  'country_data', 'category', 'city_data', 'userDetails'));
    }
    
    public function sendenquery(Request $request){
            
        $this->validate($request, [
            'name'  => 'required|min:3',
            'mobile'  => 'required|min:10',
            'message'  => 'required',
            'country' => 'required'
        ]);
        
        $country =  $request->country;
        
        $agent = AgentModel::find($request->agentId);
        
        $enqury = EnquiryModel::where('userID', '=', $request->userId)->where('productId', '=', $request->productid)->where('Category', '=', 'Watch')->first();


        
        if($enqury == null){
        $enqury = new EnquiryModel();
        $enqury->name = $request->name;
        $enqury->phone = $request->mobile;
        $enqury->country = $country;
        $enqury->queryType = $request->querytype;
        $enqury->message = $request->message;
        $enqury->Category = 'watch';
        $enqury->productId = $request->productid;
        $enqury->agent_id = $request->agentId;
        $enqury->agencies_id = $agent->agency_id;
        $enqury->userID = $request->userId;
        $enqury->agent_userid = $agent->userId;
        
        $enqury->save();
        }
        
        $suggestPrice = SubmitSuggestPriceModel::where('user_id', '=', $request->userId)->where('product_id', '=', $request->productid)->where('Category', '=', 'Watch')->first();
        
        $converted_amount = NULL;
        
        if($request->suggestedPrice != ''){

            $currency_code = CurrencyModel::find($request->input('currency'));  
            $converted_amount = currency()->convert(floatval($request->suggestedPrice), $currency_code->currency_code, "USD",false);
        }
        
        if($suggestPrice == null){
            $suggestPrice = new SubmitSuggestPriceModel();
            $suggestPrice->id = $enqury->id;
            $suggestPrice->product_id = $request->productid;
            $suggestPrice->user_Id = $request->userId;
            $suggestPrice->agent_Id = $request->agentId;
            $suggestPrice->category = 'Watch';
            $suggestPrice->submit_price = $converted_amount;
            $suggestPrice->shipping_cost = $request->shippingPrice;
            $suggestPrice->agent_userid = $agent->userId;
            
            $suggestPrice->save();
            
        }
        
        $productorder = ProductOrderModel::where('userID', '=', $request->userId)->where('productId', '=', $request->productid)->where('Category', '=', 'Watch')->first();
        
        if($productorder == null){
            
            $order = new ProductOrderModel();
            $order->id = $enqury->id;
            $order->name = $request->name;
            $order->user_emailid = $request->email;
            $order->phone = $request->mobile;
            $order->country = $request->country;
            $order->city = $request->city;
            $order->category = 'Watch';
            $order->productId = $request->productid;
            $order->agencies_id = $agent->agencies_id;
            $order->agent_id = $request->agentId;
            $order->userId = $request->userId;
            $order->agent_userid = $agent->userId;
            $order->order_status = 'waiting for approval';
            
            $order->save();
        }
        
        
        $seller_category = 'private_seller';
        if($agent->agency_id != null)
            $seller_category = 'agent';
            
            $myMessage = new MyMessageModel();
            $myMessage->message = $request->message;
            $myMessage->message_id = $enqury->id;
            $myMessage->queryType = 'enquiry';
            $myMessage->category = 'Watch';
            $myMessage->agent_id = $request->agentId;
            $myMessage->product_id = $request->productid;
            $myMessage->messageFrom = $request->userId;
            $myMessage->messageTo = $agent->userId;
                    
            $myMessage->save();
            
            $orderStatus = new OrderStatusHierarchyModel();
            
            $orderStatus->enquiryId = $enqury->id;
            $orderStatus->status = 'waiting for approval';
            
            $orderStatus->save();
            
            $user = User::find($request->userId);
            
            //Mail::to('priyank@optimalvirtualemployee.com')->send(new ListingEnquiry($user, $agent,$enqury ));
            
            /*Mail::send('email.contact-seller', [ 'user' =>$user ,'agent' =>  $agent, 'enqury' =>$enqury], function($message) {
                $message->to('priyank@optimalvirtualemployee.com', 'Priyank Sharma')->subject
                ('Submit Price Request');
                $message->from('pashu.sharma@gmail.com','Tangible Listing');
            });*/
        
        return back()->with('success_msg','Enquery send Successfully');
        
    }
}

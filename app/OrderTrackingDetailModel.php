<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OrderTrackingDetailModel extends Model
{
    protected $table = 'tbl_order_tracking_detail';
}

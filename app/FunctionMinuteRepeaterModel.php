<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FunctionMinuteRepeaterModel extends Model
{
    protected $table = 'wa_factory_features_function_minuteRepeater';
    protected $fillable = ['minute_repeater', 'status'];
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OrderStatusHierarchyModel extends Model
{
    protected $table = 'tbl_order_delivery_status_hierarchy';
}

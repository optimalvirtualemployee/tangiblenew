<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ContactusModel extends Model
{
    protected $table = 're_contactus';
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PropertyImageModel extends Model
{
    protected $table = 're_property_images';
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FunctionYearModel extends Model
{
    protected $table = 'wa_factory_features_function_year';
    protected $fillable = ['year', 'status'];
}

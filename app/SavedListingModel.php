<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SavedListingModel extends Model
{
    protected $table = 'tbl_saved_listing';
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FunctionChronographModel extends Model
{
    protected $table = 'wa_factory_features_function_chronograph';
    protected $fillable = ['chronograph', 'status'];
}

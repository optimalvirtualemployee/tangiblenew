<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

//defined car brands model here
class CarBrandModel extends Model
{
    //defined car brands model details here
    protected $table = 'au_model';
}

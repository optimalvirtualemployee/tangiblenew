<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bracelet extends Model
{
    protected $table = 'wa_factory_features_bracelet';
    protected $fillable = ['bracelet_material','bracelet_color','type_of_clasp','clasp_material'];
}

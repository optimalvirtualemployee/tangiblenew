<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FunctionTourbillionModel extends Model
{
    protected $table = 'wa_factory_features_function_tourbillion';
    protected $fillable = ['tourbillion', 'status'];
}

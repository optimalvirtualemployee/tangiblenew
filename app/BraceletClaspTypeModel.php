<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BraceletClaspTypeModel extends Model
{
    protected $table = 'wa_factory_features_bracelet_claspType';
    protected $fillable = ['type_of_clasp', 'status'];
}

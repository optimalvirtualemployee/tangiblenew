<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WatchCommentModel extends Model
{
    protected $table = 'wa_watch_comment';
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BraceletMaterialModel extends Model
{
    protected $table = 'wa_factory_features_bracelet_material';
    protected $fillable = ['bracelet_material', 'status'];
}

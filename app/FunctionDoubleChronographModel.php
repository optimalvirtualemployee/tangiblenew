<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FunctionDoubleChronographModel extends Model
{
    protected $table = 'wa_factory_features_function_doubleChronograph';
    protected $fillable = ['double_chronograph', 'status'];
}

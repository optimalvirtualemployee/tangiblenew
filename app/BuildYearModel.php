<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BuildYearModel extends Model
{
    protected $table = 're_build_year';
    protected $fillable = ['build_year', 'status'];
}

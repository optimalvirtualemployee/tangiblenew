<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AgenciesImageModel extends Model
{
    protected $table = 'tbl_agencies_images';
}

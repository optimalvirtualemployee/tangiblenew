<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WatchColourModel extends Model
{
    protected $table = 'wa_color';
}

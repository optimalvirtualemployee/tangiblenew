<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CaseGlassTypeModel extends Model
{
    protected $table = 'wa_factory_features_case_glassType';
    protected $fillable = ['glass_type', 'status'];
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AutomobileDataListingModel extends Model
{
    protected $table = 'au_automobile_detail';
}

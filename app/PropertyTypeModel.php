<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PropertyTypeModel extends Model
{
    protected $table = 're_property_type';
    protected $fillable = ['top_property_type','property_type', 'status'];
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CaseMMModel extends Model
{
    protected $table = 'wa_factory_features_case_mm';
    protected $fillable = ['case_mm', 'status'];
}

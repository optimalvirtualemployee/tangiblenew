<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AutomobileDamageImageModel extends Model
{
    protected $table = 'au_automobile_damage_images';
}

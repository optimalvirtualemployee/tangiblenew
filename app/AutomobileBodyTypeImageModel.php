<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AutomobileBodyTypeImageModel extends Model
{
    protected $table = 'au_body_type_images'; 
}

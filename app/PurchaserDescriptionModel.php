<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PurchaserDescriptionModel extends Model
{
   	protected $table = 're_purchaser_description';
    protected $fillable = ['purchaser_description', 'status'];
}

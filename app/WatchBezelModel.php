<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WatchBezelModel extends Model
{
    protected $table = 'wa_bezelmaterial';
    protected $fillable = ['bezel_material', 'status'];
    
}

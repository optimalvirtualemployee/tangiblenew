<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WatchContactusModel extends Model
{
    protected $table = 'wa_contactus';
}

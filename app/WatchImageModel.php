<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WatchImageModel extends Model
{
    protected $table = 'wa_watch_images';
}

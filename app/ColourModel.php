<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ColourModel extends Model
{
    protected $table = 'au_colour';
    protected $fillable = ['colour', 'status'];
}

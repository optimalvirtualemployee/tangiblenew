<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InteriorColourModel extends Model
{
    protected $table = 'au_interior_colour';
    protected $fillable = ['interior_colour', 'status'];
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WatchDamageImageModel extends Model
{
    protected $table = 'wa_watch_damage_images';
}

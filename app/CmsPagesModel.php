<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CmsPagesModel extends Model
{
    protected $table = 'tbl_cmspages';
}

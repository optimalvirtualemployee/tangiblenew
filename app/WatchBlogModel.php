<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WatchBlogModel extends Model
{
    protected $table = 'wa_blog';
}

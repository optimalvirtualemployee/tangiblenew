<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FinalOfferModel extends Model
{
    protected $table = 'tbl_final_offer';
}

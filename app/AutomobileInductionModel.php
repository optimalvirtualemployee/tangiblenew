<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AutomobileInductionModel extends Model
{
    protected $table = 'au_induction';
    protected $fillable = ['induction', 'status'];
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AutomobileDriveTypeModel extends Model
{
    protected $table = 'au_drive_type';
    protected $fillable = ['drive_type', 'status'];
}

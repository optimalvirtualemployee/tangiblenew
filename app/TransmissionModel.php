<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TransmissionModel extends Model
{
    protected $table = 'au_transmission';
    protected $fillable = ['transmission'];
}

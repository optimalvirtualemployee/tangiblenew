<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PackageForDealersModel extends Model
{
    protected $table = 'tbl_package_for_dealers';
    protected $fillable = ['package_for_dealers', 'status'];
}

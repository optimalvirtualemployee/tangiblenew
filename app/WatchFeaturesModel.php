<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WatchFeaturesModel extends Model
{
    protected $table = 'wa_watch_features';
}

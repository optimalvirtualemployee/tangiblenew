<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NoOfBathroomsModel extends Model
{
    protected $table = 're_bathrooms';
    protected $fillable = ['number_of_bathrooms', 'status'];
}

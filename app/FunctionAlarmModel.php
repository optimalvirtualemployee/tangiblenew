<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FunctionAlarmModel extends Model
{
    protected $table = 'wa_factory_features_function_alarm';
    protected $fillable = ['alarm', 'status'];
}

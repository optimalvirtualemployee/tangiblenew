<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AgentJobPositionModel extends Model
{
    protected $table = 'tbl_agent_job_position';
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MovementModel extends Model
{
    protected $table = 'wa_movement';
    protected $fillable = ['movement', 'status'];
}

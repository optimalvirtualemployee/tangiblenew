<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WatchPayment extends Model
{
    protected $table = 'tbl_paymentWatch';


    protected $fillable = [
       'listingId','name','email','amount','currency','transaction_id','payment_status','bankName','beneficiaryName','bankAccount','ifsc','receipt_url','transaction_complete_details'
    ];
}

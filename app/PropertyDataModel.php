<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PropertyDataModel extends Model
{
    protected $table = 're_property_details';
}

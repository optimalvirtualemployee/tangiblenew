<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PackageForPrivateSellersModel extends Model
{
   protected $table = 'tbl_package_for_private_sellers';
   protected $fillable = ['package_for_private_sellers', 'status'];
}

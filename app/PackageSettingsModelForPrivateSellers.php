<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PackageSettingsModelForPrivateSellers extends Model
{
   protected $table = 'tbl_package_settings_for_private_sellers';
    protected $fillable = ['package_id','no_of_images_per_listing','normal_search_result','weekly_statistics','basic_analytics','prioritized_search_result','escrow_platform','top_search_result','choice_listing_on_home_page','tangible_listing_on_social_media'];
}

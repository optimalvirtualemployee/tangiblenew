<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PropertyAdditionalFeaturesModel extends Model
{
    protected $table = 're_property_additional_features';
}

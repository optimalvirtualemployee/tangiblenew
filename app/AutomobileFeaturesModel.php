<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AutomobileFeaturesModel extends Model
{
    protected $table = 'au_automobile_features';
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserPermissionModel extends Model
{
    protected $table = 'tbl_user_permissions';
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PropertyBannerModel extends Model
{
    protected $table = 're_banner_images';
}

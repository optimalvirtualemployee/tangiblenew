<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PropertyContactusModel extends Model
{
    protected $table = 're_contactus';
}

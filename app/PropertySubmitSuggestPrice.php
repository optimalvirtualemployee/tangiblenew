<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PropertySubmitSuggestPrice extends Model
{
    protected $table = 'property_submitpricerequest';
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FunctionsModel extends Model
{
    protected $table = 'wa_factory_features_functions';
    protected $fillable = ['chronograph','tourbillion','GMT','annual_calendar','minute_repeater','double_chronograph','panorma_date','jumping_hour','alarm','year','day'];
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CarPriceModel extends Model
{
    protected $table = 'au_price';
}

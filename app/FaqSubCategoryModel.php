<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FaqSubCategoryModel extends Model
{
    protected $table = 'tbl_faqsubcategory';
}

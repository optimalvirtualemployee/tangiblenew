<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PropertyPriceModel extends Model
{
    protected $table = 're_price';
}

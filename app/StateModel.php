<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StateModel extends Model
{
     protected $table = 'tbl_state';
}

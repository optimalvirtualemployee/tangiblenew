<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PackageListingRangeWatchManagementModel extends Model
{
    protected $table = 'tbl_package_pricing_range_private_seller_watch';

    public function package()
    {
        return $this->belongsTo('App\PackageForPrivateSellersModel', 'package_id', 'id');
    }

}


$(document).ready(function () {


    // $(function () {

    //     $(".min-price").slider({    
    //         range: true,
    //         min: 0,
    //         max: 1000,
    //         values: [0, 1000],
    //         slide: function (event, ui) {
    //             $(".max-price").text("$" + ui.values[0] + " - $" + ui.values[1]);
    //         }
    //     });
    //     $(".max-price").text("$" + $(".min-price").slider("values", 0) +
    //         " - $" + $(".min-price").slider("values", 1));

    // });

    (function ($) {

        $('#price-range-submit').hide();

        $("#min_price,#max_price").on('change', function () {

            $('#price-range-submit').show();

            var min_price_range = parseInt($("#min_price").val());

            var max_price_range = parseInt($("#max_price").val());

            if (min_price_range > max_price_range) {
                $('#max_price').val(min_price_range);
            }

            $("#slider-range").slider({
                values: [min_price_range, max_price_range]
            });

        });


        $("#min_price,#max_price").on("paste keyup", function () {

            $('#price-range-submit').show();

            var min_price_range = parseInt($("#min_price").val());

            var max_price_range = parseInt($("#max_price").val());

            if (min_price_range == max_price_range) {

                max_price_range = min_price_range + 100;

                $("#min_price").val(min_price_range);
                $("#max_price").val(max_price_range);
            }

            $("#slider-range").slider({
                values: [min_price_range, max_price_range]
            });

        });


        /*$(function () {
            $("#slider-range").slider({
                range: true,
                orientation: "horizontal",
                min: 0.00,
                max: 10000.00,
                values: [0.00, 10000.00],
                // step: 100,

                slide: function (event, ui) {
                    if (ui.values[0] == ui.values[1]) {
                        return false;
                    }

                    $("#min_price").val(ui.values[0]);
                    $("#max_price").val(ui.values[1]);
                }
            });

            $("#min_price").val($("#slider-range").slider("values", 0));
            $("#max_price").val($("#slider-range").slider("values", 1));

        });*/

        $("#slider-range,#price-range-submit").click(function () {

            var min_price = $('#min_price').val();
            var max_price = $('#max_price').val();

        });


    })(jQuery);


    $('.custom-landing-banner-slider').slick({
        autoplay: false,
        arrows: false,
        speed: 0,
        dots: true,
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        cssEase: 'linear',
        responsive: [{
            breakpoint: 1024,
            settings: {
                arrows: false,
            }
        },
        {
            breakpoint: 575,
            settings: {
                arrows: false,
                dots: false
            }
        }
        ]
    });

    $('.trending-slider').slick({
        autoplay: false,
        autoplaySpeed: 1000,
        speed: 600,
        arrows: true,
        dots: false,
        infinite: true,
        slidesToShow: 4,
        slidesToScroll: 1,
        cssEase: 'linear',
        responsive: [{
            breakpoint: 992,
            settings: {
                arrows: true,
                slidesToShow: 3,
                slidesToScroll: 1
            }
        },
        {
            breakpoint: 768,
            settings: {
                arrows: true,
                slidesToShow: 2,
                slidesToScroll: 1
            }
        },
        {
            breakpoint: 575,
            settings: {
                arrows: true,
                slidesToShow: 1,
                slidesToScroll: 1
            }
        }
        ]
    });

    $('.testimonial-slider').slick({
        autoplay: false,
        arrows: false,
        autoplaySpeed: 1000,
        speed: 500,
        dots: true,
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        cssEase: 'linear',
        responsive: [{
            breakpoint: 768,
            settings: {
                arrows: false,
                slidesToShow: 1,
                slidesToScroll: 1
            }
        },
        {
            breakpoint: 575,
            settings: {
                arrows: false,
                slidesToShow: 1,
                slidesToScroll: 1
            }
        }
        ]
    });


    //slider

    $('.custom-banner-slider').slick({
        autoplay: false,
        arrows: false,
        speed: 0,
        dots: true,
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        cssEase: 'linear',
        responsive: [{
            breakpoint: 1024,
            settings: {
                arrows: false,
            }
        },
        {
            breakpoint: 575,
            settings: {
                arrows: false,
                dots: false
            }
        }
        ]
    });

    $('.perfect-home-slider').slick({
        autoplay: false,
        autoplaySpeed: 1000,
        speed: 600,
        arrows: true,
        dots: false,
        infinite: true,
        slidesToShow: 3,
        slidesToScroll: 1,
        cssEase: 'linear',
        responsive: [{
            breakpoint: 992,
            settings: {
                arrows: true,
                slidesToShow: 3,
                slidesToScroll: 1
            }
        },
        {
            breakpoint: 768,
            settings: {
                arrows: true,
                slidesToShow: 2,
                slidesToScroll: 1
            }
        },
        {
            breakpoint: 575,
            settings: {
                arrows: true,
                slidesToShow: 1,
                slidesToScroll: 1
            }
        }
        ]
    });
    
    $('.latest-collection-watch').slick({
        autoplay: false,
        autoplaySpeed: 1000,
        speed: 600,
        arrows: true,
        dots: false,
        infinite: true,
        slidesToShow: 4,
        slidesToScroll: 1,
        cssEase: 'linear',
        responsive: [{
            breakpoint: 992,
            settings: {
                arrows: true,
                slidesToShow: 3,
                slidesToScroll: 1
            }
        },
        {
            breakpoint: 768,
            settings: {
                arrows: true,
                slidesToShow: 2,
                slidesToScroll: 1
            }
        },
        {
            breakpoint: 575,
            settings: {
                arrows: true,
                slidesToShow: 1,
                slidesToScroll: 1
            }
        }
        ]
    });
    $('.latest-premium-watch').slick({
        autoplay: false,
        autoplaySpeed: 1000,
        speed: 600,
        arrows: true,
        dots: false,
        infinite: true,
        slidesToShow: 4,
        slidesToScroll: 1,
        cssEase: 'linear',
        responsive: [{
            breakpoint: 992,
            settings: {
                arrows: true,
                slidesToShow: 3,
                slidesToScroll: 1
            }
        },
        {
            breakpoint: 768,
            settings: {
                arrows: true,
                slidesToShow: 2,
                slidesToScroll: 1
            }
        },
        {
            breakpoint: 575,
            settings: {
                arrows: true,
                slidesToShow: 1,
                slidesToScroll: 1
            }
        }
        ]
    });
    $('.perfect-car-slider, .carFeaturedListing').slick({
        autoplay: false,
        autoplaySpeed: 1000,
        speed: 600,
        arrows: true,
        dots: false,
        infinite: true,
        slidesToShow: 4,
        slidesToScroll: 1,
        cssEase: 'linear',
        responsive: [{
            breakpoint: 992,
            settings: {
                arrows: true,
                slidesToShow: 3,
                slidesToScroll: 1
            }
        },
        {
            breakpoint: 768,
            settings: {
                arrows: true,
                slidesToShow: 2,
                slidesToScroll: 1
            }
        },
        {
            breakpoint: 575,
            settings: {
                arrows: true,
                slidesToShow: 1,
                slidesToScroll: 1
            }
        }
        ]
    });


    $('.dream-location-slider').slick({
        autoplay: false,
        autoplaySpeed: 1000,
        speed: 600,
        arrows: true,
        dots: false,
        infinite: true,
        slidesToShow: 4,
        slidesToScroll: 1,
        cssEase: 'linear',
        responsive: [{
            breakpoint: 992,
            settings: {
                arrows: true,
                slidesToShow: 3,
                slidesToScroll: 1
            }
        },
        {
            breakpoint: 768,
            settings: {
                arrows: true,
                slidesToShow: 2,
                slidesToScroll: 1
            }
        },
        {
            breakpoint: 575,
            settings: {
                arrows: true,
                slidesToShow: 1,
                slidesToScroll: 1
            }
        }
        ]
    });

    $('.property-detail-slider').slick({
        autoplay: true,
        arrows: false,
        speed: 0,
        dots: true,
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        cssEase: 'linear',
        responsive: [{
            breakpoint: 1024,
            settings: {
                arrows: false,
            }
        },
        {
            breakpoint: 575,
            settings: {
                arrows: false,
                dots: false
            }
        }
        ]
    });

    $('.home-list-slider').slick({
        autoplay: false,
        arrows: false,
        speed: 0,
        dots: true,
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        cssEase: 'linear',
        responsive: [{
            breakpoint: 1024,
            settings: {
                arrows: false,
            }
        },
        {
            breakpoint: 575,
            settings: {
                arrows: false,
                dots: false
            }
        }
        ]
    });

    $('.car-listing-slider').slick({
        autoplay: false,
        arrows: false,
        dots: true,
        infinite: true,
        speed: 500,
        fade: true,
        cssEase: 'linear'
    });

    $('.car_browse_slider').slick({
        autoplay: false,
        autoplaySpeed: 1000,
        speed: 600,
        arrows: true,
        dots: false,
        infinite: true,
        slidesToShow: 6,
        slidesToScroll: 1,
        cssEase: 'linear',
        responsive: [{
            breakpoint: 992,
            settings: {
                arrows: true,
                slidesToShow: 4,
                slidesToScroll: 1
            }
        },
        {
            breakpoint: 768,
            settings: {
                arrows: true,
                slidesToShow: 4,
                slidesToScroll: 1
            }
        },
        {
            breakpoint: 575,
            settings: {
                arrows: true,
                slidesToShow: 2,
                slidesToScroll: 1
            }
        }
        ]
    });
    // single product slider

    jQuery('.slider').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: true,
        arrows: true,
        fade: false,
        asNavFor: '.slider-nav-thumbnails',
    });

    jQuery('.slider-nav-thumbnails').slick({
        slidesToShow: 5,
        slidesToScroll: 1,
        asNavFor: '.slider',
        dots: true,
        focusOnSelect: true
    });

    // watch slider 

    jQuery(".watch-slider").slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 2000,
        arrows: true,
        fade: false,
        adaptiveHeight: true,
        asNavFor: '.watch-slider-nav-thumbnails'
    });
    jQuery('.watch-slider-nav-thumbnails').slick({
        slidesToShow: 4,
        slidesToScroll: 1,
        asNavFor: '.watch-slider',
        dots: true,
        centerMode: true,
        focusOnSelect: true
    });

    // Remove active class from all thumbnail slides
    jQuery('.slider-nav-thumbnails .slick-slide').removeClass('slick-active');

    // Set active class to first thumbnail slides
    jQuery('.slider-nav-thumbnails .slick-slide').eq(0).addClass('slick-active');

    // On before slide change match active thumbnail to current slide
    jQuery('.slider').on('beforeChange', function (event, slick, currentSlide, nextSlide) {
        var mySlideNumber = nextSlide;
        jQuery('.slider-nav-thumbnails .slick-slide').removeClass('slick-active');
        jQuery('.slider-nav-thumbnails .slick-slide').eq(mySlideNumber).addClass('slick-active');
    });

    //animation
    new WOW().init();


    $(window).scroll(function () {
        var scroll = $(window).scrollTop();

        if (scroll >= 50) {
            $("body").addClass("sticky");
        } else {
            $("body").removeClass("sticky");
        }
    });

    $(function () {
        // ------------------------------------------------------- //
        // Multi Level dropdowns
        // ------------------------------------------------------ //
        $("ul.dropdown-menu [data-toggle='dropdown']").on("click", function (event) {
            event.preventDefault();
            event.stopPropagation();

            $(this).siblings().toggleClass("show");


            if (!$(this).next().hasClass('show')) {
                $(this).parents('.dropdown-menu').first().find('.show').removeClass("show");
            }
            $(this).parents('li.nav-item.dropdown.show').on('hidden.bs.dropdown', function (e) {
                $('.dropdown-submenu .show').removeClass("show");
            });

        });
    });

    $(".watch-product-item").children("a.icon").click(function () {
        $(this).siblings(".product-thumb").children(".inside").toggleClass("active");
    })

    $(".watch-product-item").mouseleave(function () {
        $(".product-thumb .inside").removeClass("active");
    });


    /*-------------------
    Range Slider
  --------------------- */
    var rangeSlider = $(".price-range"),
        minamount = $("#minamount"),
        maxamount = $("#maxamount"),
        minPrice = rangeSlider.data('min'),
        maxPrice = rangeSlider.data('max');
    rangeSlider.slider({
        range: true,
        min: minPrice,
        max: maxPrice,
        values: [minPrice, maxPrice],
        slide: function (event, ui) {
            minamount.val('$' + ui.values[0]);
            maxamount.val('$' + ui.values[1]);
        }
    });
    minamount.val('$' + rangeSlider.slider("values", 0));
    maxamount.val('$' + rangeSlider.slider("values", 1));



});

//-------------------Select only one option from checkbox----------------------- //

$("input:checkbox").on('click', function() {
  // in the handler, 'this' refers to the box clicked on
  var $box = $(this);
  if ($box.is(":checked")) {
    // the name of the box is retrieved using the .attr() method
    // as it is assumed and expected to be immutable
    var group = "input:checkbox[name='" + $box.attr("name") + "']";
    // the checked state of the group/box on the other hand will change
    // and the current value is retrieved using .prop() method
    $(group).prop("checked", false);
    $box.prop("checked", true);
  } else {
    $box.prop("checked", false);
  }
});

//-------------------Login or Register----------------------- //
$('#usernameEmail').on('change',function(){ 
	
	$('#usernameEmail').focusout(function(){
	    $('#usernameEmail').filter(function(){
	      var email = $('#usernameEmail').val();
	      var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
	      if ( !emailReg.test( email ) ) {
	    	  emailcheck.style.color = 'red';
	    	  emailcheck.innerHTML = "Please Enter Valid Email";
	    	  
	      }else {
	    	  //alert('else');
	    	  emailcheck.style.color = 'green';
	    	  emailcheck.innerHTML = "";
	    	  $.ajax({
	    			url: "/user/checkUser",
	    			type: "GET",
	    			data: {email: email},
	    			dataType: 'json',
	    			success : function(data){
	    				var response = jQuery.trim(data);
	    				if(response == 'true'){
	    	                $('#rPassword').removeClass('d-none');
	    	            }
	    	            else{               
	    	                $('#loginForm').addClass('d-none');
	    	                $('#registerForm').removeClass('d-none');
	    	                $('#email').val(email);
	    	                $('#useremail').val(email);
	    	            }	
	    				}
	    			});  
	      }
	    });
	  });    
	    
});

$('#userLogin').on('click',function(){   
	var email = $('#usernameEmail').val();
	var password = $('#rPassword').val();
	
	var isError = false; 
	if(email == ""){
		$("#usernameEmail").css('border','1px solid red');
		isError = true;
	}if(password == ""){
		$('#rPassword').css('border','1px solid red');
		isError = true;
	}
	
	if(isError == false){
	$.ajax({
		url: "/user/logincheck",
		type: "POST",
        dataType:'json',
		headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
		data: {"email": email, "password": password},
		success : function(data){
			// console.log(data.status);
            if(data.status === 1){
    			location.reload(true);
    		}else if(data.status === 0){
				$('#error').addClass('alert alert-danger').text(data.message);
			}
		}
	});  
}
});

function checkPasswordMatch() {
    var password = $("#UserPassword").val();
    var confirmPassword = $("#confirmPassword").val();
    if (password != confirmPassword)
        $("#CheckPasswordMatch").html("Passwords does not match!");
    else
        $("#CheckPasswordMatch").html("Passwords match.");
}

function checkphonenumber() {
	
    var mobile = $("#mobileNumber").val();

    if(mobile.length !=10){
        
    	
        message.style.color = 'red';
        message.innerHTML = "required 10 digits!"
    }else {
    	message.style.color = 'green';
    	message.innerHTML = "";
    }
}

$(document).ready(function () {
   $("#confirmPassword").keyup(checkPasswordMatch);
   
   $("#mobileNumber").keyup(checkphonenumber);
});



$(document).ready(function(){
    var success_msg = $('#success_msg').val();
	
	if(success_msg === "User Register Successfully!"){
		$("#forgotPasswordModal").modal("show");
	}
	
    // setTimeout(function(){
    //   $("#forgotPasswordModal").modal("hide");
    // }, 3000 ); 
  });
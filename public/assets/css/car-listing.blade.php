<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Listing Page</title>
    <link href="https://fonts.googleapis.com/css2?family=Muli:wght@200;300;400;500;600;700;800;900&display=swap"
        rel="stylesheet">
    <link rel="stylesheet" href="/assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/hover.css/2.3.1/css/hover-min.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.css" />
    <link rel="stylesheet" href="/assets/css/animate.css">
    <link rel="stylesheet" href="/assets/css/jquery-ui.min.css" />
    <link rel="stylesheet" href="/assets/css/font-style.css">
    <link rel="stylesheet" href="/assets/css/local.css">
    <link rel="stylesheet" href="/assets/css/filters.css">
    <link rel="stylesheet" href="/assets/css/responsive.css">
</head>

<body>

    <header class="custom-header-wrapper">
        <div class="custom-top-header">
            <div class="container fulldeg">
                <div class="row">
                    <div class="col-lg-4 col-md-6 col-sm-6 col-12">
                        <div class="header-social-icon">
                            <ul>
                                <li><a class="hvr-buzz-out" href="https://www.facebook.com/tangiblelistings"><i class="fab fa-facebook-f"></i></a></li>
                                <li><a class="hvr-buzz-out" href="https://www.instagram.com/tangiblelistings/"><i class="fab fa-instagram"></i></a></li>
                                
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-8 col-md-6 col-sm-6 col-12">
                        <div class="header-phone">
                        <div class="dropdown">
                                <button class="btn float-right text-white dropdown-toggle" type="button" data-toggle="dropdown">Account
                                    <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu">
                                <!-- <li><a class="hvr-buzz-out" href="tel:(123)987654">(123) 987654</a></li>
                                <li><a class="hvr-buzz-out" href="mailto:info@tangiblelistings.com">info@tangiblelistings.com</a></li> -->
                                <li>
                                @if (Route::has('login'))
                                @auth
                                <a class="hvr-buzz-out text-dark" href="{{ url('/admin/home') }}">Home</a>
                                @else
                                <a class="hvr-buzz-out text-dark" href="{{ route('login') }}">Login</a> </br>

                                @if (Route::has('register'))
                                <a class="hvr-buzz-out text-dark" href="{{ route('register') }}">Register</a>
                                @endif 
                                @endauth
                                @endif</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="custom-bottom-header about-header">
            <nav class="navbar navbar-expand-lg">
                <div class="container fulldeg">
                    <a href="/" class="navbar-brand">
                        <img src="/assets/img/logo-header.png" class="img-fluid">
                    </a>
                    <button type="button" data-toggle="collapse" data-target="#navbarContent" aria-controls="navbars"
                        aria-expanded="false" aria-label="Toggle navigation" class="navbar-toggler">
                        <span class="navbar-toggler-icon"><i class="fas fa-bars"></i></span>
                    </button>
                    <div id="navbarContent" class="collapse navbar-collapse">
                        <ul class="navbar-nav">
                        	<!--<li class="nav-item"><a href="/" class="nav-link">Home </a></li>-->
                            <li class="nav-item active"><a href="/tangibleautomobiles" class="nav-link">Automobiles </a></li>
                            <li class="nav-item "><a href="/tangiblerealestate" class="nav-link">Real Estate</a></li>
                            <li class="nav-item "><a href="/tangiblewatches" class="nav-link">Watches</a></li>
                            <!-- <li class="nav-item"><a href="#" class="nav-link"> Articles </a></li>
                            <li class="nav-item"><a href="#" class="nav-link"> Details </a></li> -->
                            <!-- Level one dropdown -->
                            <!-- <li class="nav-item dropdown">
                              <a id="dropdownMenu1" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="nav-link dropdown-toggle">About us</a>
                              <ul aria-labelledby="dropdownMenu1" class="dropdown-menu border-0 shadow">
                                  <li><a href="become-a-seller" class="dropdown-item">Become A Seller</a></li>
                                  <li><a href="#" class="dropdown-item">Team</a></li>
                              </ul>
                          </li> -->
                            <!-- End Level one -->
                            <!-- <li class="nav-item"><a href="#" class="nav-link">Contact</a></li> -->
                        </ul>
                    </div>
                </div>
            </nav>
        </div>
    </header>

    <div class="custom-home-listing-wrapper ">
        <nav aria-label="breadcrumb ">
            <div class="container-fluid ">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/tangibleautomobiles">Home</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Car</li>
                </ol>
            </div>
        </nav>
        <div class="container-fluid pt-4 ">

            <div class="row">
<div class="col-lg-3 order-1">
                    <div class="product-filter" id="filter-form">
                    <form id="searchForm" method="GET" action="{{url('/carlisting/search')}}">
                        <div class="product-filter-inner">
                            
                            <div class="filter">
                                <p><i class="fa fa-filter" aria-hidden="true"></i> Refine Search</p>
                                <a href="javascript:;"><i class="fa fa-undo" aria-hidden="true" id="clear"> Clear</a></i>
                            </div>
                            <div class="filter-area">

                                <div class="filter-box">
                                    <a class="filter-text collapsed" data-toggle="collapse" href="#make-filter"
                                        role="button" aria-expanded="false" aria-controls="make-filter">
                                        Make <span><i class="fa fa-caret-right" aria-hidden="true"></i></span>
                                    </a>
                                    <div class="collapse" id="make-filter">
                                        <div class="inner-text">
                                            <ul id="brands">
                                                @foreach($makes as $make)
                                                <li>
                                                    <a href="#">
                                                        <div class="custom-control">
                                                        @if(isset($makeSelected))
                                                            <input type="checkbox" class="" id="{{$make->automobile_brand_name}}" name="make-{{$make->id}}" value="{{$make->id}}" {{in_array($make->id,$makeSelected) ? "Checked" : ''}}>
                                                         @else
                                                         	<input type="checkbox" class="" id="{{$make->automobile_brand_name}}" name="make-{{$make->id}}" value="{{$make->id}}">
                                                         @endif	   
                                                            <label class="custom-control-label" for="{{$make->automobile_brand_name}}">{{$make->automobile_brand_name}}</label>
                                                        </div>
                                                        <!-- <div class="value">(867)</div> -->
                                                    </a>
                                                </li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="filter-box">
                                    <a class="filter-text collapsed" data-toggle="collapse" href="#model-filter"
                                        role="button" aria-expanded="false" aria-controls="model-filter">
                                        Model <span><i class="fa fa-caret-right" aria-hidden="true"></i></span>
                                    </a>
                                    <div class="collapse" id="model-filter">
                                        <div class="inner-text">
                                            <ul id="model">
                                             @foreach($models as $model)
                                                <li>
                                                    <a href="#">
                                                        <div class="custom-control">
                                                        @if(isset($modelSelected))
                                                            <input type="checkbox" class="" id="{{$model->automobile_model_name}}" name="model-{{$model->id}}" value ="{{$model->id}}" {{in_array($model->id,$modelSelected) ? "Checked" : ''}}>
                                                        @else
                                                        	<input type="checkbox" class="" id="{{$model->automobile_model_name}}" name="model-{{$model->id}}" value ="{{$model->id}}">    
                                                         @endif
                                                            <label class="custom-control-label" for="{{$model->automobile_model_name}}">{{$model->automobile_model_name}}</label>
                                                        </div>
                                                        <!-- <div class="value">(867)</div> -->
                                                    </a>
                                                </li>
                                               @endforeach
                                            </ul>
                                        </div>
                                    </div>
                                </div>

                                <div class="filter-box">
                                    <a class="filter-text collapsed" data-toggle="collapse" href="#condition-filter"
                                        role="button" aria-expanded="false" aria-controls="condition-filter">
                                        Condition <span><i class="fa fa-caret-right" aria-hidden="true"></i></span>
                                    </a>
                                    <div class="collapse" id="condition-filter">
                                        <div class="inner-text">
                                            <div class="pt-2 pb-1 pl-3">
                                                <div class="custom-control custom-radio custom-control-inline">
                                                @if(isset($condition))
                                                    <input type="radio" id="customRadioInline1"
                                                        name="customRadioInline1" value= "New" class="custom-control-input" {{$condition == 'New' ? "Checked" : ""}}>
                                                 @else        
                                                 	<input type="radio" id="customRadioInline1"
                                                        name="customRadioInline1" value= "New" class="custom-control-input">
                                                 @endif       
                                                    <label class="custom-control-label"
                                                        for="customRadioInline1">New</label>
                                                </div>
                                                <div class="custom-control custom-radio custom-control-inline">
                                                @if(isset($condition))
                                                    <input type="radio" id="customRadioInline2"
                                                        name="customRadioInline1" value= "Used" class="custom-control-input" {{$condition == 'Used' ? "Checked" : ""}}>
                                                @else        
                                                	<input type="radio" id="customRadioInline2"
                                                        name="customRadioInline1" value= "Used" class="custom-control-input">
                                                @endif        
                                                    <label class="custom-control-label"
                                                        for="customRadioInline2">Used</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                <div class="filter-box">
                                    <a class="filter-text collapsed" data-toggle="collapse" href="#transmission-filter"
                                        role="button" aria-expanded="false" aria-controls="transmission-filter">
                                        Transmission <span><i class="fa fa-caret-right" aria-hidden="true"></i></span>
                                    </a>
                                    <div class="collapse" id="transmission-filter">
                                        <div class="inner-text">
                                            <ul>
                                            @foreach($transmissions as $transmission)
                                                <li>
                                                    <a href="#">
                                                        <div class="custom-control">
                                                        @if(isset($transmissionSelected))
                                                            <input type="checkbox" class="" id="{{$transmission->transmission}}" name="transmission-{{$transmission->id}}" value="{{$transmission->id}}" {{in_array($transmission->id,$transmissionSelected) ? "Checked" : ''}}>
                                                        @else
                                                        	<input type="checkbox" class="" id="{{$transmission->transmission}}" name="transmission-{{$transmission->id}}" value="{{$transmission->id}}">
                                                        @endif	    
                                                            <label class="custom-control-label" for="{{$transmission->transmission}}">{{$transmission->transmission}}</label>
                                                        </div>
                                                     <!-- <div class="value">(867)</div> -->   
                                                    </a>
                                                </li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    </div>
                                </div>

                                <div class="filter-box">
                                    <a class="filter-text collapsed" data-toggle="collapse" href="#odometer-filter"
                                        role="button" aria-expanded="false" aria-controls="odometer-filter">
                                        <div class="icon-heading"> Odometer </div>
                                        <span><i class="fa fa-caret-right" aria-hidden="true"></i></span>
                                    </a>
                                    <div class="collapse" id="odometer-filter">
                                        <div class="inner-text">
                                            <div class="form-row mb-2 mt-2">
                                                <label for="from">From :</label>
                                                <select class="form-control custom-select" required id="from" name="odoFrom">
                                                    @if(isset($odoFrom))
                                                    <option selected disabled>{{$odoFrom}} km</option>
                                                    <option value = "0" >0 km</option>
                                                    <option value="10000">10,000 km</option>
                                                    <option value="20000">20,000 km</option>
                                                    <option value="30000">30,000 km</option>
                                                    <option value="40000">40,000 km</option>
                                                    <option value="50000">50,000 km</option>
                                                    <option value="60000">60,000 km</option>
                                                    <option value="70000">70,000 km</option>
                                                    <option value="80000">80,000 km</option>
                                                    <option value="90000">90,000 km</option>
                                                    @else
                                                    <option selected disabled>0 km</option>
                                                    <option value="10000">10,000 km</option>
                                                    <option value="20000">20,000 km</option>
                                                    <option value="30000">30,000 km</option>
                                                    <option value="40000">40,000 km</option>
                                                    <option value="50000">50,000 km</option>
                                                    <option value="60000">60,000 km</option>
                                                    <option value="70000">70,000 km</option>
                                                    <option value="80000">80,000 km</option>
                                                    <option value="90000">90,000 km</option>
                                                    @endif
                                                </select>
                                            </div>
                                            <div class="form-row mb-2">
                                                <label for="to">To :</label>
                                                <select class="form-control custom-select" required id="to" name="odoTo">
                                                @if(isset($odoTo))
                                                    <option selected disabled>{{$odoTo}} km</option>
                                                    <option value="10000">10,000 km</option>
                                                    <option value="20000">20,000 km</option>
                                                    <option value="30000">30,000 km</option>
                                                    <option value="40000">40,000 km</option>
                                                    <option value="50000">50,000 km</option>
                                                    <option value="60000">60,000 km</option>
                                                    <option value="70000">70,000 km</option>
                                                    <option value="80000">80,000 km</option>
                                                    <option value="90000">90,000 km</option>
                                                    <option value="100000">1,00,000 km</option>
                                                 @else
                                                 	<option selected disabled>10,000 km</option>
                                                    <option value="20000">20,000 km</option>
                                                    <option value="30000">30,000 km</option>
                                                    <option value="40000">40,000 km</option>
                                                    <option value="50000">50,000 km</option>
                                                    <option value="60000">60,000 km</option>
                                                    <option value="70000">70,000 km</option>
                                                    <option value="80000">80,000 km</option>
                                                    <option value="90000">90,000 km</option>
                                                    <option value="100000">1,00,000 km</option>
                                                  @endif     
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                              <!--   <div class="filter-box">
                                    <a class="filter-text collapsed" data-toggle="collapse" href="#cylinders-filter"
                                        role="button" aria-expanded="false" aria-controls="cylinders-filter">
                                        Cylinders <span><i class="fa fa-caret-right" aria-hidden="true"></i></span>
                                    </a>
                                    <div class="collapse" id="cylinders-filter">
                                        <div class="inner-text">
                                            <ul>
                                                <li>
                                                    <a href="#">
                                                        <div class="custom-control">
                                                            <input type="checkbox" class="" id="cylinders 1">
                                                            <label class="custom-control-label" for="cylinders 1">cylinders 1</label>
                                                        </div>
                                                        <div class="value">(867)</div>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#">
                                                        <div class="custom-control">
                                                            <input type="checkbox" class="" id="cylinders 2">
                                                            <label class="custom-control-label" for="cylinders 2">cylinders 2</label>
                                                        </div>
                                                        <div class="value">(867)</div>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#">
                                                        <div class="custom-control">
                                                            <input type="checkbox" class="" id="cylinders 3">
                                                            <label class="custom-control-label" for="cylinders 3">cylinders 3</label>
                                                        </div>
                                                        <div class="value">(867)</div>
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div> -->

                                <div class="filter-box">
                                    <a class="filter-text collapsed" data-toggle="collapse" href="#fuletype-filter"
                                        role="button" aria-expanded="false" aria-controls="fuletype-filter">
                                        Fuel Type <span><i class="fa fa-caret-right" aria-hidden="true"></i></span>
                                    </a>
                                    <div class="collapse" id="fuletype-filter">
                                        <div class="inner-text">
                                            <ul>
                                            @foreach($fuel_types as $fuel_type)
                                                <li>
                                                    <a href="#">
                                                        <div class="custom-control">
                                                        @if(isset($fuelSelected))
                                                            <input type="checkbox" class="" id="{{$fuel_type->fuel_type}}" name="fuel_type-{{$fuel_type->id}}" value="{{$fuel_type->id}}" {{in_array($fuel_type->id,$fuelSelected) ? "Checked" : ''}}>
                                                        @else
                                                        	<input type="checkbox" class="" id="{{$fuel_type->fuel_type}}" name="fuel_type-{{$fuel_type->id}}" value="{{$fuel_type->id}}">
                                                        @endif	    
                                                            <label class="custom-control-label" for="{{$fuel_type->fuel_type}}">{{$fuel_type->fuel_type}}</label>
                                                        </div>
                                                      <!-- <div class="value">(867)</div> -->  
                                                    </a>
                                                </li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    </div>
                                </div>

                                <div class="filter-box">
                                    <a class="filter-text collapsed" data-toggle="collapse" href="#style-filter"
                                        role="button" aria-expanded="false" aria-controls="style-filter">
                                        <div>
                                            Style</div> <span><i class="fa fa-caret-right"
                                                aria-hidden="true"></i></span>
                                    </a>
                                    <div class="collapse" id="style-filter">
                                        <div class="inner-text">
                                            <ul>
                                            @foreach($body_types as $body_type)
                                                <li>
                                                    <a href="#">
                                                        <div class="custom-control">
                                                        @if(isset($bodySelected))
                                                            <input type="checkbox" class="" id="{{$body_type->body_type}}" name="body_type-{{$body_type->id}}" value="{{$body_type->id}}" {{in_array($body_type->id,$bodySelected) ? "Checked" : ''}}>
                                                        @else
                                                        	<input type="checkbox" class="" id="{{$body_type->body_type}}" name="body_type-{{$body_type->id}}" value="{{$body_type->id}}">
                                                        @endif	    
                                                            <label class="custom-control-label" for="{{$body_type->body_type}}">{{$body_type->body_type}}</label>
                                                        </div>
                                                        <!-- <div class="value">(867)</div> -->
                                                    </a>
                                                </li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    </div>
                                </div>


                                <div class="filter-box icon">
                                    <a class="filter-text collapsed" data-toggle="collapse" href="#drive-filter"
                                        role="button" aria-expanded="false" aria-controls="drive-filter">
                                        <div class="icon-heading"><i class="fa fa-dot-circle-o" aria-hidden="true"></i>
                                            Drive</div> <span><i class="fa fa-caret-right"
                                                aria-hidden="true"></i></span>
                                    </a>
                                    <div class="collapse" id="drive-filter">
                                        <div class="inner-text">
                                            <div class="pt-2 pb-1 pl-3">
                                                <div class="custom-control custom-radio">
                                                @if(isset($driveSelected))
                                                    <input type="radio" id="gender1" name="gender1" value="lhd"
                                                        class="custom-control-input" {{$driveSelected == 'lhd' ? "Checked" : ""}}>
                                                @else
                                                <input type="radio" id="gender1" name="gender1" value="lhd"
                                                        class="custom-control-input">
                                                @endif                
                                                    <label class="custom-control-label" for="gender1">LHD</label>
                                                </div>
                                                <div class="custom-control custom-radio">
                                                @if(isset($driveSelected))
                                                    <input type="radio" id="gender2" name="gender1" value="rhd"
                                                        class="custom-control-input" {{$driveSelected == 'rhd' ? "Checked" : ""}}>
                                                @else
                                                	<input type="radio" id="gender2" name="gender1" value="rhd"
                                                        class="custom-control-input">
                                                @endif                
                                                    <label class="custom-control-label" for="gender2">RHD</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="filter-box icon">
                                    <a class="filter-text" data-toggle="collapse" href="#price-filter" role="button"
                                        aria-expanded="false" aria-controls="price-filter">
                                        <div class="icon-heading"><i class="fa fa-money" aria-hidden="true"></i> Price
                                        </div> <span><i class="fa fa-caret-right" aria-hidden="true"></i></span>
                                    </a>
                                    <div class="collapse show" id="price-filter">
                                        <div class="inner-text">
                                            <div class="values">
                                                <!-- <div class="min-price"></div>
                                    <p class="max-price mt-3"></p> -->
                                                <div id="slider-range" class="price-filter-range" name="rangeInput">
                                                </div>
                                                <div
                                                    style="margin: 20px auto 5px;display: flex;justify-content: space-between;align-items: center;text-align: center;">
                                                    <div class="div">
                                                        <label style="line-height: 1;" class="d-block"
                                                            for="min_price">Min</label>
                                                        <input type="number" min=0 max={{$price_max-10}}
                                                            oninput="validity.valid||(value='0');" id="min_price" name="minPrice"
                                                            class="price-range-field" />
                                                    </div>
                                                    <div class="div">
                                                        <label style="line-height: 1;" class="d-block"
                                                            for="max_price">Max</label>
                                                        <input type="number" min=0 max={{$price_max}}
                                                            oninput="validity.valid||(value='10000');" id="max_price" name="maxPrice"
                                                            class="price-range-field" />
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="filter-box icon">
                                    <a class="filter-text collapsed" data-toggle="collapse" href="#year-filter"
                                        role="button" aria-expanded="false" aria-controls="year-filter">
                                        <div class="icon-heading"><i class="fa fa-calendar-check-o"
                                                aria-hidden="true"></i> Year</div>
                                        <span><i class="fa fa-caret-right" aria-hidden="true"></i></span>
                                    </a>
                                    <div class="collapse" id="year-filter">
                                        <div class="inner-text">
                                            <div class="form-row mb-2 mt-2">
                                                <label for="yearfrom">Year From :</label>
                                                <select class="form-control custom-select" required id="yearfrom" name="fromYear">
                                                    <option selected disabled>Select Here</option>
                                                    @foreach($years as $year)
                                                    @if(isset($yearMin))
                                                    <option value="{{$year->build_year}}" {{$yearMin == $year->build_year ? "Selected" : ""}}>{{$year->build_year}}</option>
                                                    @else
                                                    <option value="{{$year->build_year}}">{{$year->build_year}}</option>
                                                    @endif
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="form-row mb-2">
                                                <label for="yearto">Year to :</label>
                                                <select class="form-control custom-select" required id="yearto" name="toYear">
                                                    <option selected disabled>Select Here</option>
                                                    @foreach($years as $year)
                                                    @if(isset($yearMax))
                                                    <option value="{{$year->build_year}}" {{$yearMax == $year->build_year ? "Selected" : ""}}>{{$year->build_year}}</option>
                                                    @else
                                                    <option value="{{$year->build_year}}">{{$year->build_year}}</option>
                                                    @endif
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>

								<div class="filter-box icon">
                                    <a class="filter-text collapsed" data-toggle="collapse" href="#dcolor-filter"
                                        role="button" aria-expanded="false" aria-controls="dcolor-filter">
                                        <div class="icon-heading"><i class="fa fa-tachometer" aria-hidden="true"></i>
                                            Color</div>
                                        <span><i class="fa fa-caret-right" aria-hidden="true"></i></span>
                                    </a>
                                    <div class="collapse" id="dcolor-filter">
                                        <div class="inner-text">
                                            <div class="input-group mb-3">
                                                <select class="form-control custom-select mt-3" id="color" name="color">
                                                  <option value="" disabled selected>Select a color...</option>
												  @foreach($colours as $colour)
												  @if(isset($colorSelected))
                                                  <option value="{{$colour->id}}" {{$colorSelected == $colour->id ? "Selected" : ""}}>{{$colour->colour}}</option>
                                                  @else
                                                  <option value="{{$colour->id}}" >{{$colour->colour}}</option>
                                                  @endif
                                                  @endforeach
                                                </select>
                                              </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="filter-box icon">
                                    <a class="filter-text collapsed" data-toggle="collapse" href="#country-filter"
                                        role="button" aria-expanded="false" aria-controls="country-filter">
                                        <div class="icon-heading"><i class="fa fa-globe" aria-hidden="true"></i> Country
                                        </div>
                                        <span><i class="fa fa-caret-right" aria-hidden="true"></i></span>
                                    </a>
                                    <div class="collapse" id="country-filter">
                                        <div class="inner-text">
                                            <ul>
                                            @foreach($country_data as $country)
                                                <li>
                                                    <a href="#">
                                                        <div class="custom-control">
                                                        @if(isset($countrySelected))
                                                            <input type="checkbox" class="" id="{{$country-> country_name}}" name="country-{{$country->id}}" value="{{$country->id}}" {{in_array($country-> id,$countrySelected) ? "Checked" : ""}}>
                                                        @else
                                                        	<input type="checkbox" class="" id="{{$country-> country_name}}" name="country-{{$country->id}}" value="{{$country->id}}">
                                                        @endif	    
                                                            <label class="custom-control-label" for="{{$country-> country_name}}">{{$country-> country_name}}</label>
                                                        </div>
                                                      <!-- <div class="value">(867)</div> -->  
                                                    </a>
                                                </li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    </div>
                                </div>
							    <button type="submit" id= "searchButton" class="site-btn">Search</button>
                            </div>
                        </div>
                        </form>
                    </div>

                </div>
                <div class="col-lg-9 mb-3 order-2">
                    <div class="main-heading mb-0">
                        <h3>Luxury Cars for Sale Worldwide</h3>
                        <p>Classic and contemporary design</p>
                    </div>

                    <div class="sorting-panel">
                        <form action="#" class="needs-validation form-inline justify-content-end mb-3" novalidate>
                            <label for="sortby" class="mr-2">Sort by</label>
                            <select id="sortBy" class="form-control custom-select " name="sortBy">
                                @if(!isset($sortBy))
                                <option value="0" selected>Popularity</option>
                                @else
                                <option value="0">Popularity</option>
                                @endif
                                @if(isset($sortBy) && $sortBy == "1")
                                <option value="1" selected>High to low</option>
                                @else
                                <option value="1">High to low</option>
                                @endif
                                @if(isset($sortBy) && $sortBy == "2")
                                <option value="2" selected>Low to high</option>
                                @else
                                <option value="2">Low to high</option>
                                @endif
                                @if(isset($sortBy) && $sortBy == "3")
                                <option value="3" selected>Recent</option>
                                @else
                                <option value="3">Recent</option>
                                @endif
                            </select>
                        </form>
                    </div>


                    <div class="row  car-listing-row" id="listing">
						@foreach($automobileListing as $automobile)
                        <div class="col-sm-4">
                            <div class="stm-directory-grid-loop">
                                <div class="image">
                                    <div class="car-listing-slider">
                                    @foreach($images->where('listing_id',$automobile->id) as $image)
                                        <a href="{{route('carlisting.show',$automobile->id)}}"><img src="{{url('uploads/'.$image->filename)}}" alt=""></a>
                                        @endforeach
                                    </div>
                                    <div class="stm-badge-directory heading-font Special ">Special</div>
                                </div>
                                <div class="listing-car-item-meta">
                                    <div class="car-meta-top heading-font clearfix">
                                        <div class="price">
                                            <div class="normal-price">{{currency()->convert(floatval($automobile->value), $automobile->currency_code, currency()->getUserCurrency())}}</div>
                                        </div>
                                        <div class="car-title">
                                            <div class="labels">{{$automobile->neworused .' '. $automobile->build_year}}</div> {{$automobile->ad_title}}
                                        </div>
                                    </div>
                                    <div class="car-meta-bottom">
                                        <ul>
                                            <li>
                                                <i class="fa fa-road"></i>

                                                <span>{{$automobile->odometer}} </span>
                                            </li>
                                            <li>
                                                <i class="fa fa-car"></i>

                                                <span>12/14 </span>
                                            </li>
                                            <li>
                                                <i class="fa fa-gear"></i>
                                                <span>{{$automobile->transmission}}</span>
                                            </li>
                                            <li>
                                                <i class="fa fa-hand-paper-o"></i>
                                                <span>{{$automobile->rhdorlhd}}</span>
                                            </li>
                                            <li>
                                                <i class="fa fa-calendar"></i>
                                                <span>{{date('Y', strtotime($automobile->automobilecreationtime))}}</span>
                                            </li>
                                        </ul>
                                    </div>

                                    <div class="dealer-logo">
                                        <div>
                                            <ul>
                                                <li class="active"><i class="fa fa-star"></i></li>
                                                <li class="active"><i class="fa fa-star"></i></li>
                                                <li class="active"><i class="fa fa-star"></i></li>
                                                <li><i class="fa fa-star"></i></li>
                                                <li><i class="fa fa-star"></i></li>
                                            </ul>
                                            <p><span><i class="fa fa-check-square-o" aria-hidden="true"></i></span>
                                                {{ $automobile->agency_id === 0 ? "Private Seller" : "Dealer" }}</p>
                                        </div>
                                        <div class="country-flag">
                                            <img src="{{url('uploads/'.$automobile->filename)}}" alt="">
                                            <p class="c-code">{{$automobile->countrycode}}</p>
                                        </div>
                                    </div>

                                </div>
                                </a>
                            </div>
                        </div>
					@endforeach
                        
                    </div>

                     @if($automobileListing->hasPages())
          <div class="custom-pagination mt-4 d-flex justify-content-end">
            <nav aria-label="Page navigation example">
                <ul class="pagination">
              @if ($automobileListing->onFirstPage())
            <li class="page-item disabled"><a class="page-link" href="#">Previous</a></li>
        @else
        	<li class="page-item"><a class="page-link" href="{{ $automobileListing->withQueryString()->previousPageUrl() }}">Previous</a></li>
        @endif
        
        @for($i = 1 ; $i <= $automobileListing->lastPage() ; $i++)
        	@if($automobileListing->currentPage() == $i)
			<li class="page-item active"><a class="page-link" href="{{$automobileListing->withQueryString()->url($i)}}">{{$i}}</a></li>
			@else
			<li class="page-item"><a class="page-link" href="{{$automobileListing->withQueryString()->url($i)}}">{{$i}}</a></li>
			@endif
        @endfor        
          @if ($automobileListing->hasMorePages())
            <li class="page-item"><a class="page-link" href="{{ $automobileListing->withQueryString()->nextPageUrl() }}">Next</a></li>
        @else
            <li class="page-item disabled"><a class="page-link" href="#">Next</a></li>
        @endif    
              </ul>
            </nav>
          </div>
          @endif
                </div>
            </div>
        </div>
    </div>
    <footer>
        <div class="custom-top-footer pt-5 pb-5">
            <div class="container fulldeg">
                <div class="row">
                    <div class="col-lg-5 mr-auto col-md-4 mb-4">
                        <div class="custom-footer-logo">
                            <a href="/"><img src="/assets/img/logo-header.png" class="img-fluid"></a>
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum
                                has been the industry's standard dummy text ever since the 1500s, when an unknown
                                printer took a galley of type and scrambled it to make
                                a type specimen book.</p>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-4 mb-4">
                        <div class="custom-footer-links footer-social">
                            <h3>Contact Us</h3>
                            <ul>
                                <li>
                                    <i class="fas fa-map-marker-alt"></i> {{$contactUs->address}}
                                </li>
                                <li>
                                    Call us FREE<a href="#"><i class="fas fa-mobile-alt"></i> {{$contactUs->mobile}}</a>
                                </li>
                                <li>
                                    <a href="mailto:{{$contactUs->email}}"><i class="far fa-envelope"></i> {{$contactUs->email}} </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-4">
                        <div class="custom-footer-links footer-nav">
                            <h3>Important Links</h3>
                            <ul>
                                @if($propertyType->firstWhere('property_type', 'Residential'))
                                <li>
                                    <a href="{{url('/propertylisting/search?home-Residential='. $propertyType->firstWhere('property_type', 'Residential')->id)}}">Residential</a>
                                </li>
                                @endif
                                @if($propertyType->firstWhere('property_type', 'Apartment'))
                                <li>
                                    <a href="{{url('/propertylisting/search?home-Apartment='. $propertyType->firstWhere('property_type', 'Apartment')->id)}}">Apartment</a>
                                </li>
                                @endif
                                @if($propertyType->firstWhere('property_type', 'Single Family Home'))
                                <li>
                                    <a href="{{url('/propertylisting/search?home-Single Family Home='. $propertyType->firstWhere('property_type', 'Single Family Home')->id)}}">Single Family Home</a>
                                </li>
                                @endif
                                @if($propertyType->firstWhere('property_type', 'Villa'))
                                <li>
                                    <a href="{{url('/propertylisting/search?home-Villa='. $propertyType->firstWhere('property_type', 'Villa')->id)}}">Villa</a>
                                </li>
                                @endif
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="custom-bottom-footer pt-3 pb-3">
            <div class="container fulldeg">
                <div class="row">
                    <div class="col-md-6 col-sm-6 col-12">
                        <div class="copyright-wrapper">
                            <p>© Tangible - All rights reserved</p>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6 col-12">
                        <div class="footer-social-links">
                            <ul>
                                <li><a class="hvr-buzz-out" href="https://www.facebook.com/tangiblelistings"><i class="fab fa-facebook-f"></i></a></li>
                                <li><a class="hvr-buzz-out" href="https://www.instagram.com/tangiblelistings/"><i class="fab fa-instagram"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>



    <script src="/assets/js/jquery.js"></script>
    <script src="/assets/js/bootstrap.min.js"></script>
    <script src="/assets/js/wow.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/wow/1.1.2/wow.min.js"></script>
    <script src="/assets/js/jquery-ui.min.js"></script>
    <script src="/assets/js/local.js"></script>
</body>

<script type="text/javascript">

$(document).ready(function(){ 

	$("#sortBy").change(function(){
		var urlSortBy = window.location.href;
		if(urlSortBy.includes("/carlisting?")){
			var finalUrl = urlSortBy + '&sortBy=' +$(this).val();
			}else  if(urlSortBy.includes("/carlisting")){
		var finalUrl = urlSortBy + '/search?sortBy=' +$(this).val();
		}
		if(urlSortBy.includes("search")){
			if(urlSortBy.includes("sortBy")){
				finalUrl = urlSortBy.replace(/sortBy=\d/,'sortBy=' + +$(this).val());
				}else{
			finalUrl = window.location.href + '&sortBy=' + +$(this).val();
				}
			}
		window.location.assign(finalUrl);
	});
	
	$(function () {
        $("#slider-range").slider({
            range: true,
            orientation: "horizontal",
            min: 0,
            max: {{$price_max}},
            values: [0, {{$price_max}}],
            step: 100,

            slide: function (event, ui) {
                if (ui.values[0] == ui.values[1]) {
                    return false;
                }

               $("#min_price").val(ui.values[0]);
                $("#max_price").val(ui.values[1]);
            }
        });

        $("#min_price").val($("#slider-range").slider("values", 0));
        $("#max_price").val($("#slider-range").slider("values", 1));

    });

	$('#clear').click(function (e) {
		console.log('-----------------------------');
		  $('#filter-form').find(':input').each(function() {
		    if(this.type == 'submit'){
		          //do nothing
		      }
		      else if(this.type == 'checkbox' || this.type == 'radio') {
		        this.checked = false;
		      }
		   })
		   $('#yearfrom')[0].selectedIndex = 0;
		  $('#yearto')[0].selectedIndex = 0;

		  $('#from')[0].selectedIndex = 0;
		  $('#to')[0].selectedIndex = 0;

		  $('#color')[0].selectedIndex = 0;

		  $(function () {
		        $("#slider-range").slider({
		            range: true,
		            orientation: "horizontal",
		            min: 0,
		            max: {{$price_max}},
		            values: [0, {{$price_max}}],
		            step: 100,

		            slide: function (event, ui) {
		                if (ui.values[0] == ui.values[1]) {
		                    return false;
		                }

		               $("#min_price").val(ui.values[0]);
		                $("#max_price").val(ui.values[1]);
		            }
		        });

		        $("#min_price").val($("#slider-range").slider("values", 0));
		        $("#max_price").val($("#slider-range").slider("values", 1));

		    });
		});
	
	$('#brands').click(function(e){
		console.log('------------------------');

		if(e.target.tagName == "INPUT"){
			var checked = []
			var type = "brands"
			$("input[type='checkbox']:checked").each(function ()
			{
			    checked.push($(this).val());
			});
			console.log('checked ',checked);
			$.ajax({
				url: "/carlisting/brand",
				type: "POST",
				data: {brandFilter: checked, _token: '{{csrf_token()}}' },
				dataType: 'json',
				success : function(data){
					console.log(data);
					var listingHtml = "";

					if(data['model'] != null){
						var modelHtml = "";
						var modelData =data['model'];  
						for(var i=0; i < modelData.length; i++){
							modelHtml += `<li>
		                          <a href="#">
		                            <div class="custom-control">
		                              <input type="checkbox" class="" id="${modelData[i].automobile_model_name}" name="model-${modelData[i].id}" value="${modelData[i].id}">
		                              <label class="custom-control-label" for="${modelData[i].automobile_model_name}">${modelData[i].automobile_model_name}</label>
		                            </div>
		                            <!-- <div class="value">(867)</div>  -->
		                          </a>
		                        </li>`;	
							}
						$('#model').html(modelHtml);
							}
					

				}
						
				});
			
			}
		
});

	

});	
</script>

</html>
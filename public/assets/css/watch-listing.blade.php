<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Listing Page</title>
  <link href="https://fonts.googleapis.com/css2?family=Muli:wght@200;300;400;500;600;700;800;900&display=swap"
    rel="stylesheet">
  <link rel="stylesheet" href="/assets/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/hover.css/2.3.1/css/hover-min.css" />
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.css" />
  <link rel="stylesheet" href="/assets/css/animate.css">
  <link rel="stylesheet" href="/assets/css/jquery-ui.min.css" />
  <link rel="stylesheet" href="/assets/css/font-style.css">
  <link rel="stylesheet" href="/assets/css/local.css">
  <link rel="stylesheet" href="/assets/css/filters.css">
  <link rel="stylesheet" href="/assets/css/responsive.css">
</head>

<body>

  <header class="custom-header-wrapper">
    <div class="custom-top-header">
      <div class="container">
        <div class="row">
          <div class="col-lg-4 col-md-6 col-sm-6 col-12">
            <div class="header-social-icon">
              <ul>
                <li><a class="hvr-buzz-out" href="https://www.facebook.com/tangiblelistings"><i class="fab fa-facebook-f"></i></a></li>
                <li><a class="hvr-buzz-out" href="https://www.instagram.com/tangiblelistings/"><i class="fab fa-instagram"></i></a></li>
              </ul>
            </div>
          </div>
          <div class="col-lg-8 col-md-6 col-sm-6 col-12">
            <div class="header-phone">
            <div class="dropdown">
                                <button class="btn float-right text-white dropdown-toggle" type="button" data-toggle="dropdown">Account
                                    <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu">
                                <!-- <li><a class="hvr-buzz-out" href="tel:(123)987654">(123) 987654</a></li>
                                <li><a class="hvr-buzz-out" href="mailto:info@tangiblelistings.com">info@tangiblelistings.com</a></li> -->
                                <li>
                                @if (Route::has('login'))
                                @auth
                                <a class="hvr-buzz-out text-dark" href="{{ url('/admin/home') }}">Home</a>
                                @else
                                <a class="hvr-buzz-out text-dark" href="{{ route('login') }}">Login</a> </br>
                                <!-- @if (Route::has('register'))
                                <a class="hvr-buzz-out text-dark" href="{{ route('register') }}">Register</a>
                                @endif  -->
                                @endauth
                                @endif</li>
                                </ul>
                            </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="custom-bottom-header about-header">
      <nav class="navbar navbar-expand-lg">
        <div class="container-fluid">
          <a href="/" class="navbar-brand">
            <img src="/assets/img/logo-header.png" class="img-fluid">
          </a>
          <button type="button" data-toggle="collapse" data-target="#navbarContent" aria-controls="navbars"
            aria-expanded="false" aria-label="Toggle navigation" class="navbar-toggler">
            <span class="navbar-toggler-icon"><i class="fas fa-bars"></i></span>
          </button>
          <div id="navbarContent" class="collapse navbar-collapse">
            <ul class="navbar-nav">
                <!--<li class="nav-item"><a href="/" class="nav-link"> Home </a></li>-->
                <li class="nav-item"><a href="/tangibleautomobiles" class="nav-link">Automobiles</a></li>
                <li class="nav-item"><a href="/tangiblerealestate" class="nav-link"> Real Estate </a></li>
                <li class="nav-item active" ><a href="/tangiblewatches" class="nav-link">Watches</a></li>
           <!--   <li class="nav-item"><a href="#" class="nav-link">Articles</a></li> -->
              <!-- Level one dropdown -->
            <!--  <li class="nav-item dropdown">
                <a id="dropdownMenu1" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"
                  class="nav-link dropdown-toggle">About</a>
                <ul aria-labelledby="dropdownMenu1" class="dropdown-menu border-0 shadow">
                  <li><a href="#" class="dropdown-item">Company</a></li>
                  <li><a href="#" class="dropdown-item">Team</a></li>
                </ul>
              </li> -->
              <!-- End Level one -->
             <!-- <li class="nav-item"><a href="#" class="nav-link">Contact</a></li> -->
            </ul>
          </div>
        </div>
      </nav>
    </div>
  </header>

  <div class="custom-home-listing-wrapper  ">
    <nav aria-label="breadcrumb ">
      <div class="container-fluid">
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="/tangiblewatches">Home</a></li>
          <li class="breadcrumb-item active" aria-current="page">watch</li>
        </ol>
      </div>
    </nav>
    <div class="container-fluid pt-4">

      <div class="row">
      
<div class="col-lg-3 order-1">
          <div class="product-filter" id="filter-form">
          <form id="searchForm" method="GET" action="{{url('/watchlisting/search')}}">
            <div class="product-filter-inner">
              <div class="filter">
                <p><i class="fa fa-filter" aria-hidden="true"></i> Refine Search</p>
                <a href="javascript:;"><i class="fa fa-undo" aria-hidden="true" id="clear"> Clear</a></i>
              </div>
              <div class="filter-area">

                <div class="filter-box">
                  <a class="filter-text collapsed" data-toggle="collapse" href="#brand-filter" role="button"
                    aria-expanded="false" aria-controls="brand-filter">
                    Brand <span><i class="fa fa-caret-right" aria-hidden="true"></i></span>
                  </a>
                  <div class="collapse" id="brand-filter">
                    <div class="inner-text">
                      <ul id="brand">
                      @foreach($brands as $brand)
                        <li>
                          <a href="#">
                            <div class="custom-control" >
							@if(isset($brandSelected))	                            
                              <input type="checkbox" id="{{$brand->id}}" name="brand-{{$brand->id}}" value="{{$brand->id}}" {{in_array($brand->id,$brandSelected) ? "Checked" : ''}}>
                              @else
                              <input type="checkbox" id="{{$brand->id}}" name="brand-{{$brand->id}}" value="{{$brand->id}}">
                              @endif
                              <label class="custom-control-label" for="{{$brand->id}}" >{{$brand->watch_brand_name}} </label>
                            </div>
                           <!-- <div class="value">(867)</div> -->
                          </a>
                        </li>
                        @endforeach
                      </ul>
                    </div>
                  </div>
                </div>
                <div class="filter-box">
                  <a class="filter-text collapsed" data-toggle="collapse" href="#model-filter" role="button"
                    aria-expanded="false" aria-controls="model-filter">
                    Model <span><i class="fa fa-caret-right" aria-hidden="true"></i></span>
                  </a>
                  <div class="collapse" id="model-filter">
                    <div class="inner-text">
                      <ul id ="model">
                      @foreach($models as $model)
                        <li>
                          <a href="#">
                            <div class="custom-control">
                            @if(isset($modelSelected))
                              <input type="checkbox" id="{{$model->watch_model_name}}" name="model-{{$model->id}}" value="{{$model->id}}" {{in_array($model->id,$modelSelected) ? "Checked" : ''}}>
                              @else
                              <input type="checkbox" id="{{$model->watch_model_name}}" name="model-{{$model->id}}" value="{{$model->id}}" >
                              @endif
                              <label class="custom-control-label" for="{{$model->watch_model_name}}">{{$model->watch_model_name}}</label>
                            </div>
                            <!-- <div class="value">(867)</div>  -->
                          </a>
                        </li>
                        @endforeach
                      </ul>
                    </div>
                  </div>
                </div>

                <div class="filter-box">
                  <a class="filter-text collapsed" data-toggle="collapse" href="#condition-filter" role="button"
                    aria-expanded="false" aria-controls="condition-filter">
                    Condition <span><i class="fa fa-caret-right" aria-hidden="true"></i></span>
                  </a>
                  <div class="collapse" id="condition-filter">
                    <div class="inner-text">
                      <div class="pt-2 pb-1">
                        <div class="custom-control custom-radio custom-control-inline">
                        @if(isset($condition))
                          <input type="radio" id="customRadioInline1" name="customRadioInline1"
                            class="custom-control-input" value="New" {{$condition == 'New' ? "Checked" : ""}}>
                            @else
                            <input type="radio" id="customRadioInline1" name="customRadioInline1"
                            class="custom-control-input" value="New">
                            @endif
                          <label class="custom-control-label" for="customRadioInline1">New</label>
                        </div>
                        <div class="custom-control custom-radio custom-control-inline">
                         @if(isset($condition))
                          <input type="radio" id="customRadioInline2" name="customRadioInline1"
                            class="custom-control-input" value="Used" {{$condition == 'Used' ? "Checked" : ""}}>
                          @else
                          <input type="radio" id="customRadioInline2" name="customRadioInline1"
                            class="custom-control-input" value="Used">
                          @endif  
                          <label class="custom-control-label" for="customRadioInline2">Used</label>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>

                <div class="filter-box">
                  <a class="filter-text collapsed" data-toggle="collapse" href="#movement-filter" role="button"
                    aria-expanded="false" aria-controls="movement-filter">
                    Movement <span><i class="fa fa-caret-right" aria-hidden="true"></i></span>
                  </a>
                  <div class="collapse" id="movement-filter">
                    <div class="inner-text">
                      <ul>
                      @foreach($movements as $movement)
                        <li>
                          <a href="#">
                            <div class="custom-control" id = "multiselectwithsearch">
                            @if(isset($movementSelected))
                              <input type="checkbox" class="" id="{{$movement->movement}}" name="movement-{{$movement->id}}" value="{{$movement->id}}" {{in_array($movement->id,$movementSelected) ? "Checked" : ""}}>
                              @else
                              <input type="checkbox" class="" id="{{$movement->movement}}" name="movement-{{$movement->id}}" value="{{$movement->id}}">
                              @endif
                              <label class="custom-control-label" for="{{$movement->movement}}">{{$movement->movement}}</label>
                            </div>
                            <!-- <div class="value">(867)</div> -->
                          </a>
                        </li>
                        @endforeach
                      </ul>
                    </div>
                  </div>
                </div>

                <div class="filter-box">
                  <a class="filter-text collapsed" data-toggle="collapse" href="#casematerial-filter" role="button"
                    aria-expanded="false" aria-controls="casematerial-filter">
                    Case Material <span><i class="fa fa-caret-right" aria-hidden="true"></i></span>
                  </a>
                  <div class="collapse" id="casematerial-filter">
                    <div class="inner-text">
                      <ul>
                      @foreach($case_materials as $case)
                        <li>
                          <a href="#">
                            <div class="custom-control">
                            @if(isset($caseSelected))
                              <input type="checkbox" class="" id="{{$case->case_material}}" name="case-{{$case->id}}" value="{{$case->id}}" {{in_array($case->id,$caseSelected) ? "Checked" : ""}}>
                              @else
                              <input type="checkbox" class="" id="{{$case->case_material}}" name="case-{{$case->id}}" value="{{$case->id}}">
                              @endif
                              <label class="custom-control-label" for="{{$case->case_material}}">{{$case->case_material}}</label>
                            </div>
                          <!-- <div class="value">(867)</div> -->  
                          </a>
                        </li>
                        @endforeach
                      </ul>
                    </div>
                  </div>
                </div>

                <div class="filter-box">
                  <a class="filter-text collapsed" data-toggle="collapse" href="#bracematerial-filter" role="button"
                    aria-expanded="false" aria-controls="bracematerial-filter">
                    Bracelet Material <span><i class="fa fa-caret-right" aria-hidden="true"></i></span>
                  </a>
                  <div class="collapse" id="bracematerial-filter">
                    <div class="inner-text">
                      <ul>
                      @foreach($bracelet_materials as $bracelet)
                        <li>
                          <a href="#">
                            <div class="custom-control">
                            @if(isset($braceletSelected))
                              <input type="checkbox" class="" id="{{$bracelet->bracelet_material}}" name="bracelet-{{$bracelet->id}}" value="{{$bracelet->id}}" {{in_array($bracelet->id,$braceletSelected) ? "Checked" : ""}}>
                              @else
                              <input type="checkbox" class="" id="{{$bracelet->bracelet_material}}" name="bracelet-{{$bracelet->id}}" value="{{$bracelet->id}}">
                              @endif
                              <label class="custom-control-label" for="{{$bracelet->bracelet_material}}">{{$bracelet->bracelet_material}}</label>
                            </div>
                           <!-- <div class="value">(867)</div> --> 
                          </a>
                        </li>
                        @endforeach
                      </ul>
                    </div>
                  </div>
                </div>

                <div class="filter-box icon">
                  <a class="filter-text collapsed" data-toggle="collapse" href="#gender-filter" role="button"
                    aria-expanded="false" aria-controls="gender-filter">
                    <div class="icon-heading"><i class="fa fa-venus-mars" aria-hidden="true"></i> Gender</div> <span><i
                        class="fa fa-caret-right" aria-hidden="true"></i></span>
                  </a>
                  <div class="collapse" id="gender-filter">
                    <div class="inner-text">
                      <div class="pt-2 pb-1">
                        @foreach($gender as $gend)
                        <div class="custom-control custom-radio">
                        @if(isset($genderSelected))
                          <input type="radio" id="{{$gend->id}}" name="gender1" class="custom-control-input" Value="{{$gend->id}}" {{$genderSelected == $gend->id  ? "Checked" : ""}} >
                          @else
                          <input type="radio" id="{{$gend->id}}" name="gender1" class="custom-control-input" Value="{{$gend->id}}">
                          @endif
                          <label class="custom-control-label" for="{{$gend->id}}">{{$gend->gender}}</label>
                        </div>
                        @endforeach
                      </div>
                    </div>
                  </div>
                </div>

                <div class="filter-box icon">
                  <a class="filter-text" data-toggle="collapse" href="#price-filter" role="button" aria-expanded="false"
                    aria-controls="price-filter">
                    <div class="icon-heading"><i class="fa fa-money" aria-hidden="true"></i> Price</div> <span><i
                        class="fa fa-caret-right" aria-hidden="true"></i></span>
                  </a>
                  <div class="collapse show" id="price-filter">
                    <div class="inner-text">
                      <div class="values">
                        <!-- <div class="min-price"></div>
                        <p class="max-price mt-3"></p> -->
                        <div id="slider-range" class="price-filter-range" name="rangeInput"></div>
                        <div
                          style="margin: 20px auto 5px;display: flex;justify-content: space-between;align-items: center;text-align: center;">
                          <div class="div">
                            <label style="line-height: 1;" class="d-block" for="min_price">Min</label>
                            <input type="number" min=0 max={{$price_max-10}} oninput="validity.valid||(value='0');" id="min_price" name="minPrice"
                              class="price-range-field" />
                          </div>
                          <div class="div">
                            <label style="line-height: 1;" class="d-block" for="max_price">Max</label>
                            <input type="number" min=0 max={{$price_max}} oninput="validity.valid||(value='1000000');" name="maxPrice"
                              id="max_price" class="price-range-field" />
                          </div>

                        </div>
                      </div>
                    </div>
                  </div>
                </div>

                <div class="filter-box icon">
                  <a class="filter-text collapsed" data-toggle="collapse" href="#year-filter" role="button"
                    aria-expanded="false" aria-controls="year-filter">
                    <div class="icon-heading"><i class="fa fa-calendar-check-o" aria-hidden="true"></i> Year</div>
                    <span><i class="fa fa-caret-right" aria-hidden="true"></i></span>
                  </a>
                  <div class="collapse" id="year-filter">
                    <div class="inner-text">
                      <div class="form-row mb-2 mt-2">
                        <label for="yearfrom">Year From :</label>
                        <select class="form-control custom-select" required id="yearfrom" name="fromYear">
                          <option selected disabled>Select Here</option>
						@foreach($years as $year)	
						@if(isset($yearMin))						
                          <option value="{{$year->year_of_manufacture}}" {{$yearMin == $year->year_of_manufacture ? "Selected" : ""}}>{{$year->year_of_manufacture}}</option>
                          @else
                          <option value="{{$year->year_of_manufacture}}">{{$year->year_of_manufacture}}</option>
                          @endif
                          @endforeach
                        </select>
                      </div>
                      <div class="form-row mb-2">
                        <label for="yearto">Year to :</label>
                        <select class="form-control custom-select" required id="yearto" name="toYear">
                          <option selected disabled>Select Here</option>
                          @foreach($years as $year)	
                          @if(isset($yearMax))
                          <option value="{{$year->year_of_manufacture}}" {{$yearMax == $year->year_of_manufacture ? "Selected" : ""}}>{{$year->year_of_manufacture}}</option>
                          @else
                          <option value="{{$year->year_of_manufacture}}">{{$year->year_of_manufacture}}</option>
                          @endif
                          @endforeach
                        </select>
                      </div>
                    </div>
                  </div>
                </div>

                <div class="filter-box icon">
                  <a class="filter-text collapsed" data-toggle="collapse" href="#dcolor-filter" role="button"
                    aria-expanded="false" aria-controls="dcolor-filter">
                    <div class="icon-heading"><i class="fa fa-tachometer" aria-hidden="true"></i> Dial Colour</div>
                    <span><i class="fa fa-caret-right" aria-hidden="true"></i></span>
                  </a>
                  <div class="collapse" id="dcolor-filter">
                                        <div class="inner-text">
                                            <div class="input-group mb-3">
                                                <select class="form-control custom-select mt-3" id="color" name="color">
                                                  <option value="" disabled selected>Select a color...</option>
												  @foreach($colors as $colour)
												  @if(isset($colorSelected))
                                                  <option value="{{$colour->id}}" {{$colorSelected == $colour->id ? "Selected" : ""}}>{{$colour->dial_color}}</option>
                                                  @else
                                                  <option value="{{$colour->id}}" >{{$colour->dial_color}}</option>
                                                  @endif
                                                  @endforeach
                                                </select>
                                              </div>
                                        </div>
                                    </div>
                </div>

                <div class="filter-box icon">
                  <a class="filter-text collapsed" data-toggle="collapse" href="#country-filter" role="button"
                    aria-expanded="false" aria-controls="country-filter">
                    <div class="icon-heading"><i class="fa fa-globe" aria-hidden="true"></i> Country</div>
                    <span><i class="fa fa-caret-right" aria-hidden="true"></i></span>
                  </a>
                  <div class="collapse" id="country-filter">
                    <div class="inner-text">
                      <ul>
                      @foreach($country_data as $country)
                        <li>
                          <a href="#">
                            <div class="custom-control">
                            @if(isset($countrySelected))
                              <input type="checkbox" class="" id="{{$country-> country_name}}" name="country-{{$country-> country_name}}" value="{{$country-> id}}" {{in_array($country-> id,$countrySelected) ? "Checked" : ""}}>
                              @else
                              <input type="checkbox" class="" id="{{$country-> country_name}}" name="country-{{$country-> country_name}}" value="{{$country-> id}}">
                              @endif
                              <label class="custom-control-label" for="{{$country-> country_name}}">{{$country-> country_name}}</label>
                            </div>
                           <!-- <div class="value">(867)</div> --> 
                          </a>
                        </li>
                        @endforeach
                      </ul>
                    </div>
                  </div>
                </div>
				<button type= "submit" id= "searchButton" class="site-btn">Search</button>
              </div>
            </div>
            </form>
          </div>
        </div>

        <div class="col-lg-9 mb-3 order-1 order-lg-2">
          <div class="main-heading">
            <h3>Luxury Watches for Sale Worldwide</h3>
            <p>Classic and contemporary design</p>
          </div>
          <div class="sorting-panel">
                        <form action="#" class="needs-validation form-inline justify-content-end mb-3" novalidate>
                            <label for="sortby" class="mr-2">Sort by</label>
                            <select id="sortBy" class="form-control custom-select " name="sortBy">
                                @if(!isset($sortBy))
                                <option value="0" selected>Popularity</option>
                                @else
                                <option value="0">Popularity</option>
                                @endif
                                @if(isset($sortBy) && $sortBy == "1")
                                <option value="1" selected>High to low</option>
                                @else
                                <option value="1">High to low</option>
                                @endif
                                @if(isset($sortBy) && $sortBy == "2")
                                <option value="2" selected>Low to high</option>
                                @else
                                <option value="2">Low to high</option>
                                @endif
                                @if(isset($sortBy) && $sortBy == "3")
                                <option value="3" selected>Recent</option>
                                @else
                                <option value="3">Recent</option>
                                @endif
                            </select>
                        </form>
                    </div>
          <div class="row" id= "listing">
          @if($watchListing)
			@foreach ($watchListing as $data)
            <div class="col-sm-3">
              <div class="watch-product-item">
                <div class="product-thumb">
                  <a href="{{route('watchlisting.show',$data->id)}}"><img src="{{url('uploads/'.$images->firstWhere('listing_id',$data->id)->filename)}}" alt=""></a>
                  <div class="d-none">
                    <ul>
                      <li><a href="#"><i class="fas fa-search-plus"></i></a></li>
                      <li><a href="#"><i class="far fa-heart"></i></a></li>
                      <li><a href="#"><i class="fas fa-plus"></i></a></li>
                    </ul>
                  </div>
                  <div class="inside">
                    <div class="contents">
                      <table class="w-100">
                        <tr>
                          <td>Model</td>
                          <td><span>{{$data->model_name}}</span></td>
                        </tr>
                        <tr>
                          <td>Year</td>
                          <td><span>{{$data->year_of_manufacture}}</span></td>
                        </tr>
                        <tr>
                          <td>Make</td>
                          <td><span>{{$data->brand_name}}</span></td>
                        </tr>
                        <tr>
                          <td>Case Diameter</td>
                          <td><span>{{$data->case_diameter}}</span></td>
                        </tr>
                        <tr>
                          <td>Location</td>
                          <td><span>{{$data->city_name}}, {{$data->state_name}}</span></td>
                        </tr>
                      </table>
                    </div>
                  </div>
                </div>
                <div class="product-details">
                  <h5><a href="#">{{$data->ad_title}}</a></h5>
                  <div class="price-wrap">
                    <span class="Price-currency">{{currency()->convert(floatval($data->watch_price), $data->currency_code, currency()->getUserCurrency())}}</span>
                  </div>
                  <div class="bottom-detais">
                    <div class="dealer">
                      <ul>
                        <li><i class="fa fa-star"></i></li>
                        <li><i class="fa fa-star"></i></li>
                        <li><i class="fa fa-star"></i></li>
                        <li><i class="fa fa-star"></i></li>
                        <li><i class="fa fa-star"></i></li>
                      </ul>
                      <p class="seller-type"><i class="fa fa-check-square-o" aria-hidden="true"></i> {{ $data->agency_id === 0 ? "Private Seller" : "Dealer" }}</p>
                    </div>
                    <div class="country-flag">
                      <img src="{{url('uploads/'.$data->filename)}}" alt="">
                      <p class="c-code">{{$data->countrycode}}</p>
                    </div>
                  </div>
                </div>

                <a href="javascript:void(0);" title="click here" class="icon"><i class="fa fa-info-circle"
                    aria-hidden="true"></i></a>

              </div>
            </div>
			@endforeach
			@else
			<div class="col-sm-4">
                <div class="watch-product-item">
					<div class="product-thumb">
						<p class = "home-view">No Data Found for this search</p>
						<img src="{{url('/assets/images/NoImageFound.png')}}" alt="" class="img-fluid">
						</div>
						</div>
						</div>
                @endif



          </div>
          @if($watchListing->hasPages())
          <div class="custom-pagination mt-4 d-flex justify-content-end">
            <nav aria-label="Page navigation example">
            <!--  {!! $watchListing->withQueryString()->links() !!} --> 	
                <ul class="pagination">
              @if ($watchListing->onFirstPage())
            <li class="page-item disabled"><a class="page-link" href="#">Previous</a></li>
        @else
        	<li class="page-item"><a class="page-link" href="{{ $watchListing->withQueryString()->previousPageUrl() }}">Previous</a></li>
        @endif
        
        @for($i = 1 ; $i <= $watchListing->lastPage() ; $i++)
        	@if($watchListing->currentPage() == $i)
			<li class="page-item active"><a class="page-link" href="{{$watchListing->withQueryString()->url($i)}}">{{$i}}</a></li>
			@else
			<li class="page-item"><a class="page-link" href="{{$watchListing->withQueryString()->url($i)}}">{{$i}}</a></li>
			@endif
        @endfor        
          @if ($watchListing->hasMorePages())
            <li class="page-item"><a class="page-link" href="{{ $watchListing->withQueryString()->nextPageUrl() }}">Next</a></li>
        @else
            <li class="page-item disabled"><a class="page-link" href="#">Next</a></li>
        @endif    
              </ul>
            </nav>
          </div>
          @endif
        </div>
      </div>
    </div>
  </div>
  <footer>
    <div class="custom-top-footer pt-5 pb-5">
      <div class="container">
        <div class="row">
          <div class="col-lg-5 mr-auto col-md-4 mb-4">
            <div class="custom-footer-logo">
              <a href="/"><img src="/assets/img/logo-header.png" class="img-fluid"></a>
              <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the
                industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and
                scrambled it to make
                a type specimen book.</p>
            </div>
          </div>
          <div class="col-lg-3 col-md-4 mb-4">
            <div class="custom-footer-links footer-social">
              <h3>Contact Us</h3>
              <ul>
                <li>
                  <i class="fas fa-map-marker-alt"></i> {{$contactUs->address}}
                </li>
                <li>
                  Call us FREE<a href="#"><i class="fas fa-mobile-alt"></i> {{$contactUs->mobile}}</a>
                </li>
                <li>
                  <a href="mailto:email@email.com"><i class="far fa-envelope"></i> {{$contactUs->email}}</a>
                </li>
              </ul>
            </div>
          </div>
          <div class="col-lg-3 col-md-4">
            <div class="custom-footer-links footer-nav">
              <h3>Important Links</h3>
              <ul>
                @if($propertyType->firstWhere('property_type', 'Residential'))
                                <li>
                                    <a href="{{url('/propertylisting/search?home-Residential='. $propertyType->firstWhere('property_type', 'Residential')->id)}}">Residential</a>
                                </li>
                                @endif
                                @if($propertyType->firstWhere('property_type', 'Apartment'))
                                <li>
                                    <a href="{{url('/propertylisting/search?home-Apartment='. $propertyType->firstWhere('property_type', 'Apartment')->id)}}">Apartment</a>
                                </li>
                                @endif
                                @if($propertyType->firstWhere('property_type', 'Single Family Home'))
                                <li>
                                    <a href="{{url('/propertylisting/search?home-Single Family Home='. $propertyType->firstWhere('property_type', 'Single Family Home')->id)}}">Single Family Home</a>
                                </li>
                                @endif
                                @if($propertyType->firstWhere('property_type', 'Villa'))
                                <li>
                                    <a href="{{url('/propertylisting/search?home-Villa='. $propertyType->firstWhere('property_type', 'Villa')->id)}}">Villa</a>
                                </li>
                                @endif
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="custom-bottom-footer pt-3 pb-3">
      <div class="container">
        <div class="row">
          <div class="col-md-6 col-sm-6 col-12">
            <div class="copyright-wrapper">
              <p>© Tangible - All rights reserved</p>
            </div>
          </div>
          <div class="col-md-6 col-sm-6 col-12">
            <div class="footer-social-links">
              <ul>
                <li><a class="hvr-buzz-out" href="https://www.facebook.com/tangiblelistings"><i class="fab fa-facebook-f"></i></a></li>
                <li><a class="hvr-buzz-out" href="https://www.instagram.com/tangiblelistings/"><i class="fab fa-instagram"></i></a></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  </footer>



  <script src="/assets/js/jquery.js"></script>
  <script src="/assets/js/bootstrap.min.js"></script>
  <script src="/assets/js/wow.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/wow/1.1.2/wow.min.js"></script>
  <script src="/assets/js/jquery-ui.min.js"></script>
  <script src="/assets/js/local.js"></script>
</body>

<!------------------------------------------ JS SCRIPT SECTION ------------------------------------------------>

<script type="text/javascript">



$(document).ready(function(){ 



	$("#sortBy").change(function(){
		var urlSortBy = window.location.href;
		if(urlSortBy.includes("/watchlisting?")){
			 finalUrl = urlSortBy + '&sortBy=' +$(this).val();
			}else  if(urlSortBy.includes("/watchlisting")){
			 finalUrl = urlSortBy + '/search?sortBy=' +$(this).val();
		}
		if(urlSortBy.includes("search")){
			if(urlSortBy.includes("sortBy")){
				finalUrl = urlSortBy.replace(/sortBy=\d/,'sortBy=' + +$(this).val());
				}else{
			finalUrl = window.location.href + '&sortBy=' + +$(this).val();
				}
			}
		window.location.assign(finalUrl);
	});
	

	
	$(function () {
	    
        $("#slider-range").slider({
            range: true,
            orientation: "horizontal",
            min: 0,
            max: {{$price_max}},
            values: [0, {{$price_max}}],
            step: 100,

            slide: function (event, ui) {
                if (ui.values[0] == ui.values[1]) {
                    return false;
                }

               $("#min_price").val(ui.values[0]);
                $("#max_price").val(ui.values[1]);
            }
        });

        $("#min_price").val($("#slider-range").slider("values", 0));
        $("#max_price").val($("#slider-range").slider("values", 1));

    });

	$('#clear').click(function (e) {
	  $('#filter-form').find(':input').each(function() {
	    if(this.type == 'submit'){
	          //do nothing
	      }
	      else if(this.type == 'checkbox' || this.type == 'radio') {
	        this.checked = false;
	      }
	   })
	   $('#yearfrom')[0].selectedIndex = 0;
	  $('#yearto')[0].selectedIndex = 0;

	  $(function () {
	        $("#slider-range").slider({
	            range: true,
	            orientation: "horizontal",
	            min: 0,
	            max: {{$price_max}},
	            values: [0, {{$price_max}}],
	            step: 100,

	            slide: function (event, ui) {
	                if (ui.values[0] == ui.values[1]) {
	                    return false;
	                }

	               $("#min_price").val(ui.values[0]);
	                $("#max_price").val(ui.values[1]);
	            }
	        });

	        $("#min_price").val($("#slider-range").slider("values", 0));
	        $("#max_price").val($("#slider-range").slider("values", 1));

	    });
	});
	
	$('#brand').click(function (e) {
		if(e.target.tagName == "INPUT"){
		var checked = []
		var type = "brand"
		$("input[type='checkbox']:checked").each(function ()
		{
			checked.push($(this).val());
		});

		console.log('checked branch',checked);

			$.ajax({
	            url:"/watchlisting/brand",
	            type: 'post',
	            dataType: "json",
	            data: {
	               _token: '{{csrf_token()}}',
	               brandfilter: checked,
	               type: type
	            },
	            success: function( data ) {
					var listingHtml= '';
					console.log(data['model']);

					/* if(data['brandwatchListing'] != null){
						var listing = data['brandwatchListing'];
					for(var i=0; i < listing.length; i++){
						console.log('inside loop ',listing[i]);	
						var id = listing[i].id;
						listingHtml +=`<div class="col-sm-4">
			                   <div class="watch-product-item">
			                   <div class="product-thumb">
			                     <a href="#"><img src="{{url('uploads/')}}${'/' + listing[i].imagename}" alt=""></a>
			                     <div class="d-none">
			                       <ul>
			                         <li><a href="#"><i class="fas fa-search-plus"></i></a></li>
			                         <li><a href="#"><i class="far fa-heart"></i></a></li>
			                         <li><a href="#"><i class="fas fa-plus"></i></a></li>
			                       </ul>
			                     </div>
			                     <div class="inside">
			                       <div class="contents">
			                         <table class="w-100">
			                           <tr>
			                             <td>Model</td>
			                             <td><span>${listing[i].model_name}</span></td>
			                           </tr>
			                           <tr>
			                             <td>Year</td>
			                             <td><span>${listing[i].year_of_manufacture}</span></td>
			                           </tr>
			                           <tr>
			                             <td>Make</td>
			                             <td><span>${listing[i].brand_name}</span></td>
			                           </tr>
			                           <tr>
			                             <td>Case Diameter</td>
			                             <td><span>${listing[i].case_diameter}</span></td>
			                           </tr>
			                           <tr>
			                             <td>Location</td>
			                             <td><span>${listing[i].city_name}, ${listing[i].state_name}</span></td>
			                           </tr>
			                         </table>
			                       </div>
			                     </div>
			                   </div>
			                   <div class="product-details">
			                     <h5><a href="#">${listing[i].ad_title}</a></h5>
			                     <div class="price-wrap">
			                       <span class="Price-currency">${listing[i].watch_price === null ? "" : listing[i].currency_code}</span>${listing[i].watch_price === null ? "" : listing[i].watch_price}</span>
			                     </div>
			                     <div class="bottom-detais">
			                       <div class="dealer">
			                         <ul>
			                           <li><i class="fa fa-star"></i></li>
			                           <li><i class="fa fa-star"></i></li>
			                           <li><i class="fa fa-star"></i></li>
			                           <li><i class="fa fa-star"></i></li>
			                           <li><i class="fa fa-star"></i></li>
			                         </ul>
			                         <p class="seller-type"><i class="fa fa-check-square-o" aria-hidden="true"></i> ${listing[i].agency_id === 0 ? "Private Seller" : "Dealer" }</p>
			                       </div>
			                       <div class="country-flag">
			                         <img src="{{url('uploads/')}}${'/' + listing[i].filename}" alt="">
			                         <p class="c-code">${listing[i].countrycode}</p>
			                       </div>
			                     </div>
			                   </div>

			                   <a href="javascript:void(0);" title="click here" class="icon"><i class="fa fa-info-circle"
			                       aria-hidden="true"></i></a>

			                 </div>
			               </div>`;
						}

					$('#listing').html(listingHtml);	
					} */

					if(data['model'] != null){
					var modelHtml = "";
					var modelData =data['model'];  
					for(var i=0; i < modelData.length; i++){
						modelHtml += `<li>
	                          <a href="#">
	                            <div class="custom-control">
	                              <input type="checkbox" class="" id="${modelData[i].watch_model_name}" name="model-${modelData[i].id}" value="${modelData[i].id}">
	                              <label class="custom-control-label" for="${modelData[i].watch_model_name}">${modelData[i].watch_model_name}</label>
	                            </div>
	                            <!-- <div class="value">(867)</div>  -->
	                          </a>
	                        </li>`;	
						}
					$('#model').html(modelHtml);
						}	               
	            }
	          });
			
		}
});

});	
</script>
</html>
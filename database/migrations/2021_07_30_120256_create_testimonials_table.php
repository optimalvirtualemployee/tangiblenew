<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTestimonialsTable extends Migration
{
    public function up()
    {
        Schema::create('wa_testimonials', function (Blueprint $table) {
        $table->increments('id');
        $table->string('name',100)->nullable()->default('NULL');
        $table->string('image',100)->nullable()->default('NULL');
        $table->string('destination',100)->nullable()->default('NULL');
        $table->text('message');
        $table->enum('status',['1','0'])->nullable()->default('NULL');
        $table->enum('image_status',['0','1'])->nullable()->default('NULL');
        $table->timestamp('created_at')->default('CURRENT_TIMESTAMP');
        $table->timestamp('updated_at')->default('CURRENT_TIMESTAMP');
        $table->primary('id');

        });
    }

    public function down()
    {
        Schema::dropIfExists('wa_testimonials');
    }
}
<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/* Route::get('/', function () {
    return view('welcome');
}); */

Route::resource('/','LandingController');

/* Route::get('/admin', function () {
    return view('auth.login');
});

Route::get('logout', function(){
   Auth::logout();
   return Redirect::to('/admin');
}); */

Auth::routes();

Route::resource('/tangiblerealestate','TangibleListingRealEstate');
Route::resource('/tangiblewatches','TangibleListingWatches');
Route::resource('/tangibleautomobiles','TangibleListingAutomobiles');
Route::get('/carlisting/suggestprice/{id}','CarListingFrontEndController@suggestPrice');
Route::post('/automobile/submitsuggestprice','CarListingFrontEndController@submitSuggestPrice');
Route::get('/carlisting/search','CarListingFrontEndController@searchFilter');
Route::post('/carlisting/getYear','CarListingFrontEndController@findYear');
Route::resource('/carlisting','CarListingFrontEndController');
Route::post('/carlisting/brand','CarListingFrontEndController@search');

Route::get('/watchlisting/checkout/{id}','WatchListingFrontEndController@checkout');
Route::post('/watch/processcheckout','WatchListingFrontEndController@processCheckout');


Route::get('/watchlisting/suggestprice/{id}','WatchListingFrontEndController@suggestPrice');
Route::post('/watch/submitsuggestprice','WatchListingFrontEndController@submitSuggestPrice');
Route::get('/watchlisting/search','WatchListingFrontEndController@searchFilter');
Route::post('/watchlisting/getYear','WatchListingFrontEndController@findYear');
Route::post('/watch/enquery','WatchListingFrontEndController@sendenquery');
Route::post('/car/enquery','CarListingFrontEndController@sendenquery');
Route::post('/RealEstate/enquery','TangibleListingRealEstate@sendenquery');
Route::resource('/watchblog','SingleBlogFrontEndController');
Route::resource('/carblog','CarBlogFrontEndController');
Route::resource('/propertyblog','PropertyBlogFrontEndController');
Route::resource('/becomedealer','DealerCreateController');
Route::post('/becomedealer/details','DealerCreateController@sendDealerInfo');

Route::resource('/watchlisting','WatchListingFrontEndController');
Route::post('/watchlisting/brand','WatchListingFrontEndController@search');
Route::get('/propertylisting/search','PropertyListingFrontEndController@searchFilter');
Route::resource('/propertylisting','PropertyListingFrontEndController');
Route::get('/tangiblerealestate/suggestprice/{id}','PropertyListingFrontEndController@suggestPrice');
Route::post('/property/submitsuggestprice','PropertyListingFrontEndController@submitSuggestPrice');
Route::post('/propertylisting/autosearch','PropertyListingFrontEndController@autocompleteSearch');
//Route::post('/tangiblerealestate/search','TangibleListingRealEstate@search');
Route::post('/tangiblerealestate/autosearch','TangibleListingRealEstate@autocompleteSearch');
Route::get('/tangiblerealestatesearch/search','TangibleListingRealEstate@search');

Route::resource('/privatesellerselectpackage','PrivateSellerSelectPackageController');
Route::resource('/privatesellerselectwatchpackage','PrivateSellerSelectPackageWatchController');
Route::resource('/privatesellercategoryselect','PrivateSellerCategorySelectController');
Route::post('/getCarModel','PrivateSellerCarAdd@getCarModel');
Route::post('/getCarBrandName','PrivateSellerCarAdd@getCarBrandName');
Route::post('/getCarModelName','PrivateSellerCarAdd@getCarModelName');
Route::get('/privatesellercaradd/{package}/{pricerange}/{price}','PrivateSellerCarAdd@price');
Route::resource('/privatesellercaradd','PrivateSellerCarAdd');
Route::post('/getWatchModel','PrivateSellerWatchAdd@getWatchModel');

// Route::get('/privatesellerwatchadd/{package}/{pricerange}/{price}','PrivateSellerWatchAdd@price');
Route::get('/privatesellerwatchadd/{package_id}','PrivateSellerWatchAdd@price');
Route::post('/privateSellerWatchPayments', 'PaymentsController@privateSellerWatchPaymentStore');


Route::resource('/privatesellerwatchadd','PrivateSellerWatchAdd');
Route::post('/getState', 'PrivateSellerCarAdd@getStateByCountry');
Route::post('/getCity', 'PrivateSellerCarAdd@getCityByState');

Route::get('/customers/pdf/{productid}/{agentid}/{userid}','PdfController@index');
Route::post('/additional_custom_duties','PdfController@customDuties');

Route::prefix('admin')->group(function(){
    Route::get('/login', 'Auth\LoginController@showLoginForm')->name('admin.login');
    Route::get('/', 'Auth\LoginController@showLoginForm')->name('admin.login');
    Route::post('/logout', 'Auth\LoginController@logout')->name('admin.logout');
});

//Testimonial Landing Page View link in footer
Route::get('testimonial_page','TestimonialLandingPageController@index');

//Route::get('/admin/home', 'HomeController@index')->name('home');
Route::group(['middleware'=>['auth','admin']], function (){
    
Route::get('/admin/home', 'HomeController@index')->name('home');
Route::get('/admin/watch/dashboard', 'WatchadminController@home')->name('dashboard');
Route::get('/admin/automobile/dashboard', 'AutomobileadminController@home')->name('automobiledashboard');
Route::get('/admin/realestate/dashboard', 'RealestateadminController@home')->name('realestatedashboard');
Route::get('/admin/admincommon/dashboard', 'CommonadminController@home')->name('commonadmindashboard');
Route::get('/admin/watch/displaybrands', 'WatchadminController@displaybrands')->name('displaybrands');
Route::get('/admin/watch/createbrands', 'WatchadminController@createbrands')->name('createbrands');
Route::get('/admin/watch/updatebrands/{id}', 'WatchadminController@updatebrands')->name('updatebrands');
Route::post('/admin/watch/savebrands', 'WatchadminController@savebrands')->name('savebrands');
Route::post('/admin/watch/editbrands', 'WatchadminController@editbrands')->name('editbrands');
Route::post('/admin/watch/deletebrands', 'WatchadminController@deletebrands')->name('deletebrands');
Route::get('/admin/watch/displaybrandsmodel', 'WatchadminController@displaybrandsmodel')->name('displaybrandsmodel');
Route::get('/admin/watch/createbrandsmodel', 'WatchadminController@createbrandsmodel')->name('createbrandsmodel');
Route::post('/admin/watch/savebrandsmodel', 'WatchadminController@savebrandsmodel')->name('savebrandsmodel');
Route::get('/admin/watch/updatebrandsmodel/{id}', 'WatchadminController@updatebrandsmodel')->name('updatebrandsmodel');
Route::post('/admin/watch/editbrandsmodel', 'WatchadminController@editbrandsmodel')->name('editbrandsmodel');
Route::post('/admin/watch/deletebrandsmodel', 'WatchadminController@deletebrandsmodel')->name('deletebrandsmodel');

//------------------------------------------CAR BRANDS & MODEL START HERE --------------------------------------//
//geting car brands
Route::get('/admin/automobile/displayautomobilebrands', 'AutomobileadminController@displayautomobilebrands')->name('displayautomobilebrands');

// Crud Operation on Car Brand------
Route::get('/admin/automobile/create_car_brands','AutomobileadminController@create_car_brands')->name('create_car_brands');
Route::post('/admin/automobile/savebrands', 'AutomobileadminController@savebrands')->name('savecarbrands');
Route::get('/admin/automobile/updatebrands/{id}', 'AutomobileadminController@updatebrands')->name('updatecarbrands');
Route::post('/admin/automobile/editbrands', 'AutomobileadminController@editbrands')->name('editcarbrands');
Route::post('/admin/automobile/deletebrands', 'AutomobileadminController@deletebrands')->name('deletecarbrands');
Route::get('/admin/automobile/deletebrandsimages/{id}', 'AutomobileadminController@deletebrandsimages')->name('deletecarbrandsimages');
// Crud Operation on Car Brand------

// Crud Operation on Car Models------
Route::get('/admin/automobile/displaybrandsmodel', 'AutomobileadminController@displaybrandsmodel')
	   ->name('displaycarbrandsmodel');
Route::get('/admin/automobile/createbrandsmodel', 'AutomobileadminController@createbrandsmodel')
	   ->name('createcarbrandsmodel');
Route::post('/admin/automobile/savebrandsmodel', 'AutomobileadminController@savebrandsmodel')
       ->name('savecarbrandsmodel');
Route::get('/admin/automobile/updatebrandsmodel/{id}', 'AutomobileadminController@updatebrandsmodel')
       ->name('updatecarbrandsmodel');
Route::post('/admin/automobile/editbrandsmodel', 'AutomobileadminController@editbrandsmodel')
       ->name('editcarbrandsmodel');
Route::post('/admin/automobile/deletebrandsmodel', 'AutomobileadminController@deletebrandsmodel')
       ->name('deletecarbrandsmodel');
// Crud Operation on Car Models------
//------------------------------------------CAR BRANDS & MODEL ENDS HERE ---------------------------------------//


//------------------------------------------COUNTRIES STARTS HERE ---------------------------------------------//
Route::get('/admin/admincommon/displaycountries', 'CommonadminController@displaycountries')->name('displaycountries');
Route::get('/admin/admincommon/createcountries', 'CommonadminController@createcountries')->name('createcountries');
Route::post('/admin/admincommon/savecountries', 'CommonadminController@savecountries')->name('savecountries');
Route::get('/admin/admincommon/updatecountries/{id}', 'CommonadminController@updatecountries')->name('updatecountries');
Route::post('/admin/admincommon/editcountries', 'CommonadminController@editcountries')->name('editcountries');
Route::post('/admin/admincommon/deletecountries', 'CommonadminController@deletecountries')->name('deletecountries');
//------------------------------------------COUNTRIES ENDS HERE ----------------------------------------------//

//------------------------------------------STATE STARTS HERE ---------------------------------------------//
Route::get('/admin/admincommon/displaystates', 'CommonadminController@displaystates')->name('displaystates');
Route::get('/admin/admincommon/createstates', 'CommonadminController@createstates')->name('createstates');
Route::post('/admin/admincommon/savestates', 'CommonadminController@savestates')->name('savestates');
Route::get('/admin/admincommon/updatestates/{id}', 'CommonadminController@updatestates')->name('updatestates');
Route::post('/admin/admincommon/editstates', 'CommonadminController@editstates')->name('editstates');
Route::post('/admin/admincommon/deletestates', 'CommonadminController@deletestates')->name('deletestates');  
//------------------------------------------STATE ENDS HERE -----------------------------------------------//

//------------------------------------------CITY STARTS HERE ---------------------------------------------//
Route::get('/admin/admincommon/displaycity', 'CommonadminController@displaycity')->name('displaycity');
Route::get('/admin/admincommon/createcity', 'CommonadminController@createcity')->name('createcity');
Route::post('/admin/admincommon/savecity', 'CommonadminController@savecity')->name('savecity');
Route::get('/admin/admincommon/updatecity/{id}', 'CommonadminController@updatecity')->name('updatecity');
Route::post('/admin/admincommon/editcity', 'CommonadminController@editcity')->name('editcity');
Route::post('/admin/admincommon/deletecity', 'CommonadminController@deletecity')->name('deletecity');
Route::post('/admin/admincommon/getState', 'CommonadminController@getStateByCountry');
Route::post('/admin/admincommon/getCity', 'CommonadminController@getCityByState');
//------------------------------------------CITY ENDS HERE -----------------------------------------------//


Route::group(['prefix'=> 'admin/'], function (){
//------------------------------------------ FACTORY SETTINGS STARTS HERE -------------------------------//
	//for DialPowerReserve---
	Route::resource('dialPowerReserve','DialPowerReserveController');
	//for DialColor---
	Route::resource('dialColor','DialColorController');
	//for Bracelet---
	//Route::resource('bracelet','BraceletController');
	Route::resource('braceletMaterial','BraceletMaterialController');
	Route::resource('braceletColor','BraceletColorController');
	Route::resource('braceletClaspType','BraceletClaspTypeController');
	Route::resource('braceletClaspMaterial','BraceletClaspMaterialController');
	//for Case---
	//Route::resource('case','CaseController');
	Route::resource('caseMaterial','CaseMaterialController');
	Route::resource('caseMM','CaseMMController');
	Route::resource('caseWRD','CaseWaterRDController');
	Route::resource('caseGlassType','CaseGlassTypeController');
	//for Functions---
	//Route::resource('functions','FunctionsController');
	Route::resource('functionChronograph','FunctionChronographController');
	Route::resource('functionTourbillion','FunctionTourbillionController');
	Route::resource('functionGMT','FunctionGMTController');
	Route::resource('functionAnnualCalender','FunctionAnnualCalenderController');
	Route::resource('functionMinuteRepeater','FunctionMinuteRepeaterController');
	Route::resource('functionDoubleChronograph','FunctionDoubleChronographController');
	Route::resource('functionPanormaDate','FunctionPanormaDateController');
	Route::resource('functionJumpingHour','FunctionJumpingHourController');
	Route::resource('functionYear','FunctionYearController');
	Route::resource('functionDay','FunctionDayController');
	Route::resource('functionAlarm','FunctionAlarmController');
	
	
//------------------------------------------ FACTORY SETTINGS ENDS HERE -------------------------------//

//------------------------------------------ WATCH MASTER STARTS HERE ---------------------------------//
	//for Gender---
	Route::resource('gender','GenderController');
	//for Type---
	Route::resource('type','TypeController');
	Route::post('type/update/{id}','TypeController@update');
	//for Movement---
	Route::resource('movement','MovementController');
	//for Case Diameter---
	Route::resource('case_diameter','CaseDiameterController');
	//for Price---
	Route::resource('price','PriceController');
	//for bezel
	Route::resource('bezel','WatchBezelController');
	
//------------------------------------------ WATCH MASTER ENDS HERE -----------------------------------//

//------------------------------------------ AUTOMOBILE MASTER STARTS HERE ---------------------------------//
	//for Fuel Type---
	Route::resource('fuel_type','FuelTypeController');
	//for Colour---
	Route::resource('colour','ColourController');
	//for No of Doors---
	Route::resource('no_of_doors','NoOfDoorsController');
	//for Interior Colour---
	Route::resource('interior_colour','InteriorColourController');
	//for No of Seats---
	Route::resource('no_of_seats','NoOfSeatsController');
	//for Body Type
	Route::resource('body_type','AutomobileBodyTypeController');
	Route::get('body_typeDeleteImage/{id}','AutomobileBodyTypeController@imageDestroy');
	//for Induction
	Route::resource('induction','AutomobileInductionController');
	//for Drive Type
	Route::resource('drive_type','AutomobileDriveTypeController');
	//for Drive Type
	Route::resource('cylinders','AutomobileCylindersController');
	
//------------------------------------------ AUTOMOBILE MASTER ENDS HERE -----------------------------------//

//------------------------------------------ Residential MASTER STARTS HERE ---------------------------------//
    //for Top Property Type---
	//Route::resource('top_property_type','TopPropertyTypeController');
	Route::post('top_property_type/getSubType', 'TopPropertyTypeController@getSubPropertyType');
	//for Property Type---
	Route::resource('property_type','PropertyTypeController');
	//for No of Bedrooms---
	Route::resource('no_of_bedrooms','NoOfBedroomsController');
	//for No of Bathrooms---
	Route::resource('no_of_bathrooms','NoOfBathroomsController');
	//for No of Balconies---
	Route::resource('no_of_parking','NoOfParkingController');
	//for No of Balconies---
	Route::resource('propertyfeaturelisting','PropertyFeaturesListingController');
	
	//------------------------------------------ Residential MASTER ENDS HERE -----------------------------------//

	//------------------------------------------ Commercial MASTER STARTS HERE ---------------------------------//
	//for Top Property Type---
	//Route::post('commercial/getSubType', 'TopPropertyCommercialController@getSubPropertyType');
	//for Property Type---
	Route::resource('commercial_property_type','PropertyCommercialTypeController');
	//for No of Balconies---
	Route::resource('commercial_propertyfeature','PropertyFeaturesCommercialListingController');
	
	//------------------------------------------ Commercial MASTER ENDS HERE -----------------------------------//

//----------------------- CONDITIONS, INCLUSION and Yr OF MANUFACTURE MENU STARTS HERE ---------------------//
	
	//for Year of Manufacture---
	Route::resource('year_of_manufacture','YearOfManufactureController');
	//for Conditions---
	Route::resource('conditions','ConditionsController');
	//for Inclusion---
	Route::resource('inclusion','InclusionController');
	
//----------------------- CONDITIONS, INCLUSION and Yr OF MANUFACTURE MENU ENDS HERE ---------------------//


//----------------------- BUILD YEAR UNDER WATCH MASTER MENU STARTS HERE ---------------------//
	
	//for Built Year---
	Route::resource('build_year','BuildYearController');

//----------------------- BUILD YEAR UNDER WATCH MASTER MENU ENDS HERE -----------------------//


//----------------------- AUCTION MASTER MENU STARTS HERE ---------------------//
	
	//for Enquiry Type---
	Route::resource('enquiry_type','EnquiryTypeController');
	//for Purchaser Description---
	Route::resource('purchaser_description','PurchaserDescriptionController');

//----------------------- AUCTION MASTER MENU ENDS HERE -----------------------//


//----------------------- PACKAGE & PRICING MENU STARTS HERE ---------------------//
	
	//for Packages for Dealers---
	Route::resource('package_for_dealers','PackageForDealersController');
	//for Packages for Private Sellers---
	Route::resource('package_for_private_sellers','PackageForPrivateSellersController');
	Route::resource('package_for_ps_watch','PackageForPrivateSellersWatchController');

//----------------------- PACKAGE & PRICING MENU ENDS HERE -----------------------//	


//----------------------- PACKAGE FOR DELAERS SETTING OPTIONS STARTS HERE -----------------------//

	Route::get('package_for_dealers/package_setting/{id}','PackageForDealersController@package_setting');
	Route::post('package_for_dealers/package_setting_update/{id}','PackageForDealersController@package_setting_update');

//----------------------- PACKAGE FOR DELAERS SETTING OPTIONS ENDS HERE -----------------------//


//----------------------- PACKAGE FOR PRIVATE SELLERS SETTING OPTIONS STARTS HERE -----------------------//

	Route::get('package_for_private_sellers/package_setting/{id}','PackageForPrivateSellersController@package_setting');
	
	Route::post('package_for_private_sellers/package_setting_update/{id}','PackageForPrivateSellersController@package_setting_update');
	
	Route::get('package_for_ps_watch/package_setting/{id}','PackageForPrivateSellersWatchController@package_setting');
	
	Route::post('package_for_ps_watch/package_setting_update/{id}','PackageForPrivateSellersWatchController@package_setting_update');

	//----------------------- PACKAGE FOR PRIVATE SELLERS SETTING OPTIONS ENDS HERE -----------------------//{

// USER MANAGEMENT WITH ROLES AND PERMISSIONS
Route::resource('roles','RoleController');
Route::get('roles/showRolePermissions/{id}','RoleController@showRolePermissions');
Route::resource('users','UserController');
Route::resource('categories','CategoriesController');
// USER MANAGEMENT WITH ROLES AND PERMISSIONS	

//----------------------- PROPERTY DATA MENU STARTS HERE -----------------------//

Route::resource('propertyData','PropertyDataController');
Route::post('propertyData/update/{id}','PropertyDataController@update');
Route::get('deleteImage/{id}','PropertyImageController@destroy');
Route::resource('propertycomment','PropertyCommentController');
Route::get('propertycomment/createComment/{id}','PropertyCommentController@createComment');
Route::resource('uploadpropertyimage','PropertyImageController');
Route::get('uploadpropertyimage/createImage/{id}','PropertyImageController@createImage');
Route::resource('createpropertyfeature','PropertyFeaturesController');
Route::get('createpropertyfeature/createFeature/{id}','PropertyFeaturesController@createFeature');
Route::resource('propertybanner', 'PropertyBannerController');
Route::get('featuredproperty', 'PropertyFeaturedController@showFeaturedProperties');
Route::get('propertyData/editFeatured/{id}','PropertyFeaturedController@editFeatured');
Route::post('propertyData/updatefeatured/{id}','PropertyFeaturedController@updateFeatured');
Route::get('tophomesproperty', 'PropertyTopHomesController@showTopHomesProperties');
Route::get('propertyData/editTopHomes/{id}','PropertyTopHomesController@editTopHomes');
Route::post('propertyData/updateTopHomes/{id}','PropertyTopHomesController@updateTopHomes');
Route::resource('propertyblog', 'PropertyBlogController');
Route::post('propertyblog/update/{id}','PropertyBlogController@update');
Route::get('propertyBlogDeleteImage/{id}','PropertyBlogImageController@destroy');
Route::resource('uploadpropertyblogimage','PropertyBlogImageController');
Route::get('uploadpropertyblogimage/createImage/{id}','PropertyBlogImageController@createImage');
Route::get('dreamlocationproperty', 'PropertyDreamLocationController@showDreamLocationProperties');
Route::get('propertyData/editdreamlocation/{id}','PropertyDreamLocationController@editDreamLocation');
Route::post('propertyData/updatedreamlocation/{id}','PropertyDreamLocationController@updateDreamLocation');
Route::resource('propertynews', 'PropertyNewsController');
Route::resource('uploadpropertynewsimage','PropertyNewsImageController');
Route::get('uploadpropertynewsimage/createImage/{id}','PropertyNewsImageController@createImage');
Route::get('propertylatestoffer/updateOffer/{id}','PropertyDataController@updateLatestOffer');

//----------------------- PROPERTY DATA MENU ENDS HERE -------------------------//

//----------------------- CURRENCY DATA MENU STARTS HERE -----------------------//

Route::resource('currencyData','CurrencyDataController');
Route::get('currencySelect/select','CurrencyDataController@selectCurrency');
Route::get('currencySelect/saveSelect/{id}','CurrencyDataController@saveSelectCurrency');
Route::resource('packagepricingrange','PackageListingRangePrivateSellerController');
Route::resource('countrycurrency','CountryBasedCurrencyController');
Route::resource('currencyconverter','CurrencyConverterController');

//----------------------- CURRENCY DATA MENU ENDS HERE -------------------------//

// ---------------------- AUTOMOBILE LISTING DATA MENU STARTS HERE ------------//

Route::resource('createautomobiledata','AutomobileDataListingController');
Route::post('automobileData/update/{id}','AutomobileDataListingController@update');
Route::get('deleteAutomobileImage/{id}','AutomobileImageController@destroy');
Route::resource('automobilecomment','AutomobileCommentController');
Route::get('automobilecomment/createComment/{id}','AutomobileCommentController@createComment');
Route::post('createautomobiledata/getModel', 'AutomobileDataListingController@getModelByMake');
Route::get('automobilelatestoffer/updateOffer/{id}','AutomobileDataListingController@updateLatestOffer');

Route::resource('automobileblog', 'AutomobileBlogController');
Route::post('automobileblog/update/{id}','AutomobileBlogController@update');
Route::get('automobileBlogDeleteImage/{id}','AutomobileBlogImageController@destroy');
Route::resource('uploadautomobileblogimage','AutomobileBlogImageController');
Route::get('uploadautomobileblogimage/createImage/{id}','AutomobileBlogImageController@createImage');

Route::get('featuredautomobile', 'AutomobileFeaturedController@showFeaturedProperties');
Route::get('automobileData/editFeatured/{id}','AutomobileFeaturedController@editFeatured');
Route::post('automobileData/updatefeatured/{id}','AutomobileFeaturedController@updateFeatured');
Route::resource('automobilefeaturelisting','AutomobileFeatureListingController');

// ---------------------- AUTOMOBILE LISTING DATA MENU ENDS HERE --------------//

// ---------------------- AUTOMOBILE IMAGE MENU STARTS HERE ------------//

Route::resource('uploadautomobileimage','AutomobileImageController');
Route::get('uploadautomobileimage/createImage/{id}','AutomobileImageController@createImage');

Route::resource('automobilebanner', 'AutomobileBannerController');
Route::resource('automobilecontactus', 'AutomobileContactusController');

// ---------------------- AUTOMOBILE IMAGE MENU ENDS HERE --------------//

// ---------------------- WATCH LISTING DATA MENU STARTS HERE ------------//

Route::resource('createwatchdata','WatchDataListingController');
Route::post('watchData/update/{id}','WatchDataListingController@update');
Route::get('deleteWatchImage/{id}','WatchImageController@destroy');
Route::post('/createwatchdata/getModel', 'WatchDataListingController@getModelByBrand');
Route::get('watchlatestoffer/updateOffer/{id}','WatchDataListingController@updateLatestOffer');

Route::resource('createwatchfeature','WatchFeaturesController');
Route::get('createwatchfeature/createFeature/{id}','WatchFeaturesController@createFeature');

Route::resource('watchcomment','WatchCommentController');
Route::get('watchcomment/createComment/{id}','WatchCommentController@createComment');

Route::resource('watchbanner', 'WatchBannerController');

Route::get('featuredwatch', 'WatchFeaturedController@showFeaturedProperties');
Route::get('watchData/editFeatured/{id}','WatchFeaturedController@editFeatured');
Route::post('watchData/updatefeatured/{id}','WatchFeaturedController@updateFeatured');

Route::resource('watchfeaturelisting','WatchFeaturesListingController');

//---------------------- WATCH IMAGE ----------------------------//
Route::resource('uploadwatchimage','WatchImageController');
Route::get('uploadwatchimage/createImage/{id}','WatchImageController@createImage');
Route::resource('watchcontactus', 'WatchContactusController');
Route::resource('watchblog', 'WatchBlogController');
Route::post('watchblog/update/{id}','WatchBlogController@update');
Route::get('watchBlogDeleteImage/{id}','WatchBlogImageController@destroy');
Route::resource('uploadwatchblogimage','WatchBlogImageController');
Route::get('uploadwatchblogimage/createImage/{id}','WatchBlogImageController@createImage');

// ---------------------- WATCH LISTING DATA MENU ENDS HERE --------------//


Route::get('watch/contract', 'admin\WatchContractController@createContract');
Route::post('watch/contract/update', ['as' => 'watchContract.update', 'uses' => 'admin\WatchContractController@updateContract']);
//Route::get('watch/popup', 'WatchContractController@createContract');



//---------------------- PROFILE STARTS----------------------------//

Route::resource('profile', 'ProfileController');
Route::get('profile/{id}', 'ProfileController@show');

//---------------------- PROFILE ENDS----------------------------//

//---------------------- AGENT STARTS----------------------------//

Route::resource('agencies', 'AgenciesController');
Route::post('agenciesData/update/{id}','AgenciesController@update');
Route::get('agenciesDeleteImage/{id}','AgenciesImageController@destroy');
Route::resource('uploadagenciesimage','AgenciesImageController');
Route::get('uploadagenciesimage/createImage/{id}','AgenciesImageController@createImage');
Route::resource('agentdata', 'AgentController');
Route::post('agentData/update/{id}','AgentController@update');
Route::resource('uploadagentimage','AgentImageController');
Route::get('agentDeleteImage/{id}','AgentImageController@destroy');
Route::get('uploadagentimage/createImage/{id}','AgentImageController@createImage');
Route::resource('agentjobposition','AgentJobPositionController');
//---------------------- AGENT ENDS----------------------------//

//---------------------- CONTRACTED PRODUCTS STARTS----------------------------//
Route::resource('contactus', 'ContactusController');
Route::resource('propertycontractedproduct', 'PropertyContractedDetailsController');
Route::resource('watchcontractedproduct', 'WatchContractedDetailsController');
Route::resource('automobilecontractedproduct', 'AutomobileContractedDetailsController');

//---------------------- CONTRACTED PRODUCTS STARTS----------------------------//

//---------------------- Automobile Transmission STARTS----------------------------//
Route::resource('transmission','TransmissionController');
//---------------------- Automobile Transmission Ends----------------------------//

//---------------------- Language STARTS----------------------------//
Route::resource('language','LanguageController');
//---------------------- Language Ends------------------------------//

//---------------------- Testimonial STARTS----------------------------//
Route::resource('testimonial','admin\TestimonialController');
//---------------------- Testimonial Ends------------------------------//

//---------------------- CMS Pages STARTS----------------------------//
Route::resource('cmspages','CmsPagesController');
//---------------------- CMS Pages Ends------------------------------//

//---------------------- FAQ Management STARTS----------------------------//
Route::resource('faq-cat','FaqCategoryController');
Route::resource('faq-subcat','FaqSubCategoryController');
Route::resource('faq','FaqController');
//Sub category ajax request
Route::post('faqsub-cat','FaqSubCategoryController@getSubCategory');
//---------------------- FAQ Management Ends------------------------------//
});

});	
//Dealer Login 
Route::group(['prefix'=> 'dealer/'], function (){
	Route::get('/logout', function(){
   Auth::logout();
   return Redirect::to('dealer/login');
});
	Route::get('/login', 'DealerLoginController@login')->name('dealer.login');
	Route::get('/reset', 'DealerPasswordResetController@index');
	Route::post('/logincheck', 'DealerLoginController@checklogin')->name('logincheck');
	Route::get('/register', 'DealerLoginController@register')->name('dealer.register');
	Route::post('/newDealerRegister', 'DealerLoginController@newDealerRegister')->name('newDealerRegister');
});
Route::group(['middleware'=>['auth','dealer']], function (){

	Route::group(['prefix'=> 'dealer/'], function (){
		   Route::get('/dashboard', 'DealerPortalController@index');
		    Route::post('/getCity', 'DealerPortalController@getCityByState');
            Route::get('/addUser', 'DealerAddUserFrontEndController@create');
            Route::get('/editUser/{agentId}', 'DealerAddUserFrontEndController@edit');
            Route::get('/listagents', 'DealerAddUserFrontEndController@listAgents');
            Route::post('/addUserData', 'DealerAddUserFrontEndController@store');
            Route::post('/updateUserData', 'DealerAddUserFrontEndController@update');
            Route::get('/inventoryList', 'DealerInventoryController@inventoryList')->name('inventoryList');
            Route::get('/prospect', 'DealerEnquiryController@prospectList')->name('prospectList');
            Route::get('/enquiryList', 'DealerEnquiryController@enquiryList')->name('enquiryList');
            Route::resource('dealerManagement', 'DealerManagementController');
            Route::get('/addAuto', 'DealerAddListingsController@createAuto');
            Route::get('/addWatch', 'DealerAddListingsController@createWatch');
            Route::get('/addProperty', 'DealerAddListingsController@createProperty');
            Route::post('/addWatchData', 'DealerAddListingsController@storeWatchData');
            Route::post('/addAutoData', 'DealerAddListingsController@storeAutoData');
            Route::post('/addPropertyData', 'DealerAddListingsController@addPropertyData');
            Route::post('/getState', 'DealerAddListingsController@getStateByCountry');
            Route::post('/getCity', 'DealerAddListingsController@getCityByState');
            Route::post('/top_property_type/getSubType', 'DealerAddListingsController@getSubPropertyType');
            
            Route::get('editwatchads/{id}', 'ManageDealerInventoryController@editWatch');
            Route::get('deletewatchadsimage/{id}/{imagetype}', 'ManageDealerInventoryController@deleteWatch');
            Route::post('storewatch', 'ManageDealerInventoryController@storeWatch');
            Route::get('editautoads/{id}', 'ManageDealerInventoryController@editAuto');
            Route::get('deleteautoadsimage/{id}/{imagetype}', 'ManageDealerInventoryController@deleteAuto');
            Route::post('storeauto', 'ManageDealerInventoryController@storeAuto');
            Route::get('editpropertyads/{id}', 'ManageDealerInventoryController@editProperty');
            Route::get('deletepropertyadsimage/{id}/{imagetype}', 'ManageDealerInventoryController@deleteProperty');
            Route::post('storeproperty', 'ManageDealerInventoryController@storeProperty');
	});
});

//User Login
Route::group(['prefix'=> 'user/'], function (){
	Route::get('/logout', function(){
   Auth::logout();
   session(['url.intended' => url()->previous()]);
	    return redirect(session()->get('url.intended'));
});
    Route::get('/checkUser', 'UserLoginController@checkRegisteredUser')->name('checkUser');
	Route::get('/login', 'UserLoginController@login')->name('user.login');
	Route::post('/logincheck', 'UserLoginController@checklogin')->name('logincheck');
	Route::get('/register', 'UserLoginController@register')->name('user.register');
	Route::post('/newUserRegister', 'UserLoginController@newUserRegister')->name('newUserRegister');
});
Route::group(['middleware'=>['auth','user']], function (){

	Route::group(['prefix'=> 'user/'], function (){
	    Route::post('/savelisting','SaveListingController@saveListing');
		  Route::get('/dashboard', function () {
		    return view('user.dashboard.dashboard');
		    });
	});
});
//

//privateSeller Login
Route::group(['prefix'=> 'privateSeller/'], function (){
	Route::get('/logout', function(){
   Auth::logout();
   return Redirect::to('privateSeller/login');
});
	Route::get('/login', 'PrivateSellerLoginController@login')->name('privateSeller.login');
	Route::post('/logincheck', 'PrivateSellerLoginController@checklogin')->name('logincheck');
	Route::get('/register', 'PrivateSellerLoginController@register')->name('PrivateSeller.register');
	Route::post('/newprivateSellerRegister', 'PrivateSellerLoginController@newprivateSellerRegister')->name('newprivateSellerRegister');
	Route::group(['middleware'=>['auth','privateSeller']], function (){
		Route::get('/dashboard', 'PrivateSellerController@index');
		Route::get('/enquiryList', 'PrivateSellerEnquiryController@enquiryList')->name('enquiryList');
	});
});


//Agent Login
Route::group(['prefix'=> 'agent/'], function (){
	Route::get('logout', function(){
   Auth::logout();
   return Redirect::to('agent/login');
});
	Route::get('/login', 'AgentLoginController@login')->name('agent.login');
	Route::post('/logincheck', 'AgentLoginController@checklogin')->name('logincheck');
	Route::get('/register', 'AgentLoginController@register')->name('agent.register');
	Route::post('/newAgentRegister', 'AgentLoginController@newagentRegister')->name('newAgentRegister');
	});
	// Route::group(['middleware'=>['auth','Agent']], function (){
Route::group(['middleware'=>['auth','Agent']], function (){
			Route::group(['prefix'=> 'agent/'], function (){
			Route::get('/dashboard', 'AgentLoginController@index');
			Route::get('/agentEnquiryList', 'AgentEnquiryController@enquiryList')->name('AgentenquiryList');
			Route::get('/enquiryDetails/{message_id}', 'AgentEnquiryController@enquiryDetail');

			Route::get('/addAuto', 'AgentAddListingsController@createAuto');
			Route::get('/addWatch', 'AgentAddListingsController@createWatch');
			Route::get('/addProperty', 'AgentAddListingsController@createProperty');
			Route::post('/addWatchData', 'AgentAddListingsController@addWatchData');
			Route::post('/addAutoData', 'AgentAddListingsController@addAutoData');
			Route::post('/addPropertyData', 'AgentAddListingsController@addPropertyData');
			Route::post('/getState', 'AgentAddListingsController@getStateByCountry');
			Route::post('/getCity', 'AgentAddListingsController@getCityByState');
			Route::get('/inventoryList', 'AgentInventoryController@inventoryList')->name('inventoryList');
			Route::get('editwatchads/{id}', 'ManageAgentInventoryController@editWatch');
			Route::get('deletewatchadsimage/{id}/{imagetype}', 'ManageAgentInventoryController@deleteWatch');
			Route::post('storewatch', 'ManageAgentInventoryController@storeWatch');
			Route::get('editautoads/{id}', 'ManageAgentInventoryController@editAuto');
			Route::get('deleteautoadsimage/{id}/{imagetype}', 'ManageAgentInventoryController@deleteAuto');
			Route::post('storeauto', 'ManageAgentInventoryController@storeAuto');
			Route::get('editpropertyads/{id}', 'ManageAgentInventoryController@editProperty');
			Route::get('deletepropertyadsimage/{id}/{imagetype}', 'ManageAgentInventoryController@deleteProperty');
			Route::post('storeproperty', 'ManageAgentInventoryController@storeProperty');
			
			Route::post('/top_property_type/getSubType', 'AgentAddListingsController@getSubPropertyType');
		});
	});

//userProfile
Route::resource('myprofile', 'UserProfileController');

// seller accept offer
Route::post('/seller/acceptoffer', 'SellerOfferController@acceptOffer');
Route::post('/seller/submitfinaloffer', 'SellerOfferController@submitFinalOffer');
Route::post('/seller/submitshippingdetail', 'SellerOfferController@submitShippingDetail');
// User checkout offer
Route::post('/buyer/checkout', 'BuyerOfferController@checkout');
//Route::get('/buyer/checkout', 'BuyerOfferController@checkout');
Route::post('/buyer/counterOffer', 'BuyerOfferController@counteroffer');
Route::post('/buyer/submitcounterOffer', 'BuyerOfferController@buyerSubumitCounterOffer');
Route::post('/buyer/confirmorder', 'ConfirmOrderController@confirmOrder');
Route::post('/buyer/payment/', 'ProductPaymentController@payment')->name('paymentorder');
Route::post('/buyer/deliveryStatus/', 'BuyerOfferController@buyerDeliveryStatus')->name('paymentorder');


// seller accept offer
Route::post('/seller/acceptoffer', 'SellerOfferController@acceptOffer');
Route::post('/seller/submitfinaloffer', 'SellerOfferController@submitFinalOffer');

//my messages
Route::resource('mymessages', 'MyMessageController');
Route::resource('buy', 'BuyController');
Route::resource('sell', 'SellController');
Route::resource('mylistings', 'MyListingController');
Route::get('manageads/{id}/{category}', 'ManageAdController@manageads');
Route::get('editautoads/{id}', 'ManageAdController@editAuto');
Route::get('deleteautoadsimage/{id}/{imagetype}', 'ManageAdController@deleteAuto');
Route::get('editwatchads/{id}', 'ManageAdController@editWatch');
Route::get('deletewatchadsimage/{id}/{imagetype}', 'ManageAdController@deleteWatch');
Route::post('storewatch', 'ManageAdController@storeWatch');
Route::post('storeauto', 'ManageAdController@storeAuto');
Route::post('/getMessages', 'MyMessageController@getMessage');
Route::post('/saveMessages', 'MyMessageController@saveMessage');


//strip payment

Route::get('/payments', 'PaymentsController@create');
Route::post('/payments', 'PaymentsController@store');
Route::post('/watchPayments', 'PaymentsController@watchStore');
Route::post('/automobilePayments', 'PaymentsController@automobileStore');
Route::post('/realEstatePayments', 'PaymentsController@realEstateStore');
Route::post('/privateSellerPayments', 'PaymentsController@privateSellerPaymentStore');
Route::get('/thankyou', 'PaymentsController@thankyou');


Route::get('/stripe-payment/{packageId}', 'StripeController@handleGet');
Route::get('/reauth/{acId}', 'StripeController@reauth');

Route::post('/stripe-payment', 'StripeController@handlePost')->name('stripe.payment');

//---------------- FAQ Frontend Page Starts here --------------------------//
Route::get('faq_page','FaqFrontendController@index');
Route::get('faq_page/{slug}','FaqFrontendController@getSubCat');
Route::get('faq_page/{slug}/{cat_id}/{subcat_id}','FaqFrontendController@getFaq');
//---------------- FAQ Frontend Page Ends here ----------------------------//

Route::get('/{any}','CmsPagesController@page');
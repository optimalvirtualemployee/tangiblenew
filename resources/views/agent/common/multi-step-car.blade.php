<script src="/assets/js/jquery.js"></script>
     <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/jquery-validation@1.17.0/dist/jquery.validate.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-steps/1.1.0/jquery.steps.min.js"></script>
     <script src="/assets/js/image-uploader.min.js"></script>
     
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
        integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
        crossorigin="anonymous"></script>
    <script src="/assets/js/bootstrap.min.js"></script> 
    <script src="/assets/js/slick.min.js"></script>
    <script src="/assets/js/intlTelInput.min.js"></script>
    <script src="/assets/js/local.js"></script>
    <script src="https://js.stripe.com/v3/"></script>
    <script src="/assets/js/image-uploader.min.js"></script>
    <script>
    
        // steps JS
        $(document).ready(function(){
        	var current_fs, next_fs, previous_fs; //fieldsets
            var opacity;
            /* const ImgArray = [];
            const ImgArrayDamage = []; */ 
            /* const imageArrayfn = function () {
                alert('inside ');
                $('.uploaded img').each(function (index, value) {
                  if($(this).parents('#damagePhotos').length > 0){
                    ImgArrayDamage.push($(this).attr('src'));
                  }
                  else{
                    ImgArray.push($(this).attr('src'));
                  }
                });
                imgUploadedfn();
            }
        
            const imgUploadedfn = function () {
                let imgCount = ImgArray.length;
                let imgCountDmg = ImgArrayDamage.length;
                let html = '';
                let imgGallery = '';
                let dmgGallery= '';
                debugger;
                if (imgCount > 0) {
                    for (let j = 0; j < imgCount; j++) {                   
                        // html += ' <div class="item" data-src="' + ImgArray[j] + '"><img src="' + ImgArray[j] + '"></div>';
                        imgGallery += '<div><img src="' + ImgArray[j] + '" /></div>';
                    }
                    // $('#uploadedImages').append(html);
                    $('#imgGallery').append(imgGallery);      
                    // $('.slider').slick({
                    //     slidesToShow: 1,
                    //     slidesToScroll: 1,
                    //     autoplay: true,
                    //     arrows: true,
                    //     fade: false
                    // });
                }  
                if (imgCountDmg > 0) {
                    for (let i = 0; i < imgCountDmg; i++) { 
                      dmgGallery += '<div><img src="' + ImgArrayDamage[i] + '" /></div>';
                    }                 
                    $('#dmgGallery').append(dmgGallery);      
                    
                } 

                $('#normalPhotos').val('');
                $('#dmgPhotos').val('');
                $('#normalPhotos').val(ImgArray);
                $('#dmgPhotos').val(ImgArrayDamage);
                
                  console.log('Images Array - > ', ImgArray)  ;  
                  console.log('Damage Images Array - > ', ImgArrayDamage)  ;  
            } */ 

  /* $(".next").click(function(){
alert($(this));
console.log('current this ',$(this));
current_fs = $(this).parent();
next_fs = $(this).parent().next();

//Add Class Active
$("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");

//show the next fieldset
next_fs.show();
//hide the current fieldset with style
current_fs.animate({opacity: 0}, {
step: function(now) {
// for making fielset appear animation
opacity = 1 - now;

current_fs.css({
'display': 'none',
'position': 'relative'
});
next_fs.css({'opacity': opacity});
},
duration: 600
});
}); */  

 $(".previous").click(function(){

current_fs = $(this).parent();
previous_fs = $(this).parent().prev();

//Remove class active
$("#progressbar li").eq($("fieldset").index(current_fs)).removeClass("active");

//show the previous fieldset
previous_fs.show();

//hide the current fieldset with style
current_fs.animate({opacity: 0}, {
step: function(now) {
// for making fielset appear animation
opacity = 1 - now;

current_fs.css({
'display': 'none',
'position': 'relative'
});
previous_fs.css({'opacity': opacity});
},
duration: 600
});
}); 

$('.radio-group .radio').click(function(){
$(this).parent().find('.radio').removeClass('selected');
$(this).addClass('selected');
});

$(".submit").click(function(){
return false;
})
$('.input-images-1').imageUploader();
$('.input-images-2').imageUploader2();
});
    </script>
    <script>

    $("#countryId").change(function(){
    	$('#loading-image').show();
        var countryId = $(this).val();
        $.ajax({
          url: "/getState",
          type: "POST",
          data: {countryId: countryId, _token: '{{csrf_token()}}' },
          dataType: 'json',
          success : function(data){
            var html = `<option value="">Select State</option>`;
            for (var i = 0; i < data.length; i++) {
                var id = data[i].id;
                var name = data[i].state_name;
                var option = `<option value="${data[i].id}">${data[i].state_name}</option>`;
                html += option;
              }
              $('#stateId').html(html); 
            }
          });
        $.ajax({
			url: "/getCity",
			type: "POST",
			data: {countryId: countryId  ,_token: '{{csrf_token()}}' },
			dataType: 'json',
			success : function(data){
				var html = `<option value="">Select City</option>`;
				for (var i = 0; i < data.length; i++) {
					  var id = data[i].id;
					  var name = data[i].city_name;
					  var option = `<option value="${data[i].id}">${data[i].city_name}</option>`;
					  html += option;
					}
				$('#loading-image').hide();
					$('#cityId').html(html);	
				}
			});
      });
        $("#stateId").change(function(){
        var stateId = $(this).val();
        $('#loading-image').show();
        $.ajax({
          url: "/getCity",
          type: "POST",
          data: {stateId: stateId, _token: '{{csrf_token()}}' },
          dataType: 'json',
          success : function(data){
            var html = `<option value="">Select City</option>`;
            for (var i = 0; i < data.length; i++) {
                var id = data[i].id;
                var name = data[i].city_name;
                var option = `<option value="${data[i].id}">${data[i].city_name}</option>`;
                html += option;
              }
            $('#loading-image').hide();
              $('#cityId').html(html);  
            }
          });
      });

        $("#country_pi").change(function(){
        	$('#loading-image').show();
            var countryId = $(this).val();
            $.ajax({
              url: "/getState",
              type: "POST",
              data: {countryId: countryId, _token: '{{csrf_token()}}' },
              dataType: 'json',
              success : function(data){
                var html = `<option value="">Select State</option>`;
                for (var i = 0; i < data.length; i++) {
                    var id = data[i].id;
                    var name = data[i].state_name;
                    var option = `<option value="${data[i].id}">${data[i].state_name}</option>`;
                    html += option;
                  }
                  $('#state_pi').html(html); 
                }
              });
            $.ajax({
    			url: "/getCity",
    			type: "POST",
    			data: {countryId: countryId  ,_token: '{{csrf_token()}}' },
    			dataType: 'json',
    			success : function(data){
    				var html = `<option value="">Select City</option>`;
    				for (var i = 0; i < data.length; i++) {
    					  var id = data[i].id;
    					  var name = data[i].city_name;
    					  var option = `<option value="${data[i].id}">${data[i].city_name}</option>`;
    					  html += option;
    					}
    				$('#loading-image').hide();
    					$('#city_pi').html(html);	
    				}
    			});
          });
            $("#state_pi").change(function(){
            var stateId = $(this).val();
            $('#loading-image').show();
            $.ajax({
              url: "/getCity",
              type: "POST",
              data: {stateId: stateId, _token: '{{csrf_token()}}' },
              dataType: 'json',
              success : function(data){
                var html = `<option value="">Select City</option>`;
                for (var i = 0; i < data.length; i++) {
                    var id = data[i].id;
                    var name = data[i].city_name;
                    var option = `<option value="${data[i].id}">${data[i].city_name}</option>`;
                    html += option;
                  }
                $('#loading-image').hide();
                  $('#city_pi').html(html);  
                }
              });
          });  	
    
    $("#make1").change(function(){
		var brandId = $(this).val();
		$('#loading-image').show();
		$.ajax({
			url: "/getCarModel",
			type: "POST",
			data: {brandId: brandId, _token: '{{csrf_token()}}' },
			dataType: 'json',
			success : function(data){
				var html = `<option value="">Select Model</option>`;
				for (var i = 0; i < data.length; i++) {
					  var id = data[i].id;
					  var name = data[i].city_name;
					  var option = `<option value="${data[i].id}">${data[i].automobile_model_name}</option>`;
					  html += option;
					}
				$('#loading-image').hide();
					$('#model').html(html);	
				}
			});
	});


    // VIN Upload Image Script
    $(".imageUploadCls").change(function() {  
      const security = $(this).attr('id');
      if (this.files && this.files[0]) {
            let reader = new FileReader();
            reader.onload = function(e) {                 
                $('#'+security).parents('.avatar-upload').find('.imagePreview').css('background-image', 'url('+e.target.result +')');             
                $('#'+security).parents('.avatar-upload').find('.imagePreview').hide();
                $('#'+security).parents('.avatar-upload').find('.imagePreview').fadeIn(650);
            }
            reader.readAsDataURL(this.files[0]);
          }   
    });
    
        $(function(){
        
          // First register any plugins
          $.fn.filepond.registerPlugin(FilePondPluginImagePreview);
      
          // Turn input element into a pond
          $('.my-pond').filepond();
      
          // Set allowMultiple property to true
          $('.my-pond').filepond('allowMultiple', true);
        
          // Listen for addfile event
          $('.my-pond').on('FilePond:addfile', function(e) {
              console.log('file added event', e);
          });
      
          // Manually add a file using the addfile method
          $('.my-pond').first().filepond('addFile', 'index.html').then(function(file){
            console.log('file added', file);
          });
        
        });
        //Add Row
        $('#addRow').on('click',function(){
            const rowData = '<tr><td><input type="text" id="additional-feature" class="form-control"></td><td><button class="btn btn-danger removeRow" type="button""><i class="fa fa-times"></button></td><tr>';
            $('tbody').append(rowData);
        });

        $(document).on( 'click', '.removeRow', function(){                    
            $(this).parents('tr').remove();
        })
        

          $(document).ready(function(){

        	  	
        	  //var form = document.getElementById('payment-form');
        	  var normalImages =[];
        	  const ImgArray = [];
              const ImgArrayDamage = [];
              const ImgArraySend = [];
              const ImgArrayDamageSend = [];
        	 const imageArrayfn = function () {
                
                 $('.uploaded img').each(function (index, value) {
                   if($(this).parents('#damagePhotos').length > 0){
                       //ImgArrayDamageSend.push($(this));
                     ImgArrayDamage.push($(this).attr('src'));
                   }
                   else{
                	   //ImgArraySend.push($(this));  
                     ImgArray.push($(this).attr('src'));
                   }
                 });
                 imgUploadedfn();
             }
         
             const imgUploadedfn = function () {
                 let imgCount = ImgArray.length;
                 let imgCountDmg = ImgArrayDamage.length;
                 let html = '';
                 let imgGallery = '';
                 let dmgGallery= '';
                 if (imgCount > 0) {
                     for (let j = 0; j < imgCount; j++) {                   
                         // html += ' <div class="item" data-src="' + ImgArray[j] + '"><img src="' + ImgArray[j] + '"></div>';
                         imgGallery += '<div><img src="' + ImgArray[j] + '" /></div>';
                     }
                     // $('#uploadedImages').append(html);
                     $('#imgGallery').append(imgGallery);      
                     // $('.slider').slick({
                     //     slidesToShow: 1,
                     //     slidesToScroll: 1,
                     //     autoplay: true,
                     //     arrows: true,
                     //     fade: false
                     // });
                 }  
                 if (imgCountDmg > 0) {
                     for (let i = 0; i < imgCountDmg; i++) { 
                       dmgGallery += '<div><img src="' + ImgArrayDamage[i] + '" /></div>';
                     }                 
                     $('#dmgGallery').append(dmgGallery);      
                     
                 } 

                   $("input:file").each(function () {

                	  ImgArraySend.push(document.getElementsByName('photos'));		               	 
                     }); 
					
                     var file = $('[name="images"]');

                 $('#normalPhotos').val('');
                 $('#dmgPhotos').val('');
                 $('#normalPhotos').val(ImgArraySend);
                 $('#dmgPhotos').val(ImgArrayDamageSend);
                 
                   console.log('Images Array - > ', ImgArray)  ;  
                   console.log('Damage Images Array - > ', ImgArrayDamage)  ;  
             }
             
        var isError = false;
        $("#next1").click(function(){
				var make = $('#make1').val();
				var model = $('#model').val();
				var year = $('#year').val();

				  if(make == ""){
					$('#make1').css('border','1px solid red');
					isError = true;
					}else{
          $('#make1').css('border','');
					}
				  if(model == ""){
					$('#model').css('border','1px solid red');
					isError = true;
					}else{
          $('#model').css('border','')
					}
				  if(year == ""){
					$('#year').css('border','1px solid red');
					isError = true;
					}else{
          $('#year').css('border','')
					} 

        if(isError == false){
          current_fs = $('#next1').parent();
    			next_fs = $('#next1').parent().next();

    			//Add Class Active
    			$("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");

    			//show the next fieldset
    			next_fs.show();
    			//hide the current fieldset with style
    			current_fs.animate({opacity: 0}, {
    			step: function(now) {
    			// for making fielset appear animation
    			opacity = 1 - now;

    			current_fs.css({
    			'display': 'none',
    			'position': 'relative'
    			});
    			next_fs.css({'opacity': opacity});
    			},
    			duration: 600
    			});
               	 }
        	 });

        	 $("#next2").click(function(){
        		 var adTitle = $('#adtitle').val();
        		 var country = $('#countryId :selected').val();
        		 var city = $('#cityId :selected').val();
        		 var odometerMetric = $('#odometermetric').val();
        		 var odometer = $('#odometer').val();
        		 var currency = $('#currency').val();
        		 var price_on_request = $('#price_on_request').val();
        		 var price = $('#price').val();
        		 var pricetType = $('#pricetType').val();
        		 var color = $('#color').val();
        		 var interior_color = $('#interior_color').val();
        		 var fuelType = $('#fuelType').val();
        		 var registrationPlate = $('#registrationPlate').val();
        		 var rhdorlhd = $('#rhdorlhd').val();
        		 var condition = $('#neworused').val();
        		 var engine = $('#engine').val();
        		 var body_type = $('#body_type').val();
        		 var timezone = $('#timezone').val();
        		 var transmission = $('#transmission').val();
        		 var status = $('#status').val();
        		 var VIN = $('#VIN').val();
				
        		  if(adTitle == ""){
 					    $('#adtitle').css('border','1px solid red');
 					    isError = true;
 					    }else{
 						  $('#adtitle').css('border','')
 						  }	
        		  if(country == ""){
  					  $('#countryId').css('border','1px solid red');
  					  isError = true;
  					  }else{
  						$('#countryId').css('border','')
  						}
        		  if(city == ""){
   					  $('#cityId').css('border','1px solid red');
   					  isError = true;
   					  }else{
   						$('#cityId').css('border','')
   						}
        		  if(odometerMetric == ""){
    					$('#odometermetric').css('border','1px solid red');
    					isError = true;
    					}else{
    					$('#odometermetric').css('border','')
    					}
        		  if(odometer == ""){
    					$('#odometer').css('border','1px solid red');
    					isError = true;
    					}else{
    					$('#odometer').css('border','')
    					}
        		  if(currency == ""){
 					    $('#currency').css('border','1px solid red');
 					    isError = true;
 					    }else{
 					  	$('#currency').css('border','')
 						  }
        		  if(price_on_request == ""){
  					  $('#price_on_request').css('border','1px solid red');
  					  isError = true;
  					  }else{
  						$('#price_on_request').css('border','')
  						}
        		  if(price == ""){
   					  $('#price').css('border','1px solid red');
   					  isError = true;
   					  }else{
   						$('#price').css('border','')
   						}					
        		  if(pricetType == ""){
    					$('#pricetType').css('border','1px solid red');
    					isError = true;
    					}else{
    					$('#pricetType').css('border','')
    					}
        		  if(color == ""){
 					    $('#color').css('border','1px solid red');
 					    isError = true;
 				    	}else{
 					  	$('#color').css('border','')
 					  	}	
        		  if(interior_color == ""){
  					  $('#interior_color').css('border','1px solid red');
  					  isError = true;
  					  }else{
  						$('#interior_color').css('border','')
  						}	
        		  if(fuelType == ""){
 					    $('#fuelType').css('border','1px solid red');
 					    isError = true;
 					    }else{
 					  	$('#fuelType').css('border','')
 					  	}	
        		  if(registrationPlate == ""){
  					  $('#registrationPlate').css('border','1px solid red');
  					  isError = true;
  					  }else{
  						$('#registrationPlate').css('border','')
  						}
        		  if(rhdorlhd == ""){
   					  $('#rhdorlhd').css('border','1px solid red');
   				  	isError = true;
   				  	}else{
   						$('#rhdorlhd').css('border','')
   						}
        		  if(condition == ""){
   					  $('#condition').css('border','1px solid red');
   					  isError = true;
   					  }else{
   						$('#condition').css('border','')
   						}
        		  if(engine == ""){
   					  $('#engine').css('border','1px solid red');
   					  isError = true;
   					  }else{
   						$('#engine').css('border','')
   						}
        		  if(body_type == ""){
    					$('#body_type').css('border','1px solid red');
    					isError = true;
    					}else{
    					$('#body_type').css('border','')
    					}		
        		  if(transmission == ""){
    					$('#transmission').css('border','1px solid red');
    					isError = true;
    					}else{
    					$('#transmission').css('border','')
    					}			
        		  if(status == ""){
    					$('#status').css('border','1px solid red');
    					isError = true;
    					}else{
    					$('#status').css('border','')
    					}	
        		  if(VIN == ""){
    					$('#VIN').css('border','1px solid red');
    					isError = true;
    					}else{
    					$('#VIN').css('border','')
    					}  
        		  if(timezone == ""){
  					  $('#timezone').css('border','1px solid red');
  					  isError = true;
  					  }else{
  						$('#timezone').css('border','')
  						}
						
        		 if(isError == false){
        			 current_fs = $('#next2').parent();
         			next_fs = $('#next2').parent().next();

         			//Add Class Active
         			$("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");

         			//show the next fieldset
         			next_fs.show();
         			//hide the current fieldset with style
         			current_fs.animate({opacity: 0}, {
         			step: function(now) {
         			// for making fielset appear animation
         			opacity = 1 - now;

         			current_fs.css({
         			'display': 'none',
         			'position': 'relative'
         			});
         			next_fs.css({'opacity': opacity});
         			},
         			duration: 600
         			});
            		 }
        		 
        		 
            	 });

        	 $("#pricetType").change(function(){ 
 				var htmlcarPrice = '';
 				var htmlPrice = '';
             if($(this).val() == 3){
             	htmlcarPrice += '<input id="price" class="form-control form-control-user " type="number"  min="0" name="price" placeholder="Enter Car Value" value="" disabled>';
           	  /* htmlPrice = '<select id="pricetType" class="form-control "  name="pricetType" disabled> <option value="">Select Price</option>'; */
 				$("#price").replaceWith(htmlcarPrice);
 				/* $("#pricetType").replaceWith(htmlPrice); */	
                }else{
             	   htmlcarPrice += '<input id="price" class="form-control form-control-user" type="number"  min="0" name="price" placeholder="Enter Car Value" value="">';
               	 /* htmlPrice = '<select id="pricetType" class="form-control "  name="pricetType"> <option value="">Select Price</option> @foreach ($prices as $price) <option value="{{$price->id}}">{{$price->price}}</option> @endforeach </select>'; */
   					$("#price").replaceWith(htmlcarPrice);
   					/* $("#pricetType").replaceWith(htmlPrice); */	
   	                 } 
             
         });

        	 $('#next3').click(function() {

 				var comment = $('#comment').val();

 				 if(comment == ""){
					$('#comment').css('border','1px solid red');
					isError = true;
					}else{
						
								}

				
 				if($('.uploaded-image').length == 0){
					$('#input-images-1').css('border','1px solid red');
					isError = true;
					}else{
						
						}

 				if($('.uploaded-image').length == 0){
					$('#damagePhotos').css('border','1px solid red');
					isError = true;
					}else{
						
						}
 				if($('.uploaded-image').length == 0){
					$('#vin1').css('border','1px solid red');
					isError = true;
					}else{
						
						}
 				if($('.uploaded-image').length == 0){
					$('#vin2').css('border','1px solid red');
					isError = true;
					}else{
						
						} 

 				if(isError == false){
 					imageArrayfn();
        			 current_fs = $('#next3').parent();
         			next_fs = $('#next3').parent().next();

         			//Add Class Active
         			$("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");

         			//show the next fieldset
         			next_fs.show();
         			//hide the current fieldset with style
         			current_fs.animate({opacity: 0}, {
         			step: function(now) {
         			// for making fielset appear animation
         			opacity = 1 - now;

         			current_fs.css({
         			'display': 'none',
         			'position': 'relative'
         			});
         			next_fs.css({'opacity': opacity});
         			},
         			duration: 600
         			});
            		 }
         });    

        	 $('#next4').click(function() {

        		 var make = $('#make :selected').text();
                 var model = $('#model :selected').text();
                 var title = $('#adtitle').val();
                 var buildYear = $('#year :selected').text();
 				var price = $('#price').val();
 				var currency = $('#currency :selected').text();
 				var body_type = $('#body_type :selected').text();
 				var fuelType = $('#fuelType :selected').text();
 				var engine = $('#engine').val();
 				var transmission = $('#transmission :selected').text();
 				var color = $('#color :selected').text();
 				var interior_color = $('#interior_color :selected').text();
 				var registrationPlate = $('#registrationPlate').val();
 				var VIN = $('#VIN').val();
 				var odometermetric = $('#odometermetric :selected').text();
 				var odometer = $('#odometer').val();
 				
 				
 				var titleHtml = title + ' ' + make + ' ' + model;
 				$('#title').html(titleHtml);
 				$('#priceShow').html(price);
 				var priceTop = price;
 				$('#priceTop').html(priceTop);
 				$('#bodyTypeShow').html(body_type);
 				$('#buildYearShow').html(buildYear);
 				$('#fuelTypeShow').html(fuelType);
 				$('#engineShow').html(engine);
 				$('#transmissionShow').html(transmission);
 				$('#colorShow').html(color);
 				$('#interiorColorShow').html(interior_color);
 				$('#registrationPlateShow').html(registrationPlate);
 				$('#VINShow').html(VIN);
 				var odoShow = odometer +' '+ odometermetric;
 				$('#odometerShow').html(odoShow);

 	            var k = 1;
 				var radioHtml = ``;
 	            
 				$("input:checkbox:checked").each(function () {
 					
 					   var name = $(this).attr("id");
 					    var value = $(this).attr("value");
 							
 						if (value == 1) {

 					    	if(k == 1)
 			                    radioHtml += `<ul class="list-style-2">\n`;

 			                    radioHtml += `<li>${name}</li>\n`;

 							if(k == 4)	
 								radioHtml += `</ul>\n`;

 				           k++;
 				           if(k == 5)
 					           k = 1; 
 						
 					   }

 					});			
 				$('#featureShow').html(radioHtml);


 				var firstName = $('#firstName').val();
 				var lastName = $('#lastName').val();
 				var dob = $('#dob').val();
 				var address = $('#address').val();
 				var zipcode = $('#zipcode').val();
 				var country_pi = $('#country_pi').val();
 				var city_pi = $('#city_pi').val();
 				var phonenumber = $('#phonenumber').val();
				
       		  if(firstName == ""){
					$('#firstName').css('border','1px solid red');
					isError = true;
					}else{
						
						}
       		if(lastName == ""){
				$('#lastName').css('border','1px solid red');
				isError = true;
				}else{
					
					}
       		if(dob == ""){
				$('#dob').css('border','1px solid red');
				isError = true;
				}else{
					
					}
       		if(address == ""){
				$('#address').css('border','1px solid red');
				isError = true;
				}else{
					
					}
       		if(zipcode == ""){
				$('#zipcode').css('border','1px solid red');
				isError = true;
				}else{
					
					}
       		if(country_pi == ""){
				$('#country_pi').css('border','1px solid red');
				isError = true;
				}else{
					
					}
       		if(city_pi == ""){
				$('#city_pi').css('border','1px solid red');
				isError = true;
				}else{
					
					}
       		if(address == ""){
				$('#phonenumber').css('border','1px solid red');
				isError = true;
				}else{
					
					} 

	       		 
        		 if(isError == false){
  					//imageArrayfn();
         			 current_fs = $('#next4').parent();
          			next_fs = $('#next4').parent().next();

          			//Add Class Active
          			$("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");

          			//show the next fieldset
          			next_fs.show();
          			//hide the current fieldset with style
          			current_fs.animate({opacity: 0}, {
          			step: function(now) {
          			// for making fielset appear animation
          			opacity = 1 - now;

          			current_fs.css({
          			'display': 'none',
          			'position': 'relative'
          			});
          			next_fs.css({'opacity': opacity});
          			},
          			duration: 600
          			});
        		 }
        		 
            	 });	

        	 $('#next5').click(function() {
        		 

        		 if(!$('#agb').prop('checked')){
        			 $('.generic-errors-top').html('The terms conditions must be accepted.');
        			 isError = true;
 				}else{
 					
 					}
        		 
        		 if(isError == false){
   					//imageArrayfn();
          			 current_fs = $('#next5').parent();
           			next_fs = $('#next5').parent().next();

           			//Add Class Active
           			$("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");

           			//show the next fieldset
           			next_fs.show();
           			//hide the current fieldset with style
           			current_fs.animate({opacity: 0}, {
           			step: function(now) {
           			// for making fielset appear animation
           			opacity = 1 - now;

           			current_fs.css({
           			'display': 'none',
           			'position': 'relative'
           			});
           			next_fs.css({'opacity': opacity});
           			},
           			duration: 600
           			});

         		 }
        		 
            	 });
             
         });

        
        $(window).on('load',function(){
            $('#myModal1').modal({backdrop: 'static', keyboard: false});
        });


        var initialPrice = $('.ads-fee').text();
        
        $('.activatethis').on('click',function(){           
        	  if($(this).parents('.thispackage').hasClass('activatedPackage')){
        	    $(this).parents('.thispackage').removeClass('activatedPackage');
        	    $(this).text('Activate');
        	    $('.packageName').text('Listing Fee');
        	    $('.ads-fee').text(initialPrice);
        	  }   
        	  else{
        	  $('.thispackage').removeClass('activatedPackage');
        	  $('.activatethis').text('Activate');
        	  $(this).text('Deactivate')
        	  $(this).parents('.thispackage').addClass('activatedPackage');        
        	  $('.packageName').text($('.activatedPackage h2').text());
        	  $('.ads-fee').text($('.activatedPackage .amount').text());

        	  }
        	  

        	})
        //Payment options
      $('input[type=radio][name=postAds]').change(function() {       
      if (this.value == 'free') {
         $('.paymentOptionContainer').addClass('d-none');
         $('#postNow').removeClass('d-none');
      }
      else if (this.value == 'paid') {
        $('.paymentOptionContainer').removeClass('d-none');
        $('#postNow').addClass('d-none');
      }
      });

        /* data: {brandId: brandId, _token: '{{csrf_token()}}' } */
       
        


         // Create an instance of the Stripe object
            // Set your publishable API key
            var stripe = Stripe('{{ env("STRIPE_PUBLISH_KEY") }}');

            // Create an instance of elements
            var elements = stripe.elements();

            var style = {
                base: {
                    fontWeight: 400,
                    fontFamily: '"DM Sans", Roboto, Open Sans, Segoe UI, sans-serif',
                    fontSize: '16px',
                    lineHeight: '1.4',
                    color: '#1b1642',
                    padding: '.75rem 1.25rem',
                    '::placeholder': {
                        color: '#ccc',
                    },
                },
                invalid: {
                    color: '#dc3545',
                }
            };

            var cardElement = elements.create('cardNumber', {
                style: style
            });
            cardElement.mount('#card_number');

            var exp = elements.create('cardExpiry', {
                'style': style
            });
            exp.mount('#card_expiry');

            var cvc = elements.create('cardCvc', {
                'style': style
            });
            cvc.mount('#card_cvc');

            // Validate input of the card elements
            var resultContainer = document.getElementById('paymentResponse');
            cardElement.addEventListener('change', function (event) {
                if (event.error) {
                    resultContainer.innerHTML = '<p>' + event.error.message + '</p>';
                } else {
                    resultContainer.innerHTML = '';
                }
            });

            // Get payment form element
            var form = document.getElementById('msform');

            // Create a token when the form is submitted.
            form.addEventListener('submit', function (e) {
                e.preventDefault();
                createToken();
            });

            // Create single-use token to charge the user
            function createToken() {
                stripe.createToken(cardElement).then(function (result) {
                    if (result.error) {
                        // Inform the user if there was an error
                        resultContainer.innerHTML = '<p>' + result.error.message + '</p>';
                    } else {
                        // Send the token to your server
                        stripeTokenHandler(result.token);
                    }
                });
            }

            
            // Callback to handle the response from stripe
            function stripeTokenHandler(token) {
                
                // Insert the token ID into the form so it gets submitted to the server
                var hiddenInput = document.createElement('input');
                hiddenInput.setAttribute('type', 'hidden');
                hiddenInput.setAttribute('name', 'stripeToken');
                hiddenInput.setAttribute('value', token.id);
                form.appendChild(hiddenInput);

                // Submit the form
                form.submit();
            }
            
            
            $('.pay-via-stripe-btn').on('click', function () {
                var payButton   = $(this);
                var name        = $('#name').val();
                var email       = $('#email').val();

                if (name == '' || name == 'undefined') {
                    $('.generic-errors').html('Name field required.');
                    return false;
                }
                if (email == '' || email == 'undefined') {
                    $('.generic-errors').html('Email field required.');
                    return false;
                }

                if(!$('#terms_conditions').prop('checked')){
                    $('.generic-errors').html('The terms conditions must be accepted.');
                    return false;
                }
            });

      </script>
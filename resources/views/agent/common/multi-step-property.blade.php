<!-- Core -->
  <script src="/assets/vendor/jquery/dist/jquery.min.js"></script>  
 
  <script src="/assets/vendor/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
  <script src="/assets/vendor/js-cookie/js.cookie.js"></script>
  <script src="/assets/vendor/jquery.scrollbar/jquery.scrollbar.min.js"></script>
  <script src="/assets/vendor/jquery-scroll-lock/dist/jquery-scrollLock.min.js"></script>
  <script src="/assets/js/main.min.js"></script>
  <script src="/assets/js/image-uploader.min.js"></script>

  <script>

     //Add Row
         $('#addRow').on('click',function(){
            const rowData = '<tr><td><input type="text" id="additional-feature" class="form-control"></td><td><button class="btn btn-danger removeRow" type="button""><i class="fa fa-times"></button></td><tr>';
            $('tbody').append(rowData);
        });
        $(document).on( 'click', '.removeRow', function(){                    
            $(this).parents('tr').remove();
        })
      const togglePassword = document.querySelector('#togglePasswordEye');
      const password = document.querySelector('#adminPassword');

      adminPassword.addEventListener("focus", function(i) {
        $('#togglePasswordEye').show();
      });

      togglePassword.addEventListener('click', function (e) {
          // toggle the type attribute
          const type = password.getAttribute('type') === 'password' ? 'text' : 'password';
          password.setAttribute('type', type);
          // toggle the eye slash icon
          this.classList.toggle('fa-eye-slash');
      });
    
      /*  ==========================================
          SHOW UPLOADED IMAGE
      * ========================================== */
      function readURL(input) {
          if (input.files && input.files[0]) {
              var reader = new FileReader();

              reader.onload = function (e) {
                  $('#imageResult')
                      .attr('src', e.target.result);
              };
              reader.readAsDataURL(input.files[0]);
          }
      }

      $(function () {
          $('#upload').on('change', function () {
              readURL(input);
          });
      });

  </script>
  <script src="/assets/js/intlTelInput.js"></script>
  <script src="/assets/js/multistep-form.js"></script>
 <script type="text/javascript">
      $(document).ready(function() {

    	  $("#price").change(function(){ 
				var htmlpropertyPrice = '';
				var htmlPrice = '';
            if($(this).val() == 3){
            	htmlpropertyPrice += '<input id="priceSqft" class="form-control form-control-user required" type="number" min="0" name="priceSqft" placeholder="Enter Property Price" value="" disabled>';
          	  /*htmlPrice = '<select id="price" class="form-control required"  name="price" disabled> <option value="">Select Price</option>';*/
				$("#priceSqft").replaceWith(htmlpropertyPrice);
				/*$("#price").replaceWith(htmlPrice);	*/
               }else{
            	   htmlpropertyPrice += '<input id="priceSqft" class="form-control form-control-user required" type="number" min="0" name="priceSqft" placeholder="Enter Property Price" value="">';
              	 /*htmlPrice = '<select id="price" class="form-control required"  name="price"> <option value="">Select Price</option> @foreach ($prices as $price) <option value="{{$price->id}}">{{$price->price}}</option> @endforeach </select>';*/
  					$("#priceSqft").replaceWith(htmlpropertyPrice);
  					/*$("#price").replaceWith(htmlPrice);	*/
  	                 } 
            
        });
    	  
        $(".btn-success-feature").click(function(){ 
            var html = `<tr class = "control-group-feature">
                <td>
                <label>Additional Feature</label>
              <input class="form-control form-control-user " type="text" maxLength="100" name="additional_feature[]" placeholder="Enter Feature" value="">
              </td>
              <td>
              <button class="btn btn-primary  btn-danger btn-danger-feature" type="button">
				<i class="glyphicon glyphicon-remove"></i> Remove
			</button>
              </td>
                </tr>`;
			
                tableBody = $("table tbody");     
            	tableBody.append(html);
        });

        $("body").on("click",".btn-danger-feature",function(){ 
            $(this).parents(".control-group-feature").remove();
        });

        $(".btn-success").click(function(){ 
            var html = $(".clone").html();
            $(".increment").after(html);
        });

        $("body").on("click",".btn-danger",function(){ 
            $(this).parents(".control-group").remove();
        });

        $("#countryId").change(function(){
        var countryId = $(this).val();
        $('#loading-image').show();
        $.ajax({
          url: "/agent/getState",
          type: "POST",
          data: {countryId: countryId, _token: '{{csrf_token()}}' },
          dataType: 'json',
          success : function(data){
            var html = `<option value="">Select State</option>`;
            for (var i = 0; i < data.length; i++) {
                var id = data[i].id;
                var name = data[i].state_name;
                var option = `<option value="${data[i].id}">${data[i].state_name}</option>`;
                html += option;
              }
            $('#loading-image').hide();		
              $('#stateId').html(html); 
            }
          });
        $('#loading-image').show();
        $.ajax({
			url: "/agent/getCity",
			type: "POST",
			data: {countryId: countryId  ,_token: '{{csrf_token()}}' },
			dataType: 'json',
			success : function(data){
				var html = `<option value="">Select City</option>`;
				for (var i = 0; i < data.length; i++) {
					  var id = data[i].id;
					  var name = data[i].city_name;
					  var option = `<option value="${data[i].id}">${data[i].city_name}</option>`;
					  html += option;
					}
				$('#loading-image').hide();
					$('#cityId').html(html);	
				}
			});
      });
        $("#stateId").change(function(){
        var stateId = $(this).val();
        $('#loading-image').show();
        $.ajax({
          url: "/agent/getCity",
          type: "POST",
          data: {stateId: stateId, _token: '{{csrf_token()}}' },
          dataType: 'json',
          success : function(data){
            var html = `<option value="">Select City</option>`;
            for (var i = 0; i < data.length; i++) {
                var id = data[i].id;
                var name = data[i].city_name;
                var option = `<option value="${data[i].id}">${data[i].city_name}</option>`;
                html += option;
              }
            $('#loading-image').hide();	
              $('#cityId').html(html);  
            }
          });
      });

        $("#toppropertyType").change(function(){
            var propertyId = $(this).val();
            $('#loading-image').show();
            $.ajax({
              url: "/agent/top_property_type/getSubType",
              type: "POST",
              data: {propertyId: propertyId, _token: '{{csrf_token()}}' },
              dataType: 'json',
              success : function(data){
                var html = `<option value="">Select Sub Property Type</option>`;
                for (var i = 0; i < data.length; i++) {
                    var id = data[i].id;
                    var name = data[i].property_type;
                    var option = `<option value="${data[i].id}">${data[i].property_type}</option>`;
                    html += option;
                  }
                $('#loading-image').hide();	
                  $('#propetyType').html(html);  
                }
              });
          });  

      });

      $('.next3').click(function() {

    	  var propertyType = $('#toppropertyType :selected').text();
          var subpropertyType = $('#propetyType :selected').text();
          var country = $('#countryId :selected').text();
          var city = $('#cityId :selected').text();
		  var price = $('#priceSqft').val();
			var noofbedrooms = $('#bedRoom :selected').text();
			var noofbathrooms = $('#bathRoom :selected').text();
			var comment = $('#comment').val();

		var html = `<table class="table table-border" id="feature_table">
        <tr><th>Property Type</th><td>${propertyType}</td><th>Sub Property Type</th><td>${subpropertyType}</td></tr>
        <tr><th>Country</th><td>${country}</th><th>City</th><td>${city}</td></tr>
        <tr><th>Price</th><td>${price}</td><th>No. of Bedrooms</th><td>${noofbedrooms}BHK</td></tr>
        <tr><th>No. of Bathrooms</th><td>${noofbathrooms}</td>
      </table>`;

      
      $('#feature_table').html(html);
      $('#previewPrice').html(price);
      $('#previewcomment').html(comment);
      
      
          });
</script>
</body>
</html>
@extends('agent.common.default')
<head>
    <link rel="stylesheet" href="/assets/css/chat-box.css">
</head>
@section('title', 'Enquiry')
@section('content')
<!-- Header -->
<div class="header pb-6 d-flex align-items-center" style="min-height: 100px;">
</div>
<!-- Page content -->
<div class="container-fluid mt--6">
    <div class="row">
        <div class="col-xl-12">
            <form>
                <div class="card">
                    <div class="card-header">
                        <div class="row align-items-center">
                            <div class="col-4">
                                <h3 class="mb-0">Enquiry Details</h3>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <div class="mesgs">
                                <div class="productDetails" id="productDetails">
                                </div>
                                <div class="product" id="product">
                                </div>
                                <div class="msg_history" id="msgdata">
                                </div>
                                <div class="type_msg" id="bottom_line">
                                    <div class="input_msg_write">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <input type="hidden" id="enquiryId" value="{{$data['enquiry']['id']}}" />
                <input type="hidden" id="category" value="{{ucfirst($data['enquiry']['category'])}}" />
            </form>
        </div>
    </div>

    <script src="/assets/js/jquery.js"></script>
    <script>
    $(document).ready(function() {
        var category = $('#category').val();
        var enquiryId = $('#enquiryId').val();
        var queryType = 'enquiry';
        showMessages();

        // $('#message').on('click',function(){
        $(document).on('click', '#message', function() {
            var message = $('.write_msg').val();
            var category = $('#categoryMessage').val();
            var enquiryId = $('#enquiryIdMessage').val();
            var productId = $('#productIdMessage').val();
            var agentUserId = $('#agentUserIdMessage').val();
            var userId = $('#userIdMessage').val();
            var agentId = $('#agentIdMessage').val();
            $.ajax({
                url: "/saveMessages",
                type: "POST",
                data: {
                    enquiryId: enquiryId,
                    category: category,
                    queryType: queryType,
                    productId: productId,
                    agentUserId: agentUserId,
                    userId: userId,
                    agentId: agentId,
                    message: message,
                    _token: '{{csrf_token()}}'
                },
                dataType: 'json',
                success: function(data) {
                    // Render Chat HTML
                    renderMessages(data);

                }
            });

        });
    });




    function showMessages() {
        var category = $('#category').val();
        var enquiryId = $('#enquiryId').val();
        var queryType = 'enquiry';
        $.ajax({
            url: "/getMessages",
            type: "post",
            data: {
                enquiryId: enquiryId,
                queryType: queryType,
                category: category,
                _token: '{{csrf_token()}}'
            },
            dataType: 'json',
            success: function(data) {
                // Render Chat HTML
                renderMessages(data);
            }
        });



    }

    function renderMessages(data) {
        var category = $('#category').val();
        var enquiryId = $('#enquiryId').val();
        var queryType = 'enquiry';
        var productId = $('#productIdMessage').val();
        var agentUserId = $('#agentUserIdMessage').val();
        var userId = $('#userIdMessage').val();
        var agentId = $('#agentIdMessage').val();

        var message = data.message;
        var images = data.images;

        var d = new Date(message[0].agencycreate).getFullYear();

        var productHtml = `
                          <input type="hidden" name="category" id="categoryMessage" value="${message[0].category}">
                          <input type="hidden" name="queryType" id="queryTypeMessage" value="${queryType}">
                          <input type="hidden" name="enquiryId" id="enquiryIdMessage" value="${message[0].enquiryId}">
                          <input type="hidden" name="productId" id="productIdMessage" value="${message[0].productId}">
                          <input type="hidden" name="agentUserId" id="agentUserIdMessage" value="${message[0].agentUserId}">
                          <input type="hidden" name="userId" id="userIdMessage" value="${message[0].userId}">
                          <input type="hidden" name="agentId" id="agentIdMessage" value="${message[0].agent_id}">

                          <div class="dealerName">
                      <div class="dealerimg mr-1">
                      <img src="/uploads/${message[0].countryflag}" width="40px" height="40px" alt="">
                    </div>
                    <div class="delerTitle">
                      <p class="mb-0">${message[0].company_name}</p>
                      <p>Trusted Seller since ${d}</p>
                    </div>
                  </div>`;

        var product = ``;
        for (var i = 0; i < images.length; i++) {
            if (message[0].productId == images[i].listing_id) {
                product = `<div><img src="/uploads/${images[i].filename}" width="40px" height="40px"> </div>
                  <div class="pl-2">
                  <p class="mb-0"> <strong>${message[0].ad_title}</strong></p>
                  <p></p>
                		</div>
                	<div class="pl-2">
                		<h5>${data.productPrice}</h5>
                		
                	</div>`;
            }
        }

        var outgoingHtml = ``;
        if (data.outgoingEnquiryMessage != null) {
            console.log("inside first if");
            for (var i = 0; i < data.outgoingEnquiryMessage
                .length; i++) {

                if (data.outgoingEnquiryMessage[i].messagefrom == data
                    .loggedInUser) {

                    if (data.outgoingEnquiryMessage[i].queryType ==
                        'enquiry') {
                        outgoingHtml += `<div class="outgoing_msg" >
                          <div class="sent_msg">
                          <div class="yourOffer">
                          <p>${data.outgoingEnquiryMessage[i].message}</p></div>
                        <span class="time_date">${data.outgoingEnquiryMessage[i].time}</span></div></div>`;
                    } else if (data.outgoingEnquiryMessage[i]
                        .queryType == 'submitprice') {
                        outgoingHtml += `<div class="outgoing_msg">
                                  <div class="sent_msg">
                                    <div class="yourOffer">
                                      <table class="w-100">
                                        <tr><td colspan="2">You have ordered the following item:</td></tr>
                                        <tr><td>Item Price</td><td>${data.outgoingEnquiryMessage[i].message}</td></tr>
                                        <tr><td>Shipping Cost</td><td> 0.00</td></tr>
                                        <tr><td>Total Price</td><td>${data.outgoingEnquiryMessage[i].message}</td></tr>
                                      </table>
                                    </div>                        
                                    <span class="time_date">${data.outgoingEnquiryMessage[i].time}</span> </div>
                                </div>`;
                    } else if (data.outgoingEnquiryMessage[i]
                        .queryType == 'counterOffer') {
                        outgoingHtml += `<div class="outgoing_msg">
                                      <div class="sent_msg">
                                        <div class="yourOffer">
                                          <table class="w-100">
                                            <tr><td colspan="2">You have ordered the following item:</td></tr>
                                            <tr><td>Item Price</td><td>${data.outgoingEnquiryMessage[i].message}</td></tr>
                                            <tr><td>Shipping Cost</td><td> ${data.outgoingEnquiryMessage[i].shippingCost}</td></tr>
                                            <tr><td>Total Price</td><td>${data.outgoingEnquiryMessage[i].message}</td></tr>
                                          </table>
                                        </div>                        
                                        <span class="time_date">${data.outgoingEnquiryMessage[i].time}</span> </div>
                                    </div>`;
                    }
                } else {
                    console.log("inside else ------- ", data
                        .outgoingEnquiryMessage[i].queryType);
                    if (data.outgoingEnquiryMessage[i].queryType ==
                        'enquiry') {
                        outgoingHtml += `<div class="incoming_msg">
                          	<div class="received_msg">
                              <div class="received_withd_msg">
                                <p>${data.outgoingEnquiryMessage[i].message}</p>
                                <span class="time_date">${data.outgoingEnquiryMessage[i].time}</span></div>
                            </div>
                            </div>`;
                    } else if (data.outgoingEnquiryMessage[i]
                        .queryType == 'submitprice') {
                        $('#productDetails').html(productHtml);
                        outgoingHtml += `<div class="outgoing_msg">
                                  <div class="received_msg">
                                    <div class="received_withd_msg">
                                      <table class="w-100">
                                        <tr><td colspan="2">You have ordered the following item:</td></tr>
                                        <tr><td>Item Price</td><td>${data.outgoingEnquiryMessage[i].message}</td></tr>
                                        <tr><td>Shipping Cost</td><td> 0.00</td></tr>
                                        <tr><td>Total Price</td><td>${data.outgoingEnquiryMessage[i].message}</td></tr>
                                      </table>
                                    </div>                        
                                    <span class="time_date">${data.outgoingEnquiryMessage[i].time}</span> </div>
                                    <form id="acceptoffer" method="POST" action="{{ url('/seller/acceptoffer') }}">
                                    @csrf
                                    <input type="hidden" name="category" value="${message[0].category}">
                                    <input type="hidden" name="enquiryId" value="${message[0].enquiryId}">
                                    <input type="hidden" name="queryType" value= 'submitprice' />
                                    <input type="hidden" name="messageId" value=${data.outgoingEnquiryMessage[i].messageId} />
                                    <div class="viewOrder w-50">
                                    <button class="btn btn-success w-100"  type="submit" value="">Accept Offer</button>
                                  </div>
                                  </form>
                                </div>`;
                    } else if (data.outgoingEnquiryMessage[i]
                        .queryType == 'counterOffer' && data
                        .outgoingEnquiryMessage[i].messageFinalFrom ==
                        'seller') {
                        $('#productDetails').html(productHtml);
                        outgoingHtml += `<div class="outgoing_msg">
                                      <div class="received_msg">
                                        <div class="received_withd_msg">
                                          <table class="w-100">
                                            <tr><td colspan="2">You have ordered the following item:</td></tr>
                                            <tr><td>Item Price</td><td>${data.outgoingEnquiryMessage[i].message}</td></tr>
                                            <tr><td>Shipping Cost</td><td> ${data.outgoingEnquiryMessage[i].shippingCost}</td></tr>
                                            <tr><td>Total Price</td><td>${data.outgoingEnquiryMessage[i].totalPrice}</td></tr>
                                          </table>
                                        </div>                        
                                        <span class="time_date">${data.outgoingEnquiryMessage[i].time}</span> </div>
                                        <form id="acceptoffer" method="POST" action="{{ url('/buyer/checkout') }}">
                                        @csrf
                                        <input type="hidden" name="enquiryId" value="${message[0].enquiryId}">
                                        <input type="hidden" name="queryType" value='counterOffer' />
                                        <input type="hidden" name="category" value="${message[0].category}">
                                        <input type="hidden" name="messageId" value=${data.outgoingEnquiryMessage[i].messageId} />
                                        <div class="viewOrder w-50">
                                        <button class="btn btn-success w-100"  type="submit" value="">Accept Offer</button>
                                      </div>
                                      </form>
                                    </div>`;
                    } else if (data.outgoingEnquiryMessage[i]
                        .queryType == 'counterOffer' && data
                        .outgoingEnquiryMessage[i].messageFinalFrom ==
                        'buyer') {
                        console.log(
                            '------------------------------------',
                            data.outgoingEnquiryMessage[i]
                            .shippingCost);
                        $('#productDetails').html(productHtml);
                        outgoingHtml += `<div class="outgoing_msg">
                                          <div class="received_msg">
                                            <div class="received_withd_msg">
                                              <table class="w-100">
                                                <tr><td colspan="2">You have ordered the following item:</td></tr>
                                                <tr><td>Item Price</td><td>${data.outgoingEnquiryMessage[i].message}</td></tr>
                                                <tr><td>Shipping Cost</td><td> ${data.outgoingEnquiryMessage[i].shippingCost}</td></tr>
                                                <tr><td>Total Price</td><td>${data.outgoingEnquiryMessage[i].totalPrice}</td></tr>
                                              </table>
                                            </div>                        
                                            <span class="time_date">${data.outgoingEnquiryMessage[i].time}</span> </div>
                                            <form id="acceptoffer" method="POST" action="{{ url('/seller/acceptoffer') }}">
                                            @csrf
                                            <input type="hidden" name="enquiryId" value=${enquiryId} />
                                            <input type="hidden" name="queryType" value='counterOffer' />
                                            <input type="hidden" name="category" value=${category} />
                                            <input type="hidden" name="messageId" value=${data.outgoingEnquiryMessage[i].messageId} />
                                            <div class="viewOrder w-50">
                                            <button class="btn btn-success w-100"  type="submit" value="">Accept Offer</button>
                                          </div>
                                          </form>
                                        </div>`;
                    }

                }
                if (message[0].agentUserId == data.loggedInUser) {

                    var bottom_line = `<div class="input_msg_write">
                              <input type="text" class="write_msg" placeholder="Type a message"/>
                              <button class="msg_send_btn" type="button" id="message"><i class="fa fa-paper-plane-o" aria-hidden="true"></i></button>
                            </div>`;

                } else {
                    var bottom_line = `<div class="input_msg_write">
                                  <input type="text" class="write_msg" placeholder="Type a message"/>
                                  <button class="msg_send_btn" type="button" id="message"><i class="fa fa-paper-plane-o" aria-hidden="true"></i></button>
                                </div>`;
                }
            }

        }
        $('#productDetails').html(productHtml);
        $('#product').html(product);
        $('#msgdata').html(outgoingHtml);
        $('#bottom_line').html(bottom_line);

    }
    </script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
        integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous">
    </script>
    <script src="/assets/js/bootstrap.min.js"></script>
    <script src="/assets/js/wow.min.js"></script>
    <script src="/assets/js/slick.min.js"></script>
    <script src="/assets/js/intlTelInput.min.js"></script>
    <script src="/assets/js/local.js"></script>
    @stop
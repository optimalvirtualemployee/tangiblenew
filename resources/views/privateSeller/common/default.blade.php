@include('privateSeller.common.header')
<body>
@include('privateSeller.common.sidebar')
  <!--Main Navigation-->
    <!--Main layout-->
    @yield('content')
  <!--Main layout-->
@include('privateSeller.common.footer')
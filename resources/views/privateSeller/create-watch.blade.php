<!DOCTYPE html>
<html lang="en">
  @include('privateSeller.headheader')
  <style>
    .slick-slide img {
    max-height: 300px;
    width: auto;
    }
    #imgGallery div, #dmgGallery div {
    position: relative; 
    width: 120px;
    height: 120px;
    margin: 5px;
    border: 1px solid #e7e7e7;
    }
    #imgGallery div img, #dmgGallery div img {
    max-height: 150px;
    width: 100%;
    height: 100%;
    object-fit: cover;
    position: absolute;
    }
    .avatar-upload {
  position: relative;
  max-width: 205px;
  border: 2px dotted #bfc8d9;
}
.avatar-upload .avatar-edit {
  position: absolute;
  right: 12px;
  z-index: 1;
  top: 10px;
}
.avatar-upload .avatar-edit input {
  display: none;
}
.avatar-upload .avatar-edit input + label {
  display: inline-block;
  width: 34px;
  height: 34px;
  margin-bottom: 0;
  border-radius: 100%;
  background: #FFFFFF;
  border: 1px solid transparent;
  box-shadow: 0px 2px 4px 0px rgba(0, 0, 0, 0.12);
  cursor: pointer;
  font-weight: normal;
  transition: all 0.2s ease-in-out;
  display: flex;
  align-items: center;
  justify-content: center;
}
.avatar-upload .avatar-edit input + label:hover {
  background: #f1f1f1;
  border-color: #d6d6d6;
}
.avatar-upload .avatar-edit input + label:after {
  content: "\f040";
  font-family: 'FontAwesome';
  color: #757575;
  text-align: center;
}
.avatar-upload .avatar-preview {
  width: 192px;
  height: 192px;
  position: relative;
  border-radius: 100%;
  border: 6px solid #F8F8F8;
  box-shadow: 0px 2px 4px 0px rgba(0, 0, 0, 0.1);
  margin: 0 auto;
}
.avatar-upload .avatar-preview > div {
  width: 100%;
  height: 100%;
  border-radius: 100%;
  background-size: cover;
  background-repeat: no-repeat;
  background-position: center;
}
.security-photos{
  background-color: #fcfcfc;
    padding: 10px;
}
.previewContainer{
  background-color: #fff;
    box-shadow: 1px 1px 1px 1px #80808040;
    padding: 25px;
}
#loading-image{
  position:fixed;
  top:0px;
  right:0px;
  width:100%;
  height:100%;
  background-color:#666;
  background-image:url('/assets/img/Spinner-loader.gif');
  background-repeat:no-repeat;
  background-position:center;
  z-index:10000000;
  opacity:0.6;
}

.file-input{
  position:relative;
}

.file-input .clear-btn{
  display: none;
  position: absolute;
  top: -6px;
  right: -20px;
  font-weight: bold;
  font-size: 1.4em;
  padding: 0 0.2em;
  line-height: 1em;
  cursor: pointer;
  z-index: 1;
}

.file-input:hover > .clear-btn {
  display: block;
}
  </style>
  <body>
    @include('privateSeller.innerheader')
    
    
    <section class="become-seller-from overlay-wrapper">
    
    @if (Route::has('login'))
    @auth
    <h1 style="color:white;"> Tada Tdadskldjfshfa afhasfjklhsfdjsah</h1>
      <div class="container">
      <div class="row">
      <div class="col-lg-12">
      <div class="">
      <div id='loading-image' style='display:none'>
				</div>
        <!-- MultiStep Form -->
        <div class="container-fluid" id="grad1">
          <div class="row justify-content-center mt-0">
            <div class="col-11  col-lg-12 text-center p-0 mt-3 mb-2">
              <div class="card px-0 pt-4 pb-0 mt-3 mb-3">
                <!-- <h2><strong>Sign Up Your User Account</strong></h2>
                  <p>Fill all form field to go to next step</p> -->
                <div class="row">
                  <div class="col-md-12 mx-0">
                    <form id="msform" action="{{ url('privateSellerWatchPayments') }}" method="post" enctype="multipart/form-data">
                    @csrf
                    <input type="hidden" name="agentId" id="agentId" value="{{Auth::user()->id}}">
                    <input type="hidden" name="packageId" id="packageId" value="{{$packageId}}">
                      <!-- progressbar -->
                      <ul id="progressbar">
                        <li class="active" id="watch"><strong>Select Watch</strong></li>
                        <li id="personal"><strong>Add Details</strong></li>
                        <li id="payment"><strong>Attach Photos</strong></li>
                        <li id="personalInfo"><strong>Personal Information</strong></li>
                        <li id="confirm"><strong>Summary</strong></li>
                      </ul>
                      <!-- fieldsets -->
                      <fieldset>

                      @foreach($errors->all() as $error)
                            <li>{{$error}}</li>
                            @endforeach


                        <div class="form-card">
                          <p><b>What type of Watch you Selling?</b></p>
                          <div class="row">
                            <div class="col-6">
                              <select class=" form-control" id="brand"
                                name="brand">
                                <option value="">Make</option>
                                @foreach($brands as $brand)
                                       <option value="{{$brand->id}}">{{$brand->watch_brand_name}}</option>
                                            @endforeach
                              </select>
                            </div>
                            <div class="col-6">
                              <select class=" form-control" id="model"
                                name="model">
                                <option value="">Model</option>
                                                             
                              </select>
                            </div>                            
                          </div>
                        </div>
                        <input type="button" name="next" class="next action-button" id ="next1" value="Next Step" />
                      </fieldset>
                      <fieldset>
                        <div class="form-card">
                          <div class="">
                            <h2 class="fs-title">Watch Details</h2>
                            <hr class="my-2">
                            <div class="row">
                              <div class="col-lg-12">
                                <div class="row m-0">
                                
                                  
                                  <div class="col-lg-4">
                                    <div class="form-group">
                                      <label class="form-control-label"
                                        for="input-last-name">Country*</label>
                                      <select class="form-control" data-trigger name="countryId" id="countryId">
                                                    <option  value="">Select Country</option>
                                                    @foreach($country_data as $country)
                                            <option value="{{$country->id}}">{{$country->country_name}}</option>
                                            @endforeach
                                                  </select>
                                    </div>
                                  </div>
                                  <div class="col-lg-4">
                                    <div class="form-group">
                                      <label class="form-control-label"
                                        for="input-last-name">State</label>
                                      <select class="form-control" data-trigger name="stateId" id="stateId">
                                                    <option  value="">Select State</option>
                                                  </select>
                                    </div>
                                  </div>
                                  <div class="col-lg-4">
                                    <div class="form-group">
                                      <label class="form-control-label"
                                        for="input-last-name">City*</label>
                                      <select class="form-control" data-trigger name="cityId" id="cityId">
                                                    <option  value="">Select City</option>
                                                  </select>
                                    </div>
                                  </div>
                                  <!--<div class="col-lg-4">
                                    <div class="form-group">
                                      <label>Price On Request</label>
                                      <select id="price_on_request" class="form-control required" name="price_on_request">
                        					<option value="">Select Price On Request</option>
                        					<option value="0">No</option>
                        					<option value="1">Yes</option>
                      						</select>
                                    </div>
                                  </div>-->
                                  
                                  <div class="col-lg-4">
                                    <div class="form-group">
                                      <label class="form-control-label"
                                        for="input-last-name">Case Diameter</label>
                                      <select id="case_diameter" class="form-control"  name="case_diameter">
						<option value="">Select Case Diameter</option>
						@foreach ($case_diameters as $case_diameter)
                           <option value="{{$case_diameter->id}}">{{$case_diameter->case_diameter}}</option>
                         @endforeach
                      </select>
                                    </div>
                                  </div>
                                  
                                  <div class="col-lg-4">
                                    <div class="form-group">
                                      <label class="form-control-label"
                                        for="input-last-name">Movement</label>
                                      <select id="movement" class="form-control"  name="movement">
						<option value="">Select Movement</option>
						@foreach ($movements as $movement)
                           <option value="{{$movement->id}}">{{$movement->movement}}</option>
                         @endforeach
                         </select>
                                    </div>
                                  </div>
                                  <div class="col-lg-4">
                                    <div class="form-group">
                                      <label class="form-control-label"
                                        for="input-last-name">Watch Type</label>
                                      <select id="type" class="form-control"  name="type">
						<option value="">Select Watch Type</option>
						@foreach ($types as $type)
                           <option value="{{$type->id}}">{{$type->watch_type}}</option>
                         @endforeach
                         </select>
                                    </div>
                                  </div>
                                  <div class="col-lg-4">
                                    <div class="form-group">
                                      <label class="form-control-label"
                                        for="input-last-name">Year of Manufacturer</label>
                                      <select id="yom" class="form-control"  name="yom">
						<option value="">Select Year Of Manufacture</option>
						@foreach ($year_of_manufactures as $year_of_manufacture)
                           <option value="{{$year_of_manufacture->id}}">{{$year_of_manufacture->year_of_manufacture}}</option>
                         @endforeach
                         </select>
                                    </div>
                                  </div>
                                  <div class="col-lg-4">
                                    <div class="form-group">
                                      <label class="form-control-label"
                                        for="input-last-name">Gender*</label>
                                      <select id="gender" class="form-control required"  name="gender">
						<option value="">Select Gender</option>
						@foreach ($genders as $gender)
                           <option value="{{$gender->id}}">{{$gender->gender}}</option>
                         @endforeach
                         </select>
                                    </div>
                                  </div>
                                  <div class="col-lg-4">
                                    <div class="form-group">
                                      <label class="form-control-label"
                                        for="input-last-name">Inclusions*</label>
                                      <select id="inclusions" class="form-control"  name="inclusions">
						<option value="">Select Inclusions</option>
						@foreach ($inclusions_data as $inclusion)
                           <option value="{{$inclusion->id}}">{{$inclusion->inclusion}}</option>
                         @endforeach
                         </select>
                                    </div>
                                  </div>
                                  <div class="col-lg-4">
                                    <div class="form-group">
                                      <div class="form-group">
                                        <label class="form-control-label"
                                          for="input-last-name">Reference Number*</label>
                                        <div class="input-group mb-3">                                        
                                          <input type="text" class="form-control" id="referenceNo" name="referenceNo" placeholder="Reference Number">
                                        </div>
                                      </div>
                                  </div>
                                </div>
                                  <div class="col-lg-4">
                                    <div class="form-group">
                                      <label class="form-control-label"
                                        for="input-last-name">Watch Condition*</label>
                                      <select class="form-control" data-trigger name="watchCondition" id="watchCondition">
                                        <option value="">Select Condition</option>
                                        <option value="New">New</option>
                                        <option value="Used">Used</option>                                        
                                      </select>
                                    </div>
                                  </div>                                 
                                  <!-- <div class="col-lg-4">
                                    <div class="form-group">
                                     <label class="form-control-label" for="input-last-name">Status Of Listing*</label>
                                                        <select id ="status" name="status" class="form-control" >
                          								<option value="" >Select Status</option>
                           								<option value="1" >Active</option>
                           								<option value="0" >Inactive</option>
                        								</select>
                                    </div>
                                  </div> -->
                                  <div class="col-lg-4">
                                    <div class="form-group">
                                                        <label class="form-control-label" for="input-last-name">Timezone*</label>
                                                        <select class="form-control" data-trigger name="timezone" id="timezone">
                                                            <option value="">Select Timezone</option>
                                                            @foreach($timezone as $zone)
                                                            <option value="{{$zone->id}}">{{$zone->name .' '. $zone->offset}}</option>
                                                            @endforeach
                                                          </select>
                                                    </div>
                                  </div>
                                </div>
                                <h2 class="fs-title mt-5">Additional Info</h2>
                                <hr class="my-2">
                                <div class="row m-0">
                                                                 
                                </div>
                                <div class="row m-0">
                                <div class="col-lg-4">
                                    <div class="form-group">
                                      <label class="form-control-label"
                                        for="input-last-name">Power Reserve </label>
                                      <select id="powerReserve" class="form-control"  name="powerReserve">
						<option value="">Select Power Reserve</option>
						@foreach ($power_reserve as $power_reserve)
                           <option value="{{$power_reserve->id}}">{{$power_reserve->power_reserve}}</option>
                         @endforeach
                      </select>
                                    </div>
                                  </div>
                                  <div class="col-lg-4">                                   
                                    <div class="form-group">
                                      <label class="form-control-label"
                                        for="input-last-name">Dial Color</label>
                                        <select id="dialColor" class="form-control"  name="dialColor" >
						<option value="">Select Dial Color</option>
						@foreach ($dial_color as $dial_color)
                           <option value="{{$dial_color->id}}">{{$dial_color->dial_color}}</option>
                         @endforeach
                      </select>                                       
                                    </div>
                                  </div>
                                  <div class="col-lg-4">
                                    <div class="form-group">
                                      <label class="form-control-label"
                                        for="input-last-name">Bracelet Material</label>
                                      <select id="braceletMaterial" class="form-control"  name="braceletMaterial">
						<option value="">Select Bracelet Material</option>
						@foreach ($bracelet_material as $bracelet_material)
                           <option value="{{$bracelet_material->id}}">{{$bracelet_material->bracelet_material}}</option>
                         @endforeach
                      </select>
                                    </div>
                                  </div>

                                  <div class="col-lg-4">
                                    <div class="form-group">
                                      <label class="form-control-label"
                                        for="input-last-name">Bezel Material</label>
                                      <select id="bezelMaterial" class="form-control"  name="bezelMaterial" >
						<option value="">Select Bezel Material</option>
						@foreach ($bezel_material as $bezel_material)
                           <option value="{{$bezel_material->id}}">{{$bezel_material->bezel_material}}</option>
                         @endforeach
                      </select>
                                    </div>
                                  </div>                                  
                                  <div class="col-lg-4">                                   
                                    <div class="form-group">
                                      <label class="form-control-label"
                                        for="input-last-name">Bracelet Color</label>
                                        <select id="braceletColor" class="form-control"  name="braceletColor">
						<option value="">Select Bracelet Color</option>
						@foreach ($bracelet_color as $bracelet_color)
                           <option value="{{$bracelet_color->id}}">{{$bracelet_color->bracelet_color}}</option>
                         @endforeach
                      </select>                                      
                                    </div>
                                  </div>    
                                  <div class="col-lg-4">
                                    <div class="form-group">
                                      <label class="form-control-label"
                                        for="input-last-name">Type of Clasp</label>
                                      <select id="typeOfClasp" class="form-control"  name="typeOfClasp">
						<option value="">Select Type Of Clasp</option>
						@foreach ($type_of_clasp as $type_of_clasp)
                           <option value="{{$type_of_clasp->id}}">{{$type_of_clasp->type_of_clasp}}</option>
                         @endforeach
                      </select>
                                    </div>
                                  </div>                           
                                </div>
                                <div class="row m-0">                                
                                  <div class="col-lg-4">                                   
                                    <div class="form-group">
                                      <label class="form-control-label"
                                        for="input-last-name">Clasp Material</label>
                                        <select id="claspMaterial" class="form-control"  name="claspMaterial">
						<option value="">Select Clasp Material</option>
						@foreach ($clasp_material as $clasp_material)
                           <option value="{{$clasp_material->id}}">{{$clasp_material->clasp_material}}</option>
                         @endforeach
                      </select>                                      
                                    </div>
                                  </div>
                                  <div class="col-lg-4">                                   
                                    <div class="form-group">
                                      <label class="form-control-label"
                                        for="input-last-name">Case Material</label>
                                        <select id="caseMaterial" class="form-control"  name="caseMaterial">
						<option value="">Select Case Material</option>
						@foreach ($case_material as $case_material)
                           <option value="{{$case_material->id}}">{{$case_material->case_material}}</option>
                         @endforeach
                      </select>                                      
                                    </div>
                                  </div> 

                                  <div class="col-lg-4">                                   
                                    <div class="form-group">
                                      <label class="form-control-label"
                                        for="input-last-name">Case MM</label>
                                        <select id="caseMM" class="form-control"  name="caseMM">
						<option value="">Select Case MM</option>
						@foreach ($case_mm as $case_mm)
                           <option value="{{$case_mm->id}}">{{$case_mm->case_mm}}</option>
                         @endforeach
                      </select>                                       
                                    </div>
                                  </div>   
                                  <div class="col-lg-4">                                   
                                    <div class="form-group">
                                      <label class="form-control-label"
                                        for="input-last-name">Water Resistance Depth</label>
                                        <select id="wrd" class="form-control"  name="wrd">
						<option value="">Select Water Resistance Depth</option>
						@foreach ($wrd as $wrd)
                           <option value="{{$wrd->id}}">{{$wrd->water_resistant_depth}}</option>
                         @endforeach
                      </select>                                      
                                    </div>
                                  </div>  
                                  <div class="col-lg-4">                                   
                                    <div class="form-group">
                                      <label class="form-control-label"
                                        for="input-last-name">Glass Type</label>
                                        <select id="glassType" class="form-control"  name="glassType">
						<option value="">Select Glass Type</option>
						@foreach ($glass_type as $glass_type)
                           <option value="{{$glass_type->id}}">{{$glass_type->glass_type}}</option>
                         @endforeach
                      </select>                                     
                                    </div>
                                  </div>                         
                                </div>
                                <h2 class="fs-title mt-5">Price Details</h2>
                                <hr class="my-2">
                                <div class="row m-0">


                                <div class="col-lg-4">
                                    <div class="form-group">
                                      <label class="form-control-label"
                                        for="input-last-name">Price type*</label>
                                      <select class="form-control" data-trigger name="pricetType" id="pricetType">
                                                      <option value="">Select</option>
                                                      @foreach($prices as $price)
                                                    <option value="{{$price->id}}">{{$price->price}}</option>
                                                    @endforeach
                                                    </select>
                                    </div>
                                  </div>
                                  
                                  <div class="col-lg-4">
                                    <div class="form-group">
                                      <label class="form-control-label"
                                        for="input-last-name">Currency*</label>
                                      <select id="currency" class="form-control required"  name="currency">
                                      <option value="">Select Currency</option>
                                      @foreach ($currency_data as $currency)
                                        <option value="{{$currency->id}}">{{$currency->currency_code}}</option>
                                      @endforeach
                                    </select>
                                    </div>
                                  </div>
                                  <div class="col-lg-4">                                   
                                    <div class="form-group">
                                      <label class="form-control-label"
                                        for="input-last-name">Watch Price*</label>
                                      <input type="number" id="price" name="watch_price" class="form-control" placeholder="Watch Price">                                   
                                    </div>
                                  </div>

                                  <div class="col-lg-4">
                                    <div class="form-group">
                                      <label class="form-control-label" for="input-last-name">Commision</label>
                                      <div class="input-group mb-3">
                                        <input type="text" id="commision" name="commision" class="form-control">
                                      </div>
                                    </div>
                                    <div class="form-group">
                                      <label class="form-control-label" for="input-last-name">Revenue</label>
                                      <div class="input-group mb-3">
                                        <input type="text" id="revenue" name="revenue" class="form-control">
                                      </div>
                                    </div>
                                  </div>
                                </div>  
                                <h2 class="fs-title mt-5"> Additional information (optional) </h2>
                                <hr class="my-2">                                                    
                                <div class="accordion-1">
                                  <div class="container">
                                    <div class="row">
                                      <div class="col-md-12 ml-auto">
                                        <div class="accordion my-3"
                                          id="accordionExample">
                                          <div class="card p-0 mb-1">
                                            <div class="card-header" id="headingOne">
                                              <h5 class="mb-0">
                                                <button class="btn btn-link w-100 text-primary text-left" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" 
                                                aria-controls="collapseOne"> Features <i class="ni ni-bold-down float-right"></i>
                                              </button>
                                              </h5>
                                            </div>
                                            <div id="collapseOne"
                                              class="collapse show"
                                              aria-labelledby="headingOne"
                                              data-parent="#accordionExample">
                                              <div class="mt-3">
                                                <div class="row">
                                                  <div class="col-lg-6">
                                                    <div class="inner">
                                                    <?php echo $html1;?>
                                                    </div>
                                                    </div>
                                                    <div class="col-lg-6">
                                                    <div class="inner">
                                                    <?php echo $html2;?>
                                                    </div>
                                                    </div>
                                                    </div>
                                                                            
                                              </div>    
                                            </div>
                                          </div>
                                          <div class="card p-0 mb-1">
                                            <div class="card-header"
                                              id="headingTwo">
                                              <h5 class="mb-0">
                                                <button
                                                  class="btn btn-link w-100 text-primary text-left collapsed"
                                                  type="button"
                                                  data-toggle="collapse"
                                                  data-target="#collapseTwo"
                                                  aria-expanded="false"
                                                  aria-controls="collapseTwo">
                                                Additional Features
                                                <i
                                                  class="ni ni-bold-down float-right"></i>
                                                </button>
                                              </h5>
                                            </div>
                                            <div id="collapseTwo" class="collapse"
                                              aria-labelledby="headingTwo"
                                              data-parent="#accordionExample">
                                            
                                             <div class="row">                                             
                                              <div class="col-lg-12">
                                                <div class="additionalFeature">
                                                  <table class="table table-bordered">
                                                    <thead>
                                                      <tr>
                                                        <th>Àdd features
                                                        </th>
                                                        <th>Action</th>
                                                      </tr>
                                                    </thead>
                                                    <tbody>
                                                      <tr>
                                                        <td><input type="text" id="additional-feature" name="additional_feature[]" class="form-control">
                                                        </td>
                                                        <td><button class="btn btn-success" type="button" id="addRow"><i class="fa fa-plus"></i></button>
                                                        </td>
                                                      </tr>
                                                    </tbody>
                                                  </table>
                                                </div>
                                              </div>
                                            </div> 
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                            
                          </div>
                         
                        </div>
                        <div>
                        </div>
                        <input type="button" name="previous"
                          class="previous action-button-previous" value="Previous" />
                        <input type="button" name="next" id="next2" class="next action-button"
                          value="Next Step" />
                      </fieldset>
                      <fieldset>
                        <div class="form-card">
                          <h2 class="fs-title">Attach Photos</h2>
                          <p>Attach your watch photos here, you can drag the photos to the main </p>
                          <div class="row" >
                            <div class="col-lg-12">
                              <div class="inner">
                              
                                <div class="input-field">      
                                  <div class="input-images-1" id="input-images-1" style="padding-top: .5rem;"></div>
                                  <span id="img1" style="color: red"></span>
                                </div>
                                
                              </div>
                            </div>
                          </div>
                          <div class="row mt-5">
                            <hr class="my-2">
                            <div class="col-lg-12">
                              <div class="inner">
                                <h2 class="fs-title">Damage Photos</h2>
                                <p class="muted">Please upload the images of your damaged product</p>
                                <div class="input-field">
                                  <div class="input-images-2" id="damagePhotos" style="padding-top: .5rem;"></div>
                                </div>
                                
                              </div>
                            </div>
                          </div>
                          <div class="row mt-5">   
                            <div class="col-lg-12">
                              <h2 class="fs-title">Security Photos</h2> 
                              <p class="muted">Please upload individual images for the specific time showing below</p> 
                            </div>                         
                            <div class="col-lg-6">                             
                              <div class="inner security-photos">
                                <div class="row">
                                  <div class="col-lg-6">
                                    <div class="secutrity1">
                                      <img src="/assets/img/security-1.png" alt="">
                                    </div>
                                    <input type="hidden" class="digitalTime1" id="security-time1" name="security-time1"/>
                                    <div class="digitalTime digitalTime1">
                                      10:32
                                    </div>
                                  </div>
                                  <div class="col-lg-6">
                                    <div class="uploadImage">
                                      <div class="avatar-upload">
                                        <div class="avatar-edit">
                                            <input type='file' id="security1" name="security1[]" class="imageUploadCls" accept=".png, .jpg, .jpeg" />
                                            <label for="security1"></label>
                                        </div>
                                        <div class="avatar-preview" id="security1-image">
                                            <div class="imagePreview" style="background-image: url(img/watch.png);">
                                            </div>
                                            <span id="img2" style="color:red"></span>
                                        </div>
                                    </div>  
                                    </div>
                                  </div>
                                </div>                     
                                <hr class="my-2">
                              </div>
                            </div>
                            <div class="col-lg-6">                           
                              <div class="inner security-photos">
                                <div class="row">
                                  <div class="col-lg-6">
                                    <div class="secutrity2">
                                      <img src="/assets/img/security-2.png" alt="">
                                    </div>
                                    <input type="hidden" class="digitalTime2" id="security-time2" name="security-time2"/>
                                    <div class="digitalTime digitalTime2">
                                      10:32
                                    </div>
                                  </div>
                                  <div class="col-lg-6">
                                    <div class="uploadImage">
                                      <div class="avatar-upload">
                                        <div class="avatar-edit">
                                            <input type='file' id="security2" name="security2[]" class="imageUploadCls" accept=".png, .jpg, .jpeg" />
                                            <label for="security2"></label>
                                        </div>
                                        <div class="avatar-preview" id="security2-image">
                                          <div class="imagePreview" style="background-image: url(img/watch.png);"></div>
                                           <span id="img3" style="color:red"></span>
                                        </div>
                                    </div>  
                                    </div>
                                  </div>
                                </div>                     
                                <hr class="my-2">
                              </div>
                            </div>
                          </div>
                          <div class="row mt-5">
                            <hr class="my-2">
                            <div class="col-lg-12">
                              <div class="inner">
                                <h2 class="fs-title">Comments</h2>
                                <hr class="my-2">
                                <label>
                                Setting aside the effort to portray special and
                                recognizing highlights about your vehicle can
                                have the effect in catching a purchaser's
                                consideration.
                                </label>
                                <div class="form-group">
                                  <!-- <div
                                    class="custom-control custom-control-alternative custom-checkbox">
                                    <input class="custom-control-input"
                                      id="inspection" type="checkbox">
                                    <label class="custom-control-label"
                                      for="inspection">
                                    <span class="text-muted"> Allow Buyers
                                    to request Inseption </span>
                                    </label>
                                  </div> -->
                                  <textarea rows="4" class="form-control" id="comment" name="comment"
                                    placeholder="Imagine driving on the rough terrain in this 2012 ACE Cycle-Car. 34 km on the clock only. It is exceptional value at $24. 
                                    Only travelled 34 km. Don't let this go at this price!."></textarea>
                                </div>
                              </div>
                            </div>
                          </div>                         
                        </div>
                        <input type="button" name="previous"
                          class="previous action-button-previous" value="Previous" />
                        <input type="button" name="next3" id="next3" class="next action-button"
                          value="Next Step" />
                      </fieldset>   
                      <fieldset>
                        <div class="form-card">
                          <div class="">
                            <h2 class="fs-title">Personal Information</h2>
                            <p class="muted">Please review your personal information.</p>
                            <hr class="my-2">
                            <div class="row">
                              <div class="col-lg-12">
                                <div class="row m-0">
                                  <div class="col-lg-7">
                                    <div class="form-group">
                                      <label class="form-control-label"
                                        for="input-last-name">First Name*</label>
                                      <div class="input-group mb-3">                                        
                                        <input type="text" class="form-control" id="firstName" name="firstName" placeholder="First Name" value="{{Auth::user()->first_name}}">
                                      </div>
                                    </div>
                                  </div>
                                  <div class="col-lg-7">
                                    <div class="form-group">
                                      <label class="form-control-label" for="input-country">Last Name*</label>
                                      <input type="text" class="form-control"  id="lastName" name="lastName" placeholder="Last Name" value="{{Auth::user()->last_name}}">
                                    </div>
                                  </div>
                                  <div class="col-lg-7">
                      <div class="form-group">
                        <label class="form-control-label" for="input-email">Email address</label>
                        <input type="email" id="input-email" name="email" class="form-control" placeholder="Email Address" value="{{Auth::user()->email}}">
                      </div>
                    </div>
                                  <div class="col-lg-6">
                      <div class="form-group">
                        <label class="form-control-label" for="input-number">Phone Number</label>
                        <input type="text" id="input-number" name="phoneNumber" class="form-control" placeholder="Mobile Num" value="{{($userDetails->firstWhere('user_id',Auth::user()->id) != null) ? $userDetails->firstWhere('user_id',Auth::user()->id)->user_mobile : ""}}">
                      </div>
                      @error('phoneNumber')
          				<div class="alert alert-danger">This Field is required</div>
       				 @enderror
                    </div>                     
                                </div>








<!--                                 <h2 class="fs-title mt-5">Bank Details</h2>
                                <hr class="my-2">
                                <div class="row m-0">
                                  <div class="col-lg-6">
                                    <div class="form-group">
                                      <label class="form-control-label"
                                        for="bankName">Bank Name*</label>
                                        <input type="text" class="form-control" id="bankName" name="bankName"  placeholder="Bank Name">
                                    </div>
                                  </div> 


                                  <div class="col-lg-6">
                                    <div class="form-group">
                                      <label class="form-control-label"
                                        for="beneficiaryName">Beneficiary Name*</label>
                                        <input type="text" class="form-control" id="beneficiaryName" name="beneficiaryName"  placeholder="Beneficiary Name">
                                    </div>
                                  </div> 


                                  <div class="col-lg-6">
                                    <div class="form-group">
                                      <label class="form-control-label"
                                        for="bankAccount">Bank Account*</label>
                                        <input type="text" class="form-control" id="bankAccount" name="bankAccount"  placeholder="Bank Account">
                                    </div>
                                  </div> 

                                  <div class="col-lg-6">
                                    <div class="form-group">
                                      <label class="form-control-label"
                                        for="ifsc">IFSC Code*</label>
                                        <input type="text" class="form-control" id="ifsc" name="ifsc"  placeholder="IFSC Code">
                                    </div>
                                  </div> 

                                  </div> -->













                                <h2 class="fs-title mt-5">Address</h2>
                                <hr class="my-2">
                                <div class="row m-0">
                                  <div class="col-lg-6">
                                    <div class="form-group">
                                      <label class="form-control-label"
                                        for="input-last-name">Address*</label>
                                        <input type="text" class="form-control" id="address" name="address"  placeholder="Address" value="{{($userDetails->firstWhere('user_id',Auth::user()->id) != null) ? $userDetails->firstWhere('user_id',Auth::user()->id)->user_address : "" }}">
                                    </div>
                                  </div> 
                                  <div class="col-lg-6">
                                    <div class="form-group">
                                      <label class="form-control-label"
                                        for="input-last-name">Street Line 2</label>
                                        <input type="text" class="form-control"  id="street_2" name="street_2"  placeholder="Streen Line 2">
                                    </div>
                                  </div>   
                                  <div class="col-lg-6">
                                    <div class="form-group">
                                      <label class="form-control-label"
                                        for="input-last-name">Zip Code*</label>
                                        <input type="text" class="form-control"  id="zipcode" name="zipcode"  placeholder="Zip Code" value="{{$userDetails->firstWhere('user_id',Auth::user()->id) != null ? $userDetails->firstWhere('user_id',Auth::user()->id)->user_postalCode : ""}}">
                                    </div>
                                  </div> 
                                  <div class="col-lg-6">
                                    <div class="form-group">
                                                <label class="form-control-label" for="input-last-name">Select Country*</label>
                                                <select class="form-control" data-trigger name="country_pi" id="country_pi">
                                                    <?php $countryId = $userDetails->firstWhere('user_id', Auth::user()->id) != null ? $userDetails->firstWhere('user_id', Auth::user()->id)->country_id : "";
                         ?>
                                @if($countryId != null)
                                <option value="{{$countryId}}">{{$country_data->firstWhere('id',$countryId )->country_name}}</option>
                                @else
                                <option value="">Select Country</option>
                                @endif
                                @foreach ($country_data as $country)
                                 <option value="{{$country->id}}">{{$country->country_name}}</option>
                               @endforeach
                                </select>
                                              </div>
                                  </div>
                                  <div class="col-lg-6">
                                    <div class="form-group">
                                      <label class="form-control-label" for="input-last-name">Select State</label>
                                                <select class="form-control" data-trigger name="state_pi" id="state_pi">
                                                    <option  value="">Select State</option>
                                                  </select>
                                    </div>
                                  </div>
                                  
                                  <div class="col-lg-6">
                                    <div class="form-group">
                                      <label class="form-control-label" for="input-last-name">Select City*</label>
                                                <select class="form-control" data-trigger name="city_pi" id="city_pi">
                                                    <?php $cityId = $userDetails->firstWhere('user_id', Auth::user()->id) != null ? $userDetails->firstWhere('user_id', Auth::user()->id)->city_id : "";
                         ?>
                                @if($cityId != null)
                                <option value="{{$cityId}}">{{$city_data->firstWhere('id',$cityId )->city_name}}</option>
                                @else
                                <option value="">Select City</option>
                                @endif
                                </select>
                                    </div>
                                  </div>

                                
                                  
                                  <!-- <div class="col-lg-12">
                                    <div class="form-group">
                                      <label class="form-control-label"
                                        for="input-last-name">Phone Number*</label>
                                       <input type="number" class="form-control"  id="phonenumber" name="phonenumber"  placeholder="Phone Number">
                                    </div>
                                  </div> -->                     
                                </div>
                                <h2 class="fs-title mt-5">Upload Document</h2>
                                <hr class="my-2">
                                <div class="row m-0">
                                  <div class="col-lg-12">
                                    <div class="form-group">
                                        <label class="form-control-label"
                                        for="input-last-name">Upload Your License</label>
                                        <div class='file-input'>
                                          <input class="custom-file-input" type='file' name="license" id="license">
                                          <span class='choose-btn'>Choose</span>
                                          <span class="clear-btn" data-clear-input>&times;</span>
                                        </div>
                                    </div>
                                  </div>


                                  <div class="col-lg-12">
                                    <div class="form-group">
                                      <label class="form-control-label"
                                        for="input-last-name">Upload Your Driving License</label>
                                        <div class='file-input'>
                                          <input class="custom-file-input" type='file' name="drivingLicense" id="drivingLicense">
                                          <span class='choose-btn'>Choose</span>
                                          <span class="clear-btn" data-clear-input>&times;</span>
                                        </div>
                                    </div>
                                  </div>



                                  <div class="col-lg-12">
                                    <div class="form-group">
                                      <label class="form-control-label"
                                        for="input-last-name">Upload Your Utility Bill</label>
                                        <div class='file-input'>
                                          <input class="custom-file-input" type='file' name="utilityBill" id="utilityBill">
                                          <span class='choose-btn'>Choose</span>
                                          <span class="clear-btn" data-clear-input>&times;</span>
                                        </div>
                                    </div>
                                  </div>

                                  <div class="col-lg-12">
                                    <div class="form-group">
                                      <label class="form-control-label"
                                        for="input-last-name">Upload Your Passport*</label>
                                        <div class='file-input'>
                                          <input class="custom-file-input" type='file' id="passport" name="passport">
                                          <span class='choose-btn'>Choose</span>
                                          <span class="clear-btn" data-clear-input>&times;</span>
                                        </div>
                                    </div>
                                  </div>  

                                  <span id="documentError" style="color:red;"></span>

                                 <input type="hidden" id="anyOneDocumentUploaded" name="anyOneDocumentUploaded">

                                </div>
                              </div>                             
                            </div>                            
                          </div>                         
                        </div>
                        <div>
                        </div>
                        <input type="button" name="previous"
                          class="previous action-button-previous" value="Previous" />
                        <input type="button" name="next" class="next action-button" id="next4" value="Next Step" />
                      </fieldset> 
                      <fieldset>
                        <div class="form-card">
                        <div class="col-lg-12">
                        <div class="search-result">
                            <div class="details-box row mt-4 m-0">
                                <div class="col-lg-12 text-left">
                                    <h5>Ads Information</h5>
                                </div>
                                <div class="col-md-12">
                                    <div
                                        class="stm-listing-single-price-title heading-font clearfix text-left">
                                        <div class="price" id="priceShow"></div>
                                        <div class="stm-single-title-wrap">
                                            <h1 class="title" id="title">
                                                Rolex GT Matrix 2
                                            </h1>
                                        </div>
                                    </div>
                                    <div class="row">                                       
                                        <div class="col-lg-6">
                                            <p>Normal Images</p>
                                            <div id="imgGallery" class="d-flex flex-wrap">
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                          <p>Damage Images</p>
                                          <div id="dmgGallery" class="d-flex flex-wrap">
                                          </div>
                                      </div>
                                    </div>
                                    <!-- End single-product-images -->                                   
                                    <div class="watch-desc">

                                      <div class="stm-border-top-unit bg-color-border" id="commentShow">
                                          <h5><strong>Description</strong></h5>
                                      </div>
          
                                      <div class="stm-border-top-unit bg-color-border">
                                          <h5><strong>Basic Details</strong></h5>
                                      </div>
                                      <div class="stm-single-listing-car-features watch-details">
                                          <div class="lists-inline">
                                              <ul class="list-style-2">
                                                  <li id="brandShow">Brand: 	Rolex</li>
                                                  <li id="modelShow">Model: GMT-Master II</li>
                                              </ul>
                                              <ul class="list-style-2">
                                                  <li id="watchConditionShow">Condition: New</li>
                                                  <li id="movementShow">Movement: </li>
                                              </ul>
                                              <ul class="list-style-2">
                                                  <li id="caseMaterialShow">Case material: Steel</li>
                                                  <li id="braceletMaterialShow">Bracelet material: Stainless-Steel</li>
                                              </ul>
                                              <ul class="list-style-2">
                                                  <li id="yomShow">Year of production: </li>
                                                  <li id="genderShow">Gender: Women's</li>
                                              </ul>
                                              <ul class="list-style-2">
                                                  <li id="locationShow">Location: India,  Delhi</li>
                                                  <li id="dialColorShow">Dial Color: Gold</li>
                                              </ul>
                                          </div>
                                      </div>
                                      <!-- Start of the report section -->          
          
                                  </div>
                                </div>
                            </div>
                            <!-- <div class="row mt-5">
                              <div class="col-lg-12">
                                <div class="choosePaymmentoption">
                                  <div class="form-group"> 
                                    <label class="radio-inline"> 
                                      <input type="radio" name="postAds" checked="" value="free">Post free ads </label> 
                                      <label class="radio-inline"> <input type="radio" name="postAds" value="paid" class="ml-5">Upgrade Ads for 3 months 
                                    </label>
                                </div>
                                </div>
                              </div>
                            </div> -->
                            
                            <div class="upgradepackage">
                              <div class="card">
                                <div class="card-header bg-dark text-white">
                                 <h4> Sell faster with a Premium listing</h4>
                                </div>
                                <div class="card-body bg-light border premiumAds-suggestion">
                                  <h6 class="card-title">"TOP" distinction increases your chances of selling</h6>
                                  <p class="card-text">Sell 10% faster thanks to better ad placement</p> 
                                  
                                  <?php /* ?><div class="row">
                                	@for($i=0; $i < count($upgradepackage); $i++)
                                	@if($i === 0)
                                    <!-- Purple Table -->
                                    <div class="col-md-6">
                                       <div class="pricing-table purple thispackage">
                                          <!-- Table Head -->
                                          <h2>{{$packageList->firstWhere('id',$upgradepackage[$i]->package_id)->package_for_private_sellers}}</h2>
                                          <h5>Made for starters</h5>
                                        
                                          <!-- Price -->
                                          <div class="price-tag">
                                             <!-- <span class="symbol">$AUD</span> -->
                                             <span class="amount">{{currency()->convert(floatval($upgradepackage[$i]->pricing), 'USD', currency()->getUserCurrency())}}</span>
                                             <span class="after">/ad</span>
                                          </div>
                                          <!-- Button -->
                                          <a class="price-button activatethis" href="javascript:void(0);">Activate</a>
                                       </div>
                                    </div>
                                    @elseif($i === 1)
                                    <!-- Turquoise Table -->
                                    <div class="col-md-6">
                                       <div class="pricing-table turquoise thispackage">
                                          <!-- Table Head -->
                                          <h2>{{$packageList->firstWhere('id',$upgradepackage[$i]->package_id)->package_for_private_sellers}}</h2>
                                          <h5>Made for experienced users</h5>                                          
                                          <!-- Price -->
                                          <div class="price-tag">
                                             <!-- <span class="symbol">$AUD</span> -->
                                             <span class="amount">{{currency()->convert(floatval($upgradepackage[$i]->pricing), 'USD', currency()->getUserCurrency())}}</span>
                                             <span class="after">/ad</span>
                                          </div>
                                          <!-- Button -->
                                          <a class="price-button activatethis" href="javascript:void(0);">Activate</a>
                                       </div>
                                    </div> 
                                    @endif
                                    @endfor                                  
                               </div> <?php */ ?>                                                                                              
                                </div>                     
                              </div>
                            </div>
                            <div class="listingFee">
                              <div class="card">
                                <div class="card-header bg-white">
                                 <h4> Listing fee</h4>
                                </div>
                                <div class="card-body bg-white">
                                  <table class="w-100">
                                    <tr>
                                      <th class="packageName">Listing Fee</th><th><span class="ads-fee">{{currency()->convert(floatval($upgradepackage->pricing), 'USD', currency()->getUserCurrency())}}</span></th>                                      
                                    </tr>
                                    <tr>
                                      <td><i class="fa fa-clock mr-2"></i>Duration</td><td>3 Months</td>                                      
                                    </tr>
                                  </table>
                                </div>  
                                <div class="card-footer text-muted  bg-white border-0">
                                  Your listing is valid for 3 months. During this time, the listed item may not be sold elsewhere. If you sell your watch on Tangible, we charge a 1.5% commission fee.
                                </div>   
                                <div class="m-y-5">
                                  <div class="form-check">
                                     <label>
                                        <input name="privateSellerAgb" id="privateSellerAgb" type="checkbox" title="I accept Tangible Private Seller">
                                        I accept Tangible's Private Seller <a href="#" target="_blank" rel="noopener">General Terms and Conditions</a>
                                     </label>
                                  </div>
                               </div>                           
                              </div>
                            </div>
                        </div>
                        </div>
                        </div>
                        <div class="row form-group">
                    <div class="col-md-12">
                        <div class="text-danger font-italic generic-errors-top"></div>
                    </div>
                </div>
                        <input type="button" name="previous" class="previous action-button-previous" value="Previous" />
                        <!-- <input type="button" name="next" class="next action-button" id="next5" value="Next Step" /> -->
                        <input type="hidden" id="paymentAmount" name="paymentAmount" value="{{ $upgradepackage->pricing }}">

                        <input type="submit" id="next5" name="submit" class=" action-button" value="Submit" />
                        
                        <!-- <input id="postNow" type="button" name="postnow" class="btn btn-success action-button" value="Post ads Now" /> -->
                    </fieldset>  
                    
                  <?php /* ?>  <fieldset>
                            <div class="form-card" >
  <div class="col-lg-12">
  <div class="search-result text-left">
      <div class="header">
        <h5 class="title" id="exampleModalLongTitle">Make a payment</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="row paymentOptionContainer">
          <div class="col-lg-12 mx-auto">
          <!-- <form action="{{ url('privateSellerPayments') }}" method="post" id="payment-form" enctype="multipart/form-data">
          @csrf -->
              <div class="card ">
                  <div class="card-header">
                      <div class="bg-white shadow-sm pt-4 pl-2 pr-2 pb-2">
                          <!-- Credit card form tabs -->
                          <ul role="tablist"
                              class="nav bg-light nav-pills rounded nav-fill mb-3">
                              <li class="nav-item"> <a data-toggle="pill" href="#credit-card" class="nav-link active ">
                                  <i class="fa fa-credit-card mr-2"></i> Credit Card </a>
                              </li>
                              <li class="nav-item"> <a data-toggle="pill"  href="#paypal" class="nav-link "> <i class="fa fa-paypal mr-2"></i> Paypal </a>
                              </li>
                          </ul>
                      </div>
                      <!-- End -->
                      <!-- Credit card form content -->

                      <div class="tab-content">
                          <!-- credit card info-->
                          <div id="credit-card"
                              class="tab-pane fade show active pt-3">
                              <div class="row form-group">
                    <div class="col-md-12">
                        <label for="name">Name</label>
                        @error('name')
                        <div class="text-danger font-italic">{{ $message }}</div>
                        @enderror
                        <input type="text" name="name" id="name" class="form-control" value="{{ old('name') }}">
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col-md-12">
                        <label for="email">Email</label>
                        @error('email')
                        <div class="text-danger font-italic">{{ $message }}</div>
                        @enderror
                        <input type="text" name="email" id="email" class="form-control" value="{{ old('email') }}">
                    </div>
                </div>
                              <div class="row form-group">
                    <div class="col-md-12">
                        <!-- Display errors returned by createToken -->
                        <label>Card Number</label>
                        <div id="paymentResponse" class="text-danger font-italic"></div>
                        <div id="card_number" class="field form-control"></div>
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col-md-3">
                        <label>Expiry Date</label>
                        <div id="card_expiry" class="field form-control"></div>
                    </div>
                    <div class="col-md-3">
                        <label>CVC Code</label>
                        <div id="card_cvc" class="field form-control"></div>
                    </div>
                </div>
                              </div>
                              <div class="row form-group">
                    <div class="col-md-12">
                        <div class="form-check form-check-inline custom-control custom-checkbox">
                            <input type="checkbox" name="terms_conditions" id="terms_conditions" class="custom-control-input">
                            <label for="terms_conditions" class="custom-control-label">
                                I agree to terms & conditions
                            </label>
                        </div>
                        @error('terms_conditions')
                        <div class="text-danger font-italic">{{ $message }}</div>
                        @enderror
                    </div>
                </div>
                			 <div class="row form-group">
                    <div class="col-md-12">
                        <div class="text-danger font-italic generic-errors"></div>
                    </div>
                </div>
                              <div class="card-footer">
                              <input type="submit" value="Pay via Stripe" class="btn btn-primary pay-via-stripe-btn">
                                  <!-- <button type="button" id="subbut"
                                      class="subscribe btn btn-primary btn-block shadow-sm" >
                                  Confirm Payment
                                  </button> -->
                              </div>
                          </div>
                          <!-- End -->
                          <!-- Paypal info -->
                          <div id="paypal"
                              class="tab-pane fade pt-3">
                              <h6 class="pb-2">Select your paypal account type</h6>
                              <div class="form-group"> <label  class="radio-inline">
                                <input type="radio" name="optradio" checked>Domestic </label> <label
                                      class="radio-inline"> <input
                                      type="radio"
                                      name="optradio"
                                      class="ml-5">International
                                  </label>
                              </div>
                              <p> <button type="button"
                                  class="btn btn-primary "><i
                                  class="fa fa-paypal mr-2"></i>
                                  Log into my Paypal</button>
                              </p>
                              <p class="text-muted"> Note: After clicking on the button, you will be directed to a secure gateway
                                  for payment. After completing the payment process, you will be redirected back to the website
                                  to view details of your order.
                              </p>
                          </div>
                          <!-- End -->
                      </div>
                  </div>
              </div>
              <!-- </form> -->
          </div>
      </div>
      </div>
    </div>
  </div>
                            </fieldset>

                             <?php */ ?>
                    </form>
                    </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    
    @else
      <section>
      <!-- Modal HTML -->
        <div id="myModal2" class="modal fade">
            <div class="modal-dialog modal-login">
                
                <div class="modal-content">
                @if($errors->any())
                    @foreach($errors->all() as $error)
                      <div class="alert alert-danger">
                    {{$error}}
                      </div>
                    @endforeach
                  @endif
                  <div id="loginForm">
                    <div class="modal-header">                       			
                        <h4 class="modal-title">Member Login</h4>	
                        <!-- <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button> -->
                    </div>
                    <div class="modal-body">                        
                            <div class="form-group">
                                <input type="text" class="form-control" name="email" placeholder="Username or email" required="required" id="usernameEmail"><span id="emailcheck"></span>		
                            </div>
                            <div class="login">
                                <div class="form-group">
                                    <input type="password" class="form-control d-none" name="password" placeholder="Password" required="required" id="rPassword"> 	
                                </div>      
                            </div>
                            <div class="form-group">
                                <button id="userLogin" class="btn btn-primary btn-lg btn-block login-btn">Login</button>
                            </div>
                            <div class="modal-footer">
                                <a href="{{ route('password.request') }}">Forgot Password?</a>
                            </div>         
                    </div>
                    </div>
                    @if($errors->any())
                    @foreach($errors->all() as $error)
                      <div class="alert alert-danger">
                    {{$error}}
                      </div>
                    @endforeach
                  @endif
                    <form action="/privateSeller/newprivateSellerRegister" method="post" id="registerForm" class="needs-validation d-none" novalidate >
                    @csrf
                        <div class="modal-header">                       			
                            <h4 class="modal-title">Register</h4>	
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        </div>
                        <div class="modal-body"> 
                        <div class="register">
                            <div class="form-group">
                                <input type="text" class="form-control" name="first_name" placeholder="First Name" required="required">	
                                <div class="invalid-feedback">This Field is required</div>
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" name="last_name" placeholder="Last Name" required="required">
                                <div class="invalid-feedback">This Field is required</div>
                            </div>
                            <div class="form-group">
                                <input type="number" class="form-control" name="phone" placeholder="Phone No"  minlength="10" maxlength="10" required="required">	
                                <div class="invalid-feedback">This Field is required</div>
                            </div> 
                            <div class="form-group">
                                <input type="email" class="form-control" name="email" placeholder="email" required="required">
                                <div class="invalid-feedback">This Field is required</div>
                            </div> 
                            <div class="form-group">
                                <input type="password" class="form-control" name="password" placeholder="Password" required="required" minlength="8">
                                <div class="invalid-feedback">Password should be 8 digits minimum</div>
                            </div>   
                            <div class="form-group">
                                <input type="password" class="form-control" name="confirmpassword" placeholder="Confirm Password" required="required" minlength="8">
                                <div class="invalid-feedback">This Field is required</div>
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary btn-lg btn-block login-btn">Register</button>
                            </div>
                        </div>
                    </div>                        
                    </form>
                </div>
            </div>
        </div>
        </section>
        @endauth
   @endif 
   
    @include('tangiblehtml.innerfooter')
    <script src="/assets/js/jquery.js"></script>
    <script src="/assets/js/image-uploader.min.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="/assets/js/bootstrap.min.js"></script>
    <script src="/assets/js/wow.min.js"></script>
    <script src="/assets/js/slick.min.js"></script>
    <script src="/assets/js/local.js"></script>
    <script src="https://js.stripe.com/v3/"></script>


    <script>
    $(window).on('load',function(){
        $('#myModal2').modal({backdrop: 'static', keyboard: false});
    });
      // steps JS
      $(document).ready(function () {

      $('#price').keyup(function() {

      var price = this.value;
      var discount = (1.5/100) * price;
      //console.log(discount);
      $('#commision').val(discount);

      var revenue = price-discount;
      $('#revenue').val(revenue);

      });

    $('#passport, #utilityBill, #license, #drivingLicense').on('change', function() { 
    // select the form and submit
    $("#anyOneDocumentUploaded").val(1);
    });

    	  $("#countryId").change(function(){
    	    	$('#loading-image').show();
    	        var countryId = $(this).val();
    	        $.ajax({
    	          url: "/getState",
    	          type: "POST",
    	          data: {countryId: countryId, _token: '{{csrf_token()}}' },
    	          dataType: 'json',
    	          success : function(data){
    	            var html = `<option value="">Select State</option>`;
    	            for (var i = 0; i < data.length; i++) {
    	                var id = data[i].id;
    	                var name = data[i].state_name;
    	                var option = `<option value="${data[i].id}">${data[i].state_name}</option>`;
    	                html += option;
    	              }
    	              $('#stateId').html(html); 
    	            }
    	          });
    	        $.ajax({
    				url: "/getCity",
    				type: "POST",
    				data: {countryId: countryId  ,_token: '{{csrf_token()}}' },
    				dataType: 'json',
    				success : function(data){
    					var html = `<option value="">Select City</option>`;
    					for (var i = 0; i < data.length; i++) {
    						  var id = data[i].id;
    						  var name = data[i].city_name;
    						  var option = `<option value="${data[i].id}">${data[i].city_name}</option>`;
    						  html += option;
    						}
    					$('#loading-image').hide();
    						$('#cityId').html(html);	
    					}
    				});
    	      });
    	        $("#stateId").change(function(){
    	        var stateId = $(this).val();
    	        $('#loading-image').show();
    	        $.ajax({
    	          url: "/getCity",
    	          type: "POST",
    	          data: {stateId: stateId, _token: '{{csrf_token()}}' },
    	          dataType: 'json',
    	          success : function(data){
    	            var html = `<option value="">Select City</option>`;
    	            for (var i = 0; i < data.length; i++) {
    	                var id = data[i].id;
    	                var name = data[i].city_name;
    	                var option = `<option value="${data[i].id}">${data[i].city_name}</option>`;
    	                html += option;
    	              }
    	            $('#loading-image').hide();
    	              $('#cityId').html(html);  
    	            }
    	          });
    	      });

    	        $("#country_pi").change(function(){
    	        	$('#loading-image').show();
    	            var countryId = $(this).val();
    	            $.ajax({
    	              url: "/getState",
    	              type: "POST",
    	              data: {countryId: countryId, _token: '{{csrf_token()}}' },
    	              dataType: 'json',
    	              success : function(data){
    	                var html = `<option value="">Select State</option>`;
    	                for (var i = 0; i < data.length; i++) {
    	                    var id = data[i].id;
    	                    var name = data[i].state_name;
    	                    var option = `<option value="${data[i].id}">${data[i].state_name}</option>`;
    	                    html += option;
    	                  }
    	                  $('#state_pi').html(html); 
    	                }
    	              });
    	            $.ajax({
    	    			url: "/getCity",
    	    			type: "POST",
    	    			data: {countryId: countryId  ,_token: '{{csrf_token()}}' },
    	    			dataType: 'json',
    	    			success : function(data){
    	    				var html = `<option value="">Select City</option>`;
    	    				for (var i = 0; i < data.length; i++) {
    	    					  var id = data[i].id;
    	    					  var name = data[i].city_name;
    	    					  var option = `<option value="${data[i].id}">${data[i].city_name}</option>`;
    	    					  html += option;
    	    					}
    	    				$('#loading-image').hide();
    	    					$('#city_pi').html(html);	
    	    				}
    	    			});
    	          });
    	            $("#state_pi").change(function(){
    	            var stateId = $(this).val();
    	            $('#loading-image').show();
    	            $.ajax({
    	              url: "/getCity",
    	              type: "POST",
    	              data: {stateId: stateId, _token: '{{csrf_token()}}' },
    	              dataType: 'json',
    	              success : function(data){
    	                var html = `<option value="">Select City</option>`;
    	                for (var i = 0; i < data.length; i++) {
    	                    var id = data[i].id;
    	                    var name = data[i].city_name;
    	                    var option = `<option value="${data[i].id}">${data[i].city_name}</option>`;
    	                    html += option;
    	                  }
    	                $('#loading-image').hide();
    	                  $('#city_pi').html(html);  
    	                }
    	              });
    	          });

    	  $("#brand").change(function(){
    	      console.log('------------------------');
    	  		var brandId = $(this).val();
    	  		
    	  		$('#loading-image').show();
    	  		$.ajax({
    	  			url: "/getWatchModel",
    	  			type: "POST",
    	  			data: {brandId: brandId, _token: '{{csrf_token()}}' },
    	  			dataType: 'json',
    	  			success : function(data){
    	  				var html = `<option value="">Select Model</option>`;
    	  				for (var i = 0; i < data.length; i++) {
    	  					  var option = `<option value="${data[i].id}">${data[i].watch_model_name}</option>`;
    	  					  html += option;
    	  					}
    	  				$('#loading-image').hide();
    	  					$('#model').html(html);	
    	  				}
    	  			});
    	  	});

          
          var current_fs, next_fs, previous_fs; //fieldsets
          var opacity;
          var ImgArray = [];
          var ImgArrayDamage = []; 
          const imageArrayfn = function () {
              ImgArray = [];
              ImgArrayDamage = [];
              $('.uploaded img').each(function (index, value) {
                if($(this).parents('#damagePhotos').length > 0){
                  ImgArrayDamage.push($(this).attr('src'));
                }
                else{
                  ImgArray.push($(this).attr('src'));
                }
              });
              imgUploadedfn();
          }

          // console.log(ImgArray);
          // console.log(ImgArrayDamage);
      
          const imgUploadedfn = function () {

              let imgCount = ImgArray.length;
              let imgCountDmg = ImgArrayDamage.length;
              let html = '';
              let imgGallery = '';
              let dmgGallery= '';
              
              if (imgCount > 0) {
                  for (let j = 0; j < imgCount; j++) {                   
                      // html += ' <div class="item" data-src="' + ImgArray[j] + '"><img src="' + ImgArray[j] + '"></div>';
                      imgGallery += '<div><img src="' + ImgArray[j] + '" /></div>';
                  }
                  $('#imgGallery').empty();   
                  $('#imgGallery').append(imgGallery);      
                 
              }  
              if (imgCountDmg > 0) {
                  for (let i = 0; i < imgCountDmg; i++) { 
                    dmgGallery += '<div><img src="' + ImgArrayDamage[i] + '" /></div>';
                  }   
                  $('#dmgGallery').empty();               
                  $('#dmgGallery').append(dmgGallery);      
                  
              } 
                console.log('Images Array - > ', ImgArray)  ;  
                console.log('Damage Images Array - > ', ImgArrayDamage)  ;  
          }
          /* $(".next").click(function () {
              imageArrayfn();
              current_fs = $(this).parent();
              next_fs = $(this).parent().next();      
              //Add Class Active
              $("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");      
              //show the next fieldset
              next_fs.show();
              //hide the current fieldset with style
              current_fs.animate({ opacity: 0 }, {
                  step: function (now) {
                      // for making fielset appear animation
                      opacity = 1 - now;
      
                      current_fs.css({
                          'display': 'none',
                          'position': 'relative'
                      });
                      next_fs.css({ 'opacity': opacity });
                  },
                  duration: 600
              });
          }); */      
          $(".previous").click(function () {
              current_fs = $(this).parent();
              previous_fs = $(this).parent().prev();
              //Remove class active
              $("#progressbar li").eq($("fieldset").index(current_fs)).removeClass("active");      
              //show the previous fieldset
              previous_fs.show();
              //hide the current fieldset with style
              current_fs.animate({ opacity: 0 }, {
                  step: function (now) {
                      // for making fielset appear animation
                      opacity = 1 - now;
      
                      current_fs.css({
                          'display': 'none',
                          'position': 'relative'
                      });
                      previous_fs.css({ 'opacity': opacity });
                  },
                  duration: 600
              });
          });
          $('.radio-group .radio').click(function () {
              $(this).parent().find('.radio').removeClass('selected');
              $(this).addClass('selected');
          });
          $(".submit").click(function () {
              return false;
          })
          $('.input-images-1').imageUploader();
		      $('.input-images-2').imageUploader2();
          $( ".uploaded" ).sortable();
          $( ".uploaded" ).disableSelection();

        //Timer Array
          const timerArray = [
            ['security-1.png','01:45'],['security-2.png','02:00'],['security-3.png','03:04'],['security-4.png','04:10'],
            ['security-5.png','04:15'],['security-6.png','05:19'],['security-7.png','05:30'],['security-8.png','05:40'],
            ['security-9.png','05:50'],['security-10.png','05:56'],['security-11.png','07:49'],['security-12.png','08:09'],
            ['security-13.png','09:09'],['security-14.png','09:20'],['security-15.png','10:20'],['security-16.png','12:20'],
            ['security-17.png','01:20'],['security-18.png','02:20'],['security-19.png','02:25'],['security-20.png','02:32'],
            ['security-21.png','02:37'],['security-22.png','02:42'],['security-23.png','03:42'],['security-24.png','04:42'],
            ['security-25.png','05:42'],['security-26.png','06:49'],['security-27.png','07:49'],['security-28.png','08:49'],
            ['security-29.png','10:49'],['security-30.png','11:49'],['security-31.png','11:24'],['security-32.png','12:24'],
            ['security-33.png','01:18'],['security-34.png','02:18'],['security-35.png','03:18'],['security-36.png','04:18'],
            ['security-37.png','05:18'],['security-38.png','06:18'],['security-39.png','07:18'],['security-40.png','08:18'],
            ['security-41.png','09:32'],['security-42.png','10:32'],['security-43.png','11:32'],['security-44.png','12:43'],
            ['security-45.png','03:43'],['security-46.png','04:43'],['security-47.png','05:43'],['security-48.png','06:43'],
            ['security-49.png','07:47'],['security-50.png','08:47']
            ]      
            //Setting First clock Time            
            let RandomIndexNumber = Math.floor(Math.random() * 20);            
            let RandomImageSecurity1 = timerArray[RandomIndexNumber][0];
            let RandomTimeSecurity1 = timerArray[RandomIndexNumber][1];
            $('.secutrity1 img').attr('src','/assets/img/security-images/'+RandomImageSecurity1+'');
            $('.digitalTime1').text(RandomTimeSecurity1);
            $('.digitalTime1').val(RandomTimeSecurity1);

            //Setting second clock Time
            let RandomIndexNumber2 = Math.floor(Math.random() * 20);
            let RandomImageSecurity2 = timerArray[RandomIndexNumber2][0];
            let RandomTimeSecurity2 = timerArray[RandomIndexNumber2][1];
            $('.secutrity2 img').attr('src','/assets/img/security-images/'+RandomImageSecurity2+'');
            $('.digitalTime2').text(RandomTimeSecurity2);	
            $('.digitalTime2').val(RandomTimeSecurity2);
          
        var isError = false;
     	  $("#next1").click(function(){
				var make = $('#brand').val();
				var model = $('#model').val();
				 if(make == ""){
					$('#brand').css('border','1px solid red');
					isError = true;
					}else{
            $("#brand").removeAttr("style");
						isError = false;
						}
				if(model == ""){
					$('#model').css('border','1px solid red');
					isError = true;
					}else{
            $("#model").removeAttr("style");
						isError = false;
						} 
				

        	 if(isError == false){
        		current_fs = $('#next1').parent();
 			next_fs = $('#next1').parent().next();

 			//Add Class Active
 			$("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");

 			//show the next fieldset
 			next_fs.show();
 			//hide the current fieldset with style
 			current_fs.animate({opacity: 0}, {
 			step: function(now) {
 			// for making fielset appear animation
 			opacity = 1 - now;

 			current_fs.css({
 			'display': 'none',
 			'position': 'relative'
 			});
 			next_fs.css({'opacity': opacity});
 			},
 			duration: 600
 			});
            	 }
     	 });

     	$("#pricetType").change(function(){ 
		
         if($(this).val() == 3){

                 $("#price").prop('disabled', true);	
          }else{
                 $("#price").prop('disabled', false);			
	      
        }         
     });
     	 
     	$("#next2").click(function(){
			var currency = $('#currency').val();
			var country = $('#countryId :selected').val();
   		 	var city = $('#cityId :selected').val();
        var stateId = $('#stateId :selected').val();
   		 	var priceType = $('#pricetType :selected').val();
   		 	var price = $('#price').val();
   		 	var caseDiameter = $('#case_diameter :selected').val();
   		 	var movement = $('#movement :selected').val();
   		 	var type = $('#type :selected').val();
   		 	var gender = $('#gender :selected').val();
   		 	var inclusions = $('#inclusions').val();
   		 	var reference_num = $('#referenceNo').val();
   		 	var watchCondition = $('#watchCondition :selected').val();
   		 	//var status = $('#status :selected').val();

   		 	var powerReserve = $('#powerReserve :selected').val();
   		 	var dialColor = $('#dialColor :selected').val();
   			var braceletMaterial = $('#braceletMaterial :selected').val();
   			var bezelMaterial = $('#bezelMaterial :selected').val();
   			var braceletColor = $('#braceletColor :selected').val();
   			var typeOfClasp = $('#typeOfClasp :selected').val();
   			var claspMaterial = $('#claspMaterial :selected').val();
   			var caseMaterial = $('#caseMaterial :selected').val();
   			var caseMM = $('#caseMM :selected').val();
   			var wrd = $('#wrd :selected').val();
   			var glassType = $('#glassType :selected').val();
   			var timezone = $('#timezone').val();


			 
				
            if(country == ""){
            $('#countryId').css('border','1px solid red');
            isError = true;
            }else{
            $("#countryId").removeAttr("style");
            isError = false;
            }
            if(stateId == ""){
            $('#stateId').css('border','1px solid red');
            isError = true;
            }else{
            $("#stateId").removeAttr("style");
            isError = false;
            }
            if(city == ""){
            $('#cityId').css('border','1px solid red');
            isError = true;
            }else{
            $("#cityId").removeAttr("style");
            isError = false;
            }
            if(caseDiameter == ""){
            $('#case_diameter').css('border','1px solid red');
            isError = true;
            }else{
            $("#case_diameter").removeAttr("style");
            isError = false;
            }
            if(gender == ""){
            $('#gender').css('border','1px solid red');
            isError = true;
            }else{
            $("#gender").removeAttr("style");
            isError = false;
            }
            if(inclusions == ""){
            $('#inclusions').css('border','1px solid red');
            isError = true;
            }else{
            $("#inclusions").removeAttr("style");
            isError = false;
            }  
            if(reference_num == ""){
            $('#referenceNo').css('border','1px solid red');
            isError = true;
            }else{
            $("#referenceNo").removeAttr("style");
            isError = false;
            }
            if(watchCondition == ""){
            $('#watchCondition').css('border','1px solid red');
            isError = true;
            }else{
            $("#watchCondition").removeAttr("style");
            isError = false;
            }
            if(timezone == ""){
            $('#timezone').css('border','1px solid red');
            isError = true;
            }else{
            $("#timezone").removeAttr("style");
            isError = false;
            }
            if(priceType == ""){
            $('#pricetType').css('border','1px solid red');
            isError = true;
            }else{
            $("#pricetType").removeAttr("style");
            isError = false;
            } 
				    if(currency == ""){
					  $('#currency').css('border','1px solid red');
					  isError = true;
					  }else{
            $("#currency").removeAttr("style");
						isError = false;
						}              

	    		
	    	// 	 if(status == ""){
						// $('#status').css('border','1px solid red');
						// isError = true;
						// }else{
      //         $("#status").removeAttr("style");
						// 	isError = false;
						// 	}

		  //   		 if(powerReserve == ""){
	 		// 		$('#powerReserve').css('border','1px solid red');
	 		// 		isError = true;
	 		// 		}else{
	 		// 			isError = false;
	 		// 			}
	 		// 	if(dialColor == ""){
	 		// 				$('#dialColor').css('border','1px solid red');
	 		// 				isError = true;
	 		// 				}else{
	 		// 					isError = false;
	 		// 					}
	 		// 	if(braceletMaterial == ""){
	 		// 						$('#braceletMaterial').css('border','1px solid red');
	 		// 						isError = true;
	 		// 						}else{
	 		// 							isError = false;
	 		// 							}
	 		// 	if(bezelMaterial == ""){
	 		// 								$('#bezelMaterial').css('border','1px solid red');
	 		// 								isError = true;
	 		// 								}else{
	 		// 									isError = false;
	 		// 									}
	 		// 	if(braceletColor == ""){
	 		// 										$('#braceletColor').css('border','1px solid red');
	 		// 										isError = true;
	 		// 										}else{
	 		// 											isError = false;
	 		// 											}
	 		// 	if(typeOfClasp == ""){
	 		// 												$('#typeOfClasp').css('border','1px solid red');
	 		// 												isError = true;
	 		// 												}else{
	 		// 													isError = false;
	 		// 													}
	 		// 	if(claspMaterial == ""){
	 		// 														$('#claspMaterial').css('border','1px solid red');
	 		// 														isError = true;
	 		// 														}else{
	 		// 															isError = false;
	 		// 															}
	 		// 	if(caseMaterial == ""){
	 		// 																$('#caseMaterial').css('border','1px solid red');
	 		// 																isError = true;
	 		// 																}else{
	 		// 																	isError = false;
	 		// 																	}
	 		// 	if(caseMM == ""){
	 		// 																		$('#caseMM').css('border','1px solid red');
	 		// 																		isError = true;
	 		// 																		}else{
	 		// 																			isError = false;
	 		// 																			}
					
	    // 		 if(wrd == ""){
			// 			$('#wrd').css('border','1px solid red');
			// 				isError = true;
			// 				}else{
			// 					isError = false;
			// 					}
			// if(glassType == ""){
			// 	$('#glassType').css('border','1px solid red');
			// 		isError = true;
			// 		}else{
			// 			isError = false;
			// 			}

    	 if(isError == false){
    		current_fs = $('#next2').parent();
			next_fs = $('#next2').parent().next();

			//Add Class Active
			$("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");

			//show the next fieldset
			next_fs.show();
			//hide the current fieldset with style
			current_fs.animate({opacity: 0}, {
			step: function(now) {
			// for making fielset appear animation
			opacity = 1 - now;

			current_fs.css({
			'display': 'none',
			'position': 'relative'
			});
			next_fs.css({'opacity': opacity});
			},
			duration: 600
			});
        	 }
 	 });

     	$("#next3").click(function(){

       


     	

        // alert($('.uploaded-image').length);
        // return false;

      

        var attachPhotos = $('#input-images-1').find('.uploaded-image').length;
				if(attachPhotos == 0){
				$('#img1').html('Please attach watch photos here!');
				isError = true;
				}else{
          $('#img1').html('');
					isError = false;
				}

        // var damagePhotos = $('#damagePhotos').find('.uploaded-image').length;

				// if(damagePhotos == 0){
				// $('#damagePhotos').css('border','1px solid red');
				// isError = true;
				// }else{
				// 	isError = false;
				// 	}

        //var damagePhotos = $('#damagePhotos').find('.uploaded-image').length; 

      //  alert();
       var securityImgA = $('#security1').val();
        if(securityImgA.length <= 0){
        $('#img2').html('Please upload security first image');
        isError = true;
        return false;
        }else{
          $('#img2').html('');
          isError = false;
        }

        var securityImgB = $('#security2').val()
        if(securityImgB.length <= 0){
        $('#img3').html('Please upload security second image');
        isError = true;
         return false;
        }else{
          $('#img3').html('');
          isError = false;
        }

    //      var security1 = $('#security1').val();
				//  if(!security1){
				// $('#security1-image').css('border','1px solid red');
				// isError = true;
				// }else{
				// 	isError = false;
				// 	}
    //     var security2 = $('#security2').val();
				// if(!security2){
				// $('#security2-image').css('border','1px solid red');
				// isError = true;
				// }else{
				// 	isError = false;
				// 	} 

          var textBox =  $.trim( $('#comment').val() );

          if (textBox == "") {

          $('#comment').css('border','1px solid red');
          isError = true;
          }else{
          isError = false;
          }

    	 if(isError == false){
    		current_fs = $('#next3').parent();
			next_fs = $('#next3').parent().next();

			//Add Class Active
			$("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");

			//show the next fieldset
			next_fs.show();
			//hide the current fieldset with style
			current_fs.animate({opacity: 0}, {
			step: function(now) {
			// for making fielset appear animation
			opacity = 1 - now;

			current_fs.css({
			'display': 'none',
			'position': 'relative'
			});
			next_fs.css({'opacity': opacity});
			},
			duration: 600
			});
        	 }
 	 });

     	$('#next4').click(function() {

   		var title = $('#adtitle').val();
   		var brand = $('#brand :selected').text();
			var model = $('#model :selected').text();
			var price = $('#price').val();
			var comment = $('#comment').val();
			var watchCondition = $('#watchCondition :selected').text();
		 	var movement = $('#movement :selected').text();
		 	var caseMaterial = $('#caseMaterial :selected').text();
		 	var braceletMaterial = $('#braceletMaterial :selected').text();
		 	var yom = $('#yom :selected').text();
		 	var gender = $('#gender :selected').text();
		 	var cityId = $('#cityId :selected').text();
		 	var countryId = $('#countryId :selected').text();
		 	var dialColor = $('#dialColor :selected').text();
			
       var titleHtml = brand + ' ' + model;
			//var titleHtml = title + ' ' + brand + ' ' + model;
			$('#title').html(titleHtml);
			$('#priceShow').html(price);
			$('#commentShow').html(comment);
			$('#brandShow').html('Brand: ' +brand);
			$('#modelShow').html('Model: ' +model);
			$('#conditionShow').html('Condition: ' +watchCondition);
			$('#movementShow').html('Movement: ' +movement);
			$('#braceletMaterialShow').html('Bracelet material: ' +braceletMaterial);
			$('#caseMaterialShow').html('Case material: ' +caseMaterial);
			$('#yomShow').html('Year of production: ' +yom);
			$('#genderShow').html('Gender: ' +gender);
			var location = cityId +' ,'+ countryId;
			$('#locationShow').html('Location: ' +location);
			$('#dialColorShow').html('Dial Color: ' +dialColor);
			
			var firstName = $('#firstName').val();
			var lastName = $('#lastName').val();
			var dob = $('#dob').val();
			var address = $('#address').val();
     
			var zipcode = $('#zipcode').val();
			var country_pi = $('#country_pi').val();
			var city_pi = $('#city_pi').val();
			var phonenumber = $('#phonenumber').val();
			
      isError = false;

  		if(firstName == ""){
			$('#firstName').css('border','1px solid red');
			isError = true;
			}
  		if(lastName == ""){
			$('#lastName').css('border','1px solid red');
			isError = true;
			}
  	// 	if(dob == ""){
			// $('#dob').css('border','1px solid red');
			// isError = true;
			// }
  		if(address == ""){
			$('#address').css('border','1px solid red');
			isError = true;
			}else{
        $("#address").removeAttr("style");
        isError = true;
      }
  		if(zipcode == ""){
			$('#zipcode').css('border','1px solid red');
			isError = true;
			}else{
        $("#zipcode").removeAttr("style");
        isError = true;
      }
  		if(country_pi == ""){
			$('#country_pi').css('border','1px solid red');
			isError = true;
			}else{
        $("#country_pi").removeAttr("style");
        isError = true;
      }
  		if(city_pi == ""){
			$('#city_pi').css('border','1px solid red');
			isError = true;
			}else{
        $("#city_pi").removeAttr("style");
        isError = true;
      }
  		if(phonenumber == ""){
			$('#phonenumber').css('border','1px solid red');
			isError = true;
			}else{
        $("#phonenumber").removeAttr("style");
        isError = true;
      }
      
      var bankName = $("#bankName").val();
      if(bankName == ""){
			$('#bankName').css('border','1px solid red');
			isError = true;
			}else{
      $("#bankName").removeAttr("style");
      isError = false;
      }

      var beneficiaryName = $("#beneficiaryName").val();
      if(beneficiaryName == ""){
			$('#beneficiaryName').css('border','1px solid red');
			isError = true;
			}else{
      $("#beneficiaryName").removeAttr("style");
      isError = false;
      }

      var bankAccount = $("#bankAccount").val();
      if(bankAccount == ""){
			$('#bankAccount').css('border','1px solid red');
			isError = true;
			}else{
      $("#bankAccount").removeAttr("style");
      isError = false;
      }

      var ifsc = $("#ifsc").val();
      if(ifsc == ""){
			$('#ifsc').css('border','1px solid red');
			isError = true;
			 }else{
      $("#ifsc").removeAttr("style");
      isError = false;
      }

      var license = document.getElementById('license').files.length;
      var drivingLicense = document.getElementById('drivingLicense').files.length;
      var utilityBill = document.getElementById('utilityBill').files.length;
      var passport = document.getElementById('passport').files.length;

      var docCount = license + drivingLicense + utilityBill + passport;

      // if(!(docCount < 1)){
      // $('#documentError').html('Please select at least two document');
      // isError = true;
      // }else{

      //   $('#documentError').html('');

      // }
   		 if(isError == false){
					imageArrayfn();
    			
          current_fs = $('#next4').parent();
     			next_fs = $('#next4').parent().next();

     			//Add Class Active
     			$("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");

     			//show the next fieldset
     			next_fs.show();
     			//hide the current fieldset with style
     			current_fs.animate({opacity: 0}, {
     			step: function(now) {
     			// for making fielset appear animation
     			opacity = 1 - now;

     			current_fs.css({
     			'display': 'none',
     			'position': 'relative'
     			});
     			next_fs.css({'opacity': opacity});
     			},
     			duration: 600
     			});
        	}
   		 
       	 });

     	  $('#next5').click(function() {
        isError = false;
        if(!$("#privateSellerAgb").prop("checked"))
        {
        $('.generic-errors-top').html('The terms conditions must be accepted.');
        isError = true;
        return false;
        }
        else
        {
        $('.generic-errors-top').html('');
        }

   		 if(isError == false){
					//imageArrayfn();
     			 current_fs = $('#next5').parent();
      			next_fs = $('#next5').parent().next();

      			//Add Class Active
      			$("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");

      			//show the next fieldset
      			next_fs.show();
      			//hide the current fieldset with style
      			current_fs.animate({opacity: 0}, {
      			step: function(now) {
      			// for making fielset appear animation
      			opacity = 1 - now;

      			current_fs.css({
      			'display': 'none',
      			'position': 'relative'
      			});
      			next_fs.css({'opacity': opacity});
      			},
      			duration: 600
      			});

    		 }
   		 
       	 });
      });   
// Security Upload Image Script
$(".imageUploadCls").change(function() {  
  const security = $(this).attr('id');
  if (this.files && this.files[0]) {
        let reader = new FileReader();
        reader.onload = function(e) {                 
            $('#'+security).parents('.avatar-upload').find('.imagePreview').css('background-image', 'url('+e.target.result +')');             
            $('#'+security).parents('.avatar-upload').find('.imagePreview').hide();
            $('#'+security).parents('.avatar-upload').find('.imagePreview').fadeIn(650);
        }
        reader.readAsDataURL(this.files[0]);
      }   
});

//activate the premium ads

var initialPrice = $('.ads-fee').text();
$('.activatethis').on('click',function(){           
  if($(this).parents('.thispackage').hasClass('activatedPackage')){
    $(this).parents('.thispackage').removeClass('activatedPackage');
    $(this).text('Activate');
    $('.packageName').text('Listing Fee');
    $('.ads-fee').text(initialPrice);
  }   
  else{
  $('.thispackage').removeClass('activatedPackage');
  $('.activatethis').text('Activate');
  $(this).text('Deactivate')
  $(this).parents('.thispackage').addClass('activatedPackage');        
  $('.packageName').text($('.activatedPackage h2').text());
  $('.ads-fee').text($('.activatedPackage .amount').text());

  }
})

//Create an instance of the Stripe object
  // Set your publishable API key
  var stripe = Stripe('{{ env("STRIPE_PUBLISH_KEY") }}');

  // Create an instance of elements
  var elements = stripe.elements();

  var style = {
      base: {
          fontWeight: 400,
          fontFamily: '"DM Sans", Roboto, Open Sans, Segoe UI, sans-serif',
          fontSize: '16px',
          lineHeight: '1.4',
          color: '#1b1642',
          padding: '.75rem 1.25rem',
          '::placeholder': {
              color: '#ccc',
          },
      },
      invalid: {
          color: '#dc3545',
      }
  };

  var cardElement = elements.create('cardNumber', {
      style: style
  });
  cardElement.mount('#card_number');

  var exp = elements.create('cardExpiry', {
      'style': style
  });
  exp.mount('#card_expiry');

  var cvc = elements.create('cardCvc', {
      'style': style
  });
  cvc.mount('#card_cvc');

  // Validate input of the card elements
  var resultContainer = document.getElementById('paymentResponse');
  cardElement.addEventListener('change', function (event) {
      if (event.error) {
          resultContainer.innerHTML = '<p>' + event.error.message + '</p>';
      } else {
          resultContainer.innerHTML = '';
      }
  });

  // Get payment form element
  var form = document.getElementById('msform');

  // Create a token when the form is submitted.
  form.addEventListener('submit', function (e) {
      e.preventDefault();
      createToken();
  });

  // Create single-use token to charge the user
  function createToken() {
      stripe.createToken(cardElement).then(function (result) {
          if (result.error) {
              // Inform the user if there was an error
              resultContainer.innerHTML = '<p>' + result.error.message + '</p>';
          } else {
              // Send the token to your server
              stripeTokenHandler(result.token);
          }
      });
  }

  
  // Callback to handle the response from stripe
  function stripeTokenHandler(token) {
      
      // Insert the token ID into the form so it gets submitted to the server
      var hiddenInput = document.createElement('input');
      hiddenInput.setAttribute('type', 'hidden');
      hiddenInput.setAttribute('name', 'stripeToken');
      hiddenInput.setAttribute('value', token.id);
      form.appendChild(hiddenInput);

      // Submit the form
      form.submit();
  }	
  
  $('.pay-via-stripe-btn').on('click', function () {
      var payButton   = $(this);
      var name        = $('#name').val();
      var email       = $('#email').val();

      if (name == '' || name == 'undefined') {
          $('.generic-errors').html('Name field required.');
          return false;
      }
      if (email == '' || email == 'undefined') {
          $('.generic-errors').html('Email field required.');
          return false;
      }

      if(!$('#terms_conditions').prop('checked')){
          $('.generic-errors').html('The terms conditions must be accepted.');
          return false;
      }
  });

    </script>
<script>
// Example starter JavaScript for disabling form submissions if there are invalid fields
(function() {
  'use strict';
  window.addEventListener('load', function() {
    // Fetch all the forms we want to apply custom Bootstrap validation styles to
    var forms = document.getElementsByClassName('needs-validation');
    // Loop over them and prevent submission
    var validation = Array.prototype.filter.call(forms, function(form) {
      form.addEventListener('submit', function(event) {
        if (form.checkValidity() === false) {
          event.preventDefault();
          event.stopPropagation();
        }
        form.classList.add('was-validated');
      }, false);
    });
  }, false);
})();

$(document).ready(function(){
  $(".custom-file-input").on("change", function(){
    var fileName = $(this)[0].files[0].name;
    $(this).next(".choose-btn").html(fileName);
  });

  $(".clear-btn").click(function(){
    $(this).siblings(".custom-file-input").val('');
    $(this).siblings(".choose-btn").html('Choose');
  });
});

</script>
  </body>
</html>


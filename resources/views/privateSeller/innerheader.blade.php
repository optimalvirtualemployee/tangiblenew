
<header class="custom-header-wrapper home-nav">
        <!-- <div class="custom-top-header">
            <div class="container">
                <div class="row">
                    <div class="col-lg-4 col-md-6 col-sm-6 col-12">
                        <div class="header-social-icon">
                            <ul>
                                <li><a class="hvr-buzz-out" href="https://www.facebook.com/tangiblelistings"><i class="fab fa-facebook-f"></i></a></li>
                                <li><a class="hvr-buzz-out" href="https://www.instagram.com/tangiblelistings/"><i class="fab fa-instagram"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-8 col-md-6 col-sm-6 col-12">
                        <div class="header-phone">
                            <ul>
                                <li><a class="hvr-buzz-out" href="tel:(123)987654">(123) 987654</a></li>
                                <li><a class="hvr-buzz-out" href="mailto:info@tangiblelistings.com">info@tangiblelistings.com</a></li>
            <li>
            @if (Route::has('login'))
                    @auth
                        <a id="dropdownMenu1" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="nav-link dropdown-toggle">Hi, James</a>
                                <ul aria-labelledby="dropdownMenu1" class="dropdown-menu border-0 shadow loggedUser">
                                    <li><a href="create-private-listing.html" class="dropdown-item"><i class="fa fa-envelope"></i> Messages</a></li>
                                    <li><a href="#" class="dropdown-item"><i class="fa fa-dollar"></i> Buy</a></li>
                                    <li><a href="#" class="dropdown-item"><i class="fa fa-handshake"></i> Sell</a></li>
                                    <li><a href="#" class="dropdown-item"><i class="fa fa-file"></i> My Listing</a></li>
                                    <li><a href="#" class="dropdown-item"><i class="fa fa-user"></i> My Profile</a></li>
                                    <li><a href="#" class="dropdown-item"><i class="fa fa-gear"></i> Setting</a></li>
                                    <li><a href="#" class="dropdown-item"><i class="fa fa-sign-out"></i> Logout</a></li>
                    @else
                        <a href="#myModal" class="trigger-btn nav-link" data-toggle="modal">Login or Regsiter</a>
                    @endauth
            @endif</li> 
                            </ul>
                            
                            
                        </div>
                    </div>
                </div>
            </div>
        </div> -->
        <div class="custom-bottom-header">
            <nav class="navbar navbar-expand-lg">
                <div class="container">
                    <a href="/" class="navbar-brand">
                        <img src="/assets/img/logo-header.svg" class="img-fluid">
                    </a>
                    <button type="button" data-toggle="collapse" data-target="#navbarContent" aria-controls="navbars" aria-expanded="false" aria-label="Toggle navigation" class="navbar-toggler">
                <span class="navbar-toggler-icon"><i class="fas fa-bars"></i></span>
            </button>
                    <div id="navbarContent" class="collapse navbar-collapse">
                        <ul class="navbar-nav">
                            <li class="nav-item"><a href="/tangibleautomobiles" class="nav-link">Automobiles</a></li>
                            <li class="nav-item"><a href="/tangiblerealestate" class="nav-link"> Real Estate </a></li>
                            <li class="nav-item"><a href="/tangiblewatches" class="nav-link">Watches</a></li>
                            
                            <li>
            @if (Route::has('login'))
                    @auth
                        <li class="nav-item dropdown ml-sm-5"> <a id="dropdownMenu1" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="nav-link dropdown-toggle">Hi, {{ Auth::user()->first_name . ' '. Auth::user()->last_name  }}</a>
                        <ul aria-labelledby="dropdownMenu1" class="dropdown-menu border-0 shadow loggedUser">
                                    <li><a href="/mymessages" class="dropdown-item"><i class="fa fa-envelope"></i> Messages</a></li>
                                    <li><a href="{{url('/buy')}}" class="dropdown-item"><i class="fa fa-dollar"></i> Buy</a></li>
                                    <!--<li><a href="{{url('/sell')}}" class="dropdown-item"><i class="fa fa-handshake"></i> Sell</a></li>-->
                                    <li><a href="{{url('/mylistings')}}" class="dropdown-item"><i class="fa fa-file"></i> My Listing</a></li>
                                    <li><a href="{{url('/myprofile')}}" class="dropdown-item"><i class="fa fa-user"></i> My Profile</a></li>
                                    <!--<li><a href="#" class="dropdown-item"><i class="fa fa-gear"></i> Setting</a></li>-->
                                    <li><a href="{{url('/user/logout')}}" class="dropdown-item"><i class="fa fa-sign-out"></i> Logout</a></li>
                                </ul>
                            </li>
                    @else
                        <a href="#myModal" class="trigger-btn nav-link" data-toggle="modal">Login or Regsiter</a>

                          
                    @endauth
            @endif</li> 
                            <li class="nav-item"><a href="#currency" class="trigger-btn nav-link" data-toggle="modal"><i class="fa fa-gear"></i></a></li>
                            <li class="nav-item"> <a href="javascript:void(0);" class="nav-link sell-btn "  data-toggle="modal" data-target=".bd-example-modal-lg">Sell Now</a>
                                <ul aria-labelledby="dropdownMenu1" class="dropdown-menu border-0 shadow">
                                    <li><a href="/privatesellercategoryselect" class="dropdown-item">Create a Private Listing</a></li>
                                    <li><a href="#" class="dropdown-item">Become a Dealer</a></li>
                                </ul>
                            </li>
                            
                            <!-- End Level one -->
                          <!--  <li class="nav-item"><a href="#" class="nav-link">Contact</a></li> -->
                        </ul>
                    </div>
                </div>
            </nav>
        </div>
    </header>
@extends('admin.common.innercommondefault')
@section('title', 'User Management')
@section('content')
<style type="text/css">.bttn{ border: none;color: #4e73df;background: white;}</style>
<div class="container">
  <div class="row justify-content-center">
    <div class="col-xl-10 col-lg-12 col-md-9">
      <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h3>Users Management</h3>
            </div>
            <div class="pull-right">
                @can('userManagement-create')
                <a class="btn btn-primary" href="{{ route('users.create') }}"> Create New User</a>
            @endcan                    
            </div><br>
        </div>
      </div>
      <div class="card o-hidden border-0 shadow-lg">
        <div class="card-body p-5">
           @if ($message = Session::get('success'))
            <div class="alert alert-success">
              <p>{{ $message }}</p>
            </div>
          @endif

          <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
           <tr>
             <th>No</th>
             <th>Name</th>
             <th>Email</th>
             <th>Roles</th>
             <th width="280px">Action</th>
             
           </tr>
           @foreach ($data as $key => $user)
            <tr>
              <td>{{ ++$i }}</td>
              <td>{{ $user->first_name . ' ' .$user->last_name}}</td>
              <td>{{ $user->email }}</td>
              <td>
                @if(!empty($user->getRoleNames()))
                  @foreach($user->getRoleNames() as $v)
                     <label class="badge badge-success">{{ $v }}</label>
                  @endforeach
                @endif
              </td>
              
              <td>
                 <a href="{{ route('users.show',$user->id) }}"><i class="fas fa-eye"></i></a>&nbsp;&nbsp;
                 @can('userManagement-create')
                   <a href="{{ route('users.edit',$user->id) }}"><i class="fas fa-edit"></i></a>&nbsp;&nbsp;
                    {!! Form::open(['method' => 'DELETE','route' => ['users.destroy', $user->id],'style'=>'display:inline']) !!}
                        {!! Form::button('<i class="fas fa-trash-alt"></i>', ['type' => 'submit','class'=>'bttn']) !!}
                    {!! Form::close() !!}
                  @endcan
              </td>
            </tr>
           @endforeach
          </table>


          {!! $data->render() !!}

          </div>
        </div>
      </div>
    </div>
  </div>
@endsection
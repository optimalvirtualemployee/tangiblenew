@extends('dealer.common.multi-step-default-property')
@section('title', 'Add Property')
@section('content')

    <style>
    .slick-slide img {
    max-height: 300px;r
    width: auto;
    }
    #imgGallery div, #dmgGallery div {
    position: relative; 
    width: 120px;
    height: 120px;
    margin: 5px;
    border: 1px solid #e7e7e7;
    }
    #imgGallery div img, #dmgGallery div img {
    max-height: 150px;
    width: 100%;
    height: 100%;
    object-fit: cover;
    position: absolute;
    }
    .avatar-upload {
  position: relative;
  max-width: 205px;
  border: 2px dotted #bfc8d9;
}
.avatar-upload .avatar-edit {
  position: absolute;
  right: 12px;
  z-index: 1;
  top: 10px;
}
.avatar-upload .avatar-edit input {
  display: none;
}
.avatar-upload .avatar-edit input + label {
  display: inline-block;
  width: 34px;
  height: 34px;
  margin-bottom: 0;
  border-radius: 100%;
  background: #FFFFFF;
  border: 1px solid transparent;
  box-shadow: 0px 2px 4px 0px rgba(0, 0, 0, 0.12);
  cursor: pointer;
  font-weight: normal;
  transition: all 0.2s ease-in-out;
}
.avatar-upload .avatar-edit input + label:hover {
  background: #f1f1f1;
  border-color: #d6d6d6;
}
.avatar-upload .avatar-edit input + label:after {
  content: "\f040";
  font-family: 'FontAwesome';
  color: #757575;
  position: absolute;
  top: 10px;
  left: 0;
  right: 0;
  text-align: center;
  margin: auto;
}
.avatar-upload .avatar-preview {
  width: 192px;
  height: 192px;
  position: relative;
  border-radius: 100%;
  border: 6px solid #F8F8F8;
  box-shadow: 0px 2px 4px 0px rgba(0, 0, 0, 0.1);
  margin: 0 auto;
}
.avatar-upload .avatar-preview > div {
  width: 100%;
  height: 100%;
  border-radius: 100%;
  background-size: cover;
  background-repeat: no-repeat;
  background-position: center;
}
.security-photos{
  background-color: #fcfcfc;
    padding: 10px;
}
.previewContainer{
  background-color: #fff;
    box-shadow: 1px 1px 1px 1px #80808040;
    padding: 25px;
}
  </style>
<body>
  <!-- Sidenav -->
   <!-- Sidenav -->
    <!-- Main content -->
  <div class="main-content" id="panel">
    <!-- Topnav -->
    
  <!-- Main content -->
  <div class="main-content" id="panel">
    <!-- Header -->
    <!-- Header -->
    <div class="header pb-6 d-flex align-items-center" style="min-height: 100px;">
    </div>
    @if (count($errors) > 0)
              <div class="alert alert-danger">
                <strong>Whoops!</strong> There were some problems with your input.<br><br>
                <ul>
                   @foreach ($errors->all() as $error)
                     <li>{{ $error }}</li>
                   @endforeach
                </ul>
              </div>
            @endif
    <!-- Page content -->
    <div class="container-fluid mt--6">
      <div class="row">
        <div class="col-xl-12">        
            <div class="card">
              <div class="card-header">
                <div class="row align-items-center">
                  <div class="col-12">
                    <h3 class="mb-0">Add Your Property</h3>
                  </div>
                 
                </div>
              </div>
              <section class="overlay-wrapper">
                <div class="container-fluid">
                <div class="row">
                <div class="col-lg-12">
                  <!-- MultiStep Form -->
                  <div class="container-fluid" id="grad1">
                    <div class="row justify-content-center mt-0">
                      <div class="col-11  col-lg-12 text-center p-0 mt-3 mb-2">
                        <div class="px-0 pt-4 pb-0 mt-3 mb-3">
                          <!-- <h2><strong>Sign Up Your User Account</strong></h2>
                            <p>Fill all form field to go to next step</p> -->
                          <div class="row">
                            <div class="col-md-12 mx-0">
                              <form id="msform" action=
                              @if(isset($editStatus)){{ url('/dealer/storeproperty') }} @else {{url('dealer/addPropertyData') }}@endif
                               method="post" class="msform" enctype="multipart/form-data">
                              @csrf
                              <input type="hidden" name="dealerId" id="dealerId" value="{{Auth::user()->id}}">
                              @if(isset($property))
                              <input type="hidden" name="productId" id="productId" value="{{$property->id}}">
                              @endif
                                <!-- progressbar -->
                                <ul id="progressbar">
                                  <li class="active" id="account"><strong>Property Type</strong></li>
                                  <li id="personal"><strong>Property Details & Features</strong></li>
                                  <li id="payment"><strong>Property Images</strong></li>                                 
                                  <li id="confirm"><strong>Overview</strong></li>
                                </ul>
                                <!-- fieldsets -->
                                <fieldset>
                                  <div class="form-card">
                                    <h2 class="fs-title">Hey, James</h2>
                                    <p>What type of Property you Selling?</p>
                                    <div class="row">
                                      <div class="col-6">
                                        <select id="toppropertyType" class="form-control"  name="toppropertyType" required>
                                  <option value="">Select Top Property Type</option>
                                  @if(isset($toppropertyType_data))
                                    @foreach ($toppropertyType_data as $toppropertyType)
                                    <option value="{{$toppropertyType->id}}" @if (old('id', isset($property) ? $property->top_property_type_id : NULL) ==
                                                $toppropertyType->id) selected="selected" @endif>{{$toppropertyType->property_type}}</option>
                                   @endforeach
                                   @endif
                                </select>
                                        <div class="invalid-feedback">
                                         This Field is required
                                        </div>
                                      </div>
                                      <div class="col-6">
                                        <select id="propetyType" class="form-control"  name="propertyType" required disabled>
                                        <option value="">Select Sub Property Type</option>
                                        @if(isset($propertyType_data))
                                @foreach ($propertyType_data as $propertyType)
                                     <option value="{{$propertyType->id}}" @if (old('id', isset($property) ? $property->property_type_id : NULL) ==
                                                $propertyType->id) selected="selected" @endif>{{$propertyType->property_type}}</option>
                                  @endforeach
                                @endif
                                </select>
                                        <div class="invalid-feedback">
                                          This Field is required
                                         </div>
                                      </div>                            
                                    </div>
                                  </div>
                                  <input type="button" name="next" class="next next1 action-button"  value="Next Step" />
                                </fieldset>
                                <fieldset>
                                  <div class="form-card">
                                    <div class="">
                                      <h2 class="fs-title">Property Details</h2>
                                      <hr class="my-2">
                                      <div class="row">
                                        <div class="col-lg-12">
                                          <div class="row m-0">
                                            <div class="col-lg-4">
                                              <div class="form-group">
                                                <label class="form-control-label"
                                                  for="input-last-name">Currency*</label>
                                                <div class="input-group mb-3">                                        
                                                  <select id="currency" class="form-control required"  name="currency">
                                      <option value="">Select Currency</option>
                                      @if(isset($currency_data))
                                      @foreach ($currency_data as $currency)
                                        <option value="{{$currency->id}}" @if (old('id', isset($property) ? $property->currency_id : NULL) ==
                                                $currency->id) selected="selected" @endif>{{$currency->currency_code}}</option>
                                      @endforeach
                                      @endif
                                    </select>
                                                </div>
                                              </div>
                                            </div>
                                            <div class="col-lg-4">
                                              <div class="form-group">
                                                <label class="form-control-label"
                                                  for="input-last-name">Country*</label>
                                                <select id="countryId" class="form-control"  name="countryId" required>
                                <option value="">Select Country</option>
                                @if(isset($country_data))
                                @foreach ($country_data as $country)
                                 <option value="{{$country->id}}" @if (old('id', isset($property) ? $property->country_id : NULL) ==
                                                $country->id) selected="selected" @endif>{{$country->country_name}}</option>
                               @endforeach
                               @endif
                                </select>
                                              </div>
                                            </div>                                  
                                          </div>                     
                                          <div class="row m-0">
                                            <div class="col-lg-4">
                                              <div class="form-group">
                                                <label class="form-control-label"
                                                  for="input-last-name">State</label>
                                                <select id="stateId"  name="stateId" class="form-control" disabled>
                                <option value="" >Select State</option>
                                @if(isset($stateSelected))
                                @foreach ($state_data as $state)
                                 <option value="{{$state->id}}" @if (old('id', isset($property) ? $property->state_id : NULL) ==
                                                $state->id) selected="selected" @endif>{{$state->state_name}}</option>
                               @endforeach
                               @endif
                                </select>
                                              </div>
                                            </div>
                                            <div class="col-lg-4">
                                              <div class="form-group">
                                                <label class="form-control-label"
                                                  for="input-last-name">City*</label>
                                                <select id="cityId" name="cityId" class="form-control required" disabled>
                                <option value="">Select City</option>
                                @if(isset($city_data))
                                @foreach ($city_data as $city)
                                 <option value="{{$city->id}}" @if (old('id', isset($property) ? $property->city_id : NULL) ==
                                                $city->id) selected="selected" @endif>{{$city->city_name}}</option>
                               @endforeach
                               @endif
                                </select>
                                              </div>
                                            </div>
                                            <div class="col-lg-4">
                                              <div class="form-group">
                                                <label class="form-control-label"
                                                  for="input-last-name">Establisment Type* </label>
                                                <select id="establishmentType" class="form-control required" name="establishmentType" required>
                                  
                                  @if(isset($property))
                                  <option @if($property->establishment_type) @if($property->establishment_type==0) selected @endif @endif value="0">New</option>
                                  <option @if($property->establishment_type) @if($property->establishment_type==1) selected @endif @endif value="1">Established</option>
                                  @else
                                  <option value="">Select Establishment Type</option>
                                  <option  value="0">New</option>
                                  <option  value="1">Established</option>
                                  @endif
                                </select>
                                                <div class="invalid-feedback">
                                                  This Field is required
                                                 </div>
                                                
                                              </div>
                                            </div>
                                            <div class="col-lg-4">
                                              <div class="form-group">
                                                <label class="form-control-label"
                                                  for="input-last-name">Bedroom* </label>
                                                <select id="bedRoom" class="form-control required"   name="bedRoom" required>
                                  <option value="">Select Bedroom</option>
                                  @if(isset($bedroom_data))
                                  @foreach ($bedroom_data as $bedroom)
                                     <option value="{{$bedroom->id}}" @if (old('id', isset($property) ? $property->bed_id : NULL) ==
                                                $bedroom->id) selected="selected" @endif>{{$bedroom->number_of_bedrooms}}</option>
                                   @endforeach
                                   @endif
                                </select>
                                                <div class="invalid-feedback">
                                                  This Field is required
                                                 </div>
                                              </div>
                                            </div>
                                            <div class="col-lg-4">
                                              <div class="form-group">
                                                <label class="form-control-label"
                                                  for="input-last-name">Bathroom </label>
                                                <select id="bathRoom" class="form-control required"  name="bathRoom" required>
                                  <option value="">Select Bathroom</option>
                                  @if(isset($bathroom_data))
                                  @foreach ($bathroom_data as $bathroom)
                                   <option value="{{$bathroom->id}}" @if (old('id', isset($property) ? $property->bathroom_id : NULL) ==
                                                $bathroom->id) selected="selected" @endif>{{$bathroom->number_of_bathrooms}}</option>
                                   @endforeach
                                   @endif
                                </select>
                                              </div>
                                            </div>
                                            <div class="col-lg-4">
                                              <div class="form-group">
                                                <label class="form-control-label"
                                                  for="input-last-name">Property Metric* </label>
                                                <select id="metric" class="form-control required"  name="metric" required>
                                  @if(isset($property))
                                  <option @if($property->metric ?? '') @if($property->metric=='sqft') selected @endif @endif value="sqft">SQFT</option>
                                  <option @if($property->metric ?? '') @if($property->metric=='sqmtr') selected @endif @endif value="sqmtr">SQMTR</option>
                                  @else
                                  <option value="">Select Property Metric</option>
                                <option value="sqft">SQFT</option>
                                <option value="sqmtr">SQMTR</option>
                                @endif
                               </select>
                                                <div class="invalid-feedback">
                                                  This Field is required
                                                 </div>
                                              </div>
                                            </div>
                                            <div class="col-lg-4">                                   
                                              <div class="form-group">
                                                <label class="form-control-label"
                                                  for="input-last-name">Property Size*</label>
                                                  <input id="sol" class="form-control form-control-user" type="number" min="0"  name="sol" placeholder="Enter Size of Property" value="{{old('sol', isset($property->land_size) ? $property->land_size : NULL)}}" required>    
                                                  <div class="invalid-feedback">
                                                    This Field is required
                                                   </div>                               
                                              </div>
                                            </div>
                                            <div class="col-lg-4">                                   
                                              <div class="form-group">
                                                <label class="form-control-label"
                                                  for="input-last-name">Building Size*</label>
                                                  <input id="buildingSize" class="form-control form-control-user" type="number" min="0"  name="buildingSize" placeholder="Enter Building Size" value="{{old('buildingSize', isset($property->building_size) ? $property->building_size : NULL)}}" required>                                   
                                              </div>
                                            </div>   
                                            <div class="col-lg-4">
                                              <div class="form-group">
                                                <label class="form-control-label"
                                                  for="input-last-name">Property Sale/Lease* </label>
                                                <select id="propertySL" class="form-control required" name="propertySL" required>
                                  @if(isset($property))
                                  <option @if($property->property_sale_lease ?? '') @if($property->property_sale_lease==0) selected @endif @endif value="0" >Sale</option>
                                  <option @if($property->property_sale_lease ?? '') @if($property->property_sale_lease==1) selected @endif @endif value="1" >Lease</option>
                                  @else
                                  <option value="">Sale/Lease</option>
                                  <option value="0">Sale</option>
                                  <option value="1">Lease</option>
                                  @endif
                                </select>
                                                <div class="invalid-feedback">
                                                  This Field is required
                                                 </div>
                                              </div>
                                            </div>
                                            <div class="col-lg-4">                                   
                                              <div class="form-group">
                                                <label class="form-control-label"
                                                  for="input-last-name">Unit Number</label>
                                                  <input id="unitNumber" class="form-control form-control-user" type="number" min="0"  name="unitNumber" placeholder="Enter Unit Number" value="{{old('unitNumber', isset($property->unit_number) ? $property->unit_number : NULL)}}" required>                                   
                                              </div>
                                            </div> 
                                            <div class="col-lg-4">
                                              <div class="form-group">
                                                <label class="form-control-label"
                                                  for="input-last-name">Number of Parking*</label>
                                                <select id="parking" class="form-control"  name="parking" required>
                                  <option value="">Select Parking</option>
                                  @if(isset($parking_data))
                                  @foreach ($parking_data as $parking)
                                   <option value="{{$parking->id}}" @if (old('id', isset($property) ? $property->parking_id : NULL) ==
                                                $parking->id) selected="selected" @endif>{{$parking->number_of_balconies}}</option>
                                   @endforeach
                                   @endif
                                </select> 
                                                <div class="invalid-feedback">
                                                  This Field is required
                                                 </div>
                                              </div>
                                            </div>
                                            <!-- <div class="col-lg-4">
                                              <div class="form-group">
                                                <label class="form-control-label"
                                                  for="input-last-name">Price on Request</label>
                                                <select id="price_on_request" class="form-control" name="price_on_request" required>
                                                @if(isset($property))
                                                <option @if($property->price_on_request ?? '' ) @if($property->price_on_request==0) selected @endif @endif value="0">No</option>
                                  <option @if($property->price_on_request ?? '' ) @if($property->price_on_request==1) selected @endif @endif value="1">Yes</option>
                                  @else
                        <option value="">Select Price On Request</option>
                        <option value="0">No</option>
                        <option value="1">Yes</option>
                        @endif
                      </select>
                                              </div>
                                            </div> -->
                                            <div class="col-lg-4">
                                              <div class="form-group">
                                                <label class="form-control-label"
                                                  for="input-last-name">Price*</label>
                                                <select id="price" class="form-control"  name="price" required>
                                  <option value="">Select Price</option>
                                  @if(isset($prices))
                                  @foreach ($prices as $price)
                                   <option @if($priceSelected->id ?? '') @if($priceSelected->id==$price->id) selected @endif @endif value="{{$price->id}}">{{$price->price}}</option>
                                  @endforeach
                                  @endif
                                </select>
                                                <div class="invalid-feedback">
                                                  This Field is required
                                                 </div>
                                              </div>
                                            </div>
                                            <div class="col-lg-4">                                   
                                              <div class="form-group">
                                                <label class="form-control-label"
                                                  for="input-last-name">property Price*</label>
                               @if(isset($priceSelected))                   
                               @if($priceSelected->id == 3)
                                <input id="priceSqft" class="form-control form-control-user required" type="text"  name="priceSqft" placeholder="Enter Property Price" @if($property->property_price ?? '')  value="{{ $property->property_price}}" @endif disabled>
                                @else
                                <input id="priceSqft" class="form-control form-control-user required" type="text"  name="priceSqft" placeholder="Enter Property Price" @if($property->property_price ?? '')  value="{{ $property->property_price}}" @endif >
                                @endif
                                @else
                                <input id="priceSqft" class="form-control form-control-user required" type="text"  name="priceSqft" placeholder="Enter Property Price" value="">
                                @endif
                                                  <div class="invalid-feedback">
                                                    This Field is required
                                                   </div>                                
                                              </div>
                                            </div>
                                            
                                            <div class="col-lg-4">
                                              <div class="form-group">
                                                <label class="form-control-label"
                                                  for="input-last-name">Build Year*</label>
                                                <select id="builtId" class="form-control " style="" name="builtId" required>
                                <option value="">Select Build Year</option>
                                @if(isset($buildYear_data))
                                @foreach ($buildYear_data as $buildYear)
                                                 <option value="{{$buildYear->id}}" @if (old('id', isset($property) ? $property->year_built_id : NULL) ==
                                                $buildYear->id) selected="selected" @endif>{{$buildYear->build_year}}</option>
                                            @endforeach
                                @endif            
                                </select>
                                                <div class="invalid-feedback">
                                                  This Field is required
                                                 </div>
                                              </div>
                                            </div>
                                            <div class="col-lg-4">                                   
                                              <div class="form-group">
                                                <label class="form-control-label"
                                                  for="input-last-name">Street 1*</label>
                                                  <input id="street_1" class="form-control form-control-user" type="text"  name="street_1" placeholder="Enter Street 1" value="{{old('street_1', isset($property->street_1) ? $property->street_1 : NULL)}}" required>     
                                                  <div class="invalid-feedback">
                                                    This Field is required
                                                   </div>                              
                                              </div>
                                            </div>
                                            <div class="col-lg-4">                                   
                                              <div class="form-group">
                                                <label class="form-control-label"
                                                  for="input-last-name">Street 2*</label>
                                                  <input id="street_2" class="form-control form-control-user" type="text"  name="street_2" placeholder="Enter Street 2" value="{{old('street_2', isset($property->street_2) ? $property->street_2 : NULL)}}" required>   
                                                  <div class="invalid-feedback">
                                                    This Field is required
                                                   </div>                                
                                              </div>
                                            </div>
                                            <div class="col-lg-4">                                   
                                              <div class="form-group">
                                                <label class="form-control-label"
                                                  for="input-last-name">Postal Code*</label>
                                                  <input id="postalCode" class="form-control form-control-user " type="text"  name="postalCode" placeholder="Enter Postal Code" value="{{old('postalCode', isset($property->postal_code) ? $property->postal_code : NULL)}}" required> 
                                                  <div class="invalid-feedback">
                                                    This Field is required
                                                   </div>                                  
                                              </div>
                                            </div>
                                            <div class="col-lg-4">
                                              <div class="form-group">
                                                <label class="form-control-label"
                                                  for="input-last-name">Timezone</label>
                                                <select id="timezone" class="form-control" required name="timezone">
                            <option value="">Select Timezone</option>
                                @if(isset($timezone))
                                @foreach ($timezone as $zone)
                                 <option value="{{$zone->id}}" @if (old('id', isset($property) ? $property->timezone_id : NULL) ==
                                                $zone->id) selected="selected" @endif>{{$zone->name .' '. $zone->offset}}</option>
                               @endforeach
                               @endif
                          </select>
                                                <div class="invalid-feedback">
                                                  This Field is required
                                                 </div>
                                              </div>
                                            </div>
                                            <div class="col-lg-4">
                                              <div class="form-group">
                                                <label class="form-control-label"
                                                  for="input-last-name">Status</label>
                                                <select id ="status" name="status" class="form-control required" >
                          @if(isset($property))
                          <option value="1" {{$property->status == "1" ? 'selected' : ''}}>Active</option>
                           <option value="0" {{$property->status == "0" ? 'selected' : ''}}>Inactive</option>
                          @else
                          <option value="" >Select Status</option>
                           <option value="1" >Active</option>
                           <option value="0" >Inactive</option>
                           @endif
                        </select>
                                                <div class="invalid-feedback">
                                                  This Field is required
                                                 </div>
                                              </div>
                                            </div>
                                          </div>                                           
                                          <h2 class="fs-title mt-5">Additional  Features </h2>
                                          <hr class="my-2">                                                    
                                          <div class="accordion-1">
                                            <div class="container">
                                              <div class="row">
                                                <div class="col-md-12 ml-auto">
                                                  <div class=""
                                                    id="accordionExample">
                                                    <div class=" card p-0 mb-1">
                                                      <div class="card-header" id="headingOne">
                                                        <h5 class="mb-0">
                                                          <button class="btn btn-link w-100 text-primary text-left" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" 
                                                          aria-controls="collapseOne"> Features <i class="ni ni-bold-down float-right"></i>
                                                        </button>
                                                        </h5>
                                                      </div>
                                                      <div id="collapseOne"
                                                        class="collapse show"
                                                        aria-labelledby="headingOne"
                                                        data-parent="#accordionExample">
                                                        <div class="mt-3 p-4">
                                                          <div class="row">
                                                            <div class="col-lg-6">
                                                              <div class="inner">
                                                                
                                                                @if(isset($html1))
                                                                <?php echo $html1; ?>
                                                                @endif
                                                              </div>
                                                            </div>
                                                            <div class="col-lg-6">
                                                              <div class="inner">
                                                              @if(isset($html2))                                                               
                                                                <?php echo $html2; ?>
                                                                @endif
                                                              </div>
                                                            </div>
                                                          </div>
                                                         
                                                          
                                                                                      
                                                        </div>    
                                                      </div>
                                                    </div>
                                                    <div class="card p-0 mb-1">
                                                      <div class="card-header"
                                                        id="headingTwo">
                                                        <h5 class="mb-0">
                                                          <button
                                                            class="btn btn-link w-100 text-primary text-left collapsed"
                                                            type="button"
                                                            data-toggle="collapse"
                                                            data-target="#collapseTwo"
                                                            aria-expanded="false"
                                                            aria-controls="collapseTwo">
                                                          Additional Features
                                                          <i
                                                            class="ni ni-bold-down float-right"></i>
                                                          </button>
                                                        </h5>
                                                      </div>
                                                      <div id="collapseTwo" class="collapse"
                                                        aria-labelledby="headingTwo"
                                                        data-parent="#accordionExample">
                                                      
                                                       <div class="row m-0">                                             
                                                        <div class="col-lg-12 p-0">
                                                          <div class="additionalFeature p-5">
                                                            <table class="table table-bordered">
                                                              <thead>
                                                                <tr>
                                                                  <th>Àdd features
                                                                  </th>
                                                                  <th>Action</th>
                                                                </tr>
                                                              </thead>
                                                              <tbody>
                                                              @if(isset($additionalHtml))
                                                               <?php echo $additionalHtml; ?>
                                                              @endif
                                                                <tr>
                                                                  <td><input type="text" id="additional-feature" class="form-control">
                                                                  </td>
                                                                  <td><button class="btn btn-success" type="button" id="addRow"><i class="fa fa-plus"></i></button>
                                                                  </td>
                                                                </tr>
                                                              </tbody>
                                                            </table>
                                                          </div>
                                                        </div>
                                                      </div> 
                                                      </div>
                                                    </div>
                                                  </div>
                                                </div>
                                              </div>
                                            </div>
                                          </div>
                                        </div>
                                        <div class="col-lg-4 d-none">
                                          <div class="watchImageContainer">
                                            <div class="Image">
                                              <img src="images/watch4.jpg" alt="">
                                            </div>
                                            <div class="title mt-2">
                                              <h6>Title</h6>
                                              <p class="muted">Rolex GT 2 </p>
                                            </div>
                                            <div class="price">
                                              <h6>Price</h6>
                                              <p class="font-weight-bold">15,000 </p>
                                            </div>
                                            <hr class="my-2">
                                          </div>
                                        </div>
                                      </div>
                                      
                                    </div>
                                   
                                  </div>
                                  <div>
                                  </div>
                                  <input type="button" name="previous"
                                    class="previous action-button-previous" value="Previous" />
                                  <input type="button" name="next" class="next next2 action-button" value="Next Step" />
                                </fieldset>
                                <fieldset>
                                  <div class="form-card">
                                    <h2 class="fs-title">Attach Photos</h2>
                                    <p>Attach your Property Images here, you can drag the images here as well </p>
                                    <div class="row">
                                      <div class="col-lg-12">
                                        <div class="inner">
                                        @if(isset($images_data))
                                        @foreach ($images_data as $data)
                                    <tr >
                                      <td>
                                        <img src="{{url('uploads/'.$data->filename)}}" id="img_{{$data->id}}" class="img-thumbnail" width="75" />
                                      </td>
                                      <td>
                                        <button type="button" class="imgDelete" id="imgDelete_{{$data->id}}">
                                          <i class="fas fa-trash-alt"></i>
                                        </button> &nbsp; &nbsp;
                                    </td>
                                    </tr>
                                  @endforeach
                                  @endif
                                          <div class="input-field">                                
                                            <div class="input-images-1"  style="padding-top: .5rem;"></div>
                                          </div>
                                          <hr class="my-2">
                                        </div>
                                      </div>
                                    </div>  
                                    <div class="row">
                                      <div class="col-lg-12">
                                        <div class="inner mt-4">
                                          <div class="form-group">
                                            <label class="form-control-label" for="description">Add your description below*</label>
                                            <textarea name="comment" id="comment" cols="30" rows="4" required class="form-control">
                                            @if(isset($property))
                                            @if($comment_data->comment ?? '') {{strip_tags($comment_data->comment)}} @endif
                                            @endif
                                            </textarea>
                                            <div class="invalid-feedback">
                                              This Field is required
                                             </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>                                                                                          
                                  </div>
                                  <input type="button" name="previous"
                                    class="previous action-button-previous" value="Previous" />
                                  <input type="button" name="next" class="next action-button next3"  value="Next Step" />
                                </fieldset>
                                <fieldset>
                                  <div class="form-card">
                                  <div class="col-lg-12">
                                  <div class="search-result">
                                      <div class="details-box row mt-4 m-0">
                                          <div class="col-lg-12 text-left">
                                              <h5>Ads Information</h5>
                                          </div>
                                          <div class="col-md-12">
                                              <div
                                                  class="stm-listing-single-price-title heading-font clearfix text-left">
                                                  <div class="stm-single-title-wrap">
                                                      <h1 class="title">
                                                         4 BHK 
                                                      </h1>
                                                  </div>
                                              </div>
                                              <div class="row">                                       
                                                  <div class="col-lg-12">
                                                      <p>Normal Images</p>
                                                      <div id="imgGallery" class="d-flex flex-wrap">
                                                      </div>
                                                  </div>                                               
                                              </div>
                                              <!-- End single-product-images -->                                   
                                              <div class="property-desc border-top pt-3">
          
                                                <div class="stm-border-top-unit bg-color-border">
                                                    <h5><strong>Description</strong></h5>
                                                </div>
                    
                                                <div id="previewcomment">
                                                    <p >lorem ipsum is a dummy text</p>
                                                </div>
                    
                                                <div class="stm-border-top-unit bg-color-border">
                                                    <h5><strong>Basic Details</strong></h5>
                                                </div>
                                                <div class="table-responsive">
                                                    <table class="table table-border" id="feature_table">
                                                      <tr><th>Property Type</th><td>Commerical</td><th>Sub Property Type</th><td>Commerical</td></tr>
                                                      <tr><th>Country</th><td>Residential</th><th>City</th><td>Commerical</td></tr>
                                                      <tr><th>Price</th><td>Single</td><th>No. of Bedrooms</th><td>Commerical</td></tr>
                                                      <tr><th>No. of Bedrooms</th><td>3BHK</td>
                                                    </table>
                                                </div>
                                                <!-- Start of the report section -->          
                    
                                            </div>
                                          </div>
                                      </div>                                                                         
                                                                
                                  </div>
                                  </div>
                                  </div>
                                  <input type="button" name="previous" class="previous action-button-previous" value="Previous" />
                                  <input id="postNow" type="submit" name="postnow" class="btn btn-success action-button next4" value="Post ads Now" data-toggle="modal" data-target="#paymentNow" />
                              </fieldset>                                
                              </form>
                              </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </section>
            </div>
        </div>
      </div>
      
    </div>
  </div>
  @stop
  


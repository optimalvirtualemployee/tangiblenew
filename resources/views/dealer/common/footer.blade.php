      <!-- Footer -->
      <footer class="footer pt-0">
        <div class="row align-items-center justify-content-lg-between">
          <div class="col-lg-6">
            <div class="copyright text-center  text-lg-left  text-muted">
              &copy; 2020 <a href="#" class="font-weight-bold ml-1" target="_blank">Tangible Inc.</a>
            </div>
          </div>
          <div class="col-lg-6">
            <ul class="nav nav-footer justify-content-center justify-content-lg-end">
              <li class="nav-item">
                <a href="#" class="nav-link" target="_blank">About Us</a>
              </li>
              <li class="nav-item">
                <a href="#" class="nav-link" target="_blank">Blog</a>
              </li>
              <li class="nav-item">
                <a href="#" class="nav-link" target="_blank">Contact Us</a>
              </li>
            </ul>   
          </div>
        </div>
      </footer>
    </div>
  </div>
  <!-- Core -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="{{url('assets/vendor/jquery/dist/jquery.min.js')}}"></script>
  <script src="{{url('assets/vendor/bootstrap/dist/js/bootstrap.bundle.min.js')}}"></script>
  <script src="{{url('assets/vendor/js-cookie/js.cookie.js')}}"></script>
  <script src="{{url('assets/vendor/jquery.scrollbar/jquery.scrollbar.min.js')}}"></script>
  <script src="{{url('assets/vendor/jquery-scroll-lock/dist/jquery-scrollLock.min.js')}}"></script>
  <script src="{{url('assets/js/main.min.js')}}"></script>

	<!-- dealer management -->
	
	<script>

	const togglePassword = document.querySelector('#togglePasswordEye');
    const password = document.querySelector('#adminPassword');

    adminPassword.addEventListener("focus", function(i) {
      $('#togglePasswordEye').show();
    });

    togglePassword.addEventListener('click', function (e) {
        // toggle the type attribute
        const type = password.getAttribute('type') === 'password' ? 'text' : 'password';
        password.setAttribute('type', type);
        // toggle the eye slash icon
        this.classList.toggle('fa-eye-slash');
    });
  
    /*  ==========================================
        SHOW UPLOADED IMAGE
    * ========================================== */
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#imageResult')
                    .attr('src', e.target.result);
            };
            reader.readAsDataURL(input.files[0]);
        }
    }

    $(function () {
        $('#upload').on('change', function () {
            readURL(input);
        });
    });

    /*  ==========================================
        SHOW UPLOADED IMAGE NAME
    * ========================================== */
    var input = document.getElementById( 'upload' );
    var infoArea = document.getElementById( 'upload-label' );

    input.addEventListener( 'change', showFileName );
    function showFileName( event ) {
      var input = event.srcElement;
      var fileName = input.files[0].name;
      infoArea.textContent = 'File name: ' + fileName;
    }	
	
	  $(document).ready(function() {	
      $("#countryId").change(function(){

      $('#loading-image').show();
        var countryId = $(this).val();
        $.ajax({
          url: "/dealer/getState",
          type: "POST",
          data: {countryId: countryId, _token: '{{csrf_token()}}' },
          dataType: 'json',
          success : function(data){
            var html = `<option value="">Select State</option>`;
            for (var i = 0; i < data.length; i++) {
                var id = data[i].id;
                var name = data[i].state_name;
                var option = `<option value="${data[i].id}">${data[i].state_name}</option>`;
                html += option;
              }
              $('#stateId').html(html); 
            }
          });
        $.ajax({
      url: "/dealer/getCity",
      type: "POST",
      data: {countryId: countryId  ,_token: '{{csrf_token()}}' },
      dataType: 'json',
      success : function(data){
        var html = `<option value="">Select City</option>`;
        for (var i = 0; i < data.length; i++) {
            var id = data[i].id;
            var name = data[i].city_name;
            var option = `<option value="${data[i].id}">${data[i].city_name}</option>`;
            html += option;
          }
        $('#loading-image').hide();
          $('#cityId').html(html);  
        }
      });
      });

	      // $("#countryId").change(function(){
	      //     alert('here');
	    	 //  var countryId = $(this).val();
	    	 //  $('#loading-image').show();
	    	 //  $.ajax({
	    	 //  	url: "/dealer/getCity",
	    	 //  	type: "POST",
	    	 //  	data: {countryId: countryId  ,_token: '{{csrf_token()}}' },
	    	 //  	dataType: 'json',
	    	 //  	success : function(data){
	    	 //  		var html = `<option value="">Select City</option>`;
	    	 //  		for (var i = 0; i < data.length; i++) {
	    	 //  			  var id = data[i].id;
	    	 //  			  var name = data[i].city_name;
	    	 //  			  var option = `<option value="${data[i].id}">${data[i].city_name}</option>`;
	    	 //  			  html += option;
	    	 //  			}
	    	 //  		$('#loading-image').hide();
	    	 //  			$('#cityId').html(html);	
	    	 //  		}
	    	 //  	});
	    	 //  });
	      });
	
    $('#edit_form_dm').on('click',function(){
      $('.edit-info-form').toggleClass('d-none');
      $('.infoView').toggleClass('d-none');       
    });

    $('.add-business-hours').on('click',function(){
      $('.business-hours-form').toggleClass('d-none');
      $('.business-hours-view').toggleClass('d-none');       
    });
    <!-- dealer management -->
  </script>

</body>

</html>
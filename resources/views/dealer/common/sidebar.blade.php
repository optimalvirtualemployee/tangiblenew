  <!-- Sidenav -->
  <nav class="sidenav navbar navbar-vertical  fixed-left  navbar-expand-xs navbar-light " id="sidenav-main">
    <div class="scrollbar-inner">
      <!-- Brand -->
      <div class="sidenav-header  align-items-center">
        <a class="navbar-brand" href="{{url('/dealer/dashboard')}}">
          <img src="{{url('assets/img/brand/blue.png')}}" class="navbar-brand-img" alt="...">
        </a>
      </div>
      <div class="navbar-inner">
        <!-- Collapse -->
        <div class="collapse navbar-collapse" id="sidenav-collapse-main">
          <!-- Nav items -->
          <ul class="navbar-nav">

            <li class="nav-item">
              <a class="nav-link" href="{{url('/dealer/dashboard')}}">
                <i class="ni ni-shop text-orange2"></i>
                <span class="nav-link-text">Dashboard</span>
              </a>
            </li>
            <li class="nav-item">

              <a class="nav-link" href="@if(isset($dealer)) 
              @if($dealer->category == 'Automobiles') {{ url('/dealer/addAuto') }} 
              @elseif(($dealer->category == 'Watch')) {{ url('/dealer/addWatch') }}
              @elseif(($dealer->category == 'Real Estate')) {{ url('/dealer/addProperty') }} 
              @endif 
              @endif ">
                <i class="ni ni-planet text-orange2"></i>
                <span class="nav-link-text">Add Listing</span>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="{{url('/dealer/inventoryList')}}">
                <i class="ni ni-planet text-orange2"></i>
                <span class="nav-link-text">Inventory</span>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="{{url('/dealer/enquiryList')}}">
                <i class="ni ni-notification-70 text-orange2"></i>
                <span class="nav-link-text">Enquiry</span>
              </a>
            </li>
            <li class="nav-item dropdown">
              <a class="nav-link collapsed" href="#dealer-dashboards" data-toggle="collapse" role="button" aria-expanded="false" aria-controls="dealer-dashboards">
                <i class="ni ni-settings text-orange2"></i>
                <span class="nav-link-text">Settings</span>
              </a>
              <div class="collapse" id="dealer-dashboards">
                <ul class="nav nav-sm flex-column">
                  <li class="nav-item">
                    <a href="{{url('/dealer/listagents')}}" class="nav-link">
                      <span class="sidenav-normal"> All Agent </span>
                    </a>
                  </li>
                  <li class="nav-item">
                    <a href="{{url('/dealer/addUser')}}" class="nav-link">
                      <span class="sidenav-normal"> Add Agent </span>
                    </a>
                  </li>
                  <li class="nav-item">
                    <a href="/dealer/dealerManagement" class="nav-link">
                      <span class="sidenav-normal"> Dealership Management</span>
                    </a>
                  </li>
                </ul>
              </div>
            </li>
            <!-- <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Dropdown
              </a>
              <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                <a class="dropdown-item" href="#">Action</a>
                <a class="dropdown-item" href="#">Another action</a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="#">Something else here</a>
              </div>
            </li> -->
            <!--<li class="nav-item">-->
            <!--  <a class="nav-link" href="#">-->
            <!--    <i class="ni ni-notification-70 text-orange2"></i>-->
            <!--    <span class="nav-link-text">Promote</span>-->
            <!--  </a>-->
            <!--</li>-->
            <!--<li class="nav-item">-->
            <!--  <a class="nav-link" href="#">-->
            <!--    <i class="ni ni-send text-orange2"></i>-->
            <!--    <span class="nav-link-text">Marketing</span>-->
            <!--  </a>-->
            <!--</li>-->
            <!--<li class="nav-item">-->
            <!--  <a class="nav-link" href="#">-->
            <!--    <i class="ni ni-chart-pie-35 text-orange2"></i>-->
            <!--    <span class="nav-link-text">Reports</span>-->
            <!--  </a>-->
            <!--</li>-->
            <!--<li class="nav-item">-->
            <!--  <a class="nav-link" href="#">-->
            <!--    <i class="ni ni-settings text-orange2"></i>-->
            <!--    <span class="nav-link-text">Settings</span>-->
            <!--  </a>-->
            <!--</li>-->
          </ul>
          <!-- Divider -->
          <hr class="my-3">
          <!-- Heading -->
          <!-- <h6 class="navbar-heading p-0 text-muted">
            <span class="docs-normal">Documentation</span>
          </h6> -->
          <!-- Navigation -->
          <ul class="navbar-nav mb-md-3">
            <!--<li class="nav-item">-->
            <!--  <a class="nav-link collapsed" href="#navbar-account" data-toggle="collapse" role="button" aria-expanded="false" aria-controls="navbar-account">-->
            <!--    <i class="ni ni-circle-08"></i>-->
            <!--    <span class="nav-link-text">My Account</span>-->
            <!--  </a>-->
            <!--  <div class="collapse" id="navbar-account">-->
            <!--    <ul class="nav nav-sm flex-column">-->
            <!--      <li class="nav-item">-->
            <!--        <a href="#" class="nav-link">-->
            <!--          <span class="sidenav-normal"> Edit Your Profile </span>-->
            <!--        </a>-->
            <!--      </li>-->
            <!--      <li class="nav-item">-->
            <!--        <a href="#" class="nav-link">-->
            <!--          <span class="sidenav-normal"> View Your Bill </span>-->
            <!--        </a>-->
            <!--      </li>-->
            <!--      <li class="nav-item">-->
            <!--        <a href="#" class="nav-link">-->
            <!--          <span class="sidenav-normal"> View Invoice </span>-->
            <!--        </a>-->
            <!--      </li>-->
            <!--    </ul>-->
            <!--  </div>-->
            <!--</li>-->

            <!--<li class="nav-item">-->
            <!--  <a class="nav-link" href="#" target="_blank">-->
            <!--    <i class="ni ni-headphones"></i>-->
            <!--    <span class="nav-link-text">Help Center</span>-->
            <!--  </a>-->
            <!--</li>-->
            <!--<li class="nav-item">-->
            <!--  <a class="nav-link" href="#" target="_blank">-->
            <!--    <i class="ni ni-email-83"></i>-->
            <!--    <span class="nav-link-text">Email</span>-->
            <!--  </a>-->
            <!--</li>-->
            <li class="nav-item">
              <a class="nav-link active active-pro" href="{{url('dealer/logout')}}">
                <i class="ni ni-button-power text-orange2"></i>
                <span class="nav-link-text text-orange2">Log Out</span>
              </a>
            </li>
          </ul>
        </div>
      </div>
    </div>
  </nav>
  <!-- Main content -->
  <div class="main-content" id="panel">
    <!-- Topnav -->
    <nav class="navbar navbar-top navbar-expand navbar-dark bg-primary border-bottom">
      <div class="container-fluid">
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
          <!-- Search form -->
          <form class="navbar-search navbar-search-light form-inline mr-sm-3" id="navbar-search-main">
            <div class="form-group mb-0">
              <div class="input-group input-group-alternative input-group-merge">
                <div class="input-group-prepend">
                  <span class="input-group-text"><i class="fas fa-search"></i></span>
                </div>
                <input class="form-control" placeholder="Search" type="text">
              </div>
            </div>
            <button type="button" class="close" data-action="search-close" data-target="#navbar-search-main" aria-label="Close">
              <span aria-hidden="true">×</span>
            </button>
          </form>
          <!-- Navbar links -->
          <ul class="navbar-nav align-items-center  ml-md-auto ">
            <!-- <li class="nav-item d-xl-none">
              <div class="pr-3 sidenav-toggler sidenav-toggler-dark" data-action="sidenav-pin" data-target="#sidenav-main">
                <div class="sidenav-toggler-inner">
                  <i class="sidenav-toggler-line"></i>
                  <i class="sidenav-toggler-line"></i>
                  <i class="sidenav-toggler-line"></i>
                </div>
              </div>
            </li>
            <li class="nav-item d-sm-none">
              <a class="nav-link" href="#" data-action="search-show" data-target="#navbar-search-main">
                <i class="ni ni-zoom-split-in"></i>
              </a>
            </li>
            <li class="nav-item dropdown">
              <a class="nav-link" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="ni ni-bell-55"></i>
              </a>
              <div class="dropdown-menu dropdown-menu-xl  dropdown-menu-right  py-0 overflow-hidden">
                <div class="px-3 py-3">
                  <h6 class="text-sm text-muted m-0">You have <strong class="text-primary">13</strong> notifications.</h6>
                </div>
                <div class="list-group list-group-flush">
                  <a href="#!" class="list-group-item list-group-item-action">
                    <div class="row align-items-center">
                      <div class="col-auto">
                        <img alt="Image placeholder" src="/assets/img/theme/team-1.jpg" class="avatar rounded-circle">
                      </div>
                      <div class="col ml--2">
                        <div class="d-flex justify-content-between align-items-center">
                          <div>
                            <h4 class="mb-0 text-sm">John Snow</h4>
                          </div>
                          <div class="text-right text-muted">
                            <small>2 hrs ago</small>
                          </div>
                        </div>
                        <p class="text-sm mb-0">Let's meet at Starbucks at 11:30. Wdyt?</p>
                      </div>
                    </div>
                  </a>
                  <a href="#!" class="list-group-item list-group-item-action">
                    <div class="row align-items-center">
                      <div class="col-auto">
                        <img alt="Image placeholder" src="/assets/img/theme/team-2.jpg" class="avatar rounded-circle">
                      </div>
                      <div class="col ml--2">
                        <div class="d-flex justify-content-between align-items-center">
                          <div>
                            <h4 class="mb-0 text-sm">John Snow</h4>
                          </div>
                          <div class="text-right text-muted">
                            <small>3 hrs ago</small>
                          </div>
                        </div>
                        <p class="text-sm mb-0">A new issue has been reported.</p>
                      </div>
                    </div>
                  </a>
                  <a href="#!" class="list-group-item list-group-item-action">
                    <div class="row align-items-center">
                      <div class="col-auto">
                        <img alt="Image placeholder" src="/assets/img/theme/team-3.jpg" class="avatar rounded-circle">
                      </div>
                      <div class="col ml--2">
                        <div class="d-flex justify-content-between align-items-center">
                          <div>
                            <h4 class="mb-0 text-sm">John Snow</h4>
                          </div>
                          <div class="text-right text-muted">
                            <small>5 hrs ago</small>
                          </div>
                        </div>
                        <p class="text-sm mb-0">Your posts have been liked a lot.</p>
                      </div>
                    </div>
                  </a>
                  <a href="#!" class="list-group-item list-group-item-action">
                    <div class="row align-items-center">
                      <div class="col-auto">
                        <img alt="Image placeholder" src="/assets/img/theme/team-4.jpg" class="avatar rounded-circle">
                      </div>
                      <div class="col ml--2">
                        <div class="d-flex justify-content-between align-items-center">
                          <div>
                            <h4 class="mb-0 text-sm">John Snow</h4>
                          </div>
                          <div class="text-right text-muted">
                            <small>2 hrs ago</small>
                          </div>
                        </div>
                        <p class="text-sm mb-0">Let's meet at Starbucks at 11:30. Wdyt?</p>
                      </div>
                    </div>
                  </a>
                  <a href="#!" class="list-group-item list-group-item-action">
                    <div class="row align-items-center">
                      <div class="col-auto">
                        <img alt="Image placeholder" src="/assets/img/theme/team-5.jpg" class="avatar rounded-circle">
                      </div>
                      <div class="col ml--2">
                        <div class="d-flex justify-content-between align-items-center">
                          <div>
                            <h4 class="mb-0 text-sm">John Snow</h4>
                          </div>
                          <div class="text-right text-muted">
                            <small>3 hrs ago</small>
                          </div>
                        </div>
                        <p class="text-sm mb-0">A new issue has been reported.</p>
                      </div>
                    </div>
                  </a>
                </div>
                <a href="#!" class="dropdown-item text-center text-primary font-weight-bold py-3">View all</a>
              </div>
            </li> -->
            <li class="nav-item dropdown">
              <a class="nav-link" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="ni ni-ungroup"></i>
              </a>
              <div class="dropdown-menu dropdown-menu-lg dropdown-menu-dark bg-default  dropdown-menu-right ">
                <div class="row shortcuts px-4">
                  <a href="#!" class="col-4 shortcut-item">
                    <span class="shortcut-media avatar rounded-circle bg-gradient-red">
                      <i class="ni ni-calendar-grid-58"></i>
                    </span>
                    <small>Calendar</small>
                  </a>
                  <a href="#!" class="col-4 shortcut-item">
                    <span class="shortcut-media avatar rounded-circle bg-gradient-orange">
                      <i class="ni ni-email-83"></i>
                    </span>
                    <small>Email</small>
                  </a>
                  <a href="#!" class="col-4 shortcut-item">
                    <span class="shortcut-media avatar rounded-circle bg-gradient-info">
                      <i class="ni ni-credit-card"></i>
                    </span>
                    <small>Payments</small>
                  </a>
                  <a href="#!" class="col-4 shortcut-item">
                    <span class="shortcut-media avatar rounded-circle bg-gradient-green">
                      <i class="ni ni-books"></i>
                    </span>
                    <small>Reports</small>
                  </a>
                  <a href="#!" class="col-4 shortcut-item">
                    <span class="shortcut-media avatar rounded-circle bg-gradient-purple">
                      <i class="ni ni-pin-3"></i>
                    </span>
                    <small>Maps</small>
                  </a>
                  <a href="#!" class="col-4 shortcut-item">
                    <span class="shortcut-media avatar rounded-circle bg-gradient-yellow">
                      <i class="ni ni-basket"></i>
                    </span>
                    <small>Shop</small>
                  </a>
                </div>
              </div>
            </li>
          </ul>
          <ul class="navbar-nav align-items-center  ml-auto ml-md-0 ">
            <li class="nav-item dropdown">
              <a class="nav-link pr-0" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <div class="media align-items-center">
                  <span class="avatar avatar-sm rounded-circle">
                    @if($dealer_images->filename ?? '')
                    <img alt="Image placeholder" src="{{'/uploads/'.$dealer_images->filename}}">
                    @endif
                  </span>
                  <div class="media-body  ml-2  d-none d-lg-block">
                    <span class="mb-0 text-sm  font-weight-bold">{{ Auth::user()->first_name . ' '. Auth::user()->last_name  }}</span>
                  </div>
                </div>
              </a>
              <div class="dropdown-menu  dropdown-menu-right ">
                <div class="dropdown-header noti-title">
                  <h6 class="text-overflow m-0">Welcome!</h6>
                </div>
                <!--<a href="#!" class="dropdown-item">-->
                <!--  <i class="ni ni-single-02"></i>-->
                <!--  <span>My profile</span>-->
                <!--</a>-->
                <!--<a href="#!" class="dropdown-item">-->
                <!--  <i class="ni ni-settings-gear-65"></i>-->
                <!--  <span>Settings</span>-->
                <!--</a>-->
                <!--<a href="#!" class="dropdown-item">-->
                <!--  <i class="ni ni-calendar-grid-58"></i>-->
                <!--  <span>Activity</span>-->
                <!--</a>-->
                <!--<a href="#!" class="dropdown-item">-->
                <!--  <i class="ni ni-support-16"></i>-->
                <!--  <span>Support</span>-->
                <!--</a>-->
                <a href="{{url('dealer/logout')}}" class="dropdown-item">
                  <i class="ni ni-support-16"></i>
                  <span>Logout</span>
                </a>
              </div>
            </li>
          </ul>
        </div>
      </div>
    </nav>
    <!-- Header -->
@include('dealer.common.header')
<body>
@include('dealer.common.sidebar')
  <!--Main Navigation-->
    <!--Main layout-->
    @yield('content')
  <!--Main layout-->
@include('dealer.common.footer')
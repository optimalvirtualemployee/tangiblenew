@extends('dealer.common.default')
@section('title', 'Enquiry')
@section('content')
  <!-- Main content -->
  <div class="main-content" id="panel">
    <!-- Topnav -->
    
    <!-- Header -->
    <!-- Header -->
    <div class="header pb-6 d-flex align-items-center" style="min-height: 100px;">
    </div>
    <!-- Page content -->
    <div class="container-fluid mt--6">
      <div class="row">      
        <div class="col-xl-12">          
            <div class="card">
              <div class="card-header">
                <div class="row align-items-center">
                  <div class="col-8">
                    <h3 class="mb-0">Search Users</h3>
                  </div>                  
                </div>
                <div class="row">
                    <div class="col-lg-4">
                        <div class="form-group">
                          <label class="form-control-label" for="input-keywrod">Keyword</label>
                          <input type="text" id="input-keywrod" class="form-control" placeholder="Type user name" value="">
                        </div>
                      </div>
                      <div class="col-lg-2">
                        <div class="form-group">
                          <label class="form-control-label" for="title-prefix">Status</label>
                          <select id="user-status" class="form-control" required >
                            <option value="" selected disabled>Choose one</option>
                            <option value="active"> Active </option>
                            <option value="inactive"> Inactive </option>
                          </select>
                        </div>
                      </div>
                      <div class="col-lg-4">
                        <div class="form-group">
                          <label class="form-control-label" for="title-prefix">Location</label>
                          <select class="form-control" required  id="multiselect" multiple="multiple">                          
                            <option value="florida"> Florida </option>
                            <option value="California"> California </option>
                            <option value="Melbourne"> Melbourne </option>
                            <option value="Perth"> Perth </option>
                          </select>
                        </div>
                      </div>
                      <div class="col-lg-2 text-right">
                        <label class="form-control-label invisible" for="title-prefix"> Filter</label>
                        <i class="ni-"></i>
                        <button type="submit" class="btn btn-primary btn-lg form-control">Submit</button>
                      </div>
                </div>
              </div>
              <div class="card-body">               
                  <h6 class="l text-muted mb-4">3 Results found </h6>
                  <div class="pl-lg-4">
                    <div class="row">  
                    @foreach($agentList as $agent)
                        <div class="col-lg-4">
                            <div class="userBox p-3 border position-relative">
                                <div class="userImg">
                                    <a href="editUser/{{Crypt::encrypt($agent->agentid)}}">

                                  @if($images->firstWhere('listing_id','=', $agent->agentid) !='')                                  
                                  <img src="{{url('uploads/'.$images->firstWhere('listing_id','=', $agent->agentid)['filename'])}}" alt="" class="w-100">
                                  @endif
                                       </a>                               
                                </div>
                                <div class="userDetails mt-4 border-top pt-2">
                                    <div class="userRole">
                                        Agent
                                    </div>
                                    <div>
                                       <a href="editUser/{{Crypt::encrypt($agent->agentid)}}"> <h2 class="m-0">{{$agent->agent_name}}</h2></a>
                                    </div>                                    
                                    <div>
                                        <address>
                                            <strong>Primary Location :</strong> {{$agent->agency_name}}, {{$agent->countryName}}
                                            <p>{{$agent->line1}}, {{$agent->line2}}, {{$agent->cityName}} {{$agent->line3}}</p>                                            
                                        </address>
                                    </div>
                                    <div class="mb-4">
                                        <strong>Attached To :</strong> {{$agent->agency_name}}
                                    </div>
                                    <div>
                                        <a href=""><i class="fa fa-envelope mr-2"> {{$agent->agentemail}}</i></a>                                      
                                    </div>
                                    <div>
                                        <a href="mailto:09-9-9-92312"><i class="fa fa-phone mr-2"></i>{{$agent->agentmobile}}</a>
                                    </div>
                                </div>
                            </div>  
                        </div>   
                        @endforeach 
                    </div>                    
                  </div>                 
              </div>
            </div>         
        </div>
      </div>
      
    </div>
  </div>
  <script>
  $(document).ready(function() {
  $('#multiselect').multiselect({
    buttonWidth : '100%',
    includeSelectAllOption : true,
		nonSelectedText: 'Select an Option'
  });
});

function getSelectedValues() {
  var selectedVal = $("#multiselect").val();
	for(var i=0; i<selectedVal.length; i++){
		function innerFunc(i) {
			setTimeout(function() {
				location.href = selectedVal[i];
			}, i*2000);
		}
		innerFunc(i);
	}
}
  </script>
  @stop
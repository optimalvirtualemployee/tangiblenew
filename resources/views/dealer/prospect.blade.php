@extends('dealer.common.default')
@section('title', 'Prospect')
@section('content')
    <!-- Header -->
    <div class="header pb-6 d-flex align-items-center" style="min-height: 100px;">
    </div>
    <!-- Page content -->
    <div class="container-fluid mt--6">
      <div class="row">

        <div class="col-xl-12">
          <form>
            <div class="card">
              <div class="card-header">
                <div class="row align-items-center">
                  <div class="col-4">
                    <h3 class="mb-0">Prospect </h3>
                  </div>
                  <div class="col-8 text-right">
                    <div id="searchwrap" class="mr-4">
                        <form action="" autocomplete="off">
                            <input id="search" name="search" type="text" class="form-control" placeholder="What're we looking for ?">
                            <button type="submit" class="btn btn-icon btn-primary">
                                <span class="btn-inner--icon"><i class="fa fa-search" aria-hidden="true"></i></span>
                            </button>
                        </form>
                      </div>
                    <a class="btn btn-primary" href="#">
                        <span class="btn-inner--icon"><i class="ni ni-fat-add"></i></span>
                        <span class="btn-inner--text">Add Prospect</span> 
                    </a>
                    <a class="btn" data-toggle="collapse" href="#filterCollapse" role="button" aria-expanded="false" aria-controls="filterCollapse">
                        <span class="btn-inner--icon"><i class="ni ni-ui-04"></i></span>
                        <span class="btn-inner--text">Filter</span> 
                    </a>
                  </div>
                </div>

              </div>

              <div class="collapse" id="filterCollapse">
                <div class="card-body">
                    Filter section coming soon...
                </div>
            </div>
              
              <div class="card-body">
                <div class="table-responsive">
                    <div>
                       <table class="table align-items-center">
                          <thead class="thead-light">
                             <tr>
                                <th scope="col" class="sort" data-sort="name">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" id="customCheckall">
                                        <label class="custom-control-label" for="customCheckall">All</label>
                                      </div>
                                </th>
                                <th scope="col" class="sort" data-sort="item">Sort by Date</th>
                                <th scope="col" class="sort" data-sort="price">Stage</th>
                                <th scope="col" class="sort" data-sort="status">Allocated To</th>                                
                               
                                <th><a href="#"><i class="fas fa-envelope-square"></i></a></th>
                                <th><a href="#"><i class="fa fa-comment-alt mr-2"></i></a></th>
                                <th><a href="#"><i class="fa fa-phone-alt mr-2"></i></a></th>
                                <th><a href="#"><i class="fa fa-calendar mr-2"></i></a></th>                               
                                <th scope="col" class="sort" data-sort="action">Conversation</th>
                                <th scope="col" class="sort" data-sort="action">Enquired On</th>
                             </tr>
                          </thead>
                          <tbody class="list">
                             <tr>
                                <th scope="row">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" id="customCheck2">
                                        <label class="custom-control-label" for="customCheck2"></label>
                                      </div>
                                </th>
                                <th scope="row">
                                  <strong>
                                      BEN
                                  </strong>
                                  <br>
                                  CARAVANCAMPINGSALES | 10/10/20 12:15
                                  <br>
                                  ID:0e170a86
                                </th>
                                <td class="budget">
                                <strong>Contact</strong>
                                <br>
                                Actioned <br>
                                10/10/20 | 12:15
                                </td>
                                <td class="text-left">
                                  <img src="{{url('assets/img/brand/favicon.png')}}">
                                  <br>
                                 <select class="mt-2">
                                   <option>
                                     James
                                   </option>
                                   
                                   <option>
                                    John
                                  </option>
                                 </select>
                                </td>                         

                               
                                <td>0</td>
                                <td>0</td>
                                <td>1</td>
                                <td>0</td>                                
                                <td>
                                  <a href="#">Messages (1)</a>
                                </td>
                                <td class="text-left">
                                  <img src="{{url('assets/img/icons/common/google.svg')}}">
                                  <strong>2020 CARRERA</strong>
                                  <br>
                              <span>$79,000</span>
                              <br>
                              <span class="topdeal">TOPDEAL</span>
                              </td>
                             </tr>
                             <tr>
                              <th scope="row">
                                  <div class="custom-control custom-checkbox">
                                      <input type="checkbox" class="custom-control-input" id="customCheck2">
                                      <label class="custom-control-label" for="customCheck2"></label>
                                    </div>
                              </th>
                              <th scope="row">
                                <strong>
                                    BEN
                                </strong>
                                <br>
                                CARAVANCAMPINGSALES | 10/10/20 12:15
                                <br>
                                ID:0e170a86
                              </th>
                              <td class="budget">
                              <strong>Contact</strong>
                              <br>
                              Actioned <br>
                              10/10/20 | 12:15
                              </td>
                              <td class="text-left">
                                <img src="{{url('assets/img/brand/favicon.png')}}">
                                <br>
                               <select class="mt-2">
                                 <option>
                                   James
                                 </option>
                                 
                                 <option>
                                  John
                                </option>
                               </select>
                              </td>                        

                             
                              <td>0</td>
                              <td>0</td>
                              <td>1</td>
                              <td>0</td>
                              
                              <td>
                                <a href="#">Messages (1)</a>
                              </td>
                              <td class="text-left">
                                <img src="{{url('assets/img/icons/common/github.svg')}}">
                                <strong>2020 CARRERA</strong>
                                <br>
                              <span>$79,000</span>
                              <br>
                              <span>TOPDEAL</span>
                            </td>
                           </tr>
                           <tr>
                            <th scope="row">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" id="customCheck2">
                                    <label class="custom-control-label" for="customCheck2"></label>
                                  </div>
                            </th>
                            <th scope="row">
                              <strong>
                                  BEN
                              </strong>
                              <br>
                              CARAVANCAMPINGSALES | 10/10/20 12:15
                              <br>
                              ID:0e170a86
                            </th>
                            <td class="budget">
                            <strong>Contact</strong>
                            <br>
                            Actioned <br>
                            10/10/20 | 12:15
                            </td>
                            <td class="text-left">
                              <img src="{{url('assets/img/brand/favicon.png')}}">
                              <br>
                             <select class="mt-2">
                               <option>
                                 James
                               </option>
                               
                               <option>
                                John
                              </option>
                             </select>
                            </td>                        

                          
                            <td>0</td>
                            <td>0</td>
                            <td>1</td>
                            <td>0</td>
                           
                            <td>
                              <a href="#">Messages (1)</a>
                            </td>
                            <td class="text-left">
                              <img src="{{url('assets/img/icons/common/google.svg')}}">
                              <strong>2020 CARRERA</strong>
                              <br>
                              <span>$79,000</span>
                              <br>
                              <span class="topdeal">TOPDEAL</span>
                          </td>
                         </tr>
                        
                           
                          </tbody>
                       </table>
                    </div>
                 </div>
              </div>
            </div>
          </form>
        </div>
      </div>
@stop
@extends('admin.common.innerdefaultauto')
@section('title', 'Create Drive Type')
@section('content')
<style>
.error {
      color: red;
   }
</style>
<div class="container">
  <!-- Outer Row -->
  <div class="row justify-content-center">
    <div class="col-xl-10 col-lg-12 col-md-9">
      <div class="card o-hidden border-0 shadow-lg my-5">
        <div class="card-body p-0">
          <!-- Nested Row within Card Body -->
          <div class="row">
            <div class="col-lg-8">
              <div class="p-5">
                <div class="text-center">
                   <h1 class="h4 text-gray-900 mb-4">Create Drive Type</h1>
                </div>
                @if($errors->any())
                  @foreach($errors->all() as $error)
                    <div class="alert alert-danger">
                      {{$error}}
                    </div>
                  @endforeach
                @endif
                <form class="user" id="drive_type" method="POST" action="{{ route('drive_type.store') }}">
                  @csrf
                  <div class="form-group">
                    <input id="drive_type" class="form-control form-control-user " type="text"  name="drive_type" placeholder="Enter Body Type" value="">
                  </div>
                  <div class="form-group">
                      <label>Status</label>
                      <select id ="status" name="status" class="form-control" required>
                       <option value="">Select Status</option>
                        <option value="1" >Active</option>
                        <option value="0" >Inactive</option>
                      </select>
                    </div>
                  <button type="submit" class="btn btn-primary btn-user btn-block">Add New</button>
                </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>


<!------------------------------------------ JS SCRIPT SECTION ------------------------------------------------>

<script type="text/javascript">
    $(document).ready(function(){
      $("#drive_type").validate({
    
        rules: {
        	drive_type: {required: true}
        },
    
        messages: {
        	drive_type: {required:"Drive Type can\'t be left blank"}
        },
    
        submitHandler: function(form) {
          form.submit();
        }
      });
    });
</script>

<!------------------------------------------ JS SCRIPT SECTION ------------------------------------------------>
@stop

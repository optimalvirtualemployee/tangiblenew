@extends('admin.common.innerdefaultauto')
@section('title', 'Display Brands')
@section('content')

  <!-- Page Wrapper -->
  <div id="wrapper">
    <div id="content-wrapper" class="d-flex flex-column">
      <!-- Main Content -->
      <div id="content">
        <!-- End of Topbar -->
        <!-- Begin Page Content -->
        <div class="container-fluid">
          <!-- Page Heading -->
          <h1 class="h3 mb-2 text-gray-800">Car Brands</h1>
          <a href="{{url('/admin/automobile/create_car_brands')}}" class="btn btn-primary btn-icon-split">
            <span class="text">Add New Brand</span>
          </a>
          
          @if(Session::has('success_msg'))
            <div class="alert " id="writeMsg" style="background-color: #4e73df;margin-top: 20px;color: white;">{{ Session::get('success_msg') }}</div>
          @endif
          <div id="main"></div>
          <div class="card shadow mb-4" style="margin-top: 10px;">
            <div class="card-body" >
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th>Brand Name</th>
                      <th>Image</th>
                      <th>Show Under Branch</th>
                      <th>Status</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach ($brands as $brand)
                      <tr>
                        <td>  {{$brand->automobile_brand_name}}</td>
                        <td>
						@if($images->firstWhere('listing_id', '=',$brand->id) != null)
						<img src="{{url('uploads/'.$images->firstWhere('listing_id', '=',$brand->id)->filename)}}"  class="img-thumbnail" width="75" />
						@endif
						</td>
						<td>@if($brand->show_under_brand =='1') Active  @else Inactive @endif  </td>
                        <td>@if($brand->status =='1') Active  @else Inactive @endif  </td>
                        <td><a href="{{url('/admin/automobile/updatebrands/'.$brand->id)}}"><i class="fas fa-edit"></i></a> &nbsp; &nbsp;<a href="#" class="deletebrands" id="del_{{$brand->id}}"><i class="fas fa-trash-alt"></a></td>
                      </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
        <!-- /.container-fluid -->
      </div>
      <!-- End of Main Content -->
     

<!----------------------------------------- JS SCRIPT SECTION ---------------------------------------------------------->
<script type="text/javascript">
   $(document).on("click", ".deletebrands", function() { 
      var url = "{{ route('deletecarbrands') }}";
      var id = this.id;
      var splitid = id.split("_");
      var deleteid = splitid[1];
      if ( confirm("Do you really want to delete record?")) {
        $.ajax({
          url: url,
          type: 'POST',
          cache: false,
          data:{
             id:deleteid,
            _token:'{{ csrf_token() }}'
          },
          success: function(data){ 

              $('#main').html('<div class="alert " id="writeMsg" style="background-color: #4e73df;margin-top: 20px;color: white;">' + data.success_msg + '</div>')
               location.reload();
          }
        });
      }
    });

   $(document).ready(function(){
      setTimeout(function(){
        $("div.alert").remove();
      }, 3000 ); 
    });
</script>
<!----------------------------------------- JS SCRIPT SECTION ---------------------------------------------------------->
@stop


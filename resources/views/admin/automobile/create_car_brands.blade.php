@extends('admin.common.innerdefaultauto')
@section('title', 'Create Brands')
@section('content')
<style>
.error {
      color: red;
   }
</style>
<div class="container">
   <!-- Outer Row -->
    <div class="row justify-content-center">
      <div class="col-xl-10 col-lg-12 col-md-9">
        <div class="card o-hidden border-0 shadow-lg my-5">
          <div class="card-body p-0">
            <!-- Nested Row within Card Body -->
            <div class="row">
              <div class="col-lg-8">
                <div class="p-5">
                  <div class="text-center">
                    <h1 class="h4 text-gray-900 mb-4">Create New Brands for Cars</h1>
                  </div>
                  @if($errors->any())
                    @foreach($errors->all() as $error)
                      <div class="alert alert-danger">
                    {{$error}}
                      </div>
                    @endforeach
                  @endif
                  <form class="user" id="createbrands" method="POST" action="{{ route('savecarbrands') }}">
                  @csrf
                    <div class="form-group">
                      <input id="brandname" class="form-control form-control-user " type="text"  name="brandname" placeholder="Enter Brand Name">
                    </div>
                    <div class="form-group">
                      <label>Status</label>
                      <select id ="status" name="status" class="form-control required" >
                       <option value="">Select Status</option>
                        <option value="1" >Active</option>
                        <option value="0" >Inactive</option>
                      </select>
                    </div>
                      <button type="submit" class="btn btn-primary btn-user btn-block">Add New</button>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  
<script type="text/javascript">
    $(document).ready(function(){
      $("#createbrands").validate({
        rules: {
          brandname: "required",
        },
        
        messages: {
          brandname: "Brandname cant be left blank",
        },
      
        submitHandler: function(form) {
          form.submit();
        }
      });
    });
</script>

@stop

@extends('admin.common.innerdefaultauto')
@section('title', 'Display Automobile Listing')
@section('content')

  <!-- Page Wrapper -->
  <div id="wrapper">
    <div id="content-wrapper" class="d-flex flex-column">
      <!-- Main Content -->
      <div id="content">
        <!-- End of Topbar -->
        <!-- Begin Page Content -->
        <div class="container-fluid">
        <!-- Page Heading -->
        <h1 class="h3 mb-2 text-gray-800">Display Automobile</h1>
        <a href="{{route('createautomobiledata.create')}}" class="btn btn-primary btn-icon-split">
          <span class="text">Add Automobile Data</span>
        </a>
        @if(Session::has('success_msg'))
          <div class="alert " id="writeMsg" style="background-color: #4e73df;margin-top: 20px;color: white;">{{ Session::get('success_msg') }}</div>
            @endif
            <div id="main"></div>
            <div class="card shadow mb-4" style="margin-top: 10px;">
            <div class="card-body" >
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th>Make</th>
                      <th>Model</th>
                      <th>Odometer Reading</th>
                      <th>Status</th>
                      <th>Approval</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  
                  <tbody>
                    @foreach ($details as $data)
                      <tr>
                        <td>{{ $data->automobile_brand_name}}</td>
                        <td>{{ $data->automobile_model_name}}</td>
                        <td>{{ $data->odometer}}</td>
                        <td>{{ $data->status == 1 ? 'Active' : 'Inactive'}}</td>
                        <td>{{ $data->approval_status == 'approved' ? 'Approved' : 'Pending'}}</td>
                        <td>
                          <a href="{{route('createautomobiledata.edit', $data->id)}}"><i class="fas fa-edit"></i></a> &nbsp; &nbsp;
                           <form action="{{ route('createautomobiledata.destroy', $data->id) }}" method="POST" id="delete-form-{{ $data->id }}" style="display: none;">
                                {{csrf_field()}}
                                {{ method_field('DELETE') }}
                                <input type="hidden" value="{{ $data->id }}" name="id">
                           </form>
                           <a href="" onclick="if (!confirm('Are you sure you want to delete?')) return; 
                            event.preventDefault(); document.getElementById('delete-form-{{ $data->id }}').submit();">
                            <i class="fas fa-trash-alt"></i></a>
                            <a href="{{route('automobilecomment.show', $data->id)}}"><i class="fa fa-comments-o" style="font-size:20px"></i></a> &nbsp; &nbsp;
                            <a href="{{route('uploadautomobileimage.show',$data->id)}}"><i class="material-icons" style="font-size:20px">add_a_photo</i></a> &nbsp; &nbsp;
                            <a href="{{url('admin/automobilelatestoffer/updateOffer', $data->id)}}"  class="btn btn-primary btn-icon-split">
                          <span class="text" >{{$data->latestOffer == 1 ? 'Latest Offer Active' : 'Latest Offer InActive'}}</span>
                        </td>
                      </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
        <!-- /.container-fluid -->
      </div>
      <!-- End of Main Content -->

      <!-------------------------- JS SCRIPT SECTION --------------------------------------->
      <script type="text/javascript">
          $(document).ready(function(){
            setTimeout(function(){
                $("div.alert").remove();
            }, 3000 ); 
          });
      </script>
      <!--------------------------- JS SCRIPT SECTION ------------------------------------->
@stop


@extends('admin.common.innerdefaultauto')
@section('title', 'Update Body Type')
@section('content')
<style>
.error {
      color: red;
   }
</style>
<div class="container">
  <!-- Outer Row -->
  <div class="row justify-content-center">
    <div class="col-xl-10 col-lg-12 col-md-9">
      <div class="card o-hidden border-0 shadow-lg my-5">
        <div class="card-body p-0">
          <!-- Nested Row within Card Body -->
          <div class="row">
            <div class="col-lg-8">
              <div class="p-5">
                <div class="text-center">
                   <h1 class="h4 text-gray-900 mb-4">Update Body Type</h1>
                </div>
                @if($errors->any())
                  @foreach($errors->all() as $error)
                    <div class="alert alert-danger">
                      {{$error}}
                    </div>
                  @endforeach
                @endif
                <form class="user" id="editbody_type" method="POST" action="{{ route('body_type.update', $body_type_data->id) }}">
                  @method('PATCH')
                  @csrf
                  <div class="form-group">
                    <input id="body_type" class="form-control form-control-user " type="text"  name="body_type" placeholder="Enter Body Type" value="{{ $body_type_data->body_type}}">
                  </div>
                  <div class="form-group">
                      <label>Status</label>
                      <select id ="status" name="status" class="form-control" required>
                        <option value="1" {{$body_type_data->status == "1" ? 'selected' : ''}}>Active</option>
                        <option value="0" {{$body_type_data->status == "0" ? 'selected' : ''}}>Inactive</option>
                      </select>
                    </div> 
                  <button type="submit" class="btn btn-primary btn-user btn-block">Update</button>
                </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>


<!------------------------------------------ JS SCRIPT SECTION ------------------------------------------------>

<script type="text/javascript">
    $(document).ready(function(){
      $("#editbody_type").validate({
    
        rules: {
        	body_type: {required: true}
        },
    
        messages: {
        	body_type: {required:"Body Type can\'t be left blank"}
        },
    
        submitHandler: function(form) {
          form.submit();
        }
      });
    });
</script>

<!------------------------------------------ JS SCRIPT SECTION ------------------------------------------------>
@stop

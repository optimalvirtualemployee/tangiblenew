@extends('admin.common.innerdefaultauto')
@section('title', 'Update Blog')
@section('content')

<div class="container">
  <!-- Outer Row -->
  <div class="row justify-content-center">
    <div class="col-xl-10 col-lg-12 col-md-9">
      <div class="card o-hidden border-0 shadow-lg my-5">
        <div class="card-body p-0">
          <!-- Nested Row within Card Body -->
          <div class="row">
            <div class="col-lg-8">
              <div class="p-5">
                <div class="text-center">
                   <h1 class="h4 text-gray-900 mb-4">Update Blog</h1>
                </div>
                @if($errors->any())
                  @foreach($errors->all() as $error)
                    <div class="alert alert-danger">
                      {{$error}}
                    </div>
                  @endforeach
                @endif
                <form class="user" id="editcontactus" method="POST" action="{{ route('automobilecontactus.update', $contactus->id) }}">
                  @method('PATCH')
                  @csrf
                  <div class="form-group">
                    <input id="address" class="form-control form-control-user " type="text"  name="address" placeholder="Enter Address" value="{{$contactus->address}}">
                   </div>
                   <div class="form-group">
                    <input id="mobile" class="form-control form-control-user " type="text"  name="mobile" placeholder="Enter Mobile" value="{{$contactus->mobile}}">
                   </div>
                   <div class="form-group">
                    <input id="whatsapp" class="form-control form-control-user " type="text"  name="whatsapp" placeholder="Enter Whatsapp" value="{{$contactus->whatsapp}}">
                  </div>
                  <div class="form-group">
                    <input id="email" class="form-control form-control-user " type="email"  name="email" placeholder="Enter Email" value="{{$contactus->email}}">
                  </div> 
                  <button type="submit" class="btn btn-primary btn-user btn-block">Update</button>
                </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>


<!------------------------------------------ JS SCRIPT SECTION ------------------------------------------------>

<script type="text/javascript">
    $(document).ready(function(){
      $("#editno_of_bathrooms").validate({
    
        rules: {
          number_of_bathrooms: {required: true, number: true}
        },
    
        messages: {
          number_of_bathrooms: {required:"No. of Bathrooms can\'t be left blank",number:" Please enter numbers only"}
        },
    
        submitHandler: function(form) {
          form.submit();
        }
      });
    });
</script>

<!------------------------------------------ JS SCRIPT SECTION ------------------------------------------------>
@stop

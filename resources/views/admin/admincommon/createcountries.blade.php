@extends('admin.common.innercommondefault')
@section('title', 'Create Country')
@section('content')
<style>
.error {
      color: red;
   }
</style>
<div class="container">

    <!-- Outer Row -->
    <div class="row justify-content-center">

      <div class="col-xl-10 col-lg-12 col-md-9">

        <div class="card o-hidden border-0 shadow-lg my-5">
          <div class="card-body p-0">
            <!-- Nested Row within Card Body -->
            <div class="row">
              
              <div class="col-lg-8">
                <div class="p-5">
                  <div class="text-center">
                    <h1 class="h4 text-gray-900 mb-4">Create Country</h1>
                  </div>
                  @if($errors->any())
                    @foreach($errors->all() as $error)
                      <div class="alert alert-danger" style="color:red">
                    {{$error}}
                      </div>
                    @endforeach
                  @endif
                  <form class="user" id="createcountries" method="POST" action="{{ route('savecountries') }}" enctype="multipart/form-data">
                  @csrf
                    <div class="form-group">
                      <input id="countryname" class="form-control form-control-user" type="text" maxlength="50" name="countryname" placeholder="Enter Country">
                      <span id="lblError" style="color: red"></span>
                    </div>
                    <div class="form-group">
                      <input id="countrycode" class="form-control form-control-user" type="text" maxlength="50" name="countrycode" placeholder="Enter Country Code">
                      <span id="lblError" style="color: red"></span>
                    </div>
                    <div class="form-group">
                      <input id="phonecode" class="form-control form-control-user" type="text" maxlength="50" name="phonecode" placeholder="Enter Country's Phone Code">
                      <span id="lblError" style="color: red"></span>
                    </div>
                   <div class="form-group">
                   <div class="input-group control-group increment">
											<input type="file" name="photos[]" class="form-control">
										</div>
                   </div>
                   
                    <div class="form-group">
                      <label>Status</label>
                      <select id ="status" name="status" class="form-control">
                       <option value="">Select Status</option>
                        <option value="1" >Active</option>
                        <option value="0" >Inactive</option>
                      </select>
                    </div>
                    
                      <button type="submit" class="btn btn-primary btn-user btn-block">
                                    Add
                      </button>
                      
                    
                    
                  </form>
                 
                 
                </div>
              </div>
            </div>
          </div>
        </div>

      </div>

    </div>

  </div>

  
<script type="text/javascript">
$(document).ready(function(){

	$("#countryname").keypress(function (e) {
        var keyCode = e.keyCode || e.which;

        $("#lblError").html("");

        //Regex for Valid Characters i.e. Alphabets.
        var regex = /^[A-Za-z]+$/;

        //Validate TextBox value against the Regex.
        var isValid = regex.test(String.fromCharCode(keyCode));
        if (!isValid) {
            $("#lblError").html("Only Alphabets allowed.");
        }

        return isValid;
    });

	$("#countrycode").keypress(function (e) {
        var keyCode = e.keyCode || e.which;

        $("#lblError").html("");

        //Regex for Valid Characters i.e. Alphabets.
        var regex = /^[A-Za-z]+$/;

        //Validate TextBox value against the Regex.
        var isValid = regex.test(String.fromCharCode(keyCode));
        if (!isValid) {
            $("#lblError").html("Only Alphabets allowed.");
        }

        return isValid;
    });
	 
  $("#createcountries").validate({
    
    rules: {
     
      countryname: "required",
      status: "required"
    
      },
    
    messages: {
      countryname: "Country  cannot be empty",
      
    },
    
    submitHandler: function(form) {
      form.submit();
    }
  }); 
});
</script>

@stop

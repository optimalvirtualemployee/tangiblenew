@extends('admin.common.innercommondefault')
@section('title', 'Update Currency Converter')
@section('content')

<div class="container">
  <!-- Outer Row -->
  <div class="row justify-content-center">
    <div class="col-xl-10 col-lg-12 col-md-9">
      <div class="card o-hidden border-0 shadow-lg my-5">
        <div class="card-body p-0">
          <!-- Nested Row within Card Body -->
          <div class="row">
            <div class="col-lg-8">
              <div class="p-5">
                <div class="text-center">
                   <h1 class="h4 text-gray-900 mb-4">Update Currency Converter</h1>
                </div>
                @if($errors->any())
                  @foreach($errors->all() as $error)
                    <div class="alert alert-danger">
                      {{$error}}
                    </div>
                  @endforeach
                @endif
                <form class="user" id="updateagent" method="POST" action="{{ route('currencyconverter.update',$currency->id) }}">
                  @method('PATCH')
                  @csrf
                  <div class="form-group">
                    <label>Currency:</label><input id="currency" class="form-control form-control-user " type="text"  name="currency" placeholder="Enter Currency" value="{{$currency->name}}">
                   </div>
                   <div class="form-group">
                    <label>Code:</label><input id="code" class="form-control form-control-user " type="text"  name="code" placeholder="Enter Code" value="{{$currency->code}}">
                  </div>
                  <div class="form-group">
                    <label>Symbol:</label><input id="symbol" class="form-control form-control-user " type="text"  name="symbol" placeholder="Enter Symbol" value="{{$currency->symbol}}">
                  </div>
                  <div class="form-group">
                    <label>Exchange Rate:</label><input id="exchange_rate" class="form-control form-control-user " type="text"  name="exchange_rate" placeholder="Enter Exchange Rate" value="{{$currency->exchange_rate}}">
                  </div>
                  <div class="form-group">
                    <label>Format:</label><input id="format" class="form-control form-control-user " type="text"  name="format" placeholder="Enter Format" value="{{$currency->format}}">
                  </div>
                  <button type="submit" class="btn btn-primary btn-user btn-block">Add</button>
                </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>


<!------------------------------------------ JS SCRIPT SECTION ------------------------------------------------>

<script type="text/javascript">
    $(document).ready(function(){
    	$("#updateCurrencyData").validate({
    	    
    		rules: {
            	first_name: "required",
            	last_name: "required",
            	whatsapp: {required: true, number:true},
            	mobile: {required: true, number:true},
            	fax: {required: true, number:true},
            	office: {required: true, number:true},
            	email: {required: true},
            	address: "required"
            },
        
            submitHandler: function(form) {
              form.submit();
            }
          });
        });
        
</script>

<!------------------------------------------ JS SCRIPT SECTION ------------------------------------------------>
@stop

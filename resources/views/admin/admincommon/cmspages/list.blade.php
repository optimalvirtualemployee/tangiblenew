@extends('admin.common.innercommondefault')
@section('title', 'CMS Pages')
@section('content')
  <div id="wrapper">
    <div id="content-wrapper" class="d-flex flex-column">
      <div id="content">
        <div class="container-fluid">
          <h1 class="h3 mb-2 text-gray-800">CMS Pages</h1>
         	@can('superAdmin-create')
         		<a href="{{route('cmspages.create')}}" class="btn btn-primary btn-icon-split">
            		<span class="text">Add New Page</span>
          		</a>
         	@endcan
          @if(Session::has('success_msg'))
          	<div class="alert " id="writeMsg" style="background-color: #4e73df;margin-top: 20px;color: white;">{{ Session::get('success_msg') }}</div>
          @endif
          <div id="main"></div>
          	<div class="card shadow mb-4" style="margin-top: 10px;">
            	<div class="card-body" >
             		<div class="table-responsive">
               		<table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
               			<thead>
                 			<tr>
                 				<th>S.No.</th>
				                <th>Page Title</th>
                        <th>Page URL</th>
				                <th>Status</th>
					  				    @can('superAdmin-create')
                      	 <th>Action</th>
                      	@endcan
                    	</tr>
                  	</thead>
		                <tbody>
                      <?php $i=1;?>
                    	@foreach($CMSPageListing as $cmspage)
                    		<tr>
                          <td>{{$i}}</td>
                    			<td>{{$cmspage->page_title}}</td>
                    			<td><input type="text" value="{{url('/').'/'.$cmspage->slug}}" style="width: 100%" maxlength="500" /></td>
                          <td>
                   					@if($cmspage->status =='1') Active @else InActive @endif  
                        	</td>


                          @can('superAdmin-create')
                          <td>
                            {!! Form::open(['method'=>'DELETE', 'url' =>route('cmspages.destroy', $cmspage->id),'style' => 'display:inline']) !!}
                            {!! Form::button('<i class="fas fa-trash-alt"></i>', array('type' => 'submit','class' => 'btn','title' => 'Delete Post','onclick'=>'return confirm("Confirm delete?")')) !!}
                            {!! Form::close() !!}
                              <a  href="{{route('cmspages.edit',$cmspage->id)}}">
                                <i class="fas fa-edit"></i>
                              </a> &nbsp; &nbsp;
                          </td>
                        @endcan
                      	</tr>
                        <?php $i++;?>
                    	@endforeach
                  	</tbody>
                	</table>
              	</div>
            	</div>
          	</div>
          </div>
      </div>
    @stop
@extends('admin.common.innercommondefault')
@section('title', 'New CMS Page')
@section('content')
<style>
  .error { color: red; }
</style>
<div class="container">
  <div class="row justify-content-center">
    <div class="col-xl-10 col-lg-12 col-md-9">
      <div class="card o-hidden border-0 shadow-lg my-5">
        <div class="card-body p-0">
          <div class="row">
            <div class="col-lg-8">
              <div class="p-5">
                <div class="text-center">
                  <h1 class="h4 text-gray-900 mb-4">Create New Page</h1>
                </div>
                @if($errors->any())
                  @foreach($errors->all() as $error)
                    <div class="alert alert-danger" style="color:red">
                      {{$error}}
                    </div>
                  @endforeach
                @endif
                <form class="user" id="create_cmspage" method="POST" action="{{ route('cmspages.update',$editData->id) }}" enctype="multipart/form-data">
                  @csrf
                  @method('PUT')
                    <div class="form-group">
                      <label>Page Title</label>
                      <input id="page_title" class="form-control form-control-user" type="text" maxlength="50" name="page_title" placeholder="Enter Page Title" value="{{$editData->page_title}}">
                    </div>
                    <div class="form-group">
                      <label>Slug</label>
                      <input id="slug" class="form-control form-control-user" type="text" maxlength="50" name="slug" readonly placeholder="Slug" value="{{$editData->slug}}">
                    </div>
                    <div class="form-group">
                      <label>Meta Title</label>
                      <input id="meta_title" class="form-control form-control-user" type="text" name="meta_title" placeholder="Enter meta title" value="{{$editData->meta_title}}">
                    </div>
                    <div class="form-group">
                      <label>Meta Keyword</label>
                      <input id="meta_keyword" class="form-control form-control-user" type="text" name="meta_keyword" placeholder="Enter meta keyword" value="{{$editData->meta_keyword}}">
                    </div>
                    <div class="form-group">
                      <label>Image</label>
                      <div class="input-group control-group increment">
                        <input type="file" name="image" class="form-control">
                      </div>
                    </div>
                    <div class="form-group">
                      <label>Page Description</label>
                      <textarea id="description" class="form-control form-control-user" type="text" maxlength="1000" name="description" placeholder="Enter page description here">@if($editData->description){{$editData->description}}@endif</textarea>
                    </div>
                    <div class="form-group">
                      <label>Status</label>
                      <select id ="status" name="status" class="form-control">
                        <option value="">Select Status</option>
                        <option value="1" {{$editData->status == '1' ? 'selected="selected"' :''}} >Active</option>
                        <option value="0" {{$editData->status == '0' ? 'selected="selected"' :''}} >Inactive</option>
                      </select>
                    </div>
                    <button type="submit" class="btn btn-primary btn-user btn-block">Update</button>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <script src="https://cdn.ckeditor.com/4.5.6/standard/ckeditor.js"></script>
  <script type="text/javascript">
    $(document).ready(function(){
      $("#page_title").keypress(function (e) {
        var keyCode = e.keyCode || e.which;
        $("#lblError").remove();
        //Regex for Valid Characters i.e. Alphabets.
        var regex = /^[A-Za-z\s]*$/;
        //Validate TextBox value against the Regex.
        var isValid = regex.test(String.fromCharCode(keyCode));
        if (!isValid) {
            $("#page_title").after('<div id="lblError" style="color:red">Only Alphabets allowed.</div>');
        }
        return isValid;
    });

    $('#page_title').keyup(function() {
      var slugValue = $('#page_title').val();
      var newSlug   = slugValue.replace(/ /g, "-");
      $('#slug').val(newSlug.toLowerCase());
    });  

    $("#createcountries").validate({
      rules: {
        page_title: "required",
        status: "required"
      },
      
      messages: {
        page_title: "Page Title can't be empty",
      },
    
      submitHandler: function(form) {
        form.submit();
      }
    }); 
  });

  CKEDITOR.replace( 'description', {
    allowedContent: true,
  });
  </script>
@stop

@extends('admin.common.innercommondefault')
@section('title', 'Display Agents')
@section('content')

  <!-- Page Wrapper -->
  <div id="wrapper">
    <div id="content-wrapper" class="d-flex flex-column">
      <!-- Main Content -->
      <div id="content">
        <!-- End of Topbar -->
        <!-- Begin Page Content -->
        <div class="container-fluid">
          <!-- Page Heading -->
          <h1 class="h3 mb-2 text-gray-800">Agents</h1>
          <a href="{{route('agentdata.create')}}" class="btn btn-primary btn-icon-split">
            <span class="text">Add New Agent</span>
          </a>
          @if(Session::has('success_msg'))
            <div class="alert " id="writeMsg" style="background-color: #4e73df;margin-top: 20px;color: white;">{{ Session::get('success_msg') }}</div>
          @endif
          <div class="card shadow mb-4" style="margin-top: 10px;">
            <div class="card-body" >
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th>Name</th>
                      <th>Mobile</th>
                      <th>Whatsapp</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                 
                  <tbody>
                    @foreach ($agents as $agent)
                      <tr>
                        <td>{{$agent->first_name .' '. $agent->last_name}} </td>
                        <td>{{$agent->mobile}}</td>
                        <td>{{$agent->whatsapp}}</td>
                        <td>
                          <a href="{{route('agentdata.edit', $agent->id)}}"><i class="fas fa-edit"></i></a> &nbsp; &nbsp;
                           <form action="{{ route('agentdata.destroy', $agent->id) }}" method="POST" id="delete-form-{{ $agent->id }}" style="display: none;">
                                {{csrf_field()}}
                                {{ method_field('DELETE') }}
                                <input type="hidden" value="{{ $agent->id }}" name="id">
                           </form>
                           <a href="" onclick="if (!confirm('Are you sure you want to delete?')) return; 
                            event.preventDefault(); document.getElementById('delete-form-{{ $agent->id }}').submit();">
                            <i class="fas fa-trash-alt"></i></a>
                            <a href="{{route('uploadagentimage.show',$agent->id)}}"><i class="material-icons" style="font-size:20px">add_a_photo</i></a> &nbsp; &nbsp;
                        </td>
                      </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
        <!-- /.container-fluid -->
      </div>
      
<!----------------------------------------------------- JS SCRIPT SECTION --------------------------------->
<script type="text/javascript">
    $(document).on("click", ".deletecity", function() { 
      var url = "{{ route('deletecity') }}";
      var id = this.id;
      var splitid = id.split("_");
      var deleteid = splitid[1];
      if ( confirm("Do you really want to delete record?")) {
        $.ajax({
          url: url,
          type: 'POST',
          cache: false,
          data:{
             id:deleteid,
            _token:'{{ csrf_token() }}'
          },
          success: function(data){ 

              $('#main').html('<div class="alert " id="writeMsg" style="background-color: #4e73df;margin-top: 20px;color: white;">' + data.success_msg + '</div>')
               location.reload();
          }
        });
      }
    });

    $(document).ready(function(){
      setTimeout(function(){
        $("div.alert").remove();
      }, 3000 ); 
    });
</script>
<!----------------------------------------------------- JS SCRIPT SECTION --------------------------------->
@stop

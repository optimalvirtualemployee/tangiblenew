@extends('admin.common.innercommondefault')
@section('title', 'FAQ Category Page')
@section('content')
  <div id="wrapper">
    <div id="content-wrapper" class="d-flex flex-column">
      <div id="content">
        <div class="container-fluid">
          <h1 class="h3 mb-2 text-gray-800">FAQ Category</h1>
         	@can('superAdmin-create')
         		<a href="{{route('faq-cat.create')}}" class="btn btn-primary btn-icon-split">
            		<span class="text">Add New Category</span>
          		</a>
         	@endcan
          @if(Session::has('success_msg'))
          	<div class="alert " id="writeMsg" style="background-color: #4e73df;margin-top: 20px;color: white;">{{ Session::get('success_msg') }}</div>
          @endif
          <div id="main"></div>
          	<div class="card shadow mb-4" style="margin-top: 10px;">
            	<div class="card-body" >
             		<div class="table-responsive">
               		<table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
               			<thead>
                 			<tr>
                 				<th>S.No.</th>
				                <th>Category Name</th>
                        <th>Status</th>
					  				    @can('superAdmin-create')
                      	 <th>Action</th>
                      	@endcan
                    	</tr>
                  	</thead>
		                <tbody>
                      <?php $i=1;?>
                    	@foreach($FAQCategoryListing as $faq)
                    		<tr>
                          <td>{{$i}}</td>
                    			<td>{{$faq->name}}</td>
                    			<td>
                   					@if($faq->status =='1') Active @else InActive @endif  
                        	</td>
                          @can('superAdmin-create')
                          <td>
                            {!! Form::open(['method'=>'DELETE', 'url' =>route('faq-cat.destroy', $faq->id),'style' => 'display:inline']) !!}
                            {!! Form::button('<i class="fas fa-trash-alt"></i>', array('type' => 'submit','class' => 'btn','title' => 'Delete Post','onclick'=>'return confirm("Confirm delete?")')) !!}
                            {!! Form::close() !!}
                              <a  href="{{route('faq-cat.edit',$faq->id)}}">
                                <i class="fas fa-edit"></i>
                              </a> &nbsp; &nbsp;
                          </td>
                        @endcan
                      	</tr>
                        <?php $i++;?>
                    	@endforeach
                  	</tbody>
                	</table>
              	</div>
            	</div>
          	</div>
          </div>
      </div>
    @stop
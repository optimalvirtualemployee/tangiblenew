@extends('admin.common.innercommondefault')
@section('title', 'Create Agent Job Position')
@section('content')

<div class="container">
  <!-- Outer Row -->
  <div class="row justify-content-center">
    <div class="col-xl-10 col-lg-12 col-md-9">
      <div class="card o-hidden border-0 shadow-lg my-5">
        <div class="card-body p-0">
          <!-- Nested Row within Card Body -->
          <div class="row">
            <div class="col-lg-8">
              <div class="p-5">
                <div class="text-center">
                   <h1 class="h4 text-gray-900 mb-4">Create Agent Job Position</h1>
                </div>
                @if($errors->any())
                  @foreach($errors->all() as $error)
                    <div class="alert alert-danger">
                      {{$error}}
                    </div>
                  @endforeach
                @endif
                <form class="user" id="createagent" method="POST" action="{{ route('agentjobposition.store') }}">
                  @csrf
                  <div class="form-group">
                    <label>Job Position:</label><input id="job_position" class="form-control form-control-user " type="text"  name="job_position" placeholder="Enter Job Position" value="">
                   </div>
                   <div class="form-group">
                      <label>Status</label>
                      <select id ="status" name="status" class="form-control" required>
                       <option value="">Select Status</option>
                        <option value="1" >Active</option>
                        <option value="0" >Inactive</option>
                      </select>
                    </div>
                  <button type="submit" class="btn btn-primary btn-user btn-block">Add</button>
                </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>


<!------------------------------------------ JS SCRIPT SECTION ------------------------------------------------>

<script type="text/javascript">
    $(document).ready(function(){
      $("#createagent").validate({
    
        rules: {
        	first_name: "required",
        	last_name: "required",
        	whatsapp: {required: true, number:true},
        	mobile: {required: true, number:true},
        	fax: {required: true, number:true},
        	office: {required: true, number:true},
        	email: {required: true},
        	address: "required"
        },
    
        
    
        submitHandler: function(form) {
          form.submit();
        }
      });
    });
</script>

<!------------------------------------------ JS SCRIPT SECTION ------------------------------------------------>
@stop

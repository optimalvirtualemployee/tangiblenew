@extends('admin.common.innercommondefault')
@section('title', 'Package Setting for Private Sellers')
@section('content')

<div class="container">
  <!-- Outer Row -->
  <div class="row justify-content-center">
    <div class="col-xl-10 col-lg-12 col-md-9">
      <div class="card o-hidden border-0 shadow-lg my-5">
        <div class="card-body p-0">
          <!-- Nested Row within Card Body -->
          <div class="row">
            <div class="col-lg-10">
              <div class="p-5">
                <div class="text-center">
                   <h1 class="h4 text-gray-900 mb-4">Package Setting for Private Sellers Watch</h1>
                </div>
                @if($errors->any())
                  @foreach($errors->all() as $error)
                    <div class="alert alert-danger offset-lg-1">
                      {{$error}}
                    </div>
                  @endforeach
                @endif

                <div class="alert-danger"></div>
                
                @if($package_settings)
                  <form class="user" id="package_setting" method="POST" action="{{ url('admin/package_for_ps_watch/package_setting_update',$package_settings->package_id) }}">
                  @csrf
                    <input type="hidden" name="package_id" value="{{ $package_settings->package_id }}">
                    <div class="form-group">
                      
                      <div class="row">
                      
                        <div class="col-lg-5 offset-lg-1">
                          <label>No. of Images per Listing</label>
                          <input class="form-control form-control-user" maxlength="3" type="text" style="padding: 0.5rem;" name="no_of_images_per_listing" placeholder="Enter No. of Images per Listing" value="{{ $package_settings->no_of_images_per_listing != '' ? $package_settings->no_of_images_per_listing : old('no_of_images_per_listing') }}" autocomplete="off">
                        </div>
                    
                    </div>
                    
                    <div class="offset-lg-1 pt-2">
                    
                      <input type="checkbox" name="normal_search_result" class="chkbox" {{ $package_settings->normal_search_result == 1 ? 'checked' : '' }}> &nbsp; &nbsp;Appear In search results<br>
                    
                      <input type="checkbox" name="weekly_statistics" class="chkbox" {{ $package_settings->weekly_statistics == 1 ? 'checked' : '' }}> &nbsp; &nbsp;Weekly Statistics<br>
                    
                      <input type="checkbox" name="basic_analytics" class="chkbox" {{ $package_settings->basic_analytics == 1 ? 'checked' : '' }}> &nbsp; &nbsp;Basic Analytics<br>
                    
                      <input type="checkbox" name="prioritized_search_result" class="chkbox" {{ $package_settings->prioritized_search_result == 1 ? 'checked' : '' }}> &nbsp; &nbsp;Prioritized search results<br>
                    
                      <input type="checkbox" name="escrow_platform" class="chkbox" {{ $package_settings->escrow_platform == 1 ? 'checked' : '' }}> &nbsp; &nbsp;Ability to process payments intenationally via the Escrow platform<br>
                    
                      <input type="checkbox" name="top_search_result" class="chkbox" {{ $package_settings->top_search_result == 1 ? 'checked' : '' }}> &nbsp; &nbsp;Appear in the top of search results<br>
                    
                      <input type="checkbox" name="choice_listing_on_home_page" class="chkbox" {{ $package_settings->choice_listing_on_home_page == 1 ? 'checked' : '' }}> &nbsp; &nbsp;Eligible to have the listing appear on the home page for 24 hours <br>
                    
                      <input type="checkbox" name="tangible_listing_on_social_media" class="chkbox" {{ $package_settings->tangible_listing_on_social_media == 1 ? 'checked' : '' }}> &nbsp; &nbsp;Eligible to appear on Tangible Listing’s social media pages<br>
                    
                    </div>
                  
                  </div>
                   
                  <button type="submit" class="btn btn-primary btn-user btn-block ml-5">Update Settings</button>
                
                </form>
                
                @else
                
                  <form class="user" id="package_setting" method="POST" action="{{ url('admin/package_for_ps_watch/package_setting_update',request()->route('id')) }}">
                  
                  @csrf
                    
                    <input type="hidden" name="package_id" value="{{request()->route('id')}}">
                    
                    <div class="form-group">
                      
                      <div class="row">
                      
                        <div class="col-lg-5 offset-lg-1">
                          <label>No. of Images per Listing</label>
                          <input class="form-control form-control-user" maxlength="3" type="text" style="padding: 0.5rem;" name="no_of_images_per_listing" placeholder="Enter No. of Images per Listing" autocomplete="off">
                        </div>
                    
                      </div>
                    
                      <div class="offset-lg-1 pt-2">
                    
                        <input type="checkbox" name="normal_search_result" class="chkbox"> &nbsp; &nbsp;Appear In search results<br>

                        <input type="checkbox" name="weekly_statistics" class="chkbox"> &nbsp; &nbsp;Weekly Statistics<br>
                    
                        <input type="checkbox" name="basic_analytics" class="chkbox"> &nbsp; &nbsp;Basic Analytics<br>
                    
                        <input type="checkbox" name="prioritized_search_result" class="chkbox"> &nbsp; &nbsp;Prioritized search results<br>
                    
                        <input type="checkbox" name="escrow_platform" class="chkbox"> &nbsp; &nbsp;Ability to process payments intenationally via the Escrow platform<br>
                    
                        <input type="checkbox" name="top_search_result" class="chkbox"> &nbsp; &nbsp;Appear in the top of search results<br>
                    
                        <input type="checkbox" name="choice_listing_on_home_page" class="chkbox"> &nbsp; &nbsp;Eligible to have the listing appear on the home page for 24 hours<br>
                    
                        <input type="checkbox" name="tangible_listing_on_social_media" class="chkbox"> &nbsp; &nbsp;Eligible to appear on Tangible Listing’s social media pages<br>
                    
                      </div>
                  
                  </div>
                   
                  <button type="submit" class="btn btn-primary btn-user btn-block ml-5">Update Settings</button>
                
                </form>
                
                @endif
                
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>


<!------------------------------------------ JS SCRIPT SECTION ------------------------------------------------>

<script type="text/javascript">
    $(document).ready(function(){
      $("#package_setting").validate({
    
        rules: {
          no_of_images_per_listing: {required:true,number:true}

        },
    
        messages: {
          no_of_images_per_listing: {required: "Image Listing can\'t be left blank", number: "Please enter numbers only"}
        },
    
        submitHandler: function(form) {
          var chkbox_arr = [];
          $('.chkbox').each(function(i,j){
            if($(this).is(':checked'))
            {
              chkbox_arr[i] = 'true';
            }else{
              chkbox_arr[i] = 'false';
            }
          });
          
          // --------------
            if($.inArray('true',chkbox_arr) != -1){  
              $('.alert-danger').removeClass('alert').hide();
              form.submit();
            }else{
              $('.alert-danger').addClass('alert offset-lg-1').html('Select atleast one checkbox').show();
            }  
          // --------------
        }

      });
    });
</script>

<!------------------------------------------ JS SCRIPT SECTION ------------------------------------------------>
@stop

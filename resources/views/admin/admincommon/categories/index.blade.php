@extends('admin.common.innercommondefault')
@section('title', 'Category Management')
@section('content')
<style type="text/css">.bttn{ border: none;color: #4e73df;background: white;}</style>
<div class="container">
  <div class="row justify-content-center">
    <div class="col-xl-10 col-lg-12 col-md-9">
      <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h3>Category Permission Management</h3>
            </div>
            <!-- <div class="pull-right">
                @can('userManagement-create')
                <a class="btn btn-primary" href="{{ route('categories.create') }}"> Create New User</a>
            @endcan                    
            </div>-->
            <br> 
        </div>
      </div>
      <div class="card o-hidden border-0 shadow-lg">
        <div class="card-body p-5">
           @if ($message = Session::get('success'))
            <div class="alert alert-success">
              <p>{{ $message }}</p>
            </div>
          @endif

          <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
           <tr>
             <th>Category Name</th>
             <th width="280px">Action</th>
             
           </tr>
           @foreach ($categories as $category)
            <tr>
              <td>{{ $category->website_category}}</td>
              <td>
                 <a href="{{ route('categories.show',$category->id) }}"><i class="fas fa-eye"></i></a>&nbsp;&nbsp;
                 @can('userManagement-create')
                   <a href="{{ route('categories.edit',$category->id) }}"><i class="fas fa-edit"></i></a>&nbsp;&nbsp;
                    {!! Form::close() !!}
                  @endcan
              </td>
            </tr>
           @endforeach
          </table>

          </div>
        </div>
      </div>
    </div>
  </div>
@endsection
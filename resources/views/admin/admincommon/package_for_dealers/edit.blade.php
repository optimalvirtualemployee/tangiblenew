@extends('admin.common.innercommondefault')
@section('title', 'Update Package for Dealers')
@section('content')
<style>
.error {
      color: red;
   }
}      
</style>
<div class="container">
  <!-- Outer Row -->
  <div class="row justify-content-center">
    <div class="col-xl-10 col-lg-12 col-md-9">
      <div class="card o-hidden border-0 shadow-lg my-5">
        <div class="card-body p-0">
          <!-- Nested Row within Card Body -->
          <div class="row">
            <div class="col-lg-8">
              <div class="p-5">
                <div class="text-center">
                   <h1 class="h4 text-gray-900 mb-4">Update Package for Dealers</h1>
                </div>
                @if($errors->any())
                  @foreach($errors->all() as $error)
                    <div class="alert alert-danger">
                      {{$error}}
                    </div>
                  @endforeach
                @endif
                <form class="user" id="updatepackage_for_dealers" method="POST" action="{{ route('package_for_dealers.update',$package_for_dealers_data->id) }}">
                  @method('PATCH')
                  @csrf
                  <div class="form-group">
                    <input id="package_for_dealers" class="form-control form-control-user " type="text"  name="package_for_dealers" placeholder="Enter Package for Dealers" value="{{ $package_for_dealers_data->package_for_dealers }}">
                  </div>
                  <div class="form-group">
                    <label>Status</label>
                    <select id ="status" name="status" class="form-control" required>
                      <option value="1" {{$package_for_dealers_data->status == "1" ? 'selected' : ''}}>Active</option>
                      <option value="0" {{$package_for_dealers_data->status == "0" ? 'selected' : ''}}>Inactive</option>
                    </select>
                  </div> 
                  <button type="submit" class="btn btn-primary btn-user btn-block">Update</button>
                </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>


<!------------------------------------------ JS SCRIPT SECTION ------------------------------------------------>

<script type="text/javascript">
    $(document).ready(function(){
      $("#updatepackage_for_dealers").validate({
    
        rules: {
          package_for_dealers: "required"
        },
    
        messages: {
          package_for_dealers: "Package for Dealers can\'t be left blank"
        },
    
        submitHandler: function(form) {
          form.submit();
        }
      });
    });
</script>

<!------------------------------------------ JS SCRIPT SECTION ------------------------------------------------>
@stop

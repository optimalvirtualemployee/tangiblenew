@extends('admin.common.innercommondefault')
@section('title', 'Display Currencies')
@section('content')

  <!-- Page Wrapper -->
  <div id="wrapper">
    <div id="content-wrapper" class="d-flex flex-column">
      <!-- Main Content -->
      <div id="content">
        <!-- End of Topbar -->
        <!-- Begin Page Content -->
        <div class="container-fluid">
          <!-- Page Heading -->
          <h1 class="h3 mb-2 text-gray-800">Currency</h1>
          @if(Session::has('success_msg'))
            <div class="alert " id="writeMsg" style="background-color: #4e73df;margin-top: 20px;color: white;">{{ Session::get('success_msg') }}</div>
          @endif
          <div class="card shadow mb-4" style="margin-top: 10px;">
            <div class="card-body" >
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0" >
                  <thead>
                    <tr>
                      <th>Currency Code</th>
                      <th>Currency Name</th>
                      <th>Status</th>
                      @can('superAdmin-create')
                      <th>Action</th>
                      @endcan
                    </tr>
                  </thead>
                 
                  <tbody>
                    @foreach ($currencies as $currency)
                      <tr>
                        <td>{{$currency->currency_code}} </td>
                        <td>{{$currency->currency}} </td>
                        <td>@if($currency->status =='1') Active  @else Inactive @endif  </td>
                        @can('superAdmin-create')
                        <td>
                          <a href="{{url('admin/currencySelect/saveSelect', $currency->id)}}"  class="btn btn-primary btn-icon-split">
                          <span class="text">SelectCurrency</span>
                          </a>
                           
                        </td>
                        @endcan
                      </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
        <!-- /.container-fluid -->
      </div>
@stop

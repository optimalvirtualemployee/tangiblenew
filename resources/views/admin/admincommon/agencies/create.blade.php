@extends('admin.common.innercommondefault')
@section('title', 'Create Agencies')
@section('content')

<div class="container">
  <!-- Outer Row -->
  <div class="row justify-content-center">
    <div class="col-xl-10 col-lg-12 col-md-9">
      <div class="card o-hidden border-0 shadow-lg my-5">
        <div class="card-body p-0">
          <!-- Nested Row within Card Body -->
          <div class="row">
            <div class="col-lg-8">
              <div class="p-5">
                <div class="text-center">
                   <h1 class="h4 text-gray-900 mb-4">Create Agent</h1>
                </div>
                @if($errors->any())
                  @foreach($errors->all() as $error)
                    <div class="alert alert-danger">
                      {{$error}}
                    </div>
                  @endforeach
                @endif
                <form class="user" id="createagencies" method="POST" action="{{ route('agencies.store') }}">
                  @csrf
                  <div class="form-group">
                    <label>Name:</label><input id="name" class="form-control form-control-user " type="text"  name="name" placeholder="Enter Name" value="">
                   </div>
                   <div class="form-group">
                      <label>Select Country</label>
                      <select id ="countryid" name="countryid" class="form-control" required>
                        <option value="">Select Country</option>
                         @foreach ($countries as $country)
                           <option value="{{$country->id}}">{{$country->country_name}}</option>
                         @endforeach
                      </select>
                    </div>
                  <div class="form-group">
                      <label>Select State</label>
                      <select id ="stateId" name="stateId" class="form-control">
                        <option value="">Select State</option>
                      </select>
                    </div>
                    <div class="form-group">
                    <label>City:</label><select id="cityId" name="cityId" class="form-control" required>
                         <option value="">Select City</option>
                      </select>             
                      </div>   
                  <div class="form-group">
                    <label>Whatsapp:</label><input id="whatsapp" class="form-control form-control-user " type="text"  name="whatsapp" placeholder="Enter Whatsapp No" value="">
                  </div>
                  <div class="form-group">
                    <label>Mobile:</label><input id="mobile" class="form-control form-control-user " type="text"  name="mobile" placeholder="Enter Mobile No" value="">
                  </div>
                  <div class="form-group">
                    <label>Email:</label><input id="email" class="form-control form-control-user " type="email"  name="email" placeholder="Enter Email Address" value="">
                  </div>
                  <div class="form-group">
                    <label>Address:</label><input id="address" class="form-control form-control-user " type="text"  name="address" placeholder="Enter Address" value="">
                  </div>
                  <button type="submit" class="btn btn-primary btn-user btn-block">Add</button>
                </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>


<!----------------------------------- JS SCRIPT SECTION  ---------------------------------------------------->
<script type="text/javascript">
    $(document).ready(function(){
      $("#createagencies").validate({
        rules: {
          name:"required",
          cityId: "required",
          countryId: "required",
          whatsapp: "required",
          mobile: "required",
          email: "required",
          address: "required",
        },
    
        submitHandler: function(form) {
          form.submit();
        }
      });
	$("#countryid").change(function(){
			var countryId = $(this).val();
			$.ajax({
				url: "/admin/admincommon/getState",
				type: "POST",
				data: {countryId: countryId, _token: '{{csrf_token()}}' },
				dataType: 'json',
				success : function(data){
					var html = `<option value="">Select State</option>`;
					for (var i = 0; i < data.length; i++) {
						  var id = data[i].id;
						  var name = data[i].state_name;
						  var option = `<option value="${data[i].id}">${data[i].state_name}</option>`;
						  html += option;
						}

						$('#stateId').html(html);	
					}
				});
				
				$.ajax({
				url: "/admin/admincommon/getCity",
				type: "POST",
				data: {countryId: countryId  ,_token: '{{csrf_token()}}' },
				dataType: 'json',
				success : function(data){
					var html = `<option value="">Select City</option>`;
					for (var i = 0; i < data.length; i++) {
						  var id = data[i].id;
						  var name = data[i].city_name;
						  var option = `<option value="${data[i].id}">${data[i].city_name}</option>`;
						  html += option;
						}

						$('#cityId').html(html);	
					}
				});
		})
      
    

    $("#stateId").change(function(){
		var stateId = $(this).val();
		$.ajax({
			url: "/admin/admincommon/getCity",
			type: "POST",
			data: {stateId: stateId, _token: '{{csrf_token()}}' },
			dataType: 'json',
			success : function(data){
				var html = `<select id="cityId" name="cityId" class="form-control" required>
				<option value="">Select City</option>`;
				for (var i = 0; i < data.length; i++) {
					  var id = data[i].id;
					  var name = data[i].city_name;
					  var option = `<option value="${data[i].id}">${data[i].city_name}</option>`;
					  html += option;
					}

					$('#cityId').html(html);	
				}
			});
	});
	
    });
</script>
<!----------------------------------- JS SCRIPT SECTION  ---------------------------------------------------->
@stop

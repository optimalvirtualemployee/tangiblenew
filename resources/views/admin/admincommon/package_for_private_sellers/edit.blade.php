@extends('admin.common.innercommondefault')
@section('title', 'Update Package for Private Sellers')
@section('content')
<style>
.error {
      color: red;
   }
.column {
  float: left;
  width: 25%;
  padding: 10px;
  height: 100px; /* Should be removed. Only for demonstration */
}   
</style>
<div class="container">
  <!-- Outer Row -->
  <div class="row justify-content-center">
    <div class="col-xl-10 col-lg-12 col-md-9">
      <div class="card o-hidden border-0 shadow-lg my-5">
        <div class="card-body p-0">
          <!-- Nested Row within Card Body -->
          <div class="row">
            <div class="col-lg-8">
              <div class="p-5">
                <div class="text-center">
                   <h1 class="h4 text-gray-900 mb-4">Update Package for Private Sellers</h1>
                </div>
                @if($errors->any())
                  @foreach($errors->all() as $error)
                    <div class="alert alert-danger">
                      {{$error}}
                    </div>
                  @endforeach
                @endif
                <form class="user" id="updatepackage_for_private_sellers" method="POST" action="{{ route('package_for_private_sellers.update',$package_for_private_sellers_data->id) }}">
                  @method('PATCH')
                  @csrf
                  <div class="form-group">
                    <input id="package_for_private_sellers" class="form-control form-control-user " type="text"  name="package_for_private_sellers" placeholder="Enter Package for Private Sellers" value="{{ $package_for_private_sellers_data->package_for_private_sellers }}">
                  </div>
                  <div class="form-group">
                    <label>Status</label>
                    <select id ="status" name="status" class="form-control" required>
                      <option value="1" {{$package_for_private_sellers_data->status == "1" ? 'selected' : ''}}>Active</option>
                      <option value="0" {{$package_for_private_sellers_data->status == "0" ? 'selected' : ''}}>Inactive</option>
                    </select>
                  </div> 
                  @if($html == null)
                  <div class="form-group">	
                  <div class="row increment">
                  <span id="lblError" style="color: red"></span>
                  <div class="dtbl">
                 <div class="column"><input id="from" class="form-control form-control-user from" type="number" min="0" name="from[]" placeholder="Enter From Range" value=""></div>
                 <div class="column"><input id="to" class="form-control form-control-user to" type="number"  min="0" name="to[]" placeholder="Enter To Range" value=""></div>
                 <div class="column"><input id="pricing" class="form-control form-control-user " type="number" min="0"  name="pricing[]" placeholder="Enter Pricing" value=""></div>
                 <div class="column addbutton"><button class="btn btn-primary btn-success" type="button"><i class="glyphicon glyphicon-plus"></i>Add +</button></div> 
                  </div>
                  </div>
                  </div>
                  @endif
                  <div class="row increment">
                  <span id="lblError" style="color: red"></span>
                  <?php echo $html; ?>
                  </div>
                  
                  <button type="submit" class="btn btn-primary btn-user btn-block">Update</button>
                </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>


<!------------------------------------------ JS SCRIPT SECTION ------------------------------------------------>

<script type="text/javascript">

$(document).on('keyup', '.to', function (e) {
	$("#lblError").html("");
	var fromValue = parseInt($(this).parent().parent().find('.from').val());
	if(fromValue > parseInt($(this).val())){
		$("#lblError").html("To Value Should be greater then From value.");
		}  
});
    $(document).ready(function(){
      $("#updatepackage_for_private_sellers").validate({
    
        rules: {
          package_for_private_sellers: "required"
        },
    
        messages: {
          package_for_private_sellers: "Package for Private Sellers can\'t be left blank"
        },
    
        submitHandler: function(form) {
          form.submit();
        }
      });

      $(".btn-success").click(function(){ 
          var html = '<div class="dtbl"><div class="column"><input id="from" class="form-control form-control-user from " type="number" min="0" name="from[]" placeholder="Enter From Range" value=""></div>';
          	html += '<div class="column"><input id="to" class="form-control form-control-user to" type="number" min="0" name="to[]" placeholder="Enter To Range" value=""></div>';
          	html += '<div class="column"><input id="pricing" class="form-control form-control-user " type="number" min="0" name="pricing[]" placeholder="Enter Pricing" value=""></div>';
          	html += '<div class="column addbutton"><button class="btn btn-primary btn-danger" type="button"><i class="glyphicon glyphicon-plus"></i>Delete</button></div></div>';

          	$(".increment").append(html);
      });

	$("body").on("click",".btn-danger",function(){ 
          $(this).parent().parent().remove();
      });


	$("#updatepackage_for_private_sellers").submit(function(){
		$(this).find('input[type="number"],select').each(function(){
			$(this).css('border','');
    	});
    	var isError = false;
			$(this).find('input[type="number"],select').each(function(){	
				if($(this).val()==""){
					$(this).css('border','1px solid red');
					isError = true;
				}
			})

			if(isError == false){
				return true;
			}else{
				return false;
			}
    	})
    });
</script>

<!------------------------------------------ JS SCRIPT SECTION ------------------------------------------------>
@stop

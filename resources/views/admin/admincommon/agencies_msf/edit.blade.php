@extends('admin.common.innercommondefault')
@section('title', 'Update Dealer')
@section('content')
<style>
add css for multi step form
.wizard a,
.tabcontrol a
{
outline: 0;
}

.wizard ul,
.tabcontrol ul
{
list-style: none !important;
padding: 0;
margin: 0;
}

.wizard ul > li,
.tabcontrol ul > li
{
display: block;
padding: 0;
}

/ Accessibility /
.wizard > .steps .current-info,
.tabcontrol > .steps .current-info
{
position: absolute;
left: -999em;
}

.wizard > .content > .title,
.tabcontrol > .content > .title
{
position: absolute;
left: -999em;
}



/*
Wizard
*/

.wizard > .steps
{
position: relative;
display: block;
width: 100%;
}

.wizard.vertical > .steps
{
display: inline;
float: left;
width: 30%;
}

.wizard > .steps .number
{
background-color: #FFF;
color: #106db2;
border-radius: 100%;
padding-right: 3px;
padding-left: 5px;

}

.wizard > .steps > ul > li
{
width: 25%;
}

.wizard > .steps > ul > li,
.wizard > .actions > ul > li
{
float: left;
}

.wizard.vertical > .steps > ul > li
{
float: none;
width: 100%;
}

.wizard > .steps a,
.wizard > .steps a:hover,
.wizard > .steps a:active
{
display: block;
width: auto;
margin: 0 0.5em 0.5em;
padding: 1em 1em;
text-decoration: none;

-webkit-border-radius: 5px;
-moz-border-radius: 5px;
border-radius: 5px;
}

.wizard > .steps .disabled a,
.wizard > .steps .disabled a:hover,
.wizard > .steps .disabled a:active
{
background: #eee;
color: #aaa;
cursor: default;
}

.wizard > .steps .current a,
.wizard > .steps .current a:hover,
.wizard > .steps .current a:active
{
background: #fcfcfc;
color: #111;
cursor: default;
}

.wizard > .steps .done a,
.wizard > .steps .done a:hover,
.wizard > .steps .done a:active
{
background: #eeeeee;
color: #000;
}

.wizard > .steps .error a,
.wizard > .steps .error a:hover,
.wizard > .steps .error a:active
{
background: #eeeeee;
color: #000;
}

.wizard > .content
{
background: #eee;
display: block;
margin: 0.5em;
min-height: 25em;
overflow: scroll;
position: relative;
width: auto;

-webkit-border-radius: 5px;
-moz-border-radius: 5px;
border-radius: 5px;
}

.wizard.vertical > .content
{
display: inline;
float: left;
margin: 0 2.5% 0.5em 2.5%;
width: 65%;
}

.wizard > .content > .body
{
float: left;
position: absolute;
width: 95%;
height: 95%;
padding: 2.5%;
}

.wizard > .content > .body ul
{
list-style: disc !important;
}

.wizard > .content > .body ul > li
{
display: list-item;
}

.wizard > .content > .body > iframe
{
border: 0 none;
width: 100%;
height: 100%;
}

.wizard > .content > .body input
{
display: block;
border: 1px solid #ccc;
}

.wizard > .content > .body input[type="checkbox"]
{
display: inline-block;
}

.wizard > .content > .body input.error
{
border: 1px solid #fbc2c4;
color: #8a1f11;
}

.wizard > .content > .body label
{
display: inline;
margin-bottom: 0.5em;
}

.wizard > .content > .body label.error
{
color: #ff1e00;
display: inline;
font-size: 12px;
}

.wizard > .actions
{
position: relative;
display: block;
text-align: right;
width: 100%;
}

.wizard.vertical > .actions
{
display: inline;
float: right;
margin: 0 2.5%;
width: 95%;
}

.wizard > .actions > ul
{
display: inline-block;
text-align: right;
}

.wizard > .actions > ul > li
{
margin: 0 0.5em;
}

.wizard.vertical > .actions > ul > li
{
margin: 0 0 0 1em;
}

.wizard > .actions a,
.wizard > .actions a:hover,
.wizard > .actions a:active
{
background: #2184be;
color: #fff;
display: block;
padding: 0.5em 1em;
text-decoration: none;

-webkit-border-radius: 5px;
-moz-border-radius: 5px;
border-radius: 5px;
}

.wizard > .actions .disabled a,
.wizard > .actions .disabled a:hover,
.wizard > .actions .disabled a:active
{
background: #eee;
color: #aaa;
}

.wizard > .loading
{
}

.wizard > .loading .spinner
{
}

.wizard > .steps .current a, .wizard > .steps .current a:hover, .wizard > .steps .current a:active {
background: #106db2;
color: #fff;
cursor: default;
font-size: 16px;
font-weight: 700;
/ padding: 20px 10px; /
}

/*
Tabcontrol
*/

.tabcontrol > .steps
{
position: relative;
display: block;
width: 100%;
}

.tabcontrol > .steps > ul
{
position: relative;
margin: 6px 0 0 0;
top: 1px;
z-index: 1;
}

.tabcontrol > .steps > ul > li
{
float: left;
margin: 5px 2px 0 0;
padding: 1px;

-webkit-border-top-left-radius: 5px;
-webkit-border-top-right-radius: 5px;
-moz-border-radius-topleft: 5px;
-moz-border-radius-topright: 5px;
border-top-left-radius: 5px;
border-top-right-radius: 5px;
}

.tabcontrol > .steps > ul > li:hover
{
background: #edecec;
border: 1px solid #bbb;
padding: 0;
}

.tabcontrol > .steps > ul > li.current
{
background: #fff;
border: 1px solid #bbb;
border-bottom: 0 none;
padding: 0 0 1px 0;
margin-top: 0;
}

.tabcontrol > .steps > ul > li > a
{
color: #5f5f5f;
display: inline-block;
border: 0 none;
margin: 0;
padding: 10px 30px;
text-decoration: none;
}

.tabcontrol > .steps > ul > li > a:hover
{
text-decoration: none;
}

.tabcontrol > .steps > ul > li.current > a
{
padding: 15px 30px 10px 30px;
}

.tabcontrol > .content
{
position: relative;
display: inline-block;
width: 100%;
height: 35em;
overflow: hidden;
border-top: 1px solid #bbb;
padding-top: 20px;
}

.tabcontrol > .content > .body
{
float: left;
position: absolute;
width: 95%;
height: 95%;
padding: 2.5%;
}

.tabcontrol > .content > .body ul
{
list-style: disc !important;
}

.tabcontrol > .content > .body ul > li
{
display: list-item;
}

#contact input[type="text"],
#contact input[type="email"],
#contact input[type="tel"],
#contact input[type="url"],
#contact textarea,
#contact button[type="submit"] {
font: 400 12px/16px "Titillium Web", Helvetica, Arial, sans-serif;
}

#contact {
background: #F9F9F9;
padding: 25px;

}

#contact h3 {
display: block;
font-size: 30px;
font-weight: 300;
margin-bottom: 10px;
}

#contact h4 {
margin: 5px 0 15px;
display: block;
font-size: 13px;
font-weight: 400;
}

fieldset {
border: medium none !important;
margin: 0 0 10px;
min-width: 100%;
padding: 0;
width: 100%;
}

#contact input[type="text"],
#contact input[type="email"],
#contact input[type="tel"],
#contact input[type="url"],
#contact textarea {
width: 100%;
border: 1px solid #ccc;
background: #FFF;
margin: 0 0 5px;
padding: 10px;
}

#contact input[type="text"]:hover,
#contact input[type="email"]:hover,
#contact input[type="tel"]:hover,
#contact input[type="url"]:hover,
#contact textarea:hover {
-webkit-transition: border-color 0.3s ease-in-out;
-moz-transition: border-color 0.3s ease-in-out;
transition: border-color 0.3s ease-in-out;
border: 1px solid #aaa;
}

#contact textarea {
height: 100px;
max-width: 100%;
resize: none;
}

#contact button[type="submit"] {
cursor: pointer;
border: none;
background: #4CAF50;
color: #FFF;
margin: 0 0 5px;
padding: 10px;
font-size: 15px;
}

#contact button[type="submit"]:hover {
background: #43A047;
-webkit-transition: background 0.3s ease-in-out;
-moz-transition: background 0.3s ease-in-out;
transition: background-color 0.3s ease-in-out;
}

#contact button[type="submit"]:active {
box-shadow: inset 0 1px 3px rgba(0, 0, 0, 0.5);
}

.copyright {
text-align: center;
}

#contact input:focus,
#contact textarea:focus {
outline: 0;
border: 1px solid #aaa;
}



.steps > ul > li > a,
.actions li a {
padding: 10px;
text-decoration: none;
margin: 1px;
display: block;
color: #777;
}
.steps > ul > li,
.actions li {
list-style:none;
}
#loading-image{
  position:fixed;
  top:0px;
  right:0px;
  width:100%;
  height:100%;
  background-color:#666;
  background-image:url('/assets/img/Spinner-loader.gif');
  background-repeat:no-repeat;
  background-position:center;
  z-index:10000000;
  opacity:0.6;
}
span.current-info.audible {
  display: none;
}
</style>
<div class="container">
    <div class="row justify-content-center mt-5">
      <div class="" style="width: 100%;">
        <div class=""> 
         <!-- Nested Row within Card Body -->
          <div class="row">
            <div class="col-lg-12">
              <div class="">
                       <div id='loading-image' style='display:none'>
				</div>
                <div class="container mt-3">
@if (count($errors) > 0)
              <div class="alert alert-danger">
                <strong>Whoops!</strong> There were some problems with your input.<br><br>
                <ul>
                   @foreach ($errors->all() as $error)
                     <li>{{ $error }}</li>
                   @endforeach
                </ul>
              </div>
            @endif
                  <!-- Tab panes -->
                  <div class="tab-content">
                    <div id="standalone" class="container tab-pane active"><br>
                      <div class="container">
                           <form id="updateproperty" action="{{ url('admin/agenciesData/update',$agencies->id) }}"  method="POST" enctype="multipart/form-data">
                          @csrf
                          <div>
                            <h3>Dealer Info</h3>
                            <section>
                              <div class="form-group row mx-auto">
     
                                <div class="col-md-12 col-lg-4">
                                 <label>First Name*</label><input id="first_name" class="form-control form-control-user required" type="text"  name="first_name" placeholder="Enter Name" value="{{$agencies->first_name}}">   
                                </div>
                                <div class="col-md-12 col-lg-4">
                                  <label>Last Name*</label><input id="last_name" class="form-control form-control-user required" type="text"  name="last_name" placeholder="Enter Name" value="{{$agencies->last_name}}">
                              </div>
                              <div class="col-md-12 col-lg-4">
                                  <label>Company Name*</label><input id="company_name" class="form-control form-control-user required" type="text"  name="company_name" placeholder="Enter Company Name" value="{{$agencies->company_name}}">
                              </div>
                            </div>
                            <div class="form-group row mx-auto">
                            <div class="col-md-12 col-lg-4">
                                <label>Country*</label>
                                <select id="countryId" class="form-control required" name="countryId">
                                    @foreach ($country_data as $country)
                                       <option @if($countrySelected->id ?? '') @if($countrySelected->id==$country->id) selected @endif @endif value="{{$country->id}}">{{$country->country_name}}</option>
                                     @endforeach
                                </select>
                              </div>
                              <div class="col-md-12 col-lg-4">
                                <label>State</label>
                                <select id="stateId"  name="stateId" class="form-control required" >
                                @foreach ($state_data as $state)
                                  <option @if($stateSelected->id ?? '') @if($stateSelected->id==$state->id) selected @endif @endif value="{{$state->id}}">{{$state->state_name != '' ? $state->state_name : 'Please Select State'}}</option>
                                @endforeach
                                </select>
                              </div>
                              <div class="col-md-12 col-lg-4">
                                <label>City*</label>
                                <select id="cityId" name="cityId" class="form-control required">
                                   @foreach ($city_data as $city)
                                  <option @if($citySelected->id ?? '') @if($citySelected->id==$city->id) selected @endif @endif value="{{$city->id}}">{{$city->city_name}}</option>
                                @endforeach
                                </select>
                              </div>
                            </div>
                            <div class="form-group row mx-auto">
                            <div class="col-md-12 col-lg-4">
                                <label>Street*</label><input id="street_1" class="form-control form-control-user required" type="text"  name="street_1" placeholder="Enter Street 1" value="{{$agencies->street_1}}">
                              </div>
                            <div class="col-md-12 col-lg-4">
                                <label>Street line 2*</label><input id="street_2" class="form-control form-control-user required" type="text"  name="street_2" placeholder="Enter Street line 2" value="{{$agencies->street_2}}">
                              </div>
                              <div class="col-md-12 col-lg-4">
                                <label>Postal Code*</label><input id="postal_code" class="form-control form-control-user required" type="text"  name="postal_code" placeholder="Enter Postal Code" value="{{$agencies->postal_code}}">
                              </div>
                            </div>
                            <div class="form-group row mx-auto">
                            <div class="col-md-12 col-lg-4">
                               <label>Whatsapp*</label><input id="whatsapp" class="form-control form-control-user required" type="number" min= "0"  name="whatsapp" placeholder="Enter Whatsapp No" value="{{$agencies->whatsapp}}">    
                              </div>
                              <div class="col-md-12 col-lg-4">
                                <label>Mobile*</label><input id="mobile" class="form-control form-control-user required" type="number" min= "0" name="mobile" placeholder="Enter Mobile No" value="{{$agencies->phone_no}}">
                              </div>
                              <div class="col-md-12 col-lg-4">
                                <label>Email*</label><input id="email" class="form-control form-control-user " type="email"  name="email" placeholder="Enter Email Address" value="{{$agencies->email}}" readonly>
                              </div>
                                
                            </div>
                            <div class="form-group row mx-auto">
                            <div class="col-md-12 col-lg-4">
                              <strong>Password*</strong>
                        		<input type="password" class= "form-control" placeholder="Password" id="password" name="password">  
                              </div>
                              <div class="col-md-12 col-lg-4">
                                <strong>Confirm Password*</strong>
                        		<input class="form-control" type="password" placeholder="Confirm Password" id="confirm_password" name="confirmpassword">
                              </div>
                              <div class="col-md-12 col-lg-4">
                          <label class="form-control-label" for="timezone">Timezone</label>
                          <select id="timezone" class="form-control" required name="timezone">
                            @foreach ($timezone_data as $timezone)
                                       <option @if($timezoneSelected->id ?? '') @if($timezoneSelected->id==$timezone->id) selected @endif @endif value="{{$timezone->id}}">{{$timezone->name .' '. $timezone->offset}}</option>
                                     @endforeach
                          </select>
                        </div>
                            </div>
                            <div class="form-group row mx-auto">
                                <div class="col-md-12 col-lg-4">
                                <label>Category*</label>
                                <select id="categoryId" class="form-control required" name="categoryId">
                                    @foreach ($category_data as $category)
                                       <option @if($categorySelected->id ?? '') @if($categorySelected->id==$category->id) selected @endif @endif value="{{$category->id}}">{{$category->website_category}}</option>
                                     @endforeach
                                </select>
                              </div>
                            <div class="col-md-12 col-lg-4">
                      <label>Status*</label>
                      <select id ="status" name="status" class="form-control required" required>
                        <option value="1" {{$agencies->status == "1" ? 'selected' : ''}}>Active</option>
                        <option value="0" {{$agencies->status == "0" ? 'selected' : ''}}>Inactive</option>
                      </select>
                    </div>
                            </div>
                            </section>
                            <h3>Dealer Logo</h3>
                            <section>
                              <div class="form-group row mx-auto">
                                 @foreach ($images_data as $data)
                                    <tr >
                                      <td>
                                        <img src="{{url('uploads/'.$data->filename)}}" id="img_{{$data->id}}" class="img-thumbnail" width="75" />
                                      </td>
                                      <td>
                                        <button type="button" class="imgDelete" id="imgDelete_{{$data->id}}">
                                          <i class="fas fa-trash-alt"></i>
                                        </button> &nbsp; &nbsp;
                                    </td>
                                    </tr>
                                  @endforeach
                                
                              </div>
                              <div class="form-group row mx-auto">
                                <div class="input-group control-group increment">
                                  <input type="file" name="photos[]" class="form-control" value="fileupload">
                                </div>
                                
                                <div class="clone hide" hidden>
                                  <div class="control-group input-group"
                                    style="margin-top: 10px">
                                    <input type="file" name="photos[]" class="form-control">
                                    <div class="input-group-btn">
                                    <button class="btn btn-primary  btn-danger" type="button">
                                        <i class="glyphicon glyphicon-remove"></i> Remove
                                      </button>
                                      </div>
                                  </div>
                                </div>
                                <div class="input-group-btn">
                                    <button class="btn btn-primary  btn-success" type="button">
                                      <i class="glyphicon glyphicon-plus"></i>Add
                                    </button>
                                  </div>
                                
                              </div>
                             
                              </br>
                            
                            </section>
                              
                          </div>
                        </form>
                      
                      </div>
                    </div>
<!--                     <div id="chain" class="container tab-pane fade col-md-6"><br>
                      <form id="contact" action="" autocomplete="on" method="POST" enctype="multipart/form-data">
                        @CSRF
                        <div class="col-md-12">
                          <input type="hidden" name="venue_type" value="1">
                          <label for="confirm">Venue Upload *</label>
                          <input type="file" class="form-control required"  name="venue_file" value="{{ old('venue_file') }}">
                        </div>
                        <div class="form-group download_sample">
                               <label for="name" class="mr-sm-2">Download Sample File</label><br>
                           <a href="{{url('assets/file/venue1.xlsx')}}" download><button class="btn btn-primary btn-sm"    type="button" id="dropdownMenuButton">Download Sample File</button></a>
                        </div>

                        <a href="#" style="margin-left:10px">
                          <button type="" class="btn btn-primary mt-4">Submit</button>
                        </a>
                      </form>
                    </div>  -->
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script> -->
<script type="text/javascript" src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.7/jquery.validate.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-steps/1.1.0/jquery.steps.min.js"></script>

<script type="text/javascript">

  $(document).ready(function(){

var form = $("#updateproperty");

form.validate({
errorPlacement: function errorPlacement(error, element) { element.before(error); },

});

form.children("div").steps({
headerTag: "h3",
bodyTag: "section",
transitionEffect: "slideLeft",
autoFocus: true,
/*saveState: true,*/

onStepChanging: function (event, currentIndex, newIndex)
{
if(currentIndex == 4){
if($('#mrp_of_product').val() >= $('#cost_of_product').val()){
form.validate().settings.ignore = ":disabled,:hidden";
return form.valid();
}else{
return alert('MRP of cost should be less than or equal to cost of product');
}
}
form.validate().settings.ignore = ":disabled,:hidden";
return form.valid();

},
onFinishing: function (event, currentIndex)
{

form.validate().settings.ignore = ":disabled";
return form.valid();
},
onFinished: function (event, currentIndex)
{

/*alert('are you sure you want to save venue.!!');*/
$('#updateproperty').submit();
}

});

  });

</script>
<script type="text/javascript">
  $(document).ready(function() {
      $(".imgDelete").click(function() {
        var ids = $(this).attr("id");
        var id = ids.split("_").pop();
        $('#loading-image').show();
        $.ajax({
          url: "/admin/agenciesDeleteImage/"+id,
          type: "GET",
          success : function(data){
                 $('#img_'+id).remove();     
                 $('#imgDelete_'+id).remove();
                 $('#loading-image').hide();               
            }
          });
        
      });
  });

   
    $(document).ready(function() {

        $(".btn-success").click(function(){ 
            var html = $(".clone").html();
            $(".increment").after(html);
        });

        $("body").on("click",".btn-danger",function(){ 
            $(this).parents(".control-group").remove();
        });

        $("#countryId").change(function(){
        var countryId = $(this).val();
        $('#loading-image').show();
        $.ajax({
          url: "/admin/admincommon/getState",
          type: "POST",
          data: {countryId: countryId, _token: '{{csrf_token()}}' },
          dataType: 'json',
          success : function(data){
            var html = `<option value="">Select State</option>`;
            for (var i = 0; i < data.length; i++) {
                var id = data[i].id;
                var name = data[i].state_name;
                var option = `<option value="${data[i].id}">${data[i].state_name}</option>`;
                html += option;
              }
            $('#loading-image').hide();
              $('#stateId').html(html); 
            }
          });
        $('#loading-image').show();
        $.ajax({
			url: "/admin/admincommon/getCity",
			type: "POST",
			data: {countryId: countryId  ,_token: '{{csrf_token()}}' },
			dataType: 'json',
			success : function(data){
				var html = `<option value="">Select City</option>`;
				for (var i = 0; i < data.length; i++) {
					  var id = data[i].id;
					  var name = data[i].city_name;
					  var option = `<option value="${data[i].id}">${data[i].city_name}</option>`;
					  html += option;
					}
				$('#loading-image').hide();
					$('#cityId').html(html);	
				}
			});
      });
        $("#stateId").change(function(){
        	console.log('inside state id');
        var stateId = $(this).val();
        $('#loading-image').show();
        $.ajax({
          url: "/admin/admincommon/getCity",
          type: "POST",
          data: {stateId: stateId, _token: '{{csrf_token()}}' },
          dataType: 'json',
          success : function(data){
            var html = `<option value="">Select City</option>`;
            for (var i = 0; i < data.length; i++) {
            	console.log('data-----------');
                var id = data[i].id;
                var name = data[i].city_name;
                var option = `<option value="${data[i].id}">${data[i].city_name}</option>`;
                html += option;
              }
            $('#loading-image').hide();
              $('#cityId').html(html);  
            }
          });
      });

      });

</script>
@stop
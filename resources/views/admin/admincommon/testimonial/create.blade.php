@extends('admin.common.innercommondefault')
@section('title', 'Create Testimonial')
@section('content')
<style> .error { color: red; } </style>
<div class="container">
  <div class="row justify-content-center">
    <div class="col-xl-10 col-lg-12 col-md-9">
      <div class="card o-hidden border-0 shadow-lg my-5">
        <div class="card-body p-0">
          <div class="row">
            <div class="col-lg-8">
              <div class="p-5">
                <div class="text-center">
                  <h1 class="h4 text-gray-900 mb-4">Create Testimonial</h1>
                </div>
                @if($errors->any())
                  @foreach($errors->all() as $error)
                    <div class="alert alert-danger" style="color:red">
                      {{$error}}
                    </div>
                  @endforeach
                @endif
                <form class="user" id="testimonialCreateForm" method="POST" action="{{ route('testimonial.store') }}" enctype="multipart/form-data">
                  @csrf
                    <div class="form-group">
                      <label>Name</label>
                      <input id="name" value="{{old('name')}}" class="form-control form-control-user" type="text" maxlength="50" name="name" placeholder="Enter Name">
                    </div>
                    <div class="form-group">
                      <label>Designation</label>
                      <input id="designation" value="{{old('designation')}}"  class="form-control form-control-user" type="text" maxlength="50" name="designation" placeholder="Enter Designation">
                    </div>
                    <div class="form-group">
                      <label>Testimonial Message</label>
                      <textarea id="message" value="{{old('message')}}" class="form-control form-control-user" type="text" maxlength="1000" name="message" placeholder="Enter message">@if(old('message')) {{old('message')}}@endif</textarea> 
                    </div>

                   <div class="form-group">
                    <label>Choose Image</label>
                   <div class="input-group control-group increment">
											<input type="file" value="{{old('image')}}" name="image" class="form-control">
										</div>
                   </div>
                   <div class="form-group">
                      <label>Image Status</label>
                      <select id ="image_status" name="image_status" value="{{old('image_status')}}" class="form-control">
                       <option value="">Select Image Status</option>
                        <option value="1" >Active</option>
                        <option value="0" >Inactive</option>
                      </select>
                    </div>
                    <div class="form-group">
                      <label>Status</label>
                      <select id ="status" name="status" value="{{old('status')}}" class="form-control">
                       <option value="">Select Status</option>
                        <option value="1" >Active</option>
                        <option value="0" >Inactive</option>
                      </select>
                    </div>
                      <button type="submit" class="btn btn-primary btn-user btn-block"> Create</button>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
<script type="text/javascript">
$(document).ready(function(){
	$("#name").keypress(function (e) {
        var keyCode = e.keyCode || e.which;
        $("#lblError").remove();
        //Regex for Valid Characters i.e. Alphabets.
        var regex = /^[A-Za-z\s]+$/;
        //Validate TextBox value against the Regex.
        var isValid = regex.test(String.fromCharCode(keyCode));
        if (!isValid) {
            $('#name').after('<div id="lblError" style="color:red">Only Alphabets allowed.</div>');
        }
        return isValid;
    });

	$("#designation").keypress(function (e) {
        var keyCode = e.keyCode || e.which;
        $("#lblError").remove();
        //Regex for Valid Characters i.e. Alphabets.
        var regex = /^[A-Za-z\s]+$/;
        //Validate TextBox value against the Regex.
        var isValid = regex.test(String.fromCharCode(keyCode));
        if (!isValid) {
            $('#designation').after('<div id="lblError" style="color:red">Only Alphabets allowed.</div>');
        }

        return isValid;
    });
	 
  $("#testimonialCreateForm").validate({
    
    rules: {
     
      name:         "required",
      message:      "required",
      designation:  "required",
      image:        "required",
      image_status :"required",
      status:       "required"
    
      },
    
    messages: {
      countryname: "Country cannot be empty",
      
    },
    
    submitHandler: function(form) {
      form.submit();
    }
  }); 
});
</script>

@stop

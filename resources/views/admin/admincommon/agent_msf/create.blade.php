@extends('admin.common.innercommondefault')
@section('title', 'Create Agent')
@section('content')

<style>
add css for multi step form
.wizard a,
.tabcontrol a
{
outline: 0;
}

.wizard ul,
.tabcontrol ul
{
list-style: none !important;
padding: 0;
margin: 0;
}

.wizard ul > li,
.tabcontrol ul > li
{
display: block;
padding: 0;
}

/ Accessibility /
.wizard > .steps .current-info,
.tabcontrol > .steps .current-info
{
position: absolute;
left: -999em;
}

.wizard > .content > .title,
.tabcontrol > .content > .title
{
position: absolute;
left: -999em;
}



/*
Wizard
*/

.wizard > .steps
{
position: relative;
display: block;
width: 100%;
}

.wizard.vertical > .steps
{
display: inline;
float: left;
width: 30%;
}

.wizard > .steps .number
{
background-color: #FFF;
color: #106db2;
border-radius: 100%;
padding-right: 3px;
padding-left: 5px;

}

.wizard > .steps > ul > li
{
width: 25%;
}

.wizard > .steps > ul > li,
.wizard > .actions > ul > li
{
float: left;
}

.wizard.vertical > .steps > ul > li
{
float: none;
width: 100%;
}

.wizard > .steps a,
.wizard > .steps a:hover,
.wizard > .steps a:active
{
display: block;
width: auto;
margin: 0 0.5em 0.5em;
padding: 1em 1em;
text-decoration: none;

-webkit-border-radius: 5px;
-moz-border-radius: 5px;
border-radius: 5px;
}

.wizard > .steps .disabled a,
.wizard > .steps .disabled a:hover,
.wizard > .steps .disabled a:active
{
background: #eee;
color: #aaa;
cursor: default;
}

.wizard > .steps .current a,
.wizard > .steps .current a:hover,
.wizard > .steps .current a:active
{
background: #fcfcfc;
color: #111;
cursor: default;
}

.wizard > .steps .done a,
.wizard > .steps .done a:hover,
.wizard > .steps .done a:active
{
background: #eeeeee;
color: #000;
}

.wizard > .steps .error a,
.wizard > .steps .error a:hover,
.wizard > .steps .error a:active
{
background: #eeeeee;
color: #000;
}

.wizard > .content
{
background: #eee;
display: block;
margin: 0.5em;
min-height: 25em;
overflow: scroll;
position: relative;
width: auto;

-webkit-border-radius: 5px;
-moz-border-radius: 5px;
border-radius: 5px;
}

.wizard.vertical > .content
{
display: inline;
float: left;
margin: 0 2.5% 0.5em 2.5%;
width: 65%;
}

.wizard > .content > .body
{
float: left;
position: absolute;
width: 95%;
height: 95%;
padding: 2.5%;
}

.wizard > .content > .body ul
{
list-style: disc !important;
}

.wizard > .content > .body ul > li
{
display: list-item;
}

.wizard > .content > .body > iframe
{
border: 0 none;
width: 100%;
height: 100%;
}

.wizard > .content > .body input
{
display: block;
border: 1px solid #ccc;
}

.wizard > .content > .body input[type="checkbox"]
{
display: inline-block;
}

.wizard > .content > .body input.error
{
border: 1px solid #fbc2c4;
color: #8a1f11;
}

.wizard > .content > .body label
{
display: inline;
margin-bottom: 0.5em;
}

.wizard > .content > .body label.error
{
color: #ff1e00;
display: inline;
font-size: 12px;
}

.wizard > .actions
{
position: relative;
display: block;
text-align: right;
width: 100%;
}

.wizard.vertical > .actions
{
display: inline;
float: right;
margin: 0 2.5%;
width: 95%;
}

.wizard > .actions > ul
{
display: inline-block;
text-align: right;
}

.wizard > .actions > ul > li
{
margin: 0 0.5em;
}

.wizard.vertical > .actions > ul > li
{
margin: 0 0 0 1em;
}

.wizard > .actions a,
.wizard > .actions a:hover,
.wizard > .actions a:active
{
background: #2184be;
color: #fff;
display: block;
padding: 0.5em 1em;
text-decoration: none;

-webkit-border-radius: 5px;
-moz-border-radius: 5px;
border-radius: 5px;
}

.wizard > .actions .disabled a,
.wizard > .actions .disabled a:hover,
.wizard > .actions .disabled a:active
{
background: #eee;
color: #aaa;
}

.wizard > .loading
{
}

.wizard > .loading .spinner
{
}

.wizard > .steps .current a, .wizard > .steps .current a:hover, .wizard > .steps .current a:active {
background: #106db2;
color: #fff;
cursor: default;
font-size: 16px;
font-weight: 700;
/ padding: 20px 10px; /
}

/*
Tabcontrol
*/

.tabcontrol > .steps
{
position: relative;
display: block;
width: 100%;
}

.tabcontrol > .steps > ul
{
position: relative;
margin: 6px 0 0 0;
top: 1px;
z-index: 1;
}

.tabcontrol > .steps > ul > li
{
float: left;
margin: 5px 2px 0 0;
padding: 1px;

-webkit-border-top-left-radius: 5px;
-webkit-border-top-right-radius: 5px;
-moz-border-radius-topleft: 5px;
-moz-border-radius-topright: 5px;
border-top-left-radius: 5px;
border-top-right-radius: 5px;
}

.tabcontrol > .steps > ul > li:hover
{
background: #edecec;
border: 1px solid #bbb;
padding: 0;
}

.tabcontrol > .steps > ul > li.current
{
background: #fff;
border: 1px solid #bbb;
border-bottom: 0 none;
padding: 0 0 1px 0;
margin-top: 0;
}

.tabcontrol > .steps > ul > li > a
{
color: #5f5f5f;
display: inline-block;
border: 0 none;
margin: 0;
padding: 10px 30px;
text-decoration: none;
}

.tabcontrol > .steps > ul > li > a:hover
{
text-decoration: none;
}

.tabcontrol > .steps > ul > li.current > a
{
padding: 15px 30px 10px 30px;
}

.tabcontrol > .content
{
position: relative;
display: inline-block;
width: 100%;
height: 35em;
overflow: hidden;
border-top: 1px solid #bbb;
padding-top: 20px;
}

.tabcontrol > .content > .body
{
float: left;
position: absolute;
width: 95%;
height: 95%;
padding: 2.5%;
}

.tabcontrol > .content > .body ul
{
list-style: disc !important;
}

.tabcontrol > .content > .body ul > li
{
display: list-item;
}

#contact input[type="text"],
#contact input[type="email"],
#contact input[type="tel"],
#contact input[type="url"],
#contact textarea,
#contact button[type="submit"] {
font: 400 12px/16px "Titillium Web", Helvetica, Arial, sans-serif;
}

#contact {
background: #F9F9F9;
padding: 25px;

}

#contact h3 {
display: block;
font-size: 30px;
font-weight: 300;
margin-bottom: 10px;
}

#contact h4 {
margin: 5px 0 15px;
display: block;
font-size: 13px;
font-weight: 400;
}

fieldset {
border: medium none !important;
margin: 0 0 10px;
min-width: 100%;
padding: 0;
width: 100%;
}

#contact input[type="text"],
#contact input[type="email"],
#contact input[type="tel"],
#contact input[type="url"],
#contact textarea {
width: 100%;
border: 1px solid #ccc;
background: #FFF;
margin: 0 0 5px;
padding: 10px;
}

#contact input[type="text"]:hover,
#contact input[type="email"]:hover,
#contact input[type="tel"]:hover,
#contact input[type="url"]:hover,
#contact textarea:hover {
-webkit-transition: border-color 0.3s ease-in-out;
-moz-transition: border-color 0.3s ease-in-out;
transition: border-color 0.3s ease-in-out;
border: 1px solid #aaa;
}

#contact textarea {
height: 100px;
max-width: 100%;
resize: none;
}

#contact button[type="submit"] {
cursor: pointer;
border: none;
background: #4CAF50;
color: #FFF;
margin: 0 0 5px;
padding: 10px;
font-size: 15px;
}

#contact button[type="submit"]:hover {
background: #43A047;
-webkit-transition: background 0.3s ease-in-out;
-moz-transition: background 0.3s ease-in-out;
transition: background-color 0.3s ease-in-out;
}

#contact button[type="submit"]:active {
box-shadow: inset 0 1px 3px rgba(0, 0, 0, 0.5);
}

.copyright {
text-align: center;
}

#contact input:focus,
#contact textarea:focus {
outline: 0;
border: 1px solid #aaa;
}



.steps > ul > li > a,
.actions li a {
padding: 10px;
text-decoration: none;
margin: 1px;
display: block;
color: #777;
}
.steps > ul > li,
.actions li {
list-style:none;
}
   #loading-image{
  position:fixed;
  top:0px;
  right:0px;
  width:100%;
  height:100%;
  background-color:#666;
  background-image:url('/assets/img/Spinner-loader.gif');
  background-repeat:no-repeat;
  background-position:center;
  z-index:10000000;
   }
   span.current-info.audible {
  display: none;
}
</style>
<div class="container">
    <div class="row justify-content-center mt-5">
      <div class="" style="width: 100%;">
        <div class=""> 
         <!-- Nested Row within Card Body -->
          <div class="row">
            <div class="col-lg-12">
              <div class="">
                       <div id='loading-image' style='display:none'>
				</div>
                <div class="container mt-3">
@if (count($errors) > 0)
              <div class="alert alert-danger">
                <strong>Whoops!</strong> There were some problems with your input.<br><br>
                <ul>
                   @foreach ($errors->all() as $error)
                     <li>{{ $error }}</li>
                   @endforeach
                </ul>
              </div>
            @endif
                  <!-- Tab panes -->
                  <div class="tab-content">
                    <div id="standalone" class="container tab-pane active"><br>
                      <div class="container">
                        <form id="contact" action="{{ route('agentdata.store') }}" autocomplete="on" method="POST" enctype="multipart/form-data">
                          @csrf
                          <div>
                            <h3>Dealer Info</h3>
                            <section>
                              <div class="form-group row mx-auto">
                                <div class="col-md-12 col-lg-4">
                                  <label>Select Dealer</label>
                      <select id ="agencyId" name="agencyId" class="form-control">
                        <option value="">Select Dealer</option>
                         @foreach ($agencies as $agency)
                           <option value="{{$agency->id}}">{{$agency->first_name .' '.$agency->last_name}}</option>
                         @endforeach
                      </select>
                                </div>
                                <div class="col-md-12 col-lg-4">
                                  <label>First Name*</label><input id="first_name" class="form-control form-control-user required" type="text"  name="first_name" placeholder="Enter First Name" value="">
                              </div>
                              <div class="col-md-12 col-lg-4">
                                <label>Last Name*</label><input id="last_name" class="form-control form-control-user required" type="text"  name="last_name" placeholder="Enter Last Name" value="">
                              </div>
                            </div>
                            <div class="form-group row mx-auto">
							  <div class="col-md-12 col-lg-4">
                                <label>Country*</label>
                                <select id="countryId" class="form-control required"  name="countryId">
                                <option value="">Select Country</option>
                                @foreach ($country_data as $country)
                                 <option value="{{$country->id}}">{{$country->country_name}}</option>
                               @endforeach
                                </select>
                              </div>								
                              <div class="col-md-12 col-lg-4">
                                <label>State:</label>
                                <select id="stateId"  name="stateId" class="form-control">
                                <option value="">Select State</option>
                                </select>
                              </div>
                              <div class="col-md-12 col-lg-4">
                                <label>City*</label>
                                <select id="cityId" name="cityId" class="form-control required">
                                <option value="">Select City</option>
                                </select>
                              </div>
                            </div>
                            <div class="form-group row mx-auto">
                            <div class="col-md-12 col-lg-4">
                                <label>Street*</label><input id="street_1" class="form-control form-control-user required" type="text"  name="street_1" placeholder="Enter Street 1" value="">
                              </div>
                            <div class="col-md-12 col-lg-4">
                                <label>Street line 2*</label><input id="street_2" class="form-control form-control-user required" type="text"  name="street_2" placeholder="Enter Street line 2" value="">
                              </div>
                              <div class="col-md-12 col-lg-4">
                                <label>Postal Code*</label><input id="postal_code" class="form-control form-control-user required" type="text"  name="postal_code" placeholder="Enter Postal Code" value="">
                              </div>
                            </div>
                            <div class="form-group row mx-auto">
                              <div class="col-md-12 col-lg-4">
                                <label>Whatsapp*</label><input id="whatsapp" class="form-control form-control-user required" type="number" min="0"  name="whatsapp" placeholder="Enter Whatsapp No" value="">
                              </div>
                              <div class="col-md-12 col-lg-4">
                                <label>Mobile*</label><input id="mobile" class="form-control form-control-user required" type="number" min="0"  name="mobile" placeholder="Enter Mobile No" value="">
                              </div>
                              <div class="col-md-12 col-lg-4">
                                <label>Fax*</label><input id="fax" class="form-control form-control-user required" type="number" min="0"  name="fax" placeholder="Enter Fax" value="">  
                              </div>
                            </div>
                            <div class="form-group row mx-auto">
                              <div class="col-md-12 col-lg-4">
                                <label>Office Number*</label><input id="office" class="form-control form-control-user required" type="number" min="0"  name="office" placeholder="Enter Office No" value="">
                              </div>
                              <div class="col-md-12 col-lg-4">
                                <label>Email*</label><input id="email" class="required" type="email" name="email" placeholder="Enter Email Address" value="">
                              </div>
                              <div class="col-md-12 col-lg-4">
                                <strong>Password*</strong>
                        		<input type="password" class= "form-control required" placeholder="Password" id="password" name="password">  
                              </div>  
                            </div>
                            <div class="form-group row mx-auto">
                              <div class="col-md-12 col-lg-4">
                                <strong>Confirm Password*</strong>
                      			<input class="form-control required" type="password" placeholder="Confirm Password" id="confirm_password" name="confirmpassword">
                              </div>
                              
                              <div class="col-md-12 col-lg-4">
                          <label class="form-control-label" for="timezone">Timezone</label>
                          <select id="timezone" class="form-control" required name="timezone">
                            <option value="">Select Timezone</option>
                                @foreach ($timezone as $zone)
                                 <option value="{{$zone->id}}">{{$zone->name .' '. $zone->offset}}</option>
                               @endforeach
                          </select>
                        </div>
                              <div class="col-md-12 col-lg-4">
                      <label>Status*</label>
                      <select id ="status" name="status" class="form-control required" required>
                       <option value="">Select Status</option>
                        <option value="1" >Active</option>
                        <option value="0" >Inactive</option>
                      </select>
                    </div>
                            </div>
                            </section>
                            <h3>Property Images</h3>
                            <section>
                              <div class="form-group row mx-auto">
                                <div class="input-group control-group increment">
                                  <input type="file" name="photos[]" class="form-control required">
                                </div>
                                <div class="clone hide" hidden>
                                  <div class="control-group input-group"
                                    style="margin-top: 10px">
                                    <input type="file" name="photos[]" class="form-control">
                                    <div class="input-group-btn">
                                    <button class="btn btn-primary  btn-danger" type="button">
                                        <i class="glyphicon glyphicon-remove"></i> Remove
                                      </button>
                                      </div>
                                  </div>
                                </div>
                                <div class="input-group-btn">
                                    <button class="btn btn-primary  btn-success" type="button">
                                      <i class="glyphicon glyphicon-plus"></i>Add
                                    </button>
                                </div>
                              </div>
                             
                              </br>
                            
                            </section>
                              
                          </div>
                        </form>
                      
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script> -->
<script type="text/javascript" src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.7/jquery.validate.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-steps/1.1.0/jquery.steps.min.js"></script>

<script type="text/javascript">

  $(document).ready(function(){

var form = $("#contact");

form.validate({
errorPlacement: function errorPlacement(error, element) { element.before(error); },

});

form.children("div").steps({
headerTag: "h3",
bodyTag: "section",
transitionEffect: "slideLeft",
autoFocus: true,
/*saveState: true,*/

onStepChanging: function (event, currentIndex, newIndex)
{
if(currentIndex == 4){
if($('#mrp_of_product').val() >= $('#cost_of_product').val()){
form.validate().settings.ignore = ":disabled,:hidden";
return form.valid();
}else{
return alert('MRP of cost should be less than or equal to cost of product');
}
}
form.validate().settings.ignore = ":disabled,:hidden";
return form.valid();

},
onFinishing: function (event, currentIndex)
{

form.validate().settings.ignore = ":disabled";
return form.valid();
},
onFinished: function (event, currentIndex)
{

/*alert('are you sure you want to save venue.!!');*/
$('#contact').submit();
}

});

  });

      $(document).ready(function() {

        $(".btn-success").click(function(){ 
            var html = $(".clone").html();
            $(".increment").after(html);
        });

        $("body").on("click",".btn-danger",function(){ 
            $(this).parents(".control-group").remove();
        });


      

      $("#countryId").change(function(){
    	  var countryId = $(this).val();
    	  $('#loading-image').show();
    	  $.ajax({
    	    url: "/admin/admincommon/getState",
    	    type: "POST",
    	    data: {countryId: countryId, _token: '{{csrf_token()}}' },
    	    dataType: 'json',
    	    success : function(data){
    	      var html = `<option value="">Select State</option>`;
    	      for (var i = 0; i < data.length; i++) {
    	          var id = data[i].id;
    	          var name = data[i].state_name;
    	          var option = `<option value="${data[i].id}">${data[i].state_name}</option>`;
    	          html += option;
    	        }
    	      $('#loading-image').hide();
    	        $('#stateId').html(html); 
    	      }
    	    });
    	  $('#loading-image').show();
    	  $.ajax({
    	  	url: "/admin/admincommon/getCity",
    	  	type: "POST",
    	  	data: {countryId: countryId  ,_token: '{{csrf_token()}}' },
    	  	dataType: 'json',
    	  	success : function(data){
    	  		var html = `<option value="">Select City</option>`;
    	  		for (var i = 0; i < data.length; i++) {
    	  			  var id = data[i].id;
    	  			  var name = data[i].city_name;
    	  			  var option = `<option value="${data[i].id}">${data[i].city_name}</option>`;
    	  			  html += option;
    	  			}
    	  		$('#loading-image').hide();
    	  			$('#cityId').html(html);	
    	  		}
    	  	});
    	  });
    	  $("#stateId").change(function(){
    	  var stateId = $(this).val();
    	  $('#loading-image').show();
    	  $.ajax({
    	    url: "/admin/admincommon/getCity",
    	    type: "POST",
    	    data: {stateId: stateId, _token: '{{csrf_token()}}' },
    	    dataType: 'json',
    	    success : function(data){
    	      var html = `<option value="">Select City</option>`;
    	      for (var i = 0; i < data.length; i++) {
    	          var id = data[i].id;
    	          var name = data[i].city_name;
    	          var option = `<option value="${data[i].id}">${data[i].city_name}</option>`;
    	          html += option;
    	        }
    	      $('#loading-image').hide();
    	        $('#cityId').html(html);  
    	      }
    	    });
    	  });
      });
</script>

  
  @stop
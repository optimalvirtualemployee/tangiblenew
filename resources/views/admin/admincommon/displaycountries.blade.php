@extends('admin.common.innercommondefault')
@section('title', 'Display Countries')
@section('content')

  <!-- Page Wrapper -->
  <div id="wrapper">

   
  
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

       
        <!-- End of Topbar -->

        <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          <h1 class="h3 mb-2 text-gray-800">Countries</h1>
         @can('superAdmin-create')
         <a href="{{route('createcountries')}}" class="btn btn-primary btn-icon-split">
            <span class="text">Add New Country</span>
          </a>
          @endcan
                  @if(Session::has('success_msg'))
                  <div class="alert " id="writeMsg" style="background-color: #4e73df;margin-top: 20px;color: white;">{{ Session::get('success_msg') }}</div>
                  @endif
                  <div id="main">
                  </div>
          <div class="card shadow mb-4" style="margin-top: 10px;">
           
            <div class="card-body" >
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th>Country Name</th>
                      <th>Flag</th>
                      <th>Country Code</th>
					  <th>Phone Code</th>	
                      <th>Status</th>
                      @can('superAdmin-create')
                      <th>Action</th>
                      @endcan
                    </tr>
                  </thead>
                  
                  <tbody>
                    @foreach ($countries as $country)
                      <tr>
                        <td>  {{$country->country_name}}</td>
                        <td>
                        @if($country->filename != '')
						<img src="{{url('uploads/'.$country->filename)}}"  class="img-thumbnail" width="75" />
						@else
						<img src="{{url('uploads/NoFlag.png')}}"  class="img-thumbnail" width="75" />
						@endif
						</td>
						<td>  {{$country->countrycode}}</td>
						<td>  {{$country->phonecode}}</td>
                        <td>@if($country->status =='1') Active  @else Inactive @endif  </td>
                        @can('superAdmin-create')
                          <td>
                            <a href="{{url('/admin/admincommon/updatecountries/'.$country->id)}}">
                              <i class="fas fa-edit"></i>
                            </a> &nbsp; &nbsp;
                            <a href="#" class="deletecountries" id="del_{{$country->id}}">
                              <i class="fas fa-trash-alt"></i>
                            </a>
                          </td>
                        @endcan  
                      </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
        <!-- /.container-fluid -->
      </div>
      <!-- End of Main Content -->
     

<script type="text/javascript">
   $(document).on("click", ".deletecountries", function() { 
      var url = "{{ route('deletecountries') }}";
      var id = this.id;
      var splitid = id.split("_");
      var deleteid = splitid[1];
     if ( confirm("Do you really want to delete record?")) {
        
        $.ajax({
          url: url,
           type: 'POST',
          cache: false,
          data:{
             id:deleteid,
            _token:'{{ csrf_token() }}'
          },
          success: function(data){ 

              $('#main').html('<div class="alert " id="writeMsg" style="background-color: #4e73df;margin-top: 20px;color: white;">' + data.success_msg + '</div>')
               location.reload();
           
          }
        });
      }
      
      });

    $("document").ready(function(){
        setTimeout(function(){
            $("div.alert").remove();
        }, 3000 ); 
    });
</script>
     @stop


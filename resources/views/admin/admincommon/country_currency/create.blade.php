@extends('admin.common.innercommondefault')
@section('title', 'Create Currency')
@section('content')
<style>
.error {
      color: red;
   }   
</style>

<div class="container">
  <!-- Outer Row -->
  <div class="row justify-content-center">
    <div class="col-xl-10 col-lg-12 col-md-9">
      <div class="card o-hidden border-0 shadow-lg my-5">
        <div class="card-body p-0">
          <!-- Nested Row within Card Body -->
          <div class="row">
            <div class="col-lg-8">
              <div class="p-5">
                <div class="text-center">
                   <h1 class="h4 text-gray-900 mb-4">Create Currency Based On Country</h1>
                </div>
                @if($errors->any())
                  @foreach($errors->all() as $error)
                    <div class="alert alert-danger">
                      {{$error}}
                    </div>
                  @endforeach
                @endif
                <form class="user" id="createcurrency" method="POST" action="{{ route('countrycurrency.store') }}">
                  @csrf
                  <div class="form-group">
                    <label>Country:</label><select id="country" class="form-control"  name="country" required>
						<option value="">Select Country</option>
						@foreach ($countryList as $country)
                           <option value="{{$country->id}}">{{$country->country_name}}</option>
                         @endforeach
                      </select>
                   </div>
                   <div class="form-group">
                    <label>Currency:</label><select id="currency" class="form-control"  name="currency" required>
						<option value="">Select Currency</option>
						@foreach ($currencyList as $currency)
                           <option value="{{$currency->id}}">{{$currency->currency_code.'-'.$currency->currency}}</option>
                         @endforeach
                      </select>
                  </div>
                  <div class="form-group">
                      <label>Status</label>
                      <select id ="status" name="status" class="form-control" required>
                       <option value="">Select Status</option>
                        <option value="1" >Active</option>
                        <option value="0" >Inactive</option>
                      </select>
                    </div>
                  <button type="submit" class="btn btn-primary btn-user btn-block">Add</button>
                </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>


<!------------------------------------------ JS SCRIPT SECTION ------------------------------------------------>

<script type="text/javascript">
    $(document).ready(function(){
      $("#createcurrency").validate({
    
        rules: {
        	currencyCode: "required",
        	currency: "required",
        	status: "required"
        },
    
        messages: {
        	currencyCode: "Currency Code can\'t be left blank",
        	currency: "Currency can\'t be left blank"
        },
    
        submitHandler: function(form) {
          form.submit();
        }
      });
    });
</script>

<!------------------------------------------ JS SCRIPT SECTION ------------------------------------------------>
@stop

@extends('admin.common.innercommondefault')
@section('title', 'Update Pricing Range')
@section('content')

<style>

.column {
  float: left;
  width: 30%;
  padding: 10px;
  height: 100px; /* Should be removed. Only for demonstration */
}

</style>

<div class="container">
  <!-- Outer Row -->
  <div class="row justify-content-center">
    <div class="col-xl-10 col-lg-12 col-md-9">
      <div class="card o-hidden border-0 shadow-lg my-5">
        <div class="card-body p-0">
          <!-- Nested Row within Card Body -->
          <div class="row">
            <div class="col-lg-8">
              <div class="p-5">
                <div class="text-center">
                   <h1 class="h4 text-gray-900 mb-4">Update Pricing Range</h1>
                </div>
                @if($errors->any())
                  @foreach($errors->all() as $error)
                    <div class="alert alert-danger">
                      {{$error}}
                    </div>
                  @endforeach
                @endif
                <form class="user" id="updatepricingrange" method="POST" action="{{ route('packagepricingrange.update',$packagesPricing->id) }}">
                  @method('PATCH')
                  @csrf
                  
                  <div class="form-group">
                 <div class="form-group">
                      <label>Select Agency</label>
                      <select id ="package" name="package" class="form-control" required>
                        <option value="{{$package_data->id}}">{{$package_data->package_for_private_sellers}}</option>
                         @foreach ($packages as $data)
                           <option value="{{$data->id}}">{{$data->package_for_private_sellers}}</option>
                         @endforeach
                      </select>
                    </div>
                    
                  <div class="row increment">
                 <div class="column"><input id="from" class="form-control form-control-user " type="number"  name="from" placeholder="Enter From Range" value="{{$packagesPricing->rangeFrom}}"></div>
                 <div class="column"><input id="to" class="form-control form-control-user " type="number"  name="to" placeholder="Enter To Range" value="{{$packagesPricing->rangeTo}}"></div>
                 <div class="column"><input id="pricing" class="form-control form-control-user " type="number"  name="pricing" placeholder="Enter Pricing" value="{{$packagesPricing->pricing}}"></div>
                  </div>
                 </div>
                  <button type="submit" class="btn btn-primary btn-user btn-block">Add</button>
                </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>


<!------------------------------------------ JS SCRIPT SECTION ------------------------------------------------>

<script type="text/javascript">
    $(document).ready(function(){
    	$("#updateCurrencyData").validate({
    	    
    		rules: {
            	first_name: "required",
            	last_name: "required",
            	whatsapp: {required: true, number:true},
            	mobile: {required: true, number:true},
            	fax: {required: true, number:true},
            	office: {required: true, number:true},
            	email: {required: true},
            	address: "required"
            },
        
            submitHandler: function(form) {
              form.submit();
            }
          });
        });
</script>

<!------------------------------------------ JS SCRIPT SECTION ------------------------------------------------>
@stop

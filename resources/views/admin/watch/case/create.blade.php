@extends('admin.common.innerdefault')
@section('title', 'Create Case')
@section('content')

<div class="container">
    <!-- Outer Row -->
    <div class="row justify-content-center">
      <div class="col-xl-10 col-lg-12 col-md-9">
        <div class="card o-hidden border-0 shadow-lg my-5">
          <div class="card-body p-0">
            <!-- Nested Row within Card Body -->
            <div class="row">
              <div class="col-lg-8">
                <div class="p-5">
                  <div class="text-center">
                    <h1 class="h4 text-gray-900 mb-4">Create New Case</h1>
                  </div>
                  @if($errors->any())
                    @foreach($errors->all() as $error)
                      <div class="alert alert-danger">
                    {{$error}}
                      </div>
                    @endforeach
                  @endif
                  <form class="user" id="create_case" method="POST" action="{{ route('case.store') }}">
                  @csrf
                    <div class="form-group">
                    
                      <input id="case_matrial" class="form-control form-control-user " type="text"  name="case_matrial" placeholder="Enter Case Material" maxlength="100" value="{{ old('case_matrial') }}"><br>

                      <input id="case_mm" class="form-control form-control-user" type="text"  maxlength="100"  name="case_mm" placeholder="Enter Case MM" value="{{ old('case_mm') }}"><br>
                      
                      <input id="water_resistant_depth" class="form-control form-control-user" type="text" maxlength="100"  name="water_resistant_depth" placeholder="Enter Water Resistant Type" value="{{ old('water_resistant_depth') }}"><br>
                      
                      <input id="glass_type" class="form-control form-control-user" type="text" maxlength="100"  name="glass_type" placeholder="Enter Glass Type" value="{{ old('glass_type') }}">
                    
                    </div>
                      <button type="submit" class="btn btn-primary btn-user btn-block">Add New</button>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>


<!------------------------------------------------ JS SCRIPT SECTION --------------------------------------->
  
<script type="text/javascript">
    $(document).ready(function(){
     
      $("#create_case").validate({
        
        rules: {
         
          case_matrial: "required",
          case_mm: {required: true,number:true},
          water_resistant_depth: "required",
          glass_type: "required"
        
          },
        
        messages: {
          case_matrial: "Case Material can\'t be left blank",
          case_mm: {required: "Case MM can\'t be left blank",number:"Please enter numbers Only"},
          water_resistant_depth: "Water Resistant Depth can\'t be left blank",
          glass_type: "Glass Type cant\'t be left blank"
        },
        
        submitHandler: function(form) {
          form.submit();
        }
      });
    });
</script>
<!------------------------------------------------ JS SCRIPT SECTION --------------------------------------->
@stop

@extends('admin.common.innerdefault')
@section('title', 'Create Function')
@section('content')

<div class="container">
    <!-- Outer Row -->
    <div class="row justify-content-center">
      <div class="col-xl-10 col-lg-12 col-md-9">
        <div class="card o-hidden border-0 shadow-lg my-5">
          <div class="card-body p-0">
            <!-- Nested Row within Card Body -->
            <div class="row">
              <div class="col-lg-8">
                <div class="p-5">
                  <div class="text-center">
                    <h1 class="h4 text-gray-900 mb-4">Create New Function</h1>
                  </div>
                  @if($errors->any())
                    @foreach($errors->all() as $error)
                      <div class="alert alert-danger">
                    {{$error}}
                      </div>
                    @endforeach
                  @endif
                  <form class="user" id="create_Function" method="POST" action="{{ route('functions.store') }}">
                  @csrf
                    <div class="form-group">
                    
                      <input id="chronograph" class="form-control form-control-user " type="text"  name="chronograph" placeholder="Enter Chronograph" maxlength="100" value="{{ old('chronograph') }}"><br>

                      <input id="tourbillion" class="form-control form-control-user" type="text" maxlength="100"  name="tourbillion" placeholder="Enter Tourbillion" value="{{ old('tourbillion') }}"><br>
                      
                      <input id="GMT" class="form-control form-control-user" type="text" maxlength="100"  name="GMT" placeholder="Enter GMT" value="{{ old('GMT') }}"><br>
                      
                      <div class="row">
                        <div class="col-lg-6">
                          <select id="annual_calendar" class="form-control" name="annual_calendar">
                            <option value="">Annual Calender</option>
                            <option value="1" {{ old('annual_calendar') == 1 && old('annual_calendar') != "" ? 'selected' : ''}}>Yes</option>
                            <option value="0" {{ old('annual_calendar') == 0 && old('annual_calendar') != "" ? 'selected' : ''}}>No</option>
                          </select>
                        </div>
                        <div class="col-lg-6">
                          <select id="minute_repeater" class="form-control" name="minute_repeater">
                            <option value="">Minute Repeater</option>
                            <option value="1" {{ old('minute_repeater') == 1 && old('minute_repeater') != "" ? 'selected' : ''}}>Yes</option>
                            <option value="0" {{ old('minute_repeater') == 0 && old('minute_repeater') != "" ? 'selected' : ''}}>No</option>
                          </select>
                        </div>
                      </div>
                      <br>

                      <input id="double_chronograph" class="form-control form-control-user" type="text" maxlength="100"  name="double_chronograph" placeholder="Enter double chronograph" value="{{ old('double_chronograph') }}"><br>

                      <input id="panorma_date" class="form-control" type="date" maxlength="100"  name="panorma_date" placeholder="Enter double chronograph" value="{{ old('panorma_date') }}"><br>

                      <input id="jumping_hour" class="form-control form-control-user" type="number" min="0" step="any" maxlength="100"  name="jumping_hour" placeholder="Enter Jumping hour" value="{{ old('jumping_hour') }}"><br>

                      <select id="alarm" class="form-control" name="alarm">
                        <option value="">Alarm</option>
                        <option value="1" {{ old('alarm') == 1 && old('alarm') != "" ? 'selected' : ''}}>Yes</option>
                        <option value="0" {{ old('alarm') == 0 && old('alarm') != "" ? 'selected' : ''}}>No</option>
                      </select><br>

                      <div class="row">
                        <div class="col-lg-6">
                          <input id="year" class="form-control form-control-user" type="number" min="0" step="1" maxlength="100"  name="year" placeholder="Enter Year" value="{{ old('year') }}">
                        </div>

                        <div class="col-lg-6">
                          <input id="day" class="form-control form-control-user" type="number" min="0" step="1" maxlength="100"  name="day" placeholder="Enter Day" value="{{ old('day') }}">
                        </div>
                      </div>
                    
                    </div>
                      <button type="submit" class="btn btn-primary btn-user btn-block">Add New</button>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>


<!------------------------------------------------ JS SCRIPT SECTION --------------------------------------->
  
<script type="text/javascript">
    $(document).ready(function(){
     
      $("#create_Function").validate({
        
        rules: {
         
          chronograph: "required",
          tourbillion: "required",
          GMT: "required",
          annual_calendar: "required",
          minute_repeater: "required",
          double_chronograph: "required",
          panorma_date: "required",
          jumping_hour: "required",
          alarm: "required",
          year: "required",
          day: "required"
        
          },
        
        messages: {
          chronograph: "Chronograph can\'t be left blank",
          tourbillion: "Tourbillion can\'t be left blank",
          GMT: "GMT can\'t be left blank",
          annual_calendar: "Annual Calender can\'t be left blank",
          minute_repeater: "Minute Repeater can\'t be left blank",
          double_chronograph: "double Chronograph cant\'t be left blank",
          panorma_date: "Panorama Date can\'t be left blank",
          jumping_hour: "Jumping hour can\'t be left blank",
          alarm: "Alarm can\'t be left blank",
          year: "Year can\'t be left blank",
          day: "Day can\'t be left blank"
        },
        
        submitHandler: function(form) {
          form.submit();
        }
      });
    });
</script>
<!------------------------------------------------ JS SCRIPT SECTION --------------------------------------->
@stop

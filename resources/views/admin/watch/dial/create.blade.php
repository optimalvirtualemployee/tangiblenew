@extends('admin.common.innerdefault')
@section('title', 'Create Dial')
@section('content')

<div class="container">
    <!-- Outer Row -->
    <div class="row justify-content-center">
      <div class="col-xl-10 col-lg-12 col-md-9">
        <div class="card o-hidden border-0 shadow-lg my-5">
          <div class="card-body p-0">
            <!-- Nested Row within Card Body -->
            <div class="row">
              <div class="col-lg-8">
                <div class="p-5">
                  <div class="text-center">
                    <h1 class="h4 text-gray-900 mb-4">Create New Dial</h1>
                  </div>
                  @if($errors->any())
                    @foreach($errors->all() as $error)
                      <div class="alert alert-danger">
                    {{$error}}
                      </div>
                    @endforeach
                  @endif
                  <form class="user" id="createdial" method="POST" action="{{ route('dial.store') }}">
                  @csrf
                    <div class="form-group">
                      <input id="pr_name" class="form-control form-control-user " type="text"  name="power_reserve" placeholder="Enter Power Reserve" maxlength="100" value="{{ old('power_reserve') }}"><br>
                      <input id="dc_name" class="form-control form-control-user" type="text" maxlength="100"  name="dial_color" placeholder="Enter Dial Colour" value="{{ old('dial_color') }}">
                    </div>
                      <button type="submit" class="btn btn-primary btn-user btn-block">Add New</button>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>


<!------------------------------------------------ JS SCRIPT SECTION --------------------------------------->
  
<script type="text/javascript">
$(document).ready(function(){
 
  $("#createdial").validate({
    
    rules: {
     
      power_reserve: "required",
      dial_color: "required",
    
      },
    
    messages: {
      power_reserve: "Power Reserve cant be left blank",
      dial_color: "Dial Colour cant be left blank",
      
    },
    
    submitHandler: function(form) {
      form.submit();
    }
  });
});
</script>
<!------------------------------------------------ JS SCRIPT SECTION --------------------------------------->
@stop

@extends('admin.common.innerdefault')
@section('title', 'Update Year Of Manufacture')
@section('content')
<style>
.error {
      color: red;
   }
</style>
<div class="container">
  <!-- Outer Row -->
  <div class="row justify-content-center">
    <div class="col-xl-10 col-lg-12 col-md-9">
      <div class="card o-hidden border-0 shadow-lg my-5">
        <div class="card-body p-0">
          <!-- Nested Row within Card Body -->
          <div class="row">
            <div class="col-lg-8">
              <div class="p-5">
                <div class="text-center">
                   <h1 class="h4 text-gray-900 mb-4">Update Manufacturing Year</h1>
                </div>
                @if($errors->any())
                  @foreach($errors->all() as $error)
                    <div class="alert alert-danger">
                      {{$error}}
                    </div>
                  @endforeach
                @endif
                <form class="user" id="updateyear_of_manufacture" method="POST" action="{{ route('year_of_manufacture.update',$year_of_manufacture_data->id) }}">
                  @method('PATCH')
                  @csrf
                  <div class="form-group">
                    <input id="year_of_manufacture" class="form-control form-control-user " type="text"  name="year_of_manufacture" placeholder="Enter Year of Manufacture" maxlength="4" value="{{ $year_of_manufacture_data->year_of_manufacture }}">
                  </div>
                  <div class="form-group">
                    <label>Status</label>
                    <select id ="status" name="status" class="form-control" required>
                      <option value="1" {{$year_of_manufacture_data->status == "1" ? 'selected' : ''}}>Active</option>
                      <option value="0" {{$year_of_manufacture_data->status == "0" ? 'selected' : ''}}>Inactive</option>
                    </select>
                  </div> 
                  <button type="submit" class="btn btn-primary btn-user btn-block">Update</button>
                </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>


<!------------------------------------------ JS SCRIPT SECTION ------------------------------------------------>

<script type="text/javascript">
    $(document).ready(function(){
      $("#updateyear_of_manufacture").validate({
    
        rules: {
          year_of_manufacture: {required:true,number:true}
        },
    
        messages: {
          year_of_manufacture: {required: "Year of Manufacture can\'t be left blank", number:"Please enter numbers only"}
        },
    
        submitHandler: function(form) {
          form.submit();
        }
      });
    });
</script>

<!------------------------------------------ JS SCRIPT SECTION ------------------------------------------------>
@stop

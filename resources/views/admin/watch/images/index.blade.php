@extends('admin.common.innerdefault')
@section('title', 'Display Image')
@section('content')

  <!-- Page Wrapper -->
  <div id="wrapper">
    <div id="content-wrapper" class="d-flex flex-column">
      <!-- Main Content -->
      <div id="content">
        <!-- End of Topbar -->
        <!-- Begin Page Content -->
        <div class="container-fluid">
          <!-- Page Heading -->
          <h1 class="h3 mb-2 text-gray-800">Watch Image</h1>
          <a href="{{url('admin/uploadwatchimage/createImage',$listing_id)}}" class="btn btn-primary btn-icon-split">
            <span class="text">Add New Image</span>
          </a>
          @if(Session::has('success_msg'))
            <div class="alert " id="writeMsg" style="background-color: #4e73df;margin-top: 20px;color: white;">{{ Session::get('success_msg') }}</div>
          @endif
          <div id="main"></div>
          <div class="card shadow mb-4" style="margin-top: 10px;">
           <div class="card-body" >
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th>Image</th>
                      <th>Id</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  
                  <tbody>
                    @foreach ($details as $data)
                      <tr>
                        <td>
						<img src="{{url('uploads/'.$data->filename)}}"  class="img-thumbnail" width="75" />
						</td>
                        <td>{{$data->listing_id}}</td>
                        <td>
                          <form action="{{ route('uploadwatchimage.destroy', $data->id) }}" method="POST" id="delete-form-{{ $data->id }}" style="display: none;">
                            {{csrf_field()}}
                            {{ method_field('DELETE') }}
                            <input type="hidden" value="{{ $data->id }}" name="id">
                          </form>
                          <a href="" onclick="if (!confirm('Are you sure you want to delete?')) return; 
                            event.preventDefault(); document.getElementById('delete-form-{{ $data->id }}').submit();">
                            <i class="fas fa-trash-alt"></i>
                          </a>
                      </td>
                      </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
        <!-- /.container-fluid -->
      </div>
      <!-- End of Main Content -->
     

<!----------------------------------------------- JS SCRIPT SECTION -------------------------------------------------->

<script type="text/javascript">
    $(document).ready(function(){
      setTimeout(function(){
          $("div.alert").remove();
      }, 3000 ); 
    });
</script>
<!----------------------------------------------- JS SCRIPT SECTION -------------------------------------------------->
@stop
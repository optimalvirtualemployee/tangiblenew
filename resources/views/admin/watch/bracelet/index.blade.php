@extends('admin.common.innerdefault')
@section('title', 'Bracelet List')
@section('content')

  <!-- Page Wrapper -->
  <div id="wrapper">
    <div id="content-wrapper" class="d-flex flex-column">
      <!-- Main Content -->
      <div id="content">
        <!-- End of Topbar -->
        <!-- Begin Page Content -->
        <div class="container-fluid">
        <!-- Page Heading -->
        <h1 class="h3 mb-2 text-gray-800">Bracelet</h1>
        <a href="{{route('bracelet.create')}}" class="btn btn-primary btn-icon-split">
          <span class="text">Add New Bracelet</span>
        </a>
        @if(Session::has('success_msg'))
          <div class="alert " id="writeMsg" style="background-color: #4e73df;margin-top: 20px;color: white;">{{ Session::get('success_msg') }}</div>
            @endif
            <div id="main"></div>
            <div class="card shadow mb-4" style="margin-top: 10px;">
            <div class="card-body" >
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th>Bracelet Material</th>
                      <th>Bracelet Colour</th>
                      <th>Clasp Type</th>
                      <th>Clasp Material</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  
                  <tbody>
                    @foreach ($bracelet as $data)
                      <tr>
                        <td>{{ $data->bracelet_material }}</td>
                        <td>{{ $data->bracelet_color }}</td>
                        <td>{{ $data->type_of_clasp }}</td>
                        <td>{{ $data->clasp_material }}</td>
                        <td>
                          <a href="{{route('bracelet.edit', $data->id)}}"><i class="fas fa-edit"></i></a> &nbsp; &nbsp;
                           <form action="{{ route('bracelet.destroy', $data->id) }}" method="POST" id="delete-form-{{ $data->id }}" style="display: none;">
                                {{csrf_field()}}
                                {{ method_field('DELETE') }}
                                <input type="hidden" value="{{ $data->id }}" name="id">
                           </form>
                           <a href="" onclick="if (!confirm('Are you sure you want to delete?')) return; 
                            event.preventDefault(); document.getElementById('delete-form-{{ $data->id }}').submit();">
                            <i class="fas fa-trash-alt"></i></a>
                        </td>
                      </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
        <!-- /.container-fluid -->
      </div>
      <!-- End of Main Content -->

      <!-------------------------- JS SCRIPT SECTION --------------------------------------->
      <script type="text/javascript">
          $(document).ready(function(){
            setTimeout(function(){
                $("div.alert").remove();
            }, 3000 ); 
          });
      </script>
      <!--------------------------- JS SCRIPT SECTION ------------------------------------->
@stop


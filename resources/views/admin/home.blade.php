@extends('admin.common.default')
@section('content')
<!-- Page Wrapper -->
  <div id="wrapper">


    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

        <!-- Topbar -->
        <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">
          <marquee><h3>Welcome To Admin of Tangible Listing</h3></marquee>

          <!-- Sidebar Toggle (Topbar) -->
          
         
          <!-- Topbar Navbar -->
          <ul class="navbar-nav ml-auto">

            <!-- Nav Item - Search Dropdown (Visible Only XS) -->
            <li class="nav-item dropdown no-arrow d-sm-none">
              <a class="nav-link dropdown-toggle" href="#" id="searchDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fas fa-search fa-fw"></i>
              </a>
              <!-- Dropdown - Messages -->
              <div class="dropdown-menu dropdown-menu-right p-3 shadow animated--grow-in" aria-labelledby="searchDropdown">
                <form class="form-inline mr-auto w-100 navbar-search">
                  <div class="input-group">
                    <input type="text" class="form-control bg-light border-0 small" placeholder="Search for..." aria-label="Search" aria-describedby="basic-addon2">
                    <div class="input-group-append">
                      <button class="btn btn-primary" type="button">
                        <i class="fas fa-search fa-sm"></i>
                      </button>
                    </div>
                  </div>
                </form>
              </div>
            </li>

          

           

            <div class="topbar-divider d-none d-sm-block"></div>

            <!-- Nav Item - User Information -->
            <li class="nav-item dropdown no-arrow">
              <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <span class="mr-2 d-none d-lg-inline text-gray-600 small">Welcome {{ Auth::user()->first_name . ' '. Auth::user()->last_name  }}</span>
                
              </a>
              <!-- Dropdown - User Information -->
              <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
                <a class="dropdown-item" href="{{url('admin/profile',Auth::user()->id)}}">
                  <i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>
                  Profile
                </a>
                @canany(['superAdmin-create', 'userManagement-create'])
                <a class="dropdown-item" href="{{ url('admin/contactus') }}">
                  <i class="fas fa-cogs fa-sm fa-fw mr-2 text-gray-400"></i>
                  Contact Us
                </a>
                @endcan
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="#" data-toggle="modal" data-target="#logoutModal">
                  
                 <a class="dropdown-item" href="{{ route('admin.logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>
                </a>
                <form id="logout-form" action="{{ route('admin.logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
              </div>
            </li>

          </ul>

        </nav>
        <!-- End of Topbar -->

        <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          <div class="d-sm-flex align-items-center justify-content-between mb-4">
            
           
          </div>

          <!-- Content Row -->
          <div class="row">

            <!-- Earnings (Monthly) Card Example -->
             @can('watch-list')
              <div class="col-xl-3 col-md-6 mb-4">
                <div class="card border-left-primary shadow h-100 py-2">
                  <div class="card-body">
                    <div class="row no-gutters align-items-center">
                      <div class="col mr-2">
                      <a href="{{url('/admin/watch/dashboard')}}">  <img src="{{url('/assets/img/watchnew.jpg')}}" height="50" width="50"/></a>
                        <div class="h5 mb-0 font-weight-bold text-gray-800"></div>
                      </div>
                      <div class="col-auto">
                        
                        <div class="text-xs font-weight-bold text-primary text-uppercase mb-1"><a href="{{url('/admin/watch/dashboard')}}">Watch Category</a></div>
                      </div>

                    </div>
              You can click on above category and enter to dashboard page of Watches,where you can check all details of watches
                  </div>
                </div>
              </div>
            @endcan

            <!-- Earnings (Monthly) Card Example -->
          @can('automobile-list')
            <div class="col-xl-3 col-md-6 mb-4">
              <div class="card border-left-success shadow h-100 py-2">
                <div class="card-body">
                  <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                     
                     <a href="{{url('/admin/automobile/dashboard')}}">  <img src="{{url('/assets/img/car.jpeg')}}" height="50" width="50"/></a>
                      <div class="h5 mb-0 font-weight-bold text-gray-800"></div>
                    </div>
                    <div class="col-auto">
                      <div class="text-xs font-weight-bold text-success text-uppercase mb-1"><a href="{{url('/admin/automobile/dashboard')}}">AutoMobile Category</a></div>
                    </div>
                  </div>
                   You can click on above category and enter to dashboard page of Automobiles,where you can check all details of automobiles
                </div>
              </div>
            </div>
           @endcan

           @can('realEstate-list')
            <!-- Earnings (Monthly) Card Example -->
            <div class="col-xl-3 col-md-6 mb-4">
              <div class="card border-left-info shadow h-100 py-2">
                <div class="card-body">
                  <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                      
                    <a href="{{url('/admin/realestate/dashboard')}}">  <img src="{{url('/assets/img/realestate.jpeg')}}" height="50" width="50"/></a>
                    </div>
                    <div class="col-auto">
                     <div class="text-xs font-weight-bold text-info text-uppercase mb-1"><a href="{{url('/admin/realestate/dashboard')}}">Real Estate Category</a></div>
                    </div>
                  </div>
                   You can click on above category and enter to dashboard page of properties,where you can check all details of properties
                </div>
              </div>
            </div>
            @endcan

            <!-- Earnings (Monthly) Card Example -->
            <div class="col-xl-3 col-md-6 mb-4">
              <div class="card border-left-info shadow h-100 py-2">
                <div class="card-body">
                  <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                      
                    <a href="{{url('/admin/commonadmin/dashboard')}}"></a>
                    </div>
                    <div class="col-auto">
                     <div class="text-xs font-weight-bold text-info text-uppercase mb-1"><a href="{{url('/admin/admincommon/dashboard')}}">Super Admin Common </a></div>
                    </div>
                  </div>
                   This is common dashboard where data of all categories which are common can be handled
                </div>
              </div>
            </div>

            <!-- Pending Requests Card Example -->
            
          </div>

          <!-- Content Row -->


          <!-- Content Row -->
          <div class="row">

      

            
          </div>

        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

      <!-- Footer -->
      
      <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  

  

  
@stop
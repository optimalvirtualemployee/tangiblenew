@extends('admin.common.innerdefaultreal')
@section('title', 'Update Top Homes')
@section('content')

<div class="container">
  <!-- Outer Row -->
  <div class="row justify-content-center">
    <div class="col-xl-10 col-lg-12 col-md-9">
      <div class="card o-hidden border-0 shadow-lg my-5">
        <div class="card-body p-0">
          <!-- Nested Row within Card Body -->
          <div class="row">
            <div class="col-lg-8">
              <div class="p-5">
                <div class="text-center">
                   <h1 class="h4 text-gray-900 mb-4">Update Top Homes</h1>
                </div>
                @if($errors->any())
                  @foreach($errors->all() as $error)
                    <div class="alert alert-danger">
                      {{$error}}
                    </div>
                  @endforeach
                @endif
                <form class="user" id="updatepropertyData" method="POST" action="{{ url('admin/propertyData/updateTopHomes',$data->id) }}">
                  @csrf
                  <div class="form-group">
                    <select id="topHomes" class="form-control" name="topHomes" required>
                        <option value="{{$data->topHomes}}">{{$data->topHomes == 1 ? 'Top Home' : 'Not'}}</option>
                        <option value="0">Not</option>
                        <option value="1">Top Home</option>
                      </select>
                      </div>
                  <button type="submit" class="btn btn-primary btn-user btn-block">Update</button>
                </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>


<!------------------------------------------ JS SCRIPT SECTION ------------------------------------------------>

<script type="text/javascript">
    $(document).ready(function(){
      $("#updatepropertyData").validate({
    
        rules: {
          feature: "required"
        },
    
        messages: {
          feature: "Feature can\'t be left blank"
        },
    
        submitHandler: function(form) {
          form.submit();
        }
      });
    });
    
</script>

<!------------------------------------------ JS SCRIPT SECTION ------------------------------------------------>
@stop

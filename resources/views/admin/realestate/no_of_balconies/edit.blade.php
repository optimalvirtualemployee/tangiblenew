@extends('admin.common.innerdefaultreal')
@section('title', 'Update No of Parking')
@section('content')
<style>
.error {
      color: red;
   }
</style>
<div class="container">
  <!-- Outer Row -->
  <div class="row justify-content-center">
    <div class="col-xl-10 col-lg-12 col-md-9">
      <div class="card o-hidden border-0 shadow-lg my-5">
        <div class="card-body p-0">
          <!-- Nested Row within Card Body -->
          <div class="row">
            <div class="col-lg-8">
              <div class="p-5">
                <div class="text-center">
                   <h1 class="h4 text-gray-900 mb-4">Update No of Parking</h1>
                </div>
                @if($errors->any())
                  @foreach($errors->all() as $error)
                    <div class="alert alert-danger">
                      {{$error}}
                    </div>
                  @endforeach
                @endif
                <form class="user" id="editno_of_balconies" method="POST" action="{{ route('no_of_parking.update', $no_of_balconies_data->id) }}">
                  @method('PATCH')
                  @csrf
                  <div class="form-group">
                    <input id="number_of_balconies" class="form-control form-control-user " type="text"  name="number_of_balconies" placeholder="Enter No of Balconies" value="{{ $no_of_balconies_data->number_of_balconies}}">
                  </div>
                  <div class="form-group">
                      <label>Status</label>
                      <select id ="status" name="status" class="form-control" required>
                        <option value="1" {{$no_of_balconies_data->status == "1" ? 'selected' : ''}}>Active</option>
                        <option value="0" {{$no_of_balconies_data->status == "0" ? 'selected' : ''}}>Inactive</option>
                      </select>
                    </div> 
                  <button type="submit" class="btn btn-primary btn-user btn-block">Update</button>
                </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>


<!------------------------------------------ JS SCRIPT SECTION ------------------------------------------------>

<script type="text/javascript">
    $(document).ready(function(){
      $("#editno_of_balconies").validate({
    
        rules: {
          number_of_balconies: {required: true, number: true}
        },
    
        messages: {
          number_of_balconies: {required:"No. of Balconies can\'t be left blank",number:" Please enter numbers only"}
        },
    
        submitHandler: function(form) {
          form.submit();
        }
      });
    });
</script>

<!------------------------------------------ JS SCRIPT SECTION ------------------------------------------------>
@stop

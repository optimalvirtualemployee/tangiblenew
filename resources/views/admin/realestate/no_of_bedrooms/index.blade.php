@extends('admin.common.innerdefaultreal')
@section('title', 'Display No Of Bedrooms')
@section('content')

  <!-- Page Wrapper -->
  <div id="wrapper">
    <div id="content-wrapper" class="d-flex flex-column">
      <!-- Main Content -->
      <div id="content">
        <!-- End of Topbar -->
        <!-- Begin Page Content -->
        <div class="container-fluid">
          <!-- Page Heading -->
          <h1 class="h3 mb-2 text-gray-800">No of Bedrooms</h1>
          <a href="{{route('no_of_bedrooms.create')}}" class="btn btn-primary btn-icon-split">
            <span class="text">Add No of Bedrooms</span>
          </a>
          @if(Session::has('success_msg'))
            <div class="alert " id="writeMsg" style="background-color: #4e73df;margin-top: 20px;color: white;">{{ Session::get('success_msg') }}</div>
          @endif
          <div id="main"></div>
          <div class="card shadow mb-4" style="margin-top: 10px;">
           <div class="card-body" >
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th>No of Bedrooms</th>
                      <th>Status</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  
                  <tbody>
                    @foreach ($no_of_bedrooms as $data)
                      <tr>
                        <td>{{$data->number_of_bedrooms}}</td>
                        <td>@if($data->status =='1') Active  @else Inactive @endif  </td>
                        <td>
                        <a href="{{route('no_of_bedrooms.edit', $data->id)}}"><i class="fas fa-edit"></i></a> &nbsp; &nbsp;
                          <form action="{{ route('no_of_bedrooms.destroy', $data->id) }}" method="POST" id="delete-form-{{ $data->id }}" style="display: none;">
                            {{csrf_field()}}
                            {{ method_field('DELETE') }}
                            <input type="hidden" value="{{ $data->id }}" name="id">
                          </form>
                          <a href="" onclick="if (!confirm('Are you sure you want to delete?')) return; 
                            event.preventDefault(); document.getElementById('delete-form-{{ $data->id }}').submit();">
                            <i class="fas fa-trash-alt"></i>
                          </a>
                      </td>
                      </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
        <!-- /.container-fluid -->
      </div>
      <!-- End of Main Content -->
     

<!----------------------------------------------- JS SCRIPT SECTION -------------------------------------------------->

<script type="text/javascript">
    $(document).ready(function(){
      setTimeout(function(){
          $("div.alert").remove();
      }, 3000 ); 
    });
</script>
<!----------------------------------------------- JS SCRIPT SECTION -------------------------------------------------->
@stop
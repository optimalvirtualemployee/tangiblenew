@extends('admin.common.innerdefaultreal')
@section('title', 'Update No of Bedrooms')
@section('content')
<style>
.error {
      color: red;
   }
</style>
<div class="container">
  <!-- Outer Row -->
  <div class="row justify-content-center">
    <div class="col-xl-10 col-lg-12 col-md-9">
      <div class="card o-hidden border-0 shadow-lg my-5">
        <div class="card-body p-0">
          <!-- Nested Row within Card Body -->
          <div class="row">
            <div class="col-lg-8">
              <div class="p-5">
                <div class="text-center">
                   <h1 class="h4 text-gray-900 mb-4">Update No of Bedrooms</h1>
                </div>
                @if($errors->any())
                  @foreach($errors->all() as $error)
                    <div class="alert alert-danger">
                      {{$error}}
                    </div>
                  @endforeach
                @endif
                <form class="user" id="editno_of_bedrooms" method="POST" action="{{ route('no_of_bedrooms.update', $no_of_bedrooms_data->id) }}">
                  @method('PATCH')
                  @csrf
                  <div class="form-group">
                    <input id="number_of_bedrooms" class="form-control form-control-user required" type="number" min="1" max="100" name="number_of_bedrooms" placeholder="Enter No of Bedrooms" value="{{ $no_of_bedrooms_data->number_of_bedrooms}}">
                  </div>
                  <div class="form-group">
                      <label>Status</label>
                      <select id ="status" name="status" class="form-control" required>
                        <option value="1" {{$no_of_bedrooms_data->status == "1" ? 'selected' : ''}}>Active</option>
                        <option value="0" {{$no_of_bedrooms_data->status == "0" ? 'selected' : ''}}>Inactive</option>
                      </select>
                    </div> 
                  <button type="submit" class="btn btn-primary btn-user btn-block">Update</button>
                </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>


<!------------------------------------------ JS SCRIPT SECTION ------------------------------------------------>

<script type="text/javascript">
    $(document).ready(function(){
      $("#editno_of_bedrooms").validate({
    
        rules: {
          number_of_bedrooms: {required: true, number: true}
        },
    
        messages: {
          number_of_bedrooms: {required:"No. of Bedrooms can\'t be left blank",number:" Please enter numbers only"}
        },
    
        submitHandler: function(form) {
          form.submit();
        }
      });
    });
</script>

<!------------------------------------------ JS SCRIPT SECTION ------------------------------------------------>
@stop

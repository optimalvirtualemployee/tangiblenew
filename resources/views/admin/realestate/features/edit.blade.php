@extends('admin.common.innerdefaultreal')
@section('title', 'Update Feature')
@section('content')

<div class="container">
  <!-- Outer Row -->
  <div class="row justify-content-center">
    <div class="col-xl-10 col-lg-12 col-md-9">
      <div class="card o-hidden border-0 shadow-lg my-5">
        <div class="card-body p-0">
          <!-- Nested Row within Card Body -->
          <div class="row">
            <div class="col-lg-8">
              <div class="p-5">
                <div class="text-center">
                   <h1 class="h4 text-gray-900 mb-4">Update Feature</h1>
                </div>
                @if($errors->any())
                  @foreach($errors->all() as $error)
                    <div class="alert alert-danger">
                      {{$error}}
                    </div>
                  @endforeach
                @endif
                <form class="user" id="editfeature" method="POST" action="{{ route('createpropertyfeature.update', $id) }}">
                  @method('PATCH')
                  @csrf
                  <div class="form-group">
					<div class="form-group">
                  	<input type ="hidden" id="listing_id" class="form-control form-control-user " type="text" maxlength="50" name="listing_id" value="{{$id}}">
                    </div>
                    <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <tbody>
                    <?php echo $html;?>
                    </tbody>
                    </table>
                    </div>
                    </div>
                  <button type="submit" class="btn btn-primary btn-user btn-block">Update</button>
                </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>


<!------------------------------------------ JS SCRIPT SECTION ------------------------------------------------>

<script type="text/javascript">
    $(document).ready(function(){
      $("#editfeature").validate({
    
        rules: {
        	listing_id: "required",
        },
    
        messages: {
        	listing_id: "Listing Id can\'t be left blank",
        },
    
        submitHandler: function(form) {
          form.submit();
        }
      });
    });
</script>

<!------------------------------------------ JS SCRIPT SECTION ------------------------------------------------>
@stop

@extends('admin.common.innerdefaultreal')
@section('title', 'Display Property Features')
@section('content')

  <!-- Page Wrapper -->
  <div id="wrapper">
    <div id="content-wrapper" class="d-flex flex-column">
      <!-- Main Content -->
      <div id="content">
        <!-- End of Topbar -->
        <!-- Begin Page Content -->
        <div class="container-fluid">
          <!-- Page Heading -->
          <h1 class="h3 mb-2 text-gray-800">Property Features</h1>
          <a href="{{url('admin/createpropertyfeature/createFeature',$listing_id)}}" class="btn btn-primary btn-icon-split">
            <span class="text">Add Features</span>
          </a>
          <a href="{{route('createpropertyfeature.edit', $listing_id)}}"  class="btn btn-primary btn-icon-split">
          <span class="text">Edit Features</span>
          </a>
          @if(Session::has('success_msg'))
            <div class="alert " id="writeMsg" style="background-color: #4e73df;margin-top: 20px;color: white;">{{ Session::get('success_msg') }}</div>
          @endif
          <div id="main"></div>
          <div class="card shadow mb-4" style="margin-top: 10px;">
           <div class="card-body" >
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th>Features Available</th>	
                    </tr>
                  </thead>
                  
                  <tbody>
                    @foreach ($details as $data)
                      <tr>
                       <td>{{$data-> property_name}}</td>
                      </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
        <!-- /.container-fluid -->
      </div>
      <!-- End of Main Content -->
     

<!----------------------------------------------- JS SCRIPT SECTION -------------------------------------------------->

<script type="text/javascript">
    $(document).ready(function(){
      setTimeout(function(){
          $("div.alert").remove();
      }, 3000 ); 
    });
</script>
<!----------------------------------------------- JS SCRIPT SECTION -------------------------------------------------->
@stop


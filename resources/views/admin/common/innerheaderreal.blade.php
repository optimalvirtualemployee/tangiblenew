<meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>@yield('title')</title>

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script type="text/javascript" src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.7/jquery.validate.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-steps/1.1.0/jquery.steps.min.js"></script>


  <!-- Custom fonts for this template-->
  <link rel="stylesheet" href="{{url('assets/vendor/fontawesome-free/css/all.min.css')}}">
   <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
	
  <!-- Custom styles for this template-->
  
  <link rel="stylesheet" href="{{url('assets/css/sb-admin-2.min.css')}}">
  <link rel="stylesheet" href="{{url('assets/vendor/datatables/dataTables.bootstrap4.min.css')}}">
  
  <!-- CK editor validate script-->
   <script type="text/javascript" src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.7/jquery.validate.min.js"></script>
</head>

<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">

    <!-- Sidebar -->
    <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

      <!-- Sidebar - Brand -->
      <a class="sidebar-brand d-flex align-items-center justify-content-center" href="/admin/home">
        <div class="sidebar-brand-icon rotate-n-15">
          <i class="fas fa-laugh-wink"></i>
        </div>
        <div class="sidebar-brand-text mx-3"></div>
      </a>

      <!-- Divider -->
      <hr class="sidebar-divider my-0">

      <!-- Nav Item - Dashboard -->
      <li class="nav-item active">
        <a class="nav-link" href="{{url('/admin/realestate/dashboard')}}">
          <i class="fas fa-fw fa-tachometer-alt"></i>
          <span>Dashboard</span></a>
      </li>

      <!-- Divider -->
      <hr class="sidebar-divider">

      <!-- Heading -->
      <div class="sidebar-heading">
        Interface
      </div>

      <!-- Nav Item - Pages Collapse Menu -->
      

      <!-- Nav Item - Utilities Collapse Menu -->
      
      <!-- <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseUtilities" aria-expanded="true" aria-controls="collapseUtilities">
          <i class="fas fa-fw fa-wrench"></i>
          <span>Real Estate Data</span>
        </a>
        <div id="collapseUtilities" class="collapse" aria-labelledby="headingUtilities" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header">Custom Utilities:</h6>
            <a class="collapse-item" href="">Create Properties</a>
           
          </div>
        </div>
      </li> -->


<!------------------------------- Residential MASTER STARTS HERE --------------------------------------->
    
       <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapse_Real_Estate" aria-expanded="true" aria-controls="collapse_Real_Estate">
          <i class="fa fa-building"></i>
          <span>Residential Master</span>
        </a>
        <div id="collapse_Real_Estate" class="collapse" aria-labelledby="headingUtilities" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <!-- <a class="collapse-item" href="{{ url('admin/top_property_type') }}">Top Property Type</a> -->
            <a class="collapse-item" href="{{ url('admin/property_type') }}">Sub Property Type</a>
            <a class="collapse-item" href="{{ url('admin/no_of_bedrooms') }}">No. of Bedrooms</a>
            <a class="collapse-item" href="{{ url('admin/no_of_bathrooms') }}">No. of Bathrooms</a>
            <a class="collapse-item" href="{{ url('admin/no_of_parking') }}">No. of Parking</a>
            <a class="collapse-item" href="{{ url('admin/build_year') }}">Build Year</a>
            <a class="collapse-item" href="{{ url('admin/propertyfeaturelisting') }}">Features</a>
          </div>
        </div>
      </li>

<!------------------------------ Residential MASTER ENDS HERE ------------------------------------------>

<!------------------------------- Commercial MASTER STARTS HERE --------------------------------------->
    
       <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapse_Commercial" aria-expanded="true" aria-controls="collapse_Commercial">
          <i class="fa fa-building"></i>
          <span>Commercial Master</span>
        </a>
        <div id="collapse_Commercial" class="collapse" aria-labelledby="headingUtilities" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <!-- <a class="collapse-item" href="{{ url('admin/top_property_type') }}">Top Property Type</a> -->
            <a class="collapse-item" href="{{ url('admin/commercial_property_type') }}">Sub Property Type</a>
            <a class="collapse-item" href="{{ url('admin/no_of_bathrooms') }}">No. of Bathrooms</a>
            <a class="collapse-item" href="{{ url('admin/no_of_parking') }}">No. of Parking</a>
            <a class="collapse-item" href="{{ url('admin/build_year') }}">Build Year</a>
            <a class="collapse-item" href="{{ url('admin/commercial_propertyfeature') }}">Features</a>
          </div>
        </div>
      </li>

<!------------------------------ REAL ESTATE MASTER ENDS HERE ------------------------------------------>

<!------------------------------- AUCTION MASTER STARTS HERE --------------------------------------->
    
       <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapse_auctionMaster" aria-expanded="true" aria-controls="collapse_auctionMaster">
          <i class="fa fa-gavel"></i>
          <span>Auction Master</span>
        </a>
        <div id="collapse_auctionMaster" class="collapse" aria-labelledby="headingUtilities" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <a class="collapse-item" href="{{ url('admin/enquiry_type') }}">Enquiry Type</a>
            <a class="collapse-item" href="{{ url('admin/purchaser_description') }}">Purchaser Description</a>
          </div>
        </div>
      </li>

<!------------------------------ AUCTION MASTER ENDS HERE ------------------------------------------>

<!------------------------------ PROPERTY DATA STARTS HERE ------------------------------------------>
	
	<li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapse_propertyData" aria-expanded="true" aria-controls="collapse_propertyData">
          <i class="fa fa-gavel"></i>
          <span>Property Data</span>
        </a>
        <div id="collapse_propertyData" class="collapse" aria-labelledby="headingUtilities" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <a class="collapse-item" href="{{ url('admin/propertyData') }}">Property Data</a>
          </div>
        </div>
      </li>
	
<!------------------------------ PROPERTY DATA eNDS HERE -------------------------------------------->

<!------------------------------ PROPERTY Banner STARTS HERE ------------------------------------------>
	
	<li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapse_propertybanner" aria-expanded="true" aria-controls="collapse_propertybanner">
          <i class="fa fa-gavel"></i>
          <span>Property Banner</span>
        </a>
        <div id="collapse_propertybanner" class="collapse" aria-labelledby="headingUtilities" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <a class="collapse-item" href="{{ url('admin/propertybanner') }}">Banner</a>
          </div>
        </div>
      </li>
	
<!------------------------------ PROPERTY Banner ENDS HERE -------------------------------------------->

<!------------------------------ Dream  DATA STARTS HERE ------------------------------------------>
	
	<!-- <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapse_featuredata" aria-expanded="true" aria-controls="collapse_featuredata">
          <i class="fa fa-gavel"></i>
          <span>Featured Properties</span>
        </a>
        <div id="collapse_featuredata" class="collapse" aria-labelledby="headingUtilities" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <a class="collapse-item" href="{{ url('admin/featuredproperty') }}">Show Featured properties</a>
          </div>
        </div>
      </li> -->
	
<!------------------------------ FEATURE DATA ENDS HERE -------------------------------------------->

<!------------------------------ Top homes DATA STARTS HERE ------------------------------------------>
	
	<!-- <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapse_tophomesdata" aria-expanded="true" aria-controls="collapse_tophomesdata">
          <i class="fa fa-gavel"></i>
          <span>Top Homes</span>
        </a>
        <div id="collapse_tophomesdata" class="collapse" aria-labelledby="headingUtilities" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <a class="collapse-item" href="{{ url('admin/tophomesproperty') }}">Show Top Homes</a>
          </div>
        </div>
      </li> -->
	
<!------------------------------ Top homes DATA DATA ENDS HERE -------------------------------------------->

<!------------------------------ FEATURE DATA STARTS HERE ------------------------------------------>
	
	<!-- <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapse_dreamlocationdata" aria-expanded="true" aria-controls="collapse_dreamlocationdata">
          <i class="fa fa-gavel"></i>
          <span>Dream Location</span>
        </a>
        <div id="collapse_dreamlocationdata" class="collapse" aria-labelledby="headingUtilities" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <a class="collapse-item" href="{{ url('admin/dreamlocationproperty') }}">Show Dream Location	</a>
          </div>
        </div>
      </li> -->
	
<!------------------------------ FEATURE DATA ENDS HERE -------------------------------------------->

<!------------------------------ PROPERTY BLOG STARTS HERE ------------------------------------------>
	
	<li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapse_blog" aria-expanded="true" aria-controls="collapse_blog">
          <i class="fa fa-gavel"></i>
          <span>Blogs</span>
        </a>
        <div id="collapse_blog" class="collapse" aria-labelledby="headingUtilities" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <a class="collapse-item" href="{{ url('admin/propertyblog') }}">Show Blogs</a>
          </div>
        </div>
      </li>
	
<!------------------------------ PROPERTY BLOG DATA DATA ENDS HERE -------------------------------------------->

<!------------------------------ PROPERTY NEWS STARTS HERE ------------------------------------------>
	
	<!-- <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapse_news" aria-expanded="true" aria-controls="collapse_news">
          <i class="fa fa-gavel"></i>
          <span>News</span>
        </a>
        <div id="collapse_news" class="collapse" aria-labelledby="headingUtilities" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <a class="collapse-item" href="{{ url('admin/propertynews') }}">Show News</a>
          </div>
        </div>
      </li> -->
	
<!------------------------------ PROPERTY NEWS DATA ENDS HERE -------------------------------------------->



      <!-- Divider -->
      <hr class="sidebar-divider">

      <!-- Heading -->
      

      <!-- Divider -->
      <hr class="sidebar-divider d-none d-md-block">

      <!-- Sidebar Toggler (Sidebar) -->
      <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
      </div>

    </ul>
    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

        <!-- Topbar -->
        <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

          <!-- Sidebar Toggle (Topbar) -->
          <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
            <i class="fa fa-bars"></i>
          </button>

          <!-- Topbar Search -->
          

          <!-- Topbar Navbar -->
          <ul class="navbar-nav ml-auto">

            <!-- Nav Item - Search Dropdown (Visible Only XS) -->
            <li class="nav-item dropdown no-arrow d-sm-none">
              <a class="nav-link dropdown-toggle" href="#" id="searchDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fas fa-search fa-fw"></i>
              </a>
              <!-- Dropdown - Messages -->
              <div class="dropdown-menu dropdown-menu-right p-3 shadow animated--grow-in" aria-labelledby="searchDropdown">
                <form class="form-inline mr-auto w-100 navbar-search">
                  <div class="input-group">
                    <input type="text" class="form-control bg-light border-0 small" placeholder="Search for..." aria-label="Search" aria-describedby="basic-addon2">
                    <div class="input-group-append">
                      <button class="btn btn-primary" type="button">
                        <i class="fas fa-search fa-sm"></i>
                      </button>
                    </div>
                  </div>
                </form>
              </div>
            </li>

            <!-- Nav Item - Alerts -->
            

            <div class="topbar-divider d-none d-sm-block"></div>

            <!-- Nav Item - User Information -->
            <li class="nav-item dropdown no-arrow">
              <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <span class="mr-2 d-none d-lg-inline text-gray-600 small">Welcome {{ Auth::user()->first_name . ' '. Auth::user()->last_name  }}</span>
               
              </a>
              <!-- Dropdown - User Information -->
              <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
                <a class="dropdown-item" href="{{url('admin/profile',Auth::user()->id)}}">
                  <i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>
                  Profile
                </a>
                @canany(['superAdmin-create', 'userManagement-create'])
                <a class="dropdown-item" href="{{ url('admin/contactus') }}">
                  <i class="fas fa-cogs fa-sm fa-fw mr-2 text-gray-400"></i>
                  Contact Us
                </a>
                @endcan
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="#" data-toggle="modal" data-target="#logoutModal">
                  <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                  Logout
                </a>
              </div>
            </li>

          </ul>

        </nav>
        <!-- End of Topbar -->
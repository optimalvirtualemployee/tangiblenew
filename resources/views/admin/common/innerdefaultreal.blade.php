<!DOCTYPE html>
<html>
  <head>
   @include('admin.common.innerheaderreal')
  </head>
  <body>
   
     <!-- Counts Section --> 
     @yield('content')
     @include('admin.common.innerfooter')
    </body>
</html>   
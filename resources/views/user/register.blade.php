<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8" />
    <meta
      name="viewport"
      content="width=device-width, initial-scale=1, shrink-to-fit=no"
    />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <title>Register - Tangible User Portal</title>
    <!-- Favicon -->
    <link rel="icon" href="{{url('assets/img/brand/favicon.png')}}" type="image/png" />
    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700"/>
    <!-- Icons -->
    <link rel="stylesheet" href="{{url('assets/vendor/nucleo/css/nucleo.css')}}" type="text/css"/>
    <link rel="stylesheet" href="{{url('assets/vendor/@fortawesome/fontawesome-free/css/all.min.css')}}" type="text/css"/>
    <!-- Page plugins -->
    <link rel="stylesheet" href="{{url('assets/css/main.css')}}" type="text/css" />
  </head>
  <body style="background: #b6b6b6">
    <!-- Navbar -->
    <nav id="navbar-main" class="navbar navbar-horizontal navbar-transparent navbar-main navbar-expand-lg navbar-light">
      <div class="container">
        <a class="navbar-brand" href="dashboard.html">
          <img src="{{url('assets/img/brand/white.png')}}" />
        </a>
        <button
          class="navbar-toggler"
          type="button"
          data-toggle="collapse"
          data-target="#navbar-collapse"
          aria-controls="navbar-collapse"
          aria-expanded="false"
          aria-label="Toggle navigation"
        >
          <span class="navbar-toggler-icon"></span>
        </button>
        <div
          class="navbar-collapse navbar-custom-collapse collapse"
          id="navbar-collapse"
        >
          <div class="navbar-collapse-header">
            <div class="row">
              <div class="col-6 collapse-brand">
                <a href="dashboard.html">
                  <img src="{{url('assets/img/brand/blue.png')}}" />
                </a>
              </div>
              <div class="col-6 collapse-close">
                <button
                  type="button"
                  class="navbar-toggler"
                  data-toggle="collapse"
                  data-target="#navbar-collapse"
                  aria-controls="navbar-collapse"
                  aria-expanded="false"
                  aria-label="Toggle navigation"
                >
                  <span></span>
                  <span></span>
                </button>
              </div>
            </div>
          </div>
          <ul class="navbar-nav mr-auto">
            <li class="nav-item">
              <a href="{{url('/user/dashboard')}}" class="nav-link">
                <span class="nav-link-inner--text">Dashboard</span>
              </a>
            </li>
            <li class="nav-item">
              <a href="{{url('/user/login')}}" class="nav-link">
                <span class="nav-link-inner--text">Login</span>
              </a>
            </li>
            <li class="nav-item">
              <a href="{{url('/user/register')}}" class="nav-link">
                <span class="nav-link-inner--text">Register</span>
              </a>
            </li>
          </ul>
          <hr class="d-lg-none" />
          <ul class="navbar-nav align-items-lg-center ml-lg-auto">
            <li class="nav-item">
              <a
                class="nav-link nav-link-icon"
                href="https://www.facebook.com/creativetim"
                target="_blank"
                data-toggle="tooltip"
                data-original-title="Like us on Facebook"
              >
                <i class="fab fa-facebook-square"></i>
                <span class="nav-link-inner--text d-lg-none">Facebook</span>
              </a>
            </li>
            <li class="nav-item">
              <a
                class="nav-link nav-link-icon"
                href="https://www.instagram.com/creativetimofficial"
                target="_blank"
                data-toggle="tooltip"
                data-original-title="Follow us on Instagram"
              >
                <i class="fab fa-instagram"></i>
                <span class="nav-link-inner--text d-lg-none">Instagram</span>
              </a>
            </li>
            <li class="nav-item">
              <a
                class="nav-link nav-link-icon"
                href="https://twitter.com/creativetim"
                target="_blank"
                data-toggle="tooltip"
                data-original-title="Follow us on Twitter"
              >
                <i class="fab fa-twitter-square"></i>
                <span class="nav-link-inner--text d-lg-none">Twitter</span>
              </a>
            </li>
            <li class="nav-item">
              <a
                class="nav-link nav-link-icon"
                href="https://github.com/creativetimofficial"
                target="_blank"
                data-toggle="tooltip"
                data-original-title="Star us on Github"
              >
                <i class="fab fa-github"></i>
                <span class="nav-link-inner--text d-lg-none">Github</span>
              </a>
            </li>
          </ul>
        </div>
      </div>
    </nav>
    <!-- Main content -->
    <div class="main-content">
      <!-- Header -->
      <div class="header bg-primary py-7 py-lg-8 pt-lg-9">
        <p><br /></p>
      </div>
      <!-- Page content -->
      <div class="container mt--8 pb-5">
        <!-- Table -->
        <div class="row justify-content-center">
          <div class="col-lg-6 col-md-8">
            <div class="card bg-secondary border-0">
              <div class="card-body px-lg-5 py-lg-5">
                <form role="form" method="POST" action="{{ url('user/newUserRegister') }}">
                @csrf
                  <div class="form-group">
                    <div class="input-group input-group-merge input-group-alternative mb-3">
                      <div class="input-group-prepend">
                        <span class="input-group-text"
                          ><i class="ni ni-hat-3"></i
                        ></span>
                      </div>
                      <input class="form-control{{ $errors->has('first_name') ? ' is-invalid' : '' }}" placeholder="First Name" name="first_name" value="{{ old('first_name') }}" type="text"/>
                    </div>
                    @error('first_name')
                      <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                  </div>
                  <div class="form-group">
                    <div class="input-group input-group-merge input-group-alternative mb-3">
                      <div class="input-group-prepend">
                        <span class="input-group-text"
                          ><i class="ni ni-hat-3"></i
                        ></span>
                      </div>
                      <input class="form-control" placeholder="Last Name" name="last_name" value="{{ old('last_name') }}" type="text"/>
                    </div>
                    @error('last_name')
                      <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                  </div>
                  <div class="form-group">
                    <div class="input-group input-group-merge input-group-alternative mb-3">
                      <div class="input-group-prepend">
                        <span class="input-group-text"
                          ><i class="ni ni-email-83"></i
                        ></span>
                      </div>
                      <input class="form-control" placeholder="Email" name="email" value="{{ old('email') }}" type="email"/>
                    </div>
                    @error('email')
                      <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                  </div>
                  <div class="form-group">
                    <div class="input-group input-group-merge input-group-alternative mb-3">
                      <div class="input-group-prepend">
                        <span class="input-group-text"
                          ><i class="ni ni-email-83"></i
                        ></span>
                      </div>
                      <input class="form-control" placeholder="Phone" name="phone" value="{{ old('phone') }}" type="number"/>
                    </div>
                    @error('phone')
                      <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                  </div>
                  <div class="form-group">
                    <div class="input-group input-group-merge input-group-alternative">
                      <div class="input-group-prepend">
                        <span class="input-group-text"
                          ><i class="ni ni-lock-circle-open"></i
                        ></span>
                      </div>
                      <input class="form-control" placeholder="Password" name="password" type="password"/>
                    </div>
                    @error('password')
                      <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                  </div>
                  <div class="text-muted font-italic">
                    <small
                      >password strength:
                      <span class="text-success font-weight-700"
                        >strong</span
                      ></small
                    >
                  </div>
                  <div class="row my-4">
                    <div class="col-12">
                      <div class="custom-control custom-control-alternative custom-checkbox">
                        <input class="custom-control-input" id="customCheckRegister" name="i_agree" type="checkbox"/>
                        <label
                          class="custom-control-label"
                          for="customCheckRegister"
                          >
                          <span class="text-muted"
                            >I agree with the
                            <a href="#!">Privacy Policy</a></span
                          >
                        </label>
                        @error('i_agree')
                          <div class="alert alert-danger">Please confirm I agree with the Privacy Policy</div>
                        @enderror
                      </div>
                    </div>
                  </div>
                  <div class="text-center">
                    <button type="submit" class="btn btn-primary mt-4">
                      Create account
                    </button>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- Footer -->
    <footer class="pt-5 pb-3">
      <div class="container">
        <div class="row align-items-center justify-content-xl-between">
          <div class="col-lg-6">
            <div class="copyright text-center text-lg-left text-dark">
              &copy; 2020
              <a
                href="#"
                class="font-weight-bold text-dark ml-1"
                target="_blank"
                >Tangible Inc.</a
              >
            </div>
          </div>
          <div class="col-lg-6">
            <ul
              class="nav nav-footer justify-content-center justify-content-lg-end"
            >
              <li class="nav-item">
                <a href="#" class="nav-link text-dark" target="_blank"
                  >About Us</a
                >
              </li>
              <li class="nav-item">
                <a href="#" class="nav-link text-dark" target="_blank">Blog</a>
              </li>
              <li class="nav-item">
                <a href="#" class="nav-link text-dark" target="_blank"
                  >Contact Us</a
                >
              </li>
            </ul>
          </div>
        </div>
      </div>
    </footer>
  <script src="{{url('assets/vendor/jquery/dist/jquery.min.js')}}"></script>
  <script src="{{url('assets/vendor/bootstrap/dist/js/bootstrap.bundle.min.js')}}"></script>
  <script src="{{url('assets/vendor/js-cookie/js.cookie.js')}}"></script>
  <script src="{{url('assets/vendor/jquery.scrollbar/jquery.scrollbar.min.js')}}"></script>
  <script src="{{url('assets/vendor/jquery-scroll-lock/dist/jquery-scrollLock.min.js')}}"></script>
  <!-- Argon JS -->
  <script src="{{url('assets/js/main.min.js')}}"></script>
  </body>
</html>

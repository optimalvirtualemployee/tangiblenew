<!DOCTYPE html>
<html lang="en">
@include('tangiblehtml.headheader')

<body>
    @include('tangiblehtml.innerheader')
    <div class="custom-top-home-wrapper  ">
        <div class="container custom-testimonial-page">
            <div class="main-heading     ">
                <h3>Testimonial</h3>
            </div>
        </div>

        <!---->
        <div class="custom-testimonial-box grey-bg">
            <div class="container">
                <div class="row">
                    <div class="col-lg-4 col-md-6 custom-card">
                        <div class="card">
                            <img class="card-img-top" src="https://www.visme.co/wp-content/uploads/2018/02/IBM-logo.png"
                                alt="Card image">
                            <div class="card-body">
                                <p>Our relationship with Visme has been amazing since day one. Their responsiveness to
                                    our needs and long term plans have helped us use their solutions even more
                                    intensively and to the success of our clients needs.</p>
                                <div class="author">
                                    <!-- <div class="author-img">
                                    <img src="https://www.visme.co/wp-content/uploads/2018/02/i-ibm-hub.jpg" alt="">
                                </div> -->
                                    <div class="author-bio">
                                        <h6>HUBERT JANOWSKI <br> DIGITAL MANAGER</h6>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 custom-card">
                        <div class="card">
                            <img class="card-img-top" src="https://www.visme.co/wp-content/uploads/2018/02/IBM-logo.png"
                                alt="Card image">
                            <div class="card-body">
                                <p>Our relationship with Visme has been amazing since day one. Their responsiveness to
                                    our needs and long term plans have helped us use their solutions even more
                                    intensively and to the success of our clients needs.</p>
                                <div class="author">
                                    <!-- <div class="author-img">
                                    <img src="https://www.visme.co/wp-content/uploads/2018/02/i-ibm-hub.jpg" alt="">
                                </div> -->
                                    <div class="author-bio">
                                        <h6>HUBERT JANOWSKI <br> DIGITAL MANAGER</h6>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 custom-card">
                        <div class="card">
                            <img class="card-img-top" src="https://www.visme.co/wp-content/uploads/2018/02/IBM-logo.png"
                                alt="Card image">
                            <div class="card-body">
                                <p>Our relationship with Visme has been amazing since day one. Their responsiveness to
                                    our needs and long term plans have helped us use their solutions even more
                                    intensively and to the success of our clients needs.</p>
                                <div class="author">
                                    <!-- <div class="author-img">
                                    <img src="https://www.visme.co/wp-content/uploads/2018/02/i-ibm-hub.jpg" alt="">
                                </div> -->
                                    <div class="author-bio">
                                        <h6>HUBERT JANOWSKI <br> DIGITAL MANAGER</h6>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 custom-card">
                        <div class="card">
                            <img class="card-img-top" src="https://www.visme.co/wp-content/uploads/2018/02/IBM-logo.png"
                                alt="Card image">
                            <div class="card-body">
                                <p>Our relationship with Visme has been amazing since day one. Their responsiveness to
                                    our needs and long term plans have helped us use their solutions even more
                                    intensively and to the success of our clients needs.</p>
                                <div class="author">
                                    <!-- <div class="author-img">
                                    <img src="https://www.visme.co/wp-content/uploads/2018/02/i-ibm-hub.jpg" alt="">
                                </div> -->
                                    <div class="author-bio">
                                        <h6>HUBERT JANOWSKI <br> DIGITAL MANAGER</h6>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 custom-card">
                        <div class="card">
                            <img class="card-img-top" src="https://www.visme.co/wp-content/uploads/2018/02/IBM-logo.png"
                                alt="Card image">
                            <div class="card-body">
                                <p>Our relationship with Visme has been amazing since day one. Their responsiveness to
                                    our needs and long term plans have helped us use their solutions even more
                                    intensively and to the success of our clients needs.</p>
                                <div class="author">
                                    <!-- <div class="author-img">
                                    <img src="https://www.visme.co/wp-content/uploads/2018/02/i-ibm-hub.jpg" alt="">
                                </div> -->
                                    <div class="author-bio">
                                        <h6>HUBERT JANOWSKI <br> DIGITAL MANAGER</h6>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 custom-card">
                        <div class="card">
                            <img class="card-img-top" src="https://www.visme.co/wp-content/uploads/2018/02/IBM-logo.png"
                                alt="Card image">
                            <div class="card-body">
                                <p>Our relationship with Visme has been amazing since day one. Their responsiveness to
                                    our needs and long term plans have helped us use their solutions even more
                                    intensively and to the success of our clients needs.</p>
                                <div class="author">
                                    <!-- <div class="author-img">
                                    <img src="https://www.visme.co/wp-content/uploads/2018/02/i-ibm-hub.jpg" alt="">
                                </div> -->
                                    <div class="author-bio">
                                        <h6>HUBERT JANOWSKI <br> DIGITAL MANAGER</h6>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!---->

    </div>
    @include('tangiblehtml.partials.authentication')
    @include('tangiblehtml.innerfooter')
    <script src="/assets/js/jquery.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
        integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous">
    </script>
    <script src="/assets/js/bootstrap.min.js"></script>
    <script src="/assets/js/wow.min.js"></script>
    <script src="/assets/js/slick.min.js"></script>
    <script src="/assets/js/wow.min.js"></script>
    <script src="/assets/js/local.js"></script>

    <script>
    $(document).on('click', '.search-filter-panel .dropdown-menu', function(e) {
        e.stopPropagation();
    });
    </script>
</body>

</html>
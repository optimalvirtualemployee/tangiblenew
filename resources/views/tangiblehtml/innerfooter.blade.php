    <!-- Modal -->  
  <div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="sellNowLabel">Sell Now</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
            <div class="row">
                <div class="col-lg-6">
                    <div class="inner sell-cat">
                        <div class="uppersection mb-3 text-center">
                            <i class=" fa-5x fa fa-lock"></i>
                            <p>Choose between creating a watch or automobile listing</p>
                        </div>
                        <div class="linkto">
                            <a class="sell-btn" href="/privatesellercategoryselect">Create Private Listing</a>
                        </div>
                    </div>
                </div>           
                <div class="col-lg-6">
                    <div class="inner sell-cat">
                        <div class="uppersection mb-3 text-center">
                            <i class="fa fa-5x fa-user"></i>
                            <p>Sign up to showcase your inventory list globally.</p>
                        </div>
                        <div class="linkto">
                            <a class="sell-btn" href="/becomedealer">Become Dealer</a>
                        </div>
                    </div>
                </div>
            </div>

            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>             
            </div>
          </div>
    </div>
  </div>
    
<section>               
        <!-- Modal HTML -->
        <div id="currency" class="modal fade">
            <div class="modal-dialog modal-login">
                <div class="modal-content">                
                    <form action="#" method="post" id="currencyForm" >
                        <div class="modal-header">                       			
                            <h4 class="modal-title">Settings</h4>	
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        </div>
                        <div class="modal-body"> 
                        <div class="register">
                            <div class="form-group">
                                <select name="je_currency" id="" class="form-control">
                                    <option value="AUD">Australian dollar - AUD $</option>
                                    <option value="BRL">Brazilian real - BRL R$</option>
                                    <option value="CAD">Canadian dollar - CAD $</option>
                                    <option value="CZK">Czech korun - CZK Kč</option>
                                    <option value="DKK">Danish krone - DKK kr.</option>
                                    <option value="AED">Emirati dirham - AED AED</option>
                                    <option value="EUR">Euro - EUR €</option>
                                    <option value="HKD">Hong Kong dollar - HKD HK$</option>
                                    <option value="HUF">Hungarian forint - HUF Ft</option>
                                    <option selected="selected" value="INR">Indian rupee - INR ₹</option>
                                    <option value="JPY">Japanese yen - JPY ¥</option>
                                    <option value="MYR">Malaysian ringgit - MYR RM</option>
                                    <option value="MXN">Mexican peso - MXN $</option>
                                    <option value="NOK">Norwegian krone - NOK kr</option>
                                    <option value="PLN">Polish zloty - PLN zł</option>
                                    <option value="GBP">Pound sterling - GBP £</option>
                                    <option value="RUB">Russian ruble - RUB ₽</option>
                                    <option value="SAR">Saudi Arabian riyal - SAR ر.س</option>
                                    <option value="SGD">Singapore dollar - SGD $</option>
                                    <option value="ZAR">South African rand - ZAR R</option>
                                    <option value="KRW">South Korean won - KRW ₩</option>
                                    <option value="SEK">Swedish krona - SEK kr</option>
                                    <option value="CHF">Swiss franc - CHF CHF</option>
                                    <option value="TRY">Turkish lira - TRY ₺</option>
                                    <option value="USD">United States dollar - USD $</option></select>	
                            </div> 
                            <div class="form-group">
                                <select name="je_measurement_units" id="" class="form-control">
                                    <option selected="selected" value="sqft">Square Feet — ft²</option>
                                    <option value="sqm">Square Meter — m²</option></select>
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary btn-lg btn-block login-btn">Save Choice</button>
                            </div>
                    </div>                        
                    </form>
                </div>
            </div>
        </div>     
    </section>
<footer>
        <div class="custom-top-footer pt-5 pb-5">
            <div class="container">
                <div class="row">
                    <div class="col-lg-5 mr-auto col-md-4 mb-4">
                        <div class="custom-footer-logo">
                            <a href="#"><img src="/assets/img/logo-header.png" class="img-fluid"></a>
                            <p>Tangible Listings is a global market place which offers you access to luxury real estate, automobiles and watches. Tangible Listings provides you with a platform to buy and sell luxury items.</p>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-4 mb-4">
                        <div class="custom-footer-links footer-social">
                            <h3>Contact Us</h3>
                            <ul>
                                <li>
                                    <a  href="https://www.facebook.com/tangiblelistings"><i class="fab fa-facebook-f"></i>Tangible Listings</a>
                                </li>
                                <li>
                                    <a  href="https://www.instagram.com/tangiblelistings/"><i class="fab fa-instagram"></i>Tangible Listings</a>
                                </li>
                                <li>
                                    <a href="mailto:email@email.com"><i class="far fa-envelope"></i> {{isset($contactUs->email)?$contactUs->email:'Tangible Listings'}}</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-4">
                        <div class="custom-footer-links footer-nav">
                            <h3>Important Links</h3>
                            <ul>
                               
                                <li>
                                    <a href="#">About</a>
                                </li>
                                <li>
                                    <a href="{{url('faq_page')}}">FAQ</a>
                                </li>
                                <li>
                                    <a href="/term-and-condition">Terms and Conditions</a>
                                </li>
                                <li>
                                    <a href="#" data-toggle="modal" data-target=".bd-example-modal-lg">Sell with Us</a>
                                </li>
                                <li>
                                    <a href="{{url('testimonial_page')}}">Testimonial</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="custom-bottom-footer pt-3 pb-3">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-sm-6 col-12">
                        <div class="copyright-wrapper">
                            <p>© Tangible - All rights reserved</p>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6 col-12">
                        <div class="footer-social-links">
                            <ul>
                                <li><a class="hvr-buzz-out" href="https://www.facebook.com/tangiblelistings"><i class="fab fa-facebook-f"></i></a></li>
                                <li><a class="hvr-buzz-out" href="https://www.instagram.com/tangiblelistings/"><i class="fab fa-instagram"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <script>
    

// Example starter JavaScript for disabling form submissions if there are invalid fields
(function() {
  'use strict';
  window.addEventListener('load', function() {
    // Fetch all the forms we want to apply custom Bootstrap validation styles to
    var forms = document.getElementsByClassName('needs-validation');
    // Loop over them and prevent submission
    var validation = Array.prototype.filter.call(forms, function(form) {
      form.addEventListener('submit', function(event) {
        if (form.checkValidity() === false) {
          event.preventDefault();
          event.stopPropagation();
        }
        form.classList.add('was-validated');
      }, false);
    });
  }, false);
})();


</script>
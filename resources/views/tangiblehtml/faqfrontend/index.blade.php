<!DOCTYPE html>
<html lang="en">
@include('tangiblehtml.landing_page_header')
<body>
    @include('tangiblehtml.innerheader')
       <div class="custom-top-home-wrapper pt-5 ">
            <div class="container my-5">
                <div class="faq_master_category">
                    @foreach($Categories as $cat)
                    <div class="mast__cat mb-2">
                        <a href="/faq_page/{{$cat->slug}}" class="jumbotron row">
                            <div class="col-lg-1 col-sm-2 __cat_icon __cat_icon">
                                <i class="fa fa-bookmark-o" aria-hidden="true"></i>
                            </div>
                            <div class="col-lg-11 col-sm-10   __cat_text">
                                <h1 class="display-6">{{strtoupper($cat->name)}}</h1>
                                <!-- <p class="lead">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                                <div class="row">
                                    <div class="col-lg-1 col-sm-2 author_img">
                                        <img src="img/dummy-user.jpg" alt="">
                                    </div>
                                    <div class="col-lg-11 col-sm-10 author_content">
                                        <p>25 artivales in this collection</p>
                                        <small>Written by <strong>John Doe</strong></small>
                                    </div>
                                </div> -->
                            </div>
                        </a>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    @include('tangiblehtml.innerfooter')
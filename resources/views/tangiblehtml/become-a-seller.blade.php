<!DOCTYPE html>
<html lang="en">
@include('tangiblehtml.headheader')
<head>
<link rel="stylesheet" href="/assets/css/intlTelInput.min.css">
</head>
<body>
@include('tangiblehtml.innerheader')
    <section class="become-seller-from overlay-wrapper">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 d-flex align-items-center">
                    <div class="seller__content">

                        <h3>Please leave your contact details and <span>we will get back to you shortly</span>


                        </h3>
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. </p>
                    </div>
                </div>
                <div class="col-lg-6">
                @if(Session::has('success_msg'))
            <div class="alert " id="writeMsg" style="background-color: #4e73df;margin-top: 20px;color: white;">{{ Session::get('success_msg') }}</div>
          @endif
                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="professional-tab" data-toggle="tab" href="#professional"
                                role="tab" aria-controls="professional-tab" aria-selected="true"> I'm a professional
                                dealer</a>
                        </li>
                    </ul>

                    <div class="seller__from tab-content" id="myTabContent">
                        <div class="tab-pane fade show active" id="professional" role="tabpanel"
                            aria-labelledby="professional-tab">
                            <form id="myForm" method="POST" action="{{ url('/becomedealer/details') }}" >
                            @csrf
                                <div class="form-row">
                                    <div class="form-group col-12">
                                        <input type="text" id="name" placeholder="Name" name="name" class="from-control">
                                    </div>
                                    <div class="form-group col-12">
                                        <input type="number" id="telephone" name="phone_number"  placeholder="Phone number"
                                            class="from-control"><span id="phone"></span>
                                    </div>
                                    <div class="form-group col-12">
                                        <input type="email" id="email" placeholder="Email" name="email" class="from-control"><span id="emailValidation"></span>
                                    </div>
                                    <div class="form-group col-12">
                                        <input type="text" id="companyname" placeholder="Company name" name="company_name" class="from-control">
                                    </div>
                                    <div class="form-group what-you-sell col-12" id="categorycheck">
                                        <label>
                                            What do you sell?
                                        </label>

                                        <div class="sell-item-row mt-2">
                                            <div class="sell-item">
                                                <input type="radio" name="category" value="Car" id="car">
                                                <label for="car">
                                                    <i class="fa fa-car" aria-hidden="true"></i>
                                                    <span>car</span>
                                                </label>
                                            </div>
                                            <div class="sell-item">
                                                <input type="radio" name="category" value="Watch" id="watch">
                                                <label for="watch">
                                                    <i class="fa fa-clock" aria-hidden="true"></i>
                                                    <span>watch</span>
                                                </label>
                                            </div>
                                            <div class="sell-item">
                                                <input type="radio" name="category" value="Real Estate" id="realstate">
                                                <label for="realstate">
                                                    <i class="fa fa-home" aria-hidden="true"></i>
                                                    <span>Realstate</span>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <input id="buttonsubmit" type="button" class="read-more submit-btn" value="Send Message">
                            </form>
                        </div>
                    </div>



                </div>
            </div>
        </div>
    </section>

    <section class="prominent-dealer pt-5 pb-5">
        <div class="container pt-5">
            <div class="row">
                <div class="col-sm-6 d-flex align-items-center">
                    <div class="prominant-content">
                        <h2>Join some of the world's most prominent dealers and <span>agencies of luxury now</span>
                        </h2>
                        <p>Pricing tailored to your needs. Are you a Private seller?</p>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="lx-dealer-wrap">
                        <div class="row">
                            <div class="col-6 col-md-6">
                                <div class="agent-detail-box">
                                    <div class="agent-photo">
                                        <a href="#">
                                            <img src="images/car-dekho.png" alt="team-1" class="img-fluid">
                                        </a>
                                    </div>

                                </div>
                            </div>
                            <div class="col-6  col-md-6">
                                <div class="agent-detail-box">
                                    <div class="agent-photo">
                                        <a href="#">
                                            <img src="images/urban-home.png" alt="team-1" class="img-fluid">
                                        </a>
                                    </div>

                                </div>
                            </div>
                            <div class="col-6 col-md-6">
                                <div class="agent-detail-box">
                                    <div class="agent-photo">
                                        <a href="#">
                                            <img src="images/rolex.png" alt="team-1" class="img-fluid">
                                        </a>
                                    </div>

                                </div>
                            </div>
                            <div class="col-6  col-md-6">
                                <div class="agent-detail-box">
                                    <div class="agent-photo">
                                        <a href="#">
                                            <img src="images/suppertech.png" alt="team-1" class="img-fluid">
                                        </a>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="capture-buyer overlay-wrapper">
        <div class="container">
            <h2>Capture <strong>buyers! </strong> </h2>
            <p> 80% of our users are ready to buy properties, cars, yachts, watches and other luxury items also outside
                their country of residence
            </p>
        </div>
    </section>


    <div class="custom-our-blog  pt-5 pb-5">
        <div class="container">
            <div class="main-heading text-center">
                <h3>From Our Blog</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing</p>
            </div>
            <div class="row">
                <div class="col-sm-7">
                    <div class="blog-left">
                        <div class="blog__item">
                            <a class="home-view" href="#"><img src="images/watch-landing/blog-1.jpg" alt=""
                                    class="img-fluid"></a>
                            <div class="blog-detail-content" style="top:0px !important; bottom:inherit;">
                                <div class="blog-date d-flex">
                                    <span><i class="far fa-calendar-alt"></i> <span>March 9, 2016</span></span>
                                    <span><i class="fas fa-tag"></i> <a href="#"> Business</a></span>
                                </div>
                                <h4><a href="#">Skills That You Can Learn In The Real Estate Market</a></h4>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis mollis et sem sed
                                    sollicitudin. Donec...</p>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-5">
                    <div class="blog-right">
                        <div class="blog__right-item">
                            <div class="blog__right-item__left">
                                <a class="home-view" href="#"><img src="images/watch-landing/cat_0.jpg" alt=""
                                        class="img-fluid"></a>
                            </div>
                            <div class="blog__right-item__right">
                                <div class="blog-date d-flex">
                                    <span><i class="far fa-calendar-alt"></i> <span>March 9, 2016</span></span>

                                </div>
                                <h4><a href="#">Skills That You Can Learn In The Real Estate Market</a></h4>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis mollis</p>


                            </div>
                        </div>
                        <div class="blog__right-item">
                            <div class="blog__right-item__left">
                                <a class="home-view" href="#"><img src="images/watch-landing/cat_01.jpg" alt=""
                                        class="img-fluid"></a>
                            </div>
                            <div class="blog__right-item__right">
                                <div class="blog-date d-flex">
                                    <span><i class="far fa-calendar-alt"></i> <span>March 9, 2016</span></span>

                                </div>
                                <h4><a href="#">Skills That You Can Learn In The Real Estate Market</a></h4>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis mollis </p>


                            </div>
                        </div>
                        <div class="blog__right-item">
                            <div class="blog__right-item__left">
                                <a class="home-view" href="#"><img src="images/watch-landing/cat_02.jpg" alt=""
                                        class="img-fluid"></a>
                            </div>
                            <div class="blog__right-item__right">
                                <div class="blog-date d-flex">
                                    <span><i class="far fa-calendar-alt"></i> <span>March 9, 2016</span></span>

                                </div>
                                <h4><a href="#">Skills That You Can Learn In The Real Estate Market</a></h4>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis mollis </p>


                            </div>
                        </div>


                    </div>
                </div>
            </div>
        </div>
    </div>



    <!---------Method one: Bubble chat - Start-------------->

    <div class="support-box">
        <a href="#" class="icon-text hvr-float-shadow" id="icon-anchor">
            <p><i class="fa fa-envelope-o" aria-hidden="true"></i> Ask Us If You Any Queries</p>
        </a>
    </div>
    <!-- <script src="https://fobi.io/embed.js"></script>
    <div id="embed-fobi"  data-fobi-id="83dyUj5" data-bot-title= "Chat" data-hd-bg= "#eb5322" data-hd-ti-clr= "#FFF" data-ct-pm= "#eb5322" data-ct-sc= "#FFF" data-ct-bot-img= "https://fobi.io/head.png" data-btn-offset= "Right: 30px" data-cb-offset= "Right: 30px" data-btn-img= "https://fobi.io/icon.png" data-btn-bg= "#eb5322" data-cb-height= "400px" data-cb-width= "330px" ></div> -->
    <!--------Method one: Bubble chat - End ---------------->

<@include('tangiblehtml.innerfooter')


    <script src="/assets/js/jquery.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
        integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
        crossorigin="anonymous"></script>
    <script src="/assets/js/bootstrap.min.js"></script>
    <script src="/assets/js/wow.min.js"></script>
    <script src="/assets/js/slick.min.js"></script>
    <script src="/assets/js/intlTelInput.min.js"></script>
    <script src="/assets/js/local.js"></script>
    <script>
    
    
    $(document).ready(function () {

    	$('#buttonsubmit').on('click',function(){	
		var isError = false;

		var name = $('#name').val();
		var companyname = $('#companyname').val();
		var mobile = $("#telephone").val();
		var email = $('#email').val();
        var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
        var categoryname = $('input[name="category"]:checked');

        if(name == ""){
        $('#name').css('border','1px solid red');
        isError = true;
        }else{
        $("#name").removeAttr("style");
        isError = false;
        }

        if(mobile.length !=10){
        phone.style.color = 'red';
        phone.innerHTML = "Required 10 digits!"
        isError = true;	

        }else {
        phone.style.color = 'green';
        phone.innerHTML = "";
        isError = false;
        } 

        if(email =='')
        {
        $('#email').css('border','1px solid red');
        valid = true;
        }else{
             $("#email").removeAttr("style");
        isError = false;
        }
        if(!emailReg.test( email ))
        {
        emailValidation.style.color = 'red';
        emailValidation.innerHTML = "Please Enter Valid Email";
        isError = true;
        }else{
        $("#emailValidation").html("");
        isError = false;
        }

        // if(email == ""){
        // $('#email').css('border','1px solid red');
        // isError = true;
        // }else{
        // $("#email").removeAttr("style");
        // isError = false;
        // }
        // var emailReg = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        // if ( !emailReg.test( email ) ) {
        // emailValidation.style.color = 'red';
        // emailValidation.innerHTML = "Please Enter Valid Email";
        // isError = true;
        // }else{
        // $("#emailValidation").html("");
        // isError = false;
        // }

        if(companyname == ""){
        $('#companyname').css('border','1px solid red');
        isError = true;
        }else{
        $("#companyname").removeAttr("style");
        isError = false;
        }
        if(categoryname.length == 0){
        $('#categorycheck').css('border','1px solid red');
        isError = true;
        }else{
        $("#categorycheck").removeAttr("style");
        isError = false;
        }   

       if(isError == false){
    	   document.getElementById("myForm").submit();
           }

    	});
    });

    $("#telephone").keyup(checkphonenumber);
    
    function checkphonenumber() {

        var mobile = $("#telephone").val();
		
        if(mobile.length !=10){
            
            phone.style.color = 'red';
            phone.innerHTML = "Required 10 digits!"
        }else {
        	phone.style.color = 'green';
        	phone.innerHTML = "";
        }
    }
    
        // Show the first tab and hide the rest
        // jQuery('.row_choser .tb_link').addClass('active');
        jQuery('.tab-content').hide();
        jQuery('.tab-content:first').show();

        // Click function
        jQuery('.tb_link').click(function () {
            jQuery('.tb_link').removeClass('active');
            jQuery(this).addClass('active');
            jQuery('.tab-content').hide();

            var activeTab = jQuery(this).attr('href');
            jQuery(activeTab).fadeIn();
            return false;
        });
         var input = document.querySelector("#telephone");
        window.intlTelInput(input, ({
            separateDialCode:true
        })); 
    </script>
</body>

</html>
<!DOCTYPE html>
<html lang="en">

@include('privateSeller.headheader')
<style>
#loading-image{
  position:fixed;
  top:0px;
  right:0px;
  width:100%;
  height:100%;
  background-color:#666;
  background-image:url('/assets/img/Spinner-loader.gif');
  background-repeat:no-repeat;
  background-position:center;
  z-index:10000000;
  opacity:0.6;
}

 .slick-slide img {
    max-height: 300px;
    width: auto;
  }
  #imgGallery div{
    position: relative;
    width: 150px;
    height: 150px;
    margin: 5px;
    border: 1px solid #e7e7e7;
  }
  #imgGallery div img{
    max-height: 150px;
    width: 100%;
    height: 100%;
    object-fit: cover;
    position: absolute;
   
  }
  .avatar-upload {
  position: relative;
  max-width: 205px;
  border: 2px dotted #bfc8d9;
}
.avatar-upload .avatar-edit {
  position: absolute;
  right: 12px;
  z-index: 1;
  top: 10px;
}
.avatar-upload .avatar-edit input {
  display: none;
}
.avatar-upload .avatar-edit input + label {
  display: inline-block;
  width: 34px;
  height: 34px;
  margin-bottom: 0;
  border-radius: 100%;
  background: #FFFFFF;
  border: 1px solid transparent;
  box-shadow: 0px 2px 4px 0px rgba(0, 0, 0, 0.12);
  cursor: pointer;
  font-weight: normal;
  transition: all 0.2s ease-in-out;
}
.avatar-upload .avatar-edit input + label:hover {
  background: #f1f1f1;
  border-color: #d6d6d6;
}
.avatar-upload .avatar-edit input + label:after {
  content: "\f040";
  font-family: 'FontAwesome';
  color: #757575;
  position: absolute;
  top: 10px;
  left: 0;
  right: 0;
  text-align: center;
  margin: auto;
}
.avatar-upload .avatar-preview {
  width: 192px;
  height: 192px;
  position: relative;
  border-radius: 100%;
  border: 6px solid #F8F8F8;
  box-shadow: 0px 2px 4px 0px rgba(0, 0, 0, 0.1);
  margin: 0 auto;
}
.avatar-upload .avatar-preview > div {
  width: 100%;
  height: 100%;
  border-radius: 100%;
  background-size: cover;
  background-repeat: no-repeat;
  background-position: center;
}
.security-photos{
  background-color: #fcfcfc;
    padding: 10px;
}
.previewContainer{
  background-color: #fff;
    box-shadow: 1px 1px 1px 1px #80808040;
    padding: 25px;
}
</style>
<body>
    @include('privateSeller.innerheader')


    <section class="become-seller-from overlay-wrapper">
    @if (Route::has('login'))
    @auth
        <div class="container">
            <div class="row">                
                <div class="col-lg-12">
                    <!-- MultiStep Form -->
<div class="container-fluid" id="grad1">
    <div class="row justify-content-center mt-0">
        <div class="col-11  col-lg-12 text-center p-0 mt-3 mb-2">
            <div class="card px-0 pt-4 pb-0 mt-3 mb-3">
                <!-- <h2><strong>Sign Up Your User Account</strong></h2>
                <p>Fill all form field to go to next step</p> -->
                <div class="row">
                    <div class="col-md-12 mx-0">
                        <form id="msform" action="{{ url('storeauto') }}" method="post" enctype="multipart/form-data">
                        @csrf
                        <input type="hidden" name="agentId" id="agentId" value="{{$automobile->agent_id}}">
                        <input type="hidden" name="productId" id="productId" value="{{$automobile->id}}">
                            <!-- progressbar -->
                            <ul id="progressbar">
                                <li class="active" id="account"><strong>Select Car</strong></li>
                                <li id="personal"><strong>Add Details</strong></li>
                                <li id="payment"><strong>Attach Photos</strong></li>
                                <li id="confirm"><strong>Review & Save</strong></li>
                            </ul> <!-- fieldsets -->
                            <fieldset>
                                <div class="form-card">
                                	<!-- @Auth
                                    <h2 class="fs-title">Welcome, {{ Auth::user()->first_name . ' '. Auth::user()->last_name  }}</h2>
                                    @endauth -->  
                                    <p><b>What type of Car you Selling?</b></p>
                                    <div class="row">
                                        <div class="col-4"> 
                                            <select class="list-car form-control" id="make1" name="make1" >
                                            @foreach ($makes as $make)
                           						<option @if($makeSelected->id ?? '') @if($makeSelected->id==$make->id) selected @endif @endif value="{{$make->id}}">{{$make->automobile_brand_name}}</option>
                         					@endforeach                                      
                                            </select> 
                                        </div>
                                        <div class="col-4"> 
                                            <select class="list-car form-control" id="model" name="model" >
                                            <option value="{{$modelSelected->id}}">{{$modelSelected->automobile_model_name}}</option>
                                            </select> 
                                        </div>
                                        <div class="col-4"> 
                                            <select class="list-car form-control" id="year" name="year">
                                            @foreach ($year as $buildYear)
                                  <option @if($yearSelected->id ?? '') @if($yearSelected->id==$buildYear->id) selected @endif @endif value="{{$buildYear->id}}">{{$buildYear->build_year}}</option>
                                  @endforeach                                       
                                            </select> 
                                        </div>
                                    </div>
                                    
                                </div> <input type="button" name="next" class="next action-button" id ="next1" value="Next Step" />
                            </fieldset>
                            <fieldset>
                                <div class="form-card">
                                    <div class="">
                                        <h2 class="fs-title">Tell us more about your CAR</h2> 
                                        <div class="row m-0">
                                        
                                        <div class="col-lg-3">
                                              <div class="form-group">
                                                <label class="form-control-label" for="input-last-name">Select Country*</label>
                                                <select class="form-control" data-trigger name="countryId" id="countryId">
                                                    @foreach ($country_data as $country)
                                       <option @if($countrySelected->id ?? '') @if($countrySelected->id==$country->id) selected @endif @endif value="{{$country->id}}">{{$country->country_name}}</option>
                                     @endforeach
                                                  </select>
                                              </div>
                                            </div>
                                            <div class="col-lg-3">
                                              <div class="form-group">
                                                <label class="form-control-label" for="input-last-name">Select State</label>
                                                <select class="form-control" data-trigger name="stateId" id="stateId">
                                                    @foreach ($state_data as $state)
                                  <option @if($stateSelected->id ?? '') @if($stateSelected->id==$state->id) selected @endif @endif value="{{$state->id}}">{{$state->state_name != '' ? $state->state_name : 'Please Select State'}}</option>
                                @endforeach
                                                  </select>
                                              </div>
                                            </div>
                                            <div class="col-lg-3">
                                              <div class="form-group">
                                                <label class="form-control-label" for="input-last-name">Select City*</label>
                                                <select class="form-control" data-trigger name="cityId" id="cityId">
                                                    <option value="{{$citySelected->id}}">{{$citySelected->city_name}}</option>
                                                  </select>
                                              </div>
                                            </div>
                                          </div>
                                          <div class="row m-0">
                                          <div class="col-lg-4">
                                                <div class="form-group">
                                                    <label class="form-control-label" for="input-odometer-metric">Odometer Metric*</label>
                                                <select class="form-control" data-trigger name="odometermetric" id="odometermetric">
                                                    <option value="km" {{ $automobile->metric == 'km'  ? 'selected' : ''}}>KM</option>
                        							<option value="mi" {{ $automobile->metric == 'mi'  ? 'selected' : ''}}>Miles</option>
                        							</select>
                                                
                                                  </div>
                                              </div>
                                              <div class="col-lg-4">
                                                <div class="form-group">
                                                    <label class="form-control-label" for="input-last-name">Odometer*</label>
                                                <div class="input-group mb-3">                                                                                                 
                                                    <input type="number" class="form-control" id = "odometer" name= "odometer" value="{{$automobile->odometer}}" placeholder= "Odometer Value" aria-label="Amount (to the nearest dollar)">
                                                  </div>
                                                  </div>
                                              </div>
                                              <div class="col-lg-4">
                                                <div class="form-group">
                                                    <label class="form-control-label" for="input-last-name">Currency*</label>
                                                <select class="form-control" data-trigger name="currency" id="currency">
                                                    @if($currency_data ?? '')
                                    @foreach ($currency_data as $currency)
                                      <option @if($currencySelected->id ?? '') @if($currencySelected->id==$currency->id) selected @endif @endif value="{{$currency->id}}">{{$currency->currency_code}}</option>
                                    @endforeach
                                    @endif
                        							</select>
                                                  </div>
                                            </div>
                                          </div>
                                          <div class="row m-0">
                                          <!-- <div class="col-lg-4">
                                          <label>Price On Request*</label>
											<select id="price_on_request" class="form-control required" name="price_on_request">
                        					<option value="">Select Price On Request</option>
                        					<option @if($automobile->price_on_request ?? '' ) @if($automobile->price_on_request==0) selected @endif @endif value="0">No</option>
                                  			<option @if($automobile->price_on_request ?? '' ) @if($automobile->price_on_request==1) selected @endif @endif value="1">Yes</option>
                      						</select>
                      						</div> -->
                      						
                      						<div class="col-lg-4">
                                                <div class="form-group">
                                                  <label class="form-control-label" for="input-last-name">Select Price Type*</label>
                               <select class="form-control required"  name="pricetType" id="pricetType" >
                               @foreach ($prices as $price)
                           <option @if($priceSelected->id ?? '') @if($priceSelected->id==$price->id) selected @endif @endif value="{{$price->id}}">{{$price->price}}</option>
                         @endforeach 
						
                      </select>
                                                  
                                                </div>
                                              </div>
                                            <div class="col-lg-4">
                                                <div class="form-group">
                                                    <label class="form-control-label" for="input-last-name">Specify a sale price*</label>
                                                <div class="input-group mb-3">                                                    
                                                    @if($priceSelected->id == 3)
                                <input id="value" class="form-control form-control-user required" type="number" min="0" name="value" placeholder="Enter Car Value" value="{{$automobile->value}}" disabled>
                                @else
                                <input id="value" class="form-control form-control-user required" type="number" min="0" name="value" placeholder="Enter Car Value" value="{{$automobile->value}}" >
                                @endif                                                    
                                                  </div>
                                                  </div>
                                            </div>
                                            
                                            </div>
                                            <div class="row m-0">
                                            <div class="col-lg-4">
                                                <div class="form-group">
                                                  <label class="form-control-label" for="input-last-name">Exterior Colour*</label>
                                                  <select class="form-control" data-trigger name="color" id="color">
                                                      @foreach ($colours as $colour)
                           <option @if($colourSelected->id ?? '') @if($colourSelected->id==$colour->id) selected @endif @endif value="{{$colour->id}}">{{$colour->colour}}</option>
                         @endforeach
                                                    </select>
                                                </div>
                                              </div>
                                            <div class="col-lg-4">
                                                <div class="form-group">
                                                  <label class="form-control-label" for="input-last-name">Interior Colour*</label>
                                                  <select class="form-control" data-trigger name="interior_color" id="interior_color">
                                                      @if($interiorColourSelected)       
						<option value="{{$interiorColourSelected->id}}">{{$interiorColourSelected->interior_colour}}</option>
						@else
						<option value="">Select Interior Color</option>
						@endif
						@foreach ($interior_colours as $interior_colour)
                           <option value="{{$interior_colour->id}}">{{$interior_colour->interior_colour}}</option>
                         @endforeach
                                                    </select>
                                                </div>
                                              </div>
                                              <div class="col-lg-4">
                                                <div class="form-group">
                                                  <label class="form-control-label" for="fuelType">Fuel Type*</label>
                                                  <select class="form-control" data-trigger name="fuelType" id="fuelType">
                                                      @foreach ($fuel_types as $fuel_type)
                           <option @if($fuelTypeSelected->id ?? '') @if($fuelTypeSelected->id==$fuel_type->id) selected @endif @endif value="{{$fuel_type->id}}">{{$fuel_type->fuel_type}}</option>
                         @endforeach
                                                    </select>
                                                </div>                                                
                                               </div>
                                                <div class="col-lg-4">
                                                <div class="form-group">
                                                    <label class="form-control-label" for="registrationPlate">Registration Plate Number*</label> 
                                                <div class="input-group mb-3">                                                                                                 
                                                    <input type="text" class="form-control" id= "registrationPlate" name= "registrationPlate" value= "{{$automobile->registration_number}}" placeholder = "Registration Plate Number" aria-label="Amount (to the nearest dollar)">                                                    
                                                  </div>
                                                </div>
                                                </div>
                                                <div class="col-lg-4">
                                                <div class="form-group">
                                                    <label class="form-control-label" for="registrationPlate">Right/Left Hand Drive*</label> 
                                                <select id="rhdorlhd" class="form-control" name="rhdorlhd">
                        						<option value="">Select RHD/LHD</option>
                       							 <option value="rhd" {{ $automobile->rhdorlhd == 'rhd'  ? 'selected' : ''}}>RHD</option>
                        						 <option value="lhd" {{ $automobile->rhdorlhd == 'lhd'  ? 'selected' : ''}}>LHD</option>
                      							</select>
                                                </div>
                                                </div>
                                                <div class="col-lg-4">
                                                <div class="form-group">
                                                    <label class="form-control-label" for="registrationPlate">Car Condition*</label> 
                                                <select id="condition" class="form-control" name="condition">
                       							 <option value="new" {{ $automobile->neworused == 'new'  ? 'selected' : ''}}>New</option>
                        						 <option value="used" {{ $automobile->neworused == 'used'  ? 'selected' : ''}}>Used</option>
                      							</select>
                                                </div>
                                                </div>
                                                <div class="col-lg-4">
                                                <div class="form-group">
                                                    <label class="form-control-label" for="registrationPlate">Engine*</label> 
                                                <div class="input-group mb-3">                                                                                                 
                                                    <input type="text" class="form-control" id= "engine" name= "engine" value="{{$automobile->engine}}" placeholder = "Engine" aria-label="Amount (to the nearest dollar)">                                                    
                                                  </div>
                                                </div>
                                                </div>
                                                <div class="col-lg-12">
                                                    <div class="form-group">
                                                        <label class="form-control-label" for="input-last-name">Additonal Vehicle Details</label> 
                                                    </div>
                                                </div>
                                                <div class="col-lg-4">
                                                    <div class="form-group">
                                                        <label class="form-control-label" for="no_of_doors">No Of Doors</label>
                                                        <select class="form-control" data-trigger name="no_of_doors" id="no_of_doors">
                                                            @if($noOfDoorsSelected)       
						<option value="{{$noOfDoorsSelected->id}}">{{$noOfDoorsSelected->no_of_doors}}</option>
						@else
						<option value="">Select No Of Doors</option>
						@endif
						@foreach ($no_of_doors as $no_of_door)
                           <option value="{{$no_of_door->id}}">{{$no_of_door->no_of_doors}}</option>
                         @endforeach
                                                          </select>
                                                    </div>
                                                </div>
                                                <div class="col-lg-4">
                                                    <div class="form-group">
                                                        <label class="form-control-label" for="input-last-name">Body Types*</label>
                                                        <select class="form-control" data-trigger name="body_type" id="body_type">
                                                            @foreach ($body_types as $body_type)
                           <option @if($bodyTypeSelected->id ?? '') @if($bodyTypeSelected->id==$body_type->id) selected @endif @endif value="{{$body_type->id}}">{{$body_type->body_type}}</option>
                         @endforeach
                                                          </select>
                                                    </div>
                                                </div>
                                                <div class="col-lg-4">
                                                    <div class="form-group">
                                                        <label class="form-control-label" for="transmission">Transmission*</label>
                                                        <select class="form-control" data-trigger name="transmission" id="transmission">
                                                            @foreach ($transmissions as $transmission)
                           <option @if($transmissionSelected->id ?? '') @if($transmissionSelected->id==$transmission->id) selected @endif @endif value="{{$transmission->id}}">{{$transmission->transmission}}</option>
                         @endforeach
                                                          </select>
                                                    </div>
                                                </div>
                                                <div class="col-lg-4">
                                                    <div class="form-group">
                                                        <label class="form-control-label" for="input-last-name">Registration City</label>
                                                        <div class="input-group mb-3">                                                                                                 
                                                    <input type="text" class="form-control" id= "registrationPlace"  value= "{{$automobile->registration_place}}" name= "registrationPlace" placeholder= "Registration City" aria-label="Amount (to the nearest dollar)">                                                    
                                                  </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-4">
                                                    <div class="form-group">
                                                        <label class="form-control-label" for="input-last-name">Timezone</label>
                                                        <select class="form-control" data-trigger name="timezone" id="timezone">
                                                            @foreach ($timezone as $zone)
                                 <option @if($timezoneSelected->id ?? '') @if($timezoneSelected->id==$zone->id) selected @endif @endif value="{{$zone->id}}">{{$zone->name .' '. $zone->offset}}</option>
                               @endforeach
                                                          </select>
                                                    </div>
                                                </div>
                                                <div class="col-lg-4">
                                                    <div class="form-group">
                                                        <label class="form-control-label" for="input-last-name">Status Of Listing*</label>
                                                        <select id ="status" name="status" class="form-control" >
                          								<option value="1" {{$automobile->status == "1" ? 'selected' : ''}}>Active</option>
                        <option value="0" {{$automobile->status == "0" ? 'selected' : ''}}>Inactive</option>
                        								</select>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label class="form-control-label" for="input-last-name">Vehicle Identification Number* <a href="#">What's my VIN?</a></label>
                                                        <input type="text" id="VIN" name="VIN" class="form-control" placeholder="Vehicle Identification Number" value="{{$automobile->vin_number}}"	>
                                                        <label>Provide confidence and peace of mind by including a VIN with your ad so that potential buyers can obtain a CarFacts History Report.</label>
                                                    </div>
                                                </div>                                                
                                          </div>
                                          <div class="row m-0">
                                            <!-- <div class="col-lg-6">
                                                <h5>How can buyers contact you ?</h5>
                                                <div class="form-group">
                                                    <label class="form-control-label" for="input-last-name">Phone Number </label>
                                                    <input type="text" id="phone-number" class="form-control" placeholder="Phone Number">
                                                    <label>Your number won't be shown on the site and all calls to your virtual number will be diverted to this phone number</label>
                                                </div>
                                            </div> -->
                                            <!-- <div class="col-lg-12">
                                                <div class="inner badges">
                                                    <h2>Add Badges to your Ad</h2>
                                                    <p>To assist you with selling your vehicle during COVID-19 episode time, we're introducting Seller's Badges to tell purchasers what additional items you can offer</p>
                                                    <strong >Select services you can offer</strong>
                                                    <div class="mt-3">
                                                        <div class="custom-control custom-control-alternative custom-checkbox">
                                                            <input class="custom-control-input" id="customCheckRegister" type="checkbox">
                                                            <label class="custom-control-label" for="customCheckRegister">
                                                              <span class="text-muted"> I can video chat with potential buyers </span>
                                                            </label>
                                                        </div>
                                                        <div class="custom-control custom-control-alternative custom-checkbox">
                                                            <input class="custom-control-input" id="customCheckRegister1" type="checkbox">
                                                            <label class="custom-control-label" for="customCheckRegister1">
                                                              <span class="text-muted">  I can bring the vehicle to a potential buyer for an inspection </span>
                                                            </label>
                                                        </div>
                                                        <div class="custom-control custom-control-alternative custom-checkbox">
                                                            <input class="custom-control-input" id="customCheckRegister2" type="checkbox">
                                                            <label class="custom-control-label" for="customCheckRegister2">
                                                              <span class="text-muted"> I'm available on weekdays </span>
                                                            </label>
                                                        </div>
                                                        <div class="custom-control custom-control-alternative custom-checkbox">
                                                            <input class="custom-control-input" id="customCheckRegister3" type="checkbox">
                                                            <label class="custom-control-label" for="customCheckRegister3">
                                                              <span class="text-muted"> I'm available on weekends </span>
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div> -->
                                          </div>
                                      </div>
                                    <h2 class="fs-title mt-5">Select Features</h2> 
                                    <div class="accordion-1">
                                        <div class="container">
                                          <div class="row">
                                            <div class="col-md-12 ml-auto">
                                              <div class="accordion my-3" id="accordionExample">
                                                <div class="card p-0 mb-1">
                                                  <div class="card-header" id="headingOne">
                                                    <h5 class="mb-0">
                                                      <button class="btn btn-link w-100 text-primary text-left" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                                       Car Features
                                                        <i class="ni ni-bold-down float-right"></i>
                                      
                                                      </button>
                                                    </h5>
                                                  </div>                                      
                                                  <div id="collapseOne"
                                              class="collapse show"
                                              aria-labelledby="headingOne"
                                              data-parent="#accordionExample">
                                              <div class="mt-3">
                                                <div class="row">
                                                  <div class="col-lg-6">
                                                    <div class="inner">
                                                    <?php echo $html1; ?>
                                                    </div>
                                                    </div>
                                                    <div class="col-lg-6">
                                                    <div class="inner">
                                                    <?php echo $html2;?>
                                                    </div>
                                                    </div>
                                                    </div>
                                                                            
                                              </div>    
                                            </div>
                                                </div>
                                                <div class="card p-0 mb-1">
                                                  <div class="card-header" id="headingTwo">
                                                    <h5 class="mb-0">
                                                      <button class="btn btn-link w-100 text-primary text-left collapsed" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                                        Additional Car Features
                                                        <i class="ni ni-bold-down float-right"></i>
                                      
                                                      </button>
                                                    </h5>
                                                  </div>
                                                  <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">                                                    
                                                    <div class="additionalFeature">
                                                        <table class="table table-bordered">
                                                            <thead>
                                                            @if($additionalHtml)
                                  <?php echo $additionalHtml; ?>
                                  @endif
                                                              <tr>
                                                                <th>Àdd features</th>
                                                                <th>Action</th>                                                              
                                                              </tr>
                                                            </thead>
                                                            <tbody>
                                                              <tr>
                                                                <td><input type="text" id="additional-feature"  name="additional_feature[]" class="form-control"></td>
                                                                <td><button class="btn btn-success" type="button" id="addRow"><i class="fa fa-plus"></i></button></td>                                                               
                                                              </tr>                                                            
                                                            </tbody>
                                                          </table>
                                                    </div>
                                                  </div>
                                                </div>
                                              </div>
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                     
                                </div> 
                                <div>
                                </div>
                                <input type="button" name="previous" class="previous action-button-previous" value="Previous" /> <input type="button" name="next" class="next action-button" id="next2" value="Next Step" />
                            </fieldset>
                            <fieldset>
                        <div class="form-card">
                          <h2 class="fs-title">Attach Photos</h2>
                          <p class="muted">Lorem ipsum is a dummy text, Lorem ipsum is a dummy text</p>
                          <div class="row" >
                            <div class="col-lg-12">
                              <div class="inner">
                                <div class="input-field">      
                                  <div class="input-images-1" id="input-images-1" style="padding-top: .5rem;"></div>
                                </div>
                                <div class="editNormalImages d-none">
                                @foreach($preloadImages as $images)
                                 <img id="{{$images->id}}" src="{{$images->src}}" alt="">
                                @endforeach
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="row mt-5">
                            <hr class="my-2">
                            <div class="col-lg-12">
                              <div class="inner">
                                <h2 class="fs-title">Damage Photos</h2>
                                <p class="muted">Lorem ipsum is a dummy text, Lorem ipsum is a dummy text</p>
                                <div class="input-field">
                                  <div class="input-images-2" id="damagePhotos" style="padding-top: .5rem;"></div>
                                </div>
                                <div class="editDamageImages d-none">
                                @foreach($loadedDamageImages as $dImages)
                                 <img id="{{$dImages->id}}" src="{{$dImages->src}}" alt="">
                                @endforeach
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="row mt-5">   
                            <div class="col-lg-12">
                              <h2 class="fs-title">VIN Photos</h2>  
                              <p class="muted">Attach photos of your vehicle Identification Number </p>
                            </div>                         
                            <div class="col-lg-6">                             
                              <div class="inner security-photos">
                                <div class="row">                                 
                                  <div class="col-lg-6">
                                    <div class="uploadImage" id= "vin1" name="vin1">
                                      <div class="avatar-upload" >
                                        <div class="avatar-edit" >
                                            <input type='file' id="security1" name="security1[]" class="imageUploadCls" accept=".png, .jpg, .jpeg" />
                                            <label for="security1"></label>
                                        </div>
                                        <div class="avatar-preview">
                                            @if(count($security_data) >0)
                                            <div class="imagePreview" id="{{$security_data[0]->id}}" style="background-image: url({{'/uploads/'.$security_data[0]->filename}});">
                                        @else
                                        <div class="imagePreview" style="background-image: url(img/watch.png);"> 
                                        @endif
                                            </div>
                                        </div>
                                    </div>  
                                    </div>
                                  </div>
                                  <div class="col-lg-6">
                                    <div class="uploadImage" id= "vin2" name="vin2">
                                      <div class="avatar-upload" >
                                        <div class="avatar-edit">
                                            <input type='file' id="security2" name="security2[]" class="imageUploadCls" accept=".png, .jpg, .jpeg" />
                                            <label for="security2"></label>
                                        </div>
                                        <div class="avatar-preview">
                                          @if(count($security_data) >1)
                                            <div class="imagePreview" id="{{$security_data[1]->id}}" style="background-image: url({{'/uploads/'.$security_data[1]->filename}});">
                                        @else
                                        <div class="imagePreview" style="background-image: url(img/watch.png);"> 
                                        @endif
                                            </div>
                                        </div>
                                    </div>  
                                    </div>
                                  </div>
                                </div>                     
                                <hr class="my-2">
                              </div>
                            </div>                            
                          </div>
                          <div class="row mt-5">
                            <hr class="my-2">
                            <div class="col-lg-12">
                              <div class="inner">
                                <h2 class="fs-title">Comments</h2>
                                <hr class="my-2">
                                <label>
                                Setting aside the effort to portray special and
                                recognizing highlights about your vehicle can
                                have the effect in catching a purchaser's
                                consideration.
                                </label>
                                <div class="form-group">
                                  <!-- <div
                                    class="custom-control custom-control-alternative custom-checkbox">
                                    <input class="custom-control-input"
                                      id="inspection" type="checkbox">
                                    <label class="custom-control-label"
                                      for="inspection">
                                    <span class="text-muted"> Allow Buyers
                                    to request Inseption </span>
                                    </label>
                                  </div> -->
                                  <textarea rows="4" class="form-control" id="comment" name="comment"
                                    placeholder="Imagine driving on the rough terrain in this 2012 ACE Cycle-Car. 34 km on the clock only. It is exceptional value at $24. 
                                    Only travelled 34 km. Don't let this go at this price!.">
                                   @if($comment_data->comment ?? '') {{strip_tags($comment_data->comment)}} @endif
                                    </textarea>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <input type="button" name="previous"
                          class="previous action-button-previous" value="Previous" />
                        <input type="button" name="next" class="next action-button" id="next3" value="Next Step" />
                      </fieldset>  
                                    <fieldset>
                                    <div class="form-card">
                                <div class="col-lg-12">
                                    <div class="search-result text-left">                                        
                                        <div class="details-box row mt-4 m-0">
                                            <div class="col-lg-12"><h5>Ads Information</h5></div>
                                            <div class="col-md-12">
                                                <div class="stm-listing-single-price-title heading-font clearfix">
                                                    <div class="price" id="priceTop">$18,000</div>
                            
                                                    <div class="stm-single-title-wrap">
                                                        <h1 class="title" id="title">
                                                            New 2018 Toyota Camry
                                                        </h1>
                                                    </div>
                            
                                                </div>
                                                
                                                <div class="row">                                       
                                        <div class="col-lg-6">
                                            <p>Normal Images</p>
                                            <div id="imgGallery" class="d-flex flex-wrap">
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                          <p>Damage Images</p>
                                          <div id="dmgGallery" class="d-flex flex-wrap">
                                          </div>
                                      </div>
                                    </div>
                                                
                                                <!-- End single-product-images -->
                                                <div class="stm-border-top-unit bg-color-border">
                                                    <h5><strong>Car Details</strong></h5>
                                                </div>
                                                <!-- End stm-border-top-unit -->
                                                <div class="stm-single-car-listing-data">
                                                    <table class="stm-table-main">
                                                        <tbody>
                                                            <tr>
                                                                <td>
                                                                    <table class="inner-table">
                                                                        <tbody>
                                                                            <tr>
                                                                                <td class="label-td"> <i class="fa fa-car"></i> Body </td>
                                                                                <td class="heading-font" id="bodyTypeShow"> Sedan </td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </td>
                                                                <td class="divider-td"></td>
                                                                <td>
                                                                    <table class="inner-table">
                                                                        <tbody>
                                                                            <tr>
                                                                                <td class="label-td"> <i class="fa fa-road"></i> Odometer Reading </td>
                                                                                <td class="heading-font" id ="odometerShow"> 200 </td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </td>
                                                                <td class="divider-td"></td>
                                                                <td>
                                                                    <table class="inner-table">
                                                                        <tbody>
                                                                            <tr>
                                                                                <td class="label-td"> <i class="fa fa-oil"></i> Fuel type </td>
                                                                                <td class="heading-font" id="fuelTypeShow"> Fuel </td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </td>
                                                                <td class="divider-td"></td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <table class="inner-table">
                                                                        <tbody>
                                                                            <tr>
                                                                                <td class="label-td"> <i class="fa fa-engine"></i> Engine </td>
                                                                                <td class="heading-font" id ="engineShow"> 3200 </td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </td>
                                                                <td class="divider-td"></td>
                                                                <td>
                                                                    <table class="inner-table">
                                                                        <tbody>
                                                                            <tr>
                                                                                <td class="label-td"> <i class="fa fa-calendar"></i> Year </td>
                                                                                <td class="heading-font" id="buildYearShow"> 2018 </td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </td>
                                                                <td class="divider-td"></td>
                                                                <td>
                                                                    <table class="inner-table">
                                                                        <tbody>
                                                                            <tr>
                                                                                <td class="label-td"> <i class="fa fa-tag"></i> Price </td>
                                                                                <td class="heading-font" id="priceShow"> 18000 </td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </td>
                                                                <td class="divider-td"></td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <table class="inner-table">
                                                                        <tbody>
                                                                            <tr>
                                                                                <td class="label-td"> <i class="fa fa-car"></i> Transmission </td>
                                                                                <td class="heading-font" id="transmissionShow"> Automatic </td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </td>
                                                                <td class="divider-td"></td>
                                                                <td>
                                                                    <table class="inner-table">
                                                                        <tbody>
                                                                            <tr>
                                                                                <td class="label-td"> <i class="fa fa-cog"></i> Drive </td>
                                                                                <td class="heading-font"> AWD </td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </td>
                                                                <td class="divider-td"></td>
                                                                <td>
                                                                    <table class="inner-table">
                                                                        <tbody>
                                                                            <tr>
                                                                                <td class="label-td"> <i class="fa fa-briefcase" ></i> VIN: </td>
                                                                                <td class="heading-font" id="VINShow"> 3063 </td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </td>
                                                                <td class="divider-td"></td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <table class="inner-table">
                                                                        <tbody>
                                                                            <tr>
                                                                                <td class="label-td"> <i class="fa fa-dot-circle-o"></i> Exterior
                                                                                    Color </td>
                                                                                <td class="heading-font" id="colorShow"> Silver Metallic </td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </td>
                                                                <td class="divider-td"></td>
                                                                <td>
                                                                    <table class="inner-table">
                                                                        <tbody>
                                                                            <tr>
                                                                                <td class="label-td"> <i class="fa fa-dot-circle-o"></i> Interior
                                                                                    Color </td>
                                                                                <td class="heading-font" id="interiorColorShow"> Beige </td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </td>
                                                                <td class="divider-td"></td>
                                                                <td>
                                                                    <table class="inner-table">
                                                                        <tbody>
                                                                            <tr>
                                                                                <td class="label-td"> <i class="fa fa-key"></i> Registered </td>
                                                                                <td class="heading-font" id="registrationPlateShow"> N/A </td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </td>
                                                                <td class="divider-td"></td>
                                                            </tr>
                                                                
                                                        </tbody>
                                                    </table>
                                                </div>
                                                <!-- End stm-single-car-listing-data -->
                                                <div class="stm-car-listing-data-single stm-border-top-unit bg-color-border">
                                                    <h5>Features</h5>
                                                </div>
                                                <div class="stm-single-listing-car-features">
                                                    <div class="lists-inline" id="featureShow">
                                                        
                                                    </div>
                                                </div>                            
                                            </div>                                           
                                        </div>
                           
                            
                                </div> 
                                </div>
                                </div>   
                                <div class="row form-group">
                    <div class="col-md-12">
                        <div class="text-danger font-italic generic-errors-top"></div>
                    </div>
                </div>    
                                <input type="button" name="previous" class="previous action-button-previous" value="Previous" />
                                <input type="submit"  class="next action-button"  value="Submit" />
                        		<!-- <input id="postNow" type="button" name="postnow" class="btn btn-success action-button" value="Post ads Now" data-toggle="modal" data-target="#paymentNow" /> -->                        
                            </fieldset>
                            
                            
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>      </div>
            </div>
        </div>
    </section>
  
        @endauth
   @endif
    
    @include('privateSeller.innerfooter')
    
     <script src="/assets/js/jquery.js"></script>
	<!-- <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/jquery-validation@1.17.0/dist/jquery.validate.min.js"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-steps/1.1.0/jquery.steps.min.js"></script> -->
  <script src="/assets/js/image-uploader.min.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
	<!-- <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script> -->
  <script src="/assets/js/bootstrap.min.js"></script>
  <script src="/assets/js/wow.min.js"></script>
	<script src="/assets/js/slick.min.js"></script>
	<!-- <script src="/assets/js/intlTelInput.min.js"></script> -->
	<script src="/assets/js/local.js"></script>
	<script src="https://js.stripe.com/v3/"></script>
    <script>
    
        // steps JS
        $(document).ready(function(){
        	var current_fs, next_fs, previous_fs; //fieldsets
            var opacity;
            /* const ImgArray = [];
            const ImgArrayDamage = []; */ 
            /* const imageArrayfn = function () {
                alert('inside ');
                $('.uploaded img').each(function (index, value) {
                  if($(this).parents('#damagePhotos').length > 0){
                    ImgArrayDamage.push($(this).attr('src'));
                  }
                  else{
                    ImgArray.push($(this).attr('src'));
                  }
                });
                imgUploadedfn();
            }
        
            const imgUploadedfn = function () {
                let imgCount = ImgArray.length;
                let imgCountDmg = ImgArrayDamage.length;
                let html = '';
                let imgGallery = '';
                let dmgGallery= '';
                debugger;
                if (imgCount > 0) {
                    for (let j = 0; j < imgCount; j++) {                   
                        // html += ' <div class="item" data-src="' + ImgArray[j] + '"><img src="' + ImgArray[j] + '"></div>';
                        imgGallery += '<div><img src="' + ImgArray[j] + '" /></div>';
                    }
                    // $('#uploadedImages').append(html);
                    $('#imgGallery').append(imgGallery);      
                    // $('.slider').slick({
                    //     slidesToShow: 1,
                    //     slidesToScroll: 1,
                    //     autoplay: true,
                    //     arrows: true,
                    //     fade: false
                    // });
                }  
                if (imgCountDmg > 0) {
                    for (let i = 0; i < imgCountDmg; i++) { 
                      dmgGallery += '<div><img src="' + ImgArrayDamage[i] + '" /></div>';
                    }                 
                    $('#dmgGallery').append(dmgGallery);      
                    
                } 

                $('#normalPhotos').val('');
                $('#dmgPhotos').val('');
                $('#normalPhotos').val(ImgArray);
                $('#dmgPhotos').val(ImgArrayDamage);
                
                  console.log('Images Array - > ', ImgArray)  ;  
                  console.log('Damage Images Array - > ', ImgArrayDamage)  ;  
            } */ 

  /* $(".next").click(function(){
alert($(this));
console.log('current this ',$(this));
current_fs = $(this).parent();
next_fs = $(this).parent().next();

//Add Class Active
$("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");

//show the next fieldset
next_fs.show();
//hide the current fieldset with style
current_fs.animate({opacity: 0}, {
step: function(now) {
// for making fielset appear animation
opacity = 1 - now;

current_fs.css({
'display': 'none',
'position': 'relative'
});
next_fs.css({'opacity': opacity});
},
duration: 600
});
}); */  

 $(".previous").click(function(){

current_fs = $(this).parent();
previous_fs = $(this).parent().prev();

//Remove class active
$("#progressbar li").eq($("fieldset").index(current_fs)).removeClass("active");

//show the previous fieldset
previous_fs.show();

//hide the current fieldset with style
current_fs.animate({opacity: 0}, {
step: function(now) {
// for making fielset appear animation
opacity = 1 - now;

current_fs.css({
'display': 'none',
'position': 'relative'
});
previous_fs.css({'opacity': opacity});
},
duration: 600
});
}); 

$('.radio-group .radio').click(function(){
$(this).parent().find('.radio').removeClass('selected');
$(this).addClass('selected');
});

$(".submit").click(function(){
return false;
})
var preloadImages = [];
$(".editNormalImages img").each(function(){
  let imgObj={id:$(this).attr('id'),src:`http://${location.hostname}/${$(this).attr('src')}`};
  preloadImages.push(imgObj);
});

// console.log('preloadImages'+ JSON.stringify(preloadImages));
$('.input-images-1').imageUploader({
  preloaded: preloadImages,
  imagesInputName: 'normalImages',
  preloadedInputName: 'preloaded',
  maxSize: 2 * 1024 * 1024,
  maxFiles: 10
});
var preloadImagesDamage = [];
$(".editDamageImages img").each(function(){
  let imgObjDamage={id:$(this).attr('id'),src:`http://${location.hostname}/${$(this).attr('src')}`};
  preloadImagesDamage.push(imgObjDamage);
});

    $('.input-images-2').imageUploader2({
  preloaded: preloadImagesDamage,
  imagesInputName: 'damageImages2',
  preloadedInputName: 'damageImages2',
  maxSize: 2 * 1024 * 1024,
  maxFiles: 10
});
$( ".uploaded" ).sortable();
$( ".uploaded" ).disableSelection();
var imgType ='';
var imageId
$('.delete-image').on('click',function(){
	alert('delete image');
  imgType = 'normalPhoto'
  imageId = $(this).parents('.ui-sortable-handle').find('input').val();    
  callAjax(imgType,imageId);       
});
$('.delete-image2').on('click',function(){
  imgType = 'damagePhotos'
  imageId = $(this).parents('.ui-sortable-handle').find('input').val();     
  callAjax(imgType,imageId);         
});
function callAjax(imgType,imageId){
 //Call you ajax here 
	console.log('Image type----- ',imgType);
    alert('we are in ajax call method');

     $.ajax({
	    url: "/deleteautoadsimage/" +imageId+ "/" +imgType,
	    type: "get",
	    success : function(data){
	    	console.log('response', data);
	      }
	    });	  
}
});
    </script>
    <script>

    $("#countryId").change(function(){
    	$('#loading-image').show();
        var countryId = $(this).val();
        $.ajax({
          url: "/getState",
          type: "POST",
          data: {countryId: countryId, _token: '{{csrf_token()}}' },
          dataType: 'json',
          success : function(data){
            var html = `<option value="">Select State</option>`;
            for (var i = 0; i < data.length; i++) {
                var id = data[i].id;
                var name = data[i].state_name;
                var option = `<option value="${data[i].id}">${data[i].state_name}</option>`;
                html += option;
              }
              $('#stateId').html(html); 
            }
          });
        $.ajax({
			url: "/getCity",
			type: "POST",
			data: {countryId: countryId  ,_token: '{{csrf_token()}}' },
			dataType: 'json',
			success : function(data){
				var html = `<option value="">Select City</option>`;
				for (var i = 0; i < data.length; i++) {
					  var id = data[i].id;
					  var name = data[i].city_name;
					  var option = `<option value="${data[i].id}">${data[i].city_name}</option>`;
					  html += option;
					}
				$('#loading-image').hide();
					$('#cityId').html(html);	
				}
			});
      });
        $("#stateId").change(function(){
        var stateId = $(this).val();
        $('#loading-image').show();
        $.ajax({
          url: "/getCity",
          type: "POST",
          data: {stateId: stateId, _token: '{{csrf_token()}}' },
          dataType: 'json',
          success : function(data){
            var html = `<option value="">Select City</option>`;
            for (var i = 0; i < data.length; i++) {
                var id = data[i].id;
                var name = data[i].city_name;
                var option = `<option value="${data[i].id}">${data[i].city_name}</option>`;
                html += option;
              }
            $('#loading-image').hide();
              $('#cityId').html(html);  
            }
          });
      });

        $("#country_pi").change(function(){
        	$('#loading-image').show();
            var countryId = $(this).val();
            $.ajax({
              url: "/getState",
              type: "POST",
              data: {countryId: countryId, _token: '{{csrf_token()}}' },
              dataType: 'json',
              success : function(data){
                var html = `<option value="">Select State</option>`;
                for (var i = 0; i < data.length; i++) {
                    var id = data[i].id;
                    var name = data[i].state_name;
                    var option = `<option value="${data[i].id}">${data[i].state_name}</option>`;
                    html += option;
                  }
                  $('#state_pi').html(html); 
                }
              });
            $.ajax({
    			url: "/getCity",
    			type: "POST",
    			data: {countryId: countryId  ,_token: '{{csrf_token()}}' },
    			dataType: 'json',
    			success : function(data){
    				var html = `<option value="">Select City</option>`;
    				for (var i = 0; i < data.length; i++) {
    					  var id = data[i].id;
    					  var name = data[i].city_name;
    					  var option = `<option value="${data[i].id}">${data[i].city_name}</option>`;
    					  html += option;
    					}
    				$('#loading-image').hide();
    					$('#city_pi').html(html);	
    				}
    			});
          });
            $("#state_pi").change(function(){
            var stateId = $(this).val();
            $('#loading-image').show();
            $.ajax({
              url: "/getCity",
              type: "POST",
              data: {stateId: stateId, _token: '{{csrf_token()}}' },
              dataType: 'json',
              success : function(data){
                var html = `<option value="">Select City</option>`;
                for (var i = 0; i < data.length; i++) {
                    var id = data[i].id;
                    var name = data[i].city_name;
                    var option = `<option value="${data[i].id}">${data[i].city_name}</option>`;
                    html += option;
                  }
                $('#loading-image').hide();
                  $('#city_pi').html(html);  
                }
              });
          });  	
    
    $("#make1").change(function(){
		var brandId = $(this).val();
		$('#loading-image').show();
		$.ajax({
			url: "/getCarModel",
			type: "POST",
			data: {brandId: brandId, _token: '{{csrf_token()}}' },
			dataType: 'json',
			success : function(data){
				var html = `<option value="">Select Model</option>`;
				for (var i = 0; i < data.length; i++) {
					  var id = data[i].id;
					  var name = data[i].city_name;
					  var option = `<option value="${data[i].id}">${data[i].automobile_model_name}</option>`;
					  html += option;
					}
				$('#loading-image').hide();
					$('#model').html(html);	
				}
			});
	});


 // VIN Upload Image Script
    $(".imageUploadCls").change(function() {  
      const security = $(this).attr('id');     
      const imgeID = $(this).parents('.avatar-upload').find('.imagePreview').attr('id');  
      deleteVinImage(imgeID);

      if (this.files && this.files[0]) {
            let reader = new FileReader();
            reader.onload = function(e) {                 
                $('#'+security).parents('.avatar-upload').find('.imagePreview').css('background-image', 'url('+e.target.result +')');             
                $('#'+security).parents('.avatar-upload').find('.imagePreview').hide();
                $('#'+security).parents('.avatar-upload').find('.imagePreview').fadeIn(650);
            }
            reader.readAsDataURL(this.files[0]);
          }   
    });
    function deleteVinImage(imgeID){
    alert(imgeID);

    $.ajax({
	    url: "/deleteautoadsimage/" +imgeID+ "/securityPhotos",
	    type: "get",
	    success : function(data){
	    	console.log('response', data);
	      }
	    });
    }
    
        $(function(){
        
          // First register any plugins
          $.fn.filepond.registerPlugin(FilePondPluginImagePreview);
      
          // Turn input element into a pond
          $('.my-pond').filepond();
      
          // Set allowMultiple property to true
          $('.my-pond').filepond('allowMultiple', true);
        
          // Listen for addfile event
          $('.my-pond').on('FilePond:addfile', function(e) {
              console.log('file added event', e);
          });
      
          // Manually add a file using the addfile method
          $('.my-pond').first().filepond('addFile', 'index.html').then(function(file){
            console.log('file added', file);
          });
        
        });
        //Add Row
        $('#addRow').on('click',function(){
            const rowData = '<tr><td><input type="text" id="additional-feature" class="form-control"></td><td><button class="btn btn-danger removeRow" type="button""><i class="fa fa-times"></button></td><tr>';
            $('tbody').append(rowData);
        });

        $(document).on( 'click', '.removeRow', function(){                    
            $(this).parents('tr').remove();
        })
        

          $(document).ready(function(){

        	  	
        	  //var form = document.getElementById('payment-form');
        	  var normalImages =[];
        	  const ImgArray = [];
              const ImgArrayDamage = [];
              const ImgArraySend = [];
              const ImgArrayDamageSend = [];
        	 const imageArrayfn = function () {
                
                 $('.uploaded img').each(function (index, value) {
                   if($(this).parents('#damagePhotos').length > 0){
                       //ImgArrayDamageSend.push($(this));
                     ImgArrayDamage.push($(this).attr('src'));
                   }
                   else{
                	   //ImgArraySend.push($(this));  
                     ImgArray.push($(this).attr('src'));
                   }
                 });
                 imgUploadedfn();
             }
         
             const imgUploadedfn = function () {
                 let imgCount = ImgArray.length;
                 let imgCountDmg = ImgArrayDamage.length;
                 let html = '';
                 let imgGallery = '';
                 let dmgGallery= '';
                 if (imgCount > 0) {
                     for (let j = 0; j < imgCount; j++) {                   
                         // html += ' <div class="item" data-src="' + ImgArray[j] + '"><img src="' + ImgArray[j] + '"></div>';
                         imgGallery += '<div><img src="' + ImgArray[j] + '" /></div>';
                     }
                     // $('#uploadedImages').append(html);
                     $('#imgGallery').append(imgGallery);      
                     // $('.slider').slick({
                     //     slidesToShow: 1,
                     //     slidesToScroll: 1,
                     //     autoplay: true,
                     //     arrows: true,
                     //     fade: false
                     // });
                 }  
                 if (imgCountDmg > 0) {
                     for (let i = 0; i < imgCountDmg; i++) { 
                       dmgGallery += '<div><img src="' + ImgArrayDamage[i] + '" /></div>';
                     }                 
                     $('#dmgGallery').append(dmgGallery);      
                     
                 } 

                   $("input:file").each(function () {

                	  ImgArraySend.push(document.getElementsByName('photos'));		               	 
                     }); 
					
                     var file = $('[name="images"]');

                 $('#normalPhotos').val('');
                 $('#dmgPhotos').val('');
                 $('#normalPhotos').val(ImgArraySend);
                 $('#dmgPhotos').val(ImgArrayDamageSend);
                 
                   console.log('Images Array - > ', ImgArray)  ;  
                   console.log('Damage Images Array - > ', ImgArrayDamage)  ;  
             }
             
             var isError = false;
        	 $("#next1").click(function(){
				var make = $('#make1').val();
				var model = $('#model').val();
				var year = $('#year').val();

				if(make == ""){
					$('#make1').css('border','1px solid red');
					isError = true;
					}else{
						isError = false;
						}
				if(model == ""){
					$('#model').css('border','1px solid red');
					isError = true;
					}else{
						isError = false;
						}
				if(year == ""){
					$('#year').css('border','1px solid red');
					isError = true;
					}else{
						isError = false;
						} 

           	 if(isError == false){
           		current_fs = $('#next1').parent();
    			next_fs = $('#next1').parent().next();

    			//Add Class Active
    			$("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");

    			//show the next fieldset
    			next_fs.show();
    			//hide the current fieldset with style
    			current_fs.animate({opacity: 0}, {
    			step: function(now) {
    			// for making fielset appear animation
    			opacity = 1 - now;

    			current_fs.css({
    			'display': 'none',
    			'position': 'relative'
    			});
    			next_fs.css({'opacity': opacity});
    			},
    			duration: 600
    			});
               	 }
        	 });

        	 $("#next2").click(function(){
        		 var adTitle = $('#adtitle').val();
        		 var country = $('#countryId :selected').val();
        		 var city = $('#cityId :selected').val();
        		 var odometerMetric = $('#odometermetric').val();
        		 var odometer = $('#odometer').val();
        		 var currency = $('#currency').val();
        		 var price = $('#price').val();
        		 var pricetType = $('#pricetType').val();
        		 var color = $('#color').val();
        		 var interior_color = $('#interior_color').val();
        		 var fuelType = $('#fuelType').val();
        		 var registrationPlate = $('#registrationPlate').val();
        		 var rhdorlhd = $('#rhdorlhd').val();
        		 var condition = $('#neworused').val();
        		 var engine = $('#engine').val();
        		 var body_type = $('#body_type').val();
        		 var timezone = $('#timezone').val();
        		 var transmission = $('#transmission').val();
        		 var status = $('#status').val();
        		 var VIN = $('#VIN').val();
				
        		   if(adTitle == ""){
 					$('#adtitle').css('border','1px solid red');
 					isError = true;
 					}else{
 						isError = false;
 						}	
        		 if(country == ""){
  					$('#countryId').css('border','1px solid red');
  					isError = true;
  					}else{
  						isError = false;
  						}
        		 if(city == ""){
   					$('#cityId').css('border','1px solid red');
   					isError = true;
   					}else{
   						isError = false;
   						}
        		 if(odometerMetric == ""){
    					$('#odometermetric').css('border','1px solid red');
    					isError = true;
    					}else{
    						isError = false;
    						}
        		 if(odometer == ""){
    					$('#odometer').css('border','1px solid red');
    					isError = true;
    					}else{
    						isError = false;
    						}
        		 if(currency == ""){
 					$('#currency').css('border','1px solid red');
 					isError = true;
 					}else{
 						isError = false;
 						}
        		 if(price == ""){
   					$('#price').css('border','1px solid red');
   					isError = true;
   					}else{
   						isError = false;
   						}					
        		 if(pricetType == ""){
    					$('#pricetType').css('border','1px solid red');
    					isError = true;
    					}else{
    						isError = false;
    						}
        		 if(color == ""){
 					$('#color').css('border','1px solid red');
 					isError = true;
 					}else{
 						isError = false;
 						}	
        		 if(interior_color == ""){
  					$('#interior_color').css('border','1px solid red');
  					isError = true;
  					}else{
  						isError = false;
  						}	
        		 if(fuelType == ""){
 					$('#fuelType').css('border','1px solid red');
 					isError = true;
 					}else{
 						isError = false;
 						}	
        		 if(registrationPlate == ""){
  					$('#registrationPlate').css('border','1px solid red');
  					isError = true;
  					}else{
  						isError = false;
  						}
        		 if(rhdorlhd == ""){
   					$('#rhdorlhd').css('border','1px solid red');
   					isError = true;
   					}else{
   						isError = false;
   						}
        		 if(condition == ""){
   					$('#condition').css('border','1px solid red');
   					isError = true;
   					}else{
   						isError = false;
   						}
        		 if(engine == ""){
   					$('#engine').css('border','1px solid red');
   					isError = true;
   					}else{
   						isError = false;
   						}
        		 if(body_type == ""){
    					$('#body_type').css('border','1px solid red');
    					isError = true;
    					}else{
    						isError = false;
    						}		
        		 if(transmission == ""){
    					$('#transmission').css('border','1px solid red');
    					isError = true;
    					}else{
    						isError = false;
    						}			
        		 if(status == ""){
    					$('#status').css('border','1px solid red');
    					isError = true;
    					}else{
    						isError = false;
    						}	
        		 if(VIN == ""){
    					$('#VIN').css('border','1px solid red');
    					isError = true;
    					}else{
    						isError = false;
    						}  
        		 if(timezone == ""){
  					$('#timezone').css('border','1px solid red');
  					isError = true;
  					}else{
  						isError = false;
  						}
						
        		 if(isError == false){
        			 current_fs = $('#next2').parent();
         			next_fs = $('#next2').parent().next();

         			//Add Class Active
         			$("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");

         			//show the next fieldset
         			next_fs.show();
         			//hide the current fieldset with style
         			current_fs.animate({opacity: 0}, {
         			step: function(now) {
         			// for making fielset appear animation
         			opacity = 1 - now;

         			current_fs.css({
         			'display': 'none',
         			'position': 'relative'
         			});
         			next_fs.css({'opacity': opacity});
         			},
         			duration: 600
         			});
            		 }
        		 
        		 
            	 });

        	 $("#pricetType").change(function(){ 
 				var htmlcarPrice = '';
 				var htmlPrice = '';
 				alert($(this).val());
             if($(this).val() == 3){
             	htmlcarPrice += '<input id="value" class="form-control form-control-user " type="number"  min="0" name="value" placeholder="Enter Car Value" value="" disabled>';
           	  /* htmlPrice = '<select id="pricetType" class="form-control "  name="pricetType" disabled> <option value="">Select Price</option>'; */
 				$("#price").replaceWith(htmlcarPrice);
 				//$("#pricetType").replaceWith(htmlPrice);	
                }else{
             	   htmlcarPrice += '<input id="value" class="form-control form-control-user" type="number"  min="0" name="value" placeholder="Enter Car Value" value="">';
               	 /* htmlPrice = '<select id="pricetType" class="form-control "  name="pricetType"> <option value="">Select Price</option> @foreach ($prices as $price) <option value="{{$price->id}}">{{$price->price}}</option> @endforeach </select>'; */
   					$("#price").replaceWith(htmlcarPrice);
   					//$("#pricetType").replaceWith(htmlPrice);	
   	                 } 
             
         });

        	 $('#next3').click(function() {

 				var comment = $('#comment').val();

 				 if(comment == ""){
					$('#comment').css('border','1px solid red');
					isError = true;
					}else{
						isError = false;
								}

				
 				if($('.uploaded-image').length == 0){
					$('#input-images-1').css('border','1px solid red');
					isError = true;
					}else{
						isError = false;
						}

 				if($('.uploaded-image').length == 0){
					$('#damagePhotos').css('border','1px solid red');
					isError = true;
					}else{
						isError = false;
						}
 				if($('.uploaded-image').length == 0){
					$('#vin1').css('border','1px solid red');
					isError = true;
					}else{
						isError = false;
						}
 				if($('.uploaded-image').length == 0){
					$('#vin2').css('border','1px solid red');
					isError = true;
					}else{
						isError = false;
						} 

 				if(isError == false){
 					imageArrayfn();
        			 current_fs = $('#next3').parent();
         			next_fs = $('#next3').parent().next();

         			//Add Class Active
         			$("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");

         			//show the next fieldset
         			next_fs.show();
         			//hide the current fieldset with style
         			current_fs.animate({opacity: 0}, {
         			step: function(now) {
         			// for making fielset appear animation
         			opacity = 1 - now;

         			current_fs.css({
         			'display': 'none',
         			'position': 'relative'
         			});
         			next_fs.css({'opacity': opacity});
         			},
         			duration: 600
         			});
            		 }
         });    

        	 $('#next4').click(function() {

        		 var make = $('#make :selected').text();
                 var model = $('#model :selected').text();
                 var title = $('#adtitle').val();
                 var buildYear = $('#year :selected').text();
 				var price = $('#price').val();
 				var currency = $('#currency :selected').text();
 				var body_type = $('#body_type :selected').text();
 				var fuelType = $('#fuelType :selected').text();
 				var engine = $('#engine').val();
 				var transmission = $('#transmission :selected').text();
 				var color = $('#color :selected').text();
 				var interior_color = $('#interior_color :selected').text();
 				var registrationPlate = $('#registrationPlate').val();
 				var VIN = $('#VIN').val();
 				var odometermetric = $('#odometermetric :selected').text();
 				var odometer = $('#odometer').val();
 				
 				
 				var titleHtml = title + ' ' + make + ' ' + model;
 				$('#title').html(titleHtml);
 				$('#priceShow').html(price);
 				var priceTop = price;
 				$('#priceTop').html(priceTop);
 				$('#bodyTypeShow').html(body_type);
 				$('#buildYearShow').html(buildYear);
 				$('#fuelTypeShow').html(fuelType);
 				$('#engineShow').html(engine);
 				$('#transmissionShow').html(transmission);
 				$('#colorShow').html(color);
 				$('#interiorColorShow').html(interior_color);
 				$('#registrationPlateShow').html(registrationPlate);
 				$('#VINShow').html(VIN);
 				var odoShow = odometer +' '+ odometermetric;
 				$('#odometerShow').html(odoShow);

 	            var k = 1;
 				var radioHtml = ``;
 	            
 				$("input:checkbox:checked").each(function () {
 					
 					   var name = $(this).attr("id");
 					    var value = $(this).attr("value");
 							
 						if (value == 1) {

 					    	if(k == 1)
 			                    radioHtml += `<ul class="list-style-2">\n`;

 			                    radioHtml += `<li>${name}</li>\n`;

 							if(k == 4)	
 								radioHtml += `</ul>\n`;

 				           k++;
 				           if(k == 5)
 					           k = 1; 
 						
 					   }

 					});			
 				$('#featureShow').html(radioHtml);


 				var firstName = $('#firstName').val();
 				var lastName = $('#lastName').val();
 				var dob = $('#dob').val();
 				var address = $('#address').val();
 				var zipcode = $('#zipcode').val();
 				var country_pi = $('#country_pi').val();
 				var city_pi = $('#city_pi').val();
 				var phonenumber = $('#phonenumber').val();
				
       		  if(firstName == ""){
					$('#firstName').css('border','1px solid red');
					isError = true;
					}else{
						isError = false;
						}
       		if(lastName == ""){
				$('#lastName').css('border','1px solid red');
				isError = true;
				}else{
					isError = false;
					}
       		if(dob == ""){
				$('#dob').css('border','1px solid red');
				isError = true;
				}else{
					isError = false;
					}
       		if(address == ""){
				$('#address').css('border','1px solid red');
				isError = true;
				}else{
					isError = false;
					}
       		if(zipcode == ""){
				$('#zipcode').css('border','1px solid red');
				isError = true;
				}else{
					isError = false;
					}
       		if(country_pi == ""){
				$('#country_pi').css('border','1px solid red');
				isError = true;
				}else{
					isError = false;
					}
       		if(city_pi == ""){
				$('#city_pi').css('border','1px solid red');
				isError = true;
				}else{
					isError = false;
					}
       		if(address == ""){
				$('#phonenumber').css('border','1px solid red');
				isError = true;
				}else{
					isError = false;
					} 

	       		 
        		 if(isError == false){
  					//imageArrayfn();
         			 current_fs = $('#next4').parent();
          			next_fs = $('#next4').parent().next();

          			//Add Class Active
          			$("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");

          			//show the next fieldset
          			next_fs.show();
          			//hide the current fieldset with style
          			current_fs.animate({opacity: 0}, {
          			step: function(now) {
          			// for making fielset appear animation
          			opacity = 1 - now;

          			current_fs.css({
          			'display': 'none',
          			'position': 'relative'
          			});
          			next_fs.css({'opacity': opacity});
          			},
          			duration: 600
          			});
        		 }
        		 
            	 });	

        	 $('#next5').click(function() {
        		 

        		 if(!$('#agb').prop('checked')){
        			 $('.generic-errors-top').html('The terms conditions must be accepted.');
        			 isError = true;
 				}else{
 					isError = false;
 					}
        		 
        		 if(isError == false){
   					//imageArrayfn();
          			 current_fs = $('#next5').parent();
           			next_fs = $('#next5').parent().next();

           			//Add Class Active
           			$("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");

           			//show the next fieldset
           			next_fs.show();
           			//hide the current fieldset with style
           			current_fs.animate({opacity: 0}, {
           			step: function(now) {
           			// for making fielset appear animation
           			opacity = 1 - now;

           			current_fs.css({
           			'display': 'none',
           			'position': 'relative'
           			});
           			next_fs.css({'opacity': opacity});
           			},
           			duration: 600
           			});

         		 }
        		 
            	 });
             
         });

        
        $(window).on('load',function(){
            $('#myModal1').modal({backdrop: 'static', keyboard: false});
        });


        var initialPrice = $('.ads-fee').text();
        
        $('.activatethis').on('click',function(){           
        	  if($(this).parents('.thispackage').hasClass('activatedPackage')){
        	    $(this).parents('.thispackage').removeClass('activatedPackage');
        	    $(this).text('Activate');
        	    $('.packageName').text('Listing Fee');
        	    $('.ads-fee').text(initialPrice);
        	  }   
        	  else{
        	  $('.thispackage').removeClass('activatedPackage');
        	  $('.activatethis').text('Activate');
        	  $(this).text('Deactivate')
        	  $(this).parents('.thispackage').addClass('activatedPackage');        
        	  $('.packageName').text($('.activatedPackage h2').text());
        	  $('.ads-fee').text($('.activatedPackage .amount').text());

        	  }
        	  

        	})
        //Payment options
      $('input[type=radio][name=postAds]').change(function() {       
      if (this.value == 'free') {
         $('.paymentOptionContainer').addClass('d-none');
         $('#postNow').removeClass('d-none');
      }
      else if (this.value == 'paid') {
        $('.paymentOptionContainer').removeClass('d-none');
        $('#postNow').addClass('d-none');
      }
      });

        /* data: {brandId: brandId, _token: '{{csrf_token()}}' } */
       
        


         // Create an instance of the Stripe object
            // Set your publishable API key
            var stripe = Stripe('{{ env("STRIPE_PUBLISH_KEY") }}');

            // Create an instance of elements
            var elements = stripe.elements();

            var style = {
                base: {
                    fontWeight: 400,
                    fontFamily: '"DM Sans", Roboto, Open Sans, Segoe UI, sans-serif',
                    fontSize: '16px',
                    lineHeight: '1.4',
                    color: '#1b1642',
                    padding: '.75rem 1.25rem',
                    '::placeholder': {
                        color: '#ccc',
                    },
                },
                invalid: {
                    color: '#dc3545',
                }
            };

            var cardElement = elements.create('cardNumber', {
                style: style
            });
            cardElement.mount('#card_number');

            var exp = elements.create('cardExpiry', {
                'style': style
            });
            exp.mount('#card_expiry');

            var cvc = elements.create('cardCvc', {
                'style': style
            });
            cvc.mount('#card_cvc');

            // Validate input of the card elements
            var resultContainer = document.getElementById('paymentResponse');
            cardElement.addEventListener('change', function (event) {
                if (event.error) {
                    resultContainer.innerHTML = '<p>' + event.error.message + '</p>';
                } else {
                    resultContainer.innerHTML = '';
                }
            });

            // Get payment form element
            var form = document.getElementById('msform');

            // Create a token when the form is submitted.
            form.addEventListener('submit', function (e) {
                e.preventDefault();
                createToken();
            });

            // Create single-use token to charge the user
            function createToken() {
                stripe.createToken(cardElement).then(function (result) {
                    if (result.error) {
                        // Inform the user if there was an error
                        resultContainer.innerHTML = '<p>' + result.error.message + '</p>';
                    } else {
                        // Send the token to your server
                        stripeTokenHandler(result.token);
                    }
                });
            }

            
            // Callback to handle the response from stripe
            function stripeTokenHandler(token) {
                
                // Insert the token ID into the form so it gets submitted to the server
                var hiddenInput = document.createElement('input');
                hiddenInput.setAttribute('type', 'hidden');
                hiddenInput.setAttribute('name', 'stripeToken');
                hiddenInput.setAttribute('value', token.id);
                form.appendChild(hiddenInput);

                // Submit the form
                form.submit();
            }
            
            
            $('.pay-via-stripe-btn').on('click', function () {
                var payButton   = $(this);
                var name        = $('#name').val();
                var email       = $('#email').val();

                if (name == '' || name == 'undefined') {
                    $('.generic-errors').html('Name field required.');
                    return false;
                }
                if (email == '' || email == 'undefined') {
                    $('.generic-errors').html('Email field required.');
                    return false;
                }

                if(!$('#terms_conditions').prop('checked')){
                    $('.generic-errors').html('The terms conditions must be accepted.');
                    return false;
                }
            });

      </script>


</body>

</html>
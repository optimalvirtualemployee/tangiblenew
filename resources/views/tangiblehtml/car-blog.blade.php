<!DOCTYPE html>
<html lang="en">

@include('tangiblehtml.headheader')

<body>
    @include('tangiblehtml.innerheader')

    <section class="become-seller-from overlay-wrapper">
      
    </section>

    <div class="custom-our-blog  pt-5 pb-5">
        <div class="container">
            <div class="main-heading stm-border-bottom-unit bg-color-border">
                <h3>{{$blog->title}}</h3>
                <p>{{date('F d, Y', strtotime($blog->created_at))}}
                </p>
            </div>
            <div class="row">
                <div class="col-sm-7">
                    <div class="blog-left">
                        <img src="{{url('uploads/'.$blog_images->firstWhere('listing_id',$blog->id)->filename)}}" alt="" class="img-fluid">                       
                    </div>
                     <div class="blog__item">
                        
                            <div class="blog-detail-content">
                                <div class="blog-date d-flex">
                                    <span><i class="far fa-calendar-alt"></i> <span>{{date('F d, Y', strtotime($blog->created_at))}}</span></span>
                                    <!--<span><i class="fas fa-tag"></i> <a href="#"> {{$blog->blogType}}</a></span>-->
                                </div>
                                 <p>{{$blog->blogPost}}</p>
                            </div>
                        </div> 
                </div>
                <div class="col-sm-5">
                    <div class="blog-right">
                    @foreach($type_blog as $data)
                        <div class="blog__right-item">
                            <div class="blog__right-item__left">
                            @if($data->url != null)
                            <a class="home-view" href="{{url($data->url)}}"><img src="{{url('uploads/'.$data->filename)}}" alt="" class="img-fluid"></a>
                            @else
                                <a class="home-view" href="{{url('carblog/'.$data->id)}}"><img src="{{url('uploads/'.$data->filename)}}" alt="" class="img-fluid"></a>
                                @endif
                            </div>
                            <div class="blog__right-item__right">
                                <div class="blog-date d-flex">
                                    <span><i class="far fa-calendar-alt"></i> <span>{{date('F d, Y', strtotime($data->created_at))}}</span></span>

                                </div>
                                @if($data->url != null)
                                <h4><a href="{{url($data->url)}}">{{$data->title}}</a></h4>
                                @else
                                <h4><a href="{{url('carblog/'.$data->id)}}">{{$data->title}}</a></h4>
                                @endif
                                <p>{{$data->title}}</p>


                            </div>
                        </div>
                        @endforeach
                        <!-- <div class="blog__right-item">
                            <div class="blog__right-item__left">
                                <a class="home-view" href="#"><img src="images/watch-landing/cat_01.jpg" alt=""
                                        class="img-fluid"></a>
                            </div>
                            <div class="blog__right-item__right">
                                <div class="blog-date d-flex">
                                    <span><i class="far fa-calendar-alt"></i> <span>March 9, 2016</span></span>

                                </div>
                                <h4><a href="#">Skills That You Can Learn In The Real Estate Market</a></h4>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis mollis </p>


                            </div>
                        </div> -->
                        <!-- <div class="blog__right-item">
                            <div class="blog__right-item__left">
                                <a class="home-view" href="#"><img src="images/watch-landing/cat_02.jpg" alt=""
                                        class="img-fluid"></a>
                            </div>
                            <div class="blog__right-item__right">
                                <div class="blog-date d-flex">
                                    <span><i class="far fa-calendar-alt"></i> <span>March 9, 2016</span></span>

                                </div>
                                <h4><a href="#">Skills That You Can Learn In The Real Estate Market</a></h4>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis mollis </p>


                            </div>
                        </div> -->
                        <!-- <div class="blog__right-item">
                            <div class="blog__right-item__left">
                                <a class="home-view" href="#"><img src="images/watch-landing/cat_02.jpg" alt=""
                                        class="img-fluid"></a>
                            </div>
                            <div class="blog__right-item__right">
                                <div class="blog-date d-flex">
                                    <span><i class="far fa-calendar-alt"></i> <span>March 9, 2016</span></span>

                                </div>
                                <h4><a href="#">Skills That You Can Learn In The Real Estate Market</a></h4>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis mollis </p>


                            </div>
                        </div> -->
                        <!-- <div class="blog__right-item">
                            <div class="blog__right-item__left">
                                <a class="home-view" href="#"><img src="images/watch-landing/cat_02.jpg" alt=""
                                        class="img-fluid"></a>
                            </div>
                            <div class="blog__right-item__right">
                                <div class="blog-date d-flex">
                                    <span><i class="far fa-calendar-alt"></i> <span>March 9, 2016</span></span>

                                </div>
                                <h4><a href="#">Skills That You Can Learn In The Real Estate Market</a></h4>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis mollis </p>


                            </div>
                        </div> -->


                    </div>
                </div>
            </div>
        </div>
    </div>



    <!---------Method one: Bubble chat - Start-------------->

    <div class="support-box">
        <a href="#" class="icon-text hvr-float-shadow" id="icon-anchor">
            <p><i class="fa fa-envelope-o" aria-hidden="true"></i> Ask Us If You Any Queries</p>
        </a>
    </div>
    <!-- <script src="https://fobi.io/embed.js"></script>
    <div id="embed-fobi"  data-fobi-id="83dyUj5" data-bot-title= "Chat" data-hd-bg= "#eb5322" data-hd-ti-clr= "#FFF" data-ct-pm= "#eb5322" data-ct-sc= "#FFF" data-ct-bot-img= "https://fobi.io/head.png" data-btn-offset= "Right: 30px" data-cb-offset= "Right: 30px" data-btn-img= "https://fobi.io/icon.png" data-btn-bg= "#eb5322" data-cb-height= "400px" data-cb-width= "330px" ></div> -->
    <!--------Method one: Bubble chat - End ---------------->

    @include('tangiblehtml.innerfooter')


    <script src="/assets/js/jquery.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
        integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
        crossorigin="anonymous"></script>
    <script src="/assets/js/bootstrap.min.js"></script>
    <script src="/assets/js/wow.min.js"></script>
    <script src="/assets/js/slick.min.js"></script>
    <script src="/assets/js/intlTelInput.min.js"></script>
    <script src="/assets/js/local.js"></script>
  
</body>

</html>
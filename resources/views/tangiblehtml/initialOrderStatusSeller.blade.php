<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Review Order</title>
    <link href="https://fonts.googleapis.com/css2?family=Muli:wght@200;300;400;500;600;700;800;900&display=swap"
        rel="stylesheet">
    <link rel="stylesheet" href="/assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/hover.css/2.3.1/css/hover-min.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.css" />
    <link rel="stylesheet" href="/assets/css/animate.css">
    <link rel="stylesheet" href="/assets/css/font-style.css">
    <link rel="stylesheet" href="/assets/css/local.css">
    <link rel="stylesheet" href="/assets/css/responsive.css">
    <style>
 
 .track-order-status .track {
     position: relative;
     background-color: #ddd;
     height: 7px;
     display: -webkit-box;
     display: -ms-flexbox;
     display: flex;
     margin-bottom: 60px;
     margin-top: 50px
 }

 .track-order-status .track .step {
     -webkit-box-flex: 1;
     -ms-flex-positive: 1;
     flex-grow: 1;
     width: 25%;
     margin-top: -18px;
     text-align: center;
     position: relative
 }

 .track-order-status .track .step.active:before {
     background: #FF5722
 }

 .track-order-status .track .step::before {
     height: 7px;
     position: absolute;
     content: "";
     width: 100%;
     left: 0;
     top: 18px
 }

 .track-order-status .track .step.active .icon {
     background: #ee5435;
     color: #fff
 }

 .track-order-status .track .icon {
     display: inline-block;
     width: 40px;
     height: 40px;
     line-height: 40px;
     position: relative;
     border-radius: 100%;
     background: #ddd
 }

 .track-order-status .track .step.active .text {
     font-weight: 400;
     color: #000
 }

 .track-order-status .track .text {
     display: block;
     margin-top: 7px
 }

 .track-order-status .itemside {
     position: relative;
     display: -webkit-box;
     display: -ms-flexbox;
     display: flex;
     width: 100%
 }

 .track-order-status .itemside .aside {
     position: relative;
     -ms-flex-negative: 0;
     flex-shrink: 0
 }

 .track-order-status .img-sm {
     width: 80px;
     height: 80px;
     padding: 7px
 }

 .track-order-status ul.row,
 .track-order-status ul.row-sm {
     list-style: none;
     padding: 0
 }

 .track-order-status .itemside .info {
     padding-left: 15px;
     padding-right: 7px
 }

 .track-order-status .itemside .title {
     display: block;
     margin-bottom: 5px;
     color: #212529
 }


 .track-order-status .btn-warning {
     color: #ffffff;
     background-color: #ee5435;
     border-color: #ee5435;
     border-radius: 1px
 }

 .track-order-status .btn-warning:hover {
     color: #ffffff;
     background-color: #ff2b00;
     border-color: #ff2b00;
     border-radius: 1px
 }
 .response-time{
    border: 6px solid #ededed;
    padding: 10px;
    width: 230px;
    margin-bottom: 10px;
 }
 .averageTime{
     margin: 0;
     font-weight: bold;
     font-size: 13px;
 }
 .track-order-status .itemPrice,.track-order-status .shippingCost,.track-order-status .totalPrice,.track-order-status .yourCurrency, .track-order-status .paymentCurrency{
     display: flex;
     justify-content: space-between;
     align-items: center;
 }
    </style>
</head>

<body class="track-order-status">
    <@include('tangiblehtml.innerheader')
   <div class="container pt-5 ">
       <div class="row">
           <div class="col-lg-9">
               <div class="inner">
                <article class="card p-1 mt-5">
                    <div class="card-body">
                        <div class="track">
                        
                        @if(isset($currentStep))
                    @if($currentStep->order_status == 'seller_approved')
                            <div class="step active"> <span class="icon"> <i class="fa fa-shopping-cart"></i> </span> <span class="text">Order Submitted</span> </div>
                            <div class="step"> <span class="icon"> <i class="fa fa-user"></i> </span> <span class="text"> </span> </div>
                            <div class="step"> <span class="icon"> <i class="fa fa-truck"></i> </span> <span class="text">  </span> </div>
                        <div class="step"> <span class="icon"> <i class="fa fa-home"></i> </span> <span class="text">Delivery Expected</span> </div>
                      @endif
                      @if($currentStep->order_status == 'waiting_for_payment')
                        <div class="step active"> <span class="icon"> <i class="fa fa-shopping-cart"></i> </span> <span class="text">Order Submitted</span> </div>
                        <div class="step active"> <span class="icon"> <i class="fa fa-user"></i> </span> <span class="text"> </span> </div>
                        <div class="step"> <span class="icon"> <i class="fa fa-truck"></i> </span> <span class="text">  </span> </div>
                        <div class="step"> <span class="icon"> <i class="fa fa-home"></i> </span> <span class="text">Delivery Expected</span> </div>
                      @endif
                      @if($currentStep->order_status == 'shipped')
                        <div class="step active"> <span class="icon"> <i class="fa fa-shopping-cart"></i> </span> <span class="text">Order Submitted</span> </div>
                        <div class="step active"> <span class="icon"> <i class="fa fa-user"></i> </span> <span class="text"> </span> </div>
                        <div class="step active"> <span class="icon"> <i class="fa fa-truck"></i> </span> <span class="text">  </span> </div>
                        <div class="step"> <span class="icon"> <i class="fa fa-home"></i> </span> <span class="text">Delivery Expected</span> </div>
                      @endif
                      @endif      
                            
                        </div>
                    </div>
                </article>
                <div class="row">
                    <div class="col-lg-12">
                    @if(isset($currentStep))
                    @if($currentStep->order_status == 'seller_approved')
                        <div class="inner">
                            <h3>Waiting for buyer to accept your offer</h3>
                            <p>We have forwarded your offer to buyer, We will notify you as soon as the buyer has accepted your order.</p>
                        </div>
                      @endif
                      @if($currentStep->order_status == 'waiting_for_payment')
                        <div class="inner">
                            <h3>Waiting for buyer to make payment</h3>
                            <p>We have forwarded your offer to buyer, We will notify you as soon as the buyer has made payment.</p>
                        </div>
                      @endif
                      @if($currentStep->order_status == 'shipped')
                        <div class="inner">
                            <h3>Waiting for buyer to accept the delivery</h3>
                            <p>We have forwarded your shipping details to buyer, We will notify you as soon as the buyer has received the product.</p>
                        </div>
                      @endif
                      @endif
                    </div>
                    
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        
                    </div>
                </div>
               </div>
           </div>
           <div class="col-lg-3">
               <div class="inner">
                <div class="watch-product-item mt-5">
                    <div class="product-thumb">
                      <a href="#"><img src="{{url('uploads/',$images->filename)}}" alt=""></a>
                      <div class="d-none">
                        <ul>
                          <li><a href="#"><i class="fas fa-search-plus"></i></a></li>
                          <li><a href="#"><i class="far fa-heart"></i></a></li>
                          <li><a href="#"><i class="fas fa-plus"></i></a></li>
                        </ul>
                      </div>
                      <div class="inside">
                        <div class="contents">
                          <table class="w-100">
                            <tbody><tr>
                              <td>Model</td>
                              <td><span>TS-2019</span></td>
                            </tr>
                            <tr>
                              <td>Year</td>
                              <td><span>2019</span></td>
                            </tr>
                            <tr>
                              <td>Make</td>
                              <td><span>Something</span></td>
                            </tr>
                            <tr>
                              <td>Case Diameter</td>
                              <td><span>Something</span></td>
                            </tr>
                            <tr>
                              <td>Location</td>
                              <td><span>San Diego, CA</span></td>
                            </tr>
                          </tbody></table>
                        </div>
                      </div>
                    </div>
                    <div class="product-details">
                      <h5><a href="#">{{$enquiry->ad_title}}</a></h5>
                      <div class="price-wrap">
                        <span class="Price-currency"></span>{{currency()->convert(floatval($enquiry->value), 'USD', currency()->getUserCurrency())}}
                      </div>
                      <div class="bottom-detais">
                        <div class="dealer">
                          <ul>
                            <li><i class="fa fa-star"></i></li>
                            <li><i class="fa fa-star"></i></li>
                            <li><i class="fa fa-star"></i></li>
                            <li><i class="fa fa-star"></i></li>
                            <li><i class="fa fa-star"></i></li>
                          </ul>
                          <p class="seller-type"><i class="fa fa-check-square-o" aria-hidden="true"></i> {{ $enquiry->agency_id == 'Private Seller' ? "Private Seller" : "Dealer" }}</p>
                        </div>
                        <div class="country-flag">
                          <img src="{{url('uploads/',$enquiry->countryflag)}}" alt="">
                          <p class="c-code">{{$enquiry->countrycode}}</p>
                        </div>
                      </div>
                    </div>
    
                    <!-- <a href="javascript:void(0);" title="click here" class="icon"><i class="fa fa-info-circle" aria-hidden="true"></i></a> -->
    
                  </div>
               </div>
           </div>
       </div>
       <div class="row bg-light mt-5 mb-5">         
               <div class="col-lg-9">
                   <div class="row">
                    <div class="col-lg-6">
                        <div class="inner p-3 mt-3 mb-3 bg-white">
                            <div class="orderDetails d-flex mt-3">
                                 <div class="icon mr-3">
                                     <i class="fa  fa-file"></i>
                                 </div>
                                 <div class="order">
                                     <h4>Order Details</h4>
                                     <ul class="d-inline">
                                         <li>Payment Via and escow account</li>
                                         <li>Expected Delivery : <span>{{$enquiry->deliveryTime}}</span></li>
                                     </ul>
                                 </div>    
                            </div>
                            <div class="shippingDetails d-flex mt-3">
                             <div class="icon mr-3">
                                 <i class="fa   fa-map-marker"></i>
                             </div>
                             <div class="orderAddress">
                                 <h4>Shiiping Address</h4>
                                 <ul class="w-100 d-inline">
                                      
                                             <li> <span class="firstName">{{$enquiry->userFirstName}}</span> <span class="lastName">{{$enquiry->userLastName}}</span></li> 
                                         
                                             <li><address>{{$enquiry->userAddress}}</address></li>
                                             <li> <span class="city">{{$user_city->city_name}},</span> @if(isset($user_state))<span class="state">{{$user_state->state_name}}</span>@else<span class="state"></span>@endif</li>
                                             <li class="ZipCode">{{$enquiry->userPostalCode}}</li>
                                             <li class="Country">{{$user_country->country_name}}</li> 
                                      </ul>
                             </div>
                            </div>
                            <div class="sellerDetails d-flex mt-3">
                             <div class="icon mr-3">
                                 <i class="fa fa-user"></i>
                             </div>
                             <div class="">
                                 <p>WatchShopping.com Inc.</p>
                                 <div class="sendMessage">
                                     <a href=""><i class="fa fa-envelope mr-3"></i>Send Message</a>
                                    
                                 </div>
                             </div>                        
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="inner mt-3 mb-3 p-3">
                         <h4>Cost Summary</h4>
                            <div class="itemPrice">
                                     <div class="itemLabel">Seller Counter Offer</div>
                                     <div class="ItemCost">{{currency()->convert(floatval($enquiry->finalOfferValue), 'USD', currency()->getUserCurrency())}}</div>
                                 </div>
                                 <div class="shippingCost">
                                      <div class="shippingCostLabel">Shipping Cost</div>
                                      <div class="ItemCost">{{currency()->convert(floatval($enquiry->shippingCost), 'USD', currency()->getUserCurrency())}} </div>
                                  </div>
                                  <hr>
                                  <div class="totalPrice">
                                      <div class="totalPriceLabel"><strong>Total Price</strong></div>
                                      <div class="totalCost"><strong>{{currency()->convert(floatval($enquiry->finalOfferValue + $enquiry->shippingCost), 'USD', currency()->getUserCurrency())}}</strong></div>
                             
                        </div>
                    </div>
                   </div>
               </div>          
       </div>
    </div>
    </div>
    @include('tangiblehtml.innerfooter')
    <script src="/assets/js/jquery.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
        integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
        crossorigin="anonymous"></script>
    <script src="/assets/js/bootstrap.min.js"></script>
    <script src="/assets/js/wow.min.js"></script>
    <script src="/assets/js/slick.min.js"></script>
    <script src="/assets/js/wow.min.js"></script>
    <script src="/assets/js/local.js"></script>

    <script>
    $(document).on('click', '.search-filter-panel .dropdown-menu', function (e) {
      e.stopPropagation();   
    });
    </script>
</body>

</html>

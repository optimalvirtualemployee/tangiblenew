<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Checkout Payment</title>
    <link href="https://fonts.googleapis.com/css2?family=Muli:wght@200;300;400;500;600;700;800;900&display=swap"
        rel="stylesheet">
    <link rel="stylesheet" href="/assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/hover.css/2.3.1/css/hover-min.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.css" />
    <link rel="stylesheet" href="/assets/css/animate.css">
    <link rel="stylesheet" href="/assets/css/font-style.css">
    <link rel="stylesheet" href="/assets/css/local.css">
    <link rel="stylesheet" href="/assets/css/track-order.css">
    <link rel="stylesheet" href="/assets/css/responsive.css">
</head>

<body class="track-order-status">
    <@include('tangiblehtml.innerheader')
   <div class="container pt-5 ">
       <div class="row">
           <div class="col-lg-9">
               <div class="inner">
                <article class="card p-1 mt-5">
                    <div class="card-body">
                        <div class="track">
                            <div class="step active"> <span class="icon"> <i class="fa fa-shopping-cart"></i> </span> <span class="text">Order Submitted</span> </div>
                            <div class="step active"> <span class="icon"> <i class="fa fa-credit-card"></i> </span> <span class="text"> </span> </div>
                            <div class="step"> <span class="icon"> <i class="fa fa-truck"></i> </span> <span class="text">  </span> </div>
                            <div class="step"> <span class="icon"> <i class="fa fa-home"></i> </span> <span class="text">Delivery Expected</span> </div>
                        </div>
                    </div>
                </article>
                <div class="row">
                    <!-- <div class="col-lg-12">
                        <div class="inner">
                            <h3>Please pay for the item now</h3>
                            <p>The Seller has made you a counter offer. We've highlighted their changes below. Please ensure your review all the information. if you would like to proceed, please accept this counteroffer and pay for the item.</p>
                            <div class="d-flex">
                                <table class="table border p-2">
                                    <tr><td>Your price suggestion: </td><td>AU$ 4,600</td></tr>
                                    <tr><td>New offer:</td><td>= $3,759</td></tr>
                                </table>                       
                            </div>
                        </div>
                    </div>  -->                   
                </div>
                <div class="accordion" id="accordionExample">
                    <div class="card mb-0">
                      <div class="card-header" id="headingOne">
                        <h2 class="mb-0">
                          <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                            <i class="fa fa-shopping-cart"></i> Your Order Details
                          </button>
                        </h2>
                      </div>
                  
                      <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
                        <div class="card-body">
                            <div class="row bg-light">
                                <div class="col-lg-6">
                                    <div class="inner p-3 mt-3 mb-3 bg-white">
                                        <div class="orderDetails d-flex mt-3">
                                             <div class="icon mr-3">
                                                 <i class="fa  fa-file"></i>
                                             </div>
                                             <div class="order">
                                                 <h4>Order Details</h4>
                                                 <ul class="d-inline">
                                                     <li>Payment Via and escow account</li>
                                                     <li>Expected Delivery : <span>{{$enquiry->deliveryTime}}</span></li>
                                                 </ul>
                                             </div>    
                                        </div>
                                        <div class="shippingDetails d-flex mt-3">
                                         <div class="icon mr-3">
                                             <i class="fa fa-map-marker"></i>
                                         </div>
                                         <div class="orderAddress">
                                             <h4>Shiping Address</h4>
                                             <ul class="w-100 d-inline">
                                                <li> <span class="firstName">{{$enquiry->userFirstName}}</span> <span class="lastName">{{$enquiry->userLastName}}</span></li> 
                                         
                                             <li><address>{{$enquiry->userAddress}}</address></li>
                                             <li> <span class="city">{{$user_city->city_name}},</span> @if(isset($user_state))<span class="state">{{$user_state->state_name}}</span>@else<span class="state"></span>@endif</li>
                                             <li class="ZipCode">{{$enquiry->userPostalCode}}</li>
                                             <li class="Country">{{$user_country->country_name}}</li>
                                             </ul>
                                         </div>
                                        </div>
                                        <div class="sellerDetails d-flex mt-3">
                                         <div class="icon mr-3">
                                             <i class="fa fa-user"></i>
                                         </div>
                                         <div class="">
                                             <p>WatchShopping.com Inc.</p>
                                             <div class="sendMessage">
                                                 <a href=""><i class="fa fa-envelope mr-3"></i>Send Message</a>
                                                
                                             </div>
                                         </div>                        
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="inner mt-3 mb-3 p-3">
                                     <h4>Cost Summary</h4>
                                        <div class="itemPrice">
                                            <div class="itemLabel">Item Price</div>
                                            <div class="ItemCost">{{currency()->convert(floatval($enquiry->finalOfferValue), 'USD', currency()->getUserCurrency())}}</div>
                                        </div>
                                        <div class="shippingCost">
                                             <div class="shippingCostLabel">Shipping Costs</div>
                                             <div class="ItemCost">{{currency()->convert(floatval($enquiry->shippingCost), 'USD', currency()->getUserCurrency())}}</div>
                                         </div>
                                         <hr>
                                         <div class="totalPrice">
                                             <div class="totalPriceLabel"><strong>Total Price</strong></div>
                                             <div class="totalCost"><strong>{{currency()->convert(floatval($enquiry->finalOfferValue + $enquiry->shippingCost), 'USD', currency()->getUserCurrency())}}</strong></div>
                                         </div>
                                    </div>
                                </div>
                               </div>
                        </div>
                      </div>
                    </div>                   
                  </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="inner border p-2">
                            <h4 class="border-bottom pb-3">Transaction ID : <strong>TR-3207336</strong></h4>
                            <div class="paymentType d-flex justify-content-between align-items-center">
                                <div class="paymentDesc">
                                    <strong>Credit Card</strong>
                                    <h6>Visa, Mastercard, American Express</h6>
                                    <p>American Express plus 2% method fee </p>
                                </div>
                                <div class="paymentCards d-flex">                                  
                                       <img src="https://img.icons8.com/color/48/000000/visa.png">
                                       <img src="https://img.icons8.com/color/48/000000/mastercard-logo.png">
                                       <img src="https://img.icons8.com/color/48/000000/maestro.png"> 
                                </div>                                
                            </div>
                            <form  method="POST" action="{{url('/buyer/payment/')}}">
                            @csrf
                            <div class="cardDetails">
                                <div class="row m-0">
                                    
                                    <div class="w-100">
                                    <input type="hidden" name="enquiryId" value="{{$enquiry->id}}" />
						<input type="hidden" name="queryType" value="{{$enquiry->queryType}}" />
						<input type="hidden" name="category" value="{{$enquiry->category}}" />
						<input type="hidden" name="messageId" value="{{$enquiry->messageId}}" />
                                        <div class="form-group">
                                          <label for="card-number">Credit Card Number</label>
                                          <input type="`text" class="form-control" id="card-number" placeholder="Card Number">
                                          <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
                                        </div>
                                        <div class="form-group">
                                          <label for="Expiration">Expiration Date</label>
                                          <input type="text" class="form-control" id="Expiration" placeholder="MM/YY">
                                        </div>      
                                        <div class="form-group">
                                            <label for="secuirtyCode">Expiration Date</label>
                                            <input type="text" class="form-control" id="secuirtyCode" placeholder="Security Code">
                                          </div> 
                                          <div class="bg-light p-3 d-flex align-items-center justify-content-between">
                                              <strong>Total Price</strong>
                                              <strong>$4,090</strong>  
                                          </div>
                                          
                                      </div>                                    
                                </div>
                            </div>

                        </div>
                    </div>
                </div>    
                <div class="row mb-3">
                    <div class="col-lg-12">
                        <div class="conditions border-top pt-3">
                            <input type="checkbox" id="termsConditons" name="termsConditons" value="termsConditons">
                            <label for="termsConditons">I have read and accept the <a href="#">Terms and Conditions of Sale.</a></label> <br>

                            <input type="checkbox" id="awareof" name="awareof" value="awareof">
                            <label for="awareof">I am aware that this purchase may incure <a href="#">Additional customs duties and import taxes</a></label>
                        </div>
                    </div>
                </div>            
                  <p class="d-flex justify-content-between" >                    
                    <button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
                     Order Progress
                    </button>
                    <button class="btn btn-success" type="submit">Pay Now</button>
                  </p>
                  </form>
                  <div class="collapse" id="collapseExample">
                    <div class="card card-body">
                     <div class="orderList d-flex justify-content-between bg-light p-3 mb-3">
                         <div class="date">June, 19</div>
                         <div class="person">Buyer</div>
                         <div class="orderStatus">Offer Accepted</div>
                     </div> 
                     <div class="orderList d-flex justify-content-between bg-light p-3 mb-3">
                        <div class="date">June, 19</div>
                        <div class="person">Buyer</div>
                        <div class="orderStatus">Offer Accepted</div>
                    </div> 
                    <div class="orderList d-flex justify-content-between bg-light p-3 mb-3">
                        <div class="date">June, 19</div>
                        <div class="person">Seller</div>
                        <div class="orderStatus">Offer requested</div>
                    </div>                    
                    </div>
                  </div>
                 
               </div>
           </div>
           <div class="col-lg-3">
               <div class="inner">
                <div class="watch-product-item mt-5">
                    <div class="product-thumb">
                      <a href="#"><img src="{{url('uploads/',$images->filename)}}" alt=""></a>
                      <div class="d-none">
                        <ul>
                          <li><a href="#"><i class="fas fa-search-plus"></i></a></li>
                          <li><a href="#"><i class="far fa-heart"></i></a></li>
                          <li><a href="#"><i class="fas fa-plus"></i></a></li>
                        </ul>
                      </div>
                      <div class="inside">
                        <div class="contents">
                          <table class="w-100">
                            <tbody><tr>
                              <td>Model</td>
                              <td><span>TS-2019</span></td>
                            </tr>
                            <tr>
                              <td>Year</td>
                              <td><span>2019</span></td>
                            </tr>
                            <tr>
                              <td>Make</td>
                              <td><span>Something</span></td>
                            </tr>
                            <tr>
                              <td>Case Diameter</td>
                              <td><span>Something</span></td>
                            </tr>
                            <tr>
                              <td>Location</td>
                              <td><span>San Diego, CA</span></td>
                            </tr>
                          </tbody></table>
                        </div>
                      </div>
                    </div>
                    <div class="product-details">
                      <h5><a href="#">{{$enquiry->ad_title}}</a></h5>
                      <div class="price-wrap">
                        <span class="Price-currency"></span>{{currency()->convert(floatval($enquiry->value), 'USD', currency()->getUserCurrency())}}
                      </div>
                      <div class="bottom-detais">
                        <div class="dealer">
                          <ul>
                            <li><i class="fa fa-star"></i></li>
                            <li><i class="fa fa-star"></i></li>
                            <li><i class="fa fa-star"></i></li>
                            <li><i class="fa fa-star"></i></li>
                            <li><i class="fa fa-star"></i></li>
                          </ul>
                          <p class="seller-type"><i class="fa fa-check-square-o" aria-hidden="true"></i> {{ $enquiry->agency_id == 'Private Seller' ? "Private Seller" : "Dealer" }}</p>
                        </div>
                        <div class="country-flag">
                          <img src="{{url('uploads/',$enquiry->countryflag)}}" alt="">
                          <p class="c-code">{{$enquiry->countrycode}}</p>
                        </div>
                      </div>
                    </div>
    
                    <!-- <a href="javascript:void(0);" title="click here" class="icon"><i class="fa fa-info-circle" aria-hidden="true"></i></a> -->
    
                  </div>
               </div>
           </div>
       </div>
    </div>
    @include('tangiblehtml.innerfooter')
    <script src="/assets/js/jquery.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
        integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
        crossorigin="anonymous"></script>
    <script src="/assets/js/bootstrap.min.js"></script>
    <script src="/assets/js/wow.min.js"></script>
    <script src="/assets/js/slick.min.js"></script>
    <script src="/assets/js/wow.min.js"></script>
    <script src="/assets/js/local.js"></script>

    <script>
    $(document).on('click', '.search-filter-panel .dropdown-menu', function (e) {
      e.stopPropagation();   
    });
    </script>
</body>

</html>

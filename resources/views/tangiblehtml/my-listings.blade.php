<!DOCTYPE html>
<html lang="en">

@include('tangiblehtml.headheader')

<body>
    @include('tangiblehtml.innerheader')
   <div class="custom-top-home-wrapper pt-5 ">
        <div class="container">
            <div class="main-heading mt-5 d-flex justify-content-between">
                <h3>Your Ads</h3>     
                <div class="d-flex align-items-center">
                    <div class="statusTitle mr-3">
                        Ads status :
                    </div>
                    <div class="d-flex align-items-center mr-2"><h1 class="ads-active">&#8226;</h1> Active</div>                   
                    <div class="d-flex align-items-center mr-2"><h1 class="ads-paused">&#8226;</h1> Paused</div>
                    <div class="d-flex align-items-center mr-2"><h1 class="ads-canceled">&#8226;</h1> Canceled</div>
                </div>
            </div>
            <div class="row ">
            @if(count($automobile_listing) > 0)
            @foreach($automobile_listing as $data)
                <div class="col-lg-4 col-md-6 mb-4">
                    <div class="border p-2">
                        <div class="ads-view position-relative">                          
                            <div class="home-list-item">
                                <a class="home-view" href="#"><img src="{{'uploads/'.$automobile_images->firstWhere('listing_id','=', $data->id)->filename}}" alt="" class="img-fluid"></a>
                            </div>                            
                        </div>
                        <div class="home-content mt-1" >                            
                            <h4 class="mb-1 d-flex justify-content-between align-items-center"><a href="#">{{$data->ad_title}}</a> <span class="ads-active">&#8226;</span></h4>
                            <span class="price">{{currency()->convert(floatval($data->value), 'USD', currency()->getUserCurrency())}}</span>
                            <div class="ads-details mt-2">
                                <span>ID  <strong>{{$data->id}}</strong></span>
                                <br>
                                <span>Created <strong>{{date('D, d M, yy', strtotime($data->automobilecreationtime))}}</strong></span>
                            </div>                            
                        </div>
                        <div class="bottom-detais">
                           <a href="manageads/{{$data->id}}/automobile" class="btn btn-outline-dark m-auto">Manage Ads</a>
                        </div>

                    </div>
                </div>
                @endforeach
                @endif
                @if(count($watch_listing) > 0)
            @foreach($watch_listing as $data)
                <div class="col-lg-4 col-md-6 mb-4">
                    <div class="border p-2">
                        <div class="ads-view position-relative">                          
                            <div class="home-list-item">
                                <a class="home-view" href="#"><img src="{{'uploads/'.$watch_images->firstWhere('listing_id','=', $data->id)->filename}}" alt="" class="img-fluid"></a>
                            </div>                            
                        </div>
                        <div class="home-content mt-1" >                            
                            <h4 class="mb-1 d-flex justify-content-between align-items-center"><a href="#">{{$data->ad_title}}</a> <span class="ads-active">&#8226;</span></h4>
                            <span class="price">
        
                            {{ currency()->convert(floatval($data->value), 'USD', currency()->getUserCurrency()) }}
                            
                            </span>
                            <div class="ads-details mt-2">
                                <span>ID  <strong>{{$data->id}}</strong></span>
                                <br>
                                <span>Created <strong>{{date('D, d M, yy', strtotime($data->automobilecreationtime))}}</strong></span>
                            </div>                            
                        </div>
                        <div class="bottom-detais">
                           <a href="manageads/{{$data->id}}/watch" class="btn btn-outline-dark m-auto">Manage Ads</a>
                        </div>

                    </div>
                </div>
                @endforeach
                @endif
                
                @if(count($property_listing) > 0)
            @foreach($property_listing as $data)
                <div class="col-lg-4 col-md-6 mb-4">
                    <div class="border p-2">
                        <div class="ads-view position-relative">                          
                            <div class="home-list-item">
                                <a class="home-view" href="#"><img src="{{'uploads/'.$property_images->firstWhere('listing_id','=', $data->id)->filename}}" alt="" class="img-fluid"></a>
                            </div>                            
                        </div>
                        <div class="home-content mt-1" >                            
                            <h4 class="mb-1 d-flex justify-content-between align-items-center"><a href="#">{{$data->ad_title}}</a> <span class="ads-active">&#8226;</span></h4>
                            <span class="price">{{currency()->convert(floatval($data->value), 'USD', currency()->getUserCurrency())}}</span>
                            <div class="ads-details mt-2">
                                <span>ID  <strong>{{$data->id}}</strong></span>
                                <br>
                                <span>Created <strong>{{date('D, d M, yy', strtotime($data->creationtime))}}</strong></span>
                            </div>                            
                        </div>
                        <div class="bottom-detais">
                           <a href="manageads/{{$data->id}}/property" class="btn btn-outline-dark m-auto">Manage Ads</a>
                        </div>

                    </div>
                </div>
                @endforeach
				@endif
				@if(count($property_listing) == 0 && count($watch_listing)== 0 && count($automobile_listing)== 0)
                <div class="col-lg-4 col-md-6 mb-4">
                    <div class="border p-2">
                        <div class="ads-view position-relative">                          
                            <p>Create Your First Listing</p>
                            <li class="nav-item"> <a href="javascript:void(0);" class="nav-link sell-btn "  data-toggle="modal" data-target=".bd-example-modal-lg">Sell Now</a>
                                <ul aria-labelledby="dropdownMenu1" class="dropdown-menu border-0 shadow">
                                    <li><a href="/privatesellercategoryselect" class="dropdown-item">Create a Private Listing</a></li>
                                    <li><a href="/becomedealer" class="dropdown-item">Become a Dealer</a></li>
                                </ul>
                            </li>                            
                        </div>
                        

                    </div>
                </div>
                @endif
                
                
                
                <!-- <div class="col-lg-4 col-md-6 mb-4">
                    <div class="border p-2">
                        <div class="ads-view position-relative">                          
                            <div class="home-list-item">
                                <a class="home-view" href="#"><img src="images/car-image.jpg" alt="" class="img-fluid"></a>
                            </div>                            
                        </div>
                        <div class="home-content mt-1" >                            
                            <h4 class="mb-1 d-flex justify-content-between align-items-center"><a href="#">Luxury Car - Audi</a> <span class="ads-active">&#8226;</span></h4>
                            <span class="price">$1,790/mo</span>
                            <div class="ads-details mt-2">
                                <span>ID  <strong>SSE-AD-606344</strong></span>
                                <br>
                                <span>Created <strong>Monday, 29 April, 2019</strong></span>                               
                            </div>                            
                        </div>
                        <div class="bottom-detais">
                            <a href="manage-ads.html" class="btn btn-outline-dark m-auto">Manage Ads</a>
                        </div>
                    </div>
                </div> -->
                <!-- <div class="col-lg-4 col-md-6 mb-4">
                    <div class="border p-2">
                        <div class="ads-view position-relative">                          
                            <div class="home-list-item">
                                <a class="home-view" href="#"><img src="images/car-image.jpg" alt="" class="img-fluid"></a>
                            </div>
                          
                        </div>
                        <div class="home-content mt-1" >                            
                            <h4 class="mb-1 d-flex justify-content-between align-items-center"><a href="#">Luxury Car - Audi</a> <span class="ads-active">&#8226;</span></h4>
                            <span class="price">$1,790/mo</span>
                            <div class="ads-details mt-2">
                                <span>ID  <strong>SSE-AD-606344</strong></span>
                                <br>
                                <span>Created <strong>Monday, 29 April, 2019</strong></span>                               
                            </div>                            
                        </div>
                        <div class="bottom-detais">
                            <a href="manage-ads.html" class="btn btn-outline-dark m-auto">Manage Ads</a>
                        </div>

                    </div>
                </div> -->
                <!-- <div class="col-lg-4 col-md-6 mb-4">
                    <div class="border p-2">
                        <div class="ads-view position-relative">                          
                            <div class="home-list-item">
                                <a class="home-view" href="#"><img src="images/car-image.jpg" alt="" class="img-fluid"></a>
                            </div>
                           
                        </div>
                        <div class="home-content mt-1" >                            
                            <h4 class="mb-1 d-flex justify-content-between align-items-center"><a href="#">Luxury Car - Audi</a> <span class="ads-active">&#8226;</span></h4>
                            <span class="price">$1,790/mo</span>
                            <div class="ads-details mt-2">
                                <span>ID  <strong>SSE-AD-606344</strong></span>
                                <br>
                                <span>Created <strong>Monday, 29 April, 2019</strong></span>                               
                            </div>                            
                        </div>
                        <div class="bottom-detais">
                            <a href="manage-ads.html" class="btn btn-outline-dark m-auto">Manage Ads</a>
                        </div>

                    </div>
                </div> -->
                
            </div>
        </div>
    </div>
    @include('tangiblehtml.innerfooter')



    <script src="/assets/js/jquery.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
        integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
        crossorigin="anonymous"></script>
    <script src="/assets/js/bootstrap.min.js"></script>
    <script src="/assets/js/wow.min.js"></script>
    <script src="/assets/js/slick.min.js"></script>
    <script src="/assets/js/wow.min.js"></script>
    <script src="/assets/js/local.js"></script>

    <script>
    $(document).on('click', '.search-filter-panel .dropdown-menu', function (e) {
      e.stopPropagation();   
    });
    </script>
</body>

</html>